<?php
namespace Magento\SalesSequence\Model\Manager;

/**
 * Interceptor class for @see \Magento\SalesSequence\Model\Manager
 */
class Interceptor extends \Magento\SalesSequence\Model\Manager implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\SalesSequence\Model\ResourceModel\Meta $resourceSequenceMeta, \Magento\SalesSequence\Model\SequenceFactory $sequenceFactory)
    {
        $this->___init();
        parent::__construct($resourceSequenceMeta, $sequenceFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function getSequence($entityType, $storeId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSequence');
        if (!$pluginInfo) {
            return parent::getSequence($entityType, $storeId);
        } else {
            return $this->___callPlugins('getSequence', func_get_args(), $pluginInfo);
        }
    }
}
