<?php
namespace Magento\SalesSequence\Model\Sequence;

/**
 * Interceptor class for @see \Magento\SalesSequence\Model\Sequence
 */
class Interceptor extends \Magento\SalesSequence\Model\Sequence implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\SalesSequence\Model\Meta $meta, \Magento\Framework\App\ResourceConnection $resource, $pattern = '%s%\'.09d%s')
    {
        $this->___init();
        parent::__construct($meta, $resource, $pattern);
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrentValue()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCurrentValue');
        if (!$pluginInfo) {
            return parent::getCurrentValue();
        } else {
            return $this->___callPlugins('getCurrentValue', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getNextValue()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getNextValue');
        if (!$pluginInfo) {
            return parent::getNextValue();
        } else {
            return $this->___callPlugins('getNextValue', func_get_args(), $pluginInfo);
        }
    }
}
