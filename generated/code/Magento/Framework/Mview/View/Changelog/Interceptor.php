<?php
namespace Magento\Framework\Mview\View\Changelog;

/**
 * Interceptor class for @see \Magento\Framework\Mview\View\Changelog
 */
class Interceptor extends \Magento\Framework\Mview\View\Changelog implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\ResourceConnection $resource)
    {
        $this->___init();
        parent::__construct($resource);
    }

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'create');
        if (!$pluginInfo) {
            return parent::create();
        } else {
            return $this->___callPlugins('create', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function drop()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'drop');
        if (!$pluginInfo) {
            return parent::drop();
        } else {
            return $this->___callPlugins('drop', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function clear($versionId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'clear');
        if (!$pluginInfo) {
            return parent::clear($versionId);
        } else {
            return $this->___callPlugins('clear', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getList($fromVersionId, $toVersionId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getList');
        if (!$pluginInfo) {
            return parent::getList($fromVersionId, $toVersionId);
        } else {
            return $this->___callPlugins('getList', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getVersion()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getVersion');
        if (!$pluginInfo) {
            return parent::getVersion();
        } else {
            return $this->___callPlugins('getVersion', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getName');
        if (!$pluginInfo) {
            return parent::getName();
        } else {
            return $this->___callPlugins('getName', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getColumnName()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getColumnName');
        if (!$pluginInfo) {
            return parent::getColumnName();
        } else {
            return $this->___callPlugins('getColumnName', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setViewId($viewId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setViewId');
        if (!$pluginInfo) {
            return parent::setViewId($viewId);
        } else {
            return $this->___callPlugins('setViewId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getViewId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getViewId');
        if (!$pluginInfo) {
            return parent::getViewId();
        } else {
            return $this->___callPlugins('getViewId', func_get_args(), $pluginInfo);
        }
    }
}
