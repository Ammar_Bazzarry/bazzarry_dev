<?php
namespace Magento\Catalog\Helper\Product\ProductList;

/**
 * Interceptor class for @see \Magento\Catalog\Helper\Product\ProductList
 */
class Interceptor extends \Magento\Catalog\Helper\Product\ProductList implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->___init();
        parent::__construct($scopeConfig);
    }

    /**
     * {@inheritdoc}
     */
    public function getAvailableViewMode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAvailableViewMode');
        if (!$pluginInfo) {
            return parent::getAvailableViewMode();
        } else {
            return $this->___callPlugins('getAvailableViewMode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultViewMode($options = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDefaultViewMode');
        if (!$pluginInfo) {
            return parent::getDefaultViewMode($options);
        } else {
            return $this->___callPlugins('getDefaultViewMode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultSortField()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDefaultSortField');
        if (!$pluginInfo) {
            return parent::getDefaultSortField();
        } else {
            return $this->___callPlugins('getDefaultSortField', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAvailableLimit($mode)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAvailableLimit');
        if (!$pluginInfo) {
            return parent::getAvailableLimit($mode);
        } else {
            return $this->___callPlugins('getAvailableLimit', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultLimitPerPageValue($viewMode)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDefaultLimitPerPageValue');
        if (!$pluginInfo) {
            return parent::getDefaultLimitPerPageValue($viewMode);
        } else {
            return $this->___callPlugins('getDefaultLimitPerPageValue', func_get_args(), $pluginInfo);
        }
    }
}
