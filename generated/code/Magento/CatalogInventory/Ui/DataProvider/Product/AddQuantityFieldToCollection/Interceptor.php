<?php
namespace Magento\CatalogInventory\Ui\DataProvider\Product\AddQuantityFieldToCollection;

/**
 * Interceptor class for @see \Magento\CatalogInventory\Ui\DataProvider\Product\AddQuantityFieldToCollection
 */
class Interceptor extends \Magento\CatalogInventory\Ui\DataProvider\Product\AddQuantityFieldToCollection implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct()
    {
        $this->___init();
    }

    /**
     * {@inheritdoc}
     */
    public function addField(\Magento\Framework\Data\Collection $collection, $field, $alias = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'addField');
        if (!$pluginInfo) {
            return parent::addField($collection, $field, $alias);
        } else {
            return $this->___callPlugins('addField', func_get_args(), $pluginInfo);
        }
    }
}
