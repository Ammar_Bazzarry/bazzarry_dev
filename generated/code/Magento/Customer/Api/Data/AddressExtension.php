<?php
namespace Magento\Customer\Api\Data;

/**
 * Extension class for @see \Magento\Customer\Api\Data\AddressInterface
 */
class AddressExtension extends \Magento\Framework\Api\AbstractSimpleObject implements AddressExtensionInterface
{
    /**
     * @return string|null
     */
    public function getLocationType()
    {
        return $this->_get('location_type');
    }

    /**
     * @param string $locationType
     * @return $this
     */
    public function setLocationType($locationType)
    {
        $this->setData('location_type', $locationType);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNearestLandmark()
    {
        return $this->_get('nearest_landmark');
    }

    /**
     * @param string $nearestLandmark
     * @return $this
     */
    public function setNearestLandmark($nearestLandmark)
    {
        $this->setData('nearest_landmark', $nearestLandmark);
        return $this;
    }
}
