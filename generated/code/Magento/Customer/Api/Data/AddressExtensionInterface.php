<?php
namespace Magento\Customer\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Customer\Api\Data\AddressInterface
 */
interface AddressExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return string|null
     */
    public function getLocationType();

    /**
     * @param string $locationType
     * @return $this
     */
    public function setLocationType($locationType);

    /**
     * @return string|null
     */
    public function getNearestLandmark();

    /**
     * @param string $nearestLandmark
     * @return $this
     */
    public function setNearestLandmark($nearestLandmark);
}
