<?php
namespace Magento\Ui\Model\Manager;

/**
 * Interceptor class for @see \Magento\Ui\Model\Manager
 */
class Interceptor extends \Magento\Ui\Model\Manager implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\View\Element\UiComponent\Config\Provider\Component\Definition $componentConfigProvider, \Magento\Framework\View\Element\UiComponent\Config\DomMergerInterface $domMerger, \Magento\Framework\View\Element\UiComponent\Config\ReaderFactory $readerFactory, \Magento\Framework\View\Element\UiComponent\ArrayObjectFactory $arrayObjectFactory, \Magento\Framework\View\Element\UiComponent\Config\FileCollector\AggregatedFileCollectorFactory $aggregatedFileCollectorFactory, \Magento\Framework\Config\CacheInterface $cache, \Magento\Framework\Data\Argument\InterpreterInterface $argumentInterpreter, ?\Magento\Framework\Serialize\SerializerInterface $serializer = null)
    {
        $this->___init();
        parent::__construct($componentConfigProvider, $domMerger, $readerFactory, $arrayObjectFactory, $aggregatedFileCollectorFactory, $cache, $argumentInterpreter, $serializer);
    }

    /**
     * {@inheritdoc}
     */
    public function getData($name)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getData');
        if (!$pluginInfo) {
            return parent::getData($name);
        } else {
            return $this->___callPlugins('getData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareData($name)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareData');
        if (!$pluginInfo) {
            return parent::prepareData($name);
        } else {
            return $this->___callPlugins('prepareData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createRawComponentData($component, $evaluated = true)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'createRawComponentData');
        if (!$pluginInfo) {
            return parent::createRawComponentData($component, $evaluated);
        } else {
            return $this->___callPlugins('createRawComponentData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getReader($name)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getReader');
        if (!$pluginInfo) {
            return parent::getReader($name);
        } else {
            return $this->___callPlugins('getReader', func_get_args(), $pluginInfo);
        }
    }
}
