<?php
namespace Magento\Ui\Config\Reader;

/**
 * Interceptor class for @see \Magento\Ui\Config\Reader
 */
class Interceptor extends \Magento\Ui\Config\Reader implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct($fileName, \Magento\Framework\Config\FileResolverInterface $fileResolver, \Magento\Framework\Config\ConverterInterface $converter, \Magento\Ui\Config\Reader\Definition $definitionReader, \Magento\Ui\Config\ReaderFactory $readerFactory, \Magento\Ui\Config\Reader\DomFactory $readerDomFactory, array $idAttributes = array())
    {
        $this->___init();
        parent::__construct($fileName, $fileResolver, $converter, $definitionReader, $readerFactory, $readerDomFactory, $idAttributes);
    }

    /**
     * {@inheritdoc}
     */
    public function read($scope = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'read');
        if (!$pluginInfo) {
            return parent::read($scope);
        } else {
            return $this->___callPlugins('read', func_get_args(), $pluginInfo);
        }
    }
}
