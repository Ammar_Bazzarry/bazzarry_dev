<?php
namespace Magento\Sales\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Sales\Api\Data\OrderInterface
 */
interface OrderExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return \Magento\Sales\Api\Data\ShippingAssignmentInterface[]|null
     */
    public function getShippingAssignments();

    /**
     * @param \Magento\Sales\Api\Data\ShippingAssignmentInterface[] $shippingAssignments
     * @return $this
     */
    public function setShippingAssignments($shippingAssignments);

    /**
     * @return \Magento\Payment\Api\Data\PaymentAdditionalInfoInterface[]|null
     */
    public function getPaymentAdditionalInfo();

    /**
     * @param \Magento\Payment\Api\Data\PaymentAdditionalInfoInterface[] $paymentAdditionalInfo
     * @return $this
     */
    public function setPaymentAdditionalInfo($paymentAdditionalInfo);

    /**
     * @return \Magento\GiftMessage\Api\Data\MessageInterface|null
     */
    public function getGiftMessage();

    /**
     * @param \Magento\GiftMessage\Api\Data\MessageInterface $giftMessage
     * @return $this
     */
    public function setGiftMessage(\Magento\GiftMessage\Api\Data\MessageInterface $giftMessage);

    /**
     * @return \MageWorx\GiftCards\Api\Data\GiftCardDetailsInterface[]|null
     */
    public function getMageworxGiftcardsDetails();

    /**
     * @param \MageWorx\GiftCards\Api\Data\GiftCardDetailsInterface[] $mageworxGiftcardsDetails
     * @return $this
     */
    public function setMageworxGiftcardsDetails($mageworxGiftcardsDetails);

    /**
     * @return string|null
     */
    public function getMageworxGiftcardsDescription();

    /**
     * @param string $mageworxGiftcardsDescription
     * @return $this
     */
    public function setMageworxGiftcardsDescription($mageworxGiftcardsDescription);

    /**
     * @return float|null
     */
    public function getMageworxGiftcardsAmount();

    /**
     * @param float $mageworxGiftcardsAmount
     * @return $this
     */
    public function setMageworxGiftcardsAmount($mageworxGiftcardsAmount);

    /**
     * @return float|null
     */
    public function getBaseMageworxGiftcardsAmount();

    /**
     * @param float $baseMageworxGiftcardsAmount
     * @return $this
     */
    public function setBaseMageworxGiftcardsAmount($baseMageworxGiftcardsAmount);

    /**
     * @return string|null
     */
    public function getAmazonOrderReferenceId();

    /**
     * @param string $amazonOrderReferenceId
     * @return $this
     */
    public function setAmazonOrderReferenceId($amazonOrderReferenceId);

    /**
     * @return \Magento\Tax\Api\Data\OrderTaxDetailsAppliedTaxInterface[]|null
     */
    public function getAppliedTaxes();

    /**
     * @param \Magento\Tax\Api\Data\OrderTaxDetailsAppliedTaxInterface[] $appliedTaxes
     * @return $this
     */
    public function setAppliedTaxes($appliedTaxes);

    /**
     * @return \Magento\Tax\Api\Data\OrderTaxDetailsItemInterface[]|null
     */
    public function getItemAppliedTaxes();

    /**
     * @param \Magento\Tax\Api\Data\OrderTaxDetailsItemInterface[] $itemAppliedTaxes
     * @return $this
     */
    public function setItemAppliedTaxes($itemAppliedTaxes);

    /**
     * @return boolean|null
     */
    public function getConvertingFromQuote();

    /**
     * @param boolean $convertingFromQuote
     * @return $this
     */
    public function setConvertingFromQuote($convertingFromQuote);

    /**
     * @return \Magento\Framework\Api\AttributeValue[]|null
     */
    public function getAmastyOrderAttributes();

    /**
     * @param \Magento\Framework\Api\AttributeValue[] $amastyOrderAttributes
     * @return $this
     */
    public function setAmastyOrderAttributes($amastyOrderAttributes);
}
