<?php
namespace Magento\Paypal\Model\Api\PayflowNvp;

/**
 * Interceptor class for @see \Magento\Paypal\Model\Api\PayflowNvp
 */
class Interceptor extends \Magento\Paypal\Model\Api\PayflowNvp implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Customer\Helper\Address $customerAddress, \Psr\Log\LoggerInterface $logger, \Magento\Payment\Model\Method\Logger $customLogger, \Magento\Framework\Locale\ResolverInterface $localeResolver, \Magento\Directory\Model\RegionFactory $regionFactory, \Magento\Directory\Model\CountryFactory $countryFactory, \Magento\Paypal\Model\Api\ProcessableExceptionFactory $processableExceptionFactory, \Magento\Framework\Exception\LocalizedExceptionFactory $frameworkExceptionFactory, \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory, \Magento\Framework\Math\Random $mathRandom, \Magento\Paypal\Model\Api\NvpFactory $nvpFactory, array $data = array())
    {
        $this->___init();
        parent::__construct($customerAddress, $logger, $customLogger, $localeResolver, $regionFactory, $countryFactory, $processableExceptionFactory, $frameworkExceptionFactory, $curlFactory, $mathRandom, $nvpFactory, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getApiEndpoint()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getApiEndpoint');
        if (!$pluginInfo) {
            return parent::getApiEndpoint();
        } else {
            return $this->___callPlugins('getApiEndpoint', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPartner()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPartner');
        if (!$pluginInfo) {
            return parent::getPartner();
        } else {
            return $this->___callPlugins('getPartner', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getUser');
        if (!$pluginInfo) {
            return parent::getUser();
        } else {
            return $this->___callPlugins('getUser', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPassword');
        if (!$pluginInfo) {
            return parent::getPassword();
        } else {
            return $this->___callPlugins('getPassword', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getVendor()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getVendor');
        if (!$pluginInfo) {
            return parent::getVendor();
        } else {
            return $this->___callPlugins('getVendor', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTender()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTender');
        if (!$pluginInfo) {
            return parent::getTender();
        } else {
            return $this->___callPlugins('getTender', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPaypalTransactionId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaypalTransactionId');
        if (!$pluginInfo) {
            return parent::getPaypalTransactionId();
        } else {
            return $this->___callPlugins('getPaypalTransactionId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callGetTransactionDetails()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callGetTransactionDetails');
        if (!$pluginInfo) {
            return parent::callGetTransactionDetails();
        } else {
            return $this->___callPlugins('callGetTransactionDetails', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getVersion()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getVersion');
        if (!$pluginInfo) {
            return parent::getVersion();
        } else {
            return $this->___callPlugins('getVersion', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBillingAgreementType()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBillingAgreementType');
        if (!$pluginInfo) {
            return parent::getBillingAgreementType();
        } else {
            return $this->___callPlugins('getBillingAgreementType', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callSetExpressCheckout()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callSetExpressCheckout');
        if (!$pluginInfo) {
            return parent::callSetExpressCheckout();
        } else {
            return $this->___callPlugins('callSetExpressCheckout', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callGetExpressCheckoutDetails()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callGetExpressCheckoutDetails');
        if (!$pluginInfo) {
            return parent::callGetExpressCheckoutDetails();
        } else {
            return $this->___callPlugins('callGetExpressCheckoutDetails', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callDoExpressCheckoutPayment()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callDoExpressCheckoutPayment');
        if (!$pluginInfo) {
            return parent::callDoExpressCheckoutPayment();
        } else {
            return $this->___callPlugins('callDoExpressCheckoutPayment', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callDoDirectPayment()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callDoDirectPayment');
        if (!$pluginInfo) {
            return parent::callDoDirectPayment();
        } else {
            return $this->___callPlugins('callDoDirectPayment', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callDoReferenceTransaction()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callDoReferenceTransaction');
        if (!$pluginInfo) {
            return parent::callDoReferenceTransaction();
        } else {
            return $this->___callPlugins('callDoReferenceTransaction', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getIsFraudDetected()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getIsFraudDetected');
        if (!$pluginInfo) {
            return parent::getIsFraudDetected();
        } else {
            return $this->___callPlugins('getIsFraudDetected', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callDoReauthorization()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callDoReauthorization');
        if (!$pluginInfo) {
            return parent::callDoReauthorization();
        } else {
            return $this->___callPlugins('callDoReauthorization', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callDoCapture()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callDoCapture');
        if (!$pluginInfo) {
            return parent::callDoCapture();
        } else {
            return $this->___callPlugins('callDoCapture', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callDoAuthorization()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callDoAuthorization');
        if (!$pluginInfo) {
            return parent::callDoAuthorization();
        } else {
            return $this->___callPlugins('callDoAuthorization', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callDoVoid()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callDoVoid');
        if (!$pluginInfo) {
            return parent::callDoVoid();
        } else {
            return $this->___callPlugins('callDoVoid', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callRefundTransaction()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callRefundTransaction');
        if (!$pluginInfo) {
            return parent::callRefundTransaction();
        } else {
            return $this->___callPlugins('callRefundTransaction', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callManagePendingTransactionStatus()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callManagePendingTransactionStatus');
        if (!$pluginInfo) {
            return parent::callManagePendingTransactionStatus();
        } else {
            return $this->___callPlugins('callManagePendingTransactionStatus', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callGetPalDetails()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callGetPalDetails');
        if (!$pluginInfo) {
            return parent::callGetPalDetails();
        } else {
            return $this->___callPlugins('callGetPalDetails', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callSetCustomerBillingAgreement()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callSetCustomerBillingAgreement');
        if (!$pluginInfo) {
            return parent::callSetCustomerBillingAgreement();
        } else {
            return $this->___callPlugins('callSetCustomerBillingAgreement', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callGetBillingAgreementCustomerDetails()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callGetBillingAgreementCustomerDetails');
        if (!$pluginInfo) {
            return parent::callGetBillingAgreementCustomerDetails();
        } else {
            return $this->___callPlugins('callGetBillingAgreementCustomerDetails', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callCreateBillingAgreement()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callCreateBillingAgreement');
        if (!$pluginInfo) {
            return parent::callCreateBillingAgreement();
        } else {
            return $this->___callPlugins('callCreateBillingAgreement', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function callUpdateBillingAgreement()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'callUpdateBillingAgreement');
        if (!$pluginInfo) {
            return parent::callUpdateBillingAgreement();
        } else {
            return $this->___callPlugins('callUpdateBillingAgreement', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareShippingOptionsCallbackAddress(array $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareShippingOptionsCallbackAddress');
        if (!$pluginInfo) {
            return parent::prepareShippingOptionsCallbackAddress($request);
        } else {
            return $this->___callPlugins('prepareShippingOptionsCallbackAddress', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function formatShippingOptionsCallback()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'formatShippingOptionsCallback');
        if (!$pluginInfo) {
            return parent::formatShippingOptionsCallback();
        } else {
            return $this->___callPlugins('formatShippingOptionsCallback', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function call($methodName, array $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'call');
        if (!$pluginInfo) {
            return parent::call($methodName, $request);
        } else {
            return $this->___callPlugins('call', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setRawResponseNeeded($flag)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setRawResponseNeeded');
        if (!$pluginInfo) {
            return parent::setRawResponseNeeded($flag);
        } else {
            return $this->___callPlugins('setRawResponseNeeded', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getApiUsername()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getApiUsername');
        if (!$pluginInfo) {
            return parent::getApiUsername();
        } else {
            return $this->___callPlugins('getApiUsername', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getApiPassword()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getApiPassword');
        if (!$pluginInfo) {
            return parent::getApiPassword();
        } else {
            return $this->___callPlugins('getApiPassword', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getApiSignature()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getApiSignature');
        if (!$pluginInfo) {
            return parent::getApiSignature();
        } else {
            return $this->___callPlugins('getApiSignature', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getApiCertificate()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getApiCertificate');
        if (!$pluginInfo) {
            return parent::getApiCertificate();
        } else {
            return $this->___callPlugins('getApiCertificate', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBuildNotationCode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBuildNotationCode');
        if (!$pluginInfo) {
            return parent::getBuildNotationCode();
        } else {
            return $this->___callPlugins('getBuildNotationCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUseProxy()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getUseProxy');
        if (!$pluginInfo) {
            return parent::getUseProxy();
        } else {
            return $this->___callPlugins('getUseProxy', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProxyHost()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProxyHost');
        if (!$pluginInfo) {
            return parent::getProxyHost();
        } else {
            return $this->___callPlugins('getProxyHost', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProxyPort()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProxyPort');
        if (!$pluginInfo) {
            return parent::getProxyPort();
        } else {
            return $this->___callPlugins('getProxyPort', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPageStyle()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPageStyle');
        if (!$pluginInfo) {
            return parent::getPageStyle();
        } else {
            return $this->___callPlugins('getPageStyle', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getHdrimg()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getHdrimg');
        if (!$pluginInfo) {
            return parent::getHdrimg();
        } else {
            return $this->___callPlugins('getHdrimg', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getHdrbordercolor()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getHdrbordercolor');
        if (!$pluginInfo) {
            return parent::getHdrbordercolor();
        } else {
            return $this->___callPlugins('getHdrbordercolor', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getHdrbackcolor()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getHdrbackcolor');
        if (!$pluginInfo) {
            return parent::getHdrbackcolor();
        } else {
            return $this->___callPlugins('getHdrbackcolor', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPayflowcolor()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPayflowcolor');
        if (!$pluginInfo) {
            return parent::getPayflowcolor();
        } else {
            return $this->___callPlugins('getPayflowcolor', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentAction()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentAction');
        if (!$pluginInfo) {
            return parent::getPaymentAction();
        } else {
            return $this->___callPlugins('getPaymentAction', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBusinessAccount()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBusinessAccount');
        if (!$pluginInfo) {
            return parent::getBusinessAccount();
        } else {
            return $this->___callPlugins('getBusinessAccount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function import($to, array $publicMap = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'import');
        if (!$pluginInfo) {
            return parent::import($to, $publicMap);
        } else {
            return $this->___callPlugins('import', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function export($from, array $publicMap = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'export');
        if (!$pluginInfo) {
            return parent::export($from, $publicMap);
        } else {
            return $this->___callPlugins('export', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setPaypalCart(\Magento\Paypal\Model\Cart $cart)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setPaypalCart');
        if (!$pluginInfo) {
            return parent::setPaypalCart($cart);
        } else {
            return $this->___callPlugins('setPaypalCart', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setConfigObject(\Magento\Paypal\Model\Config $config)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setConfigObject');
        if (!$pluginInfo) {
            return parent::setConfigObject($config);
        } else {
            return $this->___callPlugins('setConfigObject', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getLocale()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getLocale');
        if (!$pluginInfo) {
            return parent::getLocale();
        } else {
            return $this->___callPlugins('getLocale', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFraudManagementFiltersEnabled()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFraudManagementFiltersEnabled');
        if (!$pluginInfo) {
            return parent::getFraudManagementFiltersEnabled();
        } else {
            return $this->___callPlugins('getFraudManagementFiltersEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDebugFlag()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDebugFlag');
        if (!$pluginInfo) {
            return parent::getDebugFlag();
        } else {
            return $this->___callPlugins('getDebugFlag', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUseCertAuthentication()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getUseCertAuthentication');
        if (!$pluginInfo) {
            return parent::getUseCertAuthentication();
        } else {
            return $this->___callPlugins('getUseCertAuthentication', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDebugReplacePrivateDataKeys()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDebugReplacePrivateDataKeys');
        if (!$pluginInfo) {
            return parent::getDebugReplacePrivateDataKeys();
        } else {
            return $this->___callPlugins('getDebugReplacePrivateDataKeys', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addData(array $arr)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'addData');
        if (!$pluginInfo) {
            return parent::addData($arr);
        } else {
            return $this->___callPlugins('addData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setData($key, $value = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setData');
        if (!$pluginInfo) {
            return parent::setData($key, $value);
        } else {
            return $this->___callPlugins('setData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function unsetData($key = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'unsetData');
        if (!$pluginInfo) {
            return parent::unsetData($key);
        } else {
            return $this->___callPlugins('unsetData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getData($key = '', $index = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getData');
        if (!$pluginInfo) {
            return parent::getData($key, $index);
        } else {
            return $this->___callPlugins('getData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDataByPath($path)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDataByPath');
        if (!$pluginInfo) {
            return parent::getDataByPath($path);
        } else {
            return $this->___callPlugins('getDataByPath', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDataByKey($key)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDataByKey');
        if (!$pluginInfo) {
            return parent::getDataByKey($key);
        } else {
            return $this->___callPlugins('getDataByKey', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDataUsingMethod($key, $args = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setDataUsingMethod');
        if (!$pluginInfo) {
            return parent::setDataUsingMethod($key, $args);
        } else {
            return $this->___callPlugins('setDataUsingMethod', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDataUsingMethod($key, $args = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDataUsingMethod');
        if (!$pluginInfo) {
            return parent::getDataUsingMethod($key, $args);
        } else {
            return $this->___callPlugins('getDataUsingMethod', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasData($key = '')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'hasData');
        if (!$pluginInfo) {
            return parent::hasData($key);
        } else {
            return $this->___callPlugins('hasData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(array $keys = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'toArray');
        if (!$pluginInfo) {
            return parent::toArray($keys);
        } else {
            return $this->___callPlugins('toArray', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function convertToArray(array $keys = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'convertToArray');
        if (!$pluginInfo) {
            return parent::convertToArray($keys);
        } else {
            return $this->___callPlugins('convertToArray', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function toXml(array $keys = array(), $rootName = 'item', $addOpenTag = false, $addCdata = true)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'toXml');
        if (!$pluginInfo) {
            return parent::toXml($keys, $rootName, $addOpenTag, $addCdata);
        } else {
            return $this->___callPlugins('toXml', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function convertToXml(array $arrAttributes = array(), $rootName = 'item', $addOpenTag = false, $addCdata = true)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'convertToXml');
        if (!$pluginInfo) {
            return parent::convertToXml($arrAttributes, $rootName, $addOpenTag, $addCdata);
        } else {
            return $this->___callPlugins('convertToXml', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function toJson(array $keys = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'toJson');
        if (!$pluginInfo) {
            return parent::toJson($keys);
        } else {
            return $this->___callPlugins('toJson', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function convertToJson(array $keys = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'convertToJson');
        if (!$pluginInfo) {
            return parent::convertToJson($keys);
        } else {
            return $this->___callPlugins('convertToJson', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function toString($format = '')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'toString');
        if (!$pluginInfo) {
            return parent::toString($format);
        } else {
            return $this->___callPlugins('toString', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function __call($method, $args)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, '__call');
        if (!$pluginInfo) {
            return parent::__call($method, $args);
        } else {
            return $this->___callPlugins('__call', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isEmpty');
        if (!$pluginInfo) {
            return parent::isEmpty();
        } else {
            return $this->___callPlugins('isEmpty', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($keys = array(), $valueSeparator = '=', $fieldSeparator = ' ', $quote = '"')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'serialize');
        if (!$pluginInfo) {
            return parent::serialize($keys, $valueSeparator, $fieldSeparator, $quote);
        } else {
            return $this->___callPlugins('serialize', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function debug($data = null, &$objects = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'debug');
        if (!$pluginInfo) {
            return parent::debug($data, $objects);
        } else {
            return $this->___callPlugins('debug', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'offsetSet');
        if (!$pluginInfo) {
            return parent::offsetSet($offset, $value);
        } else {
            return $this->___callPlugins('offsetSet', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'offsetExists');
        if (!$pluginInfo) {
            return parent::offsetExists($offset);
        } else {
            return $this->___callPlugins('offsetExists', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'offsetUnset');
        if (!$pluginInfo) {
            return parent::offsetUnset($offset);
        } else {
            return $this->___callPlugins('offsetUnset', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'offsetGet');
        if (!$pluginInfo) {
            return parent::offsetGet($offset);
        } else {
            return $this->___callPlugins('offsetGet', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function formatPrice($price)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'formatPrice');
        if (!$pluginInfo) {
            return parent::formatPrice($price);
        } else {
            return $this->___callPlugins('formatPrice', func_get_args(), $pluginInfo);
        }
    }
}
