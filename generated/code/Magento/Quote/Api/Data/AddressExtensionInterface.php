<?php
namespace Magento\Quote\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Quote\Api\Data\AddressInterface
 */
interface AddressExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return \Amasty\Conditions\Api\Data\AddressInterface|null
     */
    public function getAdvancedConditions();

    /**
     * @param \Amasty\Conditions\Api\Data\AddressInterface $advancedConditions
     * @return $this
     */
    public function setAdvancedConditions(\Amasty\Conditions\Api\Data\AddressInterface $advancedConditions);

    /**
     * @return \Magento\Framework\Api\AttributeInterface[]|null
     */
    public function getCheckoutFields();

    /**
     * @param \Magento\Framework\Api\AttributeInterface[] $checkoutFields
     * @return $this
     */
    public function setCheckoutFields($checkoutFields);

    /**
     * @return string|null
     */
    public function getLocationType();

    /**
     * @param string $locationType
     * @return $this
     */
    public function setLocationType($locationType);

    /**
     * @return string|null
     */
    public function getNearestLandmark();

    /**
     * @param string $nearestLandmark
     * @return $this
     */
    public function setNearestLandmark($nearestLandmark);

    /**
     * @return int|null
     */
    public function getPickupstoreId();

    /**
     * @param int $pickupstoreId
     * @return $this
     */
    public function setPickupstoreId($pickupstoreId);
}
