<?php
namespace Magento\Quote\Api\Data;

/**
 * Extension class for @see \Magento\Quote\Api\Data\AddressInterface
 */
class AddressExtension extends \Magento\Framework\Api\AbstractSimpleObject implements AddressExtensionInterface
{
    /**
     * @return \Amasty\Conditions\Api\Data\AddressInterface|null
     */
    public function getAdvancedConditions()
    {
        return $this->_get('advanced_conditions');
    }

    /**
     * @param \Amasty\Conditions\Api\Data\AddressInterface $advancedConditions
     * @return $this
     */
    public function setAdvancedConditions(\Amasty\Conditions\Api\Data\AddressInterface $advancedConditions)
    {
        $this->setData('advanced_conditions', $advancedConditions);
        return $this;
    }

    /**
     * @return \Magento\Framework\Api\AttributeInterface[]|null
     */
    public function getCheckoutFields()
    {
        return $this->_get('checkout_fields');
    }

    /**
     * @param \Magento\Framework\Api\AttributeInterface[] $checkoutFields
     * @return $this
     */
    public function setCheckoutFields($checkoutFields)
    {
        $this->setData('checkout_fields', $checkoutFields);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocationType()
    {
        return $this->_get('location_type');
    }

    /**
     * @param string $locationType
     * @return $this
     */
    public function setLocationType($locationType)
    {
        $this->setData('location_type', $locationType);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNearestLandmark()
    {
        return $this->_get('nearest_landmark');
    }

    /**
     * @param string $nearestLandmark
     * @return $this
     */
    public function setNearestLandmark($nearestLandmark)
    {
        $this->setData('nearest_landmark', $nearestLandmark);
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPickupstoreId()
    {
        return $this->_get('pickupstore_id');
    }

    /**
     * @param int $pickupstoreId
     * @return $this
     */
    public function setPickupstoreId($pickupstoreId)
    {
        $this->setData('pickupstore_id', $pickupstoreId);
        return $this;
    }
}
