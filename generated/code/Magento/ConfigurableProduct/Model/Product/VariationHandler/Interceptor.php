<?php
namespace Magento\ConfigurableProduct\Model\Product\VariationHandler;

/**
 * Interceptor class for @see \Magento\ConfigurableProduct\Model\Product\VariationHandler
 */
class Interceptor extends \Magento\ConfigurableProduct\Model\Product\VariationHandler implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableProduct, \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory, \Magento\Eav\Model\EntityFactory $entityFactory, \Magento\Catalog\Model\ProductFactory $productFactory, \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration, \Magento\Catalog\Model\Product\Gallery\Processor $mediaGalleryProcessor)
    {
        $this->___init();
        parent::__construct($configurableProduct, $attributeSetFactory, $entityFactory, $productFactory, $stockConfiguration, $mediaGalleryProcessor);
    }

    /**
     * {@inheritdoc}
     */
    public function generateSimpleProducts($parentProduct, $productsData)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'generateSimpleProducts');
        if (!$pluginInfo) {
            return parent::generateSimpleProducts($parentProduct, $productsData);
        } else {
            return $this->___callPlugins('generateSimpleProducts', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareAttributeSet(\Magento\Catalog\Model\Product $product)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareAttributeSet');
        if (!$pluginInfo) {
            return parent::prepareAttributeSet($product);
        } else {
            return $this->___callPlugins('prepareAttributeSet', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function duplicateImagesForVariations($productsData)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'duplicateImagesForVariations');
        if (!$pluginInfo) {
            return parent::duplicateImagesForVariations($productsData);
        } else {
            return $this->___callPlugins('duplicateImagesForVariations', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function processMediaGallery($product, $productData)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'processMediaGallery');
        if (!$pluginInfo) {
            return parent::processMediaGallery($product, $productData);
        } else {
            return $this->___callPlugins('processMediaGallery', func_get_args(), $pluginInfo);
        }
    }
}
