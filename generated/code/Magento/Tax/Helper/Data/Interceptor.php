<?php
namespace Magento\Tax\Helper\Data;

/**
 * Interceptor class for @see \Magento\Tax\Helper\Data
 */
class Interceptor extends \Magento\Tax\Helper\Data implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Helper\Context $context, \Magento\Framework\Json\Helper\Data $jsonHelper, \Magento\Tax\Model\Config $taxConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Locale\FormatInterface $localeFormat, \Magento\Tax\Model\ResourceModel\Sales\Order\Tax\CollectionFactory $orderTaxCollectionFactory, \Magento\Framework\Locale\ResolverInterface $localeResolver, \Magento\Catalog\Helper\Data $catalogHelper, \Magento\Tax\Api\OrderTaxManagementInterface $orderTaxManagement, \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency, ?\Magento\Framework\Serialize\Serializer\Json $serializer = null)
    {
        $this->___init();
        parent::__construct($context, $jsonHelper, $taxConfig, $storeManager, $localeFormat, $orderTaxCollectionFactory, $localeResolver, $catalogHelper, $orderTaxManagement, $priceCurrency, $serializer);
    }

    /**
     * {@inheritdoc}
     */
    public function getPostCodeSubStringLength()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPostCodeSubStringLength');
        if (!$pluginInfo) {
            return parent::getPostCodeSubStringLength();
        } else {
            return $this->___callPlugins('getPostCodeSubStringLength', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getConfig');
        if (!$pluginInfo) {
            return parent::getConfig();
        } else {
            return $this->___callPlugins('getConfig', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function priceIncludesTax($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'priceIncludesTax');
        if (!$pluginInfo) {
            return parent::priceIncludesTax($store);
        } else {
            return $this->___callPlugins('priceIncludesTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function applyTaxAfterDiscount($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'applyTaxAfterDiscount');
        if (!$pluginInfo) {
            return parent::applyTaxAfterDiscount($store);
        } else {
            return $this->___callPlugins('applyTaxAfterDiscount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPriceDisplayType($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPriceDisplayType');
        if (!$pluginInfo) {
            return parent::getPriceDisplayType($store);
        } else {
            return $this->___callPlugins('getPriceDisplayType', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function needPriceConversion($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'needPriceConversion');
        if (!$pluginInfo) {
            return parent::needPriceConversion($store);
        } else {
            return $this->___callPlugins('needPriceConversion', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displayFullSummary($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displayFullSummary');
        if (!$pluginInfo) {
            return parent::displayFullSummary($store);
        } else {
            return $this->___callPlugins('displayFullSummary', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displayZeroTax($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displayZeroTax');
        if (!$pluginInfo) {
            return parent::displayZeroTax($store);
        } else {
            return $this->___callPlugins('displayZeroTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displayCartPriceInclTax($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displayCartPriceInclTax');
        if (!$pluginInfo) {
            return parent::displayCartPriceInclTax($store);
        } else {
            return $this->___callPlugins('displayCartPriceInclTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displayCartPriceExclTax($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displayCartPriceExclTax');
        if (!$pluginInfo) {
            return parent::displayCartPriceExclTax($store);
        } else {
            return $this->___callPlugins('displayCartPriceExclTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displayCartBothPrices($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displayCartBothPrices');
        if (!$pluginInfo) {
            return parent::displayCartBothPrices($store);
        } else {
            return $this->___callPlugins('displayCartBothPrices', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displaySalesPriceInclTax($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displaySalesPriceInclTax');
        if (!$pluginInfo) {
            return parent::displaySalesPriceInclTax($store);
        } else {
            return $this->___callPlugins('displaySalesPriceInclTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displaySalesPriceExclTax($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displaySalesPriceExclTax');
        if (!$pluginInfo) {
            return parent::displaySalesPriceExclTax($store);
        } else {
            return $this->___callPlugins('displaySalesPriceExclTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displaySalesBothPrices($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displaySalesBothPrices');
        if (!$pluginInfo) {
            return parent::displaySalesBothPrices($store);
        } else {
            return $this->___callPlugins('displaySalesBothPrices', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displaySalesSubtotalBoth($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displaySalesSubtotalBoth');
        if (!$pluginInfo) {
            return parent::displaySalesSubtotalBoth($store);
        } else {
            return $this->___callPlugins('displaySalesSubtotalBoth', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displaySalesSubtotalInclTax($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displaySalesSubtotalInclTax');
        if (!$pluginInfo) {
            return parent::displaySalesSubtotalInclTax($store);
        } else {
            return $this->___callPlugins('displaySalesSubtotalInclTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displaySalesSubtotalExclTax($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displaySalesSubtotalExclTax');
        if (!$pluginInfo) {
            return parent::displaySalesSubtotalExclTax($store);
        } else {
            return $this->___callPlugins('displaySalesSubtotalExclTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPriceFormat($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPriceFormat');
        if (!$pluginInfo) {
            return parent::getPriceFormat($store);
        } else {
            return $this->___callPlugins('getPriceFormat', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displayPriceIncludingTax()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displayPriceIncludingTax');
        if (!$pluginInfo) {
            return parent::displayPriceIncludingTax();
        } else {
            return $this->___callPlugins('displayPriceIncludingTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displayPriceExcludingTax()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displayPriceExcludingTax');
        if (!$pluginInfo) {
            return parent::displayPriceExcludingTax();
        } else {
            return $this->___callPlugins('displayPriceExcludingTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displayBothPrices($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displayBothPrices');
        if (!$pluginInfo) {
            return parent::displayBothPrices($store);
        } else {
            return $this->___callPlugins('displayBothPrices', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function shippingPriceIncludesTax($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'shippingPriceIncludesTax');
        if (!$pluginInfo) {
            return parent::shippingPriceIncludesTax($store);
        } else {
            return $this->___callPlugins('shippingPriceIncludesTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getShippingPriceDisplayType($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getShippingPriceDisplayType');
        if (!$pluginInfo) {
            return parent::getShippingPriceDisplayType($store);
        } else {
            return $this->___callPlugins('getShippingPriceDisplayType', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displayShippingPriceIncludingTax()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displayShippingPriceIncludingTax');
        if (!$pluginInfo) {
            return parent::displayShippingPriceIncludingTax();
        } else {
            return $this->___callPlugins('displayShippingPriceIncludingTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displayShippingPriceExcludingTax()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displayShippingPriceExcludingTax');
        if (!$pluginInfo) {
            return parent::displayShippingPriceExcludingTax();
        } else {
            return $this->___callPlugins('displayShippingPriceExcludingTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function displayShippingBothPrices()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'displayShippingBothPrices');
        if (!$pluginInfo) {
            return parent::displayShippingBothPrices();
        } else {
            return $this->___callPlugins('displayShippingBothPrices', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getShippingTaxClass($store)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getShippingTaxClass');
        if (!$pluginInfo) {
            return parent::getShippingTaxClass($store);
        } else {
            return $this->___callPlugins('getShippingTaxClass', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getShippingPrice($price, $includingTax = null, $shippingAddress = null, $ctc = null, $store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getShippingPrice');
        if (!$pluginInfo) {
            return parent::getShippingPrice($price, $includingTax, $shippingAddress, $ctc, $store);
        } else {
            return $this->___callPlugins('getShippingPrice', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function discountTax($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'discountTax');
        if (!$pluginInfo) {
            return parent::discountTax($store);
        } else {
            return $this->___callPlugins('discountTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTaxBasedOn($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTaxBasedOn');
        if (!$pluginInfo) {
            return parent::getTaxBasedOn($store);
        } else {
            return $this->___callPlugins('getTaxBasedOn', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function applyTaxOnCustomPrice($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'applyTaxOnCustomPrice');
        if (!$pluginInfo) {
            return parent::applyTaxOnCustomPrice($store);
        } else {
            return $this->___callPlugins('applyTaxOnCustomPrice', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function applyTaxOnOriginalPrice($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'applyTaxOnOriginalPrice');
        if (!$pluginInfo) {
            return parent::applyTaxOnOriginalPrice($store);
        } else {
            return $this->___callPlugins('applyTaxOnOriginalPrice', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCalculationSequence($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCalculationSequence');
        if (!$pluginInfo) {
            return parent::getCalculationSequence($store);
        } else {
            return $this->___callPlugins('getCalculationSequence', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCalculationAlgorithm($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCalculationAlgorithm');
        if (!$pluginInfo) {
            return parent::getCalculationAlgorithm($store);
        } else {
            return $this->___callPlugins('getCalculationAlgorithm', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCalculatedTaxes($source)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCalculatedTaxes');
        if (!$pluginInfo) {
            return parent::getCalculatedTaxes($source);
        } else {
            return $this->___callPlugins('getCalculatedTaxes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultCustomerTaxClass()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDefaultCustomerTaxClass');
        if (!$pluginInfo) {
            return parent::getDefaultCustomerTaxClass();
        } else {
            return $this->___callPlugins('getDefaultCustomerTaxClass', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultProductTaxClass()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDefaultProductTaxClass');
        if (!$pluginInfo) {
            return parent::getDefaultProductTaxClass();
        } else {
            return $this->___callPlugins('getDefaultProductTaxClass', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isCrossBorderTradeEnabled($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isCrossBorderTradeEnabled');
        if (!$pluginInfo) {
            return parent::isCrossBorderTradeEnabled($store);
        } else {
            return $this->___callPlugins('isCrossBorderTradeEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isCatalogPriceDisplayAffectedByTax($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isCatalogPriceDisplayAffectedByTax');
        if (!$pluginInfo) {
            return parent::isCatalogPriceDisplayAffectedByTax($store);
        } else {
            return $this->___callPlugins('isCatalogPriceDisplayAffectedByTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModuleOutputEnabled');
        if (!$pluginInfo) {
            return parent::isModuleOutputEnabled($moduleName);
        } else {
            return $this->___callPlugins('isModuleOutputEnabled', func_get_args(), $pluginInfo);
        }
    }
}
