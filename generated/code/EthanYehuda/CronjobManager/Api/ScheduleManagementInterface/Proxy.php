<?php
namespace EthanYehuda\CronjobManager\Api\ScheduleManagementInterface;

/**
 * Proxy class for @see \EthanYehuda\CronjobManager\Api\ScheduleManagementInterface
 */
class Proxy implements \EthanYehuda\CronjobManager\Api\ScheduleManagementInterface, \Magento\Framework\ObjectManager\NoninterceptableInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Proxied instance name
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Proxied instance
     *
     * @var \EthanYehuda\CronjobManager\Api\ScheduleManagementInterface
     */
    protected $_subject = null;

    /**
     * Instance shareability flag
     *
     * @var bool
     */
    protected $_isShared = null;

    /**
     * Proxy constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     * @param bool $shared
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\EthanYehuda\\CronjobManager\\Api\\ScheduleManagementInterface', $shared = true)
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
        $this->_isShared = $shared;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['_subject', '_isShared', '_instanceName'];
    }

    /**
     * Retrieve ObjectManager from global scope
     */
    public function __wakeup()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Clone proxied instance
     */
    public function __clone()
    {
        $this->_subject = clone $this->_getSubject();
    }

    /**
     * Get proxied instance
     *
     * @return \EthanYehuda\CronjobManager\Api\ScheduleManagementInterface
     */
    protected function _getSubject()
    {
        if (!$this->_subject) {
            $this->_subject = true === $this->_isShared
                ? $this->_objectManager->get($this->_instanceName)
                : $this->_objectManager->create($this->_instanceName);
        }
        return $this->_subject;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(int $scheduleId) : bool
    {
        return $this->_getSubject()->execute($scheduleId);
    }

    /**
     * {@inheritdoc}
     */
    public function listJobs() : array
    {
        return $this->_getSubject()->listJobs();
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupId(string $jobCode, $groups = null)
    {
        return $this->_getSubject()->getGroupId($jobCode, $groups);
    }

    /**
     * {@inheritdoc}
     */
    public function createSchedule(string $jobCode, $time = null) : \Magento\Cron\Model\Schedule
    {
        return $this->_getSubject()->createSchedule($jobCode, $time);
    }

    /**
     * {@inheritdoc}
     */
    public function scheduleNow(string $jobCode) : \Magento\Cron\Model\Schedule
    {
        return $this->_getSubject()->scheduleNow($jobCode);
    }

    /**
     * {@inheritdoc}
     */
    public function schedule(string $jobCode, int $time) : \Magento\Cron\Model\Schedule
    {
        return $this->_getSubject()->schedule($jobCode, $time);
    }

    /**
     * {@inheritdoc}
     */
    public function flush() : bool
    {
        return $this->_getSubject()->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function kill(int $jobId, int $timestamp) : bool
    {
        return $this->_getSubject()->kill($jobId, $timestamp);
    }
}
