<?php
namespace Tigren\AdvancedCheckout\Override\Magento\Customer\Model\Data\Address;

/**
 * Interceptor class for @see \Tigren\AdvancedCheckout\Override\Magento\Customer\Model\Data\Address
 */
class Interceptor extends \Tigren\AdvancedCheckout\Override\Magento\Customer\Model\Data\Address implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory, \Magento\Framework\Api\AttributeValueFactory $attributeValueFactory, \Magento\Customer\Api\AddressMetadataInterface $metadataService, $data = array())
    {
        $this->___init();
        parent::__construct($extensionFactory, $attributeValueFactory, $metadataService, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getLocationType()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getLocationType');
        if (!$pluginInfo) {
            return parent::getLocationType();
        } else {
            return $this->___callPlugins('getLocationType', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setLocationType($locationType)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setLocationType');
        if (!$pluginInfo) {
            return parent::setLocationType($locationType);
        } else {
            return $this->___callPlugins('setLocationType', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getNearestLandmark()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getNearestLandmark');
        if (!$pluginInfo) {
            return parent::getNearestLandmark();
        } else {
            return $this->___callPlugins('getNearestLandmark', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setNearestLandmark($nearestLandmark)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setNearestLandmark');
        if (!$pluginInfo) {
            return parent::setNearestLandmark($nearestLandmark);
        } else {
            return $this->___callPlugins('setNearestLandmark', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getId');
        if (!$pluginInfo) {
            return parent::getId();
        } else {
            return $this->___callPlugins('getId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRegion()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRegion');
        if (!$pluginInfo) {
            return parent::getRegion();
        } else {
            return $this->___callPlugins('getRegion', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRegionId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRegionId');
        if (!$pluginInfo) {
            return parent::getRegionId();
        } else {
            return $this->___callPlugins('getRegionId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCountryId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCountryId');
        if (!$pluginInfo) {
            return parent::getCountryId();
        } else {
            return $this->___callPlugins('getCountryId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getStreet()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getStreet');
        if (!$pluginInfo) {
            return parent::getStreet();
        } else {
            return $this->___callPlugins('getStreet', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCompany()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCompany');
        if (!$pluginInfo) {
            return parent::getCompany();
        } else {
            return $this->___callPlugins('getCompany', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTelephone()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTelephone');
        if (!$pluginInfo) {
            return parent::getTelephone();
        } else {
            return $this->___callPlugins('getTelephone', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFax()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFax');
        if (!$pluginInfo) {
            return parent::getFax();
        } else {
            return $this->___callPlugins('getFax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPostcode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPostcode');
        if (!$pluginInfo) {
            return parent::getPostcode();
        } else {
            return $this->___callPlugins('getPostcode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCity()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCity');
        if (!$pluginInfo) {
            return parent::getCity();
        } else {
            return $this->___callPlugins('getCity', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstname()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFirstname');
        if (!$pluginInfo) {
            return parent::getFirstname();
        } else {
            return $this->___callPlugins('getFirstname', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getLastname()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getLastname');
        if (!$pluginInfo) {
            return parent::getLastname();
        } else {
            return $this->___callPlugins('getLastname', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMiddlename()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMiddlename');
        if (!$pluginInfo) {
            return parent::getMiddlename();
        } else {
            return $this->___callPlugins('getMiddlename', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPrefix()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPrefix');
        if (!$pluginInfo) {
            return parent::getPrefix();
        } else {
            return $this->___callPlugins('getPrefix', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSuffix()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSuffix');
        if (!$pluginInfo) {
            return parent::getSuffix();
        } else {
            return $this->___callPlugins('getSuffix', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getVatId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getVatId');
        if (!$pluginInfo) {
            return parent::getVatId();
        } else {
            return $this->___callPlugins('getVatId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomerId');
        if (!$pluginInfo) {
            return parent::getCustomerId();
        } else {
            return $this->___callPlugins('getCustomerId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isDefaultShipping()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isDefaultShipping');
        if (!$pluginInfo) {
            return parent::isDefaultShipping();
        } else {
            return $this->___callPlugins('isDefaultShipping', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isDefaultBilling()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isDefaultBilling');
        if (!$pluginInfo) {
            return parent::isDefaultBilling();
        } else {
            return $this->___callPlugins('isDefaultBilling', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setId');
        if (!$pluginInfo) {
            return parent::setId($id);
        } else {
            return $this->___callPlugins('setId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomerId($customerId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setCustomerId');
        if (!$pluginInfo) {
            return parent::setCustomerId($customerId);
        } else {
            return $this->___callPlugins('setCustomerId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setRegion(?\Magento\Customer\Api\Data\RegionInterface $region = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setRegion');
        if (!$pluginInfo) {
            return parent::setRegion($region);
        } else {
            return $this->___callPlugins('setRegion', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setRegionId($regionId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setRegionId');
        if (!$pluginInfo) {
            return parent::setRegionId($regionId);
        } else {
            return $this->___callPlugins('setRegionId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setCountryId($countryId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setCountryId');
        if (!$pluginInfo) {
            return parent::setCountryId($countryId);
        } else {
            return $this->___callPlugins('setCountryId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setStreet(array $street)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setStreet');
        if (!$pluginInfo) {
            return parent::setStreet($street);
        } else {
            return $this->___callPlugins('setStreet', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setCompany($company)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setCompany');
        if (!$pluginInfo) {
            return parent::setCompany($company);
        } else {
            return $this->___callPlugins('setCompany', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setTelephone($telephone)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setTelephone');
        if (!$pluginInfo) {
            return parent::setTelephone($telephone);
        } else {
            return $this->___callPlugins('setTelephone', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setFax($fax)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setFax');
        if (!$pluginInfo) {
            return parent::setFax($fax);
        } else {
            return $this->___callPlugins('setFax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setPostcode($postcode)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setPostcode');
        if (!$pluginInfo) {
            return parent::setPostcode($postcode);
        } else {
            return $this->___callPlugins('setPostcode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setCity($city)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setCity');
        if (!$pluginInfo) {
            return parent::setCity($city);
        } else {
            return $this->___callPlugins('setCity', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setFirstname($firstName)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setFirstname');
        if (!$pluginInfo) {
            return parent::setFirstname($firstName);
        } else {
            return $this->___callPlugins('setFirstname', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setLastname($lastName)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setLastname');
        if (!$pluginInfo) {
            return parent::setLastname($lastName);
        } else {
            return $this->___callPlugins('setLastname', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setMiddlename($middleName)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setMiddlename');
        if (!$pluginInfo) {
            return parent::setMiddlename($middleName);
        } else {
            return $this->___callPlugins('setMiddlename', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setPrefix($prefix)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setPrefix');
        if (!$pluginInfo) {
            return parent::setPrefix($prefix);
        } else {
            return $this->___callPlugins('setPrefix', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setSuffix($suffix)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setSuffix');
        if (!$pluginInfo) {
            return parent::setSuffix($suffix);
        } else {
            return $this->___callPlugins('setSuffix', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setVatId($vatId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setVatId');
        if (!$pluginInfo) {
            return parent::setVatId($vatId);
        } else {
            return $this->___callPlugins('setVatId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setIsDefaultShipping($isDefaultShipping)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setIsDefaultShipping');
        if (!$pluginInfo) {
            return parent::setIsDefaultShipping($isDefaultShipping);
        } else {
            return $this->___callPlugins('setIsDefaultShipping', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setIsDefaultBilling($isDefaultBilling)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setIsDefaultBilling');
        if (!$pluginInfo) {
            return parent::setIsDefaultBilling($isDefaultBilling);
        } else {
            return $this->___callPlugins('setIsDefaultBilling', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getExtensionAttributes()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getExtensionAttributes');
        if (!$pluginInfo) {
            return parent::getExtensionAttributes();
        } else {
            return $this->___callPlugins('getExtensionAttributes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setExtensionAttributes(\Magento\Customer\Api\Data\AddressExtensionInterface $extensionAttributes)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setExtensionAttributes');
        if (!$pluginInfo) {
            return parent::setExtensionAttributes($extensionAttributes);
        } else {
            return $this->___callPlugins('setExtensionAttributes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomAttribute($attributeCode)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomAttribute');
        if (!$pluginInfo) {
            return parent::getCustomAttribute($attributeCode);
        } else {
            return $this->___callPlugins('getCustomAttribute', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomAttributes()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomAttributes');
        if (!$pluginInfo) {
            return parent::getCustomAttributes();
        } else {
            return $this->___callPlugins('getCustomAttributes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomAttributes(array $attributes)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setCustomAttributes');
        if (!$pluginInfo) {
            return parent::setCustomAttributes($attributes);
        } else {
            return $this->___callPlugins('setCustomAttributes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomAttribute($attributeCode, $attributeValue)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setCustomAttribute');
        if (!$pluginInfo) {
            return parent::setCustomAttribute($attributeCode, $attributeValue);
        } else {
            return $this->___callPlugins('setCustomAttribute', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setData($key, $value)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setData');
        if (!$pluginInfo) {
            return parent::setData($key, $value);
        } else {
            return $this->___callPlugins('setData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function __toArray()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, '__toArray');
        if (!$pluginInfo) {
            return parent::__toArray();
        } else {
            return $this->___callPlugins('__toArray', func_get_args(), $pluginInfo);
        }
    }
}
