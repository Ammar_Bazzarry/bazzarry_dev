<?php
namespace Vnecoms\Vendors\Plugin\Widget\Button\Toolbar;

/**
 * Interceptor class for @see \Vnecoms\Vendors\Plugin\Widget\Button\Toolbar
 */
class Interceptor extends \Vnecoms\Vendors\Plugin\Widget\Button\Toolbar implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct()
    {
        $this->___init();
    }

    /**
     * {@inheritdoc}
     */
    public function aroundPushButtons(\Magento\Backend\Block\Widget\Button\Toolbar $subject, \Closure $proceed, \Magento\Framework\View\Element\AbstractBlock $context, \Magento\Backend\Block\Widget\Button\ButtonList $buttonList)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'aroundPushButtons');
        if (!$pluginInfo) {
            return parent::aroundPushButtons($subject, $proceed, $context, $buttonList);
        } else {
            return $this->___callPlugins('aroundPushButtons', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function pushButtons(\Magento\Framework\View\Element\AbstractBlock $context, \Magento\Backend\Block\Widget\Button\ButtonList $buttonList)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'pushButtons');
        if (!$pluginInfo) {
            return parent::pushButtons($context, $buttonList);
        } else {
            return $this->___callPlugins('pushButtons', func_get_args(), $pluginInfo);
        }
    }
}
