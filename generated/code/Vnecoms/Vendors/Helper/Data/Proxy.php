<?php
namespace Vnecoms\Vendors\Helper\Data;

/**
 * Proxy class for @see \Vnecoms\Vendors\Helper\Data
 */
class Proxy extends \Vnecoms\Vendors\Helper\Data implements \Magento\Framework\ObjectManager\NoninterceptableInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Proxied instance name
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Proxied instance
     *
     * @var \Vnecoms\Vendors\Helper\Data
     */
    protected $_subject = null;

    /**
     * Instance shareability flag
     *
     * @var bool
     */
    protected $_isShared = null;

    /**
     * Proxy constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     * @param bool $shared
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\Vnecoms\\Vendors\\Helper\\Data', $shared = true)
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
        $this->_isShared = $shared;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['_subject', '_isShared', '_instanceName'];
    }

    /**
     * Retrieve ObjectManager from global scope
     */
    public function __wakeup()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Clone proxied instance
     */
    public function __clone()
    {
        $this->_subject = clone $this->_getSubject();
    }

    /**
     * Get proxied instance
     *
     * @return \Vnecoms\Vendors\Helper\Data
     */
    protected function _getSubject()
    {
        if (!$this->_subject) {
            $this->_subject = true === $this->_isShared
                ? $this->_objectManager->get($this->_instanceName)
                : $this->_objectManager->create($this->_instanceName);
        }
        return $this->_subject;
    }

    /**
     * {@inheritdoc}
     */
    public function moduleEnabled()
    {
        return $this->_getSubject()->moduleEnabled();
    }

    /**
     * {@inheritdoc}
     */
    public function getPanelType()
    {
        return $this->_getSubject()->getPanelType();
    }

    /**
     * {@inheritdoc}
     */
    public function getSellerRegisterType()
    {
        return $this->_getSubject()->getSellerRegisterType();
    }

    /**
     * {@inheritdoc}
     */
    public function getSellerRegisterStaticBlock()
    {
        return $this->_getSubject()->getSellerRegisterStaticBlock();
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabledRegistrationAgreement()
    {
        return $this->_getSubject()->isEnabledRegistrationAgreement();
    }

    /**
     * {@inheritdoc}
     */
    public function getAgreementLabel()
    {
        return $this->_getSubject()->getAgreementLabel();
    }

    /**
     * {@inheritdoc}
     */
    public function getFooterText()
    {
        return $this->_getSubject()->getFooterText();
    }

    /**
     * {@inheritdoc}
     */
    public function getPageHelpUrl()
    {
        return $this->_getSubject()->getPageHelpUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function setPageHelpUrl($url = null)
    {
        return $this->_getSubject()->setPageHelpUrl($url);
    }

    /**
     * {@inheritdoc}
     */
    public function addPageHelpUrl($suffix)
    {
        return $this->_getSubject()->addPageHelpUrl($suffix);
    }

    /**
     * {@inheritdoc}
     */
    public function getUrl($route = '', $params = array())
    {
        return $this->_getSubject()->getUrl($route, $params);
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrentUserId()
    {
        return $this->_getSubject()->getCurrentUserId();
    }

    /**
     * {@inheritdoc}
     */
    public function prepareFilterString($filterString)
    {
        return $this->_getSubject()->prepareFilterString($filterString);
    }

    /**
     * {@inheritdoc}
     */
    public function generateResetPasswordLinkToken()
    {
        return $this->_getSubject()->generateResetPasswordLinkToken();
    }

    /**
     * {@inheritdoc}
     */
    public function getHomePageUrl()
    {
        return $this->_getSubject()->getHomePageUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function getAreaFrontName($checkHost = false)
    {
        return $this->_getSubject()->getAreaFrontName($checkHost);
    }

    /**
     * {@inheritdoc}
     */
    public function getNotSavedVendorAttributes()
    {
        return $this->_getSubject()->getNotSavedVendorAttributes();
    }

    /**
     * {@inheritdoc}
     */
    public function getModulesUseTemplateFromAdminhtml()
    {
        return $this->_getSubject()->getModulesUseTemplateFromAdminhtml();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlocksUseTemplateFromAdminhtml()
    {
        return $this->_getSubject()->getBlocksUseTemplateFromAdminhtml();
    }

    /**
     * {@inheritdoc}
     */
    public function getOpenModules()
    {
        return $this->_getSubject()->getOpenModules();
    }

    /**
     * {@inheritdoc}
     */
    public function isEnableVendorRegister()
    {
        return $this->_getSubject()->isEnableVendorRegister();
    }

    /**
     * {@inheritdoc}
     */
    public function isRequiredVendorApproval()
    {
        return $this->_getSubject()->isRequiredVendorApproval();
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultVendorGroup()
    {
        return $this->_getSubject()->getDefaultVendorGroup();
    }

    /**
     * {@inheritdoc}
     */
    public function showVendorShortDescription()
    {
        return $this->_getSubject()->showVendorShortDescription();
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptionMaxLength()
    {
        return $this->_getSubject()->getDescriptionMaxLength();
    }

    /**
     * {@inheritdoc}
     */
    public function showVendorPhoneNumber()
    {
        return $this->_getSubject()->showVendorPhoneNumber();
    }

    /**
     * {@inheritdoc}
     */
    public function showVendorOperationTime()
    {
        return $this->_getSubject()->showVendorOperationTime();
    }

    /**
     * {@inheritdoc}
     */
    public function getAddressTemplate()
    {
        return $this->_getSubject()->getAddressTemplate();
    }

    /**
     * {@inheritdoc}
     */
    public function getVendorStoreName($vendorId)
    {
        return $this->_getSubject()->getVendorStoreName($vendorId);
    }

    /**
     * {@inheritdoc}
     */
    public function getVendorStoreShortDescription($vendorId)
    {
        return $this->_getSubject()->getVendorStoreShortDescription($vendorId);
    }

    /**
     * {@inheritdoc}
     */
    public function getVendorPhone($vendorId)
    {
        return $this->_getSubject()->getVendorPhone($vendorId);
    }

    /**
     * {@inheritdoc}
     */
    public function getVendorOperationTime($vendorId)
    {
        return $this->_getSubject()->getVendorOperationTime($vendorId);
    }

    /**
     * {@inheritdoc}
     */
    public function getNotActiveVendorIds()
    {
        return $this->_getSubject()->getNotActiveVendorIds();
    }

    /**
     * {@inheritdoc}
     */
    public function isUsedCustomVendorUrl()
    {
        return $this->_getSubject()->isUsedCustomVendorUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        return $this->_getSubject()->isModuleOutputEnabled($moduleName);
    }
}
