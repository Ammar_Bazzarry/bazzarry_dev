<?php
namespace Vnecoms\VendorsShipping\Block\Checkout\Cart\TotalsProcessor;

/**
 * Interceptor class for @see \Vnecoms\VendorsShipping\Block\Checkout\Cart\TotalsProcessor
 */
class Interceptor extends \Vnecoms\VendorsShipping\Block\Checkout\Cart\TotalsProcessor implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Vnecoms\VendorsShipping\Helper\Data $helper, \Magento\Framework\UrlInterface $urlBuilder)
    {
        $this->___init();
        parent::__construct($helper, $urlBuilder);
    }

    /**
     * {@inheritdoc}
     */
    public function process($jsLayout)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'process');
        if (!$pluginInfo) {
            return parent::process($jsLayout);
        } else {
            return $this->___callPlugins('process', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUrl($route = '', $params = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getUrl');
        if (!$pluginInfo) {
            return parent::getUrl($route, $params);
        } else {
            return $this->___callPlugins('getUrl', func_get_args(), $pluginInfo);
        }
    }
}
