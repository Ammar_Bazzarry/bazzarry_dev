<?php
namespace Amasty\Oaction\Model\Source\State;

/**
 * Interceptor class for @see \Amasty\Oaction\Model\Source\State
 */
class Interceptor extends \Amasty\Oaction\Model\Source\State implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $statusCollectionFactory)
    {
        $this->___init();
        parent::__construct($statusCollectionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'toOptionArray');
        if (!$pluginInfo) {
            return parent::toOptionArray();
        } else {
            return $this->___callPlugins('toOptionArray', func_get_args(), $pluginInfo);
        }
    }
}
