<?php
namespace Amasty\OrderStatus\Model\Order\Email\Sender\Plugin\OrderCommentSender;

/**
 * Interceptor class for @see \Amasty\OrderStatus\Model\Order\Email\Sender\Plugin\OrderCommentSender
 */
class Interceptor extends \Amasty\OrderStatus\Model\Order\Email\Sender\Plugin\OrderCommentSender implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Sales\Model\Order\Email\Container\Template $templateContainer, \Magento\Sales\Model\Order\Email\Container\OrderCommentIdentity $identityContainer, \Magento\Sales\Model\Order\Email\SenderBuilderFactory $senderBuilderFactory, \Psr\Log\LoggerInterface $logger, \Magento\Sales\Model\Order\Address\Renderer $addressRenderer, \Magento\Framework\Event\ManagerInterface $eventManager, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\ObjectManagerInterface $objectManager, \Magento\Customer\Model\CustomerFactory $customerFactory, \Magento\Framework\Module\Manager $moduleManager, \Magento\Payment\Helper\Data $paymentHelper)
    {
        $this->___init();
        parent::__construct($templateContainer, $identityContainer, $senderBuilderFactory, $logger, $addressRenderer, $eventManager, $coreRegistry, $objectManager, $customerFactory, $moduleManager, $paymentHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function aroundSend($subject, $proceed, $order, $notify, $comment)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'aroundSend');
        if (!$pluginInfo) {
            return parent::aroundSend($subject, $proceed, $order, $notify, $comment);
        } else {
            return $this->___callPlugins('aroundSend', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function send(\Magento\Sales\Model\Order $order, $notify = true, $comment = '')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'send');
        if (!$pluginInfo) {
            return parent::send($order, $notify, $comment);
        } else {
            return $this->___callPlugins('send', func_get_args(), $pluginInfo);
        }
    }
}
