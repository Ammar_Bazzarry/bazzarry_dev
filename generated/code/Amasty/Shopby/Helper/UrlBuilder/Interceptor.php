<?php
namespace Amasty\Shopby\Helper\UrlBuilder;

/**
 * Interceptor class for @see \Amasty\Shopby\Helper\UrlBuilder
 */
class Interceptor extends \Amasty\Shopby\Helper\UrlBuilder implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Helper\Context $context, \Magento\Framework\Registry $registry, \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository, \Amasty\Shopby\Helper\FilterSetting $filterSettingHelper, \Magento\Framework\Url\QueryParamsResolverInterface $queryParamsResolver, \Amasty\Shopby\Helper\Category $categoryHelper, \Amasty\ShopbyBase\Api\UrlBuilderInterface $urlBuilder, \Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->___init();
        parent::__construct($context, $registry, $categoryRepository, $filterSettingHelper, $queryParamsResolver, $categoryHelper, $urlBuilder, $storeManager);
    }

    /**
     * {@inheritdoc}
     */
    public function buildUrl(\Magento\Catalog\Model\Layer\Filter\FilterInterface $filter, $optionValue)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'buildUrl');
        if (!$pluginInfo) {
            return parent::buildUrl($filter, $optionValue);
        } else {
            return $this->___callPlugins('buildUrl', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isSingleselectCategoryFilter($filter)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isSingleselectCategoryFilter');
        if (!$pluginInfo) {
            return parent::isSingleselectCategoryFilter($filter);
        } else {
            return $this->___callPlugins('isSingleselectCategoryFilter', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUrl($route, $params = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getUrl');
        if (!$pluginInfo) {
            return parent::getUrl($route, $params);
        } else {
            return $this->___callPlugins('getUrl', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildQuery(\Magento\Catalog\Model\Layer\Filter\FilterInterface $filter, $resultValue)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'buildQuery');
        if (!$pluginInfo) {
            return parent::buildQuery($filter, $resultValue);
        } else {
            return $this->___callPlugins('buildQuery', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModuleOutputEnabled');
        if (!$pluginInfo) {
            return parent::isModuleOutputEnabled($moduleName);
        } else {
            return $this->___callPlugins('isModuleOutputEnabled', func_get_args(), $pluginInfo);
        }
    }
}
