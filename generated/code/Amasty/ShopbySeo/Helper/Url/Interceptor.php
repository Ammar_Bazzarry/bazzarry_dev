<?php
namespace Amasty\ShopbySeo\Helper\Url;

/**
 * Interceptor class for @see \Amasty\ShopbySeo\Helper\Url
 */
class Interceptor extends \Amasty\ShopbySeo\Helper\Url implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Helper\Context $context, \Amasty\ShopbySeo\Helper\Data $helper, \Magento\Framework\Registry $coreRegistry, \Magento\Store\Model\StoreManagerInterface $storeManager, \Amasty\ShopbySeo\Helper\UrlParser $urlParser, \Amasty\ShopbySeo\Helper\Config $config)
    {
        $this->___init();
        parent::__construct($context, $helper, $coreRegistry, $storeManager, $urlParser, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRequest');
        if (!$pluginInfo) {
            return parent::getRequest();
        } else {
            return $this->___callPlugins('getRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function seofyUrl($url)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'seofyUrl');
        if (!$pluginInfo) {
            return parent::seofyUrl($url);
        } else {
            return $this->___callPlugins('seofyUrl', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function modifySeoIdentifier($identifier)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'modifySeoIdentifier');
        if (!$pluginInfo) {
            return parent::modifySeoIdentifier($identifier);
        } else {
            return $this->___callPlugins('modifySeoIdentifier', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function modifySeoIdentifierByAlias($identifier, $aliases = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'modifySeoIdentifierByAlias');
        if (!$pluginInfo) {
            return parent::modifySeoIdentifierByAlias($identifier, $aliases);
        } else {
            return $this->___callPlugins('modifySeoIdentifierByAlias', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasCategoryFilterParam()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'hasCategoryFilterParam');
        if (!$pluginInfo) {
            return parent::hasCategoryFilterParam();
        } else {
            return $this->___callPlugins('hasCategoryFilterParam', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function parseQuery($query)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'parseQuery');
        if (!$pluginInfo) {
            return parent::parseQuery($query);
        } else {
            return $this->___callPlugins('parseQuery', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeCategorySuffix($url)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'removeCategorySuffix');
        if (!$pluginInfo) {
            return parent::removeCategorySuffix($url);
        } else {
            return $this->___callPlugins('removeCategorySuffix', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isSeoUrlEnabled()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isSeoUrlEnabled');
        if (!$pluginInfo) {
            return parent::isSeoUrlEnabled();
        } else {
            return $this->___callPlugins('isSeoUrlEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAddSuffixSettingValue()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAddSuffixSettingValue');
        if (!$pluginInfo) {
            return parent::getAddSuffixSettingValue();
        } else {
            return $this->___callPlugins('getAddSuffixSettingValue', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isAddSuffixToShopby()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isAddSuffixToShopby');
        if (!$pluginInfo) {
            return parent::isAddSuffixToShopby();
        } else {
            return $this->___callPlugins('isAddSuffixToShopby', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSeoSuffix()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSeoSuffix');
        if (!$pluginInfo) {
            return parent::getSeoSuffix();
        } else {
            return $this->___callPlugins('getSeoSuffix', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getParam($paramName)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getParam');
        if (!$pluginInfo) {
            return parent::getParam($paramName);
        } else {
            return $this->___callPlugins('getParam', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getParams()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getParams');
        if (!$pluginInfo) {
            return parent::getParams();
        } else {
            return $this->___callPlugins('getParams', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasParams()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'hasParams');
        if (!$pluginInfo) {
            return parent::hasParams();
        } else {
            return $this->___callPlugins('hasParams', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setParam($paramName, $paramValue = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setParam');
        if (!$pluginInfo) {
            return parent::setParam($paramName, $paramValue);
        } else {
            return $this->___callPlugins('setParam', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModuleOutputEnabled');
        if (!$pluginInfo) {
            return parent::isModuleOutputEnabled($moduleName);
        } else {
            return $this->___callPlugins('isModuleOutputEnabled', func_get_args(), $pluginInfo);
        }
    }
}
