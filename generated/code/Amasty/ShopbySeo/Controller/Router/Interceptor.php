<?php
namespace Amasty\ShopbySeo\Controller\Router;

/**
 * Interceptor class for @see \Amasty\ShopbySeo\Controller\Router
 */
class Interceptor extends \Amasty\ShopbySeo\Controller\Router implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Amasty\ShopbySeo\Helper\UrlParser $urlParser, \Amasty\ShopbySeo\Helper\Url $urlHelper, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Amasty\ShopbySeo\Helper\Data $helper)
    {
        $this->___init();
        parent::__construct($urlParser, $urlHelper, $scopeConfig, $helper);
    }

    /**
     * {@inheritdoc}
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'match');
        if (!$pluginInfo) {
            return parent::match($request);
        } else {
            return $this->___callPlugins('match', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function modifyRequest(\Magento\Framework\App\RequestInterface $request, $identifier, $params = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'modifyRequest');
        if (!$pluginInfo) {
            return parent::modifyRequest($request, $identifier, $params);
        } else {
            return $this->___callPlugins('modifyRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function skipRequest(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'skipRequest');
        if (!$pluginInfo) {
            return parent::skipRequest($request);
        } else {
            return $this->___callPlugins('skipRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSeoSuffix()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSeoSuffix');
        if (!$pluginInfo) {
            return parent::getSeoSuffix();
        } else {
            return $this->___callPlugins('getSeoSuffix', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function initRequestMetaData(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'initRequestMetaData');
        if (!$pluginInfo) {
            return parent::initRequestMetaData($request);
        } else {
            return $this->___callPlugins('initRequestMetaData', func_get_args(), $pluginInfo);
        }
    }
}
