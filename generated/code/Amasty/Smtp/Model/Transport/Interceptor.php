<?php
namespace Amasty\Smtp\Model\Transport;

/**
 * Interceptor class for @see \Amasty\Smtp\Model\Transport
 */
class Interceptor extends \Amasty\Smtp\Model\Transport implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Mail\MessageInterface $message, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Amasty\Smtp\Model\Logger\MessageLogger $messageLogger, \Amasty\Smtp\Model\Logger\DebugLogger $debugLogger, \Amasty\Smtp\Helper\Data $helper, \Magento\Framework\ObjectManagerInterface $objectManager, \Amasty\Smtp\Model\Config $config, $host = '127.0.0.1', array $parameters = array())
    {
        $this->___init();
        parent::__construct($message, $scopeConfig, $messageLogger, $debugLogger, $helper, $objectManager, $config, $host, $parameters);
    }

    /**
     * {@inheritdoc}
     */
    public function sendMessage()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'sendMessage');
        if (!$pluginInfo) {
            return parent::sendMessage();
        } else {
            return $this->___callPlugins('sendMessage', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function runTest($testEmail, $storeId, $scope = 'default')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'runTest');
        if (!$pluginInfo) {
            return parent::runTest($testEmail, $storeId, $scope);
        } else {
            return $this->___callPlugins('runTest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMessage');
        if (!$pluginInfo) {
            return parent::getMessage();
        } else {
            return $this->___callPlugins('getMessage', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setConnection(\Zend_Mail_Protocol_Abstract $connection)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setConnection');
        if (!$pluginInfo) {
            return parent::setConnection($connection);
        } else {
            return $this->___callPlugins('setConnection', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getConnection()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getConnection');
        if (!$pluginInfo) {
            return parent::getConnection();
        } else {
            return $this->___callPlugins('getConnection', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function _sendMail()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, '_sendMail');
        if (!$pluginInfo) {
            return parent::_sendMail();
        } else {
            return $this->___callPlugins('_sendMail', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function send(\Zend_Mail $mail)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'send');
        if (!$pluginInfo) {
            return parent::send($mail);
        } else {
            return $this->___callPlugins('send', func_get_args(), $pluginInfo);
        }
    }
}
