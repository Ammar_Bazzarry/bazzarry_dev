<?php
namespace Vnecoms\VendorsFeaturedProduct\Model;

class Product extends \Magento\Framework\Model\AbstractModel
{

    const ENTITY = 'vendor_featured_product';

    /**
     * Model event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'vendor_featured_product';
    
    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'vendor_featured_product';

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;
    
    /**
     * Initialize customer model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Vnecoms\VendorsFeaturedProduct\Model\ResourceModel\Product');
    }
    
    /**
     * Get Product Object
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        if (!$this->_product) {
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            $this->_product = $om->create('Magento\Catalog\Model\Product');
            $this->_product->load($this->getProductId());
        }
        
        return $this->_product;
    }
    
    /**
     * Get Product Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->getProduct()->getName();
    }
    
    /**
     * Get product sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->getProduct()->getSku();
    }
}
