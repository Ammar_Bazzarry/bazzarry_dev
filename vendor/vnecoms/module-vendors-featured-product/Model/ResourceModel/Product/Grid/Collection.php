<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsFeaturedProduct\Model\ResourceModel\Product\Grid;

use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Psr\Log\LoggerInterface as Logger;

/**
 * App page collection
 */
class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager
    ) {
        $mainTable = 'ves_vendor_message';
        $resourceModel = 'Vnecoms\VendorsFeaturedProduct\Model\ResourceModel\Product';
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }
    
    protected function _construct()
    {
        parent::_construct();

        $this->addFilterToMap(
            'entity_id',
            'main_table.entity_id'
        );
        $this->addFilterToMap(
            'vendor_id',
            'main_table.vendor_id'
        );
    }
    
    /**
     * Init collection select
     *
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->joinLeft(
            ['product_entity' => $this->getTable('catalog_product_entity')],
            'main_table.product_id = product_entity.entity_id',
            ['*']
        );
        $this->getSelect()->group('msg_detail.message_id');
        $this->_joinedTables['msg_detail'] = true;
    }
}
