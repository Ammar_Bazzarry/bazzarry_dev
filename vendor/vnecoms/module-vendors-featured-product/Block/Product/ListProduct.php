<?php
namespace Vnecoms\VendorsFeaturedProduct\Block\Product;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Vnecoms\VendorsProduct\Model\Source\Approval as ProductApproval;
use Vnecoms\VendorsFeaturedProduct\Model\ResourceModel\Product\CollectionFactory as FeaturedCollectionFactory;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;
    
    /**
     * Catalog product visibility
     *
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $productVisibility;
    
    /**
     * Catalog config
     *
     * @var \Magento\Catalog\Model\Config
     */
    protected $catalogConfig;
    
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;
    
    /**
     * @var \Vnecoms\VendorsFeaturedProduct\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_featuredProductsCollectionFactory;
    
    /**
     *
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $productVisibility
     * @param FeaturedCollectionFactory $featuredProductCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        FeaturedCollectionFactory $featuredProductCollectionFactory,
        array $data = []
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->productVisibility = $productVisibility;
        $this->catalogConfig = $context->getCatalogConfig();
        $this->_coreRegistry = $context->getRegistry();
        $this->_featuredProductsCollectionFactory = $featuredProductCollectionFactory;
        
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);
    }
    
    /**
     * Get current vendor
     *
     * @return \Vnecoms\Vendors\Model\Vendor
     */
    public function getVendor()
    {
        return $this->_coreRegistry->registry('vendor');
    }
    
    /**
     * Retrieve loaded category collection
     *
     * @return AbstractCollection
     */
    protected function _getProductCollection()
    {
        if ($this->_productCollection === null) {
            $featuredCollection = $this->_featuredProductsCollectionFactory->create();
            $featuredCollection->addFieldToFilter('vendor_id', $this->getVendor()->getId());
            $today = $this->_localeDate->date()->format('Y-m-d');     
            
            $productIds = $featuredCollection->getColumnValues('product_id');
            $this->_productCollection = $this->_productCollectionFactory->create();
            $this->_productCollection->addAttributeToSelect($this->catalogConfig->getProductAttributes())
                ->addAttributeToFilter('vendor_id', $this->getVendor()->getId())
                ->addAttributeToFilter('approval', ProductApproval::STATUS_APPROVED)
                ->addAttributeToFilter('entity_id', ['in' => $productIds])
                
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->setVisibility($this->productVisibility->getVisibleInCatalogIds());
            
            $this->_productCollection->joinTable(
                ['featured_product' => $this->_productCollection->getTable('ves_vendor_featured_product')],
                "product_id=entity_id",
                ['featured_from', 'featured_to','feature_id' => 'entity_id','featured_order' => 'sort_order'],
                'featured_product.vendor_id = e.vendor_id',
                'right'
            );
            $this->_productCollection->getSelect()
                ->where('featured_product.featured_from is null OR featured_product.featured_from <= "'.$today.'"')
                ->where('featured_product.featured_to is null OR featured_product.featured_to >= "'.$today.'"')
                ->order('featured_product.sort_order ASC');
        }
        return $this->_productCollection;
    }
    
    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml()
    {
        return '';
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return ['featured_products_' . $this->getVendor()->getId()];
    }
    
    protected function _toHtml()
    {
        if (!$this->_getProductCollection()->count()) {
            return '';
        }
        return parent::_toHtml();
    }
}
