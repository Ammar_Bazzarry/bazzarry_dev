<?php
namespace Vnecoms\VendorsFeaturedProduct\Block;

/**
 * Class View
 * @package Magento\Catalog\Block\Category
 */
class Product extends \Magento\Framework\View\Element\Template implements \Magento\Framework\DataObject\IdentityInterface
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    
    /**
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    /**
     * @return string
     */
    public function getProductListHtml()
    {
        return $this->getChildHtml('product_list');
    }
    
    /**
     * Get current vendor
     *
     * @return \Vnecoms\Vendors\Model\Vendor
     */
    public function getVendor()
    {
        return $this->_coreRegistry->registry('vendor');
    }
    
    public function getIdentities()
    {
        return ['catalog_product','vnecoms_vendors_featured_product_list-'.$this->getVendor()->getId()];
    }
    
    public function getCacheKeyInfo()
    {
        return ['vnecoms_vendors_featured_products_'.$this->getVendor()->getId()];
    }
    
    /**
     * Get block cache life time
     *
     * @return int
     */
    protected function getCacheLifetime()
    {
        return 3600;
    }
}
