<?php
/**
 * Catalog price rules
 *
 * @author      Vnecoms Team <core@vnecoms.com>
 */
namespace Vnecoms\VendorsFeaturedProduct\Block\Vendors;

class Product extends \Vnecoms\Vendors\Block\Vendors\Widget\Container
{
    
    /**
     * @var \Magento\Catalog\Model\Product\TypeFactory
     */
    protected $_typeFactory;
    
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;
    
    /**
     * @var \Vnecoms\VendorsProduct\Helper\Data
     */
    protected $_productHelper;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Vnecoms_VendorsFeaturedProduct';
        $this->_controller = 'vendors_product';
        $this->_headerText = __('Manage Featured Products');
        $this->_addButtonLabel = __('Add Featured Product');
        
        parent::_construct();
        //$this->removeButton('add');
    }
    
    
    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Catalog\Model\Product\TypeFactory $typeFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Catalog\Model\Product\TypeFactory $typeFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Vnecoms\VendorsProduct\Helper\Data $productHelper,
        array $data = []
    ) {
        $this->_productFactory  = $productFactory;
        $this->_typeFactory     = $typeFactory;
        $this->_productHelper   = $productHelper;
        
        parent::__construct($context, $data);
        $this->removeButton('add');
    }
    
    /**
     * Prepare button and grid
     *
     * @return \Magento\Catalog\Block\Adminhtml\Product
     */
    protected function _prepareLayout()
    {
        $addButtonProps = [
            'id' => 'add_new_product',
            'label' => __('Add Featured Product'),
            'class' => 'btn-primary btn-lg',
            'onclick' => "setLocation('".$this->getProductCreateUrl()."')",
            'button_class' => '',
        ];
        $this->buttonList->add('add_new', $addButtonProps, 0, 0, 'toolbar');
    
        return parent::_prepareLayout();
    }

    /**
     * Retrieve product create url by specified product type
     *
     * @param string $type
     * @return string
     */
    public function getProductCreateUrl()
    {
        return $this->getUrl(
            'catalog/product_featured/new'
        );
    }
}
