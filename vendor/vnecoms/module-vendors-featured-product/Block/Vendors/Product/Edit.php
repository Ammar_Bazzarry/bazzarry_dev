<?php

namespace Vnecoms\VendorsFeaturedProduct\Block\Vendors\Product;

class Edit extends \Vnecoms\Vendors\Block\Vendors\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Vnecoms_VendorsFeaturedProduct';
        $this->_controller = 'vendors_product';
        parent::_construct();
    }

    /**
     * Getter
     *
     * @return \Vnecoms\VendorsFeaturedProduct\Model\Product
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('current_product');
    }

    /**
     * Prepare layout.
     * Adding save_and_continue button
     *
     * @return $this
     */
    protected function _preparelayout()
    {
//         if ($this->getWidgetInstance()->isCompleteToCreate()) {
//             $this->buttonList->add(
//                 'save_and_edit_button',
//                 [
//                     'label' => __('Save and Continue Edit'),
//                     'class' => 'save',
//                     'data_attribute' => [
//                         'mage-init' => [
//                             'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
//                         ],
//                     ]
//                 ],
//                 100
//             );
//         } else {
//             $this->removeButton('save');
//         }
        return parent::_prepareLayout();
    }

    /**
     * Return translated header text depending on creating/editing action
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->getWidgetInstance()->getId()) {
            return __('Product "%1"', $this->escapeHtml($this->getProduct()->getName()));
        } else {
            return __('New Featured Product');
        }
    }


    /**
     * Return save url for edit form
     *
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getUrl('*/*/save', ['_current' => true, 'back' => null]);
    }
}
