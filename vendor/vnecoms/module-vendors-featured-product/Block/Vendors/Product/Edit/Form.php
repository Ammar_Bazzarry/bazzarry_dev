<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Widget Instance edit form
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
namespace Vnecoms\VendorsFeaturedProduct\Block\Vendors\Product\Edit;

class Form extends \Vnecoms\Vendors\Block\Vendors\Widget\Form\Generic
{
    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $form->setUseContainer(true);

        $fieldset = $form->addFieldset(
            'options_fieldset',
            ['legend' => __('Featured Product Information'), 'class' => 'fieldset-wide fieldset-widget-options']
        );
        
        $chooserField = $fieldset->addField(
            'options_fieldset_entity_id',
            'label',
            [
                'name' => 'product_id',
                'label' => __('Product'),
                'required' => true,
                'class' => 'widget-option',
                'note' => __("Select a product"),
            ]
        );
        /*Add chooser helper for the field*/
        $helperData  = [
            'data' => [
                'button' => ['open' => __("Select Product...")]
            ]
        ];
        $chooserField->setValue($this->getProduct()->getProductId())->setFeaturedProduct($this->getProduct());
        $helperBlock = $this->getLayout()->createBlock(
            'Vnecoms\VendorsFeaturedProduct\Block\Vendors\Product\Chooser',
            '',
            $helperData
        );
        
        $helperBlock->setConfig($helperData)
            ->setFieldsetId($fieldset->getId())
            ->prepareElementHtml($chooserField)
            ->setValue($this->getProduct()->getId());

        $dateFormat = "M/d/Y";
        $timeFormat = $this->_localeDate->getTimeFormat(
            \IntlDateFormatter::MEDIUM
        );


        
        $fieldset->addField(
            'featured_from',
            'date',
            [
                'name' => 'featured_from',
                'label' => __('Is Featured From Date'),
                'date_format' => $dateFormat,
            ]
        );
        
        $fieldset->addField(
            'featured_to',
            'date',
            [
                'name' => 'featured_to',
                'label' => __('Is Featured To Date'),
                'date_format' => $dateFormat,
            ]
        );
        
        $fieldset->addField(
            'sort_order',
            'text',
            [
                'name' => 'sort_order',
                'label' => __('Sort ORder'),
                'class' => 'validate-number',
            ]
        );
        
        // add dependence javascript block
        $dependenceBlock = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Form\Element\Dependence');
        $this->setChild('form_after', $dependenceBlock);
        $dependenceBlock->addFieldMap($chooserField->getId(), 'product_id');
        
        $data = $this->getProduct()->getData();
        /* $data['options_fieldset_product_id'] = $this->getProduct()->getName(); */
        $form->setValues($data);
        
        
        $this->setForm($form);
        return parent::_prepareForm();
    }
    
    /**
     * Get current featured product
     *
     * @return \Vnecoms\VendorsFeaturedProduct\Model\Product
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('current_product');
    }
}

