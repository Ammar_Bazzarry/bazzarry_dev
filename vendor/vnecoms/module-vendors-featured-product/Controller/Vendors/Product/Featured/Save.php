<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsFeaturedProduct\Controller\Vendors\Product\Featured;

class Save extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    /**
     * Default customer account page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $product = $this->_objectManager->create('Vnecoms\VendorsFeaturedProduct\Model\Product');
        $product->load($id);
        
        if ($id && (!$product->getId() || $product->getVendorId() != $this->_session->getVendor()->getId())
        ) {
            $this->messageManager->addError(__("This product does not exist."));
            return $this->_redirect('*/*');
        }
        
        $newProductId = $this->getRequest()->getParam('product_id');
        $collection = $product->getCollection()
            ->addFieldToFilter('vendor_id', $this->_session->getVendor()->getId())
            ->addFieldToFilter('product_id', $newProductId);
        
        if ($product->getId()) {
            $collection->addFieldToFilter('entity_id', ['neq' => $product->getId()]);
        }
        
        if ($collection->count()) {
            $this->messageManager->addError(__("The product #%1 is already featured.", $newProductId));
            $this->_session->setFeaturedProductFormData($this->getRequest()->getParams());
            return $this->_redirect('*/*/edit', ['id' => $id]);
        }
        
        $product->setData($this->getRequest()->getParams())
            ->setId($id)
            ->setVendorId($this->_session->getVendor()->getId())
            ->save();
        
        $this->messageManager->addSuccess(__("The featured product has been saved."));
        return $this->_redirect('*/*');
    }
}
