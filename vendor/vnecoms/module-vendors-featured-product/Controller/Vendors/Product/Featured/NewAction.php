<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsFeaturedProduct\Controller\Vendors\Product\Featured;

class NewAction extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    /**
     * Default customer account page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}
