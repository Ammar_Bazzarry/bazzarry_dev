<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsFeaturedProduct\Controller\Vendors\Product\Featured;

use Magento\Framework\Controller\ResultFactory;
use Vnecoms\VendorsFeaturedProduct\Model\ResourceModel\Product\CollectionFactory;

class Delete extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    /**
     * Default customer account page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $product = $this->_objectManager->create('Vnecoms\VendorsFeaturedProduct\Model\Product');
        $product->load($this->getRequest()->getParam('id'));
        
        if (!$product->getId() || $product->getVendorId() != $this->_session->getVendor()->getId()) {
            $this->messageManager->addError(
                __('The product #%1 does not exist.', $this->getRequest()->getParam('id'))
            );
        } else {
            $this->messageManager->addSuccess(
                __("The product '%1' has been deleted.", $product->getName())
            );
            $product->delete();
        }
        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }
}
