<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsFeaturedProduct\Controller\Vendors\Product\Featured;

use Magento\Framework\Controller\ResultFactory;
use Vnecoms\VendorsFeaturedProduct\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Ui\Component\MassAction\Filter;

class massDelete extends \Vnecoms\Vendors\Controller\Vendors\Action
{

    /**
     * Massactions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var FeaturedCollectionFactory
     */
    protected $featuredCollectionFactory;

    /**
     *
     * @param \Vnecoms\Vendors\App\Action\Context $context
     * @param \Vnecoms\Vendors\App\ConfigInterface $config
     * @param Registry $coreRegistry
     * @param Date $dateFilter
     * @param Filter $filter
     * @param ProductCollectionFactory $collectionFactory
     */
    public function __construct(
        \Vnecoms\Vendors\App\Action\Context $context,
        Filter $filter,
        ProductCollectionFactory $collectionFactory,
        CollectionFactory $feturedCollectionFactory
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->featuredCollectionFactory = $feturedCollectionFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());

        $productDeleted = 0;
        foreach ($collection as $product) {
            $featureds = $this->featuredCollectionFactory->create()->addFieldToFilter("product_id",$product->getId());
            foreach ($featureds as $featured){
                $featured->delete();
            }
            $productDeleted++;
        }

        $this->messageManager->addSuccess(
            __('A total of %1 record(s) have been deleted.', $productDeleted)
        );

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }
}
