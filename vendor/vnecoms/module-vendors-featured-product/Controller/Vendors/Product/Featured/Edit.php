<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsFeaturedProduct\Controller\Vendors\Product\Featured;

class Edit extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    /**
     * Default customer account page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $product = $this->_objectManager->create('Vnecoms\VendorsFeaturedProduct\Model\Product');
        $product->load($id);
        
        if ($id && (!$product->getId() || $product->getVendorId() != $this->_session->getVendor()->getId())
        ) {
            $this->messageManager->addError(__("This product does not exist."));
            return $this->_redirect('*/*');
        }
        if ($data = $this->_session->getFeaturedProductFormData(true)) {
            $product->setData($data);
        }
        $this->_coreRegistry->register('current_product', $product);
        $this->_coreRegistry->register('product', $product);
        
        $this->_initAction();
        $title = $this->_view->getPage()->getConfig()->getTitle();
        $title->prepend(__("Catalog"));
        $title->prepend(__("Featured Products"));
        $this->_addBreadcrumb(__("Catalog"), __("Catalog"))->_addBreadcrumb(__("Featured Products"), __("Featured Products"));
        
        if ($product->getId()) {
            $title->prepend($product->getName());
            $this->_addBreadcrumb($product->getName(), $product->getName());
        } else {
            $this->_addBreadcrumb(__("New"), __("New"));
            $title->prepend(__("New Featured Products"));
        }
        
        $this->_view->renderLayout();
    }
}
