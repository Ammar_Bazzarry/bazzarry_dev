<?php
namespace Vnecoms\VendorsFeaturedProduct\Ui\Component\Listing\Columns;

class ProductActions extends \Magento\Catalog\Ui\Component\Listing\Columns\ProductActions
{
    /**
     * @param array $dSource
     * @return array
     */
    public function prepareDataSource(array $dSource)
    {
        if (isset($dSource['data']['items'])) {
            foreach ($dSource['data']['items'] as &$item) {
                $item[$this->getData('name')]['edit'] = [
                    'href' => $this->urlBuilder->getUrl(
                        'catalog/product_featured/edit',
                        ['id' => $item['feature_id']]
                    ),
                    'label' => __('Edit'),
                    'hidden' => false,
                ];
            }
        }

        return $dSource;
    }
}
