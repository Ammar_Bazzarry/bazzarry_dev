<?php
/**
 * Copyright © 2017 Vnecoms. All rights reserved.
 */

namespace Vnecoms\VendorsCms\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements \Magento\Framework\Setup\UpgradeDataInterface
{
    protected $connection;

    protected $collection;

    public function __construct
    (
        \Magento\Framework\App\ResourceConnection $connection,
        \Vnecoms\VendorsConfig\Model\ResourceModel\Config\Collection $collection
    )
    {
        $this->connection = $connection;
        $this->collection = $collection;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '2.0.1.4') < 0) {
            /**
             * change config path vendor cms from "cms/vendor_configs" to "page/cms"
             */
            $where = ['path like ?' => 'cms/vendor_configs%'];
            $this->connection->getConnection()->update($this->collection->getMainTable(), ['path' => new \Zend_Db_Expr('REPLACE(path, "cms/vendor_configs","page/cms")')], $where);
        }
    }
}
