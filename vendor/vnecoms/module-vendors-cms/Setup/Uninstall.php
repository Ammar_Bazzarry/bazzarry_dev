<?php
/**
 * Copyright © 2016 Ubertheme.com All rights reserved.
 */

namespace Vnecoms\VendorsCms\Setup;

use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class Uninstall implements UninstallInterface
{
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        //uninstall code, drop related tables
        $installer->getConnection()->dropTable($installer->getTable('vnecoms_vendor_cms_block'));
        $installer->getConnection()->dropTable($installer->getTable('vnecoms_vendor_cms_page'));

        $installer->endSetup();
    }
}
