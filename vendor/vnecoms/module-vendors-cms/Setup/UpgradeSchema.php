<?php

namespace Vnecoms\VendorsCms\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
    public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '2.0.0.1') < 0) {
            /*
            * Create table vnecoms_vendor_cms_app
            */
            $table = $setup->getConnection()->newTable(
                $setup->getTable('vnecoms_vendor_cms_app')
            )->addColumn(
                'app_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'App ID'
            )->addColumn(
                'vendor_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [],
                'Vendor Id'
            )->addColumn(
                'type',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Type'
            )->addColumn(
                'title',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'App Title'
            )->addColumn(
                'is_active',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'default' => '1'],
                'Is App Active'
            )->addColumn(
                'sort_order',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => true, 'default' => '1'],
                'Sort Order'
            )->addIndex(
                $setup->getIdxName(
                    $setup->getTable('vnecoms_vendor_cms_app'),
                    ['title', 'type'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['title', 'type'],
                ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
            )
//            ->addForeignKey(
//                $setup->getFkName('ves_vendor_entity', 'entity_id', 'vnecoms_vendor_cms_app', 'vendor_id'),
//                'entity_id',
//                $setup->getTable('vnecoms_vendor_cms_app'),
//                'vendor_id',
//                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//            )
                ->setComment(
                    'Vendor CMS App Table'
                );
            $setup->getConnection()->createTable($table);
            /*
             * End create table vnecoms_vendor_cms_app
             */
        }
        if (version_compare($context->getVersion(), '2.0.0.2') < 0) {
            $setup->getConnection()->addColumn(
                $setup->getTable('vnecoms_vendor_cms_app'),
                'instance_type',
                'VARCHAR(255) NOT NULL AFTER type'
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('vnecoms_vendor_cms_app'),
                'parameters',
                'TEXT NULL'
            );

            //set up app option table
            /*
            * Create table vnecoms_vendor_cms_app_option
            */
            $table = $setup->getConnection()->newTable(
                $setup->getTable('vnecoms_vendor_cms_app_option')
            )->addColumn(
                'option_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'identity' => true, 'nullable' => false, 'primary' => true],
                'Option ID'
            )->addColumn(
                'app_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [],
                'App Id'
            )->addColumn(
                'page_group',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                25,
                [],
                'Block Group Type'
            )->addColumn(
                'layout_handle',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Layout Handle'
            )->addColumn(
                'block_reference',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Container'
            )->addColumn(
                'page_for',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                25,
                [],
                'For instance entities'
            )->addColumn(
                'entities',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '64k',
                [],
                'Catalog entities (comma separated)'
            )->addColumn(
                'template',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Path to template'
            )->addIndex(
                $setup->getIdxName('vnecoms_vendor_cms_app_option', 'app_id'),
                'app_id'
            )->addForeignKey(
                $setup->getFkName('vnecoms_vendor_cms_app_option', 'app_id', 'vnecoms_vendor_cms_app', 'app_id'),
                'app_id',
                $setup->getTable('vnecoms_vendor_cms_app'),
                'app_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->setComment(
                'Vendor CMS App Option Table'
            );
            $setup->getConnection()->createTable($table);
            /*
             * End create table vnecoms_vendor_cms_app_option
             */
        }
        if (version_compare($context->getVersion(), '2.0.0.3') < 0) {
            /*
             * Create table 'vnecoms_vendor_cms_app_option_layout'
             */
            $table = $setup->getConnection()->newTable(
                $setup->getTable('vnecoms_vendor_cms_app_option_layout')
            )->addColumn(
                'option_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Option ID'
            )->addColumn(
                'layout_update_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Layout Update Id'
            )->addIndex(
                $setup->getIdxName('vnecoms_vendor_cms_app_option_layout', 'option_id'),
                'option_id'
            )->addIndex(
                $setup->getIdxName(
                    'vnecoms_vendor_cms_app_option_layout',
                    ['layout_update_id', 'option_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['layout_update_id', 'option_id'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )/*->addForeignKey(
                    $setup->getFkName(
                        'vnecoms_vendor_cms_app_option_layout', 'option_id', 'vnecoms_vendor_cms_app_option', 'option_id'
                    ),
                    'option_id',
                    $setup->getTable('vnecoms_vendor_cms_app_option'),
                    'option_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )*/->addForeignKey(
                $setup->getFkName(
                    'vnecoms_vendor_cms_app_option_layout',
                    'layout_update_id',
                    'vnecoms_vendor_cms_layout_update',
                    'layout_update_id'
                ),
                'layout_update_id',
                $setup->getTable('vnecoms_vendor_cms_layout_update'),
                'layout_update_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->setComment(
                'Layout updates'
            );

            $setup->getConnection()->createTable($table);

            $setup->getConnection()->addForeignKey(
                $setup->getFkName(
                    'vnecoms_vendor_cms_app_option_layout',
                    'option_id',
                    'vnecoms_vendor_cms_app_option',
                    'option_id'
                ),
                $setup->getTable('vnecoms_vendor_cms_app_option_layout'),
                'option_id',
                $setup->getTable('vnecoms_vendor_cms_app_option'),
                'option_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );

            /*
             * Create table 'vnecoms_vendor_cms_layout_update'
             */
            $table = $setup->getConnection()->newTable(
                $setup->getTable('vnecoms_vendor_cms_layout_update')
            )->addColumn(
                'layout_update_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Layout Update Id'
            )->addColumn(
                'handle',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Handle'
            )->addColumn(
                'xml',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '64k',
                [],
                'Xml'
            )->addColumn(
                'sort_order',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'default' => '0'],
                'Sort Order'
            )->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => true, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_UPDATE],
                'Last Update Timestamp'
            )->addIndex(
                $setup->getIdxName('vnecoms_vendor_cms_layout_update', ['handle']),
                ['handle']
            )->setComment(
                'Layout Updates'
            );
            $setup->getConnection()->createTable($table);
        }
        if (version_compare($context->getVersion(), '2.0.0.4') < 0) {
            $setup->getConnection()->addColumn(
                $setup->getTable('vnecoms_vendor_cms_layout_update'),
                'vendor_id',
                'INT NOT NULL AFTER handle'
            );
        }
    }
}
