<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsCms\Block\Html;

/**
 * Class Header
 * @package Vnecoms\VendorsCms\Block\Html
 */
class Header extends \Magento\Theme\Block\Html\Header
{
}
