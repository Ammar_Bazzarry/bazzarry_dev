<?php

namespace Vnecoms\VendorsCms\Block\Vendors\Block\Edit;

/**
 * Class Form Grid.
 *
 * @category Vnecoms
 * @module   VendorsCms
 *
 * @author   Vnecoms Developer Team
 */
class Form extends \Vnecoms\Vendors\Block\Vendors\Widget\Form\Generic
{
    /** @var \Vnecoms\Vendors\Model\Session  */
    protected $vendorSession;

    /** @var \Vnecoms\VendorsCms\Model\BlockFactory  */
    protected $blockFactory;

    /**
     * Form constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry             $registry
     * @param \Magento\Framework\Data\FormFactory     $formFactory
     * @param \Vnecoms\Vendors\Model\Session          $vendorSession
     * @param \Vnecoms\VendorsCms\Model\BlockFactory  $blockFactory
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Vnecoms\Vendors\Model\Session $vendorSession,
        \Vnecoms\VendorsCms\Model\BlockFactory $blockFactory,
        array $data
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->vendorSession = $vendorSession;
        $this->blockFactory = $blockFactory;
    }

    /**
     * Prepar form edit.
     *
     * @return $this
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        // id common for all form
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data',
                ],
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _prepareLayout()
    {
        $this->setChild(
            'tabs',
            $this->getLayout()->createBlock('Vnecoms\VendorsCms\Block\Vendors\Block\Tabs', 'tabs')
        );

        return parent::_prepareLayout(); // TODO: Change the autogenerated stub
    }

    /**
     * @return string
     */
    public function getTabsHtml()
    {
        return $this->getChildHtml('tabs');
    }
}
