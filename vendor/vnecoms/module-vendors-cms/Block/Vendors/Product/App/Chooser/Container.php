<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsCms\Block\Vendors\Product\App\Chooser;

class Container extends \Magento\Catalog\Block\Adminhtml\Product\Widget\Chooser\Container
{
    /**
     * @var string
     */
    protected $_template = 'Vnecoms_VendorsCms::app/product/chooser/container.phtml';
}
