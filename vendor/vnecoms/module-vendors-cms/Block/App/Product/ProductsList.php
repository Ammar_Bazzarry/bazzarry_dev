<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Vnecoms\VendorsCms\Block\App\Product;

use Vnecoms\VendorsProduct\Model\Source\Approval as ProductApproval;

/**
 * Catalog Products List widget block
 * Class ProductsList.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductsList extends \Magento\CatalogWidget\Block\Product\ProductsList implements \Vnecoms\VendorsCms\Block\App\AppInterface
{
    /**
     * @var string
     */
    protected $_template = 'Vnecoms_VendorsCms::app/product/grid.phtml';
    /**
     * @var \Vnecoms\Vendors\Model\Vendor
     */
    protected $vendor;

    /**
     * @var \Vnecoms\Vendors\Model\VendorFactory
     */
    protected $_vendorFactory;


    /**
     * ProductsList constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder
     * @param \Magento\CatalogWidget\Model\Rule $rule
     * @param \Magento\Widget\Helper\Conditions $conditionsHelper
     * @param \Vnecoms\Vendors\Model\VendorFactory $vendorFactory
     * @param array $data
     */
    public function __construct
    (
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder,
        \Magento\CatalogWidget\Model\Rule $rule,
        \Magento\Widget\Helper\Conditions $conditionsHelper,
        \Vnecoms\Vendors\Model\VendorFactory $vendorFactory,
        array $data = []
    )
    {
        $this->_vendorFactory = $vendorFactory;
        $context->getCache()->clean(\Magento\Catalog\Model\Product::CACHE_TAG);
        parent::__construct($context, $productCollectionFactory, $catalogProductVisibility, $httpContext, $sqlBuilder, $rule, $conditionsHelper, $data);
    }

    /**
     * Get vendor object
     *
     * @return \Vnecoms\Vendors\Model\Vendor
     */
    public function getVendor(){
        $vendor = $this->_coreRegistry->registry('vendor');
        if(!$vendor && $product = $this->_coreRegistry->registry('product')){
            if($vendorId = $product->getVendorId()){
                $vendor = $this->_vendorFactory->create()->load($vendorId);
            }
        }
        return $vendor;
    }

    /**
     * @return $this
     */
    public function createCollection()
    {
        $collection = parent::createCollection();

        return $collection->addFieldToFilter('vendor_id', $this->getVendor()->getId())
            ->addAttributeToFilter('approval',ProductApproval::STATUS_APPROVED)
            ->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
    }
}
