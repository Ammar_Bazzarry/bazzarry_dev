<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsCms\Block\App;

/**
 * Class StaticBlock.
 */
class StaticBlock extends \Magento\Framework\View\Element\Template implements AppInterface
{
    /**
     * @var string
     */
    protected $_template = 'Vnecoms_VendorsCms::app/static_block/default.phtml';

    /**
     * Storage for used apps.
     *
     * @var array
     */
    protected static $_appUsageMap = [];

    /**
     * Block factory.
     *
     * @var \Vnecoms\VendorsCms\Model\BlockFactory
     */
    protected $_blockFactory;

    /**
     * @var \Vnecoms\VendorsCms\Model\Template\Filter
     */
    protected $_cmsFilter;

    /**
     * StaticBlock constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Cms\Model\Template\FilterProvider       $filterProvider
     * @param \Vnecoms\VendorsCms\Model\Template\Filter        $cmsFilter
     * @param \Vnecoms\VendorsCms\Model\BlockFactory           $blockFactory
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Vnecoms\VendorsCms\Model\Template\Filter $cmsFilter,
        \Vnecoms\VendorsCms\Model\BlockFactory $blockFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_filterProvider = $filterProvider;
        $this->_cmsFilter = $cmsFilter;
        $this->_blockFactory = $blockFactory;
    }

    /**
     * Prepare block text and determine whether block output enabled or not
     * Prevent blocks recursion if needed.
     *
     * @return $this
     */
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();
        $blockId = $this->getData('block_id');
        $blockHash = get_class($this).$blockId;

        if (isset(self::$_appUsageMap[$blockHash])) {
            return $this;
        }
        self::$_appUsageMap[$blockHash] = true;

        if ($blockId) {
            $storeId = $this->_storeManager->getStore()->getId();
            /** @var \Vnecoms\VendorsCms\Model\Block $block */
            $block = $this->_blockFactory->create();
            $block->setStoreId($storeId)->load($blockId);
            if ($block->isActive()) {
                $this->setText(
                    $this->_cmsFilter->setStoreId($storeId)->filter($block->getContent())
                );
            }
        }

        unset(self::$_appUsageMap[$blockHash]);

        return $this;
    }

    /**
     * @return string
     */
    public function _toHtml()
    {
        return parent::_toHtml();
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->setTemplate('Vnecoms_VendorsCms::app/static_block/default.phtml');

        return parent::_prepareLayout(); // TODO: Change the autogenerated stub
    }
}
