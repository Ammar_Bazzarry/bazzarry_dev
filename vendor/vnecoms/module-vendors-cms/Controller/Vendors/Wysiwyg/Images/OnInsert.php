<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsCms\Controller\Vendors\Wysiwyg\Images;

class OnInsert extends \Vnecoms\VendorsCms\Controller\Vendors\Wysiwyg\Images
{
    /**
     * Fire when select image.
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        //$storeId = $this->getRequest()->getParam('store');
        $filename = $this->getRequest()->getParam('filename');
        $filename = $this->helperImages->idDecode($filename);
        $asIs = $this->getRequest()->getParam('as_is');
        //$this->_objectManager->get('Magento\Catalog\Helper\Data')->setStoreId($storeId);
        //$helper->setStoreId($storeId);

        $image = $this->helperImages->getImageHtmlDeclaration($filename, $asIs);

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();

        return $resultRaw->setContents($image);
    }
}
