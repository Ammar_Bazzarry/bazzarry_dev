<?php
/**
 * Default index action (with 404 Not Found headers)
 * Used if default page don't configure or available
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsCms\Controller\Index;

class DefaultIndex extends DefaultNoRoute
{
}
