<?php
/**
 * Created by PhpStorm.
 * User: mrtuvn
 * Date: 03/01/2017
 * Time: 15:44.
 */

namespace Vnecoms\VendorsCms\Model\Widget;

class Config extends \Magento\Widget\Model\Widget\Config
{
    protected $_url;

    public function __construct(
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Vnecoms\Vendors\Model\UrlInterface $url,
        \Magento\Framework\Url\DecoderInterface $urlDecoder,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Widget\Model\WidgetFactory $widgetFactory,
        \Magento\Framework\Url\EncoderInterface $urlEncoder
    ) {
        $this->_url = $url;
        parent::__construct($backendUrl, $urlDecoder, $assetRepo, $widgetFactory, $urlEncoder);
    }

    /*public function getBlockPluginSettings($config)
    {
        $url = $this->_assetRepo->getUrl('Vnecoms_VendorsCms::js/lib/wysiwyg/tiny_mce/plugins/blockwidget/block_plugin.js');

        $settings = [
            'widget_plugin_src' => $url,
            'widget_placeholders' => $this->_widgetFactory->create()->getPlaceholderImageUrls(),
            'widget_window_url' => $this->getWidgetWindowUrl($config),
        ];

        return $settings;
    }*/
}
