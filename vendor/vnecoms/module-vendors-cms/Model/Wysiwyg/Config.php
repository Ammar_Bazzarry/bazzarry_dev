<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsCms\Model\Wysiwyg;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

/**
 * Wysiwyg Config for Editor HTML Element.
 */
class Config extends \Magento\Framework\DataObject
{
    /**
     * Wysiwyg status enabled.
     */
    const WYSIWYG_ENABLED = 'enabled';

    /**
     * Wysiwyg status configuration path.
     */
    const WYSIWYG_STATUS_CONFIG_PATH = 'cms/wysiwyg/enabled';

    /**
     * Wysiwyg status hidden.
     */
    const WYSIWYG_HIDDEN = 'hidden';

    /**
     * Wysiwyg status disabled.
     */
    const WYSIWYG_DISABLED = 'disabled';

    /**
     * Wysiwyg image directory.
     */
    const IMAGE_DIRECTORY = 'wysiwyg';

    const IMAGE_VENDORSCMS_DIRECTORY = 'vendorscms';

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $_assetRepo;

    /**
     * @var \Magento\Variable\Model\Variable\Config
     */
    protected $_variableConfig;

    /**
     * @var \Vnecoms\VendorsCms\Model\Widget\Config
     */
    protected $_widgetConfig;

    /**
     * Core event manager proxy.
     *
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * Core store config.
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * @var array
     */
    protected $_windowSize;

    /** @var  \Vnecoms\Vendors\Model\UrlInterface */
    protected $_vendorUrl;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /** @var \Magento\Widget\Model\WidgetFactory  */
    protected $_widgetFactory;

    /** @var \Vnecoms\VendorsCms\Model\Widget\Config  */
    protected $_cmsWidgetConfig;

    /** @var \Vnecoms\VendorsCms\Model\Block\Config  */
    protected $_blockConfig;

    protected $_appConfig;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * Config constructor.
     *
     * @param \Magento\Backend\Model\UrlInterface                $backendUrl
     * @param \Magento\Framework\Event\ManagerInterface          $eventManager
     * @param \Magento\Framework\AuthorizationInterface          $authorization
     * @param \Magento\Framework\View\Asset\Repository           $assetRepo
     * @param \Magento\Variable\Model\Variable\Config            $variableConfig
     * @param \Vnecoms\VendorsCms\Model\Widget\Config            $widgetConfig
     * @param \Vnecoms\VendorsCms\Model\Block\Config             $blockConfig
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManager
     * @param \Magento\Widget\Model\WidgetFactory                $widgetFactory
     * @param \Vnecoms\VendorsCms\Model\Widget\Config            $cmsWidgetConfig
     * @param \Vnecoms\Vendors\Model\UrlInterface                $vendorUrl
     * @param Filesystem                                         $filesystem
     * @param array                                              $windowSize
     * @param array                                              $data
     */
    public function __construct(
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\AuthorizationInterface $authorization,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Variable\Model\Variable\Config $variableConfig,
        \Vnecoms\VendorsCms\Model\Widget\Config $widgetConfig,
        \Vnecoms\VendorsCms\Model\Block\Config $blockConfig,
        \Vnecoms\VendorsCms\Model\Wysiwyg\App\Config $appConfig,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Widget\Model\WidgetFactory $widgetFactory,
        \Vnecoms\VendorsCms\Model\Widget\Config $cmsWidgetConfig,
        \Vnecoms\Vendors\Model\UrlInterface $vendorUrl,
        Filesystem $filesystem,
        array $windowSize = [],
        array $data = []
    ) {
        $this->_backendUrl = $backendUrl;
        $this->_eventManager = $eventManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_authorization = $authorization;
        $this->_assetRepo = $assetRepo;
        $this->_variableConfig = $variableConfig;
        $this->_widgetConfig = $widgetConfig;
        $this->_blockConfig = $blockConfig;
        $this->_appConfig = $appConfig;
        $this->_windowSize = $windowSize;
        $this->_storeManager = $storeManager;
        $this->_widgetFactory = $widgetFactory;
        $this->_vendorUrl = $vendorUrl;
        $this->_cmsWidgetConfig = $cmsWidgetConfig;
        $this->filesystem = $filesystem;
        parent::__construct($data);
    }

    /**
     * Return Wysiwyg config as \Magento\Framework\DataObject.
     *
     * Config options description:
     *
     * enabled:                 Enabled Visual Editor or not
     * hidden:                  Show Visual Editor on page load or not
     * use_container:           Wrap Editor contents into div or not
     * no_display:              Hide Editor container or not (related to use_container)
     * translator:              Helper to translate phrases in lib
     * files_browser_*:         Files Browser (media, images) settings
     * encode_directives:       Encode template directives with JS or not
     *
     * @param array|\Magento\Framework\DataObject $data Object constructor params to override default config values
     *
     * @return \Magento\Framework\DataObject
     */
    public function getConfig($data = [])
    {
        $config = new \Magento\Framework\DataObject();

        $config->setData(
            [
                'enabled' => $this->isEnabled(),
                'hidden' => $this->isHidden(),
                'use_container' => false,
                'add_variables' => false,//disable btw
                'add_widgets' => false,
                'add_blocks' => true,
                'add_apps' => false,
                'add_images' => true,
                'add_directives' => true,
                'no_display' => false,
                'encode_directives' => true,
                'baseStaticUrl' => $this->_assetRepo->getStaticViewFileContext()->getBaseUrl(),
                'baseStaticDefaultUrl' => str_replace('index.php/', '', $this->_backendUrl->getBaseUrl())
                    .$this->filesystem->getUri(DirectoryList::STATIC_VIEW).'/',
                'directives_url' => $this->_vendorUrl->getUrl('cms/wysiwyg/directive'),
                'popup_css' => $this->_assetRepo->getUrl(
                    'mage/adminhtml/wysiwyg/tiny_mce/themes/advanced/skins/default/dialog.css'
                ),
                'content_css' => $this->_assetRepo->getUrl(
                    'mage/adminhtml/wysiwyg/tiny_mce/themes/advanced/skins/default/content.css'
                ),
                'width' => '100%',
                'height' => '',
                'plugins' => [],
            ]
        );

        $config->setData('directives_url_quoted', preg_quote($config->getData('directives_url')));

        //$url = $this->_assetRepo->getUrl('Vnecoms_VendorsCms::js/lib/wysiwyg/tiny_mce/plugins/blockwidget/editor_plugin.js');

        $config->addData(
            [
                'add_images' => true,
                'files_browser_window_url' => $this->_vendorUrl->getUrl('cms/wysiwyg_images/index'),
                'files_browser_window_width' => $this->_windowSize['width'],
                'files_browser_window_height' => $this->_windowSize['height'],
            ]
        );

        /*if ($config->getData('add_images')) {
            $settings = $this->_widgetConfig->getImagePluginSettings($config);
            $config->addData($settings);
        }*/

        if (is_array($data)) {
            $config->addData($data);
        }

        if ($config->getData('add_variables')) {
            //$settings = $this->_variableConfig->getWysiwygPluginSettings($config);
            $settings = $this->getWysiwygPluginSettings($config);
            //var_dump($settings);die();
            /*$settings['plugins'][0]['options']['url'] = $this->_vendorUrl->getUrl('cms/system_variable/wysiwygPlugin');

            $onclickParts = [
                'search' => ['html_id'],
                'subject' => 'MagentovariablePlugin.loadChooser(\'' .
                    $this->_vendorUrl->getUrl('cms/system_variable/wysiwygPlugin') .
                    '\', \'{{html_id}}\');',
            ];
            $settings['plugins'][0]['options']['onclick'] = $onclickParts;*/
            $config->addData($settings);
        }

        if ($config->getData('add_widgets')) {
            $settings = $this->_widgetConfig->getPluginSettings($config);
            $config->addData($settings);
        }

        if ($config->getData('add_blocks')) {
            $settings = $this->_blockConfig->getBlockPluginSettings($config);
            $config->addData($settings);
        }

        if ($config->getData('add_apps')) {
            $settings = $this->_appConfig->getPluginSettings($config);
            $config->addData($settings);
        }
        //echo"<pre>";var_dump($config->getData('plugins'));die;
        return $config;
    }

    /**
     * Prepare variable wysiwyg config
     *
     * @param \Magento\Framework\DataObject $config
     * @return array
     */
    public function getWysiwygPluginSettings($config)
    {
        $variableConfig = [];
        $onclickParts = [
            'search' => ['html_id'],
            'subject' => 'MagentovariablePlugin.loadChooser(\'' .
                $this->getVariablesWysiwygActionUrl() .
                '\', \'{{html_id}}\');',
        ];
        $variableWysiwyg = [
            [
                'name' => 'magentovariable',
                'src' => $this->getWysiwygJsPluginSrc(),
                'options' => [
                    'title' => __('Insert Vendors Variable ...'),
                    'url' => $this->getVariablesWysiwygActionUrl(),
                    'onclick' => $onclickParts,
                    'class' => 'add-variable vendors-variables test plugin',
                ],
            ],
        ];
        $configPlugins = $config->getData('plugins');
        $variableConfig['plugins'] = array_merge($configPlugins, $variableWysiwyg);
        return $variableConfig;
    }


    /**
     * Return path for skin images placeholder.
     *
     * @return string
     */
    public function getSkinImagePlaceholderPath()
    {
        $staticPath = $this->_storeManager->getStore()->getBaseStaticDir();
        $placeholderPath = $this->_assetRepo->createAsset('Vnecoms_VendorsCms::images/wysiwyg_skin_image.png')->getPath();

        return $staticPath.'/'.$placeholderPath;
    }

    /**
     * Check whether Wysiwyg is enabled or not.
     *
     * @return bool
     */
    public function isEnabled()
    {
        $wysiwygState = $this->_scopeConfig->getValue(
            self::WYSIWYG_STATUS_CONFIG_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );

        return in_array($wysiwygState, [self::WYSIWYG_ENABLED, self::WYSIWYG_HIDDEN]);
    }

    /**
     * Check whether Wysiwyg is loaded on demand or not.
     *
     * @return bool
     */
    public function isHidden()
    {
        $status = $this->_scopeConfig->getValue(
            self::WYSIWYG_STATUS_CONFIG_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return $status == self::WYSIWYG_HIDDEN;
    }

    /**
     * Return url to wysiwyg plugin
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function getWysiwygJsPluginSrc()
    {
        $editorPluginJs = 'Vnecoms_VendorsCms/js/lib/wysiwyg/tiny_mce/plugins/magentovariable/editor_plugin.js';
        return $this->_assetRepo->getUrl($editorPluginJs);
    }

    /**
     * Return url of action to get variables
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function getVariablesWysiwygActionUrl()
    {
        return $this->_vendorUrl->getUrl('cms/system_variable/wysiwygPlugin');
    }
}
