<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Vnecoms\VendorsCms\Model\Wysiwyg\Images\Storage;

/**
 * Wysiwyg Images storage collection.
 */
class Collection extends \Magento\Cms\Model\Wysiwyg\Images\Storage\Collection
{
}
