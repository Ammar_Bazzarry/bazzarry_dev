<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsCms\Model\Template;

/**
 * Class MediaFilter.
 */
class MediaFilter extends AbstractFilter
{
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * MediaFilter constructor.
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Variable\Model\VariableFactory $coreVariableFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Framework\UrlInterface $urlModel
     * @param \Pelago\Emogrifier $emogrifier
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Email\Model\Source\Variables $configVariables
     * @param \Magento\Framework\Filter\Template\Tokenizer\Parameter $tokenizeParams
     * @param \Magento\Framework\Filesystem $filesystem
     * @param array $variables
     */
    public function __construct(
        \Magento\Framework\Stdlib\StringUtils $string,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Variable\Model\VariableFactory $coreVariableFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\App\State $appState,
        \Magento\Framework\UrlInterface $urlModel,
        \Pelago\Emogrifier $emogrifier,
        \Magento\Framework\Registry $registry,
        \Magento\Email\Model\Source\Variables $configVariables,
        \Magento\Framework\Filter\Template\Tokenizer\Parameter $tokenizeParams,
        \Magento\Framework\Filesystem $filesystem,
        array $variables = []
    ) {
     
        $this->fileSystem = $filesystem;
        parent::__construct($string, $logger, $escaper, $assetRepo, $scopeConfig, $coreVariableFactory, $storeManager, $layout, $layoutFactory, $appState, $urlModel, $emogrifier, $registry, $configVariables, $tokenizeParams, $variables);
    }

    /**
     * Retrieve vendor url directive.
     *
     * @param array $construction
     *
     * @return string
     */
    public function mediaDirective($construction)
    {
        $skipParams = ['id'];
        $this->tokenizeParams->setString($construction[2]);
        $urlParameters = $this->tokenizeParams->tokenize();
        $url = trim($urlParameters['url']); // Ex vendorscms/athirainteriors/70x60-home-1.jpg
        
        //$params = $this->getParameters($construction[2]);
        //return $this->getBaseMediaUrl() . '/' . $url;
        //echo $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);die;
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$url;
    }
}
