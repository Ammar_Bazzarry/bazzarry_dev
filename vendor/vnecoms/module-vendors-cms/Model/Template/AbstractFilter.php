<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsCms\Model\Template;

/**
 * Cms Template Abbstract Filter.
 */
abstract class AbstractFilter extends \Magento\Email\Model\Template\Filter
{
    /**
     * @var \Magento\Framework\Filter\Template\Tokenizer\Parameter
     */
    protected $tokenizeParams;

    /** @var \Magento\Framework\Registry  */
    protected $_coreRegistry;

    /**
     * AbstractFilter constructor.
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Variable\Model\VariableFactory $coreVariableFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Framework\UrlInterface $urlModel
     * @param \Pelago\Emogrifier $emogrifier
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Email\Model\Source\Variables $configVariables
     * @param \Magento\Framework\Filter\Template\Tokenizer\Parameter $tokenizeParams
     * @param array $variables
     */
    public function __construct(
        \Magento\Framework\Stdlib\StringUtils $string,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Variable\Model\VariableFactory $coreVariableFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\App\State $appState,
        \Magento\Framework\UrlInterface $urlModel,
        \Pelago\Emogrifier $emogrifier,
        \Magento\Framework\Registry $registry,
        \Magento\Email\Model\Source\Variables $configVariables,
        \Magento\Framework\Filter\Template\Tokenizer\Parameter $tokenizeParams,
        array $variables = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($string, $logger, $escaper, $assetRepo, $scopeConfig, $coreVariableFactory, $storeManager, $layout, $layoutFactory, $appState, $urlModel, $emogrifier, $configVariables, $variables);
        $this->tokenizeParams = $tokenizeParams;
    }

    /**
     * @return \Vnecoms\Vendors\Model\Vendor
     */
    public function getVendor()
    {
        return $this->_coreRegistry->registry('current_vendor');
    }

    /**
     * @return \Vnecoms\VendorsPage\Helper\Data
     */
    public function getVendorHelper()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Vnecoms\VendorsPage\Helper\Data');
    }
}
