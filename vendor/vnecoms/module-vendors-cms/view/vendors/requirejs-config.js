/*
 * Copyright © 2017 Vnecoms. All rights reserved.
 */
var config = {
    map: {
        "*": {
            "vendorsCmsTinyMCESetup" : "Vnecoms_VendorsCms/js/lib/wysiwyg/tiny_mce/setup",
            "vendorsCmsWidget" : "Vnecoms_VendorsCms/js/lib/wysiwyg/widget",
            "vendorsCmsbrowser" : "Vnecoms_VendorsCms/js/browser",
            "appInstance" : "Vnecoms_VendorsCms/js/app/app",
            "insertBlock": "Vnecoms_VendorsCms/js/insert_block",
            "cmsVariables": "Vnecoms_VendorsCms/js/variables"
            /*"Magento_Variable/js/variables": "Vnecoms_VendorsProduct/js/components/variables"*/
        }
    },
    "shim": {
        "extjs/ext-tree-checkbox":["extjs/ext-tree"],
        "extjs/ext-tree":["prototype"],
    },
    "paths": {
        "CmsfolderTree": "Vnecoms_VendorsCms/js/folder-tree",
        // "mage/adminhtml/browser": "Vnecoms_VendorsCms/js/browser",
        // "mage/adminhtml/wysiwyg/tiny_mce/setup": "Vnecoms_VendorsCms/js/lib/wysiwyg/tiny_mce/setup"
    },
    "deps": [
    ]
};
