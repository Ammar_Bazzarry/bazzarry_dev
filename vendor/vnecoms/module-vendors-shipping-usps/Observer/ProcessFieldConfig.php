<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsShippingUSPS\Observer;

use Magento\Framework\Event\ObserverInterface;
use Vnecoms\VendorsConfig\Helper\Data;

class ProcessFieldConfig implements ObserverInterface
{
    /**
     * Add multiple vendor order row for each vendor.
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return self
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $transport = $observer->getTransport();
        $fieldset = $transport->getFieldset();

        $config = \Magento\Framework\App\ObjectManager::getInstance()->get(
            'Magento\Framework\App\Config\ScopeConfigInterface');

        $fieldsArray = [
            "shipping_method_usps_userid",
            "shipping_method_usps_password",
            "shipping_method_usps_allowed_methods",
            "shipping_method_usps_free_method",
            "shipping_method_usps_free_shipping_enable",
            "shipping_method_usps_free_shipping_subtotal",
            "shipping_method_usps_sallowspecific",
            "shipping_method_usps_specificcountry",
        ];


        if($fieldset->getHtmlId() == "shipping_method_usps"){
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE;
            $configVal = $config->getValue("carriers/usps/active",$storeScope);
            $configShipping = $config->getValue("carriers/vendor_multirate/active",$storeScope);
            if(!$configVal || !$configShipping) $transport->setForceReturn(true);

            $configShipping = $config->getValue("carriers/usps/api_type",$storeScope);
            if($configShipping == \Vnecoms\VendorsShippingUSPS\Model\Source\Type::USE_API_ADMIN){
                foreach ($fieldset->getElements() as $field) {
                    if(in_array($field->getHtmlId(),$fieldsArray))
                    $field->setIsRemoved(true);
                }
            }

        }


        return $this;
    }
}
