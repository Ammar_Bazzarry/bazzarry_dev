<?php
namespace Vnecoms\VendorsGroup\Ui;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Vnecoms\VendorsGroup\Helper\Data as Helper;
use Vnecoms\Vendors\Model\Session;

class MassAction extends \Magento\Ui\Component\MassAction
{
    /**
     * @var $helper
     */
    protected $helper;

    /**
     * @var \Vnecoms\Vendors\Model\Session
     */
    protected $session;

    /**
     * MassAction constructor.
     * @param Helper $helper
     * @param Session $session
     * @param ContextInterface $context
     * @param array $components
     * @param array $data
     */
    public function __construct(
        Helper $helper,
        Session $session,
        ContextInterface $context,
        $components = [],
        array $data = []
    )
    {
        $this->helper = $helper;
        $this->session = $session;
        parent::__construct($context, $components, $data);
    }

    /**
     * Get Vendor
     *
     * @return \Vnecoms\Vendors\Model\Vendor
     */
    public function getVendor(){
        return $this->session->getVendor();
    }

    /**
     * @inheritDoc
     */
    public function prepare()
    {
        $config = $this->getConfiguration();

        foreach ($this->getChildComponents() as $actionComponent) {
            $config['actions'][] = $actionComponent->getConfiguration();
        };

        $groupId = $this->getVendor()->getData('group_id');
        $newConfigActions = [];

        foreach ($config['actions'] as $configItem) {
            if ($configItem['type'] == 'delete' && !$this->helper->canDeleteProduct($groupId)){
                continue;
            }
            $newConfigActions[] = $configItem;
        }

        $config['actions'] = $newConfigActions;
        $this->setData('config', $config);
        $this->components = [];

        parent::prepare();
    }
}