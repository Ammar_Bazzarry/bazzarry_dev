<?php
namespace Vnecoms\VendorsGroup\Ui\DataProvider\Product\Form\Modifier;

use Vnecoms\VendorsGroup\Helper\Data as Helper;

/**
 * Data provider for categories field of product page
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 101.0.0
 */
class Categories extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier
{
    /**
     * @var \Vnecoms\Vendors\Model\Session
     */
    protected $vendorSession;

    /**
     * @var \Vnecoms\VendorsGroup\Helper\Data
     */
    protected $helper;

    /**
     * Categories constructor.
     * @param \Vnecoms\Vendors\Model\Session $vendorSession
     * @param Helper $helper
     */
    public function __construct(
        \Vnecoms\Vendors\Model\Session $vendorSession,
        Helper $helper
    ) {
        $this->vendorSession = $vendorSession;
        $this->helper = $helper;
    }

    /**
     * @param array $item
     * @param $configGroup
     * @return mixed
     */
    protected function recursive($item, $configGroup)
    {
        foreach ($item as $key=>$childItem) {
            if (in_array($childItem['value'], $configGroup)) {
                $item[$key]['is_active'] =0;
            }
            if (isset($item[$key]['optgroup'])) {
                $item[$key]['optgroup'] = $this->recursive($childItem['optgroup'], $configGroup);
            }
        }
        return $item;
    }

    /**
     * @inheritdoc
     * @since 101.0.0
     */
    public function modifyMeta(array $meta)
    {
        $groupId = $this->vendorSession->getVendor()->getGroupId();
        $configGroup = $this->helper->getCategoriesRestriction($groupId);
        $configGroup = is_array($configGroup) ? $configGroup : [];
        $meta['product-details']['children']['container_category_ids']['children']['category_ids']['arguments']['data']['config']['optgroupTmpl']
            = 'Vnecoms_VendorsGroup/grid/filters/elements/ui-select-optgroup';

        $meta['product-details']['children']['container_category_ids']['children']['category_ids']['arguments']['data']['config']['options'] =
            $this->recursive($meta['product-details']['children']['container_category_ids']['children']['category_ids']['arguments']['data']['config']['options'], $configGroup);

        return $meta;
    }

    /**
     * @inheritdoc
     * @since 101.0.0
     */
    public function modifyData(array $data)
    {
        return $data;
    }
}
