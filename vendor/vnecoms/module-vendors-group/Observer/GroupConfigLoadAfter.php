<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsGroup\Observer;

use Magento\Framework\Event\ObserverInterface;

class GroupConfigLoadAfter implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $groupConfig = $observer->getData('result');
        $groupConfigJson = json_decode($groupConfig->getValue(), true);
        if (!$groupConfigJson) return;
        $groupConfig->setValue($groupConfigJson);
    }
}
