<?php

namespace Vnecoms\VendorsGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Vnecoms\VendorsGroup\Helper\Data as Helper;
use Vnecoms\Vendors\Model\Session;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;

class ProductDeleteBefore implements ObserverInterface
{
    /**
     * @var $helper
     */
    protected $helper;

    /**
     * @var \Vnecoms\Vendors\Model\Session
     */
    protected $session;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * ProductDeleteBefore constructor.
     * @param Helper $helper
     * @param Session $session
     * @param ManagerInterface $messageManager
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        Helper $helper,
        Session $session,
        ManagerInterface $messageManager,
        UrlInterface $urlBuilder
    )
    {
        $this->helper = $helper;
        $this->session = $session;
        $this->messageManager = $messageManager;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Get Vendor
     *
     * @return \Vnecoms\Vendors\Model\Vendor
     */
    public function getVendor(){
        return $this->session->getVendor();
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $action = $observer->getControllerAction();
        $groupId = $this->getVendor()->getData('group_id');
        if (!$this->helper->canDeleteProduct($groupId)){
            $this->messageManager->addErrorMessage(
                __('You are not permitted to delete products')
            );
            $action->getActionFlag()->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
            $action->getResponse()->setRedirect($this->urlBuilder->getUrl('catalog/product'));
        }
    }
}
