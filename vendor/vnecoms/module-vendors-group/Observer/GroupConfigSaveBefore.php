<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsGroup\Observer;

use Magento\Framework\Event\ObserverInterface;

class GroupConfigSaveBefore implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $groupConfig = $observer->getData('vendor_group_config');

        if(is_array($groupConfig->getValue())){
            $groupConfig->setValue(json_encode($groupConfig->getValue()));
        }
    }
}
