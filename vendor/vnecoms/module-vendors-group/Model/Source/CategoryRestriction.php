<?php

namespace Vnecoms\VendorsGroup\Model\Source;

class CategoryRestriction extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @var array
     */
    protected $_options;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\Framework\Convert\DataObject
     */
    protected $_converter;

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Convert\DataObject $converter
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory,
        \Magento\Framework\Convert\DataObject $converter
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->_converter = $converter;
    }

    /**
     * (non-PHPdoc).
     *
     * @see \Magento\Eav\Model\Entity\Attribute\Source\SourceInterface::getAllOptions()
     */
    public function getAllOptions($blankLine = false)
    {
        if (!$this->_options) {
            $this->_options = [];
            $rootCategories = $this->collectionFactory->create()
                ->addAttributeToSelect('name')
                ->addAttributeToFilter('level', 0);

            foreach($rootCategories as $category){
                $this->_options[] = [
                    'value' => $category->getId(),
                    'label' => $category->getName(),
                ];
                $this->addChildrenCategories($category, 1);
            }

            if ($blankLine) {
                array_unshift($this->_options, ['value' => '', 'label' => __('-- Please Select --')]);
            }
        }

        return $this->_options;
    }

    protected function addChildrenCategories($parent, $level){
        foreach($parent->getChildrenCategories() as $category){
            $this->_options[] = [
                'value' => $category->getId(),
                'label' => str_pad('',  $level*2, "--").' '.$category->getName(),
            ];
            $this->addChildrenCategories($category, $level +1);
        }
    }
    /**
     * Retrieve option array.
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_options = [];
        foreach ($this->toOptionArray() as $option) {
            $_options[$option['value']] = $option['label'];
        }

        return $_options;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
