/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            messageLoader: 'Vnecoms_VendorsMessage/js/message-loader'
        }
    }
};
