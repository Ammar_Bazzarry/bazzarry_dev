<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsReport\Model\ResourceModel\Product\Sold\Collection;

class Initial extends \Vnecoms\VendorsReport\Model\ResourceModel\Report\Collection
{
    /**
     * Report sub-collection class name
     *
     * @var string
     */
    protected $_reportCollection = 'Vnecoms\VendorsReport\Model\ResourceModel\Product\Sold\Collection';
}
