<?php
namespace Vnecoms\VendorsReview\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice;

class ProcessInvoice implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * @var \Vnecoms\VendorsReview\Model\Review\LinkFactory
     */
    protected $_reviewLinkFactory;


    public function __construct(
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Vnecoms\VendorsReview\Model\Review\LinkFactory $reviewLinkFactory
    ) {
        $this->_eventManager = $eventManager;
        $this->_reviewLinkFactory = $reviewLinkFactory;
    }

    /**
     * Add multiple vendor order row for each vendor.
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return self
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $invoice = $observer->getInvoice();
        $order = $invoice->getOrder();
        $customerId = $order->getCustomerId();
        if (!$customerId) {
            return;
        }
        
        /*Group invoice item by  vendor*/
        foreach ($invoice->getAllItems() as $item) {
            $orderItem = $item->getOrderItem();
            if ($orderItem->getParentItemId()) {
                continue;
            }
            
            if ($vendorId = $orderItem->getVendorId()) {
                $link = $this->_reviewLinkFactory->create();
                $collection = $link->getCollection()->addFieldToFilter('order_item_id', $orderItem->getId());
                if ($collection->count()) {
                    continue;
                }
                
                $link->setData([
                    'vendor_id' => $vendorId,
                    'customer_id' => $customerId,
                    'order_id' => $order->getId(),
                    'order_item_id' => $orderItem->getId(),
                    'show_review_link' => 1,
                    'can_review' => 1,
                ])->save();
            }
        }
        return $this;
    }
}
