<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsReview\Observer;

use Magento\Framework\Event\ObserverInterface;
use Vnecoms\VendorsReview\Model\ResourceModel\Review\CollectionFactory;

/**
 * AdminNotification observer
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class PendingReviewObserver implements ObserverInterface
{
    /**
     * Url Builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;
    
    /**
     * Vendor collection
     * @var \Vnecoms\Vendors\Model\ResourceModel\Vendor\Collection
     */
    protected $_reviewCollection;
    
    /**
     * Constructor
     *
     * @param Context $context
     * @param Registry $coreRegistry
     * @param Date $dateFilter
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        \Magento\Framework\View\Element\Context $context,
        array $data = []
    ) {
        $this->_urlBuilder = $context->getUrlBuilder();
        $this->_reviewCollection = $collectionFactory->create();
        $this->_reviewCollection->addFieldToFilter('status', \Vnecoms\VendorsReview\Model\Review::STATUS_PENDING);
    }
    
    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_urlBuilder->getUrl($route, $params);
    }
    
    /**
     * Get number of pending vendor
     * @return number
     */
    public function getNumberOfPendingReview()
    {
        return $this->_reviewCollection->count();
    }
    
    /**
     * Add the notification if there are any vendor awaiting for approval.
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $reviewCount    = $this->getNumberOfPendingReview();
        if ($reviewCount <= 0) {
            return;
        }
        
        $transport      = $observer->getTransport();
        $notifications  = $transport->getNotifications();
        $om             = \Magento\Framework\App\ObjectManager::getInstance();
        $notification   = $om->create('Magento\Framework\DataObject');
        
        if ($reviewCount ==1) {
            $notification->setData([
                'title'=> __("Seller Review"),
                'description' => __("There is a review awaiting for your approval.<br /><a href=\"%1\">Click here</a> to review it.", $this->getUrl('vendors/reviews/index'))
            ]);
        } else {
            $notification->setData([
                'title'=> __("Seller Review"),
                'description' => __('There are %1 reviews awaiting for your approval.<br /><a href="%2">Click here</a> to review them.', sprintf('<strong style="color: #ef672f">%s</strong>', $reviewCount), $this->getUrl('vendors/reviews/index'))
            ]);
        }
        $notifications[] = $notification;
        $transport->setNotifications($notifications);
    }
}
