<?php
namespace Vnecoms\VendorsReview\Block\Home;

use Magento\Catalog\Helper\ImageFactory as ImageHelperFactory;

class Review extends \Vnecoms\VendorsReview\Block\Profile\Review
{
    /**
     * @var \Vnecoms\VendorsReview\Helper\Data
     */
    protected $_reviewHelper;

    /**
     * @var \Vnecoms\VendorsReview\Model\ResourceModel\Review\Collection
     */
    protected $_reviewsCollection;
    
    /**
     * @var ImageHelperFactory
     */
    protected $imageHelperFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Vnecoms\VendorsPage\Helper\Data $pageHelper,
        \Vnecoms\VendorsReview\Model\ReviewFactory $reviewFactory,
        \Vnecoms\Vendors\Model\VendorFactory $vendorFactory,
        \Magento\Framework\Registry $registry,
        \Vnecoms\VendorsReview\Helper\Data $reviewHelper,
        \Vnecoms\Vendors\Helper\Data $helper,
        ImageHelperFactory $imageHelperFactory,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $pageHelper,
            $reviewFactory,
            $vendorFactory,
            $registry,
            $helper,
            $data
        );
        $this->imageHelperFactory = $imageHelperFactory;
        $this->_reviewHelper = $reviewHelper;
    }
    
    /**
     * (non-PHPdoc)
     * @see \Magento\Framework\View\Element\AbstractBlock::_prepareLayout()
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->showAllReviews()) {
            return;
        }
        
        /*
         * @var \Magento\Theme\Block\Html\Pager
         * Show page
        */
        $pager = $this->getLayout()->createBlock(
            'Magento\Theme\Block\Html\Pager',
            'vendor.review.pager'
        );
        $pager->setShowAmounts(false)
        ->setCollection($this->getReviewsCollection());
        $this->setChild('pager', $pager);
    
        return $this;
    }
    
    /**
     * Get Lastest Reviews
     *
     * @return \Vnecoms\VendorsReview\Model\ResourceModel\Review\Collection
     */
    public function getReviewsCollection()
    {
        if (!$this->_reviewsCollection) {
            $this->_reviewsCollection = $this->_reviewFactory->create()->getCollection();
            $this->_reviewsCollection->addFieldToFilter('entity_pk_value', $this->getVendor()->getId())
                ->addFieldToFilter('status', \Vnecoms\VendorsReview\Model\Review::STATUS_APPROVED)
                ->setOrder('review_id', 'DESC');
            
            $numOfReviews = $this->_reviewHelper->getNumOfReviewsOnVendorHomePage();
            if ($numOfReviews && !$this->showAllReviews()) {
                $this->_reviewsCollection->setPageSize($numOfReviews);
                $this->_reviewsCollection->setCurPage(0);
            }
        }
        
        return $this->_reviewsCollection;
    }
    
    /**
     * Can show all reviews
     *
     * @return boolean
     */
    public function showAllReviews()
    {
        return $this->getData('show_all_reviews');
    }
    
    /**
     * Get view all items URL
     *
     * @return string
     */
    public function getViewAllItemsUrl()
    {
        return $this->_pageHelper->getUrl($this->getVendor(), 'reviews');
    }
    
    /**
     * Get pager html
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    
    /**
     * Get Product Image
     *
     * @param \Magento\Catalog\Model\Product $product
     */
    public function getProductImageUrl(\Magento\Catalog\Model\Product $product)
    {
        $helper = $this->imageHelperFactory->create()
        ->init($product, 'category_page_grid')->resize(100, 80);
    
        return $helper->getUrl();
    }
}
