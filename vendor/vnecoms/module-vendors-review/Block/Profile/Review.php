<?php
namespace Vnecoms\VendorsReview\Block\Profile;

class Review extends \Magento\Framework\View\Element\Template
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Vnecoms\VendorsPage\Helper\Data
     */
    protected $_pageHelper;
    
    /**
     * @var \Vnecoms\VendorsReview\Model\ReviewFactory
     */
    protected $_reviewFactory;
    
    /**
     * @var \Vnecoms\Vendors\Model\VendorFactory
     */
    protected $_vendorFactory;


    /**
     * @var \Vnecoms\Vendors\Helper\Data
     */
    protected $_vendorHelper;
    
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param \Magento\Framework\Registry $registry
     * @param Vnecoms\Vendors\Helper\Data
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Vnecoms\VendorsPage\Helper\Data $pageHelper,
        \Vnecoms\VendorsReview\Model\ReviewFactory $reviewFactory,
        \Vnecoms\Vendors\Model\VendorFactory $vendorFactory,
        \Magento\Framework\Registry $registry,
        \Vnecoms\Vendors\Helper\Data $vendorHelper,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_pageHelper = $pageHelper;
        $this->_reviewFactory = $reviewFactory;
        $this->_vendorFactory = $vendorFactory;
        $this->_vendorHelper = $vendorHelper;
        parent::__construct($context, $data);
    }

    /**
     * Get vendor object
     *
     * @return \Vnecoms\Vendors\Model\Vendor
     */
    public function getVendor()
    {
        $vendor = $this->_coreRegistry->registry('vendor');
        if (!$vendor && $product = $this->_coreRegistry->registry('product')) {
            if ($vendorId = $product->getVendorId()) {
                $vendor = $this->_vendorFactory->create()->load($vendorId);
            }
        }
        return $vendor;
    }
    
    /**
     * Get Review Count
     * @return number
     */
    public function getReviewCount()
    {
        $reviewResource = $this->_reviewFactory->create()->getResource();
        return $reviewResource->getReviewCount($this->getVendor()->getId());
    }
    
    /**
     * Get vendor rating
     *
     * @return number
     */
    public function getRating()
    {
        $reviewResource = $this->_reviewFactory->create()->getResource();
        $rating = $reviewResource->getAverageRating($this->getVendor()->getId());
        return round($rating*100/5);
    }
    
    /**
     * Is home page
     *
     * @return boolean
     */
    public function isHomePage()
    {
        return $this->_coreRegistry->registry('is_home_page');
    }
    
    /**
     * Get Review Link
     *
     * @return string
     */
    public function getReviewLink()
    {
        if ($this->isHomePage()) {
            return '#vendor-reviews';
        }
        
        return $this->_pageHelper->getUrl($this->getVendor(), 'reviews');
    }

    protected function _toHtml()
    {
        if (!$this->getVendor() || !$this->_vendorHelper->moduleEnabled() ) {
            return '';
        }
        return parent::_toHtml();
    }
}
