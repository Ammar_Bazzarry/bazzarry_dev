<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsReview\Controller\Adminhtml\Reviews;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Vnecoms\VendorsReview\Model\ResourceModel\Review\CollectionFactory;

/**
 * Class MassDelete
 */
class MassStatus extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Vnecoms\VendorsReview\Helper\Data
     */
    protected $_reviewHelper;

    
    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Vnecoms\VendorsReview\Helper\Data $reviewHelper
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->_reviewHelper = $reviewHelper;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();
        $status = $this->getRequest()->getParam('status');
        $vendor = $this->_objectManager->create('Vnecoms\Vendors\Model\Vendor');
        $customer = $this->_objectManager->create('Magento\Customer\Model\Customer');
        foreach ($collection as $review) {
            $review->setStatus($status)->save();
            $vendor->load($review->getEntityPkValue());
            $customer->load($review->getSenderPkValue());
            /*Send notification email to vendor*/
            $this->_reviewHelper->sendNewReviewNotificationToVendor($review, $vendor, $customer, $review->getItem());
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been updated.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
