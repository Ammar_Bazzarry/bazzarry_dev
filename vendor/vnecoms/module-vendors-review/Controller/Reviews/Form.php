<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsReview\Controller\Reviews;

use Magento\Framework\Exception\NotFoundException;

class Form extends \Magento\Framework\App\Action\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Vnecoms\Vendors\Helper\Data
     */
    protected $_vendorHelper;
    
    /**
     * @var \Vnecoms\VendorsPage\Helper\Data
     */
    protected $_vendorPageHelper;
    
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Model\Session $customerSession,
        \Vnecoms\Vendors\Helper\Data $vendorHelper,
        \Vnecoms\VendorsPage\Helper\Data $vendorPageHelper
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_vendorHelper = $vendorHelper;
        $this->_vendorPageHelper = $vendorPageHelper;
        $this->_customerSession = $customerSession;
    }
    
    /**
     * Display customer wishlist
     *
     * @return \Magento\Framework\View\Result\Page
     * @throws NotFoundException
     */
    public function execute()
    {
        if (!$this->_coreRegistry->registry('vendor')) {
            return $this->_forward('no-route');
        }
        $vendor = $this->_coreRegistry->registry('vendor');
        
        try {
            $orderItem = $this->_objectManager->create('Magento\Sales\Model\Order\Item');
            $orderItem->load($this->getRequest()->getParam('item'));
            
            if (!$orderItem->getId()) {
                throw new \Exception(__("The purchased item is not available."));
            }
            
            $linkCollection = $this->_objectManager->create('Vnecoms\VendorsReview\Model\ResourceModel\Review\Link\Collection');
            $linkCollection->addFieldToFilter('order_item_id', $orderItem->getId())
                ->addFieldToFilter('vendor_id', $vendor->getId())
                ->addFieldToFilter('customer_id', $this->_customerSession->getCustomerId())
                ->addFieldToFilter('can_review', 1);
            
            if (!$linkCollection->count()) {
                throw new \Exception(__("You are not allowed to leave a review for the item '%1'.", $orderItem->getName()));
            }
            
            $this->_coreRegistry->register('order_item', $orderItem);
            
            $this->_view->loadLayout();
            $pageConfig = $this->_view->getPage()->getConfig();
            $storeName = $this->_vendorHelper->getVendorStoreName($vendor->getId());
            $description = $this->_vendorHelper->getVendorStoreShortDescription($vendor->getId());
            $pageConfig->getTitle()
                ->set(__("%1's - Reviews Form", $storeName));
            $pageConfig->setDescription($description);
            
            $this->_view->renderLayout();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $redirect = $this->resultRedirectFactory->create();
            $redirect->setUrl($this->_vendorPageHelper->getUrl($vendor, 'reviews'));
            return $redirect;
        }
    }
}
