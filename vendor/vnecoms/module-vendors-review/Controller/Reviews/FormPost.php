<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsReview\Controller\Reviews;

use Magento\Framework\Exception\NotFoundException;
use Vnecoms\VendorsReview\Model\Review;

class FormPost extends \Magento\Framework\App\Action\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Vnecoms\Vendors\Helper\Data
     */
    protected $_vendorHelper;
    
    /**
     * @var \Vnecoms\VendorsPage\Helper\Data
     */
    protected $_vendorPageHelper;
    
    /**
     * @var \Vnecoms\VendorsReview\Helper\Data
     */
    protected $_reviewHelper;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;
    
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Vnecoms\Vendors\Helper\Data $vendorHelper
     * @param \Vnecoms\VendorsPage\Helper\Data $vendorPageHelper
     * @param \Vnecoms\VendorsReview\Helper\Data $reviewHelper
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Vnecoms\Vendors\Helper\Data $vendorHelper,
        \Vnecoms\VendorsPage\Helper\Data $vendorPageHelper,
        \Vnecoms\VendorsReview\Helper\Data $reviewHelper,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_vendorHelper = $vendorHelper;
        $this->_vendorPageHelper = $vendorPageHelper;
        $this->_reviewHelper = $reviewHelper;
        $this->_customerFactory = $customerFactory;
    }
    
    /**
     * Display customer wishlist
     *
     * @return \Magento\Framework\View\Result\Page
     * @throws NotFoundException
     */
    public function execute()
    {
        if (!$this->_coreRegistry->registry('vendor')) {
            return $this->_forward('no-route');
        }
        $vendor = $this->_coreRegistry->registry('vendor');
        
        try {
            $orderItem = $this->_objectManager->create('Magento\Sales\Model\Order\Item');
            $orderItem->load($this->getRequest()->getParam('item'));
            
            if (!$orderItem->getId()) {
                throw new \Exception(__("The purchased item is not available."));
            }
            
            $linkCollection = $this->_objectManager->create('Vnecoms\VendorsReview\Model\ResourceModel\Review\Link\Collection');
            $linkCollection->addFieldToFilter('order_item_id', $orderItem->getId())
                ->addFieldToFilter('vendor_id', $vendor->getId())
                ->addFieldToFilter('can_review', 1);
            
            if (!$linkCollection->count()) {
                throw new \Exception(__("You are not allowed to leave a review for the item '%1' this time.", $orderItem->getName()));
            }
            
            $link = $linkCollection->getFirstItem();
            $customer = $this->_customerFactory->create()->load($link->getCustomerId());
            
            $review = $this->_objectManager->create('Vnecoms\VendorsReview\Model\Review');
            
            $status = $this->_reviewHelper->isRequireApproval()?Review::STATUS_PENDING:Review::STATUS_APPROVED;
            if(!$this->getRequest()->getParam('title')){
                throw new \Exception(__("Please enter review title."));
            }
            $review->setData([
                'type' => Review::TYPE_VENDOR,
                'entity_pk_value' => $vendor->getId(),
                'order_id' => $orderItem->getOrderId(),
                'product_id' => $orderItem->getProductId(),
                'is_public' => 0,
                'rating' => $this->getRequest()->getParam('rating'),
                'nickname' => $this->getRequest()->getParam('nickname'),
                'title' => $this->getRequest()->getParam('title'),
                'detail' => $this->getRequest()->getParam('detail'),
                'status' => $status,
            ]);
            $review->save();
            
            $product = $orderItem->getProduct();
            /*Send notification email to customer*/
            $this->_reviewHelper->sendNewReviewNotificationToCustomer($review, $vendor, $customer, $orderItem);
            
            if (!$this->_reviewHelper->isRequireApproval()) {
                /*Send notification email to vendor*/
                $this->_reviewHelper->sendNewReviewNotificationToVendor($review, $vendor, $customer, $orderItem);
            }

            /*Update the review link*/
            $reviewLink = $this->_objectManager->create('Vnecoms\VendorsReview\Model\Review\Link');
            $reviewLink->load($orderItem->getId(), 'order_item_id');
            $reviewLink->setData('show_review_link', 0)
                ->setData('can_review', 0)
                ->save();
            
            $this->messageManager->addSuccess(__("Your review has been submited."));
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        
        $redirect = $this->resultRedirectFactory->create();
        $redirect->setUrl($this->_vendorPageHelper->getUrl($vendor, 'reviews'));
        return $redirect;
    }
}
