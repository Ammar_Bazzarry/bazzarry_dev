<?php

namespace Vnecoms\VendorsReview\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'ves_store_credit'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_vendor_review')
        )->addColumn(
            'review_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'type',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            1,
            ['unsigned' => true, 'nullable' => false,],
            'Review Type (1 => Customer, 2 => Vendor)'
        )->addColumn(
            'entity_pk_value',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Object Id (vendor_id or customer_id(depends on type) the one who gets the review.)'
        )->addColumn(
            'sender_pk_value',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Object Id (vendor_id or customer_id(depends on type) the one who leaves the review.)'
        )->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Related Order Id'
        )->addColumn(
            'product_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Related Product Id'
        )->addColumn(
            'is_public',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            null,
            ['unsigned' => true, 'nullable' => false,],
            'Is Public Review'
        )->addColumn(
            'rating',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            2,
            ['default' => '0'],
            'Rating'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            1,
            ['unsigned' => true, 'nullable' => false,],
            'Status'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Created At'
        )->setComment(
            'Vendor Review Table'
        );
        $installer->getConnection()->createTable($table);
        
        /**
         * Create table 'ves_vendor_review_detail'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_vendor_review_detail')
        )->addColumn(
            'detail_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Detail Id'
        )->addColumn(
            'review_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Review Id'
        )->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            [],
            'Title'
        )->addColumn(
            'detail',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            \Magento\Framework\DB\Ddl\Table::MAX_TEXT_SIZE,
            [],
            'Detail'
        )->addColumn(
            'nickname',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '32',
            [],
            'Nickname'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Created At'
        )->addForeignKey(
            $installer->getFkName('ves_vendor_review_detail', 'review_id', 'ves_vendor_review', 'review_id'),
            'review_id',
            $installer->getTable('ves_vendor_review'),
            'review_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Vendor Review Detail Table'
        );
        $installer->getConnection()->createTable($table);
        
        
        /**
         * Create table 'ves_vendor_review_link'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_vendor_review_link')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Link Id'
        )->addColumn(
            'vendor_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Vendor Id Id'
        )->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Customer Id'
        )->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Order Item Id'
        )->addColumn(
            'order_item_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Order Item Id'
        )->addColumn(
            'show_review_link',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Can show review link'
        )->addColumn(
            'can_review',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Can review'
        )/* ->addForeignKey(
            $installer->getFkName('ves_vendor_review_link', 'vendor_id', 'ves_vendor_entity', 'entity_id'),
            'vendor_id',
            $installer->getTable('ves_vendor_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('ves_vendor_review_link', 'customer_id', 'customer_entity', 'entity_id'),
            'customer_id',
            $installer->getTable('customer_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        ) */->setComment(
            'Vendor Review Link Table'
        );
        $installer->getConnection()->createTable($table);
        
        
        
        $installer->endSetup();
    }
}
