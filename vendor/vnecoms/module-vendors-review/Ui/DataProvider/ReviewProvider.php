<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsReview\Ui\DataProvider;

use Vnecoms\VendorsReview\Model\ResourceModel\Review\CollectionFactory;

/**
 * Class ProductDataProvider
 */
class ReviewProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    /**
     * Product collection
     *
     * @var \Vnecoms\VendorsReview\Model\ResourceModel\Review\Collection
     */
    protected $collection;

    /**
     * @var \Magento\Ui\DataProvider\AddFieldToCollectionInterface[]
     */
    protected $addFieldStrategies;

    /**
     * @var \Magento\Ui\DataProvider\AddFilterToCollectionInterface[]
     */
    protected $addFilterStrategies;

    /**
     * Construct
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Ui\DataProvider\AddFieldToCollectionInterface[] $addFieldStrategies
     * @param \Magento\Ui\DataProvider\AddFilterToCollectionInterface[] $addFilterStrategies
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $addFieldStrategies = [],
        array $addFilterStrategies = [],
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();

        $this->collection->join(['vendor'=>$this->collection->getTable('ves_vendor_entity')], 'vendor.entity_id = main_table.entity_pk_value', ['vendor_id'], null, 'left');
        $this->collection->join(['review_detail'=>$this->collection->getTable('ves_vendor_review_detail')], 'review_detail.review_id = main_table.review_id', ['title','detail','nickname'], null, 'left'); 
     
        $this->collection->join(['order_item'=>$this->collection->getTable('sales_order_item')], 'order_item.product_id = main_table.product_id AND order_item.order_id = main_table.order_id', ['name'], null, 'left');

        $this->collection->getSelect()->distinct();

        $this->addFieldStrategies = $addFieldStrategies;
        $this->addFilterStrategies = $addFilterStrategies;
    }
}
