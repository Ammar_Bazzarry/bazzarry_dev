<?php

namespace Vnecoms\BannerManager\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /*
         * Drop tables if exists
         */
        $installer->getConnection()->dropTable($installer->getTable('ves_bannermanager_banner'));
        $installer->getConnection()->dropTable($installer->getTable('ves_bannermanager_item'));



        /*
         * Create table ves_bannerslider_banner
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_bannermanager_banner')
        )->addColumn(
            'banner_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            11,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Banner ID'
        )->addColumn(
            'vendor_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Vendor ID'
        )->addColumn(
            'identifier',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ''],
            'Identifier'
        )->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ''],
            'Banner Title'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            6,
            ['nullable' => false, 'default' => 0],
            'Status'
        )->addColumn(
            'banner_template',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            60,
            ['nullable' => false, 'default' => ''],
            'Banner Template'
        )/*->addColumn(
            'slider_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            6,
            ['nullable' => true, 'default' => 0],
            'Slider Type'
        )->addColumn(
            'slider_mode',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => ''],
            'Slider Mode'
        )*/->addColumn(
            'hide_pagination',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            6,
            ['nullable' => true, 'default' => 0],
            'Hide Pagination'
        )->addColumn(
            'type_pagination',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            20,
            ['nullable' => true, 'default' => 'bullets'],
            'Type of Pagination'
        )->addColumn(
            'effect',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            20,
            ['nullable' => true, 'default' => ''],
            'Animation Effect'
        )->addColumn(
            'speed',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            6,
            ['nullable' => true, 'default' => '0'],
            'Slider speed'
        )->addColumn(
            'delay',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            6,
            ['nullable' => true, 'default' => '0'],
            'Banner delay'
        )/*->addColumn(
            'auto_height',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            6,
            ['nullable' => true, 'default' => '0'],
            'Auto Fit Height'
        )*//*->addColumn(
            'allow_touch',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            6,
            ['nullable' => true, 'default' => '0'],
            'Allow touch mobile'
        )*//*->addColumn(
            'direction',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            6,
            ['nullable' => true, 'default' => '0'],
            'Slider direction'
        )*/->addColumn(
            'creation_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Banner created time'
        )->addColumn(
            'update_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Banner update time'
        )->addColumn(
            'from_date',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => true],
            'show banner from date'
        )->addColumn(
            'to_date',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => true],
            'show banner to date'
        )->addColumn(
            'sort_order',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            11,
            ['nullable' => true, 'default' => '0'],
            'Sort Order'
        )->addIndex(
            $installer->getIdxName('ves_bannermanager_banner', ['banner_id']),
            ['banner_id']
        )->addIndex(
            $installer->getIdxName('ves_bannermanager_banner', ['vendor_id']),
            ['vendor_id']
        )->addIndex(
            $installer->getIdxName('ves_bannermanager_banner', ['status']),
            ['status']
        );
        $installer->getConnection()->createTable($table);
        /*
         * End create table ves_bannermanager_banner
         */

        /*
         * Create table ves_bannerslider_item
         */

        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_bannermanager_item')
        )->addColumn(
            'item_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            11,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Item ID'
        )->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => ''],
            'Item title'
        )->addColumn(
            'filename',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ''],
            'Filename'
        )->addColumn(
            'alt',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => ''],
            'Alt'
        )->addColumn(
            'url',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Url'
        )->addColumn(
            'target',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            10,
            ['nullable' => true, 'default' => '_blank'],
            'Target'
        )->addColumn(
            'custom_html',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Custom Html'
        )/*->addColumn(
            'width',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            100,
            ['nullable' => true],
            'Width'
        )->addColumn(
            'height',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            100,
            ['nullable' => true],
            'Height'
        )*/->addColumn(
            'short_description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ''],
            'Short Description'
        )->addColumn(
            'sort_order',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            11,
            ['nullable' => false, 'default' => '0'],
            'Sort Order'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            6,
            ['nullable' => true, 'default' => '1'],
            'Status'
        )/*->addColumn(
            'created_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => true],
            'Created Time'
        )->addColumn(
            'update_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => true],
            'Update Time'
        )*/->addColumn(
            'banner_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            11,
            ['nullable' => false],
            'Banner Id'
        )->addIndex(
            $installer->getIdxName('ves_bannermanager_item', ['item_id']),
            ['item_id']
        )->addIndex(
            $installer->getIdxName('ves_bannermanager_item', ['status']),
            ['status']
        );
        $installer->getConnection()->createTable($table);


        $installer->endSetup();
    }
}
