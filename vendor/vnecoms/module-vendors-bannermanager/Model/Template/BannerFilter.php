<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\BannerManager\Model\Template;


class BannerFilter extends \Magento\Email\Model\Template\Filter
{
    protected $_banner;

    /**
     * @var \Magento\Framework\Filter\Template\Tokenizer\Parameter
     */
    protected $tokenizeParams;

    public function __construct(
        \Magento\Framework\Stdlib\StringUtils $string,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Variable\Model\VariableFactory $coreVariableFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\App\State $appState,
        \Magento\Framework\UrlInterface $urlModel,
        \Pelago\Emogrifier $emogrifier,
        \Magento\Variable\Model\Source\Variables $configVariables,
        \Magento\Framework\Filter\Template\Tokenizer\Parameter $tokenizeParams,
        array $variables = []
    ){
        parent::__construct($string, $logger, $escaper, $assetRepo, $scopeConfig, $coreVariableFactory, $storeManager, $layout, $layoutFactory, $appState, $urlModel, $emogrifier, $configVariables, $variables);
        $this->tokenizeParams = $tokenizeParams;
    }

    /***
     * @return \Vnecoms\BannerManager\Model\Banner
     */
    public function getBanner()
    {
        if (!$this->_banner) {
            $this->_banner = \Magento\Framework\App\ObjectManager::getInstance()
                ->create('Vnecoms\BannerManager\Model\Banner');
        }

        return $this->_banner;
    }

    /**
     * Retrieve vendor block directive.
     *
     * @param array $construction
     *
     * @return string
     */
    public function bannerDirective($construction)
    {
        $skipParams = array('id');
        $this->tokenizeParams->setString($construction[2]);
        $bannerParameters = $this->tokenizeParams->tokenize();
        //var_dump($bannerParameters);die;
        $layout = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Framework\View\LayoutInterface');

        $banner = $layout->createBlock('Vnecoms\BannerManager\Block\Banner');
        if ($bannerParameters['banner_id'] && $banner) {
            $banner->setBannerFilterId($bannerParameters['banner_id']);
        }
        if ($bannerParameters['banner_id'] && $banner) {
            $banner->setBannerParams($bannerParameters);
            foreach ($bannerParameters as $k => $v) {
                if (in_array($k, $skipParams)) {
                    continue;
                }
                $banner->setDataUsingMethod($k, $v);
            }
        }
        if (!$banner) {
            return '';
        }

        return $banner->toHtml();
    }
}


