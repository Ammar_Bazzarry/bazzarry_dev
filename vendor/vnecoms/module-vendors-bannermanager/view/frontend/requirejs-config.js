/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*eslint no-unused-vars: 0*/
var config = {
    "shim": {
    },
    "map": {
          '*': {
              "swipe": "Vnecoms_BannerManager/js/swipe/swiper.min",
              "swipe.jquery": "Vnecoms_BannerManager/js/swipe/swiper.jquery.umd.min",
              "bannerWidget":  "Vnecoms_BannerManager/js/banner-widget"
          }
    }
};
