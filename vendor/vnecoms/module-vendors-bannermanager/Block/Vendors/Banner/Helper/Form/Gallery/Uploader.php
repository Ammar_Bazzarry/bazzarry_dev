<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\BannerManager\Block\Vendors\Banner\Helper\Form\Gallery;

/**
 * Adminhtml media library uploader
 */
class Uploader extends \Magento\Backend\Block\Media\Uploader
{
    /**
     * @var string
     */
    protected $_template = 'Vnecoms_BannerManager::media/uploader.phtml';
}
