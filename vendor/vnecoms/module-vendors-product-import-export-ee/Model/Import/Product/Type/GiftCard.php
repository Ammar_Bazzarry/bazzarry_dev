<?php
namespace Vnecoms\VendorsProductImportExportEE\Model\Import\Product\Type;

use Vnecoms\VendorsProductImportExport\Model\ResourceModel\Import\Data\Collection as ImportSource;
use Vnecoms\VendorsProductImportExport\Model\Import\Product\Type\TypeInterface;

class GiftCard extends \Magento\GiftCardImportExport\Model\Import\Product\Type\GiftCard implements TypeInterface
{
    /**
     * @var ImportSource
     */
    protected $_importSource;
    
    /**
     * Set Source
     *
     * @param ImportSource $source
     * @return \Vnecoms\VendorsProductImportExport\Model\Import\Product\Type\Bundle
     */
    public function setSource(ImportSource $source){
        $this->_importSource = $source;
        return $this;
    }
    
    /**
     * Get Source
     *
     * @return \Vnecoms\VendorsProductImportExport\Model\ResourceModel\Import\Data\Collection
     */
    public function getSource(){
        return $this->_importSource;
    }
}
