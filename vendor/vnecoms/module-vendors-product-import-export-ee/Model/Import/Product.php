<?php

namespace Vnecoms\VendorsProductImportExportEE\Model\Import;

use Magento\Framework\App\Filesystem\DirectoryList;
use Vnecoms\VendorsProductImportExport\Model\Import as Import;
use Magento\CatalogImportExport\Model\Import\Product\RowValidatorInterface as ValidatorInterface;
use Vnecoms\VendorsProductImportExport\Model\ResourceModel\Import\Data\Collection as ImportSource;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\Stdlib\DateTime;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;
use Magento\ImportExport\Model\Import\Entity\AbstractEntity;
use Magento\Catalog\Model\Config as CatalogConfig;
use Magento\Store\Model\Store;
use Vnecoms\VendorsProduct\Model\Source\Approval;

class Product extends \Vnecoms\VendorsProductImportExport\Model\Import\Product
{
    /**
     * Gather and save information about product entities.
     *
     * @param ImportSource $source
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function _saveVendorProducts(ImportSource $source)
    {
        $result = ['success' => [], 'error' => []];
        $priceIsGlobal = $this->_catalogData->isPriceGlobal();
        $productLimit = null;
        $productsQty = null;
    
        $entityRowsIn = [];
        $entityRowsUp = [];
        $attributes = [];
        $this->websitesCache = [];
        $this->categoriesCache = [];
        $tierPrices = [];
        $mediaGallery = [];
        $uploadedImages = [];
        $previousType = null;
        $prevAttributeSet = null;
        $existingImages = $this->_getExistingImages($source);
        $processedQueueIds = [];
        $sourceArr = [];
        $successMessage = [];
        $catalogConfig = \Magento\Framework\App\ObjectManager::getInstance()
        ->get(CatalogConfig::class);
    
        foreach ($source as $row) {
            $sourceArr[$row->getId()] = $row;
            if($row->getBehavior() != \Vnecoms\VendorsProductImportExport\Model\Import::BEHAVIOR_APPEND) continue;
    
            $rowData = json_decode($row->getProductData(), true);
            $rowData[self::COL_SKU] = $row->getSku();
            $rowData['vendor_id'] = $this->getVendor()->getId();
            $rowNum = $row->getQueueId();
            if (!$this->validateRow($rowData, $rowNum)) {
                continue;
            }
            /* if ($this->getErrorAggregator()->hasToBeTerminated()) {
             $this->getErrorAggregator()->addRowToSkip($rowNum);
             continue;
             } */
            $rowScope = $this->getRowScope($rowData);
    
            $rowSku = $rowData[self::COL_SKU];
    
            if (self::SCOPE_STORE == $rowScope) {
                // set necessary data from SCOPE_DEFAULT row
                $rowData[self::COL_TYPE] = $this->skuProcessor->getNewSku($rowSku)['type_id'];
                $rowData['attribute_set_id'] = $this->skuProcessor->getNewSku($rowSku)['attr_set_id'];
                $rowData[self::COL_ATTR_SET] = $this->skuProcessor->getNewSku($rowSku)['attr_set_code'];
            }
    
            // 1. Entity phase
            if ($this->isSkuExist($rowSku)) {
    
                if (isset($rowData['attribute_set_code'])) {
    
                    $attributeSetId = $catalogConfig->getAttributeSetId(
                        $this->getEntityTypeId(),
                        $rowData['attribute_set_code']
                    );
                     
                    // wrong attribute_set_code was received
                    if (!$attributeSetId) {
                        throw new \Magento\Framework\Exception\LocalizedException(
                            __(
                                'Wrong attribute set code "%1", please correct it and try again.',
                                $rowData['attribute_set_code']
                            )
                        );
                    }
                } else {
                    $attributeSetId = $this->skuProcessor->getNewSku($rowSku)['attr_set_id'];
                }
    
    
                $entityRowsUp[] = [
                    'updated_at' => (new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT),
                    'attribute_set_id' => $attributeSetId,
                    $this->getProductEntityLinkField() => $this->getExistingSku($rowSku)[$this->getProductEntityLinkField()]
                ];
    
            } else {
                if (!$productLimit || $productsQty < $productLimit) {
                    $entityRowsIn[$rowSku] = [
                        'attribute_set_id' => $this->skuProcessor->getNewSku($rowSku)['attr_set_id'],
                        'type_id' => $this->skuProcessor->getNewSku($rowSku)['type_id'],
                        'sku' => $rowSku,
                        'vendor_id' => $rowData['vendor_id'], /*Set the vendor id for imported item*/
                        'has_options' => isset($rowData['has_options']) ? $rowData['has_options'] : 0,
                        'created_at' => (new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT),
                        'updated_at' => (new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT),
                    ];
                    $productsQty++;
                } else {
                    $rowSku = null;
                    // sign for child rows to be skipped
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }
            }
    
            if (!array_key_exists($rowSku, $this->websitesCache)) {
                $this->websitesCache[$rowSku] = [];
            }
            // 2. Product-to-Website phase
            if (!empty($rowData[self::COL_PRODUCT_WEBSITES])) {
                $websiteCodes = explode($this->getMultipleValueSeparator(), $rowData[self::COL_PRODUCT_WEBSITES]);
                foreach ($websiteCodes as $websiteCode) {
                    $websiteId = $this->storeResolver->getWebsiteCodeToId($websiteCode);
                    $this->websitesCache[$rowSku][$websiteId] = true;
                }
            }
    
            // 3. Categories phase
            if (!array_key_exists($rowSku, $this->categoriesCache)) {
                $this->categoriesCache[$rowSku] = [];
            }
            $rowData['rowNum'] = $rowNum;
    
            /*-------------------------- don't add product if the category is not available-----------------------------------*/
            $categoryIds = $this->processRowCategories($rowData);
            /*------------------------------------------------------------------*/
    
            foreach ($categoryIds as $id) {
                $this->categoriesCache[$rowSku][$id] = true;
            }
            unset($rowData['rowNum']);
    
            // 4.1. Tier prices phase
            if (!empty($rowData['_tier_price_website'])) {
                $tierPrices[$rowSku][] = [
                    'all_groups' => $rowData['_tier_price_customer_group'] == self::VALUE_ALL,
                    'customer_group_id' => $rowData['_tier_price_customer_group'] ==
                    self::VALUE_ALL ? 0 : $rowData['_tier_price_customer_group'],
                    'qty' => $rowData['_tier_price_qty'],
                    'value' => $rowData['_tier_price_price'],
                    'website_id' => self::VALUE_ALL == $rowData['_tier_price_website'] ||
                    $priceIsGlobal ? 0 : $this->storeResolver->getWebsiteCodeToId($rowData['_tier_price_website']),
                ];
            }
    
            if (!$this->validateRow($rowData, $rowNum)) {
                continue;
            }
            $processedQueueIds[$row->getQueueId()] = $row->getQueueId();
    
            // 5. Media gallery phase
            $disabledImages = [];
            list($rowImages, $rowLabels) = $this->getImagesFromRow($rowData);
            $storeId = !empty($rowData[self::COL_STORE])
            ? $this->getStoreIdByCode($rowData[self::COL_STORE])
            : Store::DEFAULT_STORE_ID;
            if (isset($rowData['_media_is_disabled']) && strlen(trim($rowData['_media_is_disabled']))) {
                $disabledImages = array_flip(
                    explode($this->getMultipleValueSeparator(), $rowData['_media_is_disabled'])
                );
                if (empty($rowImages)) {
                    foreach (array_keys($disabledImages) as $disabledImage) {
                        $rowImages[self::COL_MEDIA_IMAGE][] = $disabledImage;
                    }
                }
            }
            $rowData[self::COL_MEDIA_IMAGE] = [];
    
            /*
             * Note: to avoid problems with undefined sorting, the value of media gallery items positions
             * must be unique in scope of one product.
            */
            $position = 0;
            foreach ($rowImages as $column => $columnImages) {
                foreach ($columnImages as $columnImageKey => $columnImage) {
                    if (!isset($uploadedImages[$columnImage])) {
                        $uploadedFile = $this->uploadMediaFiles($columnImage, true);
                        if ($uploadedFile) {
                            $uploadedImages[$columnImage] = $uploadedFile;
                        } else {
                            $this->addRowError(
                                ValidatorInterface::ERROR_MEDIA_URL_NOT_ACCESSIBLE,
                                $rowNum,
                                null,
                                null,
                                ProcessingError::ERROR_LEVEL_NOT_CRITICAL
                            );
                        }
                    } else {
                        $uploadedFile = $uploadedImages[$columnImage];
                    }
    
                    if ($uploadedFile && $column !== self::COL_MEDIA_IMAGE) {
                        $rowData[$column] = $uploadedFile;
                    }
    
                    if ($uploadedFile && !isset($mediaGallery[$storeId][$rowSku][$uploadedFile])) {
                        if (isset($existingImages[$rowSku][$uploadedFile])) {
                            if (isset($rowLabels[$column][$columnImageKey])
                                && $rowLabels[$column][$columnImageKey] != $existingImages[$rowSku][$uploadedFile]['label']
                            ) {
                                $labelsForUpdate[] = [
                                    'label' => $rowLabels[$column][$columnImageKey],
                                    'imageData' => $existingImages[$rowSku][$uploadedFile]
                                ];
                            }
                        } else {
                            if ($column == self::COL_MEDIA_IMAGE) {
                                $rowData[$column][] = $uploadedFile;
                            }
                            $mediaGallery[$storeId][$rowSku][$uploadedFile] = [
                                'attribute_id' => $this->getMediaGalleryAttributeId(),
                                'label' => isset($rowLabels[$column][$columnImageKey]) ? $rowLabels[$column][$columnImageKey] : '',
                                'position' => ++$position,
                                'disabled' => isset($disabledImages[$columnImage]) ? '1' : '0',
                                'value' => $uploadedFile,
                            ];
                        }
                    }
                }
            }
    
    
            // 6. Attributes phase
            $rowStore = (self::SCOPE_STORE == $rowScope)
            ? $this->storeResolver->getStoreCodeToId($rowData[self::COL_STORE])
            : 0;
            $productType = isset($rowData[self::COL_TYPE]) ? $rowData[self::COL_TYPE] : null;
            if (!is_null($productType)) {
                $previousType = $productType;
            }
            if (isset($rowData[self::COL_ATTR_SET])) {
                $prevAttributeSet = $rowData[self::COL_ATTR_SET];
            }
            if (self::SCOPE_NULL == $rowScope) {
                // for multiselect attributes only
                if (!is_null($prevAttributeSet)) {
                    $rowData[self::COL_ATTR_SET] = $prevAttributeSet;
                }
                if (is_null($productType) && !is_null($previousType)) {
                    $productType = $previousType;
                }
                if (is_null($productType)) {
                    continue;
                }
            }
    
            $productTypeModel = $this->_productTypeModels[$productType];
            if (!empty($rowData['tax_class_name'])) {
                $rowData['tax_class_id'] =
                $this->taxClassProcessor->upsertTaxClass($rowData['tax_class_name'], $productTypeModel);
            }
    
            if (empty($rowData[self::COL_SKU])) {
                $rowData = $productTypeModel->clearEmptyData($rowData);
            }
    
            $rowData = $productTypeModel->prepareAttributesWithDefaultValueForSave(
                $rowData,
                !$this->isSkuExist($rowSku)
            );
            $product = $this->_proxyProdFactory->create(['data' => $rowData]);
    
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            /** @var \Vnecoms\VendorsProduct\Helper\Data*/
            $helper = $om->get('Vnecoms\VendorsProduct\Helper\Data');
            /* Process product update*/
            if($this->isSkuExist($rowSku)){
                if($helper->isUpdateProductsApproval()){
                    if(!in_array(
                        $this->_vendorOldSkus[$rowSku]['approval'],
                        [Approval::STATUS_NOT_SUBMITED, Approval::STATUS_PENDING, Approval::STATUS_UNAPPROVED]
                    )){
                        $rowData = $this->processProductUdate($rowSku, $rowData);
                        $rowData['approval'] = Approval::STATUS_PENDING_UPDATE;
                    }
                }else{
                    $rowData['approval'] = Approval::STATUS_APPROVED;
                }
            }else{
                $rowData['approval'] = $helper->isNewProductsApproval()?
                    Approval::STATUS_PENDING:
                    Approval::STATUS_APPROVED;
            }
    
            foreach ($rowData as $attrCode => $attrValue) {
                $attribute = $this->retrieveAttributeByCode($attrCode);
    
                if ('multiselect' != $attribute->getFrontendInput() && self::SCOPE_NULL == $rowScope) {
                    // skip attribute processing for SCOPE_NULL rows
                    continue;
                }
                $attrId = $attribute->getId();
                $backModel = $attribute->getBackendModel();
                $attrTable = $attribute->getBackend()->getTable();
                $storeIds = [0];
    
                if (
                    'datetime' == $attribute->getBackendType()
                    && (
                        in_array($attribute->getAttributeCode(), $this->dateAttrCodes)
                        || $attribute->getIsUserDefined()
                    )
                ) {
                    $attrValue = $this->dateTime->formatDate($attrValue, false);
                } else if ('datetime' == $attribute->getBackendType() && strtotime($attrValue)) {
                    $attrValue = $this->dateTime->gmDate(
                        'Y-m-d H:i:s',
                        $this->_localeDate->date($attrValue)->getTimestamp()
                    );
                } elseif ($backModel) {
                    $attribute->getBackend()->beforeSave($product);
                    $attrValue = $product->getData($attribute->getAttributeCode());
                }
                if (self::SCOPE_STORE == $rowScope) {
                    if (self::SCOPE_WEBSITE == $attribute->getIsGlobal()) {
                        // check website defaults already set
                        if (!isset($attributes[$attrTable][$rowSku][$attrId][$rowStore])) {
                            $storeIds = $this->storeResolver->getStoreIdToWebsiteStoreIds($rowStore);
                        }
                    } elseif (self::SCOPE_STORE == $attribute->getIsGlobal()) {
                        $storeIds = [$rowStore];
                    }
                    if (!$this->isSkuExist($rowSku)) {
                        $storeIds[] = 0;
                    }
                }
                foreach ($storeIds as $storeId) {
                    if (!isset($attributes[$attrTable][$rowSku][$attrId][$storeId])) {
                        $attributes[$attrTable][$rowSku][$attrId][$storeId] = $attrValue;
                    }
                }
                // restore 'backend_model' to avoid 'default' setting
                $attribute->setBackendModel($backModel);
            }
    
            $successMessage[$row->getId()] = __('Item "%1" has been imported.', $row->getSku());
        }
    
        $errors = [];
        foreach($this->getErrorAggregator()->getAllErrors() as $error){
            $errors[$error->getRowNumber()][] = $error->getErrorMessage();
            $result['error'][] = __('Item "%1": %2',$sourceArr[$error->getRowNumber()]->getSku(),$error->getErrorMessage());
        }
    
        foreach($errors as $rowId=>$errs){
            unset($processedQueueIds[$rowId]);
            unset($successMessage[$rowId]);
            $sourceArr[$rowId]->setStatus(\Vnecoms\VendorsProductImportExport\Model\Import\Data::STATUS_ERROR)
            ->setErrorMsg(json_encode($errs))->save();
        }
    
    
        $this->transactionManager->start($this->_connection);
        try {
            $this->saveProductEntity(
                $entityRowsIn,
                $entityRowsUp
            )->_saveProductWebsites(
                $this->websitesCache
            )->_saveProductCategories(
                $this->categoriesCache
            )->_saveProductTierPrices(
                $tierPrices
            )->_saveMediaGallery(
                $mediaGallery
            )->_saveProductAttributes(
                $attributes
            );
            /*Delete processed queue items*/
            $this->objectRelationProcessor->delete(
                $this->transactionManager,
                $this->_connection,
                $source->getTable('ves_vendor_product_import_queue'),
                $this->_connection->quoteInto('queue_id IN (?)', $processedQueueIds),
                ['queue_id' => $processedQueueIds]
            );
    
            $this->transactionManager->commit();
    
            $result['success'] = array_merge($result['success'],array_values($successMessage));
        } catch (\Exception $e) {
            $this->transactionManager->rollBack();
            return ['success' => [],'error' => [$e->getMessage()]];
        }
         
        return $result;
    }
    
    /**
     * Get product entity link field
     *
     * @return string
     */
    private function getProductEntityLinkField()
    {
        if (!$this->_productEntityLinkField) {
            $this->_productEntityLinkField = $this->getMetadataPool()
            ->getMetadata(\Magento\Catalog\Api\Data\ProductInterface::class)
            ->getLinkField();
        }
        return $this->_productEntityLinkField;
    }
    /**
     * Get product entity identifier field
     *
     * @return string
     */
    private function getProductIdentifierField()
    {
        if (!$this->_productEntityIdentifierField) {
            $this->_productEntityIdentifierField = $this->getMetadataPool()
            ->getMetadata(\Magento\Catalog\Api\Data\ProductInterface::class)
            ->getIdentifierField();
        }
        return $this->_productEntityIdentifierField;
    }
    
    /**
     * Check if product exists for specified SKU
     *
     * @param string $sku
     * @return bool
     */
    private function isSkuExist($sku)
    {
        $sku = strtolower($sku);
        return isset($this->_oldSku[$sku]);
    }
    
    /**
     * Get existing product data for specified SKU
     *
     * @param string $sku
     * @return array
     */
    private function getExistingSku($sku)
    {
        return $this->_oldSku[strtolower($sku)];
    }
    
}
