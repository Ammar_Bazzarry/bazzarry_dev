<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Vnecoms\VendorsAttributeOption\Block\Adminhtml\Option;

use Vnecoms\VendorsAttributeOption\Model\Option;

/**
 * Vendor Notifications block
 */
class View extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;
    
    /**
     * Constructor
     * 
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }
    
    /**
     * Constructor
     *
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Vnecoms_VendorsAttributeOption';
        $this->_controller = 'adminhtml_option';
        $this->_mode = 'view';
    
        parent::_construct();
    
        $this->buttonList->remove('delete');
        $this->buttonList->remove('reset');
        $this->buttonList->remove('save');
    
        if ($this->canCancel($this->getOption())) {
            $this->addButton(
                'complete',
                [
                    'label' => __('Approve and Save the Attribute Option'),
                    'class' => 'save primary',
                    'onclick' => 'setLocation(\'' . $this->getCompleteUrl() . '\')',
                ],
                1
            );
            
            $this->buttonList->add(
                'cancel_request',
                [
                    'label' => __("Cancel Request"),
                    'onclick' => 'deleteConfirm(\'' . __(
                        'Are you sure you want to do this?'
                    ) . '\', \'' . $this->getCancelUrl() . '\')',
                    'class' => 'cancel cancel-request'
                ]
            );
        }
    
    }
    
    /**
     * Can cancel the requested option
     * 
     * @param Option $option
     * @return boolean
     */
    public function canCancel(Option $option){
        return $option->getStatus() == Option::STATUS_PENDING;
    }
    
    /**
     * Get current option
     *
     * @return \Vnecoms\VendorsAttributeOption\Model\Option
     */
    public function getOption(){
        return $this->_coreRegistry->registry('current_option');
    }
    
    /**
     * Get Back Url
     * 
     * @return string
     */
    public function getBackUrl(){
        return $this->getUrl('vendors/catalog_product_attribute/');
    }
    
    /**
     * Get Cancel URL
     * 
     * @return string
     */
    public function getCancelUrl(){
        return $this->getUrl('vendors/catalog_product_attribute/cancel',['id' => $this->getOption()->getId()]);
    }
    
    /**
     * Get Cancel URL
     *
     * @return string
     */
    public function getCompleteUrl(){
        return $this->getUrl('vendors/catalog_product_attribute/approve',['id' => $this->getOption()->getId()]);
    }
    
    /**
     * Getter for form header text
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        $option = $this->_coreRegistry->registry('current_option');
        return __("Option #%1", $this->escapeHtml($option->getId()));
    }
}
