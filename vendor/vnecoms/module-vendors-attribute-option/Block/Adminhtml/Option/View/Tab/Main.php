<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsAttributeOption\Block\Adminhtml\Option\View\Tab;

use Vnecoms\VendorsCredit\Model\Withdrawal;
use Magento\Backend\Block\Widget\Form;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Main extends Generic implements TabInterface
{

    /**
     * @var \Vnecoms\VendorsAttributeOption\Model\Source\Status
     */
    protected $_optionStatus;
    
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Vnecoms\VendorsAttributeOption\Model\Source\Status $optionStatus
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Vnecoms\VendorsAttributeOption\Model\Source\Status $optionStatus,
        array $data = []
    ) {
        $this->_optionStatus = $optionStatus;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare content for tab
     *
     * @return \Magento\Framework\Phrase
     * @codeCoverageIgnore
     */
    public function getTabLabel()
    {
        return __('Attribute Option');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     * @codeCoverageIgnore
     */
    public function getTabTitle()
    {
        return __('Attribute Option');
    }

    /**
     * Returns status flag about this tab can be showed or not
     *
     * @return bool
     * @codeCoverageIgnore
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return bool
     * @codeCoverageIgnore
     */
    public function isHidden()
    {
        return false;
    }

    
    /**
     * @return Form
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setUseContainer(true);
        $attribute = $this->getAttribute();
        
        $fieldset = $form->addFieldset('requested_attribute_option_form', ['legend' => __('Requested Option for Attribute: %1 (%2)', $attribute->getFrontendLabel(), $attribute->getAttributeCode())]);
        /*
        $fieldset->addType('price', 'Vnecoms\VendorsCredit\Block\Form\Element\Price'); */
        $fieldset->addType('status', 'Vnecoms\VendorsAttributeOption\Block\Form\Element\Status');
        $fieldset->addField(
            'attribute_code',
            'label',
            ['name' => 'attribute_code', 'label' => __('Attribute Code'), 'title' => __('Attribute Code'),]
        );
        $fieldset->addField(
            'option_value',
            'label',
            ['name' => 'option_value', 'label' => __('Requested Option Value'), 'title' => __('Requested Option Value'),]
        );
        $fieldset->addField(
            'comment',
            'label',
            ['name' => 'comment', 'label' => __('Comment'), 'title' => __('Comment'),]
        );
        $fieldset->addField(
            'status',
            'status',
            ['name' => 'status', 'label' => __('Status'), 'title' => __('Status'),]
        );
        $fieldset->addField(
            'created_at',
            'label',
            ['name' => 'created_at', 'label' => __('Created At'), 'title' => __('Created At'),]
        );
        
        $fieldset1 = $form->addFieldset('current_attribute_options', ['legend' => __('Current Options', $attribute->getFrontendLabel(), $attribute->getAttributeCode())]);
        $options = '<ul style="list-style: inside none disc;">';
        foreach ($attribute->getOptions() as $option) {
            $options .= "<li>".$option['label']."</li>";
        }
        $options .= '<ul>';
        $fieldset1->addField(
            'current_options',
            'note',
            ['name' => 'current_options', 'label' => __('Current Options'), 'title' => __('Current Options'),'text' => $options]
        );
        

        $option = $this->getOption();
        $values = $option->getData();
        $values['created_at'] = $this->formatDateTime($option->getCreatedAt(), \IntlDateFormatter::MEDIUM);
        $form->setValues($values);


        $this->setForm($form);

        return parent::_prepareForm();
    }
    
    /**
     * Format date time
     *
     * @param unknown $dateTime
     * @return string
     */
    public function formatDateTime($dateTime)
    {
        return $this->formatDate($dateTime, \IntlDateFormatter::MEDIUM).' '.$this->formatTime($dateTime, \IntlDateFormatter::MEDIUM);
    }
    
    /**
     * Get payment method title
     *
     * @return string
     */
    public function getPaymentMethodTitle()
    {
        return $this->_scopeConfig->getValue('withdrawal_methods/'.$this->getWithdrawal()->getMethod().'/title');
    }
    

    /**
     * Get current option
     *
     * @return \Vnecoms\VendorsAttributeOption\Model\Option
     */
    public function getOption()
    {
        return $this->_coreRegistry->registry('current_option');
    }
    
    /**
     * Get current option
     *
     * @return \Magento\Catalog\Model\Entity\Attribute
     */
    public function getAttribute()
    {
        return $this->_coreRegistry->registry('current_attribute');
    }
    
    /**
     * Format base currency
     *
     * @param float $amount
     * @return string
     */
    public function formatBaseCurrency($amount)
    {
        return $this->_storeManager->getStore()->getBaseCurrency()
        ->formatPrecision($amount, 2, [], false);
    }
    /**
     * Get Additional Info
     *
     * @return array
     */
    public function getAdditionalInfo()
    {
        $additionalInfo = $this->getWithdrawal()->getAdditionalInfo();
        return unserialize($additionalInfo);
    }
    
    /**
     * Get Withdrawal Status Label
     *
     * @return string
     */
    public function getStatusLabel()
    {
        $status = $this->_withdrawalStatus->getOptionArray();
        return $status[$this->getWithdrawal()->getStatus()];
    }
    
    /**
     * Get status html class
     *
     * @return string
     */
    public function getStatusHtmlClass()
    {
        switch ($this->getWithdrawal()->getStatus()) {
            case Withdrawal::STATUS_PENDING:
                return 'label-warning';
            case Withdrawal::STATUS_COMPLETED:
                return 'label-success';
            case Withdrawal::STATUS_CANCELED:
                return 'label-default';
                /* case Withdrawal::STATUS_REJECTED:
                 return 'label-danger'; */
        }
    }
}
