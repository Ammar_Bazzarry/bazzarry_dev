<?php
namespace Vnecoms\VendorsAttributeOption\Block\Form\Element;

use Vnecoms\VendorsAttributeOption\Model\Option;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\Data\Form\Element\CollectionFactory;
use Magento\Framework\Escaper;

class Status extends AbstractElement
{
    /**
     * @var \Vnecoms\VendorsAttributeOption\Model\Source\Status
     */
    protected $_optionStatus;
    
    /**
     * Constructor
     *
     * @param Factory $factoryElement
     * @param CollectionFactory $factoryCollection
     * @param Escaper $escaper
     * @param \Vnecoms\VendorsCredit\Model\Source\Status $withdrawalStatus
     * @param array $data
     */
    public function __construct(
        Factory $factoryElement,
        CollectionFactory $factoryCollection,
        Escaper $escaper,
        \Vnecoms\VendorsAttributeOption\Model\Source\Status $optionStatus,
        $data = []
    ) {
        $this->_optionStatus = $optionStatus;
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);
    }
    
    /**
     * Get Element HTML
     * @see \Magento\Framework\Data\Form\Element\AbstractElement::getElementHtml()
     */
    public function getElementHtml()
    {
        $status = $this->getValue();
        switch ($status) {
            case Option::STATUS_PENDING:
                return '<span class="vendor-status withdrawal-status vendor-status-pending">'.$this->getStatusLabel($status).'</span>';
            case Option::STATUS_APPROVED:
                return '<span class="vendor-status withdrawal-status vendor-status-approved">'.$this->getStatusLabel($status).'</span>';
            case Option::STATUS_UNAPPROVED:
                return '<span class="vendor-status withdrawal-status vendor-status-disabled">'.$this->getStatusLabel($status).'</span>';
        }
    }
    
    
/**
 * Get Withdrawal Status Label
 *
 * @return string
 */
    public function getStatusLabel($status)
    {
        $statusOptions = $this->_optionStatus->getOptionArray();
        return isset($statusOptions[$status])?$statusOptions[$status]:'';
    }
}
