<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsAttributeOption\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\DB\Helper as DbHelper;
use Magento\Framework\UrlInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magento\Ui\Component\Form\Element\DataType\Price;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Field;
use Vnecoms\VendorsAttributeOption\Helper\Data as OptionHelper;

/**
 * Data provider for categories field of product page
 */
class AttributeOption extends AbstractModifier
{


    /**
     * @var DbHelper
     */
    protected $dbHelper;


    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @var array
     */
    protected $meta = [];
    
    /**
     * @var \Vnecoms\VendorsAttributeOption\Helper\Data
     */
    protected $_optionHelper;
    
    /**
     *
     * @param LocatorInterface $locator
     * @param DbHelper $dbHelper
     * @param UrlInterface $urlBuilder
     * @param ArrayManager $arrayManager
     * @param OptionHelper $optionHelper
     */
    public function __construct(
        LocatorInterface $locator,
        DbHelper $dbHelper,
        UrlInterface $urlBuilder,
        ArrayManager $arrayManager,
        OptionHelper $optionHelper
    ) {
        $this->locator = $locator;
        $this->dbHelper = $dbHelper;
        $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
        $this->_optionHelper = $optionHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;
        
        $this->meta = $this->createNewAttributeOptionModal();
        
        $this->customizeAttributesField();
        return $this->meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * Customize tier price field
     *
     * @return $this
     */
    protected function customizeAttributesField()
    {
        $attributes = $this->_optionHelper->getAllowedAttributes();
        foreach ($attributes as $fieldCode) {
            $this->addOptionButton($fieldCode);
        }
        return $this;
    }
    
    /**
     * Create slide-out panel for new category creation
     *
     * @param array $meta
     * @return array
     */
    protected function createNewAttributeOptionModal()
    {
        return $this->arrayManager->set(
            'create_attribute_option_modal',
            $this->meta,
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'isTemplate' => false,
                            'componentType' => 'modal',
                            'options' => [
                                'title' => __('New Attribute Option'),
                            ],
                            'imports' => [
                                'state' => '!index=create_attribute_option:responseStatus'
                            ],
                           /*  'listens' => [
                                'index=create_attribute_option:responseStatus' => 'state'
                            ], */
                        ],
                    ],
                ],
                'children' => [
                    'create_attribute_option' => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'label' => '',
                                    'componentType' => 'container',
                                    'component' => 'Vnecoms_VendorsAttributeOption/js/components/insert-form',
                                    'dataScope' => '',
                                    'update_url' => $this->urlBuilder->getUrl('mui/index/render'),
                                    'render_url' => $this->urlBuilder->getUrl(
                                        'mui/index/render_handle',
                                        [
                                            'handle' => 'catalog_product_new_option',
                                            'store' => $this->locator->getStore()->getId(),
                                            'buttons' => 1,
                                        ]
                                    ),
                                    'autoRender' => false,
                                    'ns' => 'new_attribute_option',
                                    'externalProvider' => 'new_attribute_option.new_attribute_option_data_source',
                                    'toolbarContainer' => '${ $.parentName }',
                                    'formSubmitType' => 'ajax',
                                ],
                            ],
                        ]
                    ]
                ]
            ]
        );
    }
    
    /**
     * Get duration dynamic rows structure
     *
     * @param string $durationPath
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function addOptionButton($fieldCode)
    {
        $fieldPath = $this->arrayManager->findPath(
            $fieldCode,
            $this->meta,
            null,
            'children'
        );
        if (!$fieldPath) {
            return;
        }
        
        $fieldData = $this->arrayManager->get($fieldPath, $this->meta);
        if (array_key_exists('dataType', $fieldData['arguments']['data']['config'])
            && $fieldData['arguments']['data']['config']['dataType'] == 'multiselect') {
            $fieldData['arguments']['data']['config']['component'] = 'Vnecoms_VendorsAttributeOption/js/components/multiselect';
            $fieldData['arguments']['data']['config']['listens'] = [
                'index=create_attribute_option:responseData' => 'setParsed',
            ];
        } elseif (array_key_exists('dataType', $fieldData['arguments']['data']['config'])
            && $fieldData['arguments']['data']['config']['dataType'] == 'select') {
            $fieldData['arguments']['data']['config']['component'] = 'Vnecoms_VendorsAttributeOption/js/components/select';
            $fieldData['arguments']['data']['config']['listens'] = [
                'index=create_attribute_option:responseData' => 'setParsed',
            ];
        }

        $coreFieldPath = $this->arrayManager->get($fieldPath, $this->meta);
        $fieldDataReplace = array_replace($coreFieldPath, $fieldData);

        $this->meta = $this->arrayManager->set(
            $fieldPath,
            $this->meta,
            $fieldDataReplace
        );
        
        
        $attributeLabel = $this->arrayManager->get($fieldPath.'/arguments/data/config/label', $this->meta);
        $buttonData = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'title' => __('Add Option'),
                        'dataType' => 'button',
                        'formElement' => 'container',
                        'componentType' => 'container',
                        'additionalClasses' => 'admin__field-small',
                        'component' => 'Vnecoms_VendorsAttributeOption/js/components/button',
                        'template' => 'Vnecoms_VendorsAttributeOption/button',
                        'actions' => [
                            [
                                'targetName' => 'product_form.product_form.create_attribute_option_modal',
                                'actionName' => 'setTitle',
                                'params' => [
                                    __("Add new Option for Attribute: %1(%2)", $attributeLabel, $fieldCode)
                                ]
                            ],
                            [
                                'targetName' => 'product_form.product_form.create_attribute_option_modal', 'actionName' => 'toggleModal',
                            ],
                            [
                                'targetName' =>
                                'product_form.product_form.create_attribute_option_modal.create_attribute_option',
                                'actionName' => 'render',
                                'params' => [
                                    ['attribute' => $fieldCode]
                                ]
                            ],
                            [
                                'targetName' =>
                                'product_form.product_form.create_attribute_option_modal.create_attribute_option',
                                'actionName' => 'resetForm',
                                'params' => [
                                    $fieldCode
                                ]
                            ],
                        ],
                        'additionalForGroup' => true,
                        'provider' => false,
                        'source' => 'product_details',
                        'displayArea' => 'insideGroup',
                        'sortOrder' => 20,
                    ],
                ],
            ],
        ];
        
        $newFieldPath = $this->arrayManager->slicePath($fieldPath, 0, -1).'/'.$fieldCode.'_button';
        $this->meta = $this->arrayManager->set(
            $newFieldPath,
            $this->meta,
            $buttonData
        );
        
        $containerPath = $this->arrayManager->slicePath($fieldPath, 0, -2);
        $containerData = $this->arrayManager->get($containerPath, $this->meta);
        
        $newContainerData = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'container',
                        'componentType' => 'container',
                        'breakLine' => false,
                        'required' => false,
                        'additionalClasses' => 'attribute-option-container',
                        'component' => 'Magento_Ui/js/form/components/group',
                    ],
                ],
            ],
        ];
        $containerData = array_merge($containerData, $newContainerData);
        
        $this->meta = $this->arrayManager->set(
            $containerPath,
            $this->meta,
            $containerData
        );
    }
}
