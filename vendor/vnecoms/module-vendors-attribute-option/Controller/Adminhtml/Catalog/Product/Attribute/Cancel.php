<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsAttributeOption\Controller\Adminhtml\Catalog\Product\Attribute;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Vnecoms\VendorsAttributeOption\Model\ResourceModel\Option\CollectionFactory;
use Vnecoms\VendorsAttributeOption\Model\Option as AttributeOption;
use Vnecoms\VendorsAttributeOption\Helper\Data as OptionHelper;

/**
 * Class MassDelete
 */
class Cancel extends \Magento\Backend\App\Action
{
    /**
     * @var \Vnecoms\VendorsAttributeOption\Helper\Data
     */
    protected $_optionHelper;
    
    /**
     *
     * @param Context $context
     * @param OptionHelper $optionHelper
     */
    public function __construct(
        Context $context,
        OptionHelper $optionHelper
    ) {
        $this->_optionHelper = $optionHelper;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $option = $this->_objectManager->create('Vnecoms\VendorsAttributeOption\Model\Option');
        $option->load($this->getRequest()->getParam('id'));
        
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        
        if (!$option->getId()) {
            $this->messageManager->addError(__("The option does not exist."));
            return $resultRedirect->setPath('*/*/');
        }
        if ($option->getStatus() != AttributeOption::STATUS_PENDING) {
            $this->messageManager->addError(__("Option #%1 can not be changed status", $option->getId()));
            return $resultRedirect->setPath('*/*/');
        }

        $option->unApproved();
        
        /*Send notification to seller*/
        $this->_optionHelper->sendUnapprovedOptionNotification($option);

        $this->messageManager->addSuccess(__('The request has been cancelled.'));
        return $resultRedirect->setPath('*/*/');
    }
}
