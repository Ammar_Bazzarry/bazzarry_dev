<?php
/**
 *
 * Copyright � 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsAttributeOption\Controller\Adminhtml\Catalog\Product\Attribute;

class View extends \Magento\Catalog\Controller\Adminhtml\Product
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        parent::__construct($context, $productBuilder);
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
    }

    /**
     * Product list page
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $option = $this->_objectManager->create('Vnecoms\VendorsAttributeOption\Model\Option');
        $option->load($this->getRequest()->getParam('id'));
        if (!$option->getId()) {
            $this->messageManager->addMessage(__("The option does not exist."));
            return $this->_redirect('vendors/*');
        }
        $entityTypeId = $this->_objectManager->create(
            'Magento\Eav\Model\Entity'
        )->setType(
            \Magento\Catalog\Model\Product::ENTITY
        )->getTypeId();
        
        $model = $this->_objectManager->create(
            'Magento\Catalog\Model\Entity\Attribute'
        )->loadByCode(
            $entityTypeId,
            $option->getAttributeCode()
        );
        if (!$model->getId()) {
            $this->messageManager->addError(__('This attribute no longer exists.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('vendors/*/');
        }
        
        $this->_coreRegistry->register('current_option', $option);
        $this->_coreRegistry->register('option', $option);
        $this->_coreRegistry->register('current_attribute', $model);
        $this->_coreRegistry->register('attribute', $model);
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Vnecoms_Vendors::marketplace');
        $resultPage->getConfig()->getTitle()->prepend(__('Product Attribute Option Request'));
        return $resultPage;
    }
}
