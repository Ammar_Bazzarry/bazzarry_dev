<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsAttributeOption\Controller\Adminhtml\Catalog\Product\Attribute;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Vnecoms\VendorsAttributeOption\Model\ResourceModel\Option\CollectionFactory;
use Vnecoms\VendorsAttributeOption\Model\Option as AttributeOption;
use Vnecoms\VendorsAttributeOption\Helper\Data as OptionHelper;

/**
 * Class MassDelete
 */
class MassStatus extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Vnecoms\VendorsAttributeOption\Helper\Data
     */
    protected $_optionHelper;
    
    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        OptionHelper $optionHelper
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->_optionHelper = $optionHelper;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $count = 0;
        $status = $this->getRequest()->getParam('status');
        foreach ($collection as $option) {
            if ($option->getStatus() != AttributeOption::STATUS_PENDING) {
                $this->messageManager->addError(__("Option #%1 can not be changed status", $option->getId()));
                continue;
            }
            if ($status == AttributeOption::STATUS_APPROVED) {
                $option->approve();
                /*Send notification to seller*/
                $this->_optionHelper->sendApprovedOptionNotification($option);
            } elseif ($status == AttributeOption::STATUS_UNAPPROVED) {
                $option->unApproved();
                /*Send notification to seller*/
                $this->_optionHelper->sendUnapprovedOptionNotification($option);
            }
            
            $count ++;
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been updated.', $count));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
