<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsAttributeOption\Controller\Vendors\Product\Attribute;

use Vnecoms\VendorsAttributeOption\Model\Option as AttributeOption;

class Save extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var \Vnecoms\VendorsAttributeOption\Helper\Data
     */
    protected $_optionHelper;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $_connection;

    /**
     * @var Resource
     */
    protected $_resource;

    /**
     * Constructor
     *
     * @param \Vnecoms\Vendors\App\Action\Context $context
     * @param \Vnecoms\VendorsAttributeOption\Helper\Data $optionHelper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        \Vnecoms\Vendors\App\Action\Context $context,
        \Vnecoms\VendorsAttributeOption\Helper\Data $optionHelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->layoutFactory = $layoutFactory;
        $this->_optionHelper = $optionHelper;
        $this->_resource = $resource;
        parent::__construct($context);
    }

    /**
     * Retrieve write connection instance
     *
     * @return bool|\Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected function _getConnection()
    {
        if (null === $this->_connection) {
            $this->_connection = $this->_resource->getConnection();
        }
        return $this->_connection;
    }

    /**
     * Default customer account page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $attributeCode = $this->getRequest()->getParam('attribute');
        $optionLabel = $this->getRequest()->getParam('option');
        $comment = $this->getRequest()->getParam('comment');
        $hasError = false;
        $vendor = $this->_session->getVendor();
        $approvedGroups = $this->_optionHelper->getApprovedGroups();
        $isApprovedSeller = in_array($vendor->getGroupId(), $approvedGroups);
        $result = [
            'is_approved_seller' => $isApprovedSeller,
            'attribute' => $attributeCode,
        ];

        try {
            /* Save the option to attribute option table*/
            $option = $this->_objectManager->create('Vnecoms\VendorsAttributeOption\Model\Option');
            $option->setData([
                'vendor_id' => $vendor->getId(),
                'attribute_code' => $attributeCode,
                'option_value' => $optionLabel,
                'comment' => $comment,
                'status' => $isApprovedSeller?AttributeOption::STATUS_APPROVED:AttributeOption::STATUS_PENDING,
            ])->save();

            /* Add option to the attribute*/
            if ($isApprovedSeller) {
                /** @var \Magento\Catalog\Setup\CategorySetup $categorySetup */
                $attributeInfo = $this->_objectManager->get(\Magento\Eav\Model\Entity\Attribute::class)
                    ->loadByCode(\Magento\Catalog\Model\Product::ENTITY, $attributeCode);
                $attributeId = $attributeInfo->getAttributeId();
                $connection = $this->_getConnection();
                $optionTable = $this->_resource->getTableName('eav_attribute_option');
                $optionValueTable = $this->_resource->getTableName('eav_attribute_option_value');

                $data = ['attribute_id' => $attributeId, 'sort_order' => 0];
                $connection->insert($optionTable, $data);
                $intOptionId = $connection->lastInsertId($optionTable);

                $data = ['option_id' => $intOptionId, 'store_id' => 0, 'value' => $optionLabel];
                $connection->insert($optionValueTable, $data);
                $result['option'] = [
                    'option_id' => $intOptionId,
                    'label' => $optionLabel,
                ];
                $this->messageManager->addSuccess(__("Your product attribute option is submitted and awaiting for approval."));
            }
        } catch (\Exception $e) {
            $hasError = true;
            $this->messageManager->addError($e->getMessage());
        }

        $result['error'] = $hasError;

        /** @var $block \Magento\Framework\View\Element\Messages */
        $block = $this->layoutFactory->create()->getMessagesBlock();
        $block->setMessages($this->messageManager->getMessages(true));

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData(
            $result
        );
    }
}
