<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsAttributeOption\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const XML_ALLOWED_ATTRIBUTES = 'vendors/attribute_option/allowed_attributes';
    const XML_APPROVED_SELLER_GROUPS = 'vendors/attribute_option/approved_groups';
    
    const XML_APPROVED_NOTIFICATION_EMAIL = 'vendors/attribute_option/approved_notification';
    const XML_UNAPPROVED_NOTIFICATION_EMAIL = 'vendors/attribute_option/unapproved_notification';
    const XML_PATH_EMAIL_SENDER = 'vendors/attribute_option/sender_email_identity';

    /**
     * @var \Vnecoms\Vendors\Helper\Email
     */
    protected $_emailHelper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_localeDate;
    
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Vnecoms\Vendors\Helper\Email $emailHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Vnecoms\Vendors\Helper\Email $emailHelper,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
    ) {
        parent::__construct($context);
        $this->_emailHelper = $emailHelper;
        $this->_localeDate = $localeDate;
    }
    
    
    /**
     * Get ALlowed Attributes
     *
     * @return array
     */
    public function getAllowedAttributes()
    {
        return explode(
            ",",
            $this->scopeConfig->getValue(self::XML_ALLOWED_ATTRIBUTES)
        );
    }
    
    /**
     * Get approved seller groups
     *
     * @return array:
     */
    public function getApprovedGroups()
    {
        return explode(
            ",",
            $this->scopeConfig->getValue(self::XML_APPROVED_SELLER_GROUPS)
        );
    }
    
    /**
     * Send new approved option notification email to seller
     *
     * @param \Vnecoms\VendorsAttributeOption\Model\Option $option
     */
    public function sendApprovedOptionNotification(
        \Vnecoms\VendorsAttributeOption\Model\Option $option
    ) {
        $vendor = $option->getVendor();
        
        $option->setData('created_at_formated', $this->_localeDate->formatDateTime(
            $option->getCreatedAt(),
            \IntlDateFormatter::MEDIUM,
            \IntlDateFormatter::MEDIUM
        ));
        $this->_emailHelper->sendTransactionEmail(
            self::XML_APPROVED_NOTIFICATION_EMAIL,
            \Magento\Framework\App\Area::AREA_FRONTEND,
            self::XML_PATH_EMAIL_SENDER,
            $vendor->getEmail(),
            ['option' => $option, 'vendor' => $vendor]
        );
    }
    
    /**
     * Send new approved option notification email to seller
     *
     * @param \Vnecoms\VendorsAttributeOption\Model\Option $option
     */
    public function sendUnapprovedOptionNotification(
        \Vnecoms\VendorsAttributeOption\Model\Option $option
    ) {
        $vendor = $option->getVendor();
        $option->setData('created_at_formated', $this->_localeDate->formatDateTime(
            $option->getCreatedAt(),
            \IntlDateFormatter::MEDIUM,
            \IntlDateFormatter::MEDIUM
        ));
        $this->_emailHelper->sendTransactionEmail(
            self::XML_UNAPPROVED_NOTIFICATION_EMAIL,
            \Magento\Framework\App\Area::AREA_FRONTEND,
            self::XML_PATH_EMAIL_SENDER,
            $vendor->getEmail(),
            ['option' => $option, 'vendor' => $vendor]
        );
    }
}
