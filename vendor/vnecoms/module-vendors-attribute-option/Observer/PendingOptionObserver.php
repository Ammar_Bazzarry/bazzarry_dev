<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsAttributeOption\Observer;

use Magento\Framework\Event\ObserverInterface;
use Vnecoms\VendorsAttributeOption\Model\ResourceModel\Option\CollectionFactory;

/**
 * AdminNotification observer
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class PendingOptionObserver implements ObserverInterface
{
    /**
     * Url Builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;
    
    /**
     * Vendor collection
     * @var \Vnecoms\Vendors\Model\ResourceModel\Vendor\Collection
     */
    protected $_optionCollection;
    
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;
    
    /**
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        \Magento\Framework\View\Element\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_urlBuilder = $context->getUrlBuilder();
        $this->_optionCollection = $collectionFactory->create();
        $this->_optionCollection->addFieldToFilter('status', \Vnecoms\VendorsAttributeOption\Model\Option::STATUS_PENDING);
        $this->objectManager = $objectManager;
    }
    
    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_urlBuilder->getUrl($route, $params);
    }
    
    /**
     * Get number of pending vendor
     * @return number
     */
    public function getNumberOfPendingOption()
    {
        return $this->_optionCollection->count();
    }
    
    /**
     * Add the notification if there are any vendor awaiting for approval.
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $optionCount    = $this->getNumberOfPendingOption();
        if ($optionCount <= 0) {
            return;
        }
        
        $transport      = $observer->getTransport();
        $notifications  = $transport->getNotifications();
        $notification   = $this->objectManager->create('Magento\Framework\DataObject');

        $notification->setData([
            'title'=> __("Requested Product Attribute Options"),
            'description' => $optionCount ==1?__("There is an attribute option awaiting for your approval.<br /><a href=\"%1\">Click here</a> to review the product.", $this->getUrl('vendors/catalog_product_attribute/index')):
                __('There are <strong style="color: #ef672f">%1</strong> attribute options awaiting for your approval.<br /><a href="%2">Click here</a> to review the options.', $optionCount, $this->getUrl('vendors/catalog_product_attribute/index')),
        ]);
        
        $notifications[] = $notification;
        $transport->setNotifications($notifications);
    }
}
