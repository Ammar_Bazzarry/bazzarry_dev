<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsAttributeOption\Component;

/**
 * Class Form
 */
class Form extends \Magento\Ui\Component\Form
{
    /**
     * {@inheritdoc}
     */
    public function getDataSourceData()
    {
        $dataSource = [
            'data' => [
                'attribute' => $this->getContext()->getRequestParam('attribute')
            ]
        ];

        return $dataSource;
    }
}
