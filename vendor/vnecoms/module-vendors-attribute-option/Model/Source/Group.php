<?php
namespace Vnecoms\VendorsAttributeOption\Model\Source;

class Group extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Options array
     *
     * @var array
     */
    protected $_options = null;
    
    /**
     * @var \Vnecoms\Vendors\Model\ResourceModel\Group\CollectionFactory
     */
    protected $collectionFactory;
    
    /**
     * @param \Vnecoms\Vendors\Model\ResourceModel\Group\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Vnecoms\Vendors\Model\ResourceModel\Group\CollectionFactory $collectionFactory
    ){
        $this->collectionFactory = $collectionFactory;
    }
    
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [];
            $vendorGroupCollection = $this->collectionFactory->create();
            foreach ($vendorGroupCollection as $group) {
                $this->_options[] = ['label' => $group->getVendorGroupCode(), 'value' => $group->getId()];
            }
        }
        return $this->_options;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_options = [];
        foreach ($this->getAllOptions() as $option) {
            $_options[$option['value']] = $option['label'];
        }
        return $_options;
    }
    
    
    /**
     * Get options as array
     *
     * @return array
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
