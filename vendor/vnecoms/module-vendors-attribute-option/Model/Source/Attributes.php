<?php

namespace Vnecoms\VendorsAttributeOption\Model\Source;

class Attributes extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    protected $collectionFactory;
    
    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
    ){
        $this->collectionFactory = $collectionFactory;
    }
    
    /**
     * Options array
     *
     * @var array
     */
    protected $_options = null;
    
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [];
  
            $attributeCollection = $this->collectionFactory->create();
            $attributeCollection->addFieldToFilter('frontend_input', ['in' => ['select', 'multiselect']])
                ->addFieldToFilter('is_user_defined', 1)
                ->addVisibleFilter();
           // $attributeCollection->getSelect()->where('source_model is NULL');
            foreach ($attributeCollection as $attribute) {
                $this->_options[] = ['label' => $attribute->getAttributeCode(), 'value' => $attribute->getAttributeCode()];
            }
        }
        return $this->_options;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_options = [];
        foreach ($this->getAllOptions() as $option) {
            $_options[$option['value']] = $option['label'];
        }
        return $_options;
    }
    
    
    /**
     * Get options as array
     *
     * @return array
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
