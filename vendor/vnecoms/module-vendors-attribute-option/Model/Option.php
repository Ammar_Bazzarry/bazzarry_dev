<?php
namespace Vnecoms\VendorsAttributeOption\Model;

class Option extends \Magento\Framework\Model\AbstractModel
{

    const ENTITY = 'vendor_attribute_option';
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_UNAPPROVED = 2;
    
    /**
     * Model event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'vendor_attribute_option';
    
    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'vendor_attribute_option';

    /**
     * @var \Vnecoms\Vendors\Model\Vendor
     */
    protected $_vendor;
    
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;
    
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Vnecoms\VendorsAttributeOption\Model\ResourceModel\Option $resource,
        \Vnecoms\VendorsAttributeOption\Model\ResourceModel\Option\Collection $resourceCollection,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->objectManager = $objectManager;
    }
    
    /**
     * Initialize customer model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Vnecoms\VendorsAttributeOption\Model\ResourceModel\Option');
    }

    /**
     * Get Vendor
     *
     * @return \Vnecoms\Vendors\Model\Vendor
     */
    public function getVendor()
    {
        if (!$this->_vendor) {
            $this->_vendor = $this->objectManager->create('Vnecoms\Vendors\Model\Vendor');
            $this->_vendor->load($this->getVendorId());
        }
        
        return $this->_vendor;
    }
    
    /**
     * Approve the option
     *
     * @throws \Exception
     * @return \Vnecoms\VendorsAttributeOption\Model\Option
     */
    public function approve()
    {
        if ($this->getStatus() != self::STATUS_PENDING) {
            throw new \Exception(__("Only pending option can be approved"));
        }
        
        /* Add option to product attribute*/
        /** @var \Magento\Catalog\Setup\CategorySetup $categorySetup */
        $categorySetup = $this->objectManager->create('Magento\Catalog\Setup\CategorySetup');
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeId = $categorySetup->getAttributeId($entityTypeId, $this->getAttributeCode());
        
        $setup = $categorySetup->getSetup();
        $optionTable = $setup->getTable('eav_attribute_option');
        $optionValueTable = $setup->getTable('eav_attribute_option_value');
        
        $data = ['attribute_id' => $attributeId, 'sort_order' => 0];
        $setup->getConnection()->insert($optionTable, $data);
        $intOptionId = $setup->getConnection()->lastInsertId($optionTable);
        
        $data = ['option_id' => $intOptionId, 'store_id' => 0, 'value' => $this->getOptionValue()];
        $setup->getConnection()->insert($optionValueTable, $data);
        
        $this->setStatus(self::STATUS_APPROVED)->save();
        return $this;
    }
    
    /**
     * Unapprove the option
     *
     * @throws \Exception
     * @return \Vnecoms\VendorsAttributeOption\Model\Option
     */
    public function unApproved()
    {
        if ($this->getStatus() != self::STATUS_PENDING) {
            throw new \Exception(__("Only pending option can be unapproved"));
        }
        
        $this->setStatus(self::STATUS_UNAPPROVED)->save();
        
        return $this;
    }
}
