/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'underscore',
    'mageUtils',
    'Magento_Ui/js/form/element/multiselect'
], function (_, utils, MultiSelect) {
    'use strict';

    return MultiSelect.extend({
        /**
         * Parse data and set it to options.
         *
         * @param {Object} data - Response data object.
         * @returns {Object}
         */
        setParsed: function (data) {
            if (this.code != data.attribute) {
return; }
            if (!data.is_approved_seller) {
return; }
            
            var value = this.value();
            var options = this.options();
            
            var option = this.parseData(data);
            options.push(option);
            this.setOptions(options);
            this.set('newOption', option);
            
            value.push(option.value);
            this.value(value);
        },

        /**
         * Normalize option object.
         *
         * @param {Object} data - Option object.
         * @returns {Object}
         */
        parseData: function (data) {
            return {
                value: data.option.option_id,
                label: data.option.label,
                labeltitle: data.option.label
            };
        }
    });
});
