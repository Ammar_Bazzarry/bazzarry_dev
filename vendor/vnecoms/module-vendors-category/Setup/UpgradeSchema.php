<?php
/**
 * *
 *  * UpgradeSchema.php
 *  *
 *  * @file        UpgradeSchema.php
 *  * @copyright   Copyright (c) 2017 Vnecoms (https://www.vnecoms.com)
 *  * @site        https://www.vnecoms.com/
 *  * @author      Vnecoms <support@vnecoms.com>
 *  * @author      HiepNT <hiepnt@vnecoms.com>
 *
 */

namespace Vnecoms\VendorsCategory\Setup;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
    public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '2.1.0.1') < 0) {
            $table = $setup->getConnection()->addColumn(
                $setup->getTable('ves_vendorscategory_category'),
                'is_active',
                'int(6) unsigned NOT NULL DEFAULT 1 AFTER status'
            );
        }
        if (version_compare($context->getVersion(), '2.1.0.2') < 0) {
            $table = $setup->getConnection()->addColumn(
                $setup->getTable('catalog_product_entity'),
                'ves_category_ids',
                'TEXT NULL'
            );
        }
        if (version_compare($context->getVersion(), '2.1.0.3') < 0) {
            /*
             * Create table 'ves_vendorscategory_category_product'
             */
            $table = $setup->getConnection()
                ->newTable($setup->getTable('ves_vendorscategory_category_product'))
                ->addColumn(
                    'entity_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'nullable' => false, 'primary' => true],
                    'Entity ID'
                )
                ->addColumn(
                    'category_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'primary' => true, 'default' => '0'],
                    'Category ID'
                )
                ->addColumn(
                    'product_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'primary' => true, 'default' => '0'],
                    'Product ID'
                )
                ->addColumn(
                    'position',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false, 'default' => '0'],
                    'Position'
                )
                ->addIndex(
                    $setup->getIdxName($setup->getTable('ves_vendorscategory_category_product'), ['product_id']),
                    ['product_id']
                )
                ->addIndex(
                    $setup->getIdxName(
                        $setup->getTable('ves_vendorscategory_category_product'),
                        ['category_id', 'product_id'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['category_id', 'product_id'],
                    ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
                )
                ->addForeignKey(
                    $setup->getFkName($setup->getTable('ves_vendorscategory_category_product'), 'product_id', 'catalog_product_entity', 'entity_id'),
                    'product_id',
                    $setup->getTable('catalog_product_entity'),
                    'entity_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                )
                ->setComment('Catalog Product To Category Linkage Table');
            $setup->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '2.1.0.4') < 0) {
            $table = $setup->getConnection()->addColumn(
                $setup->getTable('ves_vendorscategory_category'),
                'included_in_menu',
                'int(6) unsigned NOT NULL DEFAULT 1 AFTER is_hide_product'
            );
        }

        if (version_compare($context->getVersion(), '2.1.0.5') < 0) {
            $table = $setup->getConnection()->addColumn(
                $setup->getTable('ves_vendorscategory_category'),
                'url_path',
                'TEXT NULL AFTER url_key'
            );
        }
        $setup->endSetup();
    }
}
