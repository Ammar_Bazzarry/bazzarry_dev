<?php

namespace Vnecoms\VendorsCategory\Setup;

use Magento\Catalog\Model\Product;

class UpgradeData implements \Magento\Framework\Setup\UpgradeDataInterface
{
    protected $collectionFactory;
    /**
     * EAV setup factory.
     *
     * @var EavSetupFactory
     */
    private $categorySetupFactory;

    private $categoryCollection;


    public function __construct(
        \Vnecoms\Vendors\Model\ResourceModel\Vendor\CollectionFactory $collectionFactory,
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Vnecoms\VendorsCategory\Model\ResourceModel\Category\CollectionFactory $factory
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->categoryCollection = $factory;
    }

    public function upgrade(
        \Magento\Framework\Setup\ModuleDataSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        //add attribute vendors_category for product
        /* @var CustomerSetup $customerSetup */
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        //add initialize data
        if (version_compare($context->getVersion(), '2.1.0.1') < 0) {
            $vendors = $this->collectionFactory->create();
            $installData = [];

            if ($vendors->count() > 0) {
                foreach ($vendors as $vendor) {
                    $select = $setup->getConnection()->select()->from(
                        $setup->getTable('ves_vendorscategory_category'),
                        ['category_id']
                    )->where(
                        'level = :level'
                    )->where(
                        'vendor_id = :vendor_id'
                    );
                    $bind = ['level' => 0, 'vendor_id' => $vendor->getId()];

                    if ($setup->getConnection()->fetchOne($select, $bind) == false) {
                        $installData[] = [
                            'vendor_id' => $vendor->getId(),
                            'real_category_id' => $vendor->getId(),
                            'name' => __('Root'),
                            'level' => 0,
                            'path' => '',
                            'is_hide_product' => 0,
                            'position' => $vendor->getId(),
                            'parent_id' => 0,
                            'status' => 1,
                            'is_active' => 1,
                            'included_in_menu' => 1,
                            'description' => __('Root'),
                            'children_count' => 0,
                            //  'created_at' => now(),
                            //   'updated_at' => now(),
                        ];
                    }
                }

                if (count($installData) > 0) {
                    $setup->getConnection()->insertArray(
                        $setup->getTable('ves_vendorscategory_category'),
                        ['vendor_id', 'real_category_id', 'name', 'level', 'path', 'is_hide_product', 'position', 'parent_id', 'status', 'is_active',
                            'included_in_menu', 'description', 'children_count',],
                        $installData
                    );
                }

                $collection = $this->categoryCollection->create()->addFieldToFilter('level', 0);

                foreach ($collection as $cat) {
                    $cat->setPath($cat->getId())->save();
                }
            }
        }
        if (version_compare($context->getVersion(), '2.1.0.2') < 0) {
            //add ves_category_ids attribute

            $categorySetup->addAttribute(
                Product::ENTITY,
                'ves_category_ids',
                [
                    'group' => 'Product Details',
                    'label' => 'Vendor Categories',
                    'type' => 'static',
                    'backend' => 'Vnecoms\VendorsCategory\Model\Product\Entity\Attribute\Backend\Category',
                    'input' => 'text',
                    'position' => 200,
                    'visible' => true,
                    'default' => '',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true, // for test, must change to false in production
                    //'source' => 'Vnecoms\VendorsProduct\Model\Source\Approval',
                    'default' => '',
                    'visible_on_front' => false,
                    'unique' => false,
                    'is_configurable' => false,
                    'used_for_promo_rules' => false,
                    'is_used_in_grid' => false,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'used_in_product_listing' => false,
                ]
            );
        }

        $setup->endSetup();
    }
}
