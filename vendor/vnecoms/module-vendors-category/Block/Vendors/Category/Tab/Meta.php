<?php

namespace Vnecoms\VendorsCategory\Block\Vendors\Category\Tab;

class Meta extends \Vnecoms\Vendors\Block\Vendors\Widget\Form\Generic implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Source\Yesno $yesno,
        array $data = []
    ) {
        $this->yesno = $yesno;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /* @var $model \Magefan\Blog\Model\Category */
        $model = $this->_coreRegistry->registry('current_category');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $fieldset = $form->addFieldset(
            'meta_fieldset',
            ['legend' => __('Search Engine Optimization')]
        );

      //  var_dump($model->getLevel());die();
        if ($model->getLevel() == 0) {
            $fieldset->addField(
                'url_key',
                'hidden',
                [
                    'name' => 'url_key',
                ]
            );
        } else {
            $urlKeyField = $fieldset->addField(
                'url_key',
                'text',
                [
                    'name' => 'url_key',
                    'label' => __('URL Key'),
                    'title' => __('URL Key'),
                ]
            );
            $renderer = $this->getLayout()->createBlock(
                'Vnecoms\VendorsCategory\Block\Vendors\Category\Form\Renderer\Fieldset\UrlKeyRenderer'
            );
            $urlKeyField->setRenderer($renderer);
        }

        $fieldset->addField(
            'meta_title',
            'text',
            [
                'name' => 'meta_title',
                'label' => __('Meta Title'),
                'title' => __('Meta Title'),
            ]
        );

        $fieldset->addField(
            'meta_keywords',
            'textarea',
            [
                'name' => 'meta_keywords',
                'label' => __('Keywords'),
                'title' => __('Meta Keywords'),
                'style' => 'width:100%; height:300px;',
            ]
        );

        $fieldset->addField(
            'meta_description',
            'textarea',
            [
                'name' => 'meta_description',
                'label' => __('Meta Description'),
                'title' => __('Meta Description'),
                'style' => 'width:100%; height:300px;',
            ]
        );

        $this->_eventManager->dispatch('vendors_category_edit_tab_meta_prepare_form', ['form' => $form]);

        $form->setValues($model->getData());

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab.
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Meta Data');
    }

    /**
     * Prepare title for tab.
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Meta Data');
    }

    /**
     * Returns status flag about this tab can be shown or not.
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not.
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action.
     *
     * @param string $resourceId
     *
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
