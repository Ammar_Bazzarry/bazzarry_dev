<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Product in category grid.
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */

namespace Vnecoms\VendorsCategory\Block\Vendors\Category\Tab;

class ProductNew extends \Magento\Framework\View\Element\Text\ListText
{
    public function __construct(\Magento\Framework\View\Element\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }
}
