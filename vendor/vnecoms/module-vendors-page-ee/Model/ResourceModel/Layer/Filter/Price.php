<?php

namespace Vnecoms\VendorsPageEE\Model\ResourceModel\Layer\Filter;

class Price extends \Vnecoms\VendorsLayerNavigation\Model\ResourceModel\Layer\Filter\Price
{
    /**
     * Retrieve clean select with joined price index table
     *
     * @return \Magento\Framework\DB\Select
     */
    protected function _getSelect()
    {
        $select = parent::_getSelect();
        $wherePart = $select->getPart(\Magento\Framework\DB\Select::WHERE);
        foreach($wherePart as $index=>$where){
            if(
                (strpos($where, 'created_in') !== false) ||
                (strpos($where, 'updated_in') !== false)
            ) {
                unset($wherePart[$index]);
            }
        }
        $wherePart = array_values($wherePart);
        $wherePart[0] = trim($wherePart[0], 'AND');
        $select->reset(\Magento\Framework\DB\Select::WHERE);
        $select->setPart(\Magento\Framework\DB\Select::WHERE, $wherePart);
        
        $fromPart = $select->getPart(\Magento\Framework\DB\Select::FROM);

		foreach($fromPart as $key=>$from){
			if(!isset($from['joinCondition'])) continue;
			$joinCondition = $from['joinCondition'];
			$joinCondition = preg_replace('/`(.*?)`.`*row_id`*\s*=\s*`*e`*.`*row_id`*/i', '`$1`.`row_id` = `e`.`entity_id`', $joinCondition);
			$fromPart[$key]['joinCondition'] = $joinCondition;
		}
        $select->reset(\Magento\Framework\DB\Select::FROM);
        $select->setPart(\Magento\Framework\DB\Select::FROM, $fromPart);

        return $select;
    }
}
