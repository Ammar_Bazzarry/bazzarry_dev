<?php

namespace Vnecoms\VendorsPriceComparison\Observer;

use Magento\Framework\Event\ObserverInterface;
use Vnecoms\VendorsProduct\Helper\Data as ProductHelper;
use Vnecoms\VendorsPriceComparison\Helper\Data as PriceComparisonHelper;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Vnecoms\VendorsPriceComparison\Model\Source\Product\Main;
use \Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;

class ProductDeleteAfter implements ObserverInterface
{
    /**
     * @var \Vnecoms\VendorsPriceComparison\Helper\Data
     */
    protected $_priceComparisonHelper;
    
    /**
     * @var \Vnecoms\VendorsProduct\Helper\Data
     */
    protected $_productHelper;
    
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_collectionFactory;

    
    /**
     * @param ProductHelper $productHelper
     * @param PriceComparisonHelper $priceComparisonHelper
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        ProductHelper $productHelper,
        PriceComparisonHelper $priceComparisonHelper,
        CollectionFactory $collectionFactory
    ) {
        $this->_priceComparisonHelper   = $priceComparisonHelper;
        $this->_productHelper           = $productHelper;
        $this->_collectionFactory       = $collectionFactory;
    }
    
    /**
     * Save product data for all child products
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return self
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product*/
        $product = $observer->getProduct();
        /* If the product is not main product, just return*/
        if($product->getData('select_from_product_id')) return;
        
        $collection = $this->_collectionFactory->create()->addAttributeToSelect('price')
            ->addAttributeToFilter('select_from_product_id', $product->getId())
            ->setOrder('entity_id', 'ASC');
        
        if(!$collection->count()) return;
        
        if($this->_priceComparisonHelper->getMainProductType() == Main::TYPE_FIRST_VENDOR){
            $this->updateFirstVendorProduct($collection);
        }
        
        if($this->_priceComparisonHelper->getMainProductType() == Main::TYPE_LOWEST_PRICE){
            $this->updateLowestPriceVendorProduct($collection);
        }
        
    }
    
    /**
     * Update main product is lowest price product
     *
     * @param ProductCollection $collection
     */
    protected function updateLowestPriceVendorProduct(ProductCollection $collection){
        $mainProduct = null;
        /* Find lowest price product*/
        foreach($collection as $product){
            if(
                !$mainProduct ||
                ($mainProduct->getPrice() > $product->getPrice())
            ) {
                $mainProduct = $product;
                continue;
            }
        }
        
        $this->setSelectFromProductId($mainProduct, 0);
        
        foreach($collection as $product){
            if($product->getId() == $mainProduct->getId()) continue;
            
            $this->setSelectFromProductId($product, $mainProduct->getId());
        }
    }
    
    /**
     * Update main product is first vendor who create the product
     * 
     * @param ProductCollection $collection
     */
    protected function updateFirstVendorProduct(ProductCollection $collection){
        $mainProduct = null;
        foreach($collection as $product){
            if(!$mainProduct){
                $mainProduct = $product;
                $this->setSelectFromProductId($mainProduct, 0);
                continue;
            }
            
            $this->setSelectFromProductId($product, $mainProduct->getId());
        }
    }
    
    /**
     * set vale for select_from_product_id attribute
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param int|null $value
     */
    protected function setSelectFromProductId(\Magento\Catalog\Model\Product $product, $value){
        $product->setData('select_from_product_id', $value)
            ->getResource()
            ->saveAttribute($product, 'select_from_product_id');
    }
}
