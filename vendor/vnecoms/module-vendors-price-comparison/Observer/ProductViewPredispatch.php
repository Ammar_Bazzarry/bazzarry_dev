<?php

namespace Vnecoms\VendorsPriceComparison\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProductViewPredispatch implements ObserverInterface
{
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;
    
    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;
    
    /**
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     */
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\App\Response\RedirectInterface $redirect
    ){
        $this->productFactory   = $productFactory;
        $this->redirect         = $redirect;
    }
    
    
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $controllerAction = $observer->getControllerAction();
        $request = $observer->getRequest();
        
        $productId = $request->getParam('id');
        $product = $this->productFactory->create()->load($productId);
        if(!$product->getId() || !$product->getData('select_from_product_id')) return;
        
        $mainProductId = $product->getData('select_from_product_id');
        $mainProduct = $this->productFactory->create()->load($mainProductId);
        if(!$mainProduct->getId()) return;
        /* $this->redirect->success($mainProduct->getProductUrl()); */
        $controllerAction->getResponse()->setRedirect($mainProduct->getProductUrl());
        $request->setDispatched(true);
        $controllerAction->getActionFlag()->set('', \Magento\Framework\App\ActionInterface::FLAG_NO_DISPATCH, true);
    }
}
