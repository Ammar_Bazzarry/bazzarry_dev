<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsPriceComparison\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\Config;
use Vnecoms\Vendors\Model\VendorFactory;
use Vnecoms\VendorsConfig\Helper\Data as ConfigHelper;
use Magento\Framework\App\Filesystem\DirectoryList;
use Vnecoms\VendorsProduct\Helper\Data as ProductHelper;
use Vnecoms\Vendors\Helper\Data as VendorHelper;
use Vnecoms\VendorsPriceComparison\Helper\Data as PriceComparisonHelper;

class Vendor extends \Magento\Framework\View\Element\Template
{
    /**
     * @var array
     */
    protected $jsLayout;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    
    /**
     * @var array|\Magento\Checkout\Block\Checkout\LayoutProcessorInterface[]
     */
    protected $layoutProcessors;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;
    
    /**
     * @var \Magento\Catalog\Model\Config
     */
    protected $catalogConfig;
    
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected $_productCollection;
    
    /**
     * Catalog product visibility
     *
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $productVisibility;
    
    /**
     * @var \Vnecoms\Vendors\Model\VendorFactory
     */
    protected $_vendorFactory;
    
    /**
     * @var ConfigHelper
     */
    protected $_configHelper;
    
    /**
     * @var \Magento\MediaStorage\Helper\File\Storage\Database
     */
    protected $_fileStorageDatabase;
    
    /**
     * @var \Magento\Framework\Filesystem\Directory\ReadInterface
     */
    protected $mediaDirectory;
    
    /**
     * @var \Vnecoms\Vendors\Helper\Image
     */
    protected $_imageHelper;
    
    /**
     * @var \Vnecoms\VendorsProduct\Helper\Data
     */
    protected $_productHelper;
    
    /**
     * @var \Vnecoms\Vendors\Helper\Data
     */
    protected $_vendorHelper;
    
    /**
     * @var \Magento\Cms\Model\Template\Filter
     */
    protected $_filter;
    
    /**
     * @var \Vnecoms\VendorsSales\Model\ResourceModel\OrderFactory
     */
    protected $_orderResourceFactory;
    
    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    protected $_cartHelper;
    
    /**
     * @var \Vnecoms\VendorsPriceComparison\Helper\Data
     */
    protected $_priceComparisonHelper;
    
    /**
     * @var \Magento\Framework\Url\EncoderInterface
     */
    protected $urlEncoder;
    
    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $formKey;


    /**
     * @var \Magento\Framework\Locale\FormatInterface
     */
    protected $_localeFormat;
    
    /**
     * The list of loaded vendors
     * @var array
     */
    protected $_vendors = [];
    
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        CollectionFactory $productCollectionFactory,
        Visibility $productVisibility,
        Config $catalogConfig,
        VendorFactory $vendorFactory,
        ConfigHelper $configHelper,
        \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageDatabase,
        \Vnecoms\Vendors\Helper\Image $imageHelper,
        ProductHelper $productHelper,
        VendorHelper $vendorHelper,
        PriceComparisonHelper $priceComparisonHelper,
        \Magento\Cms\Model\Template\Filter $filter,
        \Vnecoms\VendorsSales\Model\ResourceModel\OrderFactory $orderResourceFactory,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Data\Form\FormKey $formKey,
         \Magento\Framework\Locale\FormatInterface $localeFormat,
        array $layoutProcessors = [],
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->productVisibility = $productVisibility;
        $this->_coreRegistry = $coreRegistry;
        $this->catalogConfig = $catalogConfig;
        $this->_vendorFactory = $vendorFactory;
        $this->_configHelper = $configHelper;
        $this->_fileStorageDatabase = $fileStorageDatabase;
        $this->_mediaDirectory = $context->getFilesystem()->getDirectoryRead(DirectoryList::MEDIA);
        $this->_imageHelper = $imageHelper;
        $this->_productHelper = $productHelper;
        $this->_vendorHelper = $vendorHelper;
        $this->_priceComparisonHelper = $priceComparisonHelper;
        $this->_filter = $filter;
        $this->_orderResourceFactory = $orderResourceFactory;
        $this->_cartHelper = $cartHelper;
        $this->urlEncoder = $urlEncoder;
        $this->formKey = $formKey;
        $this->_localeFormat = $localeFormat;
        $this->jsLayout = isset($data['jsLayout']) && is_array($data['jsLayout']) ? $data['jsLayout'] : [];
        $this->layoutProcessors = $layoutProcessors;
    }
    
    /**
     * @return string
     */
    public function getJsLayout()
    {
        $this->jsLayout['components']['vendor']['products'] = $this->getProducts();
        $this->jsLayout['components']['vendor']['page_size'] = $this->_priceComparisonHelper->getShowingNumber();
        $this->jsLayout['components']['vendor']['show_country_filter'] = $this->_priceComparisonHelper->showCountryFilter();
        
        $this->jsLayout['components']['vendor']['children']['vendor_info']['config']['show_address'] = $this->_priceComparisonHelper->canShowAddress();
        $this->jsLayout['components']['vendor']['children']['vendor_info']['config']['show_sales_count'] = $this->_priceComparisonHelper->canShowSalesCount();
        $this->jsLayout['components']['vendor']['children']['vendor_info']['config']['show_joined_date'] = $this->_priceComparisonHelper->canShowJoinedDate();
        $this->jsLayout['components']['vendor']['children']['vendor_description']['config']['show_address'] = $this->_priceComparisonHelper->canShowAddress();
        $this->jsLayout['components']['vendor']['children']['vendor_description']['config']['show_sales_count'] = $this->_priceComparisonHelper->canShowSalesCount();
        $this->jsLayout['components']['vendor']['children']['vendor_description']['config']['show_joined_date'] = $this->_priceComparisonHelper->canShowJoinedDate();

        $this->jsLayout['components']['vendor']['children']['product_price']['priceFormat'] = $this->getPriceFormat();
        $this->jsLayout['components']['vendor']['children']['product_price']['basePriceFormat'] = $this->getBasePriceFormat();


        foreach ($this->layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }
        return \Zend_Json::encode($this->jsLayout);
    }
    
    /**
     * Get product collection
     * 
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getProductCollection(){
        if ($this->_productCollection === null) {
            $this->_productCollection = $this->_productCollectionFactory->create();
            $this->_productCollection->addAttributeToSelect('vendor_id')->addAttributeToFilter('select_from_product_id',$this->getProduct()->getId())
              ->addAttributeToFilter('approval',['in' => $this->_productHelper->getAllowedApprovalStatus()]);
            
            $this->_productCollection ->addAttributeToSelect($this->catalogConfig->getProductAttributes())
                ->addMinimalPrice()
               ->addFinalPrice()
                ->addTaxPercents()
                ->setVisibility($this->productVisibility->getVisibleInCatalogIds());
        }


        return $this->_productCollection;
    }
    
    /**
     * Get products
     * 
     * @return multitype:unknown
     */
    public function getProducts(){
        $products = [];
        foreach($this->getProductCollection() as $product){
            $vendorId = $product->getVendorId();
        
            if(!$vendorId) continue;
            
            $productData = $this->_prepareProductData($product);
            if($productData === false) continue;
            if($productData['pc_vendor']['status'] != \Vnecoms\Vendors\Model\Vendor::STATUS_APPROVED) continue;
            $products[] = $productData;
        }
                
        return $products;
    }
    
    /**
     * Prepare product data
     * 
     * @param \Magento\Catalog\Model\Product $product
     * @return boolean|Ambigous <\Magento\Framework\Model\mixed, multitype:>
     */
    protected function _prepareProductData(\Magento\Catalog\Model\Product $product){
        $vendorId = $product->getVendorId();
        if(!$vendorId) return false;
        if(!isset($this->_vendors[$vendorId])){
            $vendor = $this->_vendorFactory->create();
            $vendor->load($vendorId);
            if(!$vendor->getId()) return false;
        
            /* Add vendor home page URL if the homepage is installed*/
            if(class_exists('Vnecoms\VendorsPage\Helper\Data')){
                $om = \Magento\Framework\App\ObjectManager::getInstance();
                $helper = $om->create('Vnecoms\VendorsPage\Helper\Data');
                $vendor->setData('pc_home_page',$helper->getUrl($vendor));
            }
            $vendor->setData('pc_title',$this->_configHelper->getVendorConfig('general/store_information/name', $vendorId));
            $vendor->setData('pc_description',$this->_configHelper->getVendorConfig('general/store_information/short_description', $vendorId));
            $vendor->setData('pc_logo_url',$this->getLogoUrl($vendor));
            $vendor->setData('pc_logo_width',$this->getLogoWidth());
            $vendor->setData('pc_logo_height',$this->getLogoHeight());
            $vendor->setData('pc_address',$this->getAddress($vendor));
            $vendor->setData('pc_country_name', $vendor->getCountryName($this->_design->getLocale()));
            $vendor->setData('pc_sales_count',$this->getSalesCount($vendor));
            $vendor->setData(
                'pc_joined_date',
                $this->formatDate($vendor->getCreatedAt(),\IntlDateFormatter::MEDIUM)
            );
        
            $this->_vendors[$vendorId] = $vendor;
        }
        
        $productData = $product->getData();
        $productData['pc_product_url'] = $product->getProductUrl();
        $productData['pc_addtocart_url'] = $this->getAddToCartUrl($product);
        $productData['pc_vendor'] = $this->_vendors[$vendorId]->getData();
        
        return $productData;
    }
    
    /**
     * Keep Transparency logo
     *
     * @return boolean
     */
    public function keepTransparencyLogo(){
        return $this->getData('keep_transparency');
    }
    
    /**
     * Get logo width
     *
     * @return int
     */
    public function getLogoWidth(){
        $logoWidth = $this->getData('logo_width');
        return $logoWidth?$logoWidth:75;
    }
    
    /**
     * Get logo height
     *
     * @return int
     */
    public function getLogoHeight(){
        $logoHeight = $this->getData('logo_height');
        return $logoHeight?$logoHeight:75;
    }
    
    /**
     * Get Logo URL by vendor
     * 
     * @param \Vnecoms\Vendors\Model\Vendor $vendor
     * @return string
     */
    public function getLogoUrl(\Vnecoms\Vendors\Model\Vendor $vendor){
        $scopeConfig = $this->_configHelper->getVendorConfig(
            'general/store_information/logo',
            $vendor->getId()
        );
        $basePath = 'ves_vendors/logo/';
        $path =  $basePath. $scopeConfig;
    
    
        if ($scopeConfig && $this->checkIsFile($path)) {
            $this->_imageHelper->init($scopeConfig)
                ->setBaseMediaPath($basePath)
                ->keepTransparency($this->keepTransparencyLogo())
                ->backgroundColor([250,250,250])
                ->resize($this->getLogoWidth(),$this->getLogoHeight());
            return $this->_imageHelper->getUrl();
        }
    
        return $this->getViewFileUrl('Vnecoms_Vendors::images/no-logo.jpg');
    }
    
    /**
     * If DB file storage is on - find there, otherwise - just file_exists
     *
     * @param string $filename relative file path
     * @return bool
     */
    protected function checkIsFile($filename)
    {
        if ($this->_fileStorageDatabase->checkDbUsage() && !$this->_mediaDirectory->isFile($filename)) {
            $this->_fileStorageDatabase->saveFileToFilesystem($filename);
        }
        return $this->_mediaDirectory->isFile($filename);
    }
    
    /**
     * Get vendor address
     *
     * @param \Vnecoms\Vendors\Model\Vendor $vendor
     * @return string
     */
    public function getAddress(\Vnecoms\Vendors\Model\Vendor $vendor){
        $template = $this->_vendorHelper->getAddressTemplate();
        $country =  $vendor->getCountryName(
            $this->_design->getLocale()
        );
        $variables = [
            'street' => $vendor->getStreet(),
            'city' => $vendor->getCity(),
            'country' => $country,
            'region' => $vendor->getRegion(),
            'postcode' => $vendor->getPostcode(),
        ];
        return $this->_filter->setVariables($variables)->filter($template);
    }
    
    /**
     * Get sales count
     * 
     * @param \Vnecoms\Vendors\Model\Vendor $vendor
     * @return number
     */
    public function getSalesCount(\Vnecoms\Vendors\Model\Vendor $vendor){
        $resource = $this->_orderResourceFactory->create();
        return $resource->getSalesCount($vendor->getId());
    }
    
    /**
     * Retrieve url for direct adding product to cart
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param array $additional
     * @return string
     */
    public function getAddToCartUrl($product, $additional = [])
    {
        if ($this->getRequest()->getParam('wishlist_next')) {
            $additional['wishlist_next'] = 1;
        }
    
        $addUrlKey = \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED;
        $addUrlValue = $this->_urlBuilder->getUrl('*/*/*', ['_use_rewrite' => true, '_current' => true]);
        $additional[$addUrlKey] = $this->urlEncoder->encode($addUrlValue);
        $formKeyBlock = $this->_layout->getBlock('formkey');
        $additional['form_key'] = $this->formKey->getFormKey();
        return $this->_cartHelper->getAddUrl($product, $additional);
    }
    
    /**
     * Get current product
     * 
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct(){
        return $this->_coreRegistry->registry('product');
    }
    
    /**
     * (non-PHPdoc)
     * @see \Magento\Framework\View\Element\AbstractBlock::toHtml()
     */
    public function toHtml(){
        if(!$this->getProduct() || !$this->getProducts()) return '';
    
        return parent::toHtml();
    }

      /**
     * Get price format json.
     *
     * @return string
     */
    public function getPriceFormat()
    {
        return $this->_localeFormat->getPriceFormat();
    }
    
    /**
     * Get price format json.
     *
     * @return string
     */
    public function getBasePriceFormat()
    {
        return $this->_localeFormat->getPriceFormat(null, $this->_storeManager->getStore()->getBaseCurrencyCode());
    }
}
