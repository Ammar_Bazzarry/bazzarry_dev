/**
* Copyright © 2016 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
define([
    'uiComponent',
    'jquery',
    'mage/translate',
    'domReady!'
], function (Component, $, $t) {
    'use strict';

    return Component.extend({
    	defaults: {
			template: 'Vnecoms_VendorsPriceComparison/vendor',
			page_size: 5,
			is_show_all: false,
			show_country_filter: true,
			products: [],
			/*When these values are changed, the template that uses these variables will be changed too*/
            tracks: {
            	is_show_all: true,
            },
            availableCountries: null,
    	},
    	
        initialize: function () {
            this._super()
            .observe({
            	isLoading: false,
            	selectedCountry: '',
            });
        },
        /**
         * Is visible
         */
        visible: function(){
        	return this.totalProducts() > 0;
        },
        /**
         * Get total number of products
         */
        totalProducts: function(){
        	return this.products.length;
        },
        totalFilteredProducts: function(){
        	return this.getFilteredProducts().length;
        },
        /**
         * Get filtered products
         */
        getFilteredProducts: function(){
        	var self = this;
        	var result = [];
        	var products = this.products;
        	$.each(products, function(index, product){
        		if(!self.filterProduct(product)) return true;
        		result.push(product);
        	});
        	
        	return result;        	
        },
        /**
         * Get the list of products
         */
        getProducts: function(){
        	var self = this;
        	var products = this.getFilteredProducts();
        	products.sort(function(a,b){
        		return parseFloat(a.final_price) > parseFloat(b.final_price) ? 1:-1
        	});
        	
        	if(!this.is_show_all && this.canShowMore()){
        		var result = [];
        		var count = 0;
        		$.each(products, function(index, product){
        			if(count >= self.page_size) return false;
        			result.push(product);
        			count ++;
        		});
        		return result;
        	}

        	
        	return products;
        },
        /**
         * Filter product
         */
        filterProduct: function(product){
        	var selectedCountry = this.selectedCountry();
        	var filter1 = (selectedCountry=='' || (product.pc_vendor.country_id == selectedCountry));
        	return filter1;
        },
        /**
         * Get all available countries
         */
        getCountries: function(){
        	if(this.availableCountries === null){
	        	var countries = {};
	        	var countriesArr = [{code: '', country_name: $t('-- Select Country --')}];
	        	$.each(this.products,function(index, product){
	        		if(typeof(countries[product.pc_vendor.country_id]) == 'undefined'){
	        			countries[product.pc_vendor.country_id] = product.pc_vendor.pc_country_name;
	        			countriesArr.push({
	        				code: product.pc_vendor.country_id,
	        				country_name: product.pc_vendor.pc_country_name
	    				});
	        		}
	        	});
	        	
	        	countriesArr.sort(function(a,b){
	        		return a.code > b.code ? 1:-1
	        	});
	        	console.log(countriesArr);
	        	this.availableCountries = countriesArr;
        	}
        	
        	return this.availableCountries;
        },
        /**
         * Get country name by code.
         */
        getCountryByCode: function(countryCode){
        	var countries = this.getCountries();
        	var result = countryCode;
        	$.each(countries, function(index, country){
        		if(country.code == countryCode){
        			result = country.country_name;
        			return false;
        		}
        	});
        	return result;
        },
        /**
         * Unselect country
         */
        unselectCountry: function(){
        	this.selectedCountry('');
        },
        /**
         * Is showing all products
         */
        isShowingAll: function(){
        	return this.is_show_all;
        },
        /**
         * Can show Show More button
         */
        canShowMore: function(){
        	return this.totalFilteredProducts() > this.page_size;
        },
        /**
         * Toggle Show All
         */
        toggleShowAll: function(){
        	this.is_show_all = ! this.is_show_all;
        }
    });
});
