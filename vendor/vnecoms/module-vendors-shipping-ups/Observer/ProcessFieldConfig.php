<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsShippingUPS\Observer;

use Magento\Framework\Event\ObserverInterface;
use Vnecoms\VendorsConfig\Helper\Data;

class ProcessFieldConfig implements ObserverInterface
{
    /**
     * Add multiple vendor order row for each vendor.
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return self
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $transport = $observer->getTransport();
        $fieldset = $transport->getFieldset();

        $config = \Magento\Framework\App\ObjectManager::getInstance()->get(
            'Magento\Framework\App\Config\ScopeConfigInterface');

        $fieldsArray = [
            "shipping_method_ups_access_license_number",
            "shipping_method_ups_allowed_methods",
            "shipping_method_ups_free_shipping_enable",
            "shipping_method_ups_free_shipping_subtotal",
            "shipping_method_ups_free_method",
            "shipping_method_ups_origin_shipment",
            "shipping_method_ups_password",
            "shipping_method_ups_pickup",
            "shipping_method_ups_username",
            "shipping_method_ups_negotiated_active",
            "shipping_method_ups_shipper_number",
            "shipping_method_ups_sallowspecific",
            "shipping_method_ups_specificcountry",
        ];

        $fieldsArrayXML = [
            "shipping_method_ups_access_license_number",
            "shipping_method_ups_origin_shipment",
            "shipping_method_ups_password",
            "shipping_method_ups_username",
            "shipping_method_ups_negotiated_active",
            "shipping_method_ups_shipper_number",
        ];


        if($fieldset->getHtmlId() == "shipping_method_ups"){
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE;
            $configVal = $config->getValue("carriers/ups/active",$storeScope);
            $configShipping = $config->getValue("carriers/vendor_multirate/active",$storeScope);
            if(!$configVal || !$configShipping) $transport->setForceReturn(true);

            $configShipping = $config->getValue("carriers/ups/api_type",$storeScope);
            if($configShipping == \Vnecoms\VendorsShippingUPS\Model\Source\Type::USE_API_ADMIN){
                foreach ($fieldset->getElements() as $field) {
                    if(in_array($field->getHtmlId(),$fieldsArray))
                    $field->setIsRemoved(true);
                }
            }

            $configType = $config->getValue("carriers/ups/type",$storeScope);
            if($configType == 'UPS'){
                foreach ($fieldset->getElements() as $field) {
                    if(in_array($field->getHtmlId(),$fieldsArrayXML))
                        $field->setIsRemoved(true);
                }
            }
        }


        return $this;
    }
}
