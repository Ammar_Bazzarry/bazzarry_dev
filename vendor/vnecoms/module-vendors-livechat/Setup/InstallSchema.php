<?php

namespace Vnecoms\VendorsLiveChat\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'ves_vendor_livechat_visitor'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_vendor_livechat_visitor')
        )->addColumn(
            'visitor_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => true],
            'Id'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [ 'nullable' => true],
            'Name'
        )->addColumn(
            'email',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [ 'nullable' => true],
            'Email'
        )->addColumn(
            'telephone',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            16,
            [ 'nullable' => true],
            'Telephone'
        )->addColumn(
            'note',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [ 'nullable' => true],
            'Note'
        )->addColumn(
            'company',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [ 'nullable' => true, 'default' => ''],
            'Company'
        )->addColumn(
            'host',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [ 'nullable' => true,],
            'host name'
        )->addColumn(
            'ip',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [ 'nullable' => true,],
            'Ip'
        )->addColumn(
            'country',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            7,
            [ 'nullable' => true,],
            'country'
        )->addColumn(
            'city',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [ 'nullable' => true,],
            'City'
        )->addColumn(
            'region',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [ 'nullable' => true,],
            'Region'
        )->addColumn(
            'signature',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [ 'nullable' => false, 'default' => ''],
            'Signature'
        )->addColumn(
            'join_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Join Time'
        )->addColumn(
            'last_updated',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Last Visit'
        )->setComment(
            'Visitor'
        );
        $installer->getConnection()->createTable($table);
        
        /**
         * Create table 'ves_vendor_livechat_session'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_vendor_livechat_session')
        )->addColumn(
            'session_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['unsigned' => true, 'nullable' => false,],
            'Id'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        )->setComment(
            'Visitor'
        );
        $installer->getConnection()->createTable($table);
        
        /**
         * Create table 'ves_vendor_livechat_chat'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_vendor_livechat_chat')
        )->addColumn(
            'chat_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'session_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            255,
            ['unsigned' => true, 'nullable' => false,],
            'Session Id'
        )->addColumn(
            'visitor_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false,],
            'Id'
        )->addColumn(
            'vendor_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false,],
            'Vendor Id'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            [ 'nullable' => false, 'default' => 0],
            'Status'
        )->addColumn(
            'wait_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [ 'nullable' => false, 'default' => 0],
            'Wait time (seconds)'
        )->addColumn(
            'is_unread',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            null,
            [ 'nullable' => false, 'default' => 0],
            'Is Unread'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        )->addColumn(
            'updated_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Updated At'
        )->addForeignKey(
            $installer->getFkName('ves_vendor_livechat_chat', 'visitor_id', 'ves_vendor_livechat_visitor', 'visitor_id'),
            'visitor_id',
            $installer->getTable('ves_vendor_livechat_visitor'),
            'visitor_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('ves_vendor_livechat_chat', 'vendor_id', 'ves_vendor_entity', 'entity_id'),
            'vendor_id',
            $installer->getTable('ves_vendor_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('ves_vendor_livechat_chat', 'session_id', 'ves_vendor_livechat_session', 'session_id'),
            'session_id',
            $installer->getTable('ves_vendor_livechat_session'),
            'session_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Visitor'
        );
        $installer->getConnection()->createTable($table);
        
        /**
         * Create table 'ves_vendor_livechat_chat_action'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_vendor_livechat_chat_action')
        )->addColumn(
            'action_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'chat_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false,],
            'Chat Id'
        )->addColumn(
            'action',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            31,
            [ 'nullable' => true],
            'Action'
        )->addColumn(
            'execute_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_BIGINT,
            11,
            ['nullable' => false, 'unsigned' => true,],
            'Execute time'
        )->addForeignKey(
            $installer->getFkName('ves_vendor_livechat_chat_action', 'chat_id', 'ves_vendor_livechat_chat', 'chat_id'),
            'chat_id',
            $installer->getTable('ves_vendor_livechat_chat'),
            'chat_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Chat Action'
        );
        $installer->getConnection()->createTable($table);
        
        /**
         * Create table 'ves_vendor_livechat_chat_message'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_vendor_livechat_chat_message')
        )->addColumn(
            'message_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'chat_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false,],
            'Chat Id'
        )->addColumn(
            'sender_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => true,],
            'Sender Id'
        )->addColumn(
            'sender_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false,],
            'Sender Type(0 => system, 1 => visitor, 2 => operator)'
        )->addColumn(
            'sender_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['unsigned' => true, 'nullable' => true,],
            'Sender Name'
        )->addColumn(
            'message',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            \Magento\Framework\DB\Ddl\Table::MAX_TEXT_SIZE,
            ['nullable' => true,],
            'message'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        )->addForeignKey(
            $installer->getFkName('ves_vendor_livechat_chat_message', 'chat_id', 'ves_vendor_livechat_chat', 'chat_id'),
            'chat_id',
            $installer->getTable('ves_vendor_livechat_chat'),
            'chat_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Chat Message'
        );
        $installer->getConnection()->createTable($table);
        
        /**
         * Create table 'ves_vendor_livechat_chat_operator'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_vendor_livechat_chat_operator')
        )->addColumn(
            'chat_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Id'
        )->addColumn(
            'operator_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false,],
            'Operator Id'
        )/* ->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            [ 'nullable' => false, 'default' => 0],
            'Status'
        )->addColumn(
            'join_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [ 'nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Join Time'
        )->addColumn(
            'left_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => true],
            'Left Time'
        ) */
        ->addForeignKey(
            $installer->getFkName('ves_vendor_livechat_chat_operator', 'chat_id', 'ves_vendor_livechat_chat', 'chat_id'),
            'chat_id',
            $installer->getTable('ves_vendor_livechat_chat'),
            'chat_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
        ->addIndex(
            $installer->getIdxName(
                'ves_vendor_livechat_chat_operator',
                ['chat_id', 'operator_id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['chat_id', 'operator_id'],
            ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
        )->setComment(
            'Chat - Operator'
        );
        $installer->getConnection()->createTable($table);
        
        
        /**
         * Create table 'ves_vendor_livechat_browser_urls'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_vendor_livechat_browser_urls')
        )->addColumn(
            'url_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'session_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            255,
            ['unsigned' => true, 'nullable' => false,],
            'Session Id'
        )->addColumn(
            'visitor_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => true,],
            'Sender Id'
        )->addColumn(
            'vendor_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => true,],
            'Vendor Id'
        )->addColumn(
            'url',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['unsigned' => true, 'nullable' => false, 'default' => '' ],
            'Url'
        )->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['unsigned' => true, 'nullable' => true,],
            'Url Title'
        )->addColumn(
            'is_online',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            null,
            ['nullable' => false, 'default' => 0],
            'Is Online'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        )->addForeignKey(
            $installer->getFkName('ves_vendor_livechat_browser_urls', 'visitor_id', 'ves_vendor_livechat_visitor', 'visitor_id'),
            'visitor_id',
            $installer->getTable('ves_vendor_livechat_visitor'),
            'visitor_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('ves_vendor_livechat_browser_urls', 'session_id', 'ves_vendor_livechat_session', 'session_id'),
            'session_id',
            $installer->getTable('ves_vendor_livechat_session'),
            'session_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('ves_vendor_livechat_browser_urls', 'vendor_id', 'ves_vendor_entity', 'entity_id'),
            'vendor_id',
            $installer->getTable('ves_vendor_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Browser Url'
        );
        $installer->getConnection()->createTable($table);
        
        /**
         * Create table 'ves_vendor_livechat_chat_transcript'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ves_vendor_livechat_chat_transcript')
        )->addColumn(
            'transcript_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'chat_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false,],
            'Chat Id'
        )->addColumn(
            'email',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [ 'nullable' => true],
            'Action'
        )->addForeignKey(
            $installer->getFkName('ves_vendor_livechat_chat_transcript', 'chat_id', 'ves_vendor_livechat_chat', 'chat_id'),
            'chat_id',
            $installer->getTable('ves_vendor_livechat_chat'),
            'chat_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Chat Transcript'
        );
        $installer->getConnection()->createTable($table);
        
        
        /* Add new fields for operator*/
        $installer->getConnection()->addColumn(
            $setup->getTable('ves_vendor_user'),
            'livechat_nickname',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'size' => 255,
                'unsigned' => true,
                'nullable' => false,
                'default' => '',
                'after' => 'is_super_user',
                'comment' => 'Live Chat Nickname'
            ]
        );
        
        $installer->getConnection()->addColumn(
            $setup->getTable('ves_vendor_user'),
            'livechat_avatar',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'size' => 255,
                'unsigned' => true,
                'nullable' => false,
                'default' => '',
                'after' => 'livechat_nickname',
                'comment' => 'Avatar'
            ]
        );
        
        $installer->getConnection()->addColumn(
            $setup->getTable('ves_vendor_user'),
            'livechat_status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'unsigned' => true,
                'nullable' => false,
                'default' => '0',
                'after' => 'livechat_avatar',
                'comment' => 'Status'
            ]
        );
        
        $installer->getConnection()->addColumn(
            $setup->getTable('ves_vendor_user'),
            'livechat_token',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'size' => 255,
                'unsigned' => true,
                'nullable' => false,
                'default' => '',
                'after' => 'livechat_status',
                'comment' => 'Token'
            ]
        );
        
        $installer->endSetup();
    }
}
