<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsLiveChat\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Customer collection factory
     *
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    private $customerFactory;
 
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator
     */
    private $operator;
    
    /**
     * @param CollectionFactory $customerFactory
     * @param \Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator $operator
     */
    public function __construct(
        CollectionFactory $customerFactory,
        \Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator $operator
    ) {
        $this->customerFactory = $customerFactory;
        $this->operator = $operator;
    }
    
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        
        $userCollection = $this->customerFactory->create();
        $userCollection->getSelect()->joinRight(['vendor_user' => $userCollection->getTable('ves_vendor_user')],'customer_id=entity_id');
        
        /* Init User Nick Name*/
        $operators = [];
        foreach($userCollection as $user){
            $this->operator->updateOperatorData($user->getId(), 'livechat_nickname', $user->getName());
        }
        $setup->endSetup();
    }
}
