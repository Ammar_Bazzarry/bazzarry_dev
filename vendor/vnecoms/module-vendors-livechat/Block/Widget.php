<?php
namespace Vnecoms\VendorsLiveChat\Block;

use \Magento\Framework\View\Element\Template\Context;
use Magento\Framework\UrlInterface;

class Widget extends \Magento\Framework\View\Element\Template
{
    /**
     * @var array
     */
    protected $jsLayout;
    
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;
    
    /**
     * @var \Vnecoms\Vendors\Model\VendorFactory
     */
    protected $vendorFactory;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Cookie
     */
    protected $cookie;
    
    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Vnecoms\Vendors\Model\VendorFactory $vendorFactory
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Vnecoms\VendorsLiveChat\Helper\Cookie $cookie
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Vnecoms\Vendors\Model\VendorFactory $vendorFactory,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Vnecoms\VendorsLiveChat\Helper\Cookie $cookie,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry     = $coreRegistry;
        $this->vendorFactory    = $vendorFactory;
        $this->helper           = $helper;
        $this->cookie           = $cookie;
        
        $this->jsLayout = isset($data['jsLayout']) && is_array($data['jsLayout']) ? $data['jsLayout'] : [];
    }
    
    /**
     * (non-PHPdoc)
     * @see \Magento\Framework\View\Element\AbstractBlock::getJsLayout()
     */
    public function getJsLayout()
    {
        $this->jsLayout['components']['vendorslivechat']['cookieLifeTime']      = round($this->cookie->getCookielifetime() / 86400, 2);
        $this->jsLayout['components']['vendorslivechat']['cookiePath']          = $this->cookie->getCookiePath();
        $this->jsLayout['components']['vendorslivechat']['cookieDomain']        = $this->cookie->getCookieDomain();
        $this->jsLayout['components']['vendorslivechat']['debugMode']           = $this->helper->isDebugMode();
        
        $this->jsLayout['components']['vendorslivechat']['webSocketUrl']        = $this->helper->getWebSocketUrl();
        $this->jsLayout['components']['vendorslivechat']['notificationSound']   = $this->getNotificationSoundUrl();
        $this->jsLayout['components']['vendorslivechat']['initUrl']             = $this->getChatInitUrl();
        $this->jsLayout['components']['vendorslivechat']['sendOfflineMessageUrl']   = $this->getSendOfflineMessageUrl();
        $this->jsLayout['components']['vendorslivechat']['sendTranscriptUrl']       = $this->getSendTranscriptUrl();
        $this->jsLayout['components']['vendorslivechat']['baseMediaUrl']            = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]).$this->helper->getUploadFolder();
        
        if($this->getVendor()){
            $this->jsLayout['components']['vendorslivechat']['vendor_id']           = $this->getVendor()->getId();
        }
        $this->jsLayout['components']['vendorslivechat']['updateFrequency']         = 5000;
        $this->jsLayout['components']['vendorslivechat']['children']['uploader']['uploaderConfig'] =  [
            'url'               => $this->getUploadImageUrl(),
            'acceptFileTypes'   => explode(',',$this->helper->getAllowedExtensions()),
            'maxFileNumber'     => 1 //default 5 files
        ];
        
        return \Zend_Json::encode($this->jsLayout);
    }
    
    /**
     * @return string
     */
    public function getUploadImageUrl(){
        return $this->getUrl('vlivechat/index/upload');
    }
    
    /**
     * @return string
     */
    public function getSendTranscriptUrl(){
        return $this->getUrl('vlivechat/index/sendTranscript');
    }
    
    /**
     * @return string
     */
    public function getSendOfflineMessageUrl(){
        return $this->getUrl('vlivechat/index/sendOfflineMessage');
    }
    
    /**
     * @return string
     */
    public function getChatInitUrl(){
        return $this->getUrl('vlivechat/index/init');
    }
    
    /**
     * @return string
     */
    public function getChatStartUrl(){
        return $this->getUrl('vlivechat/index/start');
    }
    
    /**
     * @return string
     */
    public function getSendMessageUrl(){
        return $this->getUrl('vlivechat/index/send');
    }
    
    /**
     * @return string
     */
    public function getChatUpdateUrl(){
        return $this->getUrl('vlivechat/index/update');
    }
    
    /**
     * @return string
     */
    public function getNotificationSoundUrl(){
        return $this->getViewFileUrl('Vnecoms_VendorsLiveChat/sound/notification1.mp3');
    }
    
    /**
     * Get vendor object
     *
     * @return \Vnecoms\Vendors\Model\Vendor
     */
    public function getVendor()
    {
        $vendor = $this->coreRegistry->registry('vendor');
        if (!$vendor && $product = $this->coreRegistry->registry('product')) {
            if ($vendorId = $product->getVendorId()) {
                $vendor = $this->vendorFactory->create()->load($vendorId);
            }
        }
        return $vendor;
    }
}
