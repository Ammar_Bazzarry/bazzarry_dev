<?php
namespace Vnecoms\VendorsLiveChat\Block\Vendors\Chat;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;

class View extends \Vnecoms\Vendors\Block\Vendors\Widget\Form\Container
{
    /**
     * Block group
     *
     * @var string
     */
    protected $_blockGroup = 'Vnecoms_VendorsLiveChat';
    
    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @var \Magento\Backend\Block\Widget\Button\ButtonList
     */
    protected $buttonList;
    
    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        array $data = []
    ) {
       $this->coreRegistry  = $coreRegistry;
       $this->helper        = $helper;
       parent::__construct($context, $data); 
    }
    
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'vendors_chat';
        $this->_mode = 'view';
    
        parent::_construct();
        $this->removeButton('save');
        $this->removeButton('reset');
        $this->removeButton('delete');
        $this->addButton(
            'send_transcript',
            [
                'label' => __('Export Transcript'),
                'class' => 'btn-primary fa fa-cloud-download',
                'onclick' => "jQuery('#vlivechat-transcript-container').modal('openModal')"
            ]
        );
    }
    
    /**
     * @see \Magento\Backend\Block\Widget\Form\Container::getBackUrl()
     */
    public function getBackUrl(){
        return $this->getUrl('livechat/history');
    }
    /**
     * @return \Vnecoms\VendorsLiveChat\Model\Chat
     */
    public function getChat(){
        return $this->coreRegistry->registry('chat');
    }
    
    /**
     * @return \Vnecoms\VendorsLiveChat\Model\Visitor
     */
    public function getUser(){
        return $this->getChat()->getVisitor();
    }
    
    /**
     * @return array
     */
    public function getBrowserUrls(){
        return $this->getChat()->getBrowserUrls();
    }

    /**
     * Process Message
     *
     * @param string $message
     * @return string
     */
    public function processMessage($message){
        $isImageMessage = $this->isImageMessage($message);
        if($this->isAttachmentMessage($message)){
            $baseMediaUrl = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]).$this->helper->getUploadFolder();
            $matches = false;
            preg_match('/\[(.*?) \s*(.*?)\s*\]/', $message, $matches);
    
            $filePath = explode("/" ,$matches[2]);
            $fileName = end($filePath);
            $message = str_replace($matches[0], '<a href="'.$baseMediaUrl.$matches[2].'" target="_blank">'.$fileName.'</a>', $message);
        }elseif($this->isImageMessage($message)){
            $baseMediaUrl = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]).$this->helper->getUploadFolder();
            $matches = false;
            preg_match('/\[(.*?) \s*(.*?)\s*\]/', $message, $matches);
            
            $filePath = explode("/" ,$matches[2]);
            $fileName = end($filePath);
            $message = str_replace($matches[0], '<div class="vlivechat-img-container"><img src="'.$baseMediaUrl.$matches[2].'" /></div>', $message);
        }
        return $message;
    }
    
    /**
     * Is attachment message
     */
    public function isAttachmentMessage($message){
        return preg_match('/\[file\s*(.*?)\s*\]/', $message);
    }
    
    /**
     * Is image message
     */
    public function isImageMessage($message){
        return preg_match('/\[img\s*(.*?)\s*\]/', $message);
    }
    
    /**
     * @param \Vnecoms\VendorsLiveChat\Model\Message $message
     * @return boolean
     */
    public function isSystemMessage(\Vnecoms\VendorsLiveChat\Model\Message $message){
        return $message->getSenderType() == \Vnecoms\VendorsLiveChat\Model\Message::SENDER_TYPE_SYSTEM;
    }
    
    /**
     * @return string
     */
    public function getSendTranscriptUrl(){
        return $this->getUrl('livechat/index/sendTranscript');
    }
    
}
