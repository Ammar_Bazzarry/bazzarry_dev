<?php
namespace Vnecoms\VendorsLiveChat\Block\Vendors;

use \Magento\Framework\View\Element\Template\Context;
use Magento\Framework\UrlInterface;

class Widget extends \Magento\Framework\View\Element\Template
{
    /**
     * @var array
     */
    protected $jsLayout;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @var \Vnecoms\Vendors\Model\Session
     */
    protected $vendorSession;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator
     */
    protected $operator;
    
    /**
     * @param Context $context
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Vnecoms\Vendors\Model\Session $vendorSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Vnecoms\Vendors\Model\Session $vendorSession,
        \Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator $operator,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper           = $helper;
        $this->vendorSession    = $vendorSession;
        $this->operator         = $operator;
        
        $this->jsLayout = isset($data['jsLayout']) && is_array($data['jsLayout']) ? $data['jsLayout'] : [];
    }
    
    /**
     * (non-PHPdoc)
     * @see \Magento\Framework\View\Element\AbstractBlock::getJsLayout()
     */
    public function getJsLayout()
    {
        $this->jsLayout['components']['vendorslivechat']['debugMode']           = $this->helper->isDebugMode();
        $this->jsLayout['components']['vendorslivechat']['webSocketUrl']        = $this->helper->getWebSocketUrl();
        $this->jsLayout['components']['vendorslivechat']['notificationSound']   = $this->getNotificationSoundUrl();
        $this->jsLayout['components']['vendorslivechat']['token']               = $this->operator->getToken($this->getOperatorId());
        $this->jsLayout['components']['vendorslivechat']['updateFrequency']     = 5000;
        $this->jsLayout['components']['vendorslivechat']['operator']            = $this->operator->getOperatorData($this->getOperatorId());
        $this->jsLayout['components']['vendorslivechat']['saveNicknameUrl']     = $this->getSaveNicknameUrl();
        $this->jsLayout['components']['vendorslivechat']['saveVisitorInfoUrl']  = $this->getSaveVisitorInfoUrl();        
        $this->jsLayout['components']['vendorslivechat']['sendTranscriptUrl']   = $this->getSendTranscriptUrl();
        $this->jsLayout['components']['vendorslivechat']['baseMediaUrl']        = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]).$this->helper->getUploadFolder();
        $this->jsLayout['components']['vendorslivechat']['children']['uploader']['uploaderConfig'] =  [
            'url'               => $this->getUploadImageUrl(),
            'acceptFileTypes'   => explode(',',$this->helper->getAllowedExtensions()),
            'maxFileNumber'     => 1 //default 5 files
        ];
        return \Zend_Json::encode($this->jsLayout);
    }
    /**
     * @return string
     */
    public function getUploadImageUrl(){
        return $this->getUrl('livechat/index/upload');
    }
    
    /**
     * @return string
     */
    public function getSendTranscriptUrl(){
        return $this->getUrl('livechat/index/sendTranscript');
    }
    
    /**
     * @return string
     */
    public function getSaveVisitorInfoUrl(){
        return $this->getUrl('vlivechat/index/savevisitor');
    }
    
    /**
     * @return string
     */
    public function getSaveNicknameUrl(){
        return $this->getUrl('vlivechat/index/savenickname');
    }

    
    /**
     * @return string
     */
    public function getNotificationSoundUrl(){
        return $this->getViewFileUrl('Vnecoms_VendorsLiveChat/sound/definite.mp3');
    }
    
    /**
     * Get vendor object
     *
     * @return \Vnecoms\Vendors\Model\Vendor
     */
    public function getVendor()
    {
        return $this->vendorSession->getVendor();
    }
    
    /**
     * @return Ambigous <number, NULL>
     */
    public function getOperatorId(){
        return $this->vendorSession->getCustomerId();
    }
    
    /**
     * @see \Magento\Framework\View\Element\Template::_toHtml()
     */
    protected function _toHtml(){
        if(!$this->vendorSession->isLoggedIn()) return '';
        return parent::_toHtml();
    }
}
