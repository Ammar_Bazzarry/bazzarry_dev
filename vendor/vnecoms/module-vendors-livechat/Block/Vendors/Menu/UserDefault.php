<?php
namespace Vnecoms\VendorsLiveChat\Block\Vendors\Menu;

use Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator; 

class UserDefault extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Vnecoms\Vendors\Model\Session
     */
    protected $vendorSession;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator
     */
    protected $operator;
    
    /**
     * @var array
     */
    protected $operatorInfo;
    
    /**
     * @var array
     */
    protected $jsLayout;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Vnecoms\Vendors\Model\Session $vendorSession
     * @param Operator $operator
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Vnecoms\Vendors\Model\Session $vendorSession,
        Operator $operator,
        array $data = []
    ) {
        $this->vendorSession = $vendorSession;
        $this->operator = $operator;
        $this->helper = $helper;
        $this->jsLayout = isset($data['jsLayout']) && is_array($data['jsLayout']) ? $data['jsLayout'] : [];
        return parent::__construct($context, $data);
    }
    
    /**
     * (non-PHPdoc)
     * @see \Magento\Framework\View\Element\AbstractBlock::getJsLayout()
     */
    public function getJsLayout()
    {
        $this->jsLayout['components']['vendorslivechatuser']['debugMode']       = $this->helper->isDebugMode();
        $this->jsLayout['components']['vendorslivechatuser']['userInfoUrl']     = $this->getUserInfoUrl();
        $this->jsLayout['components']['vendorslivechatuser']['operator']            = $this->operator->getOperatorData($this->getCustomer()->getId());
    
        return \Zend_Json::encode($this->jsLayout);
    }

    /**
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer(){
        return $this->vendorSession->getCustomer();
    }
    
    /**
     * @return string
     */
    public function getUserInfoUrl(){
        return $this->getUrl('vlivechat/index/userInfo');
    }
}
