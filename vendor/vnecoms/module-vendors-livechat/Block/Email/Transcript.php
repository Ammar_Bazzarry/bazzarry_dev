<?php
namespace Vnecoms\VendorsLiveChat\Block\Email;

use \Magento\Framework\View\Element\Template\Context;
use Magento\Framework\UrlInterface;
use Vnecoms\VendorsLiveChat\Model\Message;

class Transcript extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @param Context $context
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper           = $helper;
    }
    
    /**
     * @param \Vnecoms\VendorsLiveChat\Model\Message $message
     * @return boolean
     */
    public function isSystemMessage(\Vnecoms\VendorsLiveChat\Model\Message $message){
        return $message->getSenderType() ==  Message::SENDER_TYPE_SYSTEM;
    }
    
    /**
     * @param \Vnecoms\VendorsLiveChat\Model\Message $message
     * @return boolean
     */
    public function isVisitorMessage(\Vnecoms\VendorsLiveChat\Model\Message $message){
        return $message->getSenderType() ==  Message::SENDER_TYPE_VISITOR;
    }
    
    /**
     * Process Message
     * 
     * @param string $message
     */
    public function processMessage($message){
        $isImageMessage = $this->isImageMessage($message);
        if(
            $isImageMessage ||
            $this->isAttachmentMessage($message)
        ){
            $baseMediaUrl = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]).$this->helper->getUploadFolder();
            $matches = false;
            preg_match('/\[(.*?) \s*(.*?)\s*\]/', $message, $matches);
            
            $filePath = explode("/" ,$matches[2]);
            $fileName = end($filePath);
            $message = str_replace($matches[0], '<a href="'.$baseMediaUrl.$matches[2].'">'.$fileName.'</a>', $message);
        }
        return $message;
    }
    
    /**
     * Is attachment message
     */
    public function isAttachmentMessage($message){
        return preg_match('/\[file\s*(.*?)\s*\]/', $message);
    }
    
    /**
     * Is image message
    */
    public function isImageMessage($message){
        return preg_match('/\[img\s*(.*?)\s*\]/', $message);
    }
}
