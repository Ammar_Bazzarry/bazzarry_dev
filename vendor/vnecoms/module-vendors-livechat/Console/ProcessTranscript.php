<?php

namespace Vnecoms\VendorsLiveChat\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessTranscript extends Command
{
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\$transcriptCollectionFactory\CollectionFactory
     */
    protected $transcriptCollectionFactory;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $datetime;
    
    /**
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Vnecoms\VendorsLiveChat\Model\ResourceModel\Transcript\CollectionFactory $transcriptCollectionFactory
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $datetime
     * @param string $name
     */
    public function __construct(
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Vnecoms\VendorsLiveChat\Model\ResourceModel\Transcript\CollectionFactory $transcriptCollectionFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime,
        $name = null
    ){
        $this->helper                       = $helper;
        $this->transcriptCollectionFactory  = $transcriptCollectionFactory;
        $this->objectManager                = $objectManager;
        $this->datetime                     = $datetime;
        parent::__construct($name);
    }
    
    /**
     * Configure the command
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('vlivechat:process-transcript');
        $this->setDescription('Send chat transcript to recipient(s).');
    }

    /**
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \Magento\Framework\App\State $state */
        $state = $this->objectManager->get('Magento\Framework\App\State');
        
        $state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
        /** @var \Magento\Framework\ObjectManager\ConfigLoaderInterface $configLoader */
        $configLoader = $this->objectManager->get('Magento\Framework\ObjectManager\ConfigLoaderInterface');
        
        $this->objectManager->configure($configLoader->load(\Magento\Framework\App\Area::AREA_FRONTEND));
        /** @var \Magento\Framework\Event\ManagerInterface $eventManager */
        $eventManager = $this->objectManager->get('Magento\Framework\Event\ManagerInterface');
        $eventManager->dispatch('default');
        /* Fix problem with URL. As the queue is processed from bin/magento, not from index.php so the URL will be incorrect*/
        $_SERVER['SCRIPT_FILENAME'] = 'index.php';

        $transcriptCollection = $this->transcriptCollectionFactory->create();
        $transcriptCollection->join(
            ['livechat_chat' => $transcriptCollection->getTable('ves_vendor_livechat_chat')],
            'main_table.chat_id = livechat_chat.chat_id',
            $cols = '*'
        );
        $transcriptCollection->addFieldToFilter('status',\Vnecoms\VendorsLiveChat\Model\Chat::STATUS_CLOSED)
            ->setPageSize(100);
        
        foreach($transcriptCollection as $transcript){
            $this->helper->sendTranscript($transcript->getChat(), $transcript->getEmail());
            $transcript->delete();
        }
    }
}
