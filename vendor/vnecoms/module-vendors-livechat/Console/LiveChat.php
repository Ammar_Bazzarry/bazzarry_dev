<?php

namespace Vnecoms\VendorsLiveChat\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Workerman\Worker;
use PHPSocketIO\SocketIO;
use Vnecoms\VendorsLiveChat\Model\Message;
use Magento\Framework\Exception\LocalizedException;
use Vnecoms\VendorsLiveChat\Model\Chat;
use Vnecoms\VendorsLiveChat\Model\ChatAction;
use Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator;

class LiveChat extends Command
{
    const EVENT_ERROR			                = 'error';
    const EVENT_VISITOR_CHAT_INIT               = 'visitor chat init';
    const EVENT_VISITOR_CHAT_UPDATE             = 'visitor chat update';
    const EVENT_VISITOR_CHAT_START              = 'visitor chat start';
    const EVENT_VISITOR_CHAT_NEW                = 'visitor chat new';
    const EVENT_VISITOR_SENDS_MESSAGE           = 'visitor sends message';
    const EVENT_VISITOR_SENDS_MESSAGE_RESULT    = 'visitor sends message result';
    const EVENT_VISITOR_BROWSER_URL             = 'visitor browser url';
    const EVENT_VISITOR_CHAT_END                = 'visitor chat end';
    
    const EVENT_OPERATOR_CHAT_INIT              = 'operator chat init';
    const EVENT_OPERATOR_CHAT_UPDATE			= 'operator chat update';
    const EVENT_OPERATOR_CHAT_ACCEPT			= 'operator chat accept';
    const EVENT_OPERATOR_CHAT_ACCEPT_RESULT		= 'operator chat accept result';
    const EVENT_OPERATOR_CHAT_END				= 'operator chat end';
    const EVENT_OPERATOR_SENDS_MESSAGE          = 'operator sends message';
    const EVENT_OPERATOR_SENDS_MESSAGE_RESULT   = 'operator sends message result';
    const EVENT_OPERATOR_READS_MESSAGE          = 'operator reads message';
    const EVENT_OPERATOR_PENDING_CHAT_UPDATE    = 'operator pending chat update';
    const EVENT_OPERATOR_STATUS_CHANGE   		= 'operator changes status';
    const EVENT_OPERATOR_CONTINUE_CHAT			= 'operator continues chat';
    
    const EVENT_CONNECTED						= 'connected';
    const EVENT_CHAT_STATUS_CHANGES             = 'chat status changes';
    const EVENT_TYPING   						= 'typing';
    const EVENT_RECEIVE_MESSAGE   				= 'receive message';
    
    const EVENT_USER_DICONNECTED                = 'user diconnected';
    const EVENT_USER_LEFT                       = 'user left';
    const EVENT_DISCONNECTED                    = 'disconnect';

    
    const PARAM_START   = 'start';
    const PARAM_STOP    = 'stop';
    const PARAM_DAEMON  = 'daemon';
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var array
     */
    protected $chats = [];
    
    /**
     * @var array
     */
    protected $users = [];
    
    /**
     * @var array
     */
    protected $usersByVendor = [];
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @var \PHPSocketIO\SocketIO
     */
    protected $io;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator
     */
    protected $operator;
    
    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param string $name
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        $name = null
    ){
        $this->objectManager    = $objectManager;
        $this->helper           = $helper;
        parent::__construct($name);
    }
    
	/**
     * Get Operator Resource
     * @return \Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator
     */
    protected function getOperator(){
    	if(!$this->operator){
    		$this->operator = $this->objectManager->get('Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator');
    	}
    	return $this->operator;
    }
		
    /**
     * Configure the command
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('vlivechat:server')
            ->setDescription('Live Chat server.')
            ->setDefinition([
                new InputOption(
                    self::PARAM_START,
                    null,
                    InputOption::VALUE_NONE,
                    'Start live chat server.'
                ),
                new InputOption(
                    self::PARAM_STOP,
                    null,
                    InputOption::VALUE_NONE,
                    'Stop live chat server.'
                ),
                new InputOption(
                    self::PARAM_DAEMON,
                    '-d',
                    InputOption::VALUE_NONE,
                    'Run in Daemon Mode'
                ),
            ]);
    }
    
    /**
     * @param string $message
     */
    protected function log($message){
        /* echo "----------------------------------------------\n";
        var_dump($this->chats);
        echo "----------------------------------------------\n";
        var_dump($this->users); */
        if(!$this->helper->isDebugMode()) return;
        if(is_string($message)){
            echo $message."\n";
            return;
        }
        var_dump($message);
    }
    
    /**
     * Fire event
     * 
     * @param \PHPSocketIO\Socket|PHPSocketIO\SocketIO $socket
     * @param string $eventName
     * @param array|string $data
     */
    protected function fireEvent($socket, $eventName, $data){
        $this->log('['.strtoupper(str_replace(" ", "_", $eventName)).']');
        /* $this->log($data); */
        $socket->emit($eventName, $data);
    }
    
    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        global $argv;
        $argv[1] = $input->getOption(self::PARAM_STOP)?self::PARAM_STOP:self::PARAM_START;
        $argv[2] = $input->getOption(self::PARAM_DAEMON)?'-d':null;
        $output->writeln("Start Server.");
        try{
            $context = [];
            if($this->helper->isUsedSsl()){
                $context['ssl'] = [
                    'local_cert'    => $this->helper->getCertFilePath(),
                    'local_pk'      => $this->helper->getCertKeyFilePath(),
										'verify_peer' => false,
                ];
            }
            $this->io = new SocketIO($this->helper->getWebSocketPort(), $context);
            $io = $this->io;
            $io->on('connection', function($socket)use($io){
                $this->fireEvent($socket, self::EVENT_CONNECTED, []);
                
                /**
                 * When disconnect
                 */
                $socket->on(self::EVENT_DISCONNECTED, function () use($socket) {
                    $this->log("[DISCONNECTED] Socket ID: ".$socket->id);                    
                    $this->onDiconnect($socket);
                });
                
                /**
                 * On Typing
                 */
                $socket->on(self::EVENT_TYPING, function($data)use($socket){
                    $this->log("[TYPING] User: ".$data['name']);
                    $this->onTyping($socket, $data);
                });
                
                /**
                 * On Close Chat
                 */
                $socket->on(self::EVENT_USER_LEFT, function($data)use($socket){
                    $this->log("[USER_LEFT] socket: ".$socket->id);
                    $this->onUserLeftChat($socket, $data);
                });
                
                /**
                 * On VISITOR Chat Init
                 */
                $socket->on(self::EVENT_VISITOR_CHAT_INIT, function($data)use($socket){
                    $this->log("[VISITOR_CHAT_INIT] Socket ID: ".$socket->id);
                    $this->onVisitorInitChat($socket, $data);
                });
                
                /**
                 * On VISITOR Chat START
                 */
                $socket->on(self::EVENT_VISITOR_CHAT_START, function($data)use($socket){
                    $this->log("[VISITOR_CHAT_START] Socket ID: ".$socket->id);
                    $this->onVisitorStartsChat($socket, $data);
                });
                
                /**
                 * On a VISITOR sends message 
                 */
                $socket->on(self::EVENT_VISITOR_SENDS_MESSAGE, function($data)use($socket){
                    $this->log("[VISITOR_SENDS_MESSAGE] Socket ID: ". $socket->id);
                    $this->onVisitorSendsMessage($socket, $data);
                });
                
                /**
                 * On a VISITOR ends chat
                 */
                $socket->on(self::EVENT_VISITOR_CHAT_END, function($data)use($socket){
                    $this->log("[VISITOR_CHAT_END] Socket ID: ". $socket->id);
                    $this->onVisitorEndsChat($socket, $data);
                });
                    
                    
                /**
                 * On OPERATOR Chat Init
                 */
                $socket->on(self::EVENT_OPERATOR_CHAT_INIT, function($data)use($socket){
                    $this->log("[OPERATOR_CHAT_INIT] Socket ID: ".$socket->id);
                    $this->onOperatorInitChat($socket, $data);
                });
                
                /**
                 * On OPERATOR Chat Init
                 */
                $socket->on(self::EVENT_OPERATOR_CHAT_ACCEPT, function($data)use($socket){
                    $this->log("[OPERATOR_CHAT_ACCEPT] Socket ID: ".$socket->id);
                    $this->onOperatorAcceptsChat($socket, $data);
                });
                
                /**
                 * On OPERATOR Chat End
                 */
                $socket->on(self::EVENT_OPERATOR_CHAT_END, function($data)use($socket){
                    $this->log("[EVENT_OPERATOR_CHAT_END] Socket ID: ".$socket->id);
                    $this->onOperatorEndsChat($socket, $data);
                });
                
                /**
                 * On OPERATOR Sends Message 
                 */
                $socket->on(self::EVENT_OPERATOR_SENDS_MESSAGE, function($data)use($socket){
                    $this->log("[OPERATOR_SENDS_MESSAGE] Socket ID: ".$socket->id);
                    $this->onOperatorSendsMessage($socket, $data);
                });
                
                /**
                 * On OPERATOR Sends Message
                 */
                $socket->on(self::EVENT_OPERATOR_READS_MESSAGE, function($data)use($socket){
                    $this->log("[OPERATOR_READS_MESSAGE] Socket ID: ".$socket->id);
                    $this->onOperatorReadsMessage($socket, $data);
                });
                
                /**
                 * On OPERATOR Change Status
                 */
                $socket->on(self::EVENT_OPERATOR_STATUS_CHANGE, function($data)use($socket){
                    $this->log("[OPERATOR_STATUS_CHANGE] Socket ID: ".$socket->id);
                    $this->onOperatorChangesStatus($socket, $data);
                });
                
                /**
                 * On OPERATOR Change Status
                 */
                $socket->on(self::EVENT_OPERATOR_CONTINUE_CHAT, function($data)use($socket){
                    $this->log("[OPERATOR_CONTINUE_CHAT] Socket ID: ".$socket->id);
                    $this->onOperatorContinuesChat($socket, $data);
                });
            });
            
            Worker::runAll();
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
    
    /**
     * @param \PHPSocketIO\Socket $socket
     * @param int $chatId
     * @param int $type [0 => 'System', 1 => 'Visitor', 2 => 'Operator']
     * @param int $userId
     * @param int $vendorId
     * @param int $currentUrlId
     */
    protected function addUser(
        \PHPSocketIO\Socket $socket,
        $chatId,
        $type=0,
        $userId,
        $vendorId = null,
        $currentUrlId = null
    ) {
        $socketId = $socket->id;
        
        $socket->join('user_'.$userId);
        
        if(!isset($this->users[$socketId])){
            $this->users[$socketId] = [
                'chat_ids' => [],
                'type' => $type,
                'user_id' => $userId,
                'vendor_id' => $vendorId,
                'current_url_id' => $currentUrlId
            ];
        }
    
        if($chatId && !in_array($chatId, $this->users[$socketId]['chat_ids'])){
            $this->users[$socketId]['chat_ids'][] = $chatId;
        }
        
        if($vendorId){
            $socket->join('room_vendor_'.$vendorId);
            $socket->join('room_vendor_'.$vendorId.'_'.$type);
            if(!isset($this->usersByVendor[$vendorId])){
                $this->usersByVendor[$vendorId] = [];
            }
            
            if(!isset($this->usersByVendor[$vendorId][$socketId])){
                $this->usersByVendor[$vendorId][$socketId] = [
                    'type' => $type,
                    'user_id' => $userId,
                ];
            }
            if($currentUrlId){
                $this->usersByVendor[$vendorId][$socketId]['current_url_id'] = $currentUrlId;
            }
        }
        
        if($chatId){
            /*Add the socket to room*/
            $socket->join('room_'.$chatId);
            $socket->join('room_'.$chatId.'_'.$type);
            
            if(!isset($this->chats[$chatId])){
                $this->chats[$chatId] = [];
            }
            
            if(!isset($this->chats[$chatId][$socketId])){
                $this->chats[$chatId][$socketId] = [
                    'type' => $type,
                    'user_id' => $userId,
                ];
            }
            if($currentUrlId){
                $this->chats[$chatId][$socketId]['current_url_id'] = $currentUrlId;
            }
            /* Reset chat actions by chat id*/
            /** @var \Vnecoms\VendorsLiveChat\Model\Chat */
            $chatAction = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\ResourceModel\ChatAction');
            $chatAction->resetActionsByChatId($chatId);
        }
    }
    
    /**
     * On Chat Init
     *
     * @param \PHPSocketIO\Socket $socket
     */
    protected function onDiconnect(\PHPSocketIO\Socket $socket){
        $socketId = $socket->id;
        if(!isset($this->users[$socketId])) return;
        
        $chatIds    = $this->users[$socketId]['chat_ids'];
        $userType   = $this->users[$socketId]['type'];
        $userId     = $this->users[$socketId]['user_id'];
        $vendorId   = $this->users[$socketId]['vendor_id'];

        unset($this->users[$socketId]);
        unset($this->usersByVendor[$vendorId][$socketId]);
        
        foreach($chatIds as $chatId){
            /* User diconnected*/
            if(!isset($this->chats[$chatId])) continue;
            
            unset($this->chats[$chatId][$socketId]);
            
            foreach($this->chats[$chatId] as $socketId => $user){
                $this->fireEvent(
                    $socket->broadcast->to($socketId),
                    self::EVENT_USER_DICONNECTED,
                    ['chat_id'   => $chatId, 'user_type' => $userType, 'user_id' => $userId]);
            }
            
            if(
                !sizeof($this->chats[$chatId])
            ){
                /* Close chat if there is no socket relates to it*/
                /** @var \Vnecoms\VendorsLiveChat\Model\Chat */
                $chatAction = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\ChatAction');
                /** @var \Magento\Framework\Stdlib\DateTime\DateTime */
                $dateTime = $this->objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
                
                $executeTime = time() + $this->helper->getChatTimeout();
                echo $executeTime."\n";
                $chatAction->setData([
                    'chat_id'       => $chatId,
                    'action'        => ChatAction::ACTION_CLOSE,
                    'execute_time'  => $executeTime,
                ])->save();
            }
        }
                
        $isUserDisconnected = true;
        if(isset($this->usersByVendor[$vendorId])) foreach($this->usersByVendor[$vendorId] as $socketId => $user){
            if($user['user_id'] == $userId && $user['type'] == Message::SENDER_TYPE_OPERATOR){
                $isUserDisconnected = false;
                break;
            }
        }
        if($isUserDisconnected){
            /* Update user status*/
            $this->getOperator()->updateOperatorData($userId, 'livechat_status', Operator::STATUS_INVISIBLE);
            $this->fireEvent(
                $this->io->to('room_vendor_'.$vendorId.'_'.Message::SENDER_TYPE_VISITOR),
                self::EVENT_OPERATOR_STATUS_CHANGE,
                ['operator_status' => Operator::STATUS_INVISIBLE]
            );
        }
        
        /* Fire Chat Update Event To All Operator Of Current Vendor*/
        $this->fireEvent(
            $this->io->to('room_vendor_'.$vendorId.'_'.Message::SENDER_TYPE_OPERATOR),
            self::EVENT_OPERATOR_CHAT_UPDATE,
            ['visitors' => $this->getVisitorListByVendor($vendorId)]
        );
    }
    

    /**
     * On user left chat
     *
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onUserLeftChat(\PHPSocketIO\Socket $socket, array $data){
        $chatId     = isset($data['chat_id'])?$data['chat_id']:'';
        $userType   = isset($data['user_type'])?$data['user_type']:'';
        $userId     = isset($data['user_id'])?$data['user_id']:'';
        if(!isset($this->chats[$chatId])) return;
        $canLeftChat = true;
        $canCloseChat = true;
        /* Check if there is no operator sockets or visitor sockets available then close chat.*/
        foreach($this->chats[$chatId] as $socketId => $user){
            if($user['type'] == $userType) {
                $canCloseChat = false;
                if($user['user_id'] == $userId){
                    $canLeftChat = false;
                }
            }
        }
        
        if($canLeftChat){
            /* If there is no socket related to this user, then send system message "User left the chat"*/
            
            $name = '';
            if($userType == Message::SENDER_TYPE_VISITOR){
                /** @var \Vnecoms\VendorsLiveChat\Model\Visitor */
                $visitor = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Visitor')->load($userId);
                $name = $visitor->getName();
            }elseif($userType == Message::SENDER_TYPE_OPERATOR){
                $operarorData = $this->getOperator()->getOperatorData($userId);
                $name = isset($operarorData['livechat_nickname'])?$operarorData['livechat_nickname']:'';
                
                /* Remove operator from chat */
                $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat')->removeOperator($chatId, $userId);
            }
            
            /* Create new system message */
            /** @var Vnecoms\VendorsLiveChat\Model\Message */
            $messageObj = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Message');
            $messageObj->setData([
                'chat_id'       => $chatId,
                'sender_id'     => null,
                'sender_type'   => Message::SENDER_TYPE_SYSTEM,
                'sender_name'   => null,
                'message'       => __('%1 left the chat', $name),
            ])->save();
            
            
            /* send system message to all socket relates to the current chat*/            
            foreach($this->chats[$chatId] as $socketId => $user){
                $tmpSocket = ($socketId == $socket->id)?$socket:$socket->broadcast->to($socketId);
                $this->fireEvent(
                    $tmpSocket,
                    self::EVENT_RECEIVE_MESSAGE,
                    [
                        'message'       => $messageObj->getData(),
                    ]
                );
            }
        }
        
        if($canCloseChat){
            /* When all sockets of operator or the visitor are disconnected then close chat */
            /** @var Vnecoms\VendorsLiveChat\Model\Chat */
            $chat = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat')->load($chatId);
            $chat->setStatus(Chat::STATUS_CLOSED)->save();
            
            $this->fireEvent($this->io->to('room_'.$chatId), self::EVENT_CHAT_STATUS_CHANGES, ['chat_id'   => $chatId, 'status' => Chat::STATUS_CLOSED]);
            
            /* foreach($this->chats[$chatId] as $socketId => $user){
                $tmpSocket = $socketId == $socket->id?$socket:$socket->broadcast->to($socketId);
                $this->fireEvent($tmpSocket, self::EVENT_CHAT_STATUS_CHANGES, ['chat_id'   => $chatId, 'status' => Chat::STATUS_CLOSED]);
            } */
            unset($this->chats[$chatId]);
        }
        
        /* send update urls to all operators that are in this chat*/
        $this->fireEvent(
            $this->io->to('room_'.$chatId.'_'.Message::SENDER_TYPE_OPERATOR),
            self::EVENT_VISITOR_BROWSER_URL,
            [
                'chat_id'   => $chatId,
                'activeUrlIds' => $this->getActiveUrlIdsByChatId($chatId),
            ]
        );
    }
    
    
    /**
     * On Chat Init
     *
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onTyping(\PHPSocketIO\Socket $socket, array $data){
        $chatId = isset($data['chat_id'])?$data['chat_id']:'';
        if(!isset($this->chats[$chatId])) return;
        /* Fire Chat Update Event*/
        foreach($this->chats[$chatId] as $socketId => $user){
            if($socketId == $socket->id) continue;
        
            $this->fireEvent(
                $socket->broadcast->to($socketId),
                self::EVENT_TYPING,
                ['chat_id' => $chatId, 'name'=> $data['name']]
            );
        }
    }
    
    
    /**
     * On Chat Init
     * 
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onVisitorInitChat(\PHPSocketIO\Socket $socket, array $data){
        if(isset($data['chats']) && is_array($data['chats'])){
            $chats = [];
            $hasNewMessages = false;
            $vendorId = isset($data['vendor_id'])?$data['vendor_id']:'';
            $visitorId = isset($data['visitor_id'])?$data['visitor_id']:'';
            $currentUrlId = isset($data['current_url_id'])?$data['current_url_id']:'';
            $this->addUser($socket, null, Message::SENDER_TYPE_VISITOR, $visitorId, $vendorId, $currentUrlId);
            
            foreach($data['chats'] as $chat){                
                /** @var \Vnecoms\VendorsLiveChat\Model\Chat **/
                $chatObj = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat')->load($chat['id']);
                $this->addUser($socket, $chat['id'], Message::SENDER_TYPE_VISITOR, $chatObj->getVisitorId(), $chatObj->getVendorId(), $currentUrlId);
                $messageCollection = $chatObj->getMessageCollection()
                    ->addFieldToFilter('message_id',['gt' => $chat['last_msg_id']?$chat['last_msg_id']:0]);
                $chatData = [
                    'chat_id'   => $chatObj->getId(),
                    'status'    => $chatObj->getStatus(),
                ];
        
                $messageData = $messageCollection->getData();
                $hasNewMessages = sizeof($messageData) > 0;
                $chatData['messages'] = $messageData;
                $chats[] = $chatData;
                
                /* send update urls to all operators that are in this chat*/
                $this->fireEvent(
                    $this->io->to('room_'.$chatObj->getId().'_'.Message::SENDER_TYPE_OPERATOR),
                    self::EVENT_VISITOR_BROWSER_URL,
                    [
                        'chat_id'   => $chatObj->getId(),
                        'urls' => $chatObj->getBrowserUrls(),
                        'activeUrlIds' => $this->getActiveUrlIdsByChatId($chatObj->getId()),
                    ]
                );
            }
                       
            /* Fire Chat Update Event To All Operator Of Current Vendor*/
            $this->fireEvent(
                $this->io->to('room_vendor_'.$vendorId.'_'.Message::SENDER_TYPE_OPERATOR),
                self::EVENT_OPERATOR_CHAT_UPDATE,
                ['visitors' => $this->getVisitorListByVendor($vendorId)]
            );
            
            $operatorStatus = $this->getOperator()->isVendorAvailable($vendorId);
            $this->fireEvent(
                $socket,
                self::EVENT_VISITOR_CHAT_UPDATE,
                ['has_new_message' => $hasNewMessages, 'chats'=> $chats, 'operator_status' => $operatorStatus]
            );
        }
    }
    
    /**
     * Fire pending update event for all operators of a vendor account
     * @param int $vendorId
     */
    protected function firePendingUpdateEvent($vendorId, \PHPSocketIO\Socket $socket){
        /* Fire pending chat update for all operators corresponding with vendor_id*/
        /** @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection */
        $chatCollection = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection')
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('status', ['eq' => \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_WAITING]);
        
        $this->fireEvent(
            $this->io->to('room_vendor_'.$vendorId.'_'.Message::SENDER_TYPE_OPERATOR),
            self::EVENT_OPERATOR_PENDING_CHAT_UPDATE,
            ['pending_chat_count' => $chatCollection->count()]
        );
        /* 
        foreach($this->usersByVendor[$vendorId] as $socketId => $user){
            if(
                $user['type'] != Message::SENDER_TYPE_OPERATOR ||
                $socket->id == $socketId
            ) continue;
            
            $this->fireEvent(
                $socket->broadcast->to($socketId),
                self::EVENT_OPERATOR_PENDING_CHAT_UPDATE,
                ['pending_chat_count' => $chatCollection->count()]
            );
        } */
    }
    
    /**
     * On Chat Start
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onVisitorStartsChat(\PHPSocketIO\Socket $socket, array $data){
        try{
            $vendorId = isset($data['vendor_id'])?$data['vendor_id']:'';
            if(!$vendorId){
                throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
            }
        
            $signature = isset($data['signature'])?$data['signature']:'';
            /** @var \Vnecoms\VendorsLiveChat\Model\Visitor */
            $visitor = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Visitor');
            $visitor->load($signature, 'signature');
            if(!$visitor->getId()) throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
        
            $name           = isset($data['name'])?$data['name']:'';
            $email          = isset($data['email'])?$data['email']:'';
            $sessionId      = isset($data['session_id'])?$data['session_id']:'';
            $currentUrlId   = isset($data['current_url_id'])?$data['current_url_id']:'';
            /* Update name and Email*/
            if($visitor->getName() != $name || $visitor->getEmail() != $email){
                $visitor->setName($name)->setEmail($email)->save();
            }
            
            /* Create new chat session */
            $chatSession = $this->helper->getVisitorSession($sessionId);
            /** @var \Vnecoms\VendorsLiveChat\Model\Visitor */
            $chat = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat');
            $chat->setData([
                'visitor_id'    => $visitor->getId(),
                'vendor_id'     => $vendorId,
                'session_id'    => $chatSession->getId(),
                'status'        => \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_WAITING,
                'is_unread'     => 1,
            ])->save();
        
            $this->addUser($socket, $chat->getId(), Message::SENDER_TYPE_VISITOR, $visitor->getId(), $vendorId, $currentUrlId);
            
            $chatData = $chat->getData();
            $chatData['messages'] = [];
            
            /* Create new message to chat*/
            $message = isset($data['message'])?$data['message']:'';
            if($message){
                $result = $chat->addMessage($visitor->getId(), Message::SENDER_TYPE_VISITOR, $visitor->getName(), $message);
                if($result){
                    $chatData['messages'][] = $result;
                }
            }
            $chatData['activeUrlIds'] = $this->getActiveUrlIdsByChatId($chat->getId());
            $chatData['logo'] = $this->helper->getVendorLogoUrl($vendorId);
            
            /* Fire Chat Update Event*/
            $this->fireEvent($socket, self::EVENT_VISITOR_CHAT_NEW, ['chat'=> $chatData]);
            
            $this->firePendingUpdateEvent($vendorId, $socket);
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
    
    /**
     * On Message Sent
     * 
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onVisitorSendsMessage(\PHPSocketIO\Socket $socket, array $data){
        try{
            $chatId = isset($data['chat_id'])?$data['chat_id']:'';
            /** @var \Vnecoms\VendorsLiveChat\Model\Chat */
            $chat = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat');
            $chat->load($chatId);
        
            if(!$chat->getChatId()){
                throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
            }
        
            $signature = isset($data['signature'])?$data['signature']:'';
            /** @var \Vnecoms\VendorsLiveChat\Model\Visitor */
            $visitor = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Visitor');
            $visitor->load($signature, 'signature');
            if(!$visitor->getId()) throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
        
            $message = isset($data['message'])?$data['message']:'';
            if(!$message) throw new \Exception(__("Please enter your message."));
        
            /* Create new chat message */
            $messageData = $chat->addMessage(
                $visitor->getId(),
                Message::SENDER_TYPE_VISITOR,
                $visitor->getName(),
                $message
            );
            $chat->setIsUnread(1)->save();
            
            /* Fire Chat Update Event*/
            $this->fireEvent(
                $socket,
                self::EVENT_VISITOR_SENDS_MESSAGE_RESULT,
                [
                    'success'       => true,
                    'message'       => $messageData,
                    'message_key'   => $data['message_key'],
                ]
            );
            /* Fire Receive Message Event*/
            if(isset($this->chats[$chat->getId()])) foreach($this->chats[$chat->getId()] as $socketId => $user){
                if($socketId == $socket->id) continue;
                
                $this->fireEvent(
                    $socket->broadcast->to($socketId),
                    self::EVENT_RECEIVE_MESSAGE,
                    [
                        'message' => $messageData,
                    ]
                );
            }
            
            /* If the chat is closed, just open it as waiting status*/
            if($chat->getStatus() == Chat::STATUS_CLOSED){
                $chat->setStatus(Chat::STATUS_WAITING)->save();
                $this->firePendingUpdateEvent($chat->getVendorId(), $socket);
                $this->fireEvent($this->io->to('room_'.$chatId), self::EVENT_CHAT_STATUS_CHANGES, ['chat_id'   => $chatId, 'status' => Chat::STATUS_WAITING]);
                /* Create new system chat message */
                $joinMessage = $chat->addMessage(
                    null,
                    Message::SENDER_TYPE_SYSTEM,
                    null,
                    __('%1 joined the chat', $visitor->getName())
                );
            
                /* Fire message receive Event*/
                $this->fireEvent($this->io->to('room_'.$chat->getId()), self::EVENT_RECEIVE_MESSAGE,['message' => $joinMessage]);
            }
            
            
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
    
    /**
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onVisitorEndsChat(\PHPSocketIO\Socket $socket, array $data){
        $responseData = [];
        try{
            $chatId = isset($data['chat_id'])?$data['chat_id']:'';
            
            /** @var \Vnecoms\VendorsLiveChat\Model\Chat */
            $chat = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat')->load($chatId);
    
            if(!$chat->getId()) throw new LocalizedException(__('The chat is not available'));
            
            $currentChatStatus = $chat->getStatus();
            $chat->setStatus(Chat::STATUS_CLOSED)->save();
            
            /* Create new system message */
            $visitor = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Visitor')
                ->load($this->users[$socket->id]['user_id']);
            
            /** @var Vnecoms\VendorsLiveChat\Model\Message */
            $messageData = $chat->addMessage(null, Message::SENDER_TYPE_SYSTEM, null, __('%1 left the chat', $visitor->getName()));            
            
            $this->fireEvent($this->io->to('room_'.$chatId), self::EVENT_CHAT_STATUS_CHANGES, ['chat_id'   => $chatId, 'status' => Chat::STATUS_CLOSED]);
            $this->fireEvent($this->io->to('room_'.$chatId), self::EVENT_RECEIVE_MESSAGE,['message' => $messageData]);
            
            if($currentChatStatus == Chat::STATUS_WAITING){
                $this->firePendingUpdateEvent($chat->getVendorId(), $socket);
            }
            /* foreach($this->chats[$chatId] as $socketId => $user){
                $tmpSocket = ($socketId == $socket->id)?$socket:$socket->broadcast->to($socketId);
                $this->fireEvent($tmpSocket, self::EVENT_CHAT_STATUS_CHANGES, ['chat_id'   => $chatId, 'status' => Chat::STATUS_CLOSED]);
            } */
            
            /* foreach($this->chats[$chatId] as $socketId => $user){
                $tmpSocket = ($socketId == $socket->id)?$socket:$socket->broadcast->to($socketId);
                $this->fireEvent($tmpSocket, self::EVENT_RECEIVE_MESSAGE,['message' => $messageData]);
            } */

        }catch (LocalizedException $e){
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => $e->getMessage()]
            );
        }catch (\Exception $e){
            $this->objectManager->create('Psr\Log\LoggerInterface')->error($e->getMessage());
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => __('Something went wrong. Try to refresh your page.')]
            );
            $this->log($e->getMessage());
        }
    }
    
    /**
     * @param string $token
     * @return boolean|array
     */
    protected function getOperatorByToken($token){
        $data = $this->getOperator()->getOperatorDataByToken($token);
        if(!$data || !isset($data['vendor_id'])) throw new LocalizedException(__('Operator is not logged in'));
        return $data;
    }
    
    /**
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onOperatorInitChat(\PHPSocketIO\Socket $socket, array $data){
        $responseData = [];
        try{
            $token = isset($data['token'])?$data['token']:'';
            $operatorData = $this->getOperatorByToken($token);
            $vendorId = $operatorData['vendor_id'];
            $this->addUser(
                $socket,
                0,
                Message::SENDER_TYPE_OPERATOR,
                $operatorData['customer_id'],
                $vendorId
            );
            /* Get opening chat*/
            /** @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection */
            $chatCollection = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection')
                ->addFieldToFilter('vendor_id', $vendorId)
                ->addFieldToFilter('status', ['neq' => \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_CLOSED]);
        
            $responseData['chats'] = [];
            $responseData['pending_chats'] = [];
            $chatCount = 0;
            $pendingChatsCount = 0;
            if($chatCollection->count()){
                foreach($chatCollection as $chat){
                    $chat->reset();
                    if($chat->getStatus() == Chat::STATUS_WAITING){
                        $responseData['pending_chats']['chat_'.$chat->getId()] = $this->initChatData($chat);
                        $pendingChatsCount++;
                        continue;
                    }elseif(
                        $chat->getOperatorIds() &&
                        !in_array($operatorData['customer_id'], $chat->getOperatorIds())
                    ) continue;
                    
                    $chatCount ++;
                    $responseData['chats']['chat_'.$chat->getId()] = $this->initChatData($chat);
                    /* Add the user*/
                    $this->addUser(
                        $socket,
                        $chat->getId(),
                        Message::SENDER_TYPE_OPERATOR,
                        $operatorData['customer_id'],
                        $vendorId
                    );
                }
            }
            $responseData['chat_count']         = $chatCount;
            $responseData['pending_chat_count'] = $pendingChatsCount;
            $responseData['operator']           = $operatorData;
            $responseData['visitors']           = $this->getVisitorListByVendor($vendorId);
            
            /* Fire Chat Update Event*/
            $this->fireEvent(
                $socket,
                self::EVENT_OPERATOR_CHAT_UPDATE,
                $responseData
            );
        }catch (LocalizedException $e){
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => $e->getMessage()]
            );
        }catch (\Exception $e){
            $this->objectManager->create('Psr\Log\LoggerInterface')->error($e->getMessage());
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => __('Something went wrong. Try to refresh your page.')]
            );
            $this->log($e->getMessage());
        }
    }
    
    /**
     * @param int $vendorId
     * @return multitype:|multitype:NULL
     */
    protected function getVisitorListByVendor($vendorId){
        $visitors = [];
        if(!isset($this->usersByVendor[$vendorId])) return $visitors;
        
        foreach($this->usersByVendor[$vendorId] as $socketId => $user){
            if(
                $user['type'] != Message::SENDER_TYPE_VISITOR ||
                isset($visitors[$user['user_id']])
            ) continue;
            $userModel = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Visitor')->load($user['user_id']);
            $visitors[$user['user_id']] = $userModel->getData();
            $urlModel = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Url');
            if(isset($user['current_url_id'])){
                $urlModel->load($user['current_url_id']);
            }
            $visitors[$user['user_id']]['current_url'] = $urlModel->getData();
        }
        
        return array_values($visitors);
    }
    
    /**
     * @param \Vnecoms\VendorsLiveChat\Model\Chat $chat
     * @return array
     */
    protected function initChatData(\Vnecoms\VendorsLiveChat\Model\Chat $chat){
        $chatData = $chat->getData();
        $chatData['messages']   = $chat->getMessageCollection()->getData();
        $chatData['visitor']    = $chat->getVisitor()->getData();
        $chatData['urls']       = $chat->getBrowserUrls();
        $chatData['activeUrlIds'] = $this->getActiveUrlIdsByChatId($chat->getId());
        
        return $chatData;
    }
    
    /**
     * @param int $chatId
     * @return array
     */
    protected function getActiveUrlIdsByChatId($chatId){
        $currentUrlIds = [];
        if(isset($this->chats[$chatId])){
            foreach($this->chats[$chatId] as $socketId => $user){
                if(
                    $user['type'] != Message::SENDER_TYPE_VISITOR ||
                    !isset($user['current_url_id']) ||
                    !$user['current_url_id']
                ) continue;
        
                $currentUrlIds[] = $user['current_url_id'];
            }
        }
        
        return $currentUrlIds;
    }
    
    /**
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onOperatorAcceptsChat(\PHPSocketIO\Socket $socket, array $data){
        $responseData = [];
        try{
            $token = isset($data['token'])?$data['token']:'';
            $operatorData = $this->getOperatorByToken($token);
            /* Get opening chat*/
            /** @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection */
            $chatCollection = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection')
                ->addFieldToFilter('vendor_id', $operatorData['vendor_id'])
                ->addFieldToFilter('status', \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_WAITING);

            $pendingChatCount = $chatCollection->count();
            if(!$pendingChatCount){
                throw new LocalizedException(__('There is no pending chat available.'));
            }
            
            $chat = $chatCollection->getFirstItem();
            $chat->setStatus(\Vnecoms\VendorsLiveChat\Model\Chat::STATUS_ACCEPTED)
                ->setIsUnread(0)
                ->save();
            $chat->addOperator($operatorData['customer_id']);
            $responseData['new_chat']   = $this->initChatData($chat);
            
            /*Add operator to the chat*/
            $this->addUser(
                $socket,
                $chat->getId(),
                Message::SENDER_TYPE_OPERATOR,
                $operatorData['customer_id'],
                $operatorData['vendor_id']
            );
            
            /* Fire Chat Accept Result Event*/
            $this->fireEvent(
                $socket,
                self::EVENT_OPERATOR_CHAT_ACCEPT_RESULT,
                $responseData
            );
            

            $this->firePendingUpdateEvent($operatorData['vendor_id'], $socket);
            
            /* Create new system chat message */
            $messageData = $chat->addMessage(
                null,
                Message::SENDER_TYPE_SYSTEM,
                null,
                __('%1 joined the chat', $operatorData['livechat_nickname'])
            );
            
            /* Fire message receive Event*/
            $this->fireEvent($this->io->to('room_'.$chat->getId()), self::EVENT_RECEIVE_MESSAGE,['message' => $messageData]);
            /* foreach($this->chats[$chat->getId()] as $socketId => $user){
                $tmpSocket = ($socketId == $socket->id)?$socket:$socket->broadcast->to($socketId);
                $this->fireEvent(
                    $tmpSocket,
                    self::EVENT_RECEIVE_MESSAGE,
                    [
                        'message'       => $messageData,
                    ]
                );
            } */
            
        }catch (LocalizedException $e){
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => $e->getMessage()]
            );
        }catch (\Exception $e){
            $this->objectManager->create('Psr\Log\LoggerInterface')->error($e->getMessage());
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => __('Something went wrong. Try to refresh your page.')]
            );
            $this->log($e->getMessage());
        }
    }
    
    /**
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onOperatorEndsChat(\PHPSocketIO\Socket $socket, array $data){
        $responseData = [];
        try{
            $token = isset($data['token'])?$data['token']:'';
            $operatorData = $this->getOperatorByToken($token);
            $chatId = isset($data['chat_id'])?$data['chat_id']:'';
    
            /** @var \Vnecoms\VendorsLiveChat\Model\Chat */
            $chat = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat')->load($chatId);
    
            if(!$chat->getId()) throw new LocalizedException(__('The chat is not available'));
    
            $currentChatStatus = $chat->getStatus();
            $chat->setStatus(Chat::STATUS_CLOSED)->save();

            /** @var Vnecoms\VendorsLiveChat\Model\Message */
            $messageData = $chat->addMessage(null, Message::SENDER_TYPE_SYSTEM, null, __('%1 left the chat', $operatorData['livechat_nickname']));
    
            $this->fireEvent($this->io->to('room_'.$chatId), self::EVENT_CHAT_STATUS_CHANGES, ['chat_id'   => $chatId, 'status' => Chat::STATUS_CLOSED]);
            $this->fireEvent($this->io->to('room_'.$chatId), self::EVENT_RECEIVE_MESSAGE,['message' => $messageData]);
        }catch (LocalizedException $e){
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => $e->getMessage()]
            );
        }catch (\Exception $e){
            $this->objectManager->create('Psr\Log\LoggerInterface')->error($e->getMessage());
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => __('Something went wrong. Try to refresh your page.')]
            );
            $this->log($e->getMessage());
        }
    }
    
    /**
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onOperatorSendsMessage(\PHPSocketIO\Socket $socket, array $data){
        try{
            $token = isset($data['token'])?$data['token']:'';
            $operatorData = $this->getOperatorByToken($token);
            
            $chatId = isset($data['chat_id'])?$data['chat_id']:'';
            /** @var \Vnecoms\VendorsLiveChat\Model\Chat */
            $chat = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat');
            $chat->load($chatId);
        
            if(!$chat->getChatId()){
                throw new LocalizedException(__("The request is not valid. Please refresh the page and try again."));
            }
        
            $message = isset($data['message'])?$data['message']:'';
            if(!$message) throw new LocalizedException(__("Please enter your message."));
        
            /* Create new chat message */
            $messageData = $chat->addMessage(
                $operatorData['customer_id'],
                Message::SENDER_TYPE_OPERATOR,
                $operatorData['livechat_nickname'],
                $message
            );
            
            /* Fire sends message Result Event*/
            $this->fireEvent(
                $socket,
                self::EVENT_OPERATOR_SENDS_MESSAGE_RESULT,
                [
                    'message_key'   => isset($data['message_key'])?$data['message_key']:'',
                    'chat_id'       => $chat->getId(),
                    'message'       => $messageData
                ]
            );
            
            /* Fire Chat Update Event*/
            foreach($this->chats[$chat->getId()] as $socketId => $user){
                if($socketId == $socket->id) continue;

                $this->fireEvent(
                    $socket->broadcast->to($socketId),
                    self::EVENT_RECEIVE_MESSAGE,
                    [
                        'message'       => $messageData,
                    ]
                );
            }
            
        }catch (LocalizedException $e){
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => $e->getMessage()]
            );
        }catch (\Exception $e){
            $this->objectManager->create('Psr\Log\LoggerInterface')->error($e->getMessage());
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => __('Something went wrong. Try to refresh your page.')]
            );
            $this->log($e->getMessage());
        }
    }
    
    /**
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onOperatorReadsMessage(\PHPSocketIO\Socket $socket, array $data){
        try{
            $token = isset($data['token'])?$data['token']:'';
            $token = isset($data['token'])?$data['token']:'';
            $operatorData = $this->getOperatorByToken($token);
    
            $chatId = isset($data['chat_id'])?$data['chat_id']:'';
            $chat = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat')->load($chatId);
            if(!$chat->getId() || !in_array($operatorData['customer_id'], $chat->getOperatorIds())){
                throw new LocalizedException(__('The chat does not exist.'));
            }
            
            $chat->setIsUnread(0)->save();
    
        }catch (LocalizedException $e){
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => $e->getMessage()]
            );
        }catch (\Exception $e){
            $this->objectManager->create('Psr\Log\LoggerInterface')->error($e->getMessage());
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => __('Something went wrong. Try to refresh your page.')]
            );
            $this->log($e->getMessage());
        }
    }
    
    /**
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onOperatorChangesStatus(\PHPSocketIO\Socket $socket, array $data){
        try{
            $token = isset($data['token'])?$data['token']:'';
            $token = isset($data['token'])?$data['token']:'';
            $operatorData = $this->getOperatorByToken($token);
            
            $status = isset($data['status'])?$data['status']:'';
            $userId = $operatorData['customer_id'];
            $vendorId = $operatorData['vendor_id'];
            
            $this->getOperator()->updateOperatorData($userId, 'livechat_status', $status);
            
            $this->fireEvent($this->io->to('user_'.$userId), self::EVENT_OPERATOR_STATUS_CHANGE, ['status' => $status]);
            
            $this->fireEvent(
                $socket->broadcast->to('room_vendor_'.$vendorId.'_'.Message::SENDER_TYPE_VISITOR),
                self::EVENT_OPERATOR_STATUS_CHANGE,
                ['operator_status' => $this->getOperator()->isVendorAvailable($vendorId)]
            );
            
        }catch (LocalizedException $e){
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => $e->getMessage()]
            );
        }catch (\Exception $e){
            $this->objectManager->create('Psr\Log\LoggerInterface')->error($e->getMessage());
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => __('Something went wrong. Try to refresh your page.')]
            );
            $this->log($e->getMessage());
        }
    }
    
    
    /**
     * @param \PHPSocketIO\Socket $socket
     * @param array $data
     */
    protected function onOperatorContinuesChat(\PHPSocketIO\Socket $socket, array $data){
        $responseData = [];
        try{
            $token = isset($data['token'])?$data['token']:'';
            $chatId= isset($data['chat_id'])?$data['chat_id']:'';
            $operatorData = $this->getOperatorByToken($token);
            /* Get opening chat*/
            /** @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection */
            $chat = $this->objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat')->load($chatId);
    
            if(!$chat->getId()){
                throw new LocalizedException(__('The chat is not available.'));
            }
    
            $chat->setStatus(\Vnecoms\VendorsLiveChat\Model\Chat::STATUS_ACCEPTED)->save();
            $chat->addOperator($operatorData['customer_id']);
            $responseData['new_chat']   = $this->initChatData($chat);
    
            /*Add operator to the chat*/
            $this->addUser(
                $socket,
                $chat->getId(),
                Message::SENDER_TYPE_OPERATOR,
                $operatorData['customer_id'],
                $operatorData['vendor_id']
            );
    
            /* Fire Chat Accept Result Event*/
            $this->fireEvent(
                $socket,
                self::EVENT_OPERATOR_CHAT_ACCEPT_RESULT,
                $responseData
            );
    
    
            $this->firePendingUpdateEvent($operatorData['vendor_id'], $socket);
    
            /* Create new system chat message */
            $messageData = $chat->addMessage(
                null,
                Message::SENDER_TYPE_SYSTEM,
                null,
                __('%1 joined the chat', $operatorData['livechat_nickname'])
            );
    
            /* Fire message receive Event*/
            $this->fireEvent($this->io->to('room_'.$chat->getId()), self::EVENT_RECEIVE_MESSAGE,['message' => $messageData]);
    
        }catch (LocalizedException $e){
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => $e->getMessage()]
            );
        }catch (\Exception $e){
            $this->objectManager->create('Psr\Log\LoggerInterface')->error($e->getMessage());
            $this->fireEvent(
                $socket,
                self::EVENT_ERROR,
                ['message' => __('Something went wrong. Try to refresh your page.')]
            );
            $this->log($e->getMessage());
        }
    }
}
