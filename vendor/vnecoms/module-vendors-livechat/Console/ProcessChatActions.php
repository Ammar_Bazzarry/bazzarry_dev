<?php

namespace Vnecoms\VendorsLiveChat\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessChatActions extends Command
{
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\ChatAction\CollectionFactory
     */
    protected $chatActionCollectionFactory;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $datetime;
    
    /**
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Vnecoms\VendorsLiveChat\Model\ResourceModel\ChatAction\CollectionFactory $chatActionCollectionFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $datetime
     * @param string $name
     */
    public function __construct(
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Vnecoms\VendorsLiveChat\Model\ResourceModel\ChatAction\CollectionFactory $chatActionCollectionFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime,
        $name = null
    ){
        $this->helper                       = $helper;
        $this->chatActionCollectionFactory  = $chatActionCollectionFactory;
        $this->datetime                     = $datetime;
        parent::__construct($name);
    }
    
    /**
     * Configure the command
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('vlivechat:process-actions');
        $this->setDescription('Close chat automatically when all users left.');
    }

    /**
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $now = time();
        $output->writeln(time().' Start processing');
        $actionCollection = $this->chatActionCollectionFactory->create()
            ->addFieldToFilter('execute_time',['lt' => $now])
            ->setPageSize(100);
        
        foreach($actionCollection as $action){
            $output->writeln('Process Action: '. $action->getId());
            $action->process();
            $action->delete();
        }
    }
}
