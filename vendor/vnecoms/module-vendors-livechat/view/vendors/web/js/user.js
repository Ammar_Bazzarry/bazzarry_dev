/**
* Copyright © 2016 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
define([
	'ko',
    'uiComponent',
    'jquery',
    'mage/translate',
], function (ko, Component, $, $t) {
    'use strict';
    
    return Component.extend({
    	defaults: {
    		template: 'Vnecoms_VendorsLiveChat/user-default',
    		userInfoUrl: '',
    		operator: {}
    	},
    	
        initialize: function () {
            this._super();
            this.updateUserInfo();
        },
        
        /**
         * Initializes observable properties of instance
         *
         * @returns {Object} Chainable.
         */
        initObservable: function () {
            this._super()
                .observe([
					'operator'
				]);
            return this;
        },
        
        /**
         * Initializes observable properties of instance
         *
         * @returns {Object} Chainable.
         */
        updateUserInfo: function () {
        	$.ajax({
        		url: this.userInfoUrl,
        		method: "GET",
        		dataType: "json"
    		}).done(function( response ){
        		if(response.user){
        			this.operator(response.user);
        		}
    		}.bind(this));
        },
        
        /**
         * Operator Status Info
         */
        operatorStatus: function(){
        	var label = $t('Invisible');
        	var cssClass = 'text-muted';
        	if(this.operator().livechat_status == '1'){
        		label = $t('Online');
        		cssClass = 'text-success';
        	}else if(this.operator().livechat_status == '2'){
        		label = $t('Away');
        		cssClass = 'text-warning';
        	}
        	
        	return {label: label, css_class: cssClass}; 
        }
    });
});
