/**
* Copyright © 2016 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
define([
	'ko',
    'uiCollection',
    'jquery',
    'mage/translate',
    'mage/template',
    'Magento_Ui/js/modal/modal',
    'Magento_Ui/js/modal/alert',
    'Vnecoms_VendorsLiveChat/js/socket_io',
    'jquery/ui',
    'jquery/jquery.cookie',
    'mage/mage',
    'mage/validation',
    'domReady!'
], function (ko, Component, $, $t, mageTemplate, uiModal, uiAlert, io) {
    'use strict';
    
    const OPERATOR_STATUS_INVISIBLE	= 0;
    const OPERATOR_STATUS_AVAILABLE	= 1;
    const OPERATOR_STATUS_AWAY		= 2;
    
    const EVENT_ERROR			                = 'error';
    const EVENT_OPERATOR_CHAT_INIT              = 'operator chat init';
    const EVENT_OPERATOR_CHAT_UPDATE			= 'operator chat update';
    const EVENT_OPERATOR_CHAT_ACCEPT			= 'operator chat accept';
    const EVENT_OPERATOR_CHAT_ACCEPT_RESULT		= 'operator chat accept result';
    const EVENT_OPERATOR_CHAT_END				= 'operator chat end';
    const EVENT_OPERATOR_SENDS_MESSAGE          = 'operator sends message';
    const EVENT_OPERATOR_SENDS_MESSAGE_RESULT   = 'operator sends message result';
    const EVENT_OPERATOR_READS_MESSAGE          = 'operator reads message';
    const EVENT_OPERATOR_PENDING_CHAT_UPDATE    = 'operator pending chat update';
    const EVENT_OPERATOR_STATUS_CHANGE   		= 'operator changes status';
    const EVENT_OPERATOR_CONTINUE_CHAT			= 'operator continues chat';
    
    const EVENT_VISITOR_BROWSER_URL             = 'visitor browser url';
    
    const EVENT_CONNECTED						= 'connected';    
    const EVENT_CHAT_STATUS_CHANGES             = 'chat status changes';
    const EVENT_RECEIVE_MESSAGE   				= 'receive message';
    const EVENT_TYPING   						= 'typing';
    const EVENT_USER_DICONNECTED                = 'user diconnected';
    const EVENT_USER_LEFT                      	= 'user left';
    
    return Component.extend({
    	defaults: {
    		template: 'Vnecoms_VendorsLiveChat/widget',
    		userInfoTemplate: 'Vnecoms_VendorsLiveChat/user',
    		visitorsListTemplate: 'Vnecoms_VendorsLiveChat/visitors',
    		saveNicknameUrl: '',
    		saveVisitorInfoUrl: '',
    		sendTranscriptUrl: '',
    		baseMediaUrl: '',
    		isEnableSound: true,
    		token: '',
    		operator: {},
    		visitors: [],
    		isEditingNickname: false,
    		isUpdatingNickname: false,
    		nickNameInputValue: '',
    		chats: {},
    		messages: [],
    		messagesQueue: [],
    		message: '',
    		audio: null,
    		chatsCount: 0,
    		pendingChatCount: 0,
    		updateFrequency: 3000,
    		updateTimer: null,
    		infoMessage: '',
    		typingTimers: {}
    	},
    	
        initialize: function () {
            this._super();
            window.onbeforeunload = function(){
                return $t('There are still opening chats. Are you sure to leave?');
            };
            /*
             * Init operator data by sending an ajax request to server
             */
            this.init();
            this.audio = new Audio(this.notificationSound);
            return this;
        },
        
        /**
         * Initializes observable properties of instance
         *
         * @returns {Object} Chainable.
         */
        initObservable: function () {
            this._super()
                .observe([
					'isEnableSound',
					'operator',
					'visitors',
					'nickNameInputValue',
					'isEditingNickname',
					'isUpdatingNickname',
					'chats',
					'messages',
					'messagesQueue',
					'chatsCount',
					'pendingChatCount',
					'message',
					'infoMessage'
				]);
            return this;
        },
        
        /**
         * Get User Info Template
         */
        getUserInfoTemplate: function(){
        	return this.userInfoTemplate;
        },
        
        /**
         * Get visitors list template
         */
        getVisitorsListTemplate: function(){
        	return this.visitorsListTemplate;
        },
        
        /**
         * Enable Editing Nick Name
         */
        enableEditingNickname: function(){
        	this.isEditingNickname(true);
        	this.nickNameInputValue(this.operator().livechat_nickname);
        	$('.vlivechat-username input').focus();
        },
        /**
         * Operator Status Info
         */
        operatorStatus: function(){
        	var label = $t('Invisible');
        	var cssClass = 'text-muted';
        	if(this.operator().livechat_status == '1'){
        		label = $t('Online');
        		cssClass = 'text-success';
        	}else if(this.operator().livechat_status == '2'){
        		label = $t('Away');
        		cssClass = 'text-warning';
        	}
        	
        	return {label: label, css_class: cssClass}; 
        },
        
        /**
         * Set Operator Status To Online
         */
        setOnlineStatus: function(){
        	this.fireEvent(EVENT_OPERATOR_STATUS_CHANGE, {token: this.token, status: OPERATOR_STATUS_AVAILABLE});
        	$('.vlivechat-status-changer').removeClass('open');
        },
        
        /**
         * Set Operator Status To Away
         */
        setAwayStatus: function(){
        	this.fireEvent(EVENT_OPERATOR_STATUS_CHANGE, {token: this.token, status: OPERATOR_STATUS_AWAY});
        	$('.vlivechat-status-changer').removeClass('open');
        },
        
        /**
         * Set Operator Status To Invisible
         */
        setInvisibleStatus: function(){
        	this.fireEvent(EVENT_OPERATOR_STATUS_CHANGE, {token: this.token, status: OPERATOR_STATUS_INVISIBLE});
        	$('.vlivechat-status-changer').removeClass('open');
        },
        
        /**
         * Init status dropdown event
         */
        initEvents: function(event){
        	var self = this;
        	$("#vlivechat-status-changer-btn").click(function(){
                $('.vlivechat-status-changer').toggleClass('open');
            });
        	$('body').click(function(event){
                var elms = [
                    '#vlivechat-status-changer-btn',
                    '#vlivechat-status-changer-btn i',
                    '#vlivechat-status-changer-btn span',
                    '.vlivechat-status-changer',
                    '.vlivechat-status-changer a',
                    '.vlivechat-status-changer i',
                    '.vlivechat-status-changer span'
                ];
            	if (!event.target.matches(elms.join())) {
                	$('.vlivechat-status-changer').removeClass('open');
            	}
            	
            	var usernameElms = [
            	    '.vlivechat-username-label',
                    '.vlivechat-username',
                    '.vlivechat-username input'
                ];
            	if (!event.target.matches(usernameElms.join())) {
            		if(self.isEditingNickname()){
            			if(self.nickNameInputValue() != self.operator().livechat_nickname){
            				self.saveNickname();
            				return;
            			}
            			self.isEditingNickname(false);
            		}
            	}
            });
        },
        /**
         * Save nickname
         */
        saveNickname: function(){
        	this.isUpdatingNickname(true);
        	$.ajax({
        		url: this.saveNicknameUrl,
        		method: "POST",
    			data: {
	        		nickname: this.nickNameInputValue()
        		},
        		dataType: "json"
    		}).done(function( response ){
    			this.isUpdatingNickname(false);
    			this.isEditingNickname(false);
        		if(response.success){
        			this.operator(response.operator);
        			return;
        		}
        		uiAlert({title: $t('Error ...'), content: response.message});
    		}.bind(this));
        },
        /**
         * Fire Event
         */
        fireEvent: function(eventName, data){
        	this.logEvent(eventName, data);
        	this.socket.emit(eventName, data);
        },
        
        /**
         * Log Data
         */
        log: function(data){
        	if(!this.debugMode) return;
        	if(typeof(data) == 'string') data = '[VLIVECHAT] ' + data;
        	console.log(data);
        },
        /**
         * Log Event
         */
        logEvent: function(eventName, data){
        	this.log(eventName.toUpperCase().replace(/ /g,'_'));
        	this.log(data);
        },
        /**
         * Init
         */
        init: function(){
        	if(!this.socket){
        		this.socket = io(this.webSocketUrl);
        	}
        	
        	/*this.fireEvent(EVENT_OPERATOR_CHAT_INIT, {token: this.token});*/
        	
        	/**
        	 * On connected
        	 */
        	this.socket.on(EVENT_CONNECTED, function (data) {
        		this.logEvent(EVENT_CONNECTED, data);
        		this.fireEvent(EVENT_OPERATOR_CHAT_INIT, {token: this.token});
        	}.bind(this));
        	
        	/**
        	 * On customer diconnected
        	 */
        	this.socket.on(EVENT_USER_DICONNECTED, function (data) {
        		this.logEvent(EVENT_USER_DICONNECTED, data);
        		/* After 1 minutes, send left chat request*/
        		setTimeout(function(){
        			this.fireEvent(EVENT_USER_LEFT, data);
        		}.bind(this), 60000);
        	}.bind(this));
        	
        	/**
        	 * On ERROR
        	 */
        	this.socket.on(EVENT_ERROR, function (data) {
        		this.logEvent(EVENT_ERROR, data);
        		uiAlert({title: $t('Error ...'), content: data.message});
        	}.bind(this));
        	
        	/**
        	 * On other user typing
        	 */
        	this.socket.on(EVENT_TYPING, function (data) {
        		this.logEvent(EVENT_TYPING, data);
        		var chatIndex 	= 'chat_' + data.chat_id;
        		var name	= data.name;
        		var chats 	= this.chats();
        		if(!chats[chatIndex]) return;
        		if(chats[chatIndex].typingUsers().indexOf(name) === -1){
        			chats[chatIndex].typingUsers.push(name);
        		}
        		this.chats(chats);
        		this.scrollDiv(data.chat_id);
        		
        		if(this.typingTimers[name]){
        			clearTimeout(this.typingTimers[name]);
        		}
        		this.typingTimers[name] = setTimeout(function(){
    					chats[chatIndex].typingUsers.splice(chats[chatIndex].typingUsers.indexOf(name), 1);
    					this.chats(chats);
        			}.bind(this),
        			3000
    			);
        	}.bind(this));
        	
        	/**
        	 * On Update Chats
        	 */
        	this.socket.on(EVENT_OPERATOR_CHAT_UPDATE, function (data) {
        		this.logEvent(EVENT_OPERATOR_CHAT_UPDATE, data);
        		if(data.chat_count > 0){
        			this.initChats(data.chats);
        		}
        		if(data.chat_count){
        			this.chatsCount(data.chat_count);
        		}
        		if(data.pending_chat_count){
        			this.pendingChatCount(data.pending_chat_count);
        		}
        		if(data.operator){
        			this.operator(data.operator);
        		}
        		if(data.visitors){
        			this.visitors(data.visitors);
        		}
    			if(data.pending_chat_count > 0){
    				this.playSound();
    			}
        	}.bind(this));
        	
        	/**
        	 * On Visitor Browser URLS
        	 */
        	this.socket.on(EVENT_VISITOR_BROWSER_URL, function (data) {
        		this.logEvent(EVENT_VISITOR_BROWSER_URL, data);
        		var chatIndex 	= 'chat_' + data.chat_id;
        		var chats 	= this.chats();
        		if(!chats[chatIndex]) return;
        		if(data.urls){
        			chats[chatIndex].urls(data.urls);
        		}
        		chats[chatIndex].activeUrlIds(data.activeUrlIds);
        		this.chats(chats);
        	}.bind(this));
        	
        	/**
        	 * On Update Chats
        	 */
        	this.socket.on(EVENT_OPERATOR_PENDING_CHAT_UPDATE, function (data) {
        		this.logEvent(EVENT_OPERATOR_PENDING_CHAT_UPDATE, data);
    			this.pendingChatCount(data.pending_chat_count);
    			if(data.pending_chat_count > 0){
    				this.playSound();
    			}
        	}.bind(this));
        	
        	
        	/**
        	 * On operator chat result
        	 */
        	this.socket.on(EVENT_OPERATOR_CHAT_ACCEPT_RESULT, function (data) {
        		this.logEvent(EVENT_OPERATOR_CHAT_ACCEPT_RESULT, data);
    			this.addChat(data.new_chat);
        	}.bind(this));
        	
        	/**
        	 * On operator sends message result
        	 */
        	this.socket.on(EVENT_OPERATOR_SENDS_MESSAGE_RESULT, function (data) {
        		this.logEvent(EVENT_OPERATOR_SENDS_MESSAGE_RESULT, data);
        		var chatId = data.chat_id;
    			var chats = this.chats();
    			this.removeQueuedMessageByKey(chats['chat_'+chatId], data.message_key);    			
    			chats['chat_'+chatId].messages.push(data.message);
    			this.chats(chats);
    			this.afterRenderMessages(chatId);
        	}.bind(this));
        	
        	/**
        	 * On receiving message
        	 */
        	this.socket.on(EVENT_RECEIVE_MESSAGE, function (data) {
        		this.logEvent(EVENT_RECEIVE_MESSAGE, data);
        		var chatIndex = 'chat_'+data.message.chat_id;
    			var chats = this.chats();
    			if(!chats[chatIndex]) return;
    			/* If the chat is not opened, increase the unread count*/
    			/*if(!chats[chatIndex].isOpened()){*/
    				chats[chatIndex].unreadCount(chats[chatIndex].unreadCount() + 1);
    			/*}*/
    			chats[chatIndex].messages.push(data.message);
    			chats[chatIndex].typingUsers.splice(chats[chatIndex].typingUsers.indexOf(data.message.sender_name), 1);
    			this.chats(chats);
    			this.scrollDiv(data.message.chat_id);
    			this.afterRenderMessages(data.message.chat_id);
    			this.playSound();
        	}.bind(this));
        	
        	/**
        	 * On Chat Status Changes
        	 */
        	this.socket.on(EVENT_CHAT_STATUS_CHANGES, function (data) {
        		this.logEvent(EVENT_CHAT_STATUS_CHANGES, data);
        		var chatIndex = 'chat_'+data.chat_id;
    			var chats = this.chats();
    			if(!chats[chatIndex]) return;
    			chats[chatIndex].status(data.status);
    			this.chats(chats);
        	}.bind(this));
        	
        	/**
        	 * On Chat Status Changes
        	 */
        	this.socket.on(EVENT_OPERATOR_STATUS_CHANGE, function (data) {
        		this.logEvent(EVENT_OPERATOR_STATUS_CHANGE, data);
        		var operator = this.operator();
        		operator.livechat_status = data.status;
        		this.operator(operator);
        	}.bind(this));
        	
        },
        /**
         * Init Chats
         */
        initChats: function (chats){
        	var chatCount = 0;
        	var index;
        	for(index in chats){
        		var chat = chats[index];
        		chat = this.prepareChatData(chat);
        		chats[index] = chat;
        		chatCount ++;
        	}
        	chats[index].isOpened(true);
        	this.chatsCount(chatCount);
        	this.chats(chats);
        },
        
        /**
         * Prepare Chat Data
         */
        prepareChatData: function(chat){
        	chat['messages'] 		= ko.observableArray(chat['messages']);
    		chat['messagesQueue'] 	= ko.observableArray([]);
    		chat['typingUsers']		= ko.observableArray([]);
    		chat['urls']			= ko.observableArray(chat['urls']);
    		chat['activeUrlIds']	= ko.observableArray(chat['activeUrlIds']);
    		chat['message'] 		= ko.observable('');
    		chat['isOpened'] 		= ko.observable(false);
    		chat['isEnding'] 		= ko.observable(false);
    		chat['unreadCount'] 	= ko.observable(0);
    		chat['status'] 			= ko.observable(chat['status']);
    		chat['isOpenTranscript']= ko.observable(false);
    		chat['emailsTranscript']= ko.observable('');
    		return chat;
        },
        
        /**
         * Blink the pending chat button if there is pending chat.
         */
        pendingChatBtnEffect: function(){
        	$('#vlivechat-pending-btn-container').effect( 'bounce', {}, 1000 );
        	var self = this;
        	setTimeout(function(){
        		self.pendingChatBtnEffect()
        	}, 3000);
        },
        
        /**
         * Vendor accepts chat
         */
        acceptChat: function(){
        	/*Hide all open chat*/
        	var chats = this.chats();
        	for(var index in chats){
        		chats[index].isOpened(false);
        	}
        	this.chats(chats);
        	this.fireEvent(EVENT_OPERATOR_CHAT_ACCEPT, {token: this.token});
        },
        
        /**
         * Add chat session
         */
        addChat: function(chat){
        	var chats = this.chats();
        	chat = this.prepareChatData(chat);
        	if(typeof(chats['chat_' + chat.chat_id]) != 'undefined'){
        		chats['chat_' + chat.chat_id].status(chat.status());
        		chats['chat_' + chat.chat_id].messages(chat.messages());
        	}else{
        		chat['isOpened'] = ko.observable(true);
        		chats['chat_' + chat.chat_id] = chat;
        	}
        	this.chats(chats);
        	this.chatsCount(this.chatsCount() + 1);
        },
        
        /**
         * Get chats list
         */
        getChats: function(){
        	var chats = [];
        	for(var index in this.chats()){
        		chats.push(this.chats()[index]);
        	}
        	return chats;
        },
        /**
         * Get chat messages by chat
         */
        getMessagesByChat: function(chat){
        	var messages = [];
        	var previousMessage = null;
        	var tmpMessage = [];
        	$(chat.messages()).each(function(index, message){
        		if(!previousMessage){
        			tmpMessage = {
    					sender_id: message.sender_id,
    					sender_type: message.sender_type,
    					sender_name: message.sender_name,
    					messages: [{message: message.message, created_at: message.created_at}]
        			};
        			previousMessage = message;
        			return true;
        		}
        		if(
    				message.sender_id == previousMessage.sender_id &&
    				message.sender_type == previousMessage.sender_type
				){
        			tmpMessage.messages.push({message: message.message, created_at: message.created_at});
        			return true;
        		}
        		messages.push(tmpMessage);
        		tmpMessage = tmpMessage = {
					sender_id: message.sender_id,
					sender_type: message.sender_type,
					sender_name: message.sender_name,
					messages: [{message: message.message, created_at: message.created_at}]
    			};
        		previousMessage = message;
        	});
        	
        	if($(tmpMessage).size()){
        		messages.push(tmpMessage);
        	}
        	return messages;
        },
        
        /**
         * On chat window Click
         */
        onChatWindowClick: function(chat){
        	if(!chat.unreadCount()) return true;
        	var chats = this.chats();
        	chats['chat_'+chat.chat_id].unreadCount(0);
        	this.fireEvent(EVENT_OPERATOR_READS_MESSAGE, {token: this.token, chat_id: chat.chat_id});
        	this.chats(chats);
        },
        /**
         * Toggle chatbox
         */
        toggleChatbox: function(chat){
        	if(chat.chat_id){
	        	var chats = this.chats();
	        	var currStatus = chats['chat_'+chat.chat_id].isOpened();
	        	
	        	/*Hide all chat windows*/
	        	for(var index in chats){
	        		chats[index].isOpened(false);
	        	}
	        	
	        	/*Toggle current window*/
	        	chats['chat_'+chat.chat_id].isOpened(!currStatus);
	        	this.chats(chats);
	        	return true;
        	}
        	this.isOpened(true);
        },
        /**
         * Hide chatbox
         */
        hideChatbox: function(chat){
        	if(chat.chat_id){
	        	var chats = this.chats();
	        	chats['chat_'+chat.chat_id].isOpened(false);
	        	chats['chat_'+chat.chat_id].isEnding(false);
	        	this.chats(chats);
	        	return true;
        	}
        	this.isOpened(false);
        },
        
        /**
         * End chat
         */
        endChat: function(chat){
        	var chats = this.chats();
        	
        	/* Close chatbox if the chat is ended already*/
        	if(chats['chat_'+chat.chat_id].status() == 2){
        		this.hideChatbox(chat);
        		delete chats['chat_'+chat.chat_id];
        		this.chats(chats);
        		return true;
        	}
        	
        	/*Confirm if the chat is opening*/
        	chats['chat_'+chat.chat_id].isEnding(true);
        	this.chats(chats);
        },
        
        /**
         * Confirm End Chat
         */
        confirmEndChat: function(chat){
        	/*Confirm if the chat is opening*/
        	var chats = this.chats();
        	chats['chat_'+chat.chat_id].isEnding(false);
        	this.fireEvent(EVENT_OPERATOR_CHAT_END, {chat_id: chat.chat_id, token: this.token});
        	this.chats(chats);
        },
        
        /**
         * Cancel End Chat
         */
        cancelEndChat: function(chat){
        	/*Confirm if the chat is opening*/
        	var chats = this.chats();
        	chats['chat_'+chat.chat_id].isEnding(false);
        	this.chats(chats);
        },
        
        /**
         * Send message
         */
        sendMessage: function(chat, event){
    		var keyCode = (event.which ? event.which : event.keyCode);

    		/*Send Typing event every 1 second typing*/
    		if(!chat.isTyping){
	    		chat.isTyping = true;
	    		this.fireEvent(EVENT_TYPING, {chat_id: chat.chat_id, name: this.operator().livechat_nickname});
	    		setTimeout(function(){
	    			chat.isTyping = false
	    		}.bind(this), 2000);
    		}
    		
    		if (keyCode != 13) return true;
    		var message = chat.message();
    		if(!message.length) return false;
    		
    		/*Push message to queue*/
    		var messagesQueue = chat.messagesQueue();
    		var key = Math.round(Math.random() * 100000);
    		messagesQueue.push({key: key, message: message});
        	chat.messagesQueue(messagesQueue);
        	chat.message('');
        	this.scrollDiv(chat.chat_id, 0);
    		
        	/*Send message to server*/
    		var postData = {
        		message: message,
        		chat_id: chat.chat_id,
        		message_key: key,
        		token: this.token
    		};
    		
    		this.fireEvent(EVENT_OPERATOR_SENDS_MESSAGE, postData);
    		
    		return false;
        },
        
        /**
         * Send Attachment
         */
        sendAttachment: function(chatId, file){
        	var chats = this.chats();
        	if(typeof(chats['chat_'+chatId]) == 'undefined') return;
        	/*Prepare attachment message*/
        	var message = this.isImage(file.file)?'[img ' + file.file + ']':'[file ' + file.file + ']';
        	var chat = chats['chat_'+chatId];
    		var key = Math.round(Math.random() * 100000);
    		chat.messagesQueue.push({key: key, message: message});
    		this.scrollDiv(chat.chat_id, 0);
    		
    		/*Send message to server using ajax*/
    		var postData = {
        		message: message,
        		chat_id: chat.chat_id,
        		message_key: key,
        		token: this.token
    		};

    		this.fireEvent(EVENT_OPERATOR_SENDS_MESSAGE, postData);
        },
        
        /**
         * Is Image File
         */
        isImage: function(filename){
        	var extension = filename.split('.').pop().toLowerCase();
        	return ['png','jpg','jpeg','gif'].indexOf(extension) != -1;
        },
        
        /**
         * Process Message
         */
        processMessage: function(message){
        	if(this.isAttachmentMessage(message)){
        		var filename = message.replace(/\[file\s*(.*?)\s*\]/g, '$1');
        		filename = filename.split('/').pop();
            	message = message.replace(/\[file\s*(.*?)\s*\]/g, '<div class="vlivechat-file-container"><a href="'+this.baseMediaUrl+'$1" target="_blank">'+filename+'</a></div>');
            	message.replace('//','/');
        	}else if(this.isImageMessage(message)){
        		message = message.replace(/\[img\s*(.*?)\s*\]/g, '<div class="vlivechat-img-container"><img src="'+this.baseMediaUrl+'$1" /></div>');
        		message.replace('//','/');
        	}
        	return message;
        },
        /**
         * Is attachment message
         */
        isAttachmentMessage: function(message){
        	return message.search(/\[file\s*(.*?)\s*\]/g) != -1;
        },
        /**
         * Is image message
         */
        isImageMessage: function(message){
        	return message.search(/\[img\s*(.*?)\s*\]/g) != -1;
        },
        
        /**
         * After render messages
         */
        afterRenderMessages: function(chat_id){
        	var self = this;
        	this.scrollDiv(chat_id);
        	
        	$('#vlivechat'+chat_id+' .vlivechat-img-container').each(function(){
        		if($(this).hasClass('binded-event')) return true;
        		$(this).addClass('binded-event');
        		$(this).click(function(){
            		self.viewImage($(this).children('img').first().attr('src'));
            	});
        	});
        },
        /**
         * View image by URL
         */
        viewImage: function (url) {
            $('<div class="thumbnail-preview import-view-image"></div>').html('<div class="thumbnail-preview-image-block"><img class="thumbnail-preview-image" src="'+url+'" /></div>')
            .modal({
                title: $t('View Image'),
                type: 'popup',
                modalClass: 'vlivechat-image-box',
                autoOpen: true,
                clickableOverlay: true,
                innerScroll: true,
                buttons: [],
                outerClickHandler: function (event) {
                	this.closeModal(event);
                }
             });
        },
        /**
         * Remove queued message by key
         */
        removeQueuedMessageByKey: function(chat, key){
        	var messagesQueue = chat.messagesQueue();
			var deleteIndex = '';
			$(messagesQueue).each(function(index, message){
				if(message.key == key){
					deleteIndex = index;
					return false;
				}
			});
			
			messagesQueue.splice(deleteIndex, 1);
			chat.messagesQueue(messagesQueue);
        },
        
        /**
         * Continue exist chat
         */
        continueChat: function(chat){
        	this.fireEvent(EVENT_OPERATOR_CONTINUE_CHAT, {token: this.token, chat_id: chat.chat_id});
        },
        /**
         * Scroll Div
         */
        scrollDiv: function(chat_id, timeout=500){
        	setTimeout(
        		function(){
		        	/*Scroll a chat*/
		        	if(chat_id){
			        	var livechatContent = $('#vlivechat'+chat_id+' .vlivechat-messages');
			        	livechatContent.scrollTop(livechatContent[0].scrollHeight - livechatContent[0].clientHeight);
			        	return true;
		        	}
		        	
		        	/*Scroll all chats*/
		        	$('.vlivechat-container .vlivechat-message-container').each(function(){
		        		$(this).scrollTop($(this)[0].scrollHeight - $(this)[0].clientHeight);
		        	});
	        	},
	        	500
        	);
        },
        
        /**
         * Get Active Url
         */
        getActiveUrl: function(chat){
        	var result = false;
        	$(chat.urls()).each(function(index, url){
        		if(!result) result = url;
        		if(chat.activeUrlIds().indexOf(url.url_id) !== -1) {
        			result = url;
        			return false;
        		}
        	});
        	
        	return result;
        },
        
        /**
         * Get active Visitors
         */
        getActiveVisitors: function(){
        	var result = [];
        	var chatByVendors = {};
        	$(this.getChats()).each(function(index, chat){
        		chatByVendors['visitor_'+chat.visitor.visitor_id] = chat;
        	});
        	
        	$(this.visitors()).each(function(index, visitor){
        		if(chatByVendors['visitor_'+visitor.visitor_id]) return true;
        		result.push(visitor);
        	});
        	
        	return result;
        },
        
        /**
         * Update visitor phone
         */
        updateVisitorPhone: function(chat){
        	this.updateVisitorInfo({
				visitor_id: chat.visitor.visitor_id,
				telephone: 	chat.visitor.telephone
    		});
        },
        
        /**
         * Update Visitor Note
         */
        updateVisitorNote: function(chat){
        	this.updateVisitorInfo({
				visitor_id: chat.visitor.visitor_id,
        		note: 		chat.visitor.note
    		});
        },
        
        /**
         * Update Visitor Info
         */
        updateVisitorInfo: function(params){
        	$.ajax({
        		url: this.saveVisitorInfoUrl,
        		method: "POST",
    			data: params,
        		dataType: "json"
    		}).done(function( response ){
    			
    		}.bind(this));
        },
        
        /**
         * FOrmat visitor Id
         */
        getVisitorId: function(visitor){
        	var visitorId = visitor.visitor_id+ '';
        	while(visitorId.length < 6){
        		visitorId = '0' + visitorId;
        	}
        	
        	return visitorId;
        },
        /**
         * Play sound
         */
        playSound: function(){
        	if(!this.isEnableSound()) return;
        	this.audio.play();
        },
        /**
         * Toggle Sound
         */
        toggleSound: function(){
        	this.isEnableSound(!this.isEnableSound());
        },
        /**
         * Attach file
         */
        attachFile: function(){
        	console.log('ATTACH FILE');
        },
        /**
         * Open transcript Form
         */
        openTranscriptForm: function(chat){
        	chat.isOpenTranscript(true);
        },
        /**
         * Close transcript Form
         */
        closeTranscriptForm: function(chat){
        	chat.isOpenTranscript(false);
        },
        /**
         * Send transcript
         */
        sendTranscript: function(chat){        	
        	var validate = $(jQuery('#transcript_emails_' + chat.chat_id)).validation().validation('isValid');
        	if(!validate) return false;
        	
        	$.ajax({
        		url: this.sendTranscriptUrl,
        		method: "POST",
    			data: {
    				chat_id: chat.chat_id,
    				email: chat.emailsTranscript()
        		},
        		dataType: "json"
    		}).done(function( response ){
        		if(response.success){
        			this.closeTranscriptForm(chat);
        			this.info($t('Transcript will be sent after this conversation ends.'));
        			chat.emailsTranscript('');
        			return;
        		}
        		uiAlert({title: $t('Error ...'), content: response.message});
    		}.bind(this));
        },
        
        /**
         * Show info message.
         */
        info: function(message, duration){
        	this.infoMessage(message);
        	if(!duration) duration = 4000;
        	setTimeout(function(){this.infoMessage('');}.bind(this), duration);
        },
        /**
         * Get Cookie Data
         * 
         * @return string
         */
        getCookie: function(key){
        	return $.cookie(key);
        },
        
        /**
         * Set Cookie Data
         * 
         * @return string
         */
        setCookie: function(key, value){
        	return $.cookie(key, value);
        },

        /**
         * Format number with zero
         */
        formatNumber: function(number){
        	return (number <= 9)?'0' + number:number;
        }
    });
});
