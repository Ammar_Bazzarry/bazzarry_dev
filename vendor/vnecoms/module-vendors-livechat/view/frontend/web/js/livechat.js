/**
* Copyright © 2016 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
define([
	'ko',
    'uiComponent',
    'jquery',
    'mage/translate',
    'mage/template',
    'Magento_Ui/js/modal/modal',
    'Magento_Ui/js/modal/alert',
    'Vnecoms_VendorsLiveChat/js/socket_io',
    'jquery/jquery.cookie',
    'mage/mage',
    'mage/validation',
    'domReady!'
], function (ko, Component, $, $t, mageTemplate, uiModal, uiAlert, io) {
    'use strict';
    
    const COOKIE_VISITOR_SIGNATURE 			= 'vlivechat_visitor_id';
    
    const EVENT_VISITOR_CHAT_INIT               = 'visitor chat init';
    const EVENT_VISITOR_CHAT_UPDATE             = 'visitor chat update';
    const EVENT_VISITOR_CHAT_START              = 'visitor chat start';
    const EVENT_VISITOR_CHAT_NEW                = 'visitor chat new';
    const EVENT_VISITOR_CHAT_END                = 'visitor chat end';
    const EVENT_VISITOR_SENDS_MESSAGE           = 'visitor sends message';
    const EVENT_VISITOR_SENDS_MESSAGE_RESULT    = 'visitor sends message result';
    const EVENT_RECEIVE_MESSAGE   				= 'receive message';
    const EVENT_CHAT_STATUS_CHANGES             = 'chat status changes';
    const EVENT_CONNECTED						= 'connected';
    const EVENT_DISCONNECTED                    = 'diconnect';
    const EVENT_TYPING   						= 'typing';
    const EVENT_USER_DICONNECTED                = 'user diconnected';
    const EVENT_USER_LEFT                      	= 'user left';
    
    const EVENT_OPERATOR_STATUS_CHANGE   		= 'operator changes status';
    
    return Component.extend({
    	defaults: {
    		debugMode: false,
    		template: 'Vnecoms_VendorsLiveChat/widget',
    		cookieLifeTime: 365, /*day*/
    		cookiePath: '/',
    		cookieDomain: '',
    		session_id: '',
    		webSocketUrl: '',
    		socket: null,
    		initUrl: '',
    		isEnableSound: true,
    		isOpened: false,
    		isReady: false,
    		isSentOfflineMessage: false,
    		vendor_id: '',
    		operator_status: {},
    		vendor_livechat_status:null,
    		visitor: {},
    		chats: {},
    		currentUrlId: 0,
    		messages: [],
    		messagesQueue: [],
    		message: '',
    		audio: null,
    		chatsCount: 0,
    		updateFrequency: 3000,
    		updateTimer: null,
			typingTimers: {}
    	},
    	
        initialize: function () {
            this._super();
            /*
             * Init visitor data by sending an ajax request to server
             * If the vendor is online, show livechat button, if not show contact form
             */
            this.init();
            this.audio = new Audio(this.notificationSound);
            return this;
        },
        
        /**
         * Initializes observable properties of instance
         *
         * @returns {Object} Chainable.
         */
        initObservable: function () {
            this._super()
                .observe([
					'isOpened',
					'isEnableSound',
					'isReady',
					'isSentOfflineMessage',
					'visitor',
					'operator_status',
					'chats',
					'currentUrlId',
					'messages',
					'message',
					'messagesQueue',
					'chatsCount',
					'vendor_livechat_status',
					'message'
				]);
            
            
            return this;
        },
        
        /**
         * Init
         */
        init: function(){
        	this.log('INIT');
        	$.ajax({
        		url: this.initUrl,
        		method: "POST",
    			data: {
	        		vendor_id: this.vendor_id,
	        		url: window.location.toString(),
	        		page_title: $('head title').text()
        		},
        		dataType: "json"
    		}).done(function( response ){
    			if(!response.success){
    				uiAlert({title: $t('Error ...'), content: response.message});
    				return;
    			}
    			this.session_id = response.session_id;
        		if(response.visitor){
        			this.visitor(response.visitor);
        		}
        		if(response.chats){
        			this.initChats(response.chats);
        		}
        		if(response.current_url_id){
        			this.currentUrlId(response.current_url_id);
        		}
        		this.initChatSocket();
        		
        		this.isReady(true);
    		}.bind(this));
        },
        /**
         * Init Chats
         */
        initChats: function (chats){
        	var chatCount = 0;
        	var index;
        	for(index in chats){
        		var chat = chats[index];
        		chat = this.prepareChatData(chat);
        		chats[index] = chat;
        		chatCount ++;
        	}
        	chats[index].isOpened(true);
        	this.chatsCount(chatCount);
        	this.chats(chats);
        },
        /**
         * Prepare Chat Data
         */
        prepareChatData: function(chat){
        	chat['messages'] 		= ko.observableArray(chat['messages']);
    		chat['messagesQueue'] 	= ko.observableArray([]);
    		chat['typingUsers']		= ko.observableArray([]);
    		chat['message'] 		= ko.observable('');
    		chat['isOpened'] 		= ko.observable(false);
    		chat['isEnding'] 		= ko.observable(false);
    		chat['status'] 			= ko.observable(chat['status']);
    		chat['isTyping'] 		= null;
    		chat['transcriptEmail'] = ko.observable('');
    		chat['isOpenedTranscriptForm']	= ko.observable(false);
    		chat['isSentTranscript']		= ko.observable(false);
    		return chat;
        },
        
        /**
         * Add chat session
         */
        addChat: function(chat){
        	var chats = this.chats();
        	chat = this.prepareChatData(chat);
        	chat['isOpened'] = ko.observable(true);
    		chats['chat_' + chat.chat_id] = chat;
        	this.chats(chats);
        	this.chatsCount(this.chatsCount() + 1);
        },
        
        /**
         * Init Chat Socket
         */
        initChatSocket: function(){
        	this.socket = io(this.webSocketUrl);
        	
        	/**
        	 * On connected
        	 */
        	this.socket.on(EVENT_CONNECTED, function (data) {
        		this.logEvent(EVENT_CONNECTED, data);
        		var params = {
    				chats:[],
    				vendor_id: this.vendor_id,
					visitor_id: this.visitor().visitor_id,
					session_id: this.session_id,
					current_url_id: this.currentUrlId()
				};
        		
            	for(var index in this.chats()){
            		var chat = this.chats()[index];
            		var messages = chat.messages();
            		var lastMessageId = messages.length?messages[messages.length - 1]['message_id']:0;
            		params.chats.push({id:chat.chat_id ,last_msg_id:lastMessageId});
            	}
        		this.fireEvent(EVENT_VISITOR_CHAT_INIT, params);
        	}.bind(this));
        	
        	/**
        	 * On customer diconnected
        	 */
        	this.socket.on(EVENT_USER_DICONNECTED, function (data) {
        		this.logEvent(EVENT_USER_DICONNECTED, data);
        		/* After 2 minutes, send left chat request*/
        		setTimeout(function(){
        			this.fireEvent(EVENT_USER_LEFT, data);
        		}.bind(this), 120000);
        	}.bind(this));
        	
        	/**
        	 * On Update Chats
        	 */
        	this.socket.on(EVENT_VISITOR_CHAT_UPDATE, function (data) {
        		this.logEvent(EVENT_VISITOR_CHAT_UPDATE, data);
        		if(data.has_new_message){
            		this.playSound();
            	}
        	    this.updateChats(data.chats);
        	    
        	    this.operator_status(data.operator_status);
        	}.bind(this));
        	
        	/**
        	 * On Add New Chat
        	 */
        	this.socket.on(EVENT_VISITOR_CHAT_NEW, function (data) {
        		this.logEvent(EVENT_VISITOR_CHAT_NEW, data);
        		this.addChat(data.chat);
        	}.bind(this));
        	
        	/**
        	 * On other user typing
        	 */
        	this.socket.on(EVENT_TYPING, function (data) {
        		this.logEvent(EVENT_TYPING, data);
        		var chatIndex 	= 'chat_' + data.chat_id;
        		var name	= data.name;
        		var chats 	= this.chats();
        		if(chats[chatIndex].typingUsers().indexOf(name) === -1){
        			chats[chatIndex].typingUsers.push(name);
        		}
        		this.chats(chats);
        		this.scrollDiv(data.chat_id);
        		
        		if(this.typingTimers[name]){
        			clearTimeout(this.typingTimers[name]);
        		}
        		this.typingTimers[name] = setTimeout(function(){
    					chats[chatIndex].typingUsers.splice(chats[chatIndex].typingUsers.indexOf(name), 1);
    					this.chats(chats);
        			}.bind(this),
        			3000
    			);
        	}.bind(this));
        	
        	/**
        	 * On message send result
        	 */
        	this.socket.on(EVENT_VISITOR_SENDS_MESSAGE_RESULT, function (data) {
        		this.logEvent(EVENT_VISITOR_SENDS_MESSAGE_RESULT, data);
        		if(data.success){
        			var chatId = data.message.chat_id;
        			var chats = this.chats();
        			this.removeQueuedMessageByKey(chats['chat_'+chatId], data.message_key);
        			chats['chat_'+chatId].messages.push(data.message);
        			this.chats(chats);
        			this.afterRenderMessages(chatId);
        		}else{
        			uiAlert({
        				title: $t('Error ...'),
        				content: data.message
        			});
        		}
        	}.bind(this));
        	
        	/**
        	 * On receiving message
        	 */
        	this.socket.on(EVENT_RECEIVE_MESSAGE, function (data) {
        		this.logEvent(EVENT_RECEIVE_MESSAGE, data);
        		var chatIndex = 'chat_'+data.message.chat_id;
    			var chats = this.chats();
    			chats[chatIndex].messages.push(data.message);
    			chats[chatIndex].typingUsers.splice(chats[chatIndex].typingUsers.indexOf(data.message.sender_name), 1);
				
    			this.chats(chats);
    			this.afterRenderMessages(data.message.chat_id);
    			this.playSound();
        	}.bind(this));
        	
        	/**
        	 * On Chat Status Changes
        	 */
        	this.socket.on(EVENT_CHAT_STATUS_CHANGES, function (data) {
        		this.logEvent(EVENT_CHAT_STATUS_CHANGES, data);
        		var chatIndex = 'chat_'+data.chat_id;
    			var chats = this.chats();
    			chats[chatIndex].status(data.status);
    			this.chats(chats);
        	}.bind(this));
        	
        	/**
        	 * On Chat Status Changes
        	 */
        	this.socket.on(EVENT_OPERATOR_STATUS_CHANGE, function (data) {
        		this.logEvent(EVENT_OPERATOR_STATUS_CHANGE, data);
    			this.operator_status(data.operator_status);
        	}.bind(this));

        	/**
        	 * On Update Chats
        	 */
        	this.socket.on(EVENT_DISCONNECTED, function (data) {
        		this.logEvent(EVENT_DISCONNECTED, data);
        	}.bind(this));
        },
        
        /**
         * Fire Event
         */
        fireEvent: function(eventName, data){
        	this.logEvent(eventName, data);
        	this.socket.emit(eventName, data);
        },
        
        /**
         * Log Data
         */
        log: function(data){
        	if(!this.debugMode) return;
        	if(typeof(data) == 'string') data = '[VLIVECHAT] ' + data;
        	console.log(data);
        },
        /**
         * Log Event
         */
        logEvent: function(eventName, data){
        	this.log(eventName.toUpperCase().replace(/ /g,'_'));
        	this.log(data);
        },
        /**
         * Update Chats by response data
         */
        updateChats: function (responseChats){
			var chats = this.chats();
			var updatedChatIds = [];
			$(responseChats).each(function(index, chat){
				var messages = chats['chat_'+chat.chat_id].messages();
				$(chat.messages).each(function(index, message){
    				messages.push(message);
    			});
    			chats['chat_'+chat.chat_id].messages(messages);
    			chats['chat_'+chat.chat_id].status(chat.status);
    			updatedChatIds.push(chat.chat_id);
			});
			this.chats(chats);
			/* Scroll updated chats only*/
			$(updatedChatIds).each(function(index, chatId){
				this.scrollDiv(chatId);
			}.bind(this));
        },
        /**
         * Get Signature
         */
        getSignature: function(){
        	return this.getCookie(COOKIE_VISITOR_SIGNATURE);
        },
        
        
        /**
         * Get chats list
         */
        getChats: function(){
        	var chats = [];
        	for(var index in this.chats()){
        		chats.push(this.chats()[index]);
        	}
        	return chats;
        },

        /**
         * Is current in chats list
         * 
         * @return bool
         */
        canShowNewChatBox: function(){
        	if(!this.vendor_id) return false;
        	
        	for(var index in this.chats()){
        		if(this.chats()[index].vendor_id == this.vendor_id) return false;
        	}
        	
        	return true;
        },
        /**
         * Toggle chatbox
         */
        showChatbox: function(chat){
        	if(!chat || !chat.chat_id) {
        		this.isOpened(true);
        		return false;
        	}
        	var chats = this.chats();
        	chats['chat_'+chat.chat_id].isOpened(true);
        	this.chats(chats);
        	return true;

        },
        /**
         * Toggle chatbox
         */
        hideChatbox: function(chat){
        	if(!chat || !chat.chat_id) {
        		this.isOpened(false);
        		return false;
        	}
        	
        	var chats = this.chats();
        	chats['chat_'+chat.chat_id].isOpened(false);
        	this.chats(chats);
        	return true;
        	
        },
        /**
         * End chat
         */
        endChat: function(chat){
        	if(!chat || !chat.chat_id){
        		this.hideChatbox();
        		return;
        	}
        	var chats = this.chats();
        	
        	/* Close chatbox if the chat is ended already*/
        	if(chats['chat_'+chat.chat_id].status() == 2){
        		delete chats['chat_'+chat.chat_id];
        		this.chats(chats);
        		this.hideChatbox();
        		return;
        	}
        	
        	/*Confirm if the chat is opening*/
        	chats['chat_'+chat.chat_id].isEnding(true);
        	this.chats(chats);
        },
        /**
         * Confirm End Chat
         */
        confirmEndChat: function(chat){
        	/*Confirm if the chat is opening*/
        	var chats = this.chats();
        	chats['chat_'+chat.chat_id].isEnding(false);
        	this.fireEvent(EVENT_VISITOR_CHAT_END, {chat_id: chat.chat_id});
        	this.chats(chats);
        },
        
        /**
         * Cancel End Chat
         */
        cancelEndChat: function(chat){
        	/*Confirm if the chat is opening*/
        	var chats = this.chats();
        	chats['chat_'+chat.chat_id].isEnding(false);
        	this.chats(chats);
        },
        
        /**
         * Start chat
         */
        startChat: function(){
        	/* Validate form*/
        	var validate = true;
        	jQuery('#vlivechat-start-form .vlivechat-input').each(function(index, elm){
        		validate = validate && $(elm).validation().validation('isValid');
        	});
        	if(!validate) return;
        	var data ={
    			vendor_id: this.vendor_id,
    			signature: this.visitor().signature,
        		name: this.visitor().name,
        		email: this.visitor().email,
        		message: this.message(),
        		session_id: this.session_id,
        		current_url_id: this.currentUrlId()
    		}
        	this.fireEvent(EVENT_VISITOR_CHAT_START, data);
        	this.message('');
        },
        
        /**
         * Send message
         */
        sendMessage: function(chat, event){
    		var keyCode = (event.which ? event.which : event.keyCode);
    		/*Send Typing event every 1 second typing*/
    		if(!chat.isTyping){
	    		chat.isTyping = true;
	    		this.fireEvent(EVENT_TYPING, {chat_id: chat.chat_id, name: this.visitor().name});
	    		setTimeout(function(){
	    			chat.isTyping = false
	    		}.bind(this), 2000);
    		}
    		
    		if (keyCode != 13) return true;
    		var message = chat.message();
    		if(!message.length) return false;
    		
    		/*Push message to queue*/
    		var key = Math.round(Math.random() * 100000);
    		chat.messagesQueue.push({key: key, message: message});
        	chat.message('');
        	this.scrollDiv(chat.chat_id, 0);
    		
        	/*Send message to server*/
    		var postData = {
    			signature: this.visitor().signature,
        		message: message,
        		chat_id: chat.chat_id,
        		message_key: key
    		};

    		this.fireEvent(EVENT_VISITOR_SENDS_MESSAGE, postData);
    		
    		return false;
        },
        
        /**
         * Send Attachment
         */
        sendAttachment: function(chatId, file){
        	var chats = this.chats();
        	if(typeof(chats['chat_'+chatId]) == 'undefined') return;
        	/*Prepare attachment message*/
        	var message = this.isImage(file.file)?'[img ' + file.file + ']':'[file ' + file.file + ']';
        	var chat = chats['chat_'+chatId];
    		var key = Math.round(Math.random() * 100000);
    		chat.messagesQueue.push({key: key, message: message});
    		this.scrollDiv(chat.chat_id, 0);
    		
    		/*Send message to server using ajax*/
    		var postData = {
    			signature: this.visitor().signature,
        		message: message,
        		chat_id: chat.chat_id,
        		message_key: key
    		};

    		this.fireEvent(EVENT_VISITOR_SENDS_MESSAGE, postData);
        },
        
        /**
         * Is Image File
         */
        isImage: function(filename){
        	var extension = filename.split('.').pop().toLowerCase();
        	return ['png','jpg','jpeg','gif'].indexOf(extension) != -1;
        },
        
        /**
         * Process Message
         */
        processMessage: function(message){
        	if(this.isAttachmentMessage(message)){
        		var filename = message.replace(/\[file\s*(.*?)\s*\]/g, '$1');
        		filename = filename.split('/').pop();
            	message = message.replace(/\[file\s*(.*?)\s*\]/g, '<div class="vlivechat-file-container"><a href="'+this.baseMediaUrl+'$1">'+filename+'</a></div>');
            	message.replace('//','/');
        	}else if(this.isImageMessage(message)){
        		message = message.replace(/\[img\s*(.*?)\s*\]/g, '<div class="vlivechat-img-container"><img src="'+this.baseMediaUrl+'$1" /></div>');
        		message.replace('//','/');
        	}
        	return message;
        },
        /**
         * Is attachment message
         */
        isAttachmentMessage: function(message){
        	return message.search(/\[file\s*(.*?)\s*\]/g) != -1;
        },
        /**
         * Is image message
         */
        isImageMessage: function(message){
        	return message.search(/\[img\s*(.*?)\s*\]/g) != -1;
        },
        /**
         * Remove queued message by key
         */
        removeQueuedMessageByKey: function(chat, key){
        	var messagesQueue = chat.messagesQueue();
			var deleteIndex = '';
			$(messagesQueue).each(function(index, message){
				if(message.key == key){
					deleteIndex = index;
					return false;
				}
			});
			
			messagesQueue.splice(deleteIndex, 1);
			chat.messagesQueue(messagesQueue);
        },
        
        /**
         * Send Offline Message.
         */
        sendOfflineMessage: function(){
        	var validate = true;
        	jQuery('#vlivechat-start-form .vlivechat-input').each(function(index, elm){
        		validate = validate && $(elm).validation().validation('isValid');
        	});
        	if(!validate) return;
        	
        	var data ={
    			vendor_id: this.vendor_id,
    			signature: this.visitor().signature,
        		name: this.visitor().name,
        		email: this.visitor().email,
        		message: this.message(),
        		session_id: this.session_id,
    		}
        	
        	$.ajax({
        		url: this.sendOfflineMessageUrl,
        		method: "POST",
    			data: data,
        		dataType: "json"
    		}).done(function( response ){
    			if(!response.success){
    				uiAlert({title: $t('Error ...'), content: response.message});
    				return;
    			}
    			this.isSentOfflineMessage(true);
    		}.bind(this));
        },
        /**
         * Send other offline message
         */
        sendOtherOfflineMessage: function(){
        	this.isSentOfflineMessage(false);
        	this.message('');
        },
        /**
         * After render messages
         */
        afterRenderMessages: function(chat_id){
        	var self = this;
        	this.scrollDiv(chat_id);
        	
        	$('#vlivechat'+chat_id+' .vlivechat-img-container').each(function(){
        		if($(this).hasClass('binded-event')) return true;
        		$(this).addClass('binded-event');
        		$(this).click(function(){
            		self.viewImage($(this).children('img').first().attr('src'));
            	});
        	});
        },
        /**
         * View image by URL
         */
        viewImage: function (url) {
            $('<div class="thumbnail-preview import-view-image"></div>').html('<div class="thumbnail-preview-image-block"><img class="thumbnail-preview-image" src="'+url+'" /></div>')
            .modal({
                title: $t('View Image'),
                type: 'popup',
                modalClass: 'vlivechat-image-box',
                autoOpen: true,
                innerScroll: true,
                buttons: [
                    
                ],
                closed: function () {
                    // on close
                }
             });
        },
        /**
         * Scroll Div
         */
        scrollDiv: function(chat_id, timeout=500){
        	setTimeout(
        		function(){
		        	/*Scroll a chat*/
		        	if(chat_id){
			        	var livechatContent = $('#vlivechat'+chat_id+' .vlivechat-message-container');
			        	if(livechatContent.size()){
			        		livechatContent.scrollTop(livechatContent[0].scrollHeight - livechatContent[0].clientHeight);
			        	}
			        	return true;
		        	}
		        	
		        	/*Scroll all chats*/
		        	$('.vlivechat-container .vlivechat-message-container').each(function(){
		        		$(this).scrollTop($(this)[0].scrollHeight - $(this)[0].clientHeight);
		        	});
        		},
        		timeout
    		);
        },
        /**
         * Play sound
         */
        playSound: function(){
        	if(!this.isEnableSound()) return;
        	this.audio.play();
        },
        /**
         * Toggle Sound
         */
        toggleSound: function(){
        	this.isEnableSound(!this.isEnableSound());
        },
        /**
         * Attach file
         */
        attachFile: function(){
        	console.log('ATTACH FILE');
        },
        
        /**
         * Open transcript form
         */
        openTranscriptForm: function(chat){
        	chat.isOpenedTranscriptForm(true);
        },
        /**
         * Close transcript form
         */
        closeTranscriptForm: function(chat){
        	chat.isOpenedTranscriptForm(false);
        },
        /**
         * Send transcript
         */
        sendTranscript: function(chat){
        	/* Validate form*/
        	var validate = true;
        	jQuery('#vlivechat' + chat.chat_id +' .vlivechat-transcript .vlivechat-input').each(function(index, elm){
        		validate = validate && $(elm).validation().validation('isValid');
        	});
        	if(!validate) return;
        	
        	$.ajax({
        		url: this.sendTranscriptUrl,
        		method: "POST",
    			data: {
    				session_id: this.session_id,
	        		chat_id: chat.chat_id,
	        		email: chat.transcriptEmail()
        		},
        		dataType: "json"
    		}).done(function( response ){
    			if(!response.success){
    				uiAlert({title: $t('Error ...'), content: response.message});
    				return;
    			}
        		chat.isSentTranscript(true);
        		setTimeout(function(){
        			chat.isSentTranscript(false);
        			chat.isOpenedTranscriptForm(false);
        			chat.transcriptEmail('')
        		}.bind(this), 4000);
    		}.bind(this));
        },
        
        /**
         * Get Cookie Data
         * 
         * @return string
         */
        getCookie: function(key){
        	return $.cookie(key);
        },
        
        /**
         * Set Cookie Data
         * 
         * @return string
         */
        setCookie: function(key, value){
        	return $.cookie(
    			key,
    			value,
    			{
    				path: 		this.cookiePath,
    				expires: 	this.cookieLifeTime,
    				domain: 	this.cookieDomain
				}
			);
        },

        /**
         * Format number with zero
         */
        formatNumber: function(number){
        	return (number <= 9)?'0' + number:number;
        }
    });
});
