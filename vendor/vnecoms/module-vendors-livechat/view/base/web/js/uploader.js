/*
 * Copyright © 2018 Vnecoms. All rights reserved.
 */

define(
    [
     	'jquery',
        'Magento_Ui/js/form/element/file-uploader',
        'ko',
        'uiRegistry',
        'Magento_Ui/js/lib/validation/validator'
    ],
    function (
    	$,
        UpLoader,
        ko,
        registry,
        validator,
    ) {
        return UpLoader.extend({
            defaults: {
                template: 'Vnecoms_VendorsLiveChat/uploader/uploader',
                previewTmpl: 'Vnecoms_VendorsLiveChat/uploader/preview',
                isMultipleFiles : false,
                isLoadingChat: ko.observable({}),
                inputName: 'image',
                uploaderConfig: {
                    dataType: 'json',
                    sequentialUploads: true,
                    formData: {
                        'form_key': window.FORM_KEY
                    }
                },

                links: {
                    value: '${ $.parentName }:uploadValue'
                }
            },

            /**
             *  Retrieve files type allowed
             */
            getFileTypesAllowed: function() {
                return this.uploaderConfig.acceptFileTypes;
            },

            /**
             *  Retrieve max number file allow to upload
             */
            getMaxFileNumber: function() {
                return this.uploaderConfig.maxFileNumber;
            },

            /**
             * Load start event handler.
             */
            onLoadingStart: function (e) {
            	var target = $(e.target);
            	var chat_id = target.data('chat_id');
            	var isLoadingChat = this.isLoadingChat();
            	isLoadingChat['chat_'+chat_id] = true;
            	this.isLoadingChat(isLoadingChat);
            	this._super();
            },

            /**
             * Load stop event handler.
             */
            onLoadingStop: function (e) {
            	var target = $(e.target);
            	var chat_id = target.data('chat_id');
            	var isLoadingChat = this.isLoadingChat();
            	isLoadingChat['chat_'+chat_id] = false;
            	this.isLoadingChat(isLoadingChat);
            	this._super();
            },
            
            /**
             * Handler which is invoked prior to the start of a file upload.
             *
             * @param {Event} e - Event object.
             * @param {Object} data - File data that will be uploaded.
             */
            onBeforeFileUpload: function (e, data) {
                var file     = data.files[0],
                    allowed  = this.isFileAllowed(file),
                    target   = $(e.target);
                if (allowed.passed) {
                    target.on('fileuploadsend', function (event, postData) {
                        postData.data.append('param_name', this.paramName);
                        postData.data.append('chat_id', target.data('chat_id'));
                    }.bind(data));

                    target.fileupload('process', data).done(function () {
                        data.submit();
                    });
                } else {
                    this.notifyError(allowed.message);
                }
            },
            
            /**
             * After file is uploaded.
             */
            addFile: function (file) {
            	this._super();
            	registry.get(this.parentName).sendAttachment(file.chat_id, file);
            },
            
            /**
             * Get file class name
             */
            getClassIcon:function (filename){
            	var extension = filename.split('.').pop().toLowerCase();
            	switch(extension){
            		case 'rar':
            		case 'tgz':
            		case 'bz':
	                case 'zip':
	                    return 'message-icon-file-zip';
	                case 'pdf':
	                    return 'message-icon-file-pdf';
	                case 'doc':
	                case 'docx':
	                    return 'message-icon-file-word';
	                case 'xls':
	                case 'xlsx':
	                    return 'message-icon-file-excel';
	                case 'png':
	                case 'jpeg':
	                case 'jpg':
	                case 'gif':
	                    return 'message-icon-file-image';
	                default: return 'message-icon-file-empty';
	            }
            }

        });
    }
);