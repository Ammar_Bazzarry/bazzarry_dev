<?php

namespace Vnecoms\VendorsLiveChat\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Vnecoms\VendorsLiveChat\Model\Source\ServerType;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class Data extends AbstractHelper
{
    const XML_PATH_SERVER_TYPE          = 'vendors/livechat/server_type';
    const XML_PATH_SERVER_URL           = 'vendors/livechat/server_url';
    const XML_PATH_SERVER_PORT          = 'vendors/livechat/server_port';
    const XML_PATH_CHAT_TIMEOUT         = 'vendors/livechat/chat_timeout';
    const XML_PATH_ALLOWED_EXTENSIONS   = 'vendors/livechat/allowed_extensions';
    const XML_PATH_USE_SSL              = 'vendors/livechat/use_ssl';
    const XML_PATH_CERT_PATH            = 'vendors/livechat/cert_path';
    const XML_PATH_CERT_KEY_PATH        = 'vendors/livechat/cert_key_path';
    const XML_PATH_DEBUG_MODE           = 'vendors/livechat/debug_mode';
    
    const XML_PATH_EMAIL_TEMPLATE_TRANSCRIPT    = 'vendors/livechat/email_template_chat_transcript';
    const XML_PATH_SENDER_EMAIL_IDENTITY        = 'vendors/livechat/sender_email_identity';
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\SessionFactory
     */
    protected $sessionFactory;
    
    /**
     * @var \Vnecoms\VendorsConfig\Helper\Data
     */
    protected $configHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    
    /**
     * @var \Vnecoms\Vendors\Helper\Email
     */
    protected $emailHelper;
    
    /**
     * @param Context $context
     * @param \Vnecoms\VendorsLiveChat\Model\SessionFactory $sessionFactory
     * @param \Vnecoms\VendorsConfig\Helper\Data $configHelper
     * @param \Vnecoms\Vendors\Helper\Email $emailHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        \Vnecoms\VendorsLiveChat\Model\SessionFactory $sessionFactory,
        \Vnecoms\VendorsConfig\Helper\Data $configHelper,
        \Vnecoms\Vendors\Helper\Email $emailHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->sessionFactory   = $sessionFactory;
        $this->configHelper     = $configHelper;
        $this->emailHelper      = $emailHelper;
        $this->storeManager     = $storeManager;
        parent::__construct($context);
    }
    
    /**
     * @return string
     */
    public function getWebSocketUrl(){
        $serverType = $this->scopeConfig->getValue(self::XML_PATH_SERVER_TYPE);
        if($serverType == ServerType::TYPE_CUSTOM)
            return $this->scopeConfig->getValue(self::XML_PATH_SERVER_URL);
        
        $baseUrl = str_replace("index.php", '', $this->_urlBuilder->getBaseUrl());
        $urlInfo = parse_url($baseUrl);
        return sprintf('%s://%s:%s',$urlInfo['scheme'],$urlInfo['host'], $this->getWebSocketPort());
    }
    
    /**
     * Get Websocket Port
     * 
     * @return string
     */
    public function getWebSocketPort(){
        return $this->scopeConfig->getValue(self::XML_PATH_SERVER_PORT);
    }
    
    /**
     * Is debug mode
     * 
     * @return boolean
     */
    public function isDebugMode(){
        return (bool) $this->scopeConfig->getValue(self::XML_PATH_DEBUG_MODE);
    }
    
    /**
     * Get visitor Session
     * 
     * @param string $sessionId
     * @return \Vnecoms\VendorsLiveChat\Model\Session
     */
    public function getVisitorSession($sessionId){
        $session = $this->sessionFactory->create();
        $session->load($sessionId,'code');
        /* If the session is not exist, create a new one*/
        if(!$session->getId()){
            $session->setData([
                'code' => $sessionId
            ])->save();
        }
    
        return $session;
    }
    
    /**
     * After this time(seconds) when both visitor and operator left, the chat will be closed automatically.
     * @return number
     */
    public function getChatTimeout(){
        return $this->scopeConfig->getValue(self::XML_PATH_CHAT_TIMEOUT);
    }
    
    /**
     * @return boolean
     */
    public function isUsedSsl(){
        return (bool) $this->scopeConfig->getValue(self::XML_PATH_USE_SSL);
    }
    
    /**
     * Get cert file path
     * 
     * @return string
     */
    public function getCertFilePath(){
        return $this->scopeConfig->getValue(self::XML_PATH_CERT_PATH);
    }
    
    /**
     * Get cert key file path
     *
     * @return string
     */
    public function getCertKeyFilePath(){
        return $this->scopeConfig->getValue(self::XML_PATH_CERT_KEY_PATH);
    }
    
    /**
     * @param int $vendorId
     * @return string
     */
    public function getVendorLogoUrl($vendorId){
        $logoConfig = $this->configHelper->getVendorConfig(
            'general/store_information/logo',
            $vendorId
        );
        
        if($logoConfig){
            $basePath = 'ves_vendors/logo/';
            $baseUrl = $this->storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            );
            return $baseUrl.$basePath. $logoConfig;
        }
        
        return '';
    }
    
    /**
     * Get allowed Extensions
     * 
     * @return string
     */
    public function getAllowedExtensions(){
        $allowedExtensions = $this->scopeConfig->getValue(self::XML_PATH_ALLOWED_EXTENSIONS);
        return str_replace(" ","", $allowedExtensions);
    }
    
    /**
     * @return string
     */
    public function getUploadFolder(){
        return 'ves_vendorslivechat/attachment';
    }
    
    /**
     * @param \Vnecoms\VendorsLiveChat\Model\Chat $chat
     * @param string $email
     */
    public function sendTranscript(
        \Vnecoms\VendorsLiveChat\Model\Chat $chat,
        $email
    ) {
        try {
            $this->emailHelper->sendTransactionEmail(
                self::XML_PATH_EMAIL_TEMPLATE_TRANSCRIPT,
                \Magento\Framework\App\Area::AREA_FRONTEND,
                self::XML_PATH_SENDER_EMAIL_IDENTITY,
                $email,
                ['chat' => $chat]
            );
        } catch (\Exception $e) {
            
        }
    }
}
