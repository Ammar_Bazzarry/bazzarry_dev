<?php

namespace Vnecoms\VendorsLiveChat\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;


/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class Cookie extends AbstractHelper
{
    /**
     * Cookie life time
     */
    CONST COOKIE_LIFE = 31536000;
    
    CONST COOKIE_VISITOR_SIGNATURE = 'vlivechat_visitor_id';
    
    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $cookieManager;
    
    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    protected $cookieMetadataFactory;
    
    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $sessionManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;
    
    /**
     * @param Context $context
     * @param CookieManagerInterface $cookieManager
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param SessionManagerInterface $sessionManager
     * @param DateTime $date
     */
    public function __construct(
        Context $context,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager,
        DateTime $date
    ) {
        parent::__construct($context);
        $this->cookieManager            = $cookieManager;
        $this->cookieMetadataFactory    = $cookieMetadataFactory;
        $this->sessionManager           = $sessionManager;
        $this->date                     = $date;
    }
    

    /**
     * Get Cookie Path
     *
     * @return string
     */
    public function getCookiePath(){
        return $this->sessionManager->getCookiePath();
    }
    
    /**
     * Get Cookie Domain
     *
     * @return string
     */
    public function getCookieDomain(){
        return $this->sessionManager->getCookieDomain();
    }
    
    /**
     * Get data from cookie set in remote address
     *
     * @return value
     */
    public function getCookie($name)
    {
        return $this->cookieManager->getCookie($name);
    }
    
    /**
     * @param string $name
     * @param string $value
     * @param number $duration
     * 
     * @return \Vnecoms\VendorsLiveChat\Helper\Data
     */
    public function setCookie($name, $value, $duration = 31536000)
    {
        $metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($duration)
            ->setPath($this->getCookiePath())
            ->setDomain($this->getCookieDomain());
    
        $this->cookieManager->setPublicCookie($name, $value, $metadata);
        
        return $this;
    }

    
    /**
     * @param string $name
     * @return \Vnecoms\VendorsLiveChat\Helper\Data
     */
    public function deleteCookie($name)
    {
        $this->cookieManager->deleteCookie(
            $name,
            $this->cookieMetadataFactory
                ->createCookieMetadata()
                ->setPath($this->sessionManager->getCookiePath())
                ->setDomain($this->sessionManager->getCookieDomain())
        );
        return $this;
    }
    
    /**
     * @return int
     */
    public function getCookielifetime()
    {
        return self::COOKIE_LIFE;
    }
    
    /**
     * @return string
     */
    public function getSignatureFromCookie(){
        return $this->getCookie(self::COOKIE_VISITOR_SIGNATURE);
    }
    
    /**
     * @param unknown $value
     */
    public function setSignatureCookie($value){
        $this->setCookie(self::COOKIE_VISITOR_SIGNATURE, $value);
    }
    
    /**
     * @return string
     */
    public function generateSignature(){
        return md5($this->date->timestamp().rand(9, 100000));
    }
    
    /**
     * @return \Magento\Framework\Stdlib\DateTime\DateTime
     */
    public function getDate(){
        return $this->date;
    }
}
