<?php

namespace Vnecoms\VendorsLiveChat\Controller\Index;
use Magento\Framework\App\Action\Context;

class Start extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        try{
            $request = $this->getRequest();
            $vendorId = $request->getParam('vendor_id');
            if(!$vendorId){
                throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
            }
            
            $signature = $request->getParam('signature');
            /** @var \Vnecoms\VendorsLiveChat\Model\Visitor */
            $visitor = $this->_objectManager->get('Vnecoms\VendorsLiveChat\Model\Visitor');
            $visitor->load($signature, 'signature');
            if(!$visitor->getId()) throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
            
            $name = $request->getParam('name');
            $email = $request->getParam('email');
            /* Update name and Email*/
            if($visitor->getName() != $name || $visitor->getEmail() != $email){
                $visitor->setName($name)->setEmail($email)->save();
            }

            /* Create new chat session */
            /** @var \Vnecoms\VendorsLiveChat\Model\Visitor */
            $chat = $this->_objectManager->get('Vnecoms\VendorsLiveChat\Model\Chat');
            $chat->setData([
                'visitor_id'    => $visitor->getId(),
                'vendor_id'     => $vendorId,
                'status'        => \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_WAITING
            ])->save();
            
            $chatData = $chat->getData();
            $chatData['messages'] = $chat->getMessageCollection()->getData();
            $response->setData([
                'success' => true,
                'chat' => $chatData
            ]);
        }catch(\Exception $e){
            $response->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }

}
