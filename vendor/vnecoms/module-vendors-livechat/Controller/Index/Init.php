<?php

namespace Vnecoms\VendorsLiveChat\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

class Init extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Cookie
     */
    protected $cookie;

    
    /**
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Vnecoms\VendorsLiveChat\Helper\Cookie $cookie
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Vnecoms\VendorsLiveChat\Helper\Cookie $cookie,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->resultJsonFactory    = $resultJsonFactory;
        $this->customerSession      = $customerSession;
        $this->helper               = $helper;
        $this->cookie               = $cookie;
        $this->date                 = $date;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        try{
            $signature = $this->cookie->getSignatureFromCookie();
            /** @var \Vnecoms\VendorsLiveChat\Model\Visitor */
            $visitor = $this->_objectManager->get('Vnecoms\VendorsLiveChat\Model\Visitor');
            $visitor->load($signature, 'signature');
            if(!$signature || !$visitor->getId()){
                $signature = $this->cookie->generateSignature();
                $visitor->setData([
                    'signature' => $signature,
                ]);
                $this->cookie->setSignatureCookie($signature);
            }
            $visitor->setLastUpdated($this->date->timestamp());
            /* If the visitor name is not set and he is logged in just set the customer name*/
            if(!$visitor->getName() && $this->customerSession->isLoggedIn()){
                $customer = $this->customerSession->getCustomer();
                $visitor->setCustomerId($customer->getId());
                $visitor->setName($customer->getName());
                $visitor->setEmail($customer->getEmail());
            }
            
            $visitor->save();
            $sessionId = md5($this->customerSession->getSessionId());
            $chatSession = $this->helper->getVisitorSession($sessionId);
            $responseData = [
                'success' => true,
                'visitor' => $visitor->getData(),
                'session_id' => $sessionId,
            ];
            
            /* Get opening chat*/
            /** @var \Vnecoms\VendorsLiveChat\Model\Visitor */
            $chatCollection = $this->_objectManager->get('Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection')
                ->addFieldToFilter('visitor_id', $visitor->getId())
                ->addFieldToFilter('status', ['neq' => \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_CLOSED]);
            if($chatCollection->count()){
                $responseData['chats'] = [];
                foreach($chatCollection as $chat){
                    if($chat->getSessionId() != $chatSession->getSessionId()){
                        $chat->updateSessionId($chatSession->getSessionId());
                    }
                    $chatData = $chat->getData();
                    $chatData['messages'] = $chat->getMessageCollection()->getData();
                    $chatData['logo'] = $this->helper->getVendorLogoUrl($chat->getVendorId());
                    $responseData['chats']['chat_'.$chat->getId()] = $chatData;
                }
            }
            
            /*Get current vendor livechat info*/
            $vendorId = $this->getRequest()->getParam('vendor_id');
            if($vendorId){
                $responseData['is_vendor_available'] = $this->_objectManager->create('Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator')
                    ->isVendorAvailable($vendorId); 
            }
            
            /* Save visited URL*/
            $url = trim($this->getRequest()->getParam('url'),'/');
            if($url && $vendorId){
                /** @var \Vnecoms\VendorsLiveChat\Model\Url */
                $urlObj = $this->_objectManager->create('Vnecoms\VendorsLiveChat\Model\Url');
                $responseData['current_url_id'] = $urlObj->getResource()->isVisitedUrl($chatSession->getId(), $url);
                if(!$responseData['current_url_id']){
                    $urlObj->setData([
                        'session_id'    => $chatSession->getId(),
                        'visitor_id'    => $visitor->getId(),
                        'url'           => $url,
                        'vendor_id'     => $vendorId,
                        'title'         => $this->getRequest()->getParam('page_title'),
                        'is_online'     => 1,
                    ]);
                    $urlObj->save();
                    $responseData['current_url_id'] = $urlObj->getId();
                }
            }
        }catch (LocalizedException $e){
            $responseData = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            
        }catch (\Exception $e){
            $this->_objectManager->create('Psr\Log\LoggerInterface')->error($e->getMessage());
            $responseData = [
                'success' => false,
                'message' => __('Something went wrong. Try to refresh your page.'),
            ];
        }
        $response->setData($responseData);
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
}
