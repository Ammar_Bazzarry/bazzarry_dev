<?php

namespace Vnecoms\VendorsLiveChat\Controller\Index;

use Magento\Framework\App\Action\Context;
use Vnecoms\VendorsLiveChat\Model\Message;

class SendOfflineMessage extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;
    
    /**
     * 
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        $this->date = $date;
        $this->localeDate = $localeDate;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        try{
            $request = $this->getRequest();
            $vendorId = $request->getParam('vendor_id');
            if(!$vendorId){
                throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
            }
            
            $signature = $request->getParam('signature');
            /** @var \Vnecoms\VendorsLiveChat\Model\Visitor */
            $visitor = $this->_objectManager->create('Vnecoms\VendorsLiveChat\Model\Visitor');
            $visitor->load($signature, 'signature');
            if(!$visitor->getId()) throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
            
            $name = $request->getParam('name');
            $email = $request->getParam('email');
            /* Update name and Email*/
            if($visitor->getName() != $name || $visitor->getEmail() != $email){
                $visitor->setName($name)->setEmail($email)->save();
            }

            $chatSession = $this->helper->getVisitorSession($this->getRequest()->getParam('session_id'));
            
            /* Create new chat session */
            /** @var \Vnecoms\VendorsLiveChat\Model\Visitor */
            $chat = $this->_objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat');
            $chat->setData([
                'visitor_id'    => $visitor->getId(),
                'vendor_id'     => $vendorId,
                'session_id'    => $chatSession->getId(),
                'status'        => \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_CLOSED,
                'is_unread'     => 1,
            ])->save();
            
            if($message = $this->getRequest()->getParam('message')){
                $chat->addMessage($visitor->getId(), Message::SENDER_TYPE_VISITOR, $visitor->getName(), $message);
            }
            
            $response->setData([
                'success' => true,
            ]);
        }catch(\Exception $e){
            $response->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
 
}
