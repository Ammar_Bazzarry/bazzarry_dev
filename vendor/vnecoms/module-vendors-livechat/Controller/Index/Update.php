<?php

namespace Vnecoms\VendorsLiveChat\Controller\Index;

use Magento\Framework\App\Action\Context;
use Vnecoms\VendorsLiveChat\Model\Message;

class Update extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;
    
    /**
     * 
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        $this->date = $date;
        $this->localeDate = $localeDate;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        try{
            $request = $this->getRequest();
            $chatData = $request->getParam('chat_ids');
            if(!$chatData){
                throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
            }
            $chatData = explode(',', $chatData);
            $chatIds = [];
            $lastMessageIds = [];
            foreach($chatData as $chat){
                $chat = explode(':', $chat);
                $chatIds[] = $chat[0];
                $lastMessageIds[$chat[0]] = $chat[1];
            }
            
            /** @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection */
            $collection = $this->_objectManager->get('Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection');
            $collection->addFieldToFilter('chat_id',['in' => $chatIds]);            
            $chats = [];
            $hasNewMessages = false; 
            foreach($collection as $chat){
                $messageCollection = $chat->getMessageCollection()
                    ->addFieldToFilter('message_id',['gt' => $lastMessageIds[$chat->getId()]]);
                
                if(!$messageCollection->count()) continue;
                
                $hasNewMessages = true;
                $chatData = [
                    'chat_id'   => $chat->getId(),
                    'status'    => $chat->getStatus(),
                ];
                $chatData['messages'] = $messageCollection->getData();
                $chats[] = $chatData;
            }

            
            $response->setData([
                'success' => true,
                'has_new_message' => $hasNewMessages,
                'chats' => $chats,
            ]);
        }catch(\Exception $e){
            $response->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
 
}
