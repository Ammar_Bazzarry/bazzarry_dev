<?php
namespace Vnecoms\VendorsLiveChat\Controller\Vendors\Chat;

use Magento\Framework\Exception\LocalizedException;
class View extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    protected $_aclResource = 'Vnecoms_VendorsLiveChat::livechat';
    
    /**
     * @return void
     */
    public function execute()
    {
        try{
            $chatId = $this->getRequest()->getParam('id');
            $chat = $this->_objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat')->load($chatId);

            if(
                !$chat->getId() ||
                ($this->_session->getVendor()->getId() != $chat->getVendorId())
            ){
                throw new LocalizedException(__("The chat does not exist."));
            }
            if($chat->getIsUnread()){
                $chat->setIsUnread(false)->save();
            }
            $this->_coreRegistry->register('chat', $chat);
            $this->_coreRegistry->register('current_chat', $chat);
            
            $this->_initAction();
            $this->setActiveMenu('Vnecoms_VendorsLiveChat::livechat_history');
            $this->_view->getPage()->getConfig()->getTitle()->prepend(__("Visitors"));
            $this->_addBreadcrumb(__("Live Chat"), __("Live Chat"), $this->getUrl('livechat/history'));
            $this->_addBreadcrumb(__("View"), __("View"));
            $this->_view->renderLayout();
        }catch (LocalizedException $e){
            $this->messageManager->addError($e->getMessage());
            return $this->_redirect('livechat/history');
        }catch(\Exception $e){
            $this->messageManager->addError(__('Something went wrong.'));
            return $this->_redirect('livechat/history');
        }
    }
}
