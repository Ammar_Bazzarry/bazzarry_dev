<?php
namespace Vnecoms\VendorsLiveChat\Controller\Vendors\History;

class Index extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    protected $_aclResource = 'Vnecoms_VendorsLiveChat::livechat';
    
    /**
     * @return void
     */
    public function execute()
    {
        $this->getRequest()->setParam('vendor_id', $this->_session->getVendor()->getId());
        $this->_initAction();
        $this->setActiveMenu('Vnecoms_VendorsLiveChat::livechat_history');
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__("History"));
        $this->_addBreadcrumb(__("Live Chat"), __("Live Chat"));
        $this->_addBreadcrumb(__("History"), __("History"));
        $this->_view->renderLayout();
    }
}
