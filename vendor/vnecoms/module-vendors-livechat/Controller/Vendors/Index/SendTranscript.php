<?php

namespace Vnecoms\VendorsLiveChat\Controller\Vendors\Index;

use Vnecoms\Vendors\App\Action\Context;

class SendTranscript extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;
    
    /**
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        try{
            $request = $this->getRequest();
            $chatId = $request->getParam('chat_id');
            if(!$chatId){
                throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
            }
            
            /** @var \Vnecoms\VendorsLiveChat\Model\Chat */
            $chat = $this->_objectManager->create('Vnecoms\VendorsLiveChat\Model\Chat');
            $chat->load($chatId);
            if(!$chat->getId()) throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
            
            if($chat->getVendorId() != $this->_session->getVendor()->getId()){
                throw new \Exception(__("You do not have permission to do this action."));
            }
            $emails = str_replace(' ','',$request->getParam('email'));
            if(!$emails) throw new \Exception(__("Please enter your email."));
            
            /* Create new transcript */
            $emails = explode(",", $emails);
            foreach($emails as $email){
                /** @var \Vnecoms\VendorsLiveChat\Model\Transcript */
                $transcript = $this->_objectManager->create('Vnecoms\VendorsLiveChat\Model\Transcript');
                $transcript->setData([
                    'chat_id'       => $chat->getId(),
                    'email'         => $email,
                ])->save();
            }
            $response->setData([
                'success' => true,
            ]);
        }catch(\Exception $e){
            $response->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
 
}
