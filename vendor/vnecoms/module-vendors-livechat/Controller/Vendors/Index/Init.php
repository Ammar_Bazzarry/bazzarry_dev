<?php

namespace Vnecoms\VendorsLiveChat\Controller\Vendors\Index;

use Vnecoms\Vendors\App\Action\Context;

class Init extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    protected $_aclResource = 'Vnecoms_VendorsLiveChat::livechat';
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;

    
    /**
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        $this->date = $date;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        $responseData = [];
        try{
            /* Get opening chat*/
            /** @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection */
            $chatCollection = $this->_objectManager->get('Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection')
                ->addFieldToFilter('vendor_id', $this->_session->getVendor()->getId())
                ->addFieldToFilter('status', ['neq' => \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_CLOSED]);
            
            $responseData['chats'] = [];
            $responseData['pending_chats'] = [];
            $chatCount = 0;
            $pendingChatsCount = 0;
            if($chatCollection->count()){
                foreach($chatCollection as $chat){
                    if(!$chat->getOperatorIds()){
                        $responseData['pending_chats']['chat_'.$chat->getId()] = $this->initChatData($chat);
                        $pendingChatsCount++;
                        continue;
                    }elseif(
                        $chat->getOperatorIds() &&
                        !in_array($this->_session->getCustomerId(), $chat->getOperatorIds())
                    ) continue;
                    $chatCount ++;
                    $responseData['chats']['chat_'.$chat->getId()] = $this->initChatData($chat);
                }
            }
            $responseData['chat_count'] = $chatCount;
            $responseData['pending_chat_count'] = $pendingChatsCount;
            $responseData['success'] = true;
        }catch (\Exception $e){
            $this->_objectManager->get('Psr\Log\LoggerInterface')->error($e->getMessage());
            $response->setData([
                'success' => false,
                'message' => __('Something went wrong. Try to refresh your page.')
            ]);
        }
        $response->setData($responseData);
        
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
    
    /**
     * @param \Vnecoms\VendorsLiveChat\Model\Chat $chat
     * @return array
     */
    public function initChatData(\Vnecoms\VendorsLiveChat\Model\Chat $chat){
        $chatData = $chat->getData();
        $chatData['messages']   = $chat->getMessageCollection()->getData();
        $chatData['visitor']    = $chat->getVisitor()->getData();
        return $chatData;
    }
}
