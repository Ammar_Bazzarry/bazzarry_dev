<?php

namespace Vnecoms\VendorsLiveChat\Controller\Vendors\Index;

use Vnecoms\Vendors\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

class Savevisitor extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    protected $_aclResource = 'Vnecoms_VendorsLiveChat::livechat';
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;
    
    /**
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        $responseData = [];
        try{
            $visitorId = $this->getRequest()->getParam('visitor_id');
            /** @var Vnecoms\VendorsLiveChat\Model\Visitor */
            $visitor = $this->_objectManager->create('Vnecoms\VendorsLiveChat\Model\Visitor');
            $visitor->load($visitorId);
            if(!$visitor->getId()) throw new LocalizedException(__('Visitor does not exist.'));
            $data = $this->getRequest()->getParams();
            unset($data['visitor_id']);
            
            $visitor->addData($data)
                ->save();
            
            $responseData['success']    = true;
        }catch (LocalizedException $e){
            $responseData = [
                'success' => false,
                'message' => __($e->getMessage())
            ];
        }catch (\Exception $e){
            $this->_objectManager->get('Psr\Log\LoggerInterface')->error($e->getMessage());
            $responseData = [
                'success' => false,
                'message' => __('Something went wrong. Try to refresh your page.')
            ];
        }
        $response->setData($responseData);
        
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
}
