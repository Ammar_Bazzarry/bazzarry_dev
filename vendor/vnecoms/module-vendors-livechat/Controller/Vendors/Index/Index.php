<?php
namespace Vnecoms\VendorsLiveChat\Controller\Vendors\Index;

class Index extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    protected $_aclResource = 'Vnecoms_VendorsLiveChat::livechat';
    
    /**
     * @return void
     */
    public function execute()
    {
        $this->_initAction();
        $this->setActiveMenu('Vnecoms_VendorsLiveChat::livechat_visitor');
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__("Visitors"));
        $this->_addBreadcrumb(__("Live Chat"), __("Live Chat"));
        $this->_addBreadcrumb(__("Visitors"), __("Visitors"));
        $this->_view->renderLayout();
    }
}
