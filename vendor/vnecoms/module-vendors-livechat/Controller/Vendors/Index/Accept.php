<?php

namespace Vnecoms\VendorsLiveChat\Controller\Vendors\Index;

use Vnecoms\Vendors\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

class Accept extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    protected $_aclResource = 'Vnecoms_VendorsLiveChat::livechat';
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;

    
    /**
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        $this->date = $date;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        $responseData = [];
        try{
            /* Get opening chat*/
            /** @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection */
            $chatCollection = $this->_objectManager->get('Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection')
                ->addFieldToFilter('vendor_id', $this->_session->getVendor()->getId())
                ->addFieldToFilter('status', \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_WAITING);

            $pendingChatCount = $chatCollection->count();
            if(!$pendingChatCount){
                throw new LocalizedException(__('There is no pending chat available.'));
            }
            
            $chat = $chatCollection->getFirstItem();
            $chat->addOperator($this->_session->getCustomerId());
            $chat->setStatus(\Vnecoms\VendorsLiveChat\Model\Chat::STATUS_ACCEPTED)->save();
            $responseData['success']    = true;
            $responseData['new_chat']   = $this->initChatData($chat);
            $responseData['pending_chat_count'] = $pendingChatCount - 1;
        }catch (LocalizedException $e){
            $responseData = [
                'success' => false,
                'message' => __($e->getMessage())
            ];
        }catch (\Exception $e){
            $this->_objectManager->get('Psr\Log\LoggerInterface')->error($e->getMessage());
            $responseData = [
                'success' => false,
                'message' => __('Something went wrong. Try to refresh your page.')
            ];
        }
        $response->setData($responseData);
        
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
    
    /**
     * @param \Vnecoms\VendorsLiveChat\Model\Chat $chat
     * @return array
     */
    public function initChatData(\Vnecoms\VendorsLiveChat\Model\Chat $chat){
        $chatData = $chat->getData();
        $chatData['messages']   = $chat->getMessageCollection()->getData();
        $chatData['visitor']    = $chat->getVisitor()->getData();
        return $chatData;
    }
}
