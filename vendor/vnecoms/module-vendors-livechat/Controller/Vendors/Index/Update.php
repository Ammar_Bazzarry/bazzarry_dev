<?php

namespace Vnecoms\VendorsLiveChat\Controller\Vendors\Index;

use Vnecoms\Vendors\App\Action\Context;

class Update extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    protected $_aclResource = 'Vnecoms_VendorsLiveChat::livechat';
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;

    
    /**
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        $this->date = $date;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        try{
            $request = $this->getRequest();
            $chatData = $request->getParam('chat_ids');
            $chats = [];
            $hasNewMessages = false;
            $lastMessageIds = [];
            $pendingChatCount = 0;
            if($chatData){
                $chatData = explode(',', $chatData);               
                foreach($chatData as $chat){
                    $chat = explode(':', $chat);
                    $lastMessageIds[$chat[0]] = $chat[1];
                    $chats['chat_'.$chat[0]] = [
                        'chat_id'   => $chat[0],
                        'status'    => \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_CLOSED,
                    ];
                }
            }
                
            /** @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection */
            $collection = $this->_objectManager->get('Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection')
                ->addFieldToFilter('vendor_id',['in' => $this->_session->getVendor()->getId()])
                ->addFieldToFilter('status', ['neq' => \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_CLOSED]);
                   
            foreach($collection as $chat){
                if(!$chat->getOperatorIds()){
                    $pendingChatCount++;
                    continue;
                }elseif(
                    $chat->getOperatorIds() &&
                    !in_array($this->_session->getCustomerId(), $chat->getOperatorIds())
                ) continue;
                
                $messageCollection = $chat->getMessageCollection();
                if(isset($lastMessageIds[$chat->getId()])){
                    $messageCollection->addFieldToFilter('message_id',['gt' => $lastMessageIds[$chat->getId()]]);
                }
                
                $chatData = [
                    'chat_id'   => $chat->getId(),
                    'status'    => $chat->getStatus(),
                ];
                if($messageCollection->count()){
                    $hasNewMessages = true;
                    $chatData['messages'] = $messageCollection->getData();
                }
                $chats['chat_'.$chat->getId()] = $chatData;
            }

            $response->setData([
                'success' => true,
                'has_new_message' => $hasNewMessages,
                'pending_chat_count' => $pendingChatCount,
                'chats' => $chats,
            ]);
        }catch(\Exception $e){
            $response->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
}
