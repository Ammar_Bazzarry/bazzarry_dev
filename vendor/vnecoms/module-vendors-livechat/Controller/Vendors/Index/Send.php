<?php

namespace Vnecoms\VendorsLiveChat\Controller\Vendors\Index;

use Vnecoms\Vendors\App\Action\Context;
use Vnecoms\VendorsLiveChat\Model\Message;

class Send extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    protected $_aclResource = 'Vnecoms_VendorsLiveChat::livechat';
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;

    
    /**
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        $this->date = $date;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        try{
            $request = $this->getRequest();
            $chatId = $request->getParam('chat_id');
            /** @var \Vnecoms\VendorsLiveChat\Model\Chat */
            $chat = $this->_objectManager->get('Vnecoms\VendorsLiveChat\Model\Chat');
            $chat->load($chatId);
        
            if(!$chat->getChatId()){
                throw new \Exception(__("The request is not valid. Please refresh the page and try again."));
            }
        
            $message = trim($request->getParam('message'));
            if(!$message) throw new \Exception(__("Please enter your message."));
        
            /* Create new chat message */
            /** @var \Vnecoms\VendorsLiveChat\Model\Message */
            $messageObj = $this->_objectManager->get('Vnecoms\VendorsLiveChat\Model\Message');
            $messageObj->setData([
                'chat_id'       => $chat->getId(),
                'sender_id'     => $this->_session->getCustomerId(),
                'sender_type'   => Message::SENDER_TYPE_OPERATOR,
                'sender_name'   => $this->_session->getCustomer()->getName(),
                'message'       => $message,
            ])->save();
        
            /*Get only new Messages based on last_message_id*/
            /** @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Message\Collection */
            $messageCollection = $this->_objectManager->get('Vnecoms\VendorsLiveChat\Model\ResourceModel\Message\Collection');
            $messageCollection->addFieldToFilter('chat_id', $chat->getId())
            ->addFieldToFilter('message_id',['gt' => $request->getParam('last_message_id')]);
        
            $response->setData([
                'success' => true,
                'messages' => $messageCollection->getData(),
                'message_key' => $request->getParam('message_key'),
            ]);
        }catch(\Exception $e){
            $response->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
}
