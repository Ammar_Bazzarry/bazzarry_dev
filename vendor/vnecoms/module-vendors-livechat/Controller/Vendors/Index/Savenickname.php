<?php

namespace Vnecoms\VendorsLiveChat\Controller\Vendors\Index;

use Vnecoms\Vendors\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

class Savenickname extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    protected $_aclResource = 'Vnecoms_VendorsLiveChat::livechat';
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Helper\Data
     */
    protected $helper;

    /**
     * @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator
     */
    protected $operator;
    
    /**
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Vnecoms\VendorsLiveChat\Helper\Data $helper
     * @param \Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator $operator
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vnecoms\VendorsLiveChat\Helper\Data $helper,
        \Vnecoms\VendorsLiveChat\Model\ResourceModel\Operator $operator
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        $this->operator = $operator;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        $responseData = [];
        try{
            /* Get opening chat*/
            $operatorId = $this->_session->getCustomerId();
            $nickname = $this->getRequest()->getParam('nickname');
            $this->operator->updateOperatorData($operatorId, 'livechat_nickname', $nickname);
            
            $responseData['success']    = true;
            $responseData['operator']   = $this->operator->getOperatorData($operatorId);
        }catch (LocalizedException $e){
            $responseData = [
                'success' => false,
                'message' => __($e->getMessage())
            ];
        }catch (\Exception $e){
            $this->_objectManager->get('Psr\Log\LoggerInterface')->error($e->getMessage());
            $responseData = [
                'success' => false,
                'message' => __('Something went wrong. Try to refresh your page.')
            ];
        }
        $response->setData($responseData);
        
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
}
