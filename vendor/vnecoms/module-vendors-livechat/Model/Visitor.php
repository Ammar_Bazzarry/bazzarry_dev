<?php
namespace Vnecoms\VendorsLiveChat\Model;

class Visitor extends \Magento\Framework\Model\AbstractModel
{
    const TYPE_SYSTEM       = 0;
    const TYPE_VISITOR      = 1;
    const TYPE_OPERATOR     = 2;
    /**
     * Model event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'vendor_livechat_visitor';
    
    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'visitor';
    
    /**
     * Initialize customer model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Vnecoms\VendorsLiveChat\Model\ResourceModel\Visitor');
    }
}
