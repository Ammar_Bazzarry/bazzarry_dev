<?php
namespace Vnecoms\VendorsLiveChat\Model;

use Vnecoms\VendorsLiveChat\Model\Chat;

class ChatAction extends \Magento\Framework\Model\AbstractModel
{
    const ACTION_CLOSE  = 'close';
    
    /**
     * Model event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'vendor_livechat_chat_action';
    
    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'chat_action';
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\ChatFactory
     */
    protected $chatFactory;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\Chat
     */
    protected $chat;
    
    /**
     * Initialize customer model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Vnecoms\VendorsLiveChat\Model\ResourceModel\ChatAction');
    }
    
    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Vnecoms\VendorsLiveChat\Model\ResourceModel\ChatAction $resource
     * @param \Vnecoms\VendorsLiveChat\Model\ResourceModel\ChatAction\Collection $resourceCollection
     * @param \Vnecoms\VendorsLiveChat\Model\ChatFactory $chatFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Vnecoms\VendorsLiveChat\Model\ResourceModel\ChatAction $resource,
        \Vnecoms\VendorsLiveChat\Model\ResourceModel\ChatAction\Collection $resourceCollection,
        \Vnecoms\VendorsLiveChat\Model\ChatFactory $chatFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->chatFactory = $chatFactory;
    }
    
    /**
     * @return \Vnecoms\VendorsLiveChat\Model\Chat
     */
    public function getChat(){
        if(!$this->chat){
            $this->chat = $this->chatFactory->create()->load($this->getChatId());
        }
        return $this->chat;
    }
    
    /**
     * @return \Vnecoms\VendorsLiveChat\Model\ChatAction
     */
    public function process(){
        switch ($this->getAction()){
            case self::ACTION_CLOSE:
                $this->getChat()->setStatus(Chat::STATUS_CLOSED)->save();
                break;
        }
        
        return $this;
    }
}
