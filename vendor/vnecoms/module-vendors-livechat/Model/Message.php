<?php
namespace Vnecoms\VendorsLiveChat\Model;

class Message extends \Magento\Framework\Model\AbstractModel
{    
    const SENDER_TYPE_SYSTEM    = 0;
    const SENDER_TYPE_VISITOR   = 1;
    const SENDER_TYPE_OPERATOR  = 2;
    /**
     * Model event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'vendor_livechat_message';
    
    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'message';
    
    /**
     * Initialize customer model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Vnecoms\VendorsLiveChat\Model\ResourceModel\Message');
    }
}
