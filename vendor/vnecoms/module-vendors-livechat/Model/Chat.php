<?php
namespace Vnecoms\VendorsLiveChat\Model;

class Chat extends \Magento\Framework\Model\AbstractModel
{    
    const STATUS_WAITING    = 0;
    const STATUS_ACCEPTED   = 1;
    const STATUS_CLOSED     = 2;
    
    /**
     * Model event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'vendor_livechat_chat';
    
    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'chat';
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\MessageFactory
     */
    protected $messageFactory;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\ResourceModel\Message\Collection
     */
    protected $messageCollection;
    
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\VisitorFactory
     */
    protected $visitorFactory;
    
    /**
     * @var * @param \Vnecoms\VendorsLiveChat\Model\UrlFactory
     */
    protected $urlFactory;
    
    /**
     * @var array
     */
    protected $urls;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\Visitor
     */
    protected $visitor;
    
    /**
     * Initialize customer model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat');
    }
    
    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat $resource
     * @param \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection $resourceCollection
     * @param \Vnecoms\VendorsLiveChat\Model\MessageFactory $messageFactory
     * @param \Vnecoms\VendorsLiveChat\Model\VisitorFactory $visitorFactory
     * @param \Vnecoms\VendorsLiveChat\Model\UrlFactory $urlFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat $resource,
        \Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Collection $resourceCollection,
        \Vnecoms\VendorsLiveChat\Model\MessageFactory $messageFactory,
        \Vnecoms\VendorsLiveChat\Model\VisitorFactory $visitorFactory,
        \Vnecoms\VendorsLiveChat\Model\UrlFactory $urlFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->messageFactory   = $messageFactory;
        $this->visitorFactory   = $visitorFactory;
        $this->urlFactory       = $urlFactory;
    }
    
    /**
     * @return \Vnecoms\VendorsLiveChat\Model\ResourceModel\Message\Collection
     */
    public function getMessageCollection(){
        if(!$this->messageCollection){
            $this->messageCollection = $this->messageFactory->create()->getCollection();
            $this->messageCollection->addFieldToFilter('chat_id', $this->getId());
            $this->messageCollection->setOrder('message_id', 'ASC');
        }
        
        return $this->messageCollection;
    }
    
    /**
     * @return array
     */
    public function getBrowserUrls(){
        if(!$this->urls){
            $collection = $this->urlFactory->create()
                ->getCollection()
                ->addFieldToFilter('session_id', $this->getSessionId())
                ->addFieldToFilter('vendor_id', $this->getVendorId())
                ->setOrder('url_id', 'ASC');
            $this->urls = [];
            foreach($collection as $url){
                $this->urls[] = [
                    'url_id'        => $url->getId(),
                    'title'         => $url->getTitle(),
                    'url'           => $url->getUrl(),
                    'is_online'     => $url->getIsOnline(),
                    'created_at'    => $url->getCreatedAt(),
                ];
            }
        }
        
        return $this->urls;
    }
    
    /**
     * @param int $customerId
     */
    public function addOperator($customerId){
        $this->getResource()->addOperator($this, $customerId);
        return $this;
    }
    
    /**
     * @param int $customerId
     */
    public function removeOperator($customerId){
        $this->getResource()->removeOperator($this, $customerId);
        return $this;
    }
    
    /**
     * @param int $customerId
     */
    public function removeAllOperator(){
        $this->getResource()->removeAllOperator($this);
        return $this;
    }
    
    /**
     * @param int $senderId
     * @param int $senderType
     * @param string $senderName
     * @param string $message
     * @return \Vnecoms\VendorsLiveChat\Model\Chat
     */
    public function addMessage($senderId, $senderType, $senderName, $message){
        if(!$this->getId()) return false;
        $messageObj = $this->messageFactory->create();
        $messageObj->setData([
            'chat_id'       => $this->getId(),
            'sender_id'     => $senderId,
            'sender_type'   => $senderType,
            'sender_name'   => $senderName,
            'message'       => $message,
        ])->save();
        
        return $messageObj->getData();
    }
    /**
     * @return \Vnecoms\VendorsLiveChat\Model\Visitor
     */
    public function getVisitor(){
        if(!$this->visitor){
            $this->visitor = $this->visitorFactory->create()->load($this->getVisitorId());
        }
        
        return $this->visitor;
    }
    
    /**
     * Get Operator Ids
     * 
     * @return array
     */
    public function getOperatorIds(){
        if(!$this->hasData('operator_ids')){
            $this->setData('operator_ids', $this->getResource()->getOperatorIds($this));
        }
        return $this->getData('operator_ids');
    }
    
    /**
     * @param int $sessionId
     */
    public function updateSessionId($sessionId){
        $this->getResource()->updateBrowserUrlsSessionId($this->getSessionId(), $sessionId);
        $this->setSessionId($sessionId)->save();
    }
    
    /**
     * Reset
     */
    public function reset(){
        $this->messageCollection = null;
        $this->visitor = null;
        $this->unsetData('operator_ids');
    }
}
