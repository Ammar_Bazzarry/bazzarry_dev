<?php
namespace Vnecoms\VendorsLiveChat\Model\Source;

class ServerType implements \Magento\Framework\Option\ArrayInterface
{
    const TYPE_DEFAULT  = 0;
    const TYPE_CUSTOM   = 1;
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::TYPE_DEFAULT, 'label' => __('Default')],
            ['value' => self::TYPE_CUSTOM, 'label' => __('Custom')],
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            self::TYPE_DEFAULT => __('Default'),
            self::TYPE_CUSTOM => __('Custom')
        ];
    }
}
