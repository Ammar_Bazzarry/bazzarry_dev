<?php
namespace Vnecoms\VendorsLiveChat\Model;

class Session extends \Magento\Framework\Model\AbstractModel
{    
    /**
     * Model event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'vendor_livechat_session';
    
    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'session';
    
    /**
     * Initialize customer model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Vnecoms\VendorsLiveChat\Model\ResourceModel\Session');
    }
}
