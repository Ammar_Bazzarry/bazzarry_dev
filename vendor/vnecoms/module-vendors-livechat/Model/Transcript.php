<?php
namespace Vnecoms\VendorsLiveChat\Model;

class Transcript extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Model event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'vendor_livechat_chat_transcript';
    
    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'chat_transcript';
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\ChatFactory
     */
    protected $chatFactory;
    
    /**
     * @var \Vnecoms\VendorsLiveChat\Model\Chat
     */
    protected $chat;
    
    /**
     * Initialize customer model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Vnecoms\VendorsLiveChat\Model\ResourceModel\Transcript');
    }
    
    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Vnecoms\VendorsLiveChat\Model\ResourceModel\Transcript $resource
     * @param \Vnecoms\VendorsLiveChat\Model\ResourceModel\Transcript\Collection $resourceCollection
     * @param \Vnecoms\VendorsLiveChat\Model\ChatFactory $chatFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Vnecoms\VendorsLiveChat\Model\ResourceModel\Transcript $resource,
        \Vnecoms\VendorsLiveChat\Model\ResourceModel\Transcript\Collection $resourceCollection,
        \Vnecoms\VendorsLiveChat\Model\ChatFactory $chatFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->chatFactory = $chatFactory;
    }
    
    /**
     * @return \Vnecoms\VendorsLiveChat\Model\Chat
     */
    public function getChat(){
        if(!$this->chat){
            $this->chat = $this->chatFactory->create()->load($this->getChatId());
        }
        return $this->chat;
    }
}
