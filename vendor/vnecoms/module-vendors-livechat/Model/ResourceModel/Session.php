<?php

namespace Vnecoms\VendorsLiveChat\Model\ResourceModel;

class Session extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ves_vendor_livechat_session', 'session_id');
    }
}
