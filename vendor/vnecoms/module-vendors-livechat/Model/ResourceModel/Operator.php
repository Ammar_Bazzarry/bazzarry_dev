<?php

namespace Vnecoms\VendorsLiveChat\Model\ResourceModel;

class Operator extends \Vnecoms\Vendors\Model\ResourceModel\Vendor
{
    const STATUS_INVISIBLE  = 0;
    const STATUS_AVAILABLE  = 1;
    const STATUS_AWAY       = 2;
    
    /**
     * Check if there is an operator of vendor account available for chat.
     * 
     * @param int|\Vnecoms\Vendors\Model\Vendor $vendorId
     * @return boolean
     */
    public function isVendorAvailable($vendorId){
        if($vendorId instanceof \Vnecoms\Vendors\Model\Vendor){
            $vendorId = $vendorId->getId();
        }
        
        $conn = $this->getConnection();
        $userTbl = $this->getTable('ves_vendor_user');
        
        $select = $conn->select();
        $select->from(
            $userTbl,
            ['is_available' => 'count(customer_id)']
        )->where(
            'vendor_id = :vendor_id'
        )->where(
            'livechat_status = :livechat_status'
        );
        $bind = ['vendor_id' => $vendorId, 'livechat_status' => self::STATUS_AVAILABLE];
        return (bool) $conn->fetchOne($select, $bind);
    }
    
    /**
     * Get User Data
     * 
     * @param int|\Magento\Customer\Model\Customer $customerId
     * @return multitype:
     */
    public function getOperatorData($customerId){
        if($customerId instanceof \Magento\Customer\Model\Customer){
            $customerId = $customerId->getId();
        }
        $conn = $this->getConnection();
        $userTbl = $this->getTable('ves_vendor_user');
        
        $select = $conn->select();
        $select->from(
            $userTbl,
            ['vendor_id', 'customer_id', 'livechat_nickname', 'livechat_avatar', 'livechat_status', 'livechat_token']
        )->where(
            'customer_id = :customer_id'
        );
        $bind = ['customer_id' => $customerId];
        
        return $conn->fetchRow($select, $bind);
    }
    
    /**
     * Get Operator Data By Token
     * 
     * @param string $token
     * @return multitype:
     */
    public function getOperatorDataByToken($token){
        $conn = $this->getConnection();
        $userTbl = $this->getTable('ves_vendor_user');

        $select = $conn->select();
        $select->from(
            $userTbl,
            ['vendor_id', 'customer_id', 'livechat_nickname', 'livechat_avatar', 'livechat_status', 'livechat_token']
        )->where(
            'livechat_token = :token'
        );
        $bind = ['token' => $token];
        return $conn->fetchRow($select, $bind);
    }
    
    /**
     * @param int $customerId
     * @return string
     */
    public function getToken($customerId){
        $data = $this->getOperatorData($customerId);
        if(!isset($data['livechat_token']) || !$data['livechat_token']){
            /*Generate token*/
            $token = md5(md5($customerId).rand(1, 100000));
            $this->updateOperatorData($customerId, 'livechat_token', $token);
            
            unset($this->userData[$customerId]);
            return $token;
        }
        return $data['livechat_token'];
    }
    
    /**
     * Update Operator Data
     * 
     * @param int $customerId
     * @param string $field
     * @param string $value
     * @return \Vnecoms\VendorsSubAccount\Model\ResourceModel\Operator
     */
    public function updateOperatorData($customerId, $field, $value){
        if($customerId instanceof \Magento\Customer\Model\Customer){
            $customerId = $customerId->getId();
        }
        $conn = $this->getConnection();
        $userTbl = $this->getTable('ves_vendor_user');
        if(!in_array($field, ['livechat_nickname', 'livechat_avatar', 'livechat_status', 'livechat_token'])){
            return;
        }
        $bind = [$field => $value];

        $conn->update($userTbl, $bind,'customer_id='.$customerId);
        
        return $this;
    }
    
    /**
     * Update Operator Data By TOken
     *
     * @param string $token
     * @param string $field
     * @param string $value
     * @return \Vnecoms\VendorsSubAccount\Model\ResourceModel\Operator
     */
    public function updateOperatorDataByToken($token, $field, $value){
        $conn = $this->getConnection();
        $userTbl = $this->getTable('ves_vendor_user');
        if(!in_array($field, ['livechat_nickname', 'livechat_avatar', 'livechat_status', 'livechat_token'])){
            return;
        }
        $bind = [$field => $value];
    
        $conn->update($userTbl, $bind,'livechat_token="'.$token.'"');
    
        return $this;
    }
}
