<?php

namespace Vnecoms\VendorsLiveChat\Model\ResourceModel;

class Visitor extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ves_vendor_livechat_visitor', 'visitor_id');
    }
}
