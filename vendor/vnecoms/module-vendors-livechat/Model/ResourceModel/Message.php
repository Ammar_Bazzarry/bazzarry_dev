<?php

namespace Vnecoms\VendorsLiveChat\Model\ResourceModel;

class Message extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ves_vendor_livechat_chat_message', 'message_id');
    }
}
