<?php

namespace Vnecoms\VendorsLiveChat\Model\ResourceModel;

class Chat extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ves_vendor_livechat_chat', 'chat_id');
    }
    
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $chat){
        parent::_afterSave($chat);
        
        if($chat->getStatus() == \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_CLOSED){
            /* Remove all operator from chat*/
            $this->removeAllOperator($chat->getId());
        }
        return $this;
    }
    
    /**
     * @param \Vnecoms\VendorsLiveChat\Model\Chat|int $chatId
     * @param int $customerId
     */
    public function addOperator($chatId, $customerId){
        if($chatId instanceof \Vnecoms\VendorsLiveChat\Model\Chat){
            $chatId = $chatId->getId(); 
        }
        
        $this->getConnection()->insert(
            $this->getTable('ves_vendor_livechat_chat_operator'),
            ['chat_id' => $chatId, 'operator_id' => $customerId]
        );
    }
    
    /**
     * Remove an Operator from chat
     * 
     * @param \Vnecoms\VendorsLiveChat\Model\Chat|int $chatId
     * @param int $customerId
     */
    public function removeOperator($chatId, $customerId){
        if($chatId instanceof \Vnecoms\VendorsLiveChat\Model\Chat){
            $chatId = $chatId->getId();
        }
        $this->getConnection()->delete(
            $this->getTable('ves_vendor_livechat_chat_operator'),
            ['chat_id' => $chatId, 'operator_id' => $customerId]
        );
    }
    
    /**
     * Remove All Operator From Chat
     * 
     * @param \Vnecoms\VendorsLiveChat\Model\Chat|int $chatId
     */
    public function removeAllOperator($chatId){
        if($chatId instanceof \Vnecoms\VendorsLiveChat\Model\Chat){
            $chatId = $chatId->getId();
        }
        $this->getConnection()->delete(
            $this->getTable('ves_vendor_livechat_chat_operator'),
            ['chat_id' => $chatId]
        );
    }
    
    /**
     * Get operator Ids
     * 
     * @param \Vnecoms\VendorsLiveChat\Model\Chat $chat
     * @return multitype:
     */
    public function getOperatorIds(\Vnecoms\VendorsLiveChat\Model\Chat $chat){
        $connection = $this->getConnection();
        $select = $connection->select();
        $select->from(
            $this->getTable('ves_vendor_livechat_chat_operator'),
            ['operator_id']
        )->where(
            'chat_id = :chat_id'
        );
        $bind = ['chat_id' => $chat->getId()];
        return $connection->fetchCol($select, $bind);
    }
    
    /**
     * Get pending chat count by vendor Id
     *
     * @param int $vendorId
     * @return int
     */
    public function getPendingChatCount($vendorId){
        $connection = $this->getConnection();
        $select = $connection->select();
        $select->from(
            $this->getTable('ves_vendor_livechat_chat'),
            ['pending_chat_count' => 'COUNT(chat_id)']
        )->where(
            'vendor_id = :vendor_id'
        )->where(
            'status = :status'
        );
        $bind = ['vendor_id' => $vendorId, 'status' => \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_WAITING];
    
        return (int)$connection->fetchOne($select, $bind);
    }
    
    /**
     * Update session id for all browser urls
     * 
     * @param int $oldSessionId
     * @param int $sessionId
     */
    public function updateBrowserUrlsSessionId($oldSessionId, $sessionId){
        $connection = $this->getConnection();
        $connection->update(
            $this->getTable('ves_vendor_livechat_browser_urls'),
            ['session_id' => $sessionId],
            'session_id ='.$oldSessionId
        );
    }
}
