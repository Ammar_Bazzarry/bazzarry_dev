<?php
namespace Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * App page collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'chat_id';


    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Vnecoms\VendorsLiveChat\Model\Chat', 'Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat');
    }
}
