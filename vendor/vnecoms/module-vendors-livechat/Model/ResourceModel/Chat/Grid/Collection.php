<?php

namespace Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat\Grid;

use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Psr\Log\LoggerInterface as Logger;

/**
 * App page collection
 */
class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager
    ) {
        $mainTable = 'ves_vendor_livechat_chat';
        $resourceModel = 'Vnecoms\VendorsLiveChat\Model\ResourceModel\Chat';
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }

    protected function _construct()
    {
        parent::_construct();
    
        $this->addFilterToMap(
            'chat_id',
            'main_table.chat_id'
        );
        $this->addFilterToMap(
            'visitor_id',
            'main_table.visitor_id'
        );
        $this->addFilterToMap(
            'created_at',
            'main_table.created_at'
        );
    }
    
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->joinLeft(
            ['chat_message' => $this->getTable('ves_vendor_livechat_chat_message')],
            'chat_message.chat_id=main_table.chat_id'
        );
        $this->getSelect()->joinLeft(
            ['chat_visitor' => $this->getTable('ves_vendor_livechat_visitor')],
            'chat_visitor.visitor_id=main_table.visitor_id'
        );
        
        $this->getSelect()->group('main_table.chat_id');
        $this->addFieldToFilter('status', \Vnecoms\VendorsLiveChat\Model\Chat::STATUS_CLOSED);
        return $this;
    }
}
