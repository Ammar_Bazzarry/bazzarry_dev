<?php
namespace Vnecoms\VendorsLiveChat\Model\ResourceModel\Message;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * App page collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'message_id';


    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Vnecoms\VendorsLiveChat\Model\Message', 'Vnecoms\VendorsLiveChat\Model\ResourceModel\Message');
    }
}
