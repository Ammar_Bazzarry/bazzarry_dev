<?php

namespace Vnecoms\VendorsLiveChat\Model\ResourceModel;

class Url extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ves_vendor_livechat_browser_urls', 'url_id');
    }
    
    /**
     * Is visited URL
     * 
     * @param string $sessionId
     * @param string $url
     */
    public function isVisitedUrl($sessionId, $url){
        $conn = $this->getConnection();
        
        $select = $conn->select();
        $select->from(
            $this->getMainTable(),
            ['url_id' => 'url_id']
        )->where(
            'session_id = :session_id'
        )->where(
            'url = :url'
        );
        $bind = ['session_id' => $sessionId, 'url' => $url];
        return  $conn->fetchOne($select, $bind);
    }
}
