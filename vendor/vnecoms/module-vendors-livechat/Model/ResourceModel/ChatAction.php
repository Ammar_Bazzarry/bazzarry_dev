<?php

namespace Vnecoms\VendorsLiveChat\Model\ResourceModel;

class ChatAction extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ves_vendor_livechat_chat_action', 'action_id');
    }
    
    /**
     * Delete actions by chat id
     * 
     * @param int $chatId
     * @return \Vnecoms\VendorsLiveChat\Model\ResourceModel\ChatAction
     */
    public function resetActionsByChatId($chatId){
        $this->getConnection()->delete(
            $this->getMainTable(),
            ['chat_id = ?' => $chatId]
        );
        return $this;
    }
}
