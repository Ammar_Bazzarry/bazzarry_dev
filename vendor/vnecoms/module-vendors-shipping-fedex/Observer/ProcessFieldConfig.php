<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsShippingFedEx\Observer;

use Magento\Framework\Event\ObserverInterface;
use Vnecoms\VendorsConfig\Helper\Data;

class ProcessFieldConfig implements ObserverInterface
{
    /**
     * Add multiple vendor order row for each vendor.
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return self
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $transport = $observer->getTransport();
        $fieldset = $transport->getFieldset();

        $config = \Magento\Framework\App\ObjectManager::getInstance()->get(
            'Magento\Framework\App\Config\ScopeConfigInterface');

        $fieldsArray = [
            "shipping_method_fedex_account",
            "shipping_method_fedex_meter_number",
            "shipping_method_fedex_key",
            "shipping_method_fedex_password",
           // "shipping_method_fedex_max_package_weight",
            "shipping_method_fedex_allowed_methods",
            "shipping_method_fedex_free_method",
            "shipping_method_fedex_free_shipping_enable",
            "shipping_method_fedex_free_shipping_subtotal",
            "shipping_method_fedex_sallowspecific",
            "shipping_method_fedex_specificcountry",
        ];


        if($fieldset->getHtmlId() == "shipping_method_fedex"){
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE;
            $configVal = $config->getValue("carriers/fedex/active",$storeScope);
            $configShipping = $config->getValue("carriers/vendor_multirate/active",$storeScope);
            if(!$configVal || !$configShipping) $transport->setForceReturn(true);

            $configShipping = $config->getValue("carriers/fedex/api_type",$storeScope);
            if($configShipping == \Vnecoms\VendorsShippingFedEx\Model\Source\Type::USE_API_ADMIN){
                foreach ($fieldset->getElements() as $field) {
                    if(in_array($field->getHtmlId(),$fieldsArray))
                    $field->setIsRemoved(true);
                }

            }
        }


        return $this;
    }
}
