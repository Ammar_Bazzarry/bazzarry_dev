<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\VendorsShippingDHL\Observer;

use Magento\Framework\Event\ObserverInterface;
use Vnecoms\VendorsConfig\Helper\Data;

class ProcessFieldConfig implements ObserverInterface
{
    /**
     * Add multiple vendor order row for each vendor.
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return self
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $transport = $observer->getTransport();
        $fieldset = $transport->getFieldset();

        $config = \Magento\Framework\App\ObjectManager::getInstance()->get(
            'Magento\Framework\App\Config\ScopeConfigInterface');

        $fieldsArray = [
            "shipping_method_dhl_content_type",
            "shipping_method_dhl_id",
            "shipping_method_dhl_password",
            "shipping_method_dhl_account",
            "shipping_method_dhl_doc_methods",
            "shipping_method_dhl_nondoc_methods",
            "shipping_method_dhl_free_method_doc",
            "shipping_method_dhl_free_method_nondoc",
            "shipping_method_dhl_free_shipping_enable",
            "shipping_method_dhl_free_shipping_subtotal",
            "shipping_method_dhl_sallowspecific",
            "shipping_method_dhl_specificcountry",
        ];


        if($fieldset->getHtmlId() == "shipping_method_dhl"){
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE;
            $configVal = $config->getValue("carriers/dhl/active",$storeScope);
            $configShipping = $config->getValue("carriers/vendor_multirate/active",$storeScope);
            if(!$configVal || !$configShipping) $transport->setForceReturn(true);

            $configShipping = $config->getValue("carriers/dhl/api_type",$storeScope);
            if($configShipping == \Vnecoms\VendorsShippingDHL\Model\Source\Type::USE_API_ADMIN){
                foreach ($fieldset->getElements() as $field) {
                    if(in_array($field->getHtmlId(),$fieldsArray))
                    $field->setIsRemoved(true);
                }

            }
        }


        return $this;
    }
}
