<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Vnecoms\VendorsProductImportExport\Model;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Import model
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 *
 * @method string getBehavior() getBehavior()
 * @method \Magento\ImportExport\Model\Import setEntity() setEntity(string $value)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Import extends \Magento\ImportExport\Model\Import
{
    /**
     * Import field separator.
     */
    const FIELD_SEPARATOR = ',';
    
    /**
     * Allow multiple values wrapping in double quotes for additional attributes.
     */
    const FIELDS_ENCLOSURE = 'fields_enclosure';
    
    /**
     * Import multiple value separator.
     */
    const FIELD_MULTIPLE_VALUE_SEPARATOR = ',';
    
    /**
     * @var \Vnecoms\Vendors\Model\Vendor
     */
    protected $_vendor;

    /**
     * Set Vendor
     * 
     * @param \Vnecoms\Vendors\Model\Vendor $vendor
     */
    public function setVendor(\Vnecoms\Vendors\Model\Vendor $vendor){
        $this->_vendor = $vendor;
        return $this;
    }
    
    /**
     * Get Vendor
     * 
     * @return \Vnecoms\Vendors\Model\Vendor
     */
    public function getVendor(){
        return $this->_vendor;
    }
    
    /**
     * Override standard entity getter.
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return string
     */
    public function getEntity()
    {
        return 'catalog_product';
    }

    /**
     * Import/Export working directory (source files, result files, lock files etc.).
     *
     * @return string
     */
    public function getWorkingDir()
    {
        return $this->_varDirectory->getAbsolutePath('importexport/'.$this->_vendor->getVendorId().'/');
    }
    
    /**
     * (non-PHPdoc)
     * @see \Magento\ImportExport\Model\Import::_getEntityAdapter()
     */
    protected function _getEntityAdapter()
    {
        $adapter = parent::_getEntityAdapter();
        if(!$adapter->getVendor()){
            $adapter->setVendor($this->getVendor());
        }
        
        return $adapter;
    }
    
    /**
     * Create history report
     *
     * @param string $entity
     * @param string $extension
     * @param string $sourceFileRelative
     * @param array $result
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function createHistoryReport($sourceFileRelative, $entity, $extension = null, $result = null)
    {
        /*Don't save history report for vendor*/
        return $this;
    }
    
    /**
     * Returns source adapter object.
     *
     * @param string $sourceFile Full path to source file
     * @return \Magento\ImportExport\Model\Import\AbstractSource
     */
    protected function _getSourceAdapter($sourceFile)
    {
        return \Vnecoms\VendorsProductImportExport\Model\Import\Adapter::findAdapterFor(
            $sourceFile,
            $this->_filesystem->getDirectoryWrite(DirectoryList::ROOT),
            $this->getData(self::FIELD_FIELD_SEPARATOR)
        );
    }
}
