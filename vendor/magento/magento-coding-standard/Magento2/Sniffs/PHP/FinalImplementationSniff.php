<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */
namespace Magento2\Sniffs\PHP;

use PHP_CodeSniffer\Sniffs\Sniff;
use PHP_CodeSniffer\Files\File;

/**
 * Magento is a highly extensible and customizable platform. The use of final classes and methods is prohibited.
 */
class FinalImplementationSniff implements Sniff
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        return [T_FINAL];
    }

    /**
     * @inheritdoc
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $phpcsFile->addError(
            // phpcs:ignore Magento2.Files.LineLength.MaxExceeded
            'Final keyword is prohibited in Magento. It decreases extensibility and is not compatible with plugins and proxies.',
            $stackPtr,
            'FoundFinal'
        );
    }
}
