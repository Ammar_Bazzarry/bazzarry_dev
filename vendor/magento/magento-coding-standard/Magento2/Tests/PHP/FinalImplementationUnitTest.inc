<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

final class ImABadExample {}

class ImAlsoABadExample {
    final public function badFinalFunction(){}
}

class ImAGoodExample {}

class ImAlsoAGoodExample {
    private function emptyFunction(){}
}
