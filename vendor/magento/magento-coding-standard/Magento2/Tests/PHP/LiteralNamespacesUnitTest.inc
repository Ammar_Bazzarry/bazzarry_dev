<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

class Fixture
{
    public function ok()
    {
        $this->create(Magento\CustomerSegment\Model\Segment\Condition\Customer\Address::class);
    }

    public function notOk()
    {
        $this->create('Magento\CustomerSegment\Model\Segment\Condition\Customer\Address');
    }
}
