<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

class A
{
    public $data = [];

    private $k = null;

    var $a;
    var $b = null;
    var $c = 1;
    var $cc;

    protected $d;
    protected $e = [];
}
