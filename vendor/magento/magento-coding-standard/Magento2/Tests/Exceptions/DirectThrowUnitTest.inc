<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

class Foo
{
    protected $isEnabled = true;

    public function go()
    {
        if (!$this->isEnabled) {
            throw new Exception('Action disabled.');
        }
    }

    public function exceptionTest()
    {
        if (!$this->isEnabled) {
            throw new \Exception('Action disabled.');
        }
    }

    public function localizedExceptionTest()
    {

        if (!$this->isEnabled) {
            throw new LocalizedException('Localized exception.');
        }
    }
}
