<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Magento\Tests\NamingConvention\true;

    class Int {};

    interface float {};

    trait bool {};

    class string {};

    class false {};

    class resource {};

    interface Null{};

    trait mixed {};

    interface numeric {};
    
    trait object {};
