<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */
namespace Magento2\Tests\Translation;

use PHP_CodeSniffer\Tests\Standards\AbstractSniffUnitTest;

/**
 * Class ConstantUsageUnitTest
 */
class ConstantUsageUnitTest extends AbstractSniffUnitTest
{
    /**
     * @inheritdoc
     */
    public function getErrorList()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getWarningList()
    {
        return [
            5 => 1,
            7 => 1,
            9 => 1,
            12 => 1,
            15 => 1,
            17 => 1,
            19 => 1,
            21 => 1,
            24 => 1,
        ];
    }
}
