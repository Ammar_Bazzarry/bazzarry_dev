<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

/**
 * Interface FooInterface
 * @api
 */
interface FooInterface
{
    public function execute();
}

/**
 * // Rule find: api annotation for this class
 * @api
 */
abstract class Foo implements FooInterface
{

}

/**
 * // Rule find: api annotation for this class
 * @api
 */
abstract
class FooBar implements FooInterface
{

}

/**
 * Class Bar
 */
class Bar extends Foo
{
    public function execute()
    {
        // TODO: Implement execute() method.
    }
}
