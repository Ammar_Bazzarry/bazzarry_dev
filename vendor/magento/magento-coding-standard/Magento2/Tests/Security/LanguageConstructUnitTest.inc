<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

class TestLanguageConstruct
{
    public function go()
    {
        echo '123';
        $a = `ls`;
        $s = 'select echo, print, exit, die from `ls`';
        die();
    }
}

print(1);
exit();
