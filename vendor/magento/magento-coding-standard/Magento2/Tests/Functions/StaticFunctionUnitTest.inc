<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Foo\Bar;

interface  FooInterface
{
    public static
    function aStaticMethod();

    public function normalMethod();
}

class Foo implements FooInterface
{
    private static $property;

    public static function aStaticMethod()
    {
        return self::$property;
    }

    public function normalMethod()
    {
        return $this;
    }
}
