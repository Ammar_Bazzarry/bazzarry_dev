var config = {
    paths: {
        'owlcarousel': 'js/owlcarousel/owl.carousel.min',
        'mousewheel': 'js/customscrollbar/jquery.mousewheel.min',
        'customscrollbar': 'js/customscrollbar/jquery.mCustomScrollbar.min',
        'qtyButton': 'js/qty-button',
        'wishlistColor': 'js/wishlist-color'
    },
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        },
        'customscrollbar': {
            deps: ['jquery', 'mousewheel']
        }
    },
    map: {
        '*': {
            'owlWidget': 'js/owlcarousel/owl-widget'
        }
    }
};
