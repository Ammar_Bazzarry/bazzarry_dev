define([
    'jquery',
    'jquery/ui'
], function ($) {
    'use strict';

    $.widget('tigren.qtyButton', {
        _create: function () {
            this._bind();
        },

        _bind: function () {
            var body = $('body');
            $('.quantity-plus').click(function () {
                var inputQty = $(this).prev('input.qty');
                if(body.hasClass('catalog-product-view')){
                    inputQty = $('#qty');
                }
                inputQty.val(Number(inputQty.val()) + 1);
            });

            $('.quantity-minus').click(function () {
                var inputQty = $(this).next('input.qty');
                if(body.hasClass('catalog-product-view')){
                    inputQty = $('#qty');
                }
                var value = Number(inputQty.val()) - 1;
                if (value > 0) {
                    inputQty.val(value);
                }
            });

        },
    });
    return $.tigren.qtyButton;
});