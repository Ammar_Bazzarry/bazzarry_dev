define([
    'jquery',
    'Magento_Customer/js/customer-data',
    'jquery/ui'
], function ($, customerData) {
    'use strict';

    $.widget('mage.wishlistColor', {
        options: {
            wishlistColor: {
                wishlistBtnSelector: '[data-action="add-to-wishlist"]'
            }
        },

        _create: function () {
            this._bind();
        },

        _bind: function(){
            this.initAddedWishlistItems();
        },

        initAddedWishlistItems: function () {
            var self = this;
            customerData.reload(['wishlist'], true).success(function () {
                var addedWishlistItemIds = [],
                    addedWishlistItems = customerData.get('wishlist')().items;
                if (!addedWishlistItems) {
                    return;
                }
                for (var i = 0; i < addedWishlistItems.length; i++) {
                    addedWishlistItemIds.push(parseInt(addedWishlistItems[i].product_id));
                }
                $.each($(self.options.wishlistColor.wishlistBtnSelector), function () {
                    var dataPost = $(this).data('post').data;
                    if (dataPost) {
                        var product = parseInt(dataPost.product);
                        if (product && $.inArray(product, addedWishlistItemIds) !== -1) {
                            $(this).addClass('in-wishlist');
                        }
                    }
                });
            });
        },
    });
    return $.mage.wishlistColor;
});
