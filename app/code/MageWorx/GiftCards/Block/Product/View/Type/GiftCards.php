<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\GiftCards\Block\Product\View\Type;

/**
 * Giftcards product data view
 */
class GiftCards extends \Magento\Catalog\Block\Product\View\AbstractView
{
    /**
     * @var int
     */
    public $cardType;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $currency;

    /**
     * @var array
     */
    protected $aAdditionalPrices;

    /**
     * @var \MageWorx\GiftCards\Helper\Data
     */
    protected $helper;

    /**
     * @var  \Magento\Checkout\Helper\Data
     */
    public $checkoutHelper;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Stdlib\ArrayUtils $arrayUtils
     * @param \Magento\Directory\Model\Currency $currency
     * @param \MageWorx\GiftCards\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        \Magento\Directory\Model\Currency $currency,
        \MageWorx\GiftCards\Helper\Data $helper,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        array $data = []
    ) {
        parent::__construct($context, $arrayUtils, $data);
        $this->_isScopePrivate = true;
        $this->currency        = $currency;
        $this->helper          = $helper;
        $this->checkoutHelper  = $checkoutHelper;
    }

    /**
     * Set cardType and priceStatus
     *
     */
    public function _construct()
    {
        $product = $this->getProduct();

        if ($product) {
            $this->cardType = $product->getData('mageworx_gc_type');
        }

        parent::_construct();
    }

    /**
     * @return int
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return bool
     */
    public function isShowMailDeliveryDate()
    {
        return $this->helper->getShowMailDeliveryDate();
    }

    /**
     * @return array
     */
    public function getAdditionalPrices()
    {
        if (!$this->aAdditionalPrices && $this->getProduct()->getData('mageworx_gc_additional_price')) {
            $this->aAdditionalPrices = explode(';', $this->getProduct()->getData('mageworx_gc_additional_price'));
        }

        return $this->aAdditionalPrices;
    }


    /**
     * @return mixed
     */
    public function getMinPrice()
    {
        $prices = $this->getAllPrices();

        $min = min($prices);
        if (!$this->getProduct()->getMageworxGcAllowOpenAmount()) {
            $min = $min > 0 ? $min : null;
        }

        return $min;
    }

    /**
     * @return mixed
     */
    public function getMaxPrice()
    {
        $prices = $this->getAllPrices();

        $max = max($prices);

        return $max > 0 ? $max : null;
    }

    /**
     * @return mixed
     */
    public function getAllPrices()
    {
        $prices  = [];
        $product = $this->getProduct();

        if ($product->getPrice() > 0) {
            $prices[] = $product->getPrice();
        }

        if ($product->getMageworxGcAllowOpenAmount()) {
            $prices[] = $product->getMageworxGcOpenAmountMin() ? $product->getMageworxGcOpenAmountMin() : 0;
            if ($product->getMageworxGcOpenAmountMax() > 0) {
                $prices[] = $product->getMageworxGcOpenAmountMax();
            }
        }

        if ($this->getAdditionalPrices()) {
            foreach ($this->getAdditionalPrices() as $additionalPrice) {
                if ($additionalPrice) {
                    $prices[] = $additionalPrice;
                }
            }
        }

        return $prices;
    }

    /**
     * @return string
     */
    public function getGiftCardPrice()
    {
        $from = '';
        $to   = '';

        $min = $this->getMinPrice();
        $max = $this->getMaxPrice();

        if ($min == $max) {
            return $this->checkoutHelper->formatPrice($min);
        }

        if ($min !== null) {
            $from = __('From ');
        }
        if ($max) {
            $to = $from ? __(' to ') : __('To ');
        }

        return $from . $this->checkoutHelper->formatPrice($min) .
            $to . $this->checkoutHelper->formatPrice($max);
    }

    /**
     * @return string
     */
    public function getPlaceholder()
    {
        $product  = $this->getProduct();
        $from     = '';
        $to       = '';
        $splitter = '';

        if ($product->getMageworxGcAllowOpenAmount()) {
            $from = $this->getCurrency()->convert($product->getMageworxGcOpenAmountMin());
            $to   = $this->getCurrency()->convert($product->getMageworxGcOpenAmountMax());
        }

        $currencySymbol = $this->getCurrency()->getCurrencySymbol();

        if ($from) {
            $from = $currencySymbol . $from;
        }

        if ($to) {
            $to = $currencySymbol . $to;
        }

        if ($from && $to) {
            $splitter = ' - ';
        }

        return $from . $splitter . $to;
    }
}
