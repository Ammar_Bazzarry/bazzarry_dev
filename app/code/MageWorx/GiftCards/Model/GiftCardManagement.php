<?php
/**
 *
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\GiftCards\Model;

use MageWorx\GiftCards\Helper\Data as Helper;
use Magento\Quote\Api\CartRepositoryInterface as CartRepository;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use MageWorx\GiftCards\Model\Session as GiftCardSession;
use MageWorx\GiftCards\Helper\GiftCardManagement as GiftCardManagementHelper;

class GiftCardManagement implements \MageWorx\GiftCards\Api\GiftCardManagementInterface
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * @var GiftCardsRepository
     */
    private $giftCardsRepository;

    /**
     * @var CartRepository
     */
    private $cartRepository;

    /**
     * @var Session
     */
    private $giftCardSession;

    /**
     * @var GiftCardManagementHelper
     */
    private $giftCardManagementHelper;

    /**
     * GiftCardManagement constructor.
     *
     * @param Helper $helper
     * @param GiftCardsRepository $giftCardsRepository
     * @param CartRepository $cartRepository
     * @param Session $giftCardSession
     * @param GiftCardManagementHelper $giftCardManagementHelper
     */
    public function __construct(
        Helper $helper,
        GiftCardsRepository $giftCardsRepository,
        CartRepository $cartRepository,
        GiftCardSession $giftCardSession,
        GiftCardManagementHelper $giftCardManagementHelper
    ) {
        $this->helper                   = $helper;
        $this->giftCardsRepository      = $giftCardsRepository;
        $this->cartRepository           = $cartRepository;
        $this->giftCardSession          = $giftCardSession;
        $this->giftCardManagementHelper = $giftCardManagementHelper;
    }

    /**
     * @param int $cartId
     * @param string $giftCardCode
     * @return bool
     * @throws CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function applyToCart($cartId, $giftCardCode)
    {
        /** @var \MageWorx\GiftCards\Model\GiftCards $giftCard */
        $giftCard = $this->giftCardsRepository->getByCode($giftCardCode);

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->cartRepository->getActive($cartId);

        if (!$quote->getItemsCount()) {
            throw new NoSuchEntityException(__('Cart %cartId doesn\'t contain products', $cartId));
        }

        if ($giftCard->isValid(
            true,
            true,
            $quote->getStoreId(),
            $quote->getCustomerGroupId(),
            true
        )) {
            try {
                $this->giftCardSession->setActive(1);
                $this->giftCardManagementHelper->setSessionVars($giftCard);

                $quote->getShippingAddress()->setCollectShippingRates(true);
                $quote->collectTotals();
                $this->cartRepository->save($quote);
            } catch (\Exception $e) {
                throw new CouldNotSaveException(__('Could not add gift card code'));
            }

            return true;
        }

        return false;
    }

    /**
     * @param int $cartId
     * @param string $giftCardCode
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function removeFromCart($cartId, $giftCardCode)
    {
        /** @var \MageWorx\GiftCards\Model\GiftCards $giftCard */
        $giftCard   = $this->giftCardsRepository->getByCode($giftCardCode);
        $giftCardId = $giftCard->getId();

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->cartRepository->getActive($cartId);

        if (!$quote->getItemsCount()) {
            throw new NoSuchEntityException(__('Cart %cartId doesn\'t contain products', $cartId));
        }

        try {
            if ($this->giftCardSession->getActive()) {
                $giftCardsIds = $this->giftCardSession->getGiftCardsIds();

                if ($giftCardsIds && isset($giftCardsIds[$giftCardId])) {
                    $sessionBalance    = $this->giftCardSession->getGiftCardBalance();
                    $newSessionBalance = $sessionBalance - $giftCardsIds[$giftCardId]['balance'];

                    unset($giftCardsIds[$giftCardId]);

                    if (empty($giftCardsIds)) {
                        $this->giftCardSession->clearStorage();
                        $this->giftCardSession->setIsReadyToClean(1);
                    }

                    $this->giftCardSession->setGiftCardBalance($newSessionBalance);
                    $this->giftCardSession->setGiftCardsIds($giftCardsIds);
                }
            }

            $quote->getShippingAddress()->setCollectShippingRates(true);
            $quote->collectTotals();
            $this->cartRepository->save($quote);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Could not delete gift card from quote'));
        }

        return true;
    }
}
