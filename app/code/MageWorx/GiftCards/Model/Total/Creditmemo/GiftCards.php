<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\GiftCards\Model\Total\Creditmemo;

use Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal;

class GiftCards extends AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @return $this|AbstractTotal
     */
    public function collect(\Magento\Sales\Model\Order\Creditmemo $creditmemo)
    {
        $order = $creditmemo->getOrder();
        if ($order->getMageworxGiftcardsAmount() < 0) {
            $creditmemo->setMageworxGiftcardsAmount($order->getMageworxGiftcardsAmount());
            $creditmemo->setBaseMageworxGiftcardsAmount($order->getBaseMageworxGiftcardsAmount());
            $creditmemo->setMageworxGiftcardsDescription($order->getMageworxGiftcardsDescription());

            $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $creditmemo->getMageworxGiftcardsAmount());
            $creditmemo->setBaseGrandTotal(
                $creditmemo->getBaseGrandTotal() + $creditmemo->getBaseMageworxGiftcardsAmount()
            );
        } else {
            $creditmemo->setMageworxGiftcardsAmount(0);
            $creditmemo->setBaseMageworxGiftcardsAmount(0);
            $creditmemo->setMageworxGiftcardsDetails('');
        }

        return $this;
    }
}