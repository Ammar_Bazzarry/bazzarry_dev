<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\GiftCards\Model\Total\Invoice;

use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

class GiftCards extends AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return $this|AbstractTotal
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $order = $invoice->getOrder();

        if ($order->getMageworxGiftcardsAmount() < 0) {
            $invoice->setMageworxGiftcardsAmount($order->getMageworxGiftcardsAmount());
            $invoice->setBaseMageworxGiftcardsAmount($order->getBaseMageworxGiftcardsAmount());
            $invoice->setMageworxGiftcardsDescription($order->getMageworxGiftcardsDescription());

            $invoice->setGrandTotal($invoice->getGrandTotal() + $invoice->getMageworxGiftcardsAmount());
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $invoice->getBaseMageworxGiftcardsAmount());
        } else {
            $invoice->setMageworxGiftcardsAmount(0);
            $invoice->setBaseMageworxGiftcardsAmount(0);
            $invoice->setMageworxGiftcardsDescription('');
        }

        return $this;
    }
}