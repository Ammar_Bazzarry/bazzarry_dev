<?php
/**
 *
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\GiftCards\Api;

/**
 * @api
 */
interface GiftCardManagementInterface
{
    /**
     * @param int $cartId
     * @param string $giftCardCode
     * @return boolean
     */
    public function applyToCart($cartId, $giftCardCode);

    /**
     * @param int $cartId
     * @param string $giftCardCode
     * @return boolean
     */
    public function removeFromCart($cartId, $giftCardCode);
}
