<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\GiftCards\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context as ContextHelper;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class Price extends AbstractHelper
{
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    protected $currencyFactory;

    /**
     * Price constructor.
     *
     * @param ContextHelper $context
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        ContextHelper $context,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->currencyFactory = $currencyFactory;
        $this->priceCurrency = $priceCurrency;
        parent::__construct($context);
    }

    /**
     * @param string $balance
     * @param \Magento\Store\Api\Data\StoreInterface $store
     * @param string|null $cardCurrency
     * @return float
     */
    public function convertCardCurrencyToStoreCurrency($balance, $store, $cardCurrency)
    {
        if ($this->needConvertPrice($store, $cardCurrency, $balance)) {
            $rate                = $this->priceCurrency->convert($balance, $store, $cardCurrency) / $balance;
            $baseCurrencyBalance = $balance / $rate;

            $storeCurrencyBalance = $this->priceCurrency->convert(
                $baseCurrencyBalance,
                $store,
                $store->getCurrentCurrencyCode()
            );

            return $storeCurrencyBalance;
        }

        return $balance;
    }

    /**
     * @param string $balance
     * @param \Magento\Store\Api\Data\StoreInterface $store
     * @param string|null $cardCurrency
     * @return float
     */
    public function convertCurrencyToBaseCurrency($balance, $store, $cardCurrency)
    {
        if ($this->needConvertToBase($store, $cardCurrency, $balance)) {
            $currency = $this->currencyFactory->create()->load($cardCurrency);
            $rate = $currency->getRate($store->getBaseCurrencyCode());
            $baseCurrencyBalance = $balance * $rate;

            return round($baseCurrencyBalance,2);
        }

        return $balance;
    }

    public function convertBaseToCartCurrency($price, $store){
        $rate                = $store->getBaseCurrency()->getRate('USD');
        $cartBalance = $price * $rate;

        return round($cartBalance,2);
    }


    /**
     * @param string $balance
     * @param \Magento\Store\Api\Data\StoreInterface $store
     * @param string|null $cardCurrency
     * @return float
     */
    public function convertBaseCurrencyToStoreCurrency($balance, $store, $cardCurrency)
    {
        if ($this->needConvertBaseToStore($store, $cardCurrency, $balance)) {
            $rate                = $this->priceCurrency->convert($balance, $store, $store->getCurrentCurrencyCode()) / $balance;
            $rate = round($rate,4);
            $cardCurrencyBalance = $balance * $rate;

            return round($cardCurrencyBalance,2);
        }

        return $balance;
    }

    /**
     * @param \Magento\Store\Api\Data\StoreInterface $store
     * @param string|null $cardCurrency
     * @param string $balance
     * @return bool
     */
    protected function needConvertPrice($store, $cardCurrency, $balance)
    {
        return (isset($cardCurrency) && $store->getCurrentCurrencyCode() != $cardCurrency && $balance > 0);
    }

    protected function needConvertBaseToStore($store, $cardCurrency, $balance)
    {
        return (isset($cardCurrency) && $store->getCurrentCurrencyCode() != $store->getBaseCurrencyCode() && $balance > 0);
    }

    protected function needConvertToBase($store, $cardCurrency, $balance)
    {
        return (isset($cardCurrency) && $store->getBaseCurrencyCode() != $cardCurrency && $balance > 0);
    }
}