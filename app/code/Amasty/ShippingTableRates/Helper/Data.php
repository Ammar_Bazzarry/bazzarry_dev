<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_ShippingTableRates
 */

namespace Amasty\ShippingTableRates\Helper;

use Magento\Directory\Helper\Data as DirectoryHelper;

/**
 * Class Data
 * @package Amasty\ShippingTableRates\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;

    /**
     * @var \Magento\Framework\Escaper
     */
    private $escaper;

    /**
     * @var \Amasty\MultiInventory\Model\WarehouseFactory
     */
    protected $_warehouseFactory;

    /**
     * @var DirectoryHelper
     */
    protected $directoryHelper;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Escaper $escaper
     * @param DirectoryHelper $directoryHelper
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Escaper $escaper,
        DirectoryHelper $directoryHelper,
        \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory
    ) {
        $this->_objectManager = $objectManager;
        $this->_eavConfig = $eavConfig;
        parent::__construct($context);
        $this->escaper = $escaper;
        $this->directoryHelper = $directoryHelper;
        $this->_warehouseFactory = $warehouseFactory;
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return [
            '0' => __('Inactive'),
            '1' => __('Active'),
        ];
    }

    /**
     * @param bool $needHash
     * @return array
     */
    public function getCountries($needHash = false)
    {
        /**
         * @var \Magento\Directory\Model\Country $countriesModel
         */
        $countriesModel = $this->_objectManager->get('Magento\Directory\Model\Country');
        $countries = $countriesModel->getCollection()->toOptionArray();
        unset($countries[0]);

        if ($needHash) {
            $countries = $this->_toHash($countries);
        }

        return $countries;
    }

    /**
     * @param bool $needHash
     * @return array
     */
    public function getStates($needHash = false)
    {
        /**
         * @var \Magento\Directory\Model\Region $stateModel
         */
        $regionModel = $this->_objectManager->get('Magento\Directory\Model\Region');
        $regions = $regionModel->getCollection()->toOptionArray();
        $regions = $this->_addCountriesToStates($regions);

        if ($needHash) {
            $regions = $this->_toHash($regions);
        }

        return $regions;
    }

    /**
     * @param bool $needHash
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getTypes($needHash = false)
    {
        $options = [];

        $attribute = $this->_eavConfig->getAttribute('catalog_product', 'am_shipping_type');
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

        if ($needHash) {
            $options = $this->_toHash($options, false);
        }

        return $options;
    }

    /**
     * @param $zip
     * @return array('area', 'district')
     */
    public function getDataFromZip($zip)
    {
        $dataZip = ['area' => '', 'district' => ''];

        if (!empty($zip)) {
            $zipSpell = str_split($zip);
            foreach ($zipSpell as $element) {
                if ($element === ' ') {
                    break;
                }
                if (is_numeric($element)) {
                    $dataZip['district'] = $dataZip['district'] . $element;
                } elseif (empty($dataZip['district'])) {
                    $dataZip['area'] = $dataZip['area'] . $element;
                }
            }
        }

        return $dataZip;
    }

    /**
     * @param $regions
     * @return mixed
     */
    protected function _addCountriesToStates($regions)
    {
        $hashCountry = $this->getCountries(true);
        foreach ($regions as $key => $region) {
            if (isset($region['country_id'])) {
                $regions[$key]['label'] = $hashCountry[$region['country_id']] . "/" . $region['label'];
            }
        }

        return $regions;
    }

    /**
     * @param $options
     * @param bool $needSort
     * @return array
     */
    protected function _toHash($options, $needSort = true)
    {
        $hash = [];
        foreach ($options as $option) {
            $hash[$option['value']] = $option['label'];
        }
        if ($needSort) {
            asort($hash);
        }
        $hashAll['0'] = 'All';
        $hash = $hashAll + $hash;
        $options = $hash;

        return $options;
    }

    /**
     * @param $string
     * @return array|string
     */
    public function escapeHtml($string)
    {
        return $this->escaper->escapeHtml($string, ['b', 'i', 'u', 's']);
    }

    /**
     * @param bool $needHash
     * @return array
     */
    public function getWarehouseData($needHash = false)
    {
        $warehouseOptions = [];
        $warehouseOptionArray = $this->_warehouseFactory->create()
            ->getCollection()
            ->toOptionArray();

        if ($needHash) {
            $warehouseOptions = $this->_toHash($warehouseOptionArray);
            unset($warehouseOptions[0]);
        }

        return $warehouseOptions;
    }

    /**
     * @return array
     */
    public function getRegions()
    {
        $regionOptions = [
            0 => __('-- Select City --')
        ];
        $regions = $this->directoryHelper->getRegionData();

        unset($regions['config']);

        foreach ($regions as $countryCode => $countryRegions) {
            if ($countryCode !== 'YE') {
                continue;
            }

            foreach ($countryRegions as $regionId => $regionData) {
                $regionOptions[$regionId] = $regionData['name'];
            }
        }

        return $regionOptions;
    }

    /**
     * @return bool
     */
    public function calculateShippingFeeBaseOnWarehouse()
    {
        return (bool) $this->scopeConfig->getValue(
            'carriers/amstrates/calculate_shipping_base_on_warehouse',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES
        );
    }
}
