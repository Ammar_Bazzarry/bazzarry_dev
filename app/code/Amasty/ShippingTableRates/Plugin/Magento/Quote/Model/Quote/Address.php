<?php

namespace Amasty\ShippingTableRates\Plugin\Magento\Quote\Model\Quote;

use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Address
 * @package Amasty\ShippingTableRates\Override\Magento\Quote\Model\Quote
 */
class Address
{
    /**
     * @var mixed
     */
    protected $storeManager;

    /**
     * @var \Magento\Quote\Model\Quote\Address
     */
    protected $_addressRateFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address
     */
    protected $_rateCollector;

    /**
     * @var \Magento\Quote\Model\Quote\Address
     */
    protected $_rateRequestFactory;

    /**
     * Address constructor.
     * @param \Magento\Quote\Model\Quote\Address\RateFactory $addressRateFactory
     * @param \Magento\Quote\Model\Quote\Address\RateCollectorInterfaceFactory $rateCollector
     * @param \Magento\Quote\Model\Quote\Address\RateRequestFactory $rateRequestFactory
     * @param StoreManagerInterface|null $storeManager
     */
    public function __construct(
        \Magento\Quote\Model\Quote\Address\RateFactory $addressRateFactory,
        \Magento\Quote\Model\Quote\Address\RateCollectorInterfaceFactory $rateCollector,
        \Magento\Quote\Model\Quote\Address\RateRequestFactory $rateRequestFactory,
        StoreManagerInterface $storeManager = null
    ) {

        $this->_rateCollector = $rateCollector;
        $this->_rateRequestFactory = $rateRequestFactory;
        $this->_addressRateFactory = $addressRateFactory;
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address $subject
     * @param $proceed
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem|null $item
     * @return bool
     */
    public function aroundRequestShippingRates(
        \Magento\Quote\Model\Quote\Address $subject,
        $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item = null
    ) {
        /** @var $request \Magento\Quote\Model\Quote\Address\RateRequest */
        $request = $this->_rateRequestFactory->create();
        $request->setAllItems($item ? [$item] : $subject->getAllItems());
        $request->setDestCountryId($subject->getCountryId());
        $request->setDestRegionId($subject->getRegionId());
        $request->setDestRegionCode($subject->getRegionCode());
        $request->setDestStreet($subject->getStreetFull());
        $request->setDestCity($subject->getCity());
        $request->setDestPostcode($subject->getPostcode());
        $request->setPackageValue($item ? $item->getBaseRowTotal() : $subject->getBaseSubtotal());
        $packageWithDiscount = $item ? $item->getBaseRowTotal() -
            $item->getBaseDiscountAmount() : $subject->getBaseSubtotalWithDiscount();
        $request->setPackageValueWithDiscount($packageWithDiscount);
        $request->setPackageWeight($item ? $item->getRowWeight() : $subject->getWeight());
        $request->setPackageQty($item ? $item->getQty() : $subject->getItemQty());

        /**
         * Need for shipping methods that use insurance based on price of physical products
         */
        $packagePhysicalValue = $item ? $item->getBaseRowTotal() : $subject->getBaseSubtotal() -
            $subject->getBaseVirtualAmount();
        $request->setPackagePhysicalValue($packagePhysicalValue);

        $request->setFreeMethodWeight($item ? 0 : $subject->getFreeMethodWeight());

        /**
         * Store and website identifiers specified from StoreManager
         */
        $request->setQuoteStoreId($subject->getQuote()->getStoreId());
        if ($subject->getQuote()->getStoreId()) {
            $storeId = $subject->getQuote()->getStoreId();
            $request->setStoreId($storeId);
            $request->setWebsiteId($this->storeManager->getStore($storeId)->getWebsiteId());
        } else {
            $request->setStoreId($this->storeManager->getStore()->getId());
            $request->setWebsiteId($this->storeManager->getWebsite()->getId());
        }
        $request->setFreeShipping($subject->getFreeShipping());
        /**
         * Currencies need to convert in free shipping
         */
        $request->setBaseCurrency($this->storeManager->getStore()->getBaseCurrency());
        $request->setPackageCurrency($this->storeManager->getStore()->getCurrentCurrency());
        $request->setLimitCarrier($subject->getLimitCarrier());
        $baseSubtotalInclTax = $subject->getBaseSubtotalTotalInclTax();
        $request->setBaseSubtotalInclTax($baseSubtotalInclTax);

        $shippingAddressExtAttributes = $subject->getExtensionAttributes();
        if (is_array($shippingAddressExtAttributes) && isset($shippingAddressExtAttributes['pickupstore_id'])) {
            $request->setPickupstoreId($shippingAddressExtAttributes['pickupstore_id']);
        } elseif ($shippingAddressExtAttributes instanceof \Magento\Quote\Api\Data\AddressExtension) {
            $request->setPickupstoreId($shippingAddressExtAttributes->getPickupstoreId());
        }

        $result = $this->_rateCollector->create()->collectRates($request)->getResult();

        $found = false;
        if ($result) {
            $shippingRates = $result->getAllRates();

            foreach ($shippingRates as $shippingRate) {
                $rate = $this->_addressRateFactory->create()->importShippingRate($shippingRate);
                if (!$item) {
                    $subject->addShippingRate($rate);
                }

                if ($subject->getShippingMethod() == $rate->getCode()) {
                    if ($item) {
                        $item->setBaseShippingAmount($rate->getPrice());
                    } else {

                        /** @var \Magento\Store\Api\Data\StoreInterface */
                        $store = $this->storeManager->getStore();
                        $amountPrice = $store->getBaseCurrency()
                            ->convert($rate->getPrice(), $store->getCurrentCurrencyCode());
                        $subject->setBaseShippingAmount($rate->getPrice());
                        $subject->setShippingAmount($amountPrice);
                    }

                    $found = true;
                }
            }
        }

        return $found;
    }
}
