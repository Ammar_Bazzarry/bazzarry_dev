<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_ShippingTableRates
 */


namespace Amasty\ShippingTableRates\Model\Carrier;

use Amasty\ShippingTableRates\Helper\Data;
use Amasty\ShippingTableRates\Model\Config\Source\MethodType;
use Amasty\ShippingTableRates\Model\Method;
use Amasty\ShippingTableRates\Model\Rate;
use Amasty\ShippingTableRates\Model\RateFactory;
use Amasty\ShippingTableRates\Model\ResourceModel\Method\Collection;
use Amasty\ShippingTableRates\Model\ResourceModel\Method\CollectionFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Table
 * @package Amasty\ShippingTableRates\Model\Carrier
 */
class Table extends AbstractCarrier implements
    CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'amstrates';

    /**
     * @var bool
     */
    protected $_isFixed = true;

    /**
     * @var ResultFactory
     */
    protected $_rateResultFactory;

    /**
     * @var MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var \Amasty\ShippingTableRates\Model\ResourceModel\Label\CollectionFactory
     */
    private $labelCollectionFactory;

    /**
     * @var CollectionFactory
     */
    private $methodCollectionFactory;

    /**
     * @var RateFactory
     */
    private $rateFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Data
     */
    private $helperData;

    /**
     * @var \Amasty\ShippingTableRates\Model\ResourceModel\Rate
     */
    private $resourceModelRate;

    /**
     * Table constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param ObjectManagerInterface $objectManager
     * @param \Amasty\ShippingTableRates\Model\ResourceModel\Label\CollectionFactory $labelCollectionFactory
     * @param CollectionFactory $methodCollectionFactory
     * @param RateFactory $rateFactory
     * @param StoreManagerInterface $storeManager
     * @param Data $helperData
     * @param CheckoutSession $checkoutSession
     * @param \Amasty\ShippingTableRates\Model\ResourceModel\Rate $resourceModelRate
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        ObjectManagerInterface $objectManager,
        \Amasty\ShippingTableRates\Model\ResourceModel\Label\CollectionFactory $labelCollectionFactory,
        CollectionFactory $methodCollectionFactory,
        RateFactory $rateFactory,
        StoreManagerInterface $storeManager,
        Data $helperData,
        CheckoutSession $checkoutSession,
        \Amasty\ShippingTableRates\Model\ResourceModel\Rate $resourceModelRate,
        array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_objectManager = $objectManager;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        $this->labelCollectionFactory = $labelCollectionFactory;
        $this->methodCollectionFactory = $methodCollectionFactory;
        $this->rateFactory = $rateFactory;
        $this->storeManager = $storeManager;
        $this->helperData = $helperData;
        $this->checkoutSession = $checkoutSession;
        $this->resourceModelRate = $resourceModelRate;
    }

    /**
     * @param RateRequest $request
     * @return bool|\Magento\Framework\DataObject|Result|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigData('active')) {
            return false;
        }

        $quoteItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
        $storeId = $this->storeManager->getStore()->getId();

        /** @var Result $result */
        $result = $this->_rateResultFactory->create();

        /** @var \Amasty\ShippingTableRates\Model\ResourceModel\Label\Collection $customLabel */
        $customLabel = $this->labelCollectionFactory->create();

        /** @var Collection $methodCollection */
        $methodCollection = $this->methodCollectionFactory->create()
            ->addFieldToFilter('is_active', 1)
            ->addStoreFilter($storeId)
            ->addCustomerGroupFilter($this->getCustomerGroupId($request));

        $calShippingFeeBaseOnWarehouse = $this->helperData->calculateShippingFeeBaseOnWarehouse();
        if ($calShippingFeeBaseOnWarehouse) {
            $quoteId = $this->checkoutSession->getQuote()->getId();
            $warehouseIds = $this->resourceModelRate->filterWarehouseIdByWarehouseQuoteItem($quoteId);
            if (count($warehouseIds) > 0) {
                $methodCollection->addFieldToFilter('warehouse', ['in' => $warehouseIds]);
            }
        }

        $pickupstoreId = $request->getPickupstoreId();
        if ($pickupstoreId) {
            $methodCollection->addFieldToFilter('method_type', MethodType::METHOD_TYPE_STORE_PICKUP);
        } else {
            $methodCollection->addFieldToFilter('method_type', ['neq' => MethodType::METHOD_TYPE_STORE_PICKUP]);
        }

        /** @var Rate $modelRate */
        $modelRate = $this->rateFactory->create();
        $rates = $modelRate->findBy($request, $methodCollection);
        $countOfRates = 0;
        foreach ($methodCollection as $customMethod) {
            $customLabelData = $customLabel->addFiltersByMethodIdStoreId($customMethod->getId(), $storeId)
                ->getLastItem();
            /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
            $method = $this->_rateMethodFactory->create();
            // record carrier information
            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getConfigData('title'));

            if (isset($rates[$customMethod->getId()]['cost'])) {
                // record method information
                $method->setMethod($this->_code . $customMethod->getId());
                $label = $this->helperData->escapeHtml($customLabelData->getLabel());
                if ($label === null) {
                    $methodTitle = __($customMethod->getName());
                } else {
                    $methodTitle = __($label);
                }
                $methodTitle = str_replace('{day}', $rates[$customMethod->getId()]['time'], $methodTitle);
                $method->setMethodTitle($methodTitle);

                $method->setCost($rates[$customMethod->getId()]['cost']);
                $method->setPrice($rates[$customMethod->getId()]['cost']);

                $method->setPos($customMethod->getPos());

                // add this rate to the result
                $result->append($method);
                $countOfRates++;
            }
        }

        if (($countOfRates == 0) && ($this->getConfigData('showmethod') == 1)) {
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($this->getConfigData('specificerrmsg'));
            $result->append($error);
        }

        return $result;
    }

    /**
     * @param $request
     * @return int
     */
    public function getCustomerGroupId($request)
    {
        $allItems = $request->getAllItems();

        if ($allItems) {
            foreach ($allItems as $item) {
                return $item->getProduct()->getCustomerGroupId();
            }
        }

        return 0;
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        /* @var Method $modelMethod */
        $modelMethod = $this->_objectManager->create('Amasty\ShippingTableRates\Model\Method');
        $collection = $modelMethod->getCollection();
        $collection
            ->addFieldToFilter('is_active', 1);
        $arr = [];
        foreach ($collection as $method) {
            $methodCode = 'amstrates' . $method->getId();
            $arr[$methodCode] = $method->getName();
        }

        return $arr;
    }
}
