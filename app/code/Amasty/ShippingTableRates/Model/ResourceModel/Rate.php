<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_ShippingTableRates
 */


namespace Amasty\ShippingTableRates\Model\ResourceModel;

class Rate extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function deleteBy($methodId)
    {
        $this->getConnection()->delete($this->getMainTable(), 'method_id=' . (int)$methodId);
    }

    /**
     * @param int $methodId shipping method
     * @param array $data importing from csv and already exploded
     * @return string
     */
    public function batchInsert($methodId, $data)
    {
        $resultArray = [];
        $err = '';
        foreach ($data as $rate) {
            array_unshift($rate, $methodId);
            $resultArray[] = $rate;
        }
        $arrayWithFields = [
            'method_id',
            'country',
            'state',
            'zip_from',
            'zip_to',
            'price_from',
            'price_to',
            'weight_from',
            'weight_to',
            'qty_from',
            'qty_to',
            'shipping_type',
            'cost_base',
            'cost_percent',
            'cost_product',
            'cost_weight',
            'time_delivery',
            'num_zip_from',
            'num_zip_to'
        ];

        try {
            $this->getConnection()->insertArray($this->getMainTable(), $arrayWithFields, $resultArray);
        } catch (\Exception $e) {
            $err = $e->getMessage();
        }

        return $err;
    }

    public function filterWarehouseIdByWarehouseQuoteItem($quoteId)
    {
        $tableWarehouseQuoteItem = $this->getConnection()->getTableName('amasty_multiinventory_warehouse_quote_item');
        $select = $this->getConnection()->select()->from($tableWarehouseQuoteItem, 'warehouse_id')
            ->where('quote_id = :quote_id');
        $bind = [
            'quote_id' => $quoteId
        ];
        $itemWarehouse = $this->getConnection()->fetchCol($select, $bind);
        return $itemWarehouse;
    }

    protected function _construct()
    {
        $this->_init('amasty_table_rate', 'id');
    }
}
