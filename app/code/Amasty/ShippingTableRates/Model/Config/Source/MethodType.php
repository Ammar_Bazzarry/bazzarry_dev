<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_ShippingTableRates
 */

namespace Amasty\ShippingTableRates\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class MethodType
 * @package Amasty\ShippingTableRates\Model\Config\Source
 */
class MethodType implements ArrayInterface
{
    const METHOD_TYPE_STANDARD = '1';

    const METHOD_TYPE_STORE_PICKUP = '2';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            self::METHOD_TYPE_STANDARD => __('Standard'),
            self::METHOD_TYPE_STORE_PICKUP => __('Pickup at Store'),
        ];
    }
}
