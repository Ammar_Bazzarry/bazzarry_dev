<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Orderattr
 */


namespace Amasty\Orderattr\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Module\Manager;

class OSC extends Field
{
    /**
     * @var Magento\Framework\Module\Manager
     */
    private $manager;

    public function __construct(
        Manager $manager,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        $this->manager = $manager;
        parent::__construct($context, $data);
    }

    public function render(AbstractElement $element)
    {
        if ($this->manager->isEnabled('Amasty_Checkout')) {
            $element->setValue(__('Installed'));
            $element->setHtmlId('amasty_is_instaled');
            $url = $this->getUrl('adminhtml/system_config/edit', ['section' => 'amasty_checkout']);
            $element->setComment(__('Specify One Step Checkout settings properly See more details '
                . '<a href="%1" target="_blank">here</a>',
                $url
            ));
        } else {
            $element->setValue(__('Not Installed'));
            $element->setHtmlId('amasty_not_instaled');
            $element->setComment(__('Supply your Magento 2 with one step checkout to speed up the whole shopping '
                . 'process and significantly enhance customer satisfaction. See more details <a href="'
                . 'https://amasty.com/one-step-checkout-for-magento-2.html'
                . '?utm_source=extension&utm_medium=backend&utm_campaign=from_orderattr_to_osc_m2" target="_blank">'
                . 'here.'
            ));
        }

        return parent::render($element);
    }
}