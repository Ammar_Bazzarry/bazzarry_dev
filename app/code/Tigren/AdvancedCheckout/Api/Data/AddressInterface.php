<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Api\Data;


/**
 * Interface AddressInterface
 * @package Tigren\AdvancedCheckout\Api\Data
 */
interface AddressInterface extends \Magento\Customer\Api\Data\AddressInterface
{
    /**
     *
     */
    const LOCATION_TYPE = 'location_type';
    /**
     *
     */
    const NEAREST_LANDMARK = 'nearest_landmark';

    /**
     * @return mixed
     */
    public function getLocationType();

    /**
     * @param $locationType
     * @return mixed
     */
    public function setLocationType($locationType);

    /**
     * @return mixed
     */
    public function getNearestLandmark();

    /**
     * @param $nearestLandmark
     * @return mixed
     */
    public function setNearestLandmark($nearestLandmark);

}
