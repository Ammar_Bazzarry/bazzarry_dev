<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('customer_address_entity');

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            /**
             * Update table 'amasty_multiinventory_warehouse'
             */
            $connection = $installer->getConnection();
            if ($connection->isTableExists($tableName) == true) {
                if (!$connection->tableColumnExists($tableName, 'location_type')) {
                    $connection->addColumn(
                        $tableName,
                        'location_type',
                        [
                            'type' => Table::TYPE_TEXT,
                            'nullable' => 11,
                            'comment' => 'Location Type',
                        ]
                    );
                }
                if (!$connection->tableColumnExists($tableName, 'nearest_landmark')) {
                    $connection->addColumn(
                        $tableName,
                        'nearest_landmark',
                        [
                            'type' => Table::TYPE_TEXT,
                            'size' => 255,
                            'comment' => 'Nearest Landmark',
                        ]
                    );
                }
            }
        }

        $installer->endSetup();
    }
}
