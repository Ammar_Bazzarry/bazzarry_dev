<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Plugin\Magento\Customer\Model\Account;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;

class Redirect
{
    protected $coreSession;

    protected $url;

    protected $resultFactory;

    public function __construct(
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        UrlInterface $url,
        ResultFactory $resultFactory
    )
    {
        $this->coreSession = $coreSession;
        $this->url = $url;
        $this->resultFactory = $resultFactory;
    }

    public function aroundGetRedirect ($subject, \Closure $proceed)
    {
        if ($this->coreSession->getIsCheckout()) {
            /** @var \Magento\Framework\Controller\Result\Redirect $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $result->setUrl($this->url->getUrl('checkout/'));
            return $result;
        }

        return $proceed();
    }
}