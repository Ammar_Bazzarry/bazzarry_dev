<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Plugin\Magento\Quote\Model\Quote\Address;

use Magento\Quote\Model\Quote\Address;
use Magento\Sales\Api\Data\OrderAddressInterface;

class ToOrderAddress
{
    /**
     * @param Address $object
     * @param array $data
     * @return OrderAddressInterface
     */
    public function aroundConvert(
        \Magento\Quote\Model\Quote\Address\ToOrderAddress $subject,
        \Closure $proceed,
        Address $object,
        $data = []
    ) {
        $orderAddress = $proceed($object, $data);

        $orderAddress->setLocationType($object->getLocationType());
        $orderAddress->setNearestLandmark($object->getNearestLandmark());

        return $orderAddress;
    }

}
