<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Plugin;

use Magento\Customer\Api\AddressRepositoryInterface as Subject;
use Magento\Customer\Api\Data\AddressInterface as Entity;

class AddressRepositoryInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $httpRequest;

    /**
     * @var \Magento\Framework\Logger\Monolog
     */
    protected $logger;

    /**
     * @var \\Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;

    /**
     * AddressRepositoryInterface constructor.
     * @param \Magento\Framework\App\RequestInterface $httpRequest
     * @param \Magento\Framework\Logger\Monolog $logger
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $httpRequest,
        \Magento\Framework\Logger\Monolog $logger,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->httpRequest = $httpRequest;
        $this->logger = $logger;
        $this->connection = $resourceConnection->getConnection();;
    }

    /**
     * @param Subject $subject
     * @param Entity $entity
     * @param $addressId
     * @return Entity
     */
    public function afterGetById(Subject $subject, Entity $entity, $addressId)
    {
        $extensionAttributes = $entity->getExtensionAttributes();
        if ($extensionAttributes === null) {
            return $entity;
        }

        $additionAddressDataSelect = $this->connection->select()
            ->from(
                ['cae' => $this->connection->getTableName('customer_address_entity')],
                ['location_type', 'nearest_landmark']
            )
            ->where('entity_id = ?', $addressId);
        $additionAddressData = $this->connection->fetchRow($additionAddressDataSelect);

        if (!empty($additionAddressData)) {
            $extensionAttributes->setLocationType($additionAddressData['location_type']);
            $extensionAttributes->setNearestLandmark($additionAddressData['nearest_landmark']);
        } else {
            $extensionAttributes->setLocationType('');
            $extensionAttributes->setNearestLandmark('');
        }

        $entity->setExtensionAttributes($extensionAttributes);

        return $entity;
    }

    /**
     * @param Subject $subject
     * @param Entity $entity
     *
     * @return array [Entity]
     */
    public function beforeSave(Subject $subject, Entity $entity)
    {
        $extensionAttributes = $entity->getExtensionAttributes();
        if ($extensionAttributes === null) {
            return [$entity];
        }


        $customAttributes = [
            'location_type' => $extensionAttributes->getLocationType() ?: $this->httpRequest->getParam('location_type'),
            'nearest_landmark' => $extensionAttributes->getNearestLandmark() ?: $this->httpRequest->getParam('nearest_landmark'),
        ];

        $entity->setLocationType($extensionAttributes->getLocationType() ?: $this->httpRequest->getParam('location_type'));
        $entity->setNearestLandmark($extensionAttributes->getNearestLandmark() ?: $this->httpRequest->getParam('nearest_landmark'));

        foreach ($customAttributes as $attributeCode => $attributeValue) {
            $entity->setCustomAttribute($attributeCode, $attributeValue);
        }

        return [$entity];
    }
}