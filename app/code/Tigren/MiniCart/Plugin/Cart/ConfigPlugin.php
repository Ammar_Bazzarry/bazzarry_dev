<?php

namespace Tigren\MiniCart\Plugin\Cart;

use Magento\Checkout\Block\Cart\Sidebar;
use Magento\Framework\UrlInterface;

class ConfigPlugin
{
    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * ConfigPlugin constructor.
     * @param UrlInterface $url
     */
    public function __construct(
        UrlInterface $url
    )
    {
        $this->url = $url;
    }

    /**
     * @param Sidebar $subject
     * @param array $result
     * @return array
     */
    public function afterGetConfig(
        Sidebar $subject,
        array $result
    )
    {
        $result['emptyMiniCart'] = $this->url->getUrl('minicart/cart/emptycart');
        return $result;
    }
}