<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Tigren\Vendors\Override\Vnecoms\Controller\Seller;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\Uploader;
use Vnecoms\Vendors\Model\Session as VendorSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Action\Context;
use Vnecoms\Vendors\Model\Vendor;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Psr\Log\LoggerInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Store\Model\StoreManagerInterface;

class RegisterPost extends \Vnecoms\Vendors\Controller\Seller\RegisterPost
{
    /**
     * @var \Vnecoms\Vendors\Model\Session
     */
    protected $_vendorSession;
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    
    /**
     * @var \Vnecoms\Vendors\Helper\Data
     */
    protected $_vendorHelper;
    
    /**
     * @var \Vnecoms\Vendors\Model\VendorFactory
     */
    protected $_vendorFactory;
    
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * File system
     *
     * @var Filesystem
     */
    protected $_fileSystem;
    /**
     * File Uploader factory
     *
     * @var UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @var CustomerFactory
     */
    protected $customerFactory;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    public function __construct(
        Context $context,
        VendorSession $vendorSession,
        ScopeConfigInterface $scopeConfig,
        \Vnecoms\Vendors\Helper\Data $vendorHelper,
        \Vnecoms\Vendors\Model\VendorFactory $vendorFactory,
        Filesystem $fileSystem,
        UploaderFactory $fileUploaderFactory,
        LoggerInterface $logger,
        CustomerFactory $customerFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager= $storeManager;
        $this->customerFactory=$customerFactory;
        $this->_fileSystem = $fileSystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_logger = $logger;
        parent::__construct($context, $vendorSession, $scopeConfig, $vendorHelper, $vendorFactory);
    }

    /**
     * Renders CMS Home page
     *
     * @param string|null $coreRoute
     * @return \Magento\Framework\Controller\Result\Forward
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute($coreRoute = null)
    {
        $website = $this->storeManager->getWebsite()->getId();
        if (!$this->_vendorHelper->moduleEnabled()) {
            return $this->_forward('no-route');
        }
        
        if (!$this->_vendorHelper->isEnableVendorRegister()) {
            $this->_messageManager->addError(__("You don't have permission to access this page"));
            return $this->_redirect('customer/account');
        }
        $files = $this->getRequest()->getFiles();
        $path = $this->_fileSystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath(
            'vendors/document/'
        );
        $documentName = !empty($files['document-file']['name']);
        if ($documentName){
            $newName = preg_replace('/[^a-zA-Z0-9_\-\.]/', '', $files['document-file']['name']);
            if (substr($newName, 0, 1) == '.') {
                $newName = 'document_' . $newName;
            }
            $i = 0;
            while (@file_exists($path . $newName)) {
                $newName = ++$i . '_' . $newName;
            }
            try {
                if ($documentName) {
                    /** @var $uploader Uploader */
                    $uploader = $this->_fileUploaderFactory->create(['fileId' => 'document-file']);
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                    $uploader->setAllowRenameFiles(true);
                    $uploader->save($path, $newName);
                }
            } catch (\Exception $e) {
                $this->_logger->critical($e);
            }
        }
        $vendorData = $this->getRequest()->getParam('vendor_data');
        if ($vendorData && is_array($vendorData)) {
            try {
                $vendor = $this->_vendorFactory->create();
                $vendorData['document_file']['value']= $newName;
                $vendor->setData($vendorData);
                $vendor->setGroupId($this->_vendorHelper->getDefaultVendorGroup());

                $customer= $this->customerFactory->create();
                $customer->setWebsiteId($website);
                $customerEmail = $this->getRequest()->getParam('email');
                $customer->loadByEmail($customerEmail);
                if (!empty($customer)){
                    $vendor->setCustomer($customer);
                }
                $vendor->setWebsiteId($customer->getWebsiteId());
                
                if ($this->_vendorHelper->isRequiredVendorApproval()) {
                    $vendor->setStatus(Vendor::STATUS_PENDING);
                    $message = __("Your seller account has been created and awaiting for approval.");
                } else {
                    $vendor->setStatus(Vendor::STATUS_APPROVED);
                    $message = __("Your seller account has been created.");
                }
                
                $errors = $vendor->validate();
                
                if ($errors !== true) {
                    throw new \Exception(implode("<br />", $errors));
                }
                
                $vendor->save();
                if (!$vendor->getCustomer()){
                    $message = __("Your seller account must have the same email as your customer account.");
                    $this->_messageManager->addError($message);
                    return $this->_redirect('marketplace/seller/register');
                }
                if ($this->_vendorHelper->isUsedCustomVendorUrl()) {
                    return $this->_redirect('vendors/account/login', ['success_message' => base64_encode(__("Your seller account has been created. You can now login to vendor panel."))]);
                }
                $this->_messageManager->addSuccess($message);
                return $this->_redirect('vendors');
            } catch (\Exception $e) {
                $this->_messageManager->addError($e->getMessage());
                $this->_vendorSession->setFormData($vendorData);
                return $this->_redirect('marketplace/seller/register');
            }
        } else {
            $this->_messageManager->addError(__("POST data is invalid"));
            return $this->_redirect('marketplace/seller/register');
        }
    }
}
