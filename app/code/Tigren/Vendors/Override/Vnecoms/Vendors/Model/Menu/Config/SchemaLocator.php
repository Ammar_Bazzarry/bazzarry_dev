<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Override\Vnecoms\Vendors\Model\Menu\Config;

use Magento\Framework\Module\Dir;

/**
 * Class SchemaLocator
 * @package Tigren\Vendors\Override\Vnecoms\Vendors\Model\Menu\Config
 */
class SchemaLocator extends \Magento\Backend\Model\Menu\Config\SchemaLocator
{
    /**
     * @param \Magento\Framework\Module\Dir\Reader $moduleReader
     */
    public function __construct(\Magento\Framework\Module\Dir\Reader $moduleReader)
    {
        $this->_schema = $moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Tigren_Vendors') . '/menu.xsd';
    }
}
