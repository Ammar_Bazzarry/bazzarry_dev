<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Override\Vnecoms\Vendors\Block\Vendors\Menu;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class User
 * @package Tigren\Vendors\Override\Vnecoms\Vendors\Block\Vendors\Menu
 */
class User extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Vnecoms\Vendors\Model\Session
     */
    protected $vendorSession;

    /**
     * @var \Vnecoms\VendorsConfig\Helper\Data
     */
    protected $_configHelper;

    /**
     * @var \Magento\MediaStorage\Helper\File\Storage\Database
     */
    protected $_fileStorageDatabase;

    /**
     * @var \Magento\Framework\Filesystem\Directory\ReadInterface
     */
    protected $_mediaDirectory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Vnecoms\Vendors\Model\Session $vendorSession
     * @param \Vnecoms\VendorsConfig\Helper\Data $helper
     * @param \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageDatabase
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Vnecoms\Vendors\Model\Session $vendorSession,
        \Vnecoms\VendorsConfig\Helper\Data $helper,
        \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageDatabase,
        array $data = []
    ) {
        $this->_fileStorageDatabase = $fileStorageDatabase;
        $this->_configHelper = $helper;
        $this->vendorSession = $vendorSession;
        $this->_mediaDirectory = $context->getFilesystem()->getDirectoryRead(DirectoryList::MEDIA);
        return parent::__construct($context, $data);
    }

    /**
     * Get the name of currently logged in vendor
     *
     * @return string
     */
    public function getVendorName()
    {
        return $this->vendorSession->getCustomer()->getName();
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getVendorAvatar()
    {
        $vendor = $this->vendorSession->getVendor();
        $avatar = $this->_configHelper->getVendorConfig(
            'general/store_information/avatar',
            $vendor->getId()
        );
        $basePath = 'ves_vendors/avatar/';
        $path = $basePath . $avatar;
        $logoUrl = $this->_storeManager->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $path;
        if ($avatar !== null && $this->checkIsFile($path)) {
            return $logoUrl;
        }

        return $this->getViewFileUrl('images/user.png');
    }

    /**
     * If DB file storage is on - find there, otherwise - just file_exists
     *
     * @param string $filename relative file path
     * @return bool
     */
    protected function checkIsFile($filename)
    {
        if ($this->_fileStorageDatabase->checkDbUsage() && !$this->_mediaDirectory->isFile($filename)) {
            $this->_fileStorageDatabase->saveFileToFilesystem($filename);
        }
        return $this->_mediaDirectory->isFile($filename);
    }
}
