<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2020 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Override\Vnecoms\Vendors\Block\Account;

use Vnecoms\Vendors\Model\Source\PanelType;

/**
 * Class Link
 * @package Tigren\Vendors\Override\Vnecoms\Vendors\Block\Account
 */
class Link extends \Magento\Framework\View\Element\Html\Link implements \Magento\Customer\Block\Account\SortLinkInterface
{

    /**
     * @var \Vnecoms\Vendors\Helper\Data
     */
    protected $_vendorHelper;
    
    /**
     * @var \Vnecoms\Vendors\Model\Session
     */
    protected $_vendorSession;
    
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Vnecoms\Vendors\Helper\Data $vendorHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Vnecoms\Vendors\Helper\Data $vendorHelper,
        \Vnecoms\Vendors\Model\Session $session,
        array $data = []
    ) {
        $this->_vendorHelper = $vendorHelper;
        $this->_vendorSession = $session;
        parent::__construct($context, $data);
    }
    
    /**
     * Is registered vendor
     *
     * @return boolean
     */
    public function getIsRegisteredVendor()
    {
        return $this->_vendorSession->isLoggedIn() && $this->_vendorSession->getVendor()->getId();
    }
    
    /**
     * @return string
     */
    public function getHref()
    {
        if ($this->_vendorHelper->getPanelType() == PanelType::TYPE_SIMPLE) {
            return $this->getUrl('marketplace/seller/index');
        }

        return $this->_vendorHelper->getUrl('dashboard');
    }
    
    /**
     * (non-PHPdoc)
     *
     * @see \Magento\Framework\View\Element\Html\Link::_toHtml()
     */
    protected function _toHtml()
    {
        if (!$this->_vendorHelper->moduleEnabled() ||
            !$this->getIsRegisteredVendor()
        ) {
            return '';
        }
        
        return parent::_toHtml();
    }

    /**
     * @inheritDoc
     */
    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }
}
