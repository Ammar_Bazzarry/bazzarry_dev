<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Override\Vnecoms\Vendors\Observer;

use Exception;
use Magento\Customer\Model\Session;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Message\ManagerInterface;
use Magento\MediaStorage\Model\File\Uploader;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Store\Model\StoreManagerInterface;
use Vnecoms\Vendors\Helper\Data;
use Vnecoms\Vendors\Model\Vendor;
use Vnecoms\Vendors\Model\VendorFactory;
use Psr\Log\LoggerInterface;

/**
 * Class CustomerRegisterSuccess
 * @package Tigren\Vendors\Override\Vnecoms\Vendors\Observer
 */
class CustomerRegisterSuccess implements ObserverInterface
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var Data
     */
    protected $_vendorHelper;

    /**
     * @var VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var Data
     */
    protected $vendorHelper;

    /**
     * @var RegionFactory
     */
    protected $regionFactory;

    /**
     * @var UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * File system
     *
     * @var Filesystem
     */
    protected $_fileSystem;
    /**
     * File Uploader factory
     *
     * @var UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param Data $vendorHelper
     * @param ManagerInterface $messageManager
     * @param VendorFactory $vendorFactory
     * @param RegionFactory $regionFactory
     * @param Session $customerSession
     * @param UploaderFactory $uploaderFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Data $vendorHelper,
        ManagerInterface $messageManager,
        VendorFactory $vendorFactory,
        RegionFactory $regionFactory,
        Session $customerSession,
        UploaderFactory $uploaderFactory,
        StoreManagerInterface $storeManager,
        Filesystem $fileSystem,
        UploaderFactory $fileUploaderFactory,
        LoggerInterface $logger
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_vendorHelper = $vendorHelper;
        $this->_vendorFactory = $vendorFactory;
        $this->_messageManager = $messageManager;
        $this->customerSession = $customerSession;
        $this->regionFactory = $regionFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->_storeManager = $storeManager;
        $this->_fileSystem = $fileSystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_logger = $logger;
    }

    /**
     * Add the notification if there are any vendor awaiting for approval.
     * @param Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        if (!$this->_vendorHelper->isEnableVendorRegister()) {
            return;
        }

        $customer = $observer->getCustomer();
        $controller = $observer->getAccountController();
        $vendorData = $controller->getRequest()->getParam('vendor_data');
        $files = $controller->getRequest()->getFiles();
        if (!$controller->getRequest()->getParam('is_seller', false)) {
            return;
        }
        $path = $this->_fileSystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath(
            'vendors/document/'
        );
        $documentName = !empty($files['document-file']['name']);
        $newName = preg_replace('/[^a-zA-Z0-9_\-\.]/', '', $files['document-file']['name']);
        if (substr($newName, 0, 1) == '.') {
            $newName = 'document_' . $newName;
        }
        $i = 0;
        while (@file_exists($path . $newName)) {
            $newName = ++$i . '_' . $newName;
        }
        try {
            if ($documentName) {
                /** @var $uploader Uploader */
                $uploader = $this->_fileUploaderFactory->create(['fileId' => 'document-file']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploader->setAllowRenameFiles(true);
                $uploader->save($path, $newName);
            }
        } catch (\Exception $e) {
            $this->_logger->critical($e);
        }
        if ($vendorData && is_array($vendorData)) {
            $vendor = $this->_vendorFactory->create();
            $vendorData['document_file']['value']= $newName;
            $vendor->setData($vendorData);
            $vendor->setGroupId($this->_vendorHelper->getDefaultVendorGroup());
            $vendor->setCustomer($customer);
            $vendor->setWebsiteId($customer->getWebsiteId());
            if ($this->_vendorHelper->isRequiredVendorApproval()) {
                $vendor->setStatus(Vendor::STATUS_PENDING);
                $vendor->sendNewAccountEmail("registered");

            } else {
                $vendor->setStatus(Vendor::STATUS_APPROVED);
                $vendor->sendNewAccountEmail("active");
            }

            $errors = $vendor->validate();

            if ($errors !== true) {
                throw new Exception(implode("<br />", $errors));
            }
            $vendor->save();
            if ($this->_vendorHelper->isUsedCustomVendorUrl()) {
                $redirectUrl = $this->_vendorHelper->getUrl(
                    'account/login',
                    ['success_message' => base64_encode(__("Your seller account has been created. You can now login to vendor panel."))]
                );
            } else {
                $redirectUrl = $this->_vendorHelper->getHomePageUrl();
            }

            $this->customerSession->setBeforeAuthUrl($redirectUrl);
            $this->_messageManager->getMessages(true);
        }
    }
}
