<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Model\Source\Option\Attribute;

use Magento\Eav\Model\Entity\Attribute\Source\Table;

/**
 * Class Business
 * @package Tigren\Vendors\Model\Source\Attribute
 */
class Business extends Table
{
    /**
     * @var
     */
    protected $_options;

    /**
     * @return array|null
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['label' => __('Individual'), 'value' => 1],
                ['label' => __('Business'), 'value' => 2]
            ];
        }
        return $this->_options;
    }

    /**
     * Retrieve Option value text
     *
     * @param string $value
     * @return mixed
     */
    public function getOptionText($value)
    {
        // TODO: Implement getOptionText() method.
    }
}
