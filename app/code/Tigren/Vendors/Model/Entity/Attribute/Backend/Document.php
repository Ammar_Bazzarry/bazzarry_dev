<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Model\Entity\Attribute\Backend;

use Exception;
use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Magento\MediaStorage\Model\File\Uploader;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Psr\Log\LoggerInterface;

/**
 * Class File
 * @package Tigren\Vendors\Model\Source\Attribute
 */
class Document extends AbstractBackend
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var File
     */
    protected $file;

    /**
     * @var UploaderFactory
     */
    protected $fileUploaderFactory;

    /**
     * File constructor.
     * @param LoggerInterface $logger
     * @param Filesystem $filesystem
     * @param File $file
     * @param UploaderFactory $fileUploaderFactory
     */
    public function __construct(
        LoggerInterface $logger,
        Filesystem $filesystem,
        File $file,
        UploaderFactory $fileUploaderFactory
    ) {
        $this->file = $file;
        $this->filesystem = $filesystem;
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->logger = $logger;
    }

    /**
     * @return $this|AbstractBackend
     */
    public function afterSave($object)
    {
        $path = $this->filesystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath(
            'vendors/document/'
        );
        $delete = $object->getData('file_delete');
        if ($delete == 1) {
            $object->setData('document_file', '');
        }
        if (empty($_FILES)) {
            return $this;// if no image is set then nothing to do
        }
        $oldFile = $object->getData('document_file');
        if (($oldFile != '') && !empty($_FILES)) {
            @unlink($path . $oldFile);
            $object->getData('document_file', '');
        }
        try {
            /** @var $uploader Uploader */
            $uploader = $this->fileUploaderFactory->create(['fileId' => 'document_file']);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
            $uploader->setAllowRenameFiles(true);
            $result = $uploader->save($path);
            $newName = preg_replace('/[^a-zA-Z0-9_\-\.]/', '', $result['file']);
            if (substr($newName, 0, 1) == '.') // all non-english symbols
            {
                $newName = 'vendor_' . $newName;
            }
            $i = 0;
            while (file_exists($path . $newName)) {
                $newName = ++$i . '_' . $newName;
            }
            $object->setData('document_file', $newName);
        } catch (Exception $e) {
            if ($e->getCode() != Uploader::TMP_NAME_EMPTY) {
                $this->logger->critical($e);
            }
        }
        return $this;
    }
}