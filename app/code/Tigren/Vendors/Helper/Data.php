<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Registry;
use Tigren\VendorsConfig\Block\System\Config\Form\Field\Day;
use Tigren\VendorsConfig\Block\System\Config\Form\Field\Hour;
use Tigren\VendorsConfig\Block\System\Config\Form\Field\Type;

/**
 * Class Data
 * @package Tigren\Vendors\Helper
 */
class Data extends AbstractHelper
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;
    /**
     * @var \Vnecoms\VendorsConfig\Helper\Data
     */
    protected $_configHelper;
    /**
     * @var Day
     */
    protected $_vendorDay;
    /**
     * @var Hour
     */
    protected $_vendorHour;
    /**
     * @var Type
     */
    protected $_vendorHourType;

    /**
     * Data constructor.
     * @param Context $context
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        Registry $registry,
        \Vnecoms\VendorsConfig\Helper\Data $configHelper,
        Day $vendorDay,
        Hour $vendorHour,
        Type $vendorHourType
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $registry;
        $this->_configHelper = $configHelper;
        $this->_vendorDay = $vendorDay;
        $this->_vendorHour = $vendorHour;
        $this->_vendorHourType = $vendorHourType;
    }

    /**
     * @return mixed
     */
    public function getVendorId()
    {
        $vendor = $this->_coreRegistry->registry('vendor');
        if (!$vendor && $product = $this->_coreRegistry->registry('product')) {
            if ($vendorId = $product->getVendorId()) {
                return $vendorId;
            }
        }
        return $vendor->getId();
    }

    /**
     * @return bool
     */
    public function showProfile()
    {
        return true;
    }

    /**
     * @param $vendorId
     * @return string
     */
    public function getOperationTime($vendorId)
    {
        $vendorHourConfig = $this->_configHelper->getVendorConfig('general/store_information/hours', $vendorId);
        $vendorHourConfigs = @unserialize($vendorHourConfig);

        $vendorConfig = [];
        if (isset($vendorHourConfigs["__empty"])) {
            unset($vendorHourConfigs["__empty"]);
        }
        if (!$vendorHourConfigs) {
            return false;
        }
        foreach ($vendorHourConfigs as $hourConfig) {
            $vendorConfig['from_hour'] = $hourConfig['hour_from'];
            $vendorConfig['to_hour'] = $hourConfig['to_hour'];
            $vendorConfig['type_from'] = $hourConfig['type_from'];
            $vendorConfig['type_to'] = $hourConfig['type_to'];
            $vendorConfig['day_from'] = $hourConfig['day_from'];
            $vendorConfig['day_to'] = $hourConfig['day_to'];
        }

        $hourTypeLabel = $this->getHourType();
        foreach ($hourTypeLabel as $value => $label) {
            if ($vendorConfig['type_from'] == $value) {
                $vendorConfig['type_from'] = $label;
            }
            if ($vendorConfig['type_to'] == $value) {
                $vendorConfig['type_to'] = $label;
            }
        }

        $dayLabels = $this->getDay();
        foreach ($dayLabels as $value => $label) {
            if ($vendorConfig['day_from'] == $value) {
                $vendorConfig['day_from'] = $label;
            }
            if ($vendorConfig['day_to'] == $value) {
                $vendorConfig['day_to'] = $label;
            }
        }

        $vendorConfigData = "From " . $vendorConfig['from_hour'] . $vendorConfig['type_from'] . " to " . $vendorConfig['to_hour'] . $vendorConfig['type_to'] . ", " . $vendorConfig['day_from'] . " to " . $vendorConfig['day_to'];

        return $vendorConfigData;
    }

    /**
     * @return array
     */
    public function getHourType(): array
    {
        return [
            '0' => 'p.m',
            '1' => 'a.m'
        ];
    }

    /**
     * @return array
     */
    public function getDay(): array
    {
        return [
            '1' => 'Monday',
            '2' => 'Tuesday',
            '3' => 'Wednesday',
            '4' => 'Thusday',
            '5' => 'Friday',
            '6' => 'Saturday',
            '7' => 'Sunday'
        ];
    }
}
