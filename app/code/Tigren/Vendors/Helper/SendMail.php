<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Helper;

use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;

/**
 * Class SendMail
 * @package Tigren\Vendors\Helper
 */
class SendMail
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @param TransportBuilder $transportBuilder
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->transportBuilder = $transportBuilder;
    }

    /**
     * Send email with coupon code to recipient
     *
     * @return void
     * @throws LocalizedException
     * @throws MailException
     */
    public function sendApprovedEmail($sellerName, $sellerEmail)
    {
        $sender = $this->getEmailSender();
        $this->send(
            $this->getEmailTemplate('vendor_approved'),
            [
                'area' => Area::AREA_FRONTEND,
                'store' => Store::DEFAULT_STORE_ID
            ],
            $this->prepareTemplateVars(
                [
                    'seller_name' => $sellerName,
                    'seller_mail' => $sellerEmail,

                ]
            ),
            $sender,
            $sellerEmail,
            $sellerName
        );
    }

    /**
     * @param $sellerName
     * @param $sellerEmail
     * @throws LocalizedException
     * @throws MailException
     */
    public function sendRejectEmail($sellerName, $sellerEmail)
    {
        $sender = $this->getEmailSender();
        $this->send(
            $this->getEmailTemplate('vendor_reject'),
            [
                'area' => Area::AREA_FRONTEND,
                'store' => Store::DEFAULT_STORE_ID
            ],
            $this->prepareTemplateVars(
                [
                    'seller_name' => $sellerName,
                    'seller_mail' => $sellerEmail,

                ]
            ),
            $sender,
            $sellerEmail,
            $sellerName
        );
    }

    /**
     * Send email
     *
     * @param string $templateId
     * @param array $templateOptions
     * @param array $templateVars
     * @param string $from
     * @param string $recipientEmail
     * @param string $recipientName
     * @return void
     * @throws LocalizedException
     * @throws MailException
     */
    private function send(
        $templateId,
        array $templateOptions,
        array $templateVars,
        $from,
        $recipientEmail,
        $recipientName
    ) {
        $this->transportBuilder
            ->setTemplateIdentifier($templateId)
            ->setTemplateOptions($templateOptions)
            ->setTemplateVars($templateVars)
            ->setFrom($from)
            ->addTo($recipientEmail, $recipientName);
        $this->transportBuilder->getTransport()->sendMessage();
    }

    /**
     * Prepare template vars
     *
     * @param array $data
     * @return array
     */
    private function prepareTemplateVars($data)
    {
        $templateVars = [];

        if (isset($data['seller_name'])) {
            $templateVars['seller_name'] = $data['seller_name'];
        }
        if (isset($data['seller_mail'])) {
            $templateVars['customer_email'] = $data['seller_mail'];
        }
        return $templateVars;
    }

    /**
     * @param $path
     * @return mixed
     */
    public function getEmailTemplate($path)
    {
        return $this->scopeConfig->getValue('general/vendor_email/' . $path, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getEmailSender()
    {
        return $this->scopeConfig->getValue('general/vendor_email/vendor_email_sender', ScopeInterface::SCOPE_STORE);
    }
}
