<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Plugin\Magento\Theme\Model\View;

/**
 * Class Design
 * @package Tigren\Vendors\Plugin\Magento\Theme\Model\View
 */
class Design
{
    /**
     * Get default theme which declared in configuration
     *
     * Write default theme to core_config_data
     *
     * @param \Magento\Theme\Model\View\Design $subject
     * @param \Closure $proceed
     * @param string|null $area
     * @param array $params
     * @return string|int
     */
    public function aroundGetConfigurationDesignTheme(
        \Magento\Theme\Model\View\Design $subject,
        \Closure $proceed,
        $area = null,
        array $params = []
    ) {
        if (!$area) {
            $area = $subject->getArea();
        }

        if ($area == 'vendors') {
            return 'Tigren/vendor';
        }

        return $proceed($area, $params);
    }
}
