<?php

/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Observer;

use Exception;
use Magento\Customer\Model\Customer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;

/**
 * Class SaveCustomer
 * @package Tigren\Vendors\Observer
 */
class SaveCustomer implements ObserverInterface
{

    /**
     * @param Observer $observer
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        $request = $observer->getData('request');
        $dataPost = $request->getPostValue();
        $vendor = $observer->getData('vendor');
        /** @var Customer $customer */
        $customer = $vendor->getCustomer();
        if ($customer->getId()) {
            if(isset($dataPost['firstname']) && $dataPost['firstname']){
                $customer->setFirstname($dataPost['firstname']);
            }
            if(isset($dataPost['lastname']) && $dataPost['lastname']){
                $customer->setLastname($dataPost['lastname']);
            }
            $customer->save();
        }
    }
}
