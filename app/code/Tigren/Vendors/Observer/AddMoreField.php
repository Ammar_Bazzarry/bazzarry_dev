<?php

/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Observer;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Quote\Model\Quote\AddressFactory;
use Psr\Log\LoggerInterface as Logger;

/**
 * Class SalesOrderSaveBefore
 * @package Tigren\StorePickup\Observer
 */
class AddMoreField implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * AddMoreField constructor.
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Magento\Framework\Registry $registry
    ) {
        $this->_coreRegistry = $registry;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $model = $this->_coreRegistry->registry('current_vendor');
        $fieldset = $observer->getData('fieldset');
        if ($fieldset->getId() == 'fieldset_1') {
            $fieldset->addField('firstname', 'text',
                [
                    'name' => 'firstname',
                    'label' => __('First name'),
                    'title' => __('First name'),
                    'required' => false
                ]);
            $fieldset->addField('lastname', 'text',
                [
                    'name' => 'lastname',
                    'label' => __('Last name'),
                    'title' => __('Last name'),
                    'required' => false
                ]);
        }

    }
}
