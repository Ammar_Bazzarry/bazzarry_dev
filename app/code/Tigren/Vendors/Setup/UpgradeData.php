<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Setup;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Config;
use Vnecoms\Vendors\Model\Vendor;

/**
 * Class UpgradeData
 * @package Tigren\CustomTheme\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * UpgradeData constructor.
     * @param EavSetupFactory $eavSetupFactory
     * @param Config $eavConfig
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        Config $eavConfig
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }

    /**
     * {@inheritdoc}
     * @throws LocalizedException
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $attribute = $eavSetup->getAttribute('vendor', 'region');
            $sql = "UPDATE eav_attribute SET frontend_label='City' Where attribute_id =" . $attribute['attribute_id'];
            $setup->getConnection()->query($sql);
            $attribute = $eavSetup->getAttribute('vendor', 'region_id');
            $sql = "UPDATE eav_attribute SET frontend_label='City' Where attribute_id =" . $attribute['attribute_id'];
            $setup->getConnection()->query($sql);
            $attribute = $eavSetup->getAttribute('vendor', 'city');
            $sql = "UPDATE ves_vendor_eav_attribute SET is_used_in_registration_form=0,is_used_in_profile_form=0,hide_from_vendor_panel=1 Where attribute_id =" . $attribute['attribute_id'];
            $setup->getConnection()->query($sql);
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {

            $eavSetup->addAttribute(
                Vendor::ENTITY,
                'business_type',
                [
                    'type' => 'int',
                    'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                    'frontend' => '',
                    'label' => 'Seller Type',
                    'input' => 'select',
                    'class' => '',
                    'source' => 'Tigren\Vendors\Model\Source\Option\Attribute\Business',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => '',
                ]
            );
            $businessType = $this->eavConfig->getAttribute(Vendor::ENTITY, 'business_type');
            $businessType->setData('is_used_in_registration_form', 1);
            $businessType->setData('is_used_in_profile_form', 1);
            $businessType->setData('hide_from_vendor_panel', 0);
            $businessType->save();
            $eavSetup->addAttribute(
                Vendor::ENTITY,
                'vertify_type',
                [
                    'type' => 'int',
                    'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                    'frontend' => '',
                    'label' => 'Document Type',
                    'input' => 'select',
                    'class' => '',
                    'source' => 'Tigren\Vendors\Model\Source\Option\Attribute\Type',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => '',
                ]
            );
            $vertifyType = $this->eavConfig->getAttribute(Vendor::ENTITY, 'vertify_type');
            $vertifyType->setData('is_used_in_registration_form', 1);
            $vertifyType->setData('is_used_in_profile_form', 1);
            $vertifyType->setData('hide_from_vendor_panel', 0);
            $vertifyType->save();
            $eavSetup->addAttribute(
                Vendor::ENTITY,
                'document_id',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Document ID',
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
            $documentId = $this->eavConfig->getAttribute(Vendor::ENTITY, 'document_id');
            $documentId->setData('is_used_in_registration_form', 1);
            $documentId->setData('is_used_in_profile_form', 1);
            $documentId->setData('hide_from_vendor_panel', 0);
            $documentId->save();
            $eavSetup->addAttribute(
                Vendor::ENTITY,
                'seller_gender',
                [
                    'type' => 'int',
                    'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                    'frontend' => '',
                    'label' => 'Gender',
                    'input' => 'select',
                    'class' => '',
                    'source' => 'Tigren\Vendors\Model\Source\Option\Attribute\Gender',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => '',
                ]
            );
            $gender = $this->eavConfig->getAttribute(Vendor::ENTITY, 'seller_gender');
            $gender->setData('is_used_in_registration_form', 1);
            $gender->setData('is_used_in_profile_form', 1);
            $gender->setData('hide_from_vendor_panel', 0);
            $gender->save();
            $eavSetup->addAttribute(
                Vendor::ENTITY,
                'document_file',
                [
                    'type' => 'varchar',
                    'backend' => 'Tigren\Vendors\Model\Entity\Attribute\Backend\Document',
                    'frontend' => '',
                    'label' => 'Vertify Document',
                    'input' => 'file',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => '',
                ]
            );
            $documentFile = $this->eavConfig->getAttribute(Vendor::ENTITY, 'document_file');
            $documentFile->setData('is_used_in_registration_form', 1);
            $documentFile->setData('is_used_in_profile_form', 1);
            $documentFile->setData('hide_from_vendor_panel', 0);
            $documentFile->save();
            $eavSetup->addAttribute(
                Vendor::ENTITY,
                'file_delete',
                [
                    'type' => 'int',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Is Delete Document',
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'unique' => false,
                    'apply_to' => '',
                ]
            );
            $attribute = $this->eavConfig->getAttribute(Vendor::ENTITY, 'file_delete');
            $attribute->setData('is_used_in_registration_form', 0);
            $attribute->setData('is_used_in_profile_form', 0);
            $attribute->setData('hide_from_vendor_panel', 0);
            $attribute->save();
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $connection = $setup->getConnection();
            $table = $setup->getTable('ves_vendor_fieldset_attr');
            $businessType = $this->eavConfig->getAttribute(Vendor::ENTITY, 'business_type');
            $connection->insert(
                $table,
                ['fieldset_id' => 1, 'attribute_id' => $businessType['attribute_id']]
            );
            $vertifyType = $this->eavConfig->getAttribute(Vendor::ENTITY, 'vertify_type');
            $connection->insert(
                $table,
                ['fieldset_id' => 1, 'attribute_id' => $vertifyType['attribute_id']]
            );
            $documentId = $this->eavConfig->getAttribute(Vendor::ENTITY, 'document_id');
            $connection->insert(
                $table,
                ['fieldset_id' => 1, 'attribute_id' => $documentId['attribute_id']]
            );
            $gender = $this->eavConfig->getAttribute(Vendor::ENTITY, 'seller_gender');
            $connection->insert(
                $table,
                ['fieldset_id' => 1, 'attribute_id' => $gender['attribute_id']]
            );
            $documentFile = $this->eavConfig->getAttribute(Vendor::ENTITY, 'document_file');
            $connection->insert(
                $table,
                ['fieldset_id' => 8, 'attribute_id' => $documentFile['attribute_id']]
            );
            $customer_country = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'country_id');
            $customer_country->setIsUserDefined(1)->save();
            $vendor_country = $this->eavConfig->getAttribute(Vendor::ENTITY, 'country_id');
            $vendor_country->setData('is_used_in_registration_form', 0);
            $vendor_country->setData('is_used_in_profile_form', 0);
            $vendor_country->setData('hide_from_vendor_panel', 1);
            $vendor_country->save();
        }
        $setup->endSetup();
    }
}
