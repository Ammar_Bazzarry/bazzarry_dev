<?php

namespace Tigren\Vendors\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup,
                            ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $setup->getConnection()->changeColumn(
                $setup->getTable('cataloginventory_stock_item'),
                'qty',
                'qty',
                [
                    'type' => Table::TYPE_DECIMAL,
                    'length' => '12,0',
                    'nullable' => true,
                    'comment' => ''
                ]
            );
        }

        $setup->endSetup();
    }
}