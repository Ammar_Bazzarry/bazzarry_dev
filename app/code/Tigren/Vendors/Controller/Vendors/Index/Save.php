<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Controller\Vendors\Index;

use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\Uploader;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Psr\Log\LoggerInterface;
use Vnecoms\Vendors\App\Action\Context;
use Vnecoms\Vendors\Controller\Vendors\Action;

class Save extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    protected $_aclResource = 'Vnecoms_Vendors::account';

    /**
     * File system
     *
     * @var Filesystem
     */
    protected $_fileSystem;
    /**
     * File Uploader factory
     *
     * @var UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @return void
     */
    public function __construct(
        Context $context,
        Filesystem $fileSystem,
        UploaderFactory $fileUploaderFactory,
        LoggerInterface $logger
    ) {
        $this->_fileSystem = $fileSystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_logger = $logger;
        parent::__construct($context);
    }

    public function execute()
    {
        $vendor = $this->_session->getVendor();
        if ($vendor->getStatus() != \Vnecoms\Vendors\Model\Vendor::STATUS_APPROVED) {
            $this->messageManager->addError(__("Your seller account status is %1, You are not allowed to access this page", $vendor->getStatusLabel()));
            $this->_redirect('account');
            return;
        }
        $request = $this->getRequest();
        $file = $this->getRequest()->getFiles();
        if ($request->getPostValue()) {
            try {
                $vendorData = $request->getParam('vendor_data');

                $notAllowedAttributes = $this->_helper->getNotSavedVendorAttributes();
                foreach ($notAllowedAttributes as $attr) {
                    unset($vendorData[$attr]);
                }
                $path = $this->_fileSystem->getDirectoryRead(
                    DirectoryList::MEDIA
                )->getAbsolutePath(
                    'vendors/document/'
                );
                $documentName = !empty($file['vendor_data']['document_file']['name']);
                $newName = preg_replace('/[^a-zA-Z0-9_\-\.]/', '', $file['vendor_data']['document_file']['name']);
                if (substr($newName, 0, 1) == '.') {
                    $newName = 'document_' . $newName;
                }
                $i = 0;
                while (@file_exists($path . $newName)) {
                    $newName = ++$i . '_' . $newName;
                }
                try {
                    if ($documentName) {
                        /** @var $uploader Uploader */
                        $uploader = $this->_fileUploaderFactory->create(['fileId' => 'document_file']);
                        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                        $uploader->setAllowRenameFiles(true);
                        $uploader->save($path, $newName);
                    }
                } catch (Exception $e) {
                    $this->_logger->critical($e);
                }
                $vendorData['document_file']['value'] = $newName;
                $vendor->addData($vendorData);

                $this->_eventManager->dispatch(
                    'vendor_account_prepare_save',
                    ['vendor' => $vendor, 'request' => $request]
                );

                // Save vendor
                $vendor->save();

                // After save
                $this->_eventManager->dispatch(
                    'vendor_account_save_after',
                    ['vendor' => $vendor, 'request' => $request]
                );
                // Done Saving customer, finish save action
                $this->_coreRegistry->register('current_vendor_id', $vendor->getId());
                $this->messageManager->addSuccess(__('You account information is saved.'));

                $this->_redirect('account');
            } catch (\Magento\Eav\Model\Entity\Attribute\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            } catch (Exception $e) {
                $this->messageManager->addError(
                    __('Something went wrong while saving the seller data. Please review the error log.')
                );
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            }
        }
        $this->_redirect('account');
    }
}
