<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Controller\Product;

use Magento\Framework\App\Action\Context;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Vnecoms\VendorsProduct\Helper\Data as ProductHelper;
use Magento\Catalog\Model\Config;

/**
 * Class Validate
 * @package Tigren\ProductReviewRestrict\Controller\Review
 */
class Profile extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Vnecoms\Vendors\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var \Vnecoms\VendorsReview\Model\ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var \Vnecoms\VendorsSales\Model\ResourceModel\OrderFactory
     */
    protected $_orderResourceFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected $_productCollection;

    /**
     * Catalog product visibility
     *
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $productVisibility;

    /**
     * @var \Vnecoms\VendorsProduct\Helper\Data
     */
    protected $_productHelper;

    /**
     * @var \Magento\Catalog\Model\Config
     */
    protected $catalogConfig;

    protected $_priceHelper;

    /**
     * Profile constructor.
     * @param Context $context
     * @param \Vnecoms\Vendors\Model\VendorFactory $vendorFactory
     * @param \Vnecoms\VendorsReview\Model\ReviewFactory $reviewFactory
     * @param \Vnecoms\VendorsSales\Model\ResourceModel\OrderFactory $orderFactory
     */
    public function __construct(
        Context $context,
        CollectionFactory $productCollectionFactory,
        \Vnecoms\Vendors\Model\VendorFactory $vendorFactory,
        Visibility $productVisibility,
        ProductHelper $productHelper,
        \Vnecoms\VendorsReview\Model\ReviewFactory $reviewFactory,
        Config $catalogConfig,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Vnecoms\VendorsSales\Model\ResourceModel\OrderFactory $orderFactory
    ) {
        $this->_priceHelper = $priceHelper;
        $this->catalogConfig = $catalogConfig;
        $this->productVisibility = $productVisibility;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_orderResourceFactory = $orderFactory;
        $this->_reviewFactory = $reviewFactory;
        $this->_productHelper = $productHelper;
        $this->_vendorFactory = $vendorFactory;
        parent::__construct(
            $context
        );
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $vendorId = $this->getRequest()->getParam('vendor_id');
        $productId = $this->getRequest()->getParam('product_id');
        $productPrice = $this->getRequest()->getParam('product_price');
        $result = [
            'sales_count' => 0,
            'review_count' => 0,
            'rating' => 0,
            'more_offer' => 0,
            'min_price' => $this->_priceHelper->currency($productPrice, true, false),
            'show_more_offer' => 0
        ];
        if ($vendorId) {
            $vendor = $this->_vendorFactory->create()->load($vendorId);
            if ($vendor->getId()) {
                /** @var \Vnecoms\VendorsReview\Model\ResourceModel\Review $reviewResource */
                $reviewResource = $this->_reviewFactory->create()->getResource();
                $result['review_count'] = $reviewResource->getReviewCount($vendor->getId());
                $resource = $this->_orderResourceFactory->create();
                $result['sales_count'] = $resource->getSalesCount($vendor->getId());
                $rating = $reviewResource->getAverageRating($vendor->getId());
                $result['rating'] = round($rating * 100 / 5);
                $products = $this->getProductCollection($productId);
                $result['more_offer'] = $products->count();
                if ($products->count() > 0) {
                    $result['show_more_offer'] = 1;
                }
                $minProduct = $products->getFirstItem();
                if ($minProduct->getFinalPrice() < $productPrice) {
                    $result['min_price'] = $this->_priceHelper->currency($minProduct->getFinalPrice(), true, false);
                }
            }
        }
        return $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
        );
    }

    /**
     * @param $productId
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getProductCollection($productId)
    {
        if ($this->_productCollection === null) {
            $this->_productCollection = $this->_productCollectionFactory->create();
            $this->_productCollection->addAttributeToSelect('vendor_id')->addAttributeToFilter('select_from_product_id',
                $productId)
                ->addAttributeToFilter('approval', ['in' => $this->_productHelper->getAllowedApprovalStatus()]);

            $this->_productCollection->addAttributeToSelect($this->catalogConfig->getProductAttributes())
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->setVisibility($this->productVisibility->getVisibleInCatalogIds())
                ->addAttributeToSort('price', 'ASC');
        }


        return $this->_productCollection;
    }
}
