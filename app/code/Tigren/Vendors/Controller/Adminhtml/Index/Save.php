<?php
/**
 * *
 *  * @author Tigren Solutions <info@tigren.com>
 *  * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 *  * @license Open Software License ("OSL") v. 3.0
 *
 */

namespace Tigren\Vendors\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Filesystem;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Psr\Log\LoggerInterface;
use Tigren\Vendors\Helper\SendMail;
use Vnecoms\Vendors\Model\VendorFactory;

class Save extends \Vnecoms\Vendors\Controller\Adminhtml\Index\Save
{

    /**
     * Class Save
     */

    /**
     * @var VendorFactory
     */
    protected $_vendorFactory;
    /**
     * @var SendMail
     */
    protected $_sendmail;
    /**
     * File system
     *
     * @var Filesystem
     */
    protected $_fileSystem;
    /**
     * File Uploader factory
     *
     * @var UploaderFactory
     */
    protected $_fileUploaderFactory;
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * Save constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param Date $dateFilter
     * @param VendorFactory $vendorFactory
     * @param SendMail $sendmail
     * @param Filesystem $fileSystem
     * @param UploaderFactory $fileUploaderFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        Date $dateFilter,
        VendorFactory $vendorFactory,
        SendMail $sendmail,
        LoggerInterface $logger
    )
    {
        $this->_sendmail = $sendmail;
        $this->_logger = $logger;
        parent::__construct($context, $coreRegistry, $dateFilter, $vendorFactory);
    }

    /**
     * @return void
     */
    public function execute()
    {
        if ($this->getRequest()->getPostValue()) {
            try {
                // optional fields might be set in request for future processing by observers in other modules
                $vendorData = $this->getRequest()->getParam('vendor_data');
                $vendorId = $this->getRequest()->getParam('id');
                $request = $this->getRequest();
                $isExistingVendor = (bool)$vendorId;
                $vendor = $this->vendorFactory->create();
                if ($isExistingVendor) {
                    $vendor->load($vendorId);
                    $currentStatus = $vendor->getData('status');
                }
                $isDeleteVendorDocument = 0;
                $document = $vendorData['document_file'];
                if (isset($document['delete'])) {
                    $isDeleteVendorDocument = $document['delete'];
                }
                $vendor->addData($vendorData);
                $vendor->setData('file_delete', $isDeleteVendorDocument);
                $this->_eventManager->dispatch(
                    'adminhtml_vendor_prepare_save',
                    ['vendor' => $vendor, 'request' => $request]
                );

                // Save vendor
                $vendor->save();
                $newStatus = $vendor->getData('status');
                if (($newStatus != $currentStatus) && ($newStatus == 2)) {
                    try {
                        $this->_sendmail->sendApprovedEmail($vendor->getData('vendor_id'), $vendor->getData('email'));
                    } catch (MailException $e) {
                        $this->_logger->critical($e);
                    } catch (LocalizedException $e) {
                        $this->_logger->critical($e);
                    }
                }
                if (($newStatus != $currentStatus) && ($newStatus == 3)) {
                    try {
                        $this->_sendmail->sendRejectEmail($vendor->getData('name'), $vendor->getData('email'));
                    } catch (MailException $e) {
                        $this->_logger->critical($e);
                    } catch (LocalizedException $e) {
                        $this->_logger->critical($e);
                    }
                }

                // After save
                $this->_eventManager->dispatch(
                    'adminhtml_vendor_save_after',
                    ['vendor' => $vendor, 'request' => $request]
                );
                // Done Saving customer, finish save action
                $this->_coreRegistry->register('current_vendor_id', $vendorId);
                $this->messageManager->addSuccess(__('You saved the vendor.'));
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('vendors/*/edit', ['id' => $vendorId]);
                    return;
                }
                $this->_redirect('*/*/*');

                return;
            } catch (\Magento\Eav\Model\Entity\Attribute\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_logger->critical($e);
                $this->_redirect('vendors/*/edit', ['id' => $this->getRequest()->getParam('id')]);
                return;
            } catch (Exception $e) {
                $this->messageManager->addError(
                    __('Something went wrong while saving the seller data. Please review the error log.')
                );
                $this->_logger->critical($e);
                $this->_redirect('vendors/*/edit', ['id' => $this->getRequest()->getParam('id')]);
                return;
            }
        }
        $this->_redirect('vendors/*/');
    }
}
