<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Block\Product;

/**
 * Class Profile
 * @package Tigren\Vendors\Block\Product
 */
class Profile extends \Vnecoms\Vendors\Block\Profile
{
    /**
     * @var \Vnecoms\VendorsPage\Helper\Data
     */
    protected $_pageHelper;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;

    /**
     * Profile constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Vnecoms\Vendors\Helper\Data $vendorHelper
     * @param \Vnecoms\VendorsConfig\Helper\Data $configHelper
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageDatabase
     * @param \Vnecoms\Vendors\Helper\Image $imageHelper
     * @param \Vnecoms\VendorsSales\Model\ResourceModel\OrderFactory $orderResourceFactory
     * @param \Magento\Cms\Model\Template\Filter $filter
     * @param \Vnecoms\Vendors\Model\VendorFactory $vendorFactory
     * @param \Vnecoms\VendorsPage\Helper\Data $pageHelper
     * @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Vnecoms\Vendors\Helper\Data $vendorHelper,
        \Vnecoms\VendorsConfig\Helper\Data $configHelper,
        \Magento\Framework\Registry $registry,
        \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageDatabase,
        \Vnecoms\Vendors\Helper\Image $imageHelper,
        \Vnecoms\VendorsSales\Model\ResourceModel\OrderFactory $orderResourceFactory,
        \Magento\Cms\Model\Template\Filter $filter,
        \Vnecoms\Vendors\Model\VendorFactory $vendorFactory,
        \Vnecoms\VendorsPage\Helper\Data $pageHelper,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        array $data = []
    ) {
        $this->_pageHelper = $pageHelper;
        $this->jsLayout = isset($data['jsLayout']) ? $data['jsLayout'] : [];
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
        parent::__construct($context, $vendorHelper, $configHelper, $registry, $fileStorageDatabase, $imageHelper,
            $orderResourceFactory, $filter, $vendorFactory, $data);
    }

    /**
     * @return bool|false|string
     */
    public function getJsLayout()
    {
        $product = $this->_coreRegistry->registry('current_product');
        if (isset($this->jsLayout['components'])) {
            $this->jsLayout['components']['product-vendor-profile']['provider'] = 'profileProvider';
            $provider = [
                'component' => 'uiComponent',
                'vendor_id' => $this->getVendor()->getId(),
                'product_id' => $product->getId(),
                'product_price' => $product->getFinalPrice()
            ];
            $this->jsLayout['components']['profileProvider'] = $provider;
        }

        return $this->serializer->serialize($this->jsLayout);
    }

    /**
     * Is home page
     *
     * @return boolean
     */
    public function isHomePage()
    {
        return $this->_coreRegistry->registry('is_home_page');
    }

    /**
     * Get Review Link
     *
     * @return string
     */
    public function getReviewLink()
    {
        if ($this->isHomePage()) {
            return '#vendor-reviews';
        }

        return $this->_pageHelper->getUrl($this->getVendor(), 'reviews');
    }
}
