<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Block\Vendors\Widget\Form\Element;

use Magento\Framework\Data\Form\Element\CollectionFactory;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\Escaper;
use Magento\Framework\Filesystem;
use Magento\Framework\Url\EncoderInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Store\Model\StoreManagerInterface;
use Vnecoms\Vendors\Helper\Data;

/**
 * Class File
 * @package Tigren\Vendors\Block\Vendors\Widget\Form\Element
 */
class File extends \Vnecoms\Vendors\Block\Vendors\Widget\Form\Element\File
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * File constructor.
     * @param Factory $factoryElement
     * @param CollectionFactory $factoryCollection
     * @param Escaper $escaper
     * @param \Magento\Backend\Helper\Data $adminhtmlData
     * @param Repository $assetRepo
     * @param EncoderInterface $urlEncoder
     * @param Data $vendorUrl
     * @param StoreManagerInterface $storeManager
     * @param Filesystem $filesystem
     * @param array $data
     */
    public function __construct(
        Factory $factoryElement,
        CollectionFactory $factoryCollection,
        Escaper $escaper,
        \Magento\Backend\Helper\Data $adminhtmlData,
        Repository $assetRepo,
        EncoderInterface $urlEncoder,
        Data $vendorUrl,
        StoreManagerInterface $storeManager,
        Filesystem $filesystem,
        $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->filesystem = $filesystem;
        parent::__construct($factoryElement, $factoryCollection, $escaper, $adminhtmlData, $assetRepo, $urlEncoder, $vendorUrl, $data);
    }

    /**
     * Return Preview/Download URL
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _getPreviewUrl()
    {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        $file = $baseUrl . "pub/media/vendors/document/" . $this->getValue();
        return $file;
    }
}
