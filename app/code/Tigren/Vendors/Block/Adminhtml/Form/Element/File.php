<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Vendors\Block\Adminhtml\Form\Element;

use Magento\Backend\Helper\Data;
use Magento\Framework\Data\Form\Element\CollectionFactory;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\Escaper;
use Magento\Framework\Url\EncoderInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class File
 * @package Tigren\Vendors\Block\Adminhtml\Form\Element
 */
class File extends \Vnecoms\Vendors\Block\Adminhtml\Form\Element\File
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * File constructor.
     * @param Factory $factoryElement
     * @param CollectionFactory $factoryCollection
     * @param Escaper $escaper
     * @param Data $adminhtmlData
     * @param Repository $assetRepo
     * @param EncoderInterface $urlEncoder
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Factory $factoryElement,
        CollectionFactory $factoryCollection,
        Escaper $escaper,
        Data $adminhtmlData,
        Repository $assetRepo,
        EncoderInterface $urlEncoder,
        StoreManagerInterface $storeManager,
        $data = []
    ) {
        $this->storeManager = $storeManager;
        parent::__construct($factoryElement, $factoryCollection, $escaper, $adminhtmlData, $assetRepo, $urlEncoder, $data);
    }

    /**
     * Return Preview/Download URL
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _getPreviewUrl()
    {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        $file = $baseUrl . "pub/media/vendors/document/" . $this->getValue();
        return $file;
    }
}
