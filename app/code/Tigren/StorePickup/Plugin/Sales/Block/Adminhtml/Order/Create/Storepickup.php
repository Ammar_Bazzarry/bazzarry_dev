<?php
/**
 * Tigren
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category   Adminhtml Orders storepickup store selector
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Plugin\Sales\Block\Adminhtml\Order\Create;

use Magento\Sales\Block\Adminhtml\Order\Create\Shipping\Method\Form;

/**
 * Class Storepickup
 * @package Tigren\StorePickup\Plugin\Sales\Block\Adminhtml\Order\Create
 */
class Storepickup
{
    /**
     *
     * @param Form $subject
     * @param Form $result
     * @return Form
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterToHtml(
        Form $subject,
        $result
    ) {
        if ($subject->getPickupstoreId()) {
            $orderAttributesForm = $subject->getLayout()->createBlock(
                'Tigren\StorePickup\Block\Adminhtml\Order\Create\Storepickup'
            );
            $orderAttributesForm->setTemplate('Tigren_StorePickup::store_list.phtml');
            $orderAttributesForm->setStore($subject->getStore());
            $orderAttributesFormHtml = $orderAttributesForm->toHtml();
            return $result . $orderAttributesFormHtml;
        }
        return $result;
    }
}
