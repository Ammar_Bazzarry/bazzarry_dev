<?php
/**
 * Tigren
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category   Adminhtml Orders storepickup store selector
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Plugin\Sales\Block\Adminhtml\Order\View;

/**
 * Class Info
 * @package Tigren\StorePickup\Plugin\Sales\Block\Adminhtml\Order\View
 */
class Info
{
    /**
     * Change the template for renaming shipping address to storepickup address
     *
     * @param \Temando\Shipping\Block\Adminhtml\Sales\Order\View\Info $subject
     */
    public function beforeToHtml(\Temando\Shipping\Block\Adminhtml\Sales\Order\View\Info $subject)
    {
        if ($subject->getTemplate() === 'Temando_Shipping::sales/order/view/info.phtml') {
            $subject->setTemplate('Tigren_StorePickup::sales/order/view/info.phtml');
        }
    }
}
