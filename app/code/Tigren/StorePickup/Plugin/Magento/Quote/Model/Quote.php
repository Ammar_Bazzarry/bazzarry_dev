<?php
/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Plugin\Magento\Quote\Model;

use Magento\Quote\Api\Data\AddressExtensionInterfaceFactory;

/**
 * Class Quote
 * @package Tigren\StorePickup\Plugin\Magento\Quote\Model
 */
class Quote
{
    /**
     * @var AddressExtensionInterfaceFactory
     */
    protected $addressExtensionInterfaceFactory;

    /**
     * Quote constructor.
     * @param AddressExtensionInterfaceFactory $addressExtensionInterfaceFactory
     */
    public function __construct(
        AddressExtensionInterfaceFactory $addressExtensionInterfaceFactory
    ) {
        $this->addressExtensionInterfaceFactory = $addressExtensionInterfaceFactory;
    }

    /**
     * @param \Magento\Quote\Model\Quote $subject
     * @param \Closure $proceed
     * @return mixed
     */
    public function aroundGetShippingAddress(
        \Magento\Quote\Model\Quote $subject,
        \Closure $proceed
    ) {
        /** @var \Magento\Quote\Api\Data\AddressInterface $shippingAddress */
        $shippingAddress = $proceed();
        $extAttributes = $shippingAddress->getExtensionAttributes();

        if (!$extAttributes) {
            $extAttributes = $this->addressExtensionInterfaceFactory->create();
        }

        if ($extAttributes instanceof \Magento\Quote\Api\Data\AddressExtension) {
            $extAttributes->setPickupstoreId($subject->getPickupstoreId());
            $shippingAddress->setExtensionAttributes($extAttributes);
        }

        return $shippingAddress;
    }
}