<?php
/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Plugin\Backend\Block;

/**
 * Class Menu
 * @package Tigren\StorePickup\Plugin\Backend\Block
 */
class Menu
{
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $_authSession;

    /**
     * Menu plugin constructor
     * @param \Magento\Backend\Model\Auth\Session $authSession
     */
    public function __construct(
        \Magento\Backend\Model\Auth\Session $authSession
    ) {
        $this->_authSession = $authSession;
    }

    /**
     * Plugin to remove my order node if admin user
     * @param \Magento\Backend\Block\Menu $subject
     * @param $menu
     * @param int $level
     * @param int $limit
     * @param array $colBrakes
     * @return array
     */
    public function beforeRenderNavigation(
        \Magento\Backend\Block\Menu $subject,
        $menu,
        $level = 0,
        $limit = 0,
        $colBrakes = []
    ) {
        $storeRole = \Tigren\StorePickup\Helper\Data::STORE_ROLE;
        $storeOrderAcl = \Tigren\StorePickup\Helper\Data::STORE_ORDER_ACL;
        $storeProfileAcl = \Tigren\StorePickup\Helper\Data::STORE_PROFILE_ACL;

        $userRole = $this->_authSession->getUser()->getRole()->getRoleName();

        foreach ($menu as $key => $menuItem) {
            if ($userRole != $storeRole && $menuItem->getId() == $storeOrderAcl) {
                unset($menu[$key]);
            }
        }

        foreach ($menu as $key => $menuItem) {
            if ($userRole != $storeRole && $menuItem->getId() == $storeProfileAcl) {
                unset($menu[$key]);
            }
        }
        return [$menu, $level, $limit, $colBrakes];
    }
}