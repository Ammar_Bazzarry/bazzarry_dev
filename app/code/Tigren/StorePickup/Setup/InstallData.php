<?php
/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category   Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Setup;

use Magento\Authorization\Model\Acl\Role\Group;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Setup\Module\Setup;

/**
 * class InstallData
 * @package Tigren\Marketplace\Setup
 */
class InstallData implements InstallDataInterface
{
    /*
     *
     * Data keys
     */
    const KEY_USER = 'tigren_stores';

    /**
     * Setup
     *
     * @var Setup
     */
    private $setup;

    /**
     * @var \Magento\Authorization\Model\RulesFactory
     */
    protected $_rulesFactory;

    /**
     *
     * @param Setup $setup
     * @param \Magento\Authorization\Model\RulesFactory $rulesFactory
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        Setup $setup,
        \Magento\Authorization\Model\RulesFactory $rulesFactory,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->setup = $setup;
        $this->_rulesFactory = $rulesFactory;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        /**
         * Admin 'store' role creation
         */
        $adminRoleData = [
            'tree_level' => 1,
            'role_type' => Group::ROLE_TYPE,
            'user_type' => UserContextInterface::USER_TYPE_ADMIN,
            'role_name' => self::KEY_USER,
        ];
        $this->setup->getConnection()->insert($this->setup->getTable('authorization_role'), $adminRoleData);

        $roleId = $this->setup->getConnection()->lastInsertId();

        if ($roleId) {
            $resource = "Magento_Backend::myaccount,"
                . "Tigren_StorePickup::add_row,"
                . "Tigren_StorePickup::orders";
            $resources = explode(',', $resource);

            $this->_rulesFactory->create()->setRoleId($roleId)->setResources($resources)->saveRel();
        }
        $setup->endSetup();
    }
}
