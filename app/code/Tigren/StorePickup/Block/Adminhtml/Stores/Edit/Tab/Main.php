<?php

/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Block\Adminhtml\Stores\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Cms\Model\Wysiwyg\Config;
use Magento\Directory\Model\Config\Source\Country;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Tigren\StorePickup\Model\Status;

/**
 * Class Main
 * @package Tigren\StorePickup\Block\Adminhtml\Stores\Edit\Tab
 */
class Main extends Generic implements TabInterface
{
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Tigren\StorePickup\Model\Status
     */
    protected $_storeStatus;

    /**
     *
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $_country;

    /**
     *
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Config $wysiwygConfig
     * @param Status $storeStatu
     * @param Country $country
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        Status $storeStatu,
        Country $country,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_storeStatus = $storeStatu;
        $this->_country = $country;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Store Profile');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Store Profile');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepare form fields
     *
     * @return \Magento\Backend\Block\Widget\Form
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        /** @var $model \Tigren\StorePickup\Model\Stores */
        $model = $this->_coreRegistry->registry('row_data');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('stores_');
        $form->setFieldNameSuffix('store_form');

        $fieldset = $form->addFieldset(
            'base_fieldset', ['legend' => __('Basic Details')]
        );

        if ($model->getStoreId()) {
            $fieldset->addField(
                'store_id', 'hidden', ['name' => 'store_id']
            );
        }
        $fieldset->addField(
            'store_name', 'text', [
                'name' => 'store_name',
                'label' => __('Store Name'),
                'required' => true
            ]
        );
        $optionsc = $this->_country->toOptionArray();
        $country = $fieldset->addField(
            'country_id', 'select', [
                'name' => 'country_id',
                'label' => __('Country'),
                'title' => __('Country'),
                'values' => $optionsc,
                'required' => true
            ]
        );

        $state = $fieldset->addField(
            'region_dropdown', 'select', [
                'name' => 'region_dropdown',
                'label' => __('Region'),
                'title' => __('Region'),
                'values' => ['--Please Select Country--'],
                'required' => false
            ]
        );

        $fieldset->addField(
            'region_text_box', 'text', [
                'name' => 'city',
                'label' => __('Region'),
                'required' => false
            ]
        );

        $fieldset->addField(
            'region_id', 'hidden', ['name' => 'region_id']
        );

        $fieldset->addField(
            'region', 'hidden', ['name' => 'region']
        );

        $fieldset->addField(
            'street', 'textarea', [
                'name' => 'street',
                'label' => __('Address'),
                'required' => true,
                'style' => 'height: 0em; width: 25em;'
            ]
        );

        $fieldset->addField(
            'city', 'text', [
                'name' => 'city',
                'label' => __('City'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'postcode', 'text', [
                'name' => 'postcode',
                'label' => __('Postcode'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'firstname', 'text', [
                'name' => 'firstname',
                'label' => __('Contact Person First Name'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'lastname', 'text', [
                'name' => 'lastname',
                'label' => __('Contact Person Last Name'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'telephone', 'text', [
                'name' => 'telephone',
                'label' => __('Telephone'),
                'required' => true,
                'class' => 'validate-number'
            ]
        );

        $fieldset->addField(
            'store_start_time', 'time', [
                'name' => 'store_start_time',
                'label' => __('Store Start Time'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'store_close_time',
            'time', [
                'name' => 'store_close_time',
                'label' => __('Store Close Time'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'status', 'select', [
                'label' => __('Status'),
                'name' => 'status',
                'title' => __('Status'),
                'required' => true,
                'options' => ['1' => __('Enable'), '0' => __('Disable')]
            ]
        );

        $fieldset->addField(
            'pickup_interval', 'select', [
                'label' => __('Pickup Time Interval'),
                'name' => 'pickup_interval',
                'title' => __('Pickup Time Interval'),
                'required' => true,
                'values' => $this->getPickupOptionArray()
            ]
        );

        /*
         * Add Ajax to the Country select box html output
         */
        $country->setAfterElementHtml("
            <script type=\"text/javascript\">
                    require([
                    'jquery',
                    'mage/template',
                    'jquery/ui',
                    'mage/translate'
                ],
                function($, mageTemplate) {
                   $('#edit_form').on('change', '#stores_country_id', function(event){
                        $.ajax({
                               url : '" . $this->getUrl('storepickup/*/regionlist') . "region/'+  $('#stores_region_id').val()+'/country/' +  $('#stores_country_id').val(),
                                type: 'get',
                                dataType: 'json',
                               showLoader:true,
                               success: function(data){
                                    if (data.html_content == 0) {
                                        $('.field-region_dropdown').hide();
                                        var regionVal = $('#stores_region').val();
                                        $('#stores_region_text_box').val(regionVal);
                                        $('.field-region_text_box').addClass('required-entry _required');
                                        $('#stores_region_text_box').addClass('required-entry _required');
                                        $('.field-region_dropdown').removeClass('required-entry _required');
                                        $('#stores_region_dropdown').removeClass('required-entry _required');
                                        $('.field-region_text_box').show();
                                    } else {
                                        $('#stores_region_dropdown').empty();
                                        $('#stores_region_dropdown').append(data.html_content);
                                        $('.field-region_dropdown').show();
                                        $('.field-region_text_box').hide();
                                        $('.field-region_text_box').removeClass('required-entry _required');
                                        $('#stores_region_text_box').removeClass('required-entry _required');
                                        $('.field-region_dropdown').addClass('required-entry _required');
                                        $('#stores_region_dropdown').addClass('required-entry _required');
                                    }
                               }
                            });
                   })
                   
                   $('#edit_form').on('change', '#stores_region_dropdown', function(event){
                        var region = $('#stores_region_dropdown option:selected').text();
                        var region_id = $('#stores_region_dropdown').val();
                        $('#stores_region_id').val(region_id);
                        $('#stores_region').val(region);
                    });
                    
                    $('#edit_form').on('change', '#stores_region_text_box', function(event){
                        var region = $('#stores_region_text_box').val();
                        $('#stores_region').val(region);
                    });
                    
                    $(document).ready(function(){
                        $('.field-region_text_box').hide();
                        var interval = 0;
                        setInterval(function(){
                            var stores_store_id = $('#stores_store_id').val();
                            if(interval == 0 && stores_store_id > 0) { 
                                var countryVal = $('#stores_country_id').val();
                                if (countryVal != '') {
                                    interval++;
                                    $('#stores_country_id').trigger('change');
                                }
                            }
                        }, 1000);
                    });
                }
            );
            </script>"
        );

        $data = $model->getData();
        $form->setValues($data);
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare select options for pickup dropdown
     * @return array
     */
    public function getPickupOptionArray()
    {
        return [
            [
                'label' => __('------- Please choose option -------'),
                'value' => '',
            ],
            ['value' => '1', 'label' => __('1 Hour')],
            ['value' => '2', 'label' => __('2 Hour')],
            ['value' => '3', 'label' => __('3 Hour')],
            ['value' => '4', 'label' => __('4 Hour')],
            ['value' => '5', 'label' => __('5 Hour')],
            ['value' => '6', 'label' => __('6 Hour')],
            ['value' => '7', 'label' => __('7 Hour')],
            ['value' => '8', 'label' => __('8 Hour')],
            ['value' => '9', 'label' => __('9 Hour')],
            ['value' => '10', 'label' => __('10 Hour')],
            ['value' => '11', 'label' => __('11 Hour')],
            ['value' => '12', 'label' => __('12 Hour')],
        ];
    }
}
