<?php

/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Block\Adminhtml\Order;

use Magento\Backend\Block\Template;

/**
 * Class Details
 * @package Tigren\StorePickup\Block\Adminhtml\Order
 */
class Details extends Template
{
    /**
     * Constant for pending order status
     */
    const ORDER_PENDING_STATUS = 'pending';

    /**
     * Constant for processing order status
     */
    const ORDER_PROCESSING_STATUS = 'processing';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Details constructor
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        array $data = array()
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    /**
     * Return buttom url
     * @return string
     */
    public function getMarkAsDeliverUrl()
    {
        $orderId = $this->_coreRegistry->registry('store_view_order_id');
        return $this->getUrl('storepickup/stores/processOrder/order_id/' . $orderId);
    }

    /**
     * Check order status to maintain button visibility
     * @return boolean
     */
    public function isAvailableForCompletion()
    {
        $orderStatus = $this->getOrderStatus();
        $allowedStatus = [
            self::ORDER_PENDING_STATUS,
            self::ORDER_PROCESSING_STATUS
        ];
        if (in_array($orderStatus, $allowedStatus)) {
            return true;
        }
        return false;
    }

    /**
     * Return order status
     *
     * @return string
     */
    public function getOrderStatus()
    {
        return $this->getOrder()->getStatus();
    }

    /**
     * Return order
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('order');
    }

    /**
     * Return receiver name for delivery
     * @return string
     */
    public function getReceiverName()
    {
        $firstName = $this->getOrder()->getShippingAddress()->getFirstname();
        $lastName = $this->getOrder()->getShippingAddress()->getLastname();
        return $firstName . ' ' . $lastName;
    }

    /**
     * Return buttom url
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('storepickup/stores/orders');
    }
}
