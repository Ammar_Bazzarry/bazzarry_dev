<?php

/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Model\ResourceModel;

/**
 * Class Products
 * @package Tigren\StorePickup\Model\ResourceModel
 */
class Products extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Map stores product
     * @param $fileContents
     * @return array
     */
    public function mapStoresProduct($fileContents)
    {
        $errors = [];
        if (!empty($fileContents)) {
            $columns = [
                'store_id',
                'product_id',
                'sku'
            ];
            foreach ($fileContents as $key => $fileContent) {
                try {
                    $this->getConnection()->insertArray('tigren_stores_product_map', $columns, [$fileContent]);
                } catch (\Exception $e) {
                    $row = $key + 1;
                    $errors[] = 'Duplicate entry for row : ' . $row . '-------Sku : ' . $fileContent[2];
                    continue;
                }
            }
        }
        return $errors;
    }

    /**
     * Delete unmapped data from table
     * @param $productIds
     * @param $storeId
     * @return array
     */
    public function deleteUnmappedProduct($productIds, $storeId)
    {
        $errors = [];
        if (!empty($productIds)) {
            $this->getConnection()->delete(
                'tigren_stores_product_map',
                'product_id IN (' . implode(',', $productIds) . ') AND store_id = ' . $storeId
            );
        }
        return $errors;
    }

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('tigren_stores_product_map', 'id');
    }
}
