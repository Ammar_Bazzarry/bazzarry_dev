<?php
/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class StorePickupConfigProvider
 */
class StorePickupConfigProvider implements ConfigProviderInterface
{
    /**
     * const for google map api key
     */
    const XML_PATH_GOOGLE_URL_API = 'stores_section/storepickup_map/api_key';

    /**
     * const for google map zoom
     */
    const XML_PATH_GOOGLE_MAP_ZOOM = 'stores_section/storepickup_map/zoom';

    /**
     * const for google map radius
     */
    const XML_PATH_GOOGLE_MAP_REDIUS = 'stores_section/storepickup_map/radius';

    /**
     * const for google map radius
     */
    const XML_PATH_GOOGLE_MAP_LATITUDE = 'stores_section/storepickup_map/latitude';

    /**
     * const for google map radius
     */
    const XML_PATH_GOOGLE_MAP_LONGITUDE = 'stores_section/storepickup_map/longitude';

    /**
     * const for google map store zoom level
     */
    const XML_PATH_GOOGLE_MAP_ZOOM_INDIVIDUAL = 'stores_section/storepickup_individual/zoom_individual';

    /**
     * Error message for address
     */
    const XML_ERROE_MESSAGE_FOR_ADDRESS = 'stores_section/storepickup_error_message/method_not_available_for_address';

    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Tigren\StorePickup\Helper\Data
     */
    protected $storePickupHelper;

    /**
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param \Tigren\StorePickup\Helper\Data $storePickupHelper
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Tigren\StorePickup\Helper\Data $storePickupHelper
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storePickupHelper = $storePickupHelper;
    }

    /**
     * {@inheritdoc}
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getConfig()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $config = [
            'storepickup' => [
                'google' => [
                    'apiUrl' => $this->scopeConfig->getValue(self::XML_PATH_GOOGLE_URL_API, $storeScope),
                ],
                'center' => [
                    'lat' => $this->scopeConfig->getValue(self::XML_PATH_GOOGLE_MAP_LATITUDE, $storeScope),
                    'lng' => $this->scopeConfig->getValue(self::XML_PATH_GOOGLE_MAP_LONGITUDE, $storeScope),
                ],
                'radius' => $this->scopeConfig->getValue(self::XML_PATH_GOOGLE_MAP_REDIUS, $storeScope),
                'zoom' => $this->scopeConfig->getValue(self::XML_PATH_GOOGLE_MAP_ZOOM, $storeScope),
                'marker_zoom' => $this->scopeConfig->getValue(self::XML_PATH_GOOGLE_MAP_ZOOM_INDIVIDUAL, $storeScope),
                'addressError' => $this->scopeConfig->getValue(self::XML_ERROE_MESSAGE_FOR_ADDRESS, $storeScope),
            ],
            'pickupStores' => $this->storePickupHelper->getStoresData(),
            'directoryRegions' => $this->storePickupHelper->getRegionsData()
        ];
        return $config;
    }
}