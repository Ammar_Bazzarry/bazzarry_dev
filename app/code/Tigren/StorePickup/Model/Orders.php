<?php
/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Model;

use Tigren\StorePickup\Api\Data\OrderGridInterface;

/**
 * Class Orders
 * @package Tigren\StorePickup\Model
 */
class Orders extends \Magento\Framework\Model\AbstractModel implements OrderGridInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'tigren_stores_orders';

    /**
     * @var string
     */
    protected $_cacheTag = 'tigren_stores_orders';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'tigren_stores_orders';

    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId.
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get IncrementId.
     *
     * @return string
     */
    public function getIncrementId()
    {
        return $this->getData(self::INCREMENT_ID);
    }

    /**
     * Set IncrementId.
     */
    public function setIncrementId($incrementId)
    {
        return $this->setData(self::INCREMENT_ID, $incrementId);
    }

    /**
     * Get CreatedAt.
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get BillingName.
     *
     * @return string
     */
    public function getBillingName()
    {
        return $this->getData(self::BILLING_NAME);
    }

    /**
     * Set BillingName.
     */
    public function setBillingName($billingName)
    {
        return $this->setData(self::BILLING_NAME, $billingName);
    }

    /**
     * Get ShippingName.
     *
     * @return string
     */
    public function getShippingName()
    {
        return $this->getData(self::SHIPPING_NAME);
    }

    /**
     * Set ShippingName.
     */
    public function setShippingName($shippingName)
    {
        return $this->setData(self::SHIPPING_NAME, $shippingName);
    }

    /**
     * Get BaseGrandTotal.
     *
     * @return double
     */
    public function getBaseGrandTotal()
    {
        return $this->getData(self::BASE_GRAND_TOTAL);
    }

    /**
     * Set BaseGrandTotal.
     */
    public function setBaseGrandTotal($baseGrandTotal)
    {
        return $this->setData(self::BASE_GRAND_TOTAL, $baseGrandTotal);
    }

    /**
     * Get GrandTotal.
     *
     * @return double
     */
    public function getGrandTotal()
    {
        return $this->getData(self::GRAND_TOTAL);
    }

    /**
     * Set GrandTotal.
     */
    public function setGrandTotal($grandTotal)
    {
        return $this->setData(self::GRAND_TOTAL, $grandTotal);
    }

    /**
     * Get Status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Set Status.
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Tigren\StorePickup\Model\ResourceModel\Orders');
    }
}
