<?php
/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Model;

/**
 * Class User
 * @package Tigren\StorePickup\Model
 */
class User extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Tigren\StorePickup\Model\ResourceModel\User');
    }
}
