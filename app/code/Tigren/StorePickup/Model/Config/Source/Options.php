<?php

/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Model\Config\Source;

use Tigren\StorePickup\Helper\Data;

/**
 * Class Options
 * @package Tigren\StorePickup\Model\Config\Source
 */
class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @var \Tigren\StorePickup\Helper\Data
     */
    protected $helper;

    /**
     * @param Data $helper
     */
    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Return all stores
     * @return array
     */
    public function getAllOptions()
    {
        $storesProfiles = $this->helper->getAllStores();
        $storesOptions = [];
        foreach ($storesProfiles as $key => $storeData) {
            $storesOptions[$key]['label'] = $storeData->getStoreName();
            $storesOptions[$key]['value'] = $storeData->getStoreId();
        }
        return $storesOptions;
    }
}
