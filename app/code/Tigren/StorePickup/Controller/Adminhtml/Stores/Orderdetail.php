<?php

/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Controller\Adminhtml\Stores;

use Magento\Sales\Model\Order;

/**
 * Class Orderdetail
 * @package Tigren\StorePickup\Controller\Adminhtml\Stores
 */
class Orderdetail extends \Magento\Backend\App\Action
{
    /**
     * Core Registry
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    /**
     * Sales order
     *
     * @var \Magento\Sales\Model\Order
     */
    protected $salesOrder;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param Order $salesOrder
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        Order $salesOrder,
        \Magento\Framework\Registry $coreRegistry
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->resultPageFactory = $resultPageFactory;
        $this->salesOrder = $salesOrder;
    }

    /**
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $orderId = $this->getRequest()->getParam('id');
        $this->_coreRegistry->register('store_view_order_id', $orderId);

        $orderData = $this->salesOrder->load($orderId);

        $this->_coreRegistry->register('order', $orderData);
        $this->_coreRegistry->register('current_order', $orderData);
        $this->_coreRegistry->register('sales_order', $orderData);

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Tigren_StorePickup::orders');
        $resultPage->getConfig()->getTitle()->prepend(__('Orders Detail'));
        return $resultPage;
    }

    /**
     * Access rights checking
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tigren_StorePickup::orders');
    }
}
