<?php

/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Controller\Adminhtml\Stores;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;
use Magento\User\Model\UserFactory;
use Tigren\StorePickup\Helper\Data;
use Tigren\StorePickup\Model\StoresFactory;
use Tigren\StorePickup\Model\UserFactory as LocalUserFactory;

/**
 * Class AddRow
 * @package Tigren\StorePickup\Controller\Adminhtml\Stores
 */
class AddRow extends \Magento\Backend\App\Action
{
    /**
     * @var \Tigren\StorePickup\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Tigren\StorePickup\Model\StoresFactory
     */
    private $storesFactory;

    /**
     * @var \Tigren\StorePickup\Model\UserFactory
     */
    private $userFactory;

    /**
     * @var \Magento\User\Model\UserFactory
     */
    private $adminUserFactory;

    /**
     *
     * @param Context $context
     * @param Registry $coreRegistry
     * @param StoresFactory $storesFactory
     * @param LocalUserFactory $userFactory
     * @param UserFactory $adminUserFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        StoresFactory $storesFactory,
        LocalUserFactory $userFactory,
        UserFactory $adminUserFactory,
        Data $helper
    ) {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->storesFactory = $storesFactory;
        $this->userFactory = $userFactory;
        $this->adminUserFactory = $adminUserFactory;
        $this->helper = $helper;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $rowId = (int)$this->getRequest()->getParam('id');

        //Assign login store id in case of store user
        if ($rowId == 0) {
            $isStoreUser = $this->helper->isStoreLogin();
            if ($isStoreUser) {
                $rowId = $this->helper->getStoreId();
            }
        }
        //End

        $rowData = $this->storesFactory->create();
        $userData = $this->userFactory->create();
        $adminUserData = $this->adminUserFactory->create();
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        if ($rowId) {
            $rowData = $rowData->load($rowId);
            $userData = $userData->load($rowId, 'store_id');
            $userId = $userData->getUserId();
            $adminUserData = $adminUserData->load($userId);

            $rowTitle = $rowData->getstoreName();
            if (!$rowData->getStoreId()) {
                $this->messageManager->addError(__('store data no longer exist.'));
                $this->_redirect('storepickup/stores/rowdata');
                return;
            }
        }
        $this->coreRegistry->register('row_data', $rowData);
        $this->coreRegistry->register('user_data', $adminUserData);
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $title = $rowId ? __('Edit - ') . $rowTitle : __('Add Store');
        $resultPage->getConfig()->getTitle()->prepend($title);

        return $resultPage;
    }

    /**
     * Access rights checking
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tigren_StorePickup::add_row');
    }
}
