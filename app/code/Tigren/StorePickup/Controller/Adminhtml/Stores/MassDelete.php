<?php

/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Controller\Adminhtml\Stores;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;
use Tigren\StorePickup\Helper\Data;
use Tigren\StorePickup\Model\ResourceModel\Stores\CollectionFactory;
use Tigren\StorePickup\Model\StoresFactory;

/**
 * Class MassDelete
 * @package Tigren\StorePickup\Controller\Adminhtml\Stores
 */
class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * Massactions filter.
     * @var Filter
     */
    protected $_filter;

    /**
     * @var \Tigren\StorePickup\Model\ResourceModel\Stores\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Stores Model
     * @var \Tigren\StorePickup\Model\StoresFactory
     */
    protected $storesModel;

    /**
     * Store helper
     *
     * @var \Tigren\StorePickup\Helper\Data
     */
    protected $helper;

    /**
     *
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param StoresFactory $storesModel
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        StoresFactory $storesModel,
        Data $helper
    ) {

        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->storesModel = $storesModel;
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        $recordDeleted = 0;
        foreach ($collection as $record) {
            $storeId = $record->getStoreId();
            //Delete store user
            $userId = $this->helper->getUserId($storeId);
            $this->helper->deleteStoreUser($userId);
            //End

            $storesModel = $this->storesModel->create()->load($storeId);
            $storesModel->delete();
            $recordDeleted++;
        }
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $recordDeleted));

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }

    /**
     * Access rights checking
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tigren_StorePickup::row_data_delete');
    }
}
