<?php

/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Controller\Adminhtml\Stores;

/**
 * Class Regionlist
 * @package Tigren\StorePickup\Controller\Adminhtml\Stores
 */
class Regionlist extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Core Registry
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Regionlist constructor
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * @return void
     */
    public function execute()
    {
        $countryCode = $this->getRequest()->getParam('country');
        $this->_coreRegistry->register('store_profile_country_code', $countryCode);

        $regionId = $this->getRequest()->getParam('region');
        $this->_coreRegistry->register('store_profile_region_code', $regionId);

        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
