/**
 * Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 */

var config = {
    map: {
        '*': {
            'Magento_Checkout/template/billing-address.html':
                'Tigren_StorePickup/template/billing-address.html',
            'Magento_Checkout/js/model/shipping-save-processor/default':
                'Tigren_StorePickup/js/model/shipping-save-processor/default'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Tigren_StorePickup/js/view/shipping': true
            },
            'Magento_Checkout/js/view/billing-address': {
                'Tigren_StorePickup/js/view/billing-address': true
            },
            'Magento_Checkout/js/view/shipping-information': {
                'Tigren_StorePickup/js/view/shipping-information': true
            },
            'Magento_Checkout/js/model/checkout-data-resolver': {
                'Tigren_StorePickup/js/model/checkout-data-resolver': true
            },
            'Magento_Checkout/js/model/shipping-rate-processor/new-address': {
                'Tigren_StorePickup/js/model/shipping-rate-processor/new-address': true
            }
        }
    }
};