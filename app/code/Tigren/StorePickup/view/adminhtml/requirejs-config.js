/**
  * Copyright (c) 2019 Tigren Solutions
  * https://www.tigren.com
  */ 
var config = {
    map: {
        '*': {
            'storepickup' : 'Tigren_StorePickup/js/storepickup'
        }
    }
};