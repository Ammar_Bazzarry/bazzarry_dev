<?php
/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Observer\Backend;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Element\Template;
use Tigren\StorePickup\Helper\Data as StorePickupHelper;

/**
 * Class AddHtmlToOrderShippingViewObserver
 * @package Tigren\StorePickup\Observer\Backend
 */
class AddHtmlToOrderShippingViewObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\View\Element\Template
     */
    protected $additionalShippingBlock;

    /**
     * @var StorePickupHelper
     */
    protected $storePickupHelper;

    /**
     * Observer construct
     *
     * @param Template $additionalShippingBlock
     * @param StorePickupHelper $storePickupHelper
     */
    public function __construct(Template $additionalShippingBlock, StorePickupHelper $storePickupHelper)
    {
        $this->additionalShippingBlock = $additionalShippingBlock;
        $this->storePickupHelper = $storePickupHelper;
    }

    /**
     * Set the vendor details to order shipping view block
     *
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        if ($observer->getElementName() == 'order_shipping_view') {
            $orderShippingViewBlock = $observer->getLayout()->getBlock($observer->getElementName());
            $order = $orderShippingViewBlock->getOrder();
            if ($order->getPickupstoreId()) {
                $details = $this->storePickupHelper->getPickupInterval($order->getCreatedAt(), $order->getPickupstoreId());
                $this->additionalShippingBlock->setPickupFromDate($details['from']);
                $this->additionalShippingBlock->setPickupToDate($details['to']);
                $this->additionalShippingBlock->setTemplate('Tigren_StorePickup::order_info_shipping_info.phtml');
                $html = $observer->getTransport()->getOutput() . $this->additionalShippingBlock->toHtml();
                $observer->getTransport()->setOutput($html);
            }
        }
    }
}
