<?php
/**
 * Tigren
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category   Adminhtml Orders storepickup store selector
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Observer\Backend;

use Magento\Framework\Event\ObserverInterface;
use Tigren\StorePickup\Helper\Data;

/**
 * Class SellerFilterObserver
 */
class StoreFilterObserver implements ObserverInterface
{
    /**
     * @var \Tigren\StorePickup\Helper\Data
     */
    protected $_helperData;

    /**
     * @var array
     */
    protected $_actions = [
        'stores_order_list'
    ];

    /**
     * @param Data $helperData
     */
    public function __construct(
        Data $helperData
    ) {
        $this->_helperData = $helperData;
    }

    /**
     * Manange sales order according to seller
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return StoreFilterObserver
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $controller = $observer->getControllerAction();
        if ($this->_helperData->isStoreLogin()) {
            if (in_array($controller->getRequest()->getParam('namespace'), $this->_actions)) {
                $filters = $controller->getRequest()->getParam('filters');
                $filters['pickupstore_id'] = $this->_helperData->getStoreId();
                $controller->getRequest()->setParam('filters', $filters);
            }
        }
        return $this;
    }
}
