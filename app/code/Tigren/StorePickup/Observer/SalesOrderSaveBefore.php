<?php

/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category   Adminhtml Orders storepickup store selector
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Observer;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Quote\Model\Quote\AddressFactory;
use Psr\Log\LoggerInterface as Logger;
use Tigren\StorePickup\Model\StoresFactory;

/**
 * Class SalesOrderSaveBefore
 * @package Tigren\StorePickup\Observer
 */
class SalesOrderSaveBefore implements \Magento\Framework\Event\ObserverInterface
{
    /**
     *
     * @var Logger
     */
    protected $logger;

    /**
     *
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     *
     * @var StoresFactory
     */
    protected $storeProfiles;

    /**
     *
     * @var AddressFactory
     */
    protected $quoteAddressFactory;

    /**
     *
     * @param StoresFactory $storeProfiles
     * @param CheckoutSession $checkoutSession
     * @param AddressFactory $quoteAddressFactory
     * @param Logger $logger
     */
    public function __construct(
        StoresFactory $storeProfiles,
        CheckoutSession $checkoutSession,
        AddressFactory $quoteAddressFactory,
        Logger $logger
    ) {
        $this->logger = $logger;
        $this->storeProfiles = $storeProfiles;
        $this->checkoutSession = $checkoutSession;
        $this->quoteAddressFactory = $quoteAddressFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Quote\Model\Quote */
        $quote = $this->checkoutSession->getQuote();
        $pickupStoreId = $quote->getPickupstoreId();
        if ($pickupStoreId != 0 && $pickupStoreId != "") {
            //Get store details
            $storeModel = $this->storeProfiles->create()->getCollection();
            $storeModel->addFilterToMap('main_store_id', 'main_table.store_id');
            $storeModel->addFieldToFilter('main_store_id', $pickupStoreId)->getFirstItem();
            $storeDetails = $storeModel->getData();
            if (!empty($storeDetails)) {
                $storeStreet = $storeDetails[0]['street'];
                $storeCountryId = $storeDetails[0]['country_id'];
                $storeRegion = $storeDetails[0]['region'];
                $storeRegionId = $storeDetails[0]['region_id'];
                $storePostcode = $storeDetails[0]['postcode'];
                $storeCity = $storeDetails[0]['city'];
                $storeFirstname = $storeDetails[0]['firstname'];
                $storeLastname = $storeDetails[0]['lastname'];
                $storeContactNumber = $storeDetails[0]['telephone'];
                $addressData = [
                    'street' => $storeStreet,
                    'city' => $storeCity,
                    'country_id' => $storeCountryId,
                    'region' => $storeRegion,
                    'region_id' => $storeRegionId,
                    'postcode' => $storePostcode,
                    'telephone' => $storeContactNumber,
                    'firstname' => $storeFirstname,
                    'lastname' => $storeLastname
                ];

                $quoteShippingAddress = $this->quoteAddressFactory->create();
                $quoteShippingAddress->setData($addressData);
                $quote->setShippingAddress($quoteShippingAddress);
                $quote->setPickupstoreId($pickupStoreId);
                $quote->setShippingMethod($quote->getShippingAddress()->getShippingMethod());
                $quote->save();

                $order = $observer->getEvent()->getOrder();
                $order->setPickupstoreId($pickupStoreId);
                $order->getShippingAddress()->addData($addressData);
            }
        }
    }
}
