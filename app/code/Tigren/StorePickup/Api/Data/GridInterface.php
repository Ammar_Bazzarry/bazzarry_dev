<?php

/**
 * Tigren Store Pickup Plugin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is available on the World Wide Web at:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to access it on the World Wide Web, please send an email
 * To: support@tigren.com.  We will send you a copy of the source file.
 *
 * @category  Class
 * @package    Tigren_StorePickup
 * @copyright  Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tigren Solutions <support@tigren.com>
 */

namespace Tigren\StorePickup\Api\Data;

/**
 * Interface GridInterface
 * @package Tigren\StorePickup\Api\Data
 */
interface GridInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter.
     */
    const STORE_ID = 'store_id';
    const STORE_NAME = 'store_name';
    const CITY = 'city';
    const POSTCODE = 'postcode';
    const FIRSTNAME = 'firstname';
    const LASTNAME = 'lastname';
    const TELEPHONE = 'telephone';
    const IS_ACTIVE = 'is_active';
    const CREATED_AT = 'created_at';

    /**
     * Get StoreId.
     *
     * @return int
     */
    public function getStoreId();

    /**
     * Set StoreId.
     */
    public function setStoreId($storeId);

    /**
     * Get StoreName.
     *
     * @return string
     */
    public function getStoreName();

    /**
     * Set StoreName.
     */
    public function setStoreName($storeName);

    /**
     * Get City.
     *
     * @return string
     */
    public function getCity();

    /**
     * Set City.
     */
    public function setCity($city);

    /**
     * Get Postcode.
     *
     * @return string
     */
    public function getPostcode();

    /**
     * Set Postcode.
     */
    public function setPostcode($postcode);

    /**
     * Get Postcode.
     *
     * @return string
     */
    public function getFirstname();

    /**
     * Set Postcode.
     */
    public function setFirstname($firstname);

    /**
     * Get Lastname.
     *
     * @return string
     */
    public function getLastname();

    /**
     * Set Lastname.
     */
    public function setLastname($firstname);

    /**
     * Get Postcode.
     *
     * @return string
     */
    public function getTelephone();

    /**
     * Set Postcode.
     */
    public function setTelephone($telephone);

    /**
     * Get IsActive.
     *
     * @return string
     */
    public function getIsActive();

    /**
     * Set IsActive.
     */
    public function setIsActive($isActive);
}
