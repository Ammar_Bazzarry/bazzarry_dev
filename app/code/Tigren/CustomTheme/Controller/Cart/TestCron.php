<?php
namespace Tigren\CustomTheme\Controller\Cart;

class TestCron extends \Magento\Framework\App\Action\Action
{
    /**
     * @return mixed
     * @throws \Exception
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('\Vnecoms\VendorsCredit\Cron\ReleaseEscrowTrans');
        $helper->execute();
        echo 'Finished';
        die;
    }
}