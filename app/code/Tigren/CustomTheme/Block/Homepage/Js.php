<?php
namespace Tigren\CustomTheme\Block\Homepage;


class Js extends \Magento\Framework\View\Element\Template
{


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Data\Form\FormKey $formKey,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->formKey = $formKey;
    }


}