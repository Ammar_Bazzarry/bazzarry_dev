<?php
namespace Tigren\CustomTheme\Block\Homepage;

class CategoryFooter extends \Magento\Framework\View\Element\Template
{

    protected $_categoryCollectionFactory;
    protected $categoryFlatConfig;
    protected $_helper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory,
        \Magento\Catalog\Helper\Category $helper,
        \Magento\Catalog\Model\Indexer\Category\Flat\State $categoryFlatState,
        array $data = []
    )
    {
        $this->categoryFlatConfig = $categoryFlatState;
        $this->_helper = $helper;
        $this->_categoryCollectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    public function getCategoriesData(){
        $data = [];
        $categories = $this->_helper->getStoreCategories();
        foreach($categories as $key2 => $catLv2) {
            $child = [];
            if($catLv2->hasChildren()) {
                if($this->categoryFlatConfig->isFlatEnabled() && $catLv2->getUseFlatResource()){
                    $lv3List = $catLv2->getChildrenNodes();
                }
                else{
                    $lv3List = $catLv2->getChildren();
                }
                foreach($lv3List as $lv3){
                    $child[] = [
                        'name' => $lv3->getName(),
                        'path' => $this->_helper->getCategoryUrl($lv3)
                    ];
                }
            }
            $data[$catLv2->getname()] = $child;
        }
        return $data;
    }


}