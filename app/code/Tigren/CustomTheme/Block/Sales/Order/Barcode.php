<?php

namespace Tigren\CustomTheme\Block\Sales\Order;

use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\App\Filesystem\DirectoryList;

class Barcode extends \Magento\Framework\View\Element\Template
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    protected $dir;

    /**
     * Barcode constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Registry $registry,
        DirectoryList $directoryList,
        \Magento\Framework\Filesystem $filesystem,
        array $data = []
    )
    {
        $this->dir = $directoryList;
        $this->_filesystem = $filesystem;
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve current order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->coreRegistry->registry('current_order');
    }


    /**
     * @return string
     * @throws \Zend_Barcode_Exception
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function generateBarcode()
    {
        if (!file_exists($this->dir->getPath('media') . '/barcode')) {
            mkdir($this->dir->getPath('media') . '/barcode', 0775);
        }
        $order = $this->getOrder();
        $imageUrl = $path = $this->dir->getPath('media') . '/barcode/' .strtolower($order->getIncrementId()) .".png";

        if (!file_exists($imageUrl)){
            $config = new \Zend_Config([
                'barcode' => 'code39',
                'barcodeParams' => [
                    'text' => $order->getIncrementId(),
                    'drawText' => true,
                    'barHeight' => 50,
                    'barThickWidth' => 3,
                ],
                'renderer' => 'image',
                'rendererParams' => ['imageType' => 'png']
            ]);

            $barcodeResource = \Zend_Barcode::factory($config)->draw();
            ob_start();
            //header( "Content-type: image/png" );
            $path = $this->dir->getPath('media') . '/barcode/' .strtolower($order->getIncrementId()) .".png";
            //$save = $path;
            imagepng($barcodeResource, $path,0,null);
            imagedestroy($barcodeResource);
            ob_get_clean();
        }
    }

    public function getImageUrl()
    {
        try {
            $order = $this->getOrder();
            $this->generateBarcode();
            $imageUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'barcode/'.strtolower($order->getIncrementId()) .".png";
            return $imageUrl;
        } catch (FileSystemException $e) {
        } catch (\Zend_Barcode_Exception $e) {
        } catch (NoSuchEntityException $e) {
        }

    }

}
