<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomTheme\Block\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Class CustomInfo
 * @package Tigren\CustomTheme\Block\Product
 */
class CustomInfo extends \Magento\Catalog\Block\Product\View
{

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $_categoryCollectionFactory;
    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $_helper;

    /**
     * CustomInfo constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Url\EncoderInterface $urlEncoder
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Customer\Model\Session $customerSession
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory
     * @param \Magento\Catalog\Helper\Category $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Customer\Model\Session $customerSession,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory,
        \Magento\Catalog\Helper\Category $helper,
        array $data = []
    )
    {
        $this->_helper = $helper;
        $this->_categoryCollectionFactory = $collectionFactory;
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data
        );
    }

    /**
     * @return array
     */
    public function getCategoryData()
    {
        $product = $this->getProduct();
        $results = [];
        if ($product && $product->getId()) {
            $categoryIds = $product->getCategoryIds();
            if ($categoryIds){
                $categories = $this->_categoryCollectionFactory->create()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('entity_id', $categoryIds);
                foreach ($categories as $category) {
                    $results[] = [
                        'name' => $category->getName(),
                        'path' => $this->_helper->getCategoryUrl($category)
                    ];
                }
            }

        }
        return $results;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTagsData()
    {
        $product = $this->getProduct();
        $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
        $url = $baseUrl . 'catalogsearch/result/?q=';
        $results = [];
        if ($product && $product->getId()) {
            $tags = $product->getData('product_tags');
            if ($tags) {
                $tags = explode(',', $tags);
                if (count($tags) > 0) {
                    foreach ($tags as $tag) {
                        $results[] =
                            [
                                'label' => trim($tag),
                                'url' => $url . trim($tag)
                            ];
                    }

                }
            }
        }
        return $results;
    }
}