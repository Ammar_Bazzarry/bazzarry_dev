<?php

namespace Tigren\CustomTheme\Block\Html;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\StoreManagerInterface;
use Tigren\Bannermanager\Model\BlockFactory;

/**
 * Class Topmenu
 * @package Tigren\CustomTheme\Block\Html
 */
class Topmenu extends \Magento\Theme\Block\Html\Topmenu
{
    /**
     * @var BlockFactory
     */
    protected $_blockFactory;

    /**
     * @var CategoryFactory
     */
    protected $categoryFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry = null;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Topmenu constructor.
     * @param Template\Context $context
     * @param BlockFactory $blockFactory
     * @param NodeFactory $nodeFactory
     * @param TreeFactory $treeFactory
     * @param CategoryFactory $categoryFactory
     * @param StoreManagerInterface $storeManager
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        BlockFactory $blockFactory,
        NodeFactory $nodeFactory,
        TreeFactory $treeFactory,
        CategoryFactory $categoryFactory,
        StoreManagerInterface $storeManager,
        Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $nodeFactory, $treeFactory, $data);
        $this->_blockFactory = $blockFactory;
        $this->categoryFactory = $categoryFactory;
        $this->coreRegistry = $registry;
        $this->_storeManager = $storeManager;
    }

    /**
     * @param Node $menuTree
     * @param string $childrenWrapClass
     * @param int $limit
     * @param array $colBrakes
     * @return string
     * @throws NoSuchEntityException
     */
    protected function _getHtml(
        Node $menuTree,
        $childrenWrapClass,
        $limit,
        $colBrakes = []
    ) {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = $parentLevel === null ? 0 : $parentLevel + 1;

        $counter = 1;
        $itemPosition = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        /** @var Node $child */
        foreach ($children as $child) {
            if ($childLevel === 0 && $child->getData('is_parent_active') === false) {
                continue;
            }
            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }
            if (!empty($colBrakes) && count($colBrakes) && $colBrakes[$counter]['colbrake']) {
                $html .= '</ul></li><li class="column"><ul>';
            }

            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            if ($child->hasChildren()) {
                $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>' . $this->escapeHtml(
                        $child->getName()
                    ) . '</span></a></span>' . $this->_addSubMenu(
                        $child,
                        $childLevel,
                        $childrenWrapClass,
                        $limit
                    );
            } else {
                $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>' . $this->escapeHtml(
                        $child->getName()
                    ) . '</span></a>' . $this->_addSubMenu(
                        $child,
                        $childLevel,
                        $childrenWrapClass,
                        $limit
                    );
            }
            $html .= '</li>';
            $itemPosition++;
            $counter++;
        }

        if (!empty($colBrakes) && count($colBrakes) && $limit) {
            $html = '<li class="column"><ul>' . $html . '</ul></li>';
        }

        return $html;
    }

    /**
     * @param Node $child
     * @param string $childLevel
     * @param string $childrenWrapClass
     * @param int $limit
     * @return string
     * @throws NoSuchEntityException
     */
    protected function _addSubMenu($child, $childLevel, $childrenWrapClass, $limit)
    {
        $html = parent::_addSubMenu($child, $childLevel, $childrenWrapClass, $limit);

        if (!$child->hasChildren()) {
            return $html;
        }

        if ($childLevel == 0) {
            if ($child->_getData('thumbnail')) {
                $imgUrlLv2 = $this->getUrlImage($child->_getData('thumbnail'));
                $imgLv2 = '<a class="banner-image-url" href="' . $child->getUrl() .'"><img src="' . $imgUrlLv2 . '"/></a>';
                $html = '<ul class="menu-banners-wrap has-banner"><li class="category-list">' . $html;
                $html .= '<li class="category-banner-menu">';
                // HTML of banner ads
                $html .= $imgLv2;
                $html .= '</li>';
            } else {
                $html = '<ul class="menu-banners-wrap no-banner"><li class="category-list">' . $html;
            }
            $html .= '</ul></li>';
        }

        if ($childLevel == 1) {
            if ($child->_getData('thumbnail')) {
                $imgUrlLv3 = $this->getUrlImage($child->_getData('thumbnail'));
                $imgLv3 = '<a class="banner-image-url" href="' . $child->getUrl() .'"><img src="' . $imgUrlLv3 . '"/></a>';
                $html = $imgLv3 . $html;
            }
        }

        return $html;
    }

    /**
     * @param $fileName
     * @return string
     * @throws NoSuchEntityException
     */
    public function getUrlImage($fileName)
    {
        return $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/category/' . $fileName;
    }

    /**
     * Get cache key informative items
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function getCacheKeyInfo()
    {
        return array_merge(
            [
                'BLOCK_TPL',
                $this->_storeManager->getStore()->getCode(),
                $this->getTemplateFile(),
                'base_url' => $this->getBaseUrl(),
                'template' => $this->getTemplate()
            ], $this->getIdentities()
        );
    }
}
