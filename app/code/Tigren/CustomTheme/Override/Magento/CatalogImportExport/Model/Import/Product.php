<?php
namespace Tigren\CustomTheme\Override\Magento\CatalogImportExport\Model\Import;

class Product extends \Magento\CatalogImportExport\Model\Import\Product
{
    private $productEntityLinkField;

    protected function _saveProductAttributes(array $attributesData)
    {
        $eavAttribute = \Magento\Framework\App\ObjectManager::getInstance()
            ->create('Magento\Eav\Model\ResourceModel\Entity\Attribute');
        $attId = $eavAttribute->getIdByCode('catalog_product', 'name');

        $linkField = $this->getProductEntityLinkField();
        foreach ($attributesData as $tableName => $skuData) {
            $tableData = [];
            foreach ($skuData as $sku => $attributes) {
                $linkId = $this->_oldSku[strtolower($sku)][$linkField];
                foreach ($attributes as $attributeId => $storeValues) {
                    foreach ($storeValues as $storeId => $storeValue) {
                        $tableData[] = [
                            $linkField => $linkId,
                            'attribute_id' => $attributeId,
                            'store_id' => $storeId,
                            'value' => $attributeId == $attId?trim($storeValue):$storeValue,
                        ];
                    }
                }
            }
            $this->_connection->insertOnDuplicate($tableName, $tableData, ['value']);
        }

        return $this;
    }

    private function getProductEntityLinkField()
    {
        if (!$this->productEntityLinkField) {
            try {
                $this->productEntityLinkField = $this->getMetadataPool()
                    ->getMetadata(\Magento\Catalog\Api\Data\ProductInterface::class)
                    ->getLinkField();
            } catch (\Exception $e) {
            }
        }
        return $this->productEntityLinkField;
    }

}
