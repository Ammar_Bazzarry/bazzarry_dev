<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */


namespace Tigren\CustomTheme\Override\Amasty\Shopby\Plugin\Store;

use Magento\Framework\App\ActionInterface;

class Switcher
{
    const STORE_PARAM_NAME = '___store';

    /**
     * @var \Amasty\ShopbyBase\Api\UrlBuilderInterface
     */
    private $urlBuilder;

    /**
     * @var \Magento\Framework\Url\EncoderInterface
     */
    private $encoder;

    /**
     * @var \Magento\Framework\Data\Helper\PostHelper
     */
    private $postHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    protected $_request;

    public function __construct(
        \Amasty\ShopbyBase\Api\UrlBuilderInterface $urlBuilder,
        \Magento\Framework\Url\EncoderInterface $encoder,
        \Magento\Framework\Data\Helper\PostHelper $postHelper,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->urlBuilder = $urlBuilder;
        $this->encoder = $encoder;
        $this->postHelper = $postHelper;
        $this->storeManager = $storeManager;
        $this->_request = $request;
    }

    /**
     * @param \Magento\Store\Block\Switcher $subject
     * @param \Closure $closure
     * @param $store
     * @param array $data
     * @return false|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundGetTargetStorePostData(
        \Magento\Store\Block\Switcher $subject,
        \Closure $closure,
        \Magento\Store\Model\Store $store,
        $data = []
    )
    {
        $currentUrl = $store->getCurrentUrl(false);
        $data[self::STORE_PARAM_NAME] = $store->getCode();
        $data['___from_store'] = $this->storeManager->getStore()->getCode();
        $data[ActionInterface::PARAM_NAME_URL_ENCODED] = $this->encoder->encode($currentUrl);
        $url = $subject->getUrl('stores/store/redirect');
        return $this->postHelper->getPostData($url, $data);
    }
}
