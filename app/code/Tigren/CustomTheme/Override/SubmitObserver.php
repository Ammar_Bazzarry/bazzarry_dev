<?php

namespace Tigren\CustomTheme\Override;

use Magento\Sales\Model\Order\Email\Sender\OrderSender;

class SubmitObserver extends \Magento\Quote\Observer\SubmitObserver
{
    protected $_multInventoryHelperSystem;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var OrderSender
     */
    private $orderSender;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param OrderSender $orderSender
     */

    public function __construct(\Psr\Log\LoggerInterface $logger,
                                \Amasty\MultiInventory\Helper\System $multiInventoryHelperSystem,
                                OrderSender $orderSender)
    {
        parent::__construct($logger, $orderSender);
        $this->logger = $logger;
        $this->_multInventoryHelperSystem = $multiInventoryHelperSystem;
        $this->orderSender = $orderSender;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var  \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        /** @var  \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();

        /**
         * a flag to set that there will be redirect to third party after confirmation
         */
        $redirectUrl = $quote->getPayment()->getOrderPlaceRedirectUrl();
        if (!$redirectUrl && $order->getCanSendNewEmailFlag()) {
            try {
                if(!$this->_multInventoryHelperSystem->getSeparateOrders()) {
                    $this->orderSender->send($order);
                }
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
    }

}