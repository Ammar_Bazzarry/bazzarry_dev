<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */

namespace Tigren\CustomTheme\Model\ResourceModel\Warehouse;

use Amasty\MultiInventory\Model\ResourceModel\Warehouse\Collection;

class CollectionWarehouse extends Collection
{
    public function toOptionArray()
    {
        return $this->_toOptionArray(null, 'title');
    }
}
