<?php

namespace Tigren\CustomTheme\Model\Checkout\Cod;

use Tigren\CustomTheme\Api\Checkout\Cod\VerifyInterface;


class Verify implements VerifyInterface
{

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Tigren\CustomTheme\Helper\Data
     */
    protected $_helper;

    /**
     * Verify constructor.
     * @param \Tigren\CustomTheme\Helper\Data $helper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Tigren\CustomTheme\Helper\Data $helper,
        \Magento\Checkout\Model\Session $checkoutSession
    )
    {
        $this->_helper = $helper;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @return bool|mixed
     */
    public function sendSms()
    {
        $verifyCode = rand(100000, 999999);

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->_checkoutSession->getQuote();
        $quote->setData('cod_verify',$verifyCode);
        $result = [];
        try {
            $quote->save();
            $this->_helper->sendSmsVerify($quote);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }


    /**
     * @param mixed $code
     * @return bool|mixed
     */
    public function verify($code)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->_checkoutSession->getQuote();
        if($quote->getData('cod_verify') && $code && $quote->getData('cod_verify') === $code)
        {
            return true;
        }else{
            return false;
        }
    }

}