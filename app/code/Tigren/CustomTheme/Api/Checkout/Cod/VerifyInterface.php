<?php

namespace Tigren\CustomTheme\Api\Checkout\Cod;


interface VerifyInterface
{

    /**
     * @return mixed
     */
    public function sendSms();


    /**
     * @param mixed $code
     * @return mixed
     */
    public function verify($code);

}
