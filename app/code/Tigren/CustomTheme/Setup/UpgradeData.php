<?php

namespace Tigren\CustomTheme\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Class UpgradeData
 * @package Tigren\CustomTheme\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    private $salesSetupFactory;

    /**
     * @var QuoteSetupFactory
     */
    private $quoteSetupFactory;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     * @param SalesSetupFactory $salesSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        SalesSetupFactory $salesSetupFactory,
        QuoteSetupFactory $quoteSetupFactory,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        if (version_compare($context->getVersion(), '1.0.6', '<')) {

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'region')
                ->addData([
                    'used_in_forms' => [
                        'adminhtml_checkout',
                        'adminhtml_customer',
                        'adminhtml_customer_address',
                        'customer_account_edit',
                        'customer_address_edit',
                        'customer_register_address'
                    ]
                ])
                ->addData([
                    'visible' => true
                ])
                ->addData([
                    'label' => 'City'
                ]);
            $attribute->save();

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'region_id')
                ->addData([
                    'used_in_forms' => [
                        'adminhtml_checkout',
                        'adminhtml_customer',
                        'adminhtml_customer_address',
                        'customer_account_edit',
                        'customer_address_edit',
                        'customer_register_address'
                    ]
                ])
                ->addData([
                    'visible' => true
                ])
                ->addData([
                    'label' => 'City'
                ]);
            $attribute->save();

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'city');
            $attribute->delete();

            ;
        }
        if (version_compare($context->getVersion(), '1.0.7', '<')) {
            $customerSetup->addAttribute(\Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY, 'city', [
                'label' => 'City',
                'input' => 'text',
                'type' => 'static',
                'required' => false,
                'position' => 1000,
                'visible' => false,
                'system' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false,
                'backend' => ''
            ]);
            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'city')
                ->addData([
                    'used_in_forms' => [
                    ]
                ]);
            $attribute->save();

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'region');
            $attribute->setFrontendLabel('City');
            $attribute->save();
            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'region_id');
            $attribute->setFrontendLabel('City');
            $attribute->save();
        }
        if (version_compare($context->getVersion(), '1.0.10', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'product_tags',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Tags',
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
        }
    }

}
