<?php
namespace Tigren\CustomTheme\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $setup->getConnection();
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'cod_verify',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'cod_verify'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.8', '<')) {
            $setup->getConnection();
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'hash_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'hash_id'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.9', '<')) {
            $setup->getConnection();
            $setup->getConnection()->addColumn(
                $setup->getTable('quote'),
                'cod_verify',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'cod_verify'
                ]
            );

            $setup->getConnection()->dropColumn($setup->getTable('sales_order'), 'cod_verify');
            $setup->getConnection()->dropColumn($setup->getTable('sales_order'), 'hash_id');
        }
        if (version_compare($context->getVersion(), '1.0.11', '<')) {
            $setup->getConnection();
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_creditmemo_grid'),
                'base_currency_code',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'base_currency_code'
                ]
            );
        }


        $setup->endSetup();
    }
}
