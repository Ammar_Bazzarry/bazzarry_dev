var config = {
    map: {
        '*': {
            'Magento_OfflinePayments/js/view/payment/method-renderer/cashondelivery-method': 'Tigren_CustomTheme/js/view/payment/method-renderer/cashondelivery-method',
            'ajaxCart': 'Tigren_CustomTheme/js/ajax-to-cart',
            'catalogAddToCart': 'Tigren_CustomTheme/js/catalog-add-to-cart',
            'Magento_OfflinePayments/template/payment/cashondelivery.html':
                'Tigren_CustomTheme/template/payment/cashondelivery.html',
            'Magento_Checkout/js/view/minicart': 'Tigren_CustomTheme/js/view/minicart',
            'Magento_Checkout/js/view/summary/shipping': 'Tigren_CustomTheme/js/view/summary/shipping',
            'sidebar': 'Tigren_CustomTheme/js/sidebar',
            'Magento_Catalog/js/product/storage/data-storage': 'Tigren_CustomTheme/js/product/storage/data-storage'
        }
    }
};