<?php

namespace Tigren\CustomTheme\Observer;

use Magento\Framework\Event\ObserverInterface;

class ChangeCurrencyPosition implements ObserverInterface
{
    protected $_locale;

    public function __construct(
        \Magento\Framework\Locale\Resolver $locale
    )
    {
        $this->_locale = $locale;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $locale = $this->_locale->getLocale();

        if($locale === 'ar_SA') {
            $currencyOptions = $observer->getEvent()->getCurrencyOptions();
            $currencyOptions->setData('position', \Magento\Framework\Currency::RIGHT);
            return $this;
        }
    }
}