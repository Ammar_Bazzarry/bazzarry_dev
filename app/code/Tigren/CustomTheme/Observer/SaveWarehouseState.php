<?php

namespace Tigren\CustomTheme\Observer;

use Magento\Framework\Event\ObserverInterface;
use Amasty\MultiInventory\Api\WarehouseRepositoryInterface as Repository;
use Amasty\MultiInventory\Helper\Distance as Helper;

/**
 * Class SaveWarehouseState
 * @package Tigren\CustomTheme\Observer
 */
class SaveWarehouseState implements ObserverInterface
{

    /**
     * @var \Tigren\CustomTheme\Helper\Data
     */
    private $helper;
    /**
     * @var Repository
     */
    private $repository;
    /**
     * @var Helper
     */
    private $whHelper;


    /**
     * SaveWarehouseState constructor.
     * @param Repository $repository
     * @param Helper $whHelper
     * @param \Tigren\CustomTheme\Helper\Data $helper
     */
    public function __construct(
        Repository $repository,
        Helper $whHelper,
        \Tigren\CustomTheme\Helper\Data $helper
    )
    {
        $this->whHelper = $whHelper;
        $this->repository = $repository;
        $this->helper = $helper;
    }


    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Amasty\MultiInventory\Model\Warehouse $warehouse */
        $warehouse = $observer->getObject();
        $address = $this->prepareAddressForGoogle($warehouse->getData());
        $latlng = $this->whHelper->getCoordinatesByAddress($address);
        if (isset($latlng)) {
            $warehouse->setLng($latlng['lng']);
            $warehouse->setLat($latlng['lat']);
            $warehouse->save();
        }
    }

    /**
     * @param $data
     * @return string
     */
    public function prepareAddressForGoogle($data)
    {
        $address = "";
        $arrayCodes = ['country', 'country_id', 'state', 'region', 'city', 'address', 'street', 'zip'];
        foreach ($arrayCodes as $code) {
            if (isset($data[$code]) && !empty($data[$code])) {
                if ($code == 'region') {
                    /** If address was save, region will be an array on M2.2.* */
                    if (is_array($data[$code])) {
                        $data[$code] = (string)$data[$code][$code];
                    }
                    /** If address was saved, region will be an object on M2.1.* */
                    if (is_object($data[$code])) {
                        $data[$code] = (string)$data[$code]->getRegion();
                    }
                }
                if ($code == 'state' && is_numeric($data[$code])) {
                    $state = $this->helper->getStateName($data[$code]);
                    $data[$code] = $state;
                }
                if (strlen($address) > 0) {
                    $address .= " ";
                }
                $address .= $data[$code];
            }
        }

        return urlencode($address);
    }
}
