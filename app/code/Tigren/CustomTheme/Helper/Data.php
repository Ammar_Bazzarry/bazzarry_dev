<?php

namespace Tigren\CustomTheme\Helper;


/**
 * Class Data
 * @package Tigren\CustomTheme\Helper
 */
/**
 * Class Data
 * @package Tigren\CustomTheme\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Lof\SmsNotification\Model\SendSms
     */
    protected $sendsms;
    /**
     * @var \Magento\Framework\Locale\Resolver
     */
    protected $_store;
    /**
     * @var \Magento\Framework\App\Cache\Type\Block
     */
    protected $cache;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Lof\SmsNotification\Helper\Data
     */
    protected $_smsNotificationData;

    /**
     * @var \Lof\SmsNotification\Helper\msg91ApiCall
     */
    protected $_msg91ApiCall;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $_httpContext;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlInterface;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var \Magento\Theme\Block\Html\Header\Logo
     */
    protected $_logo;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $_categoryCollection;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    protected $_categoryFactory;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Lof\SmsNotification\Model\SendSms $sendsms
     * @param \Magento\Framework\Locale\Resolver $store
     * @param \Magento\Framework\App\Cache\Type\Block $cache
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
        \Lof\SmsNotification\Model\SendSms $sendsms,
        \Magento\Framework\Locale\Resolver $store,
        \Magento\Framework\App\Cache\Type\Block $cache,
        \Lof\SmsNotification\Helper\Data $smsNotificationData,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Theme\Block\Html\Header\Logo $logo,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall
    )
    {
        parent::__construct($context);
        $this->_logo = $logo;
        $this->resource = $resource;
        $this->_store = $store;
        $this->cache = $cache;
        $this->_smsNotificationData = $smsNotificationData;
        $this->_msg91ApiCall = $msg91ApiCall;
        $this->_httpContext = $httpContext;
        $this->_urlInterface = $urlInterface;
        $this->_customerSession = $customerSession;
        $this->sendsms = $sendsms;
        $this->_customerFactory = $customerFactory;
        $this->_categoryCollection = $collectionFactory;
        $this->_registry = $registry;
        $this->_categoryFactory = $categoryFactory;
    }


    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @throws \Exception
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function sendSmsVerify($quote)
    {
        /** @var \Lof\SmsNotification\Helper\Data $helperData */
        $helperData = $this->_smsNotificationData;
        $msg91ApiCall = $this->_msg91ApiCall;
        $customerFirstName = $quote->getShippingAddress()->getFirstname();
        $customerLastName = $quote->getShippingAddress()->getLastname();
        $email = $quote->getCustomerEmail();
        $storeName = $quote->getStore()->getName();
        $subTotal = $quote->getSubtotal();
        $verifyCode = $quote->getData('cod_verify');
        $telephone = $quote->getShippingAddress()->getTelephone();
        if ($quote->getData('pickupstore_id')) {
            if ($quote->getCustomerId()) {
                $customer = $this->_customerFactory->create()->load($quote->getCustomerId());
                $telephone = $customer->getMobilenumber();
            }
        }
        if ($helperData->getConfig('sms_customer/enable_sms_verify_cod_order')) {
            $message = $helperData->getVerifyCodMessageForUser($subTotal, $customerFirstName, $customerLastName, $email, $storeName, $verifyCode);
            $smsType = $helperData->getConfig('sms_settings/type_sms');
            if ($smsType === 'bulksms') {
                $send_to = 'customer';
                $temp = $this->sendsms->send($telephone, $message, $send_to);
            }
            if ($smsType === 'msg91') {
                if (!$msg91ApiCall->validateSmsConfig()) {
                    $this->_logger->error('Please Configure all SMS Gateway Fields Configuration.');
                    return;
                }
                $this->_msg91ApiCall->callApiUrl($telephone, $message);
            }
        }
    }

    /**
     * @return string
     */
    public function getOrderLogoUrl()
    {
        $folderName = \Magento\Config\Model\Config\Backend\Image\Logo::UPLOAD_DIR;
        $storeLogoPath = $this->scopeConfig->getValue(
            'sales/order_print/logo',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $path = 'orderlogo/' . $storeLogoPath;
        $logoUrl = $this->_urlBuilder
                ->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $path;

        if ($storeLogoPath !== null) {
            $url = $logoUrl;
        } else {
            $url = $this->_logo->getLogoSrc();
        }
        return $url;
    }


    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    public function getConnection()
    {
        return $this->resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
    }

    /**
     * @param $stateId
     * @return string
     */
    public function getStateName($stateId)
    {
        $sql = $this->getConnection()->select()->from('directory_country_region', 'default_name')
            ->where('region_id = ?', $stateId);
        $state = $this->getConnection()->fetchOne($sql);
        return $state;
    }

    /**
     * @return mixed|null
     */
    public function getIsLoggedIn()
    {
        return $this->_httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->_urlInterface->getBaseUrl();
    }

    /**
     * @return \Magento\Customer\Model\Session
     */
    public function getCustomerSession()
    {
        return $this->_customerSession;
    }

    /**
     * @return string
     */
    public function getWishlistUrl()
    {
        return $this->_urlInterface->getUrl('wishlist');
    }

    /**
     * @return string
     */
    public function getCheckoutUrl()
    {
        return $this->_urlInterface->getUrl('checkout');
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Category\Collection
     */
    public function getCategories()
    {
        $categories = $this->_categoryCollection->create();
        $categories->addFieldToSelect('*');
        $categories->addFieldToFilter('level', 2);
        return $categories;
    }

    /**
     * @return mixed
     */
    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    public function getCategoryName($categoryId)
    {
        $name = '';
        $category = $this->_categoryFactory->create()->load($categoryId);
        if ($category->getId()){
            $name = $category->getName();
        }
        return $name;
    }
}
