<?php

namespace Tigren\CustomTheme\Plugin\Magento\Customer\Model\ResourceModel;

use Magento\Customer\Model\Address as CustomerAddressModel;
use Magento\Customer\Model\Customer as CustomerModel;
use Magento\Customer\Model\ResourceModel\Address\Collection;
use Magento\Framework\Exception\InputException;

/**
 * Class AddressRepository
 * @package Tigren\CustomTheme\Plugin\Magento\Customer\Model\ResourceModel
 */
class AddressRepository extends \Magento\Customer\Model\ResourceModel\AddressRepository
{

    /**
     * @param \Magento\Customer\Model\ResourceModel\AddressRepository $sub
     * @param \Closure $process
     * @param \Magento\Customer\Api\Data\AddressInterface $address
     * @return \Magento\Customer\Api\Data\AddressInterface
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundSave(\Magento\Customer\Model\ResourceModel\AddressRepository $sub, \Closure $process, \Magento\Customer\Api\Data\AddressInterface $address)
    {
        $addressModel = null;
        $customerModel = $this->customerRegistry->retrieve($address->getCustomerId());
        if ($address->getId()) {
            $addressModel = $this->addressRegistry->retrieve($address->getId());
        }

        if ($addressModel === null) {
            /** @var \Magento\Customer\Model\Address $addressModel */
            $addressModel = $this->addressFactory->create();
            $addressModel->updateData($address);
            $addressModel->setCustomer($customerModel);
        } else {
            $addressModel->updateData($address);
        }
        $addressModel->setStoreId($customerModel->getStoreId());

        $errors = $addressModel->validate();
        if (is_array($errors)&&($cityArrKey = array_search(__('%fieldName is a required field.', ['fieldName' => 'city']),
                $errors)) !== false){
            $errors = true;
        }

        if ($errors !== true && $errors) {
            $inputException = new InputException();
            foreach ($errors as $error) {
                $inputException->addError($error);
            }
            throw $inputException;
        }
        $addressModel->save();
        $address->setId($addressModel->getId());
        // Clean up the customer registry since the Address save has a
        // side effect on customer : \Magento\Customer\Model\ResourceModel\Address::_afterSave
        $this->addressRegistry->push($addressModel);
        $this->updateAddressCollection($customerModel, $addressModel);

        return $addressModel->getDataModel();
    }

    /**
     * @param CustomerModel $customer
     * @param CustomerAddressModel $address
     */
    private function updateAddressCollection(CustomerModel $customer, CustomerAddressModel $address)
    {
        $customer->getAddressesCollection()->removeItemByKey($address->getId());
        $customer->getAddressesCollection()->addItem($address);
    }
}
