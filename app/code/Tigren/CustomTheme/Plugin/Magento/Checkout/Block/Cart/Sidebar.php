<?php

namespace Tigren\CustomTheme\Plugin\Magento\Checkout\Block\Cart;

/**
 * Class Sidebar
 * @package Tigren\CustomTheme\Plugin\Magento\Checkout\Block\Cart
 */
class Sidebar
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Cart constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(\Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->_storeManager = $storeManager;
    }

    /**
     * @param \Magento\Checkout\Block\Cart\Sidebar $subject
     * @param $result
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterGetConfig(\Magento\Checkout\Block\Cart\Sidebar $subject, $result)
    {
        $result['currency_code'] = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        return $result;
    }
}