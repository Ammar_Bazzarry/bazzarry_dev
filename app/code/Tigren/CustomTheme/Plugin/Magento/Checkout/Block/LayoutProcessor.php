<?php

namespace Tigren\CustomTheme\Plugin\Magento\Checkout\Block;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\UrlInterface;
use Magento\Store\Api\StoreResolverInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class LayoutProcessor
 * @package Marginframe\Thaiaddress\Plugin\Magento\Checkout\Block
 */
class LayoutProcessor
{
    /**
     * @var CheckoutSession
     */
    public $checkoutSession;

    /**
     * @var null
     */
    public $quote = null;

    /**
     * @var null
     */
    public $customer = null;

    /**
     * @var \Magento\Ui\Component\Form\AttributeMapper
     */
    protected $attributeMapper;

    /**
     * @var \Magento\Checkout\Block\Checkout\AttributeMerger
     */
    protected $merger;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Customer\Model\AttributeMetadataDataProvider
     */
    private $attributeMetadataDataProvider;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var DirectoryHelper
     */
    private $directoryHelper;

    /**
     * LayoutProcessor constructor.
     *
     * @param \Magento\Customer\Model\AttributeMetadataDataProvider $attributeMetadataDataProvider
     * @param \Magento\Ui\Component\Form\AttributeMapper $attributeMapper
     * @param \Magento\Checkout\Block\Checkout\AttributeMerger $merger
     * @param CheckoutSession $checkoutSession
     * @param UrlInterface $urlBuilder
     * @param StoreResolverInterface $storeResolver @deprecated
     * @param DirectoryHelper $directoryHelper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Customer\Model\AttributeMetadataDataProvider $attributeMetadataDataProvider,
        \Magento\Ui\Component\Form\AttributeMapper $attributeMapper,
        \Magento\Checkout\Block\Checkout\AttributeMerger $merger,
        CheckoutSession $checkoutSession,
        UrlInterface $urlBuilder,
        StoreResolverInterface $storeResolver,
        DirectoryHelper $directoryHelper,
        StoreManagerInterface $storeManager = null
    )
    {
        $this->attributeMetadataDataProvider = $attributeMetadataDataProvider;
        $this->attributeMapper = $attributeMapper;
        $this->merger = $merger;
        $this->checkoutSession = $checkoutSession;
        $this->urlBuilder = $urlBuilder;
        $this->directoryHelper = $directoryHelper;
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param \Closure $proceed
     * @param array $jsLayout
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function aroundProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        \Closure $proceed,
        array $jsLayout
    )
    {
        $jsLayoutResult = $proceed($jsLayout);
        if ($this->getQuote()->isVirtual()) {
            return $jsLayoutResult;
        }

        $cityShipping = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input'
            ],
            'dataScope' => 'shippingAddress.city',
            'label' => __('City'),
            'provider' => 'checkoutProvider',
            'validation' => [
                'required-entry' => 1,
                'max_text_length' => 255,
                'min_text_length' => 1
            ],
            'visible' => false,
            'sortOrder' => 230
        ];
        $cityBilling = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'billingAddress',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input'
            ],
            'dataScope' => 'billingAddress.city',
            'label' => __('City'),
            'provider' => 'checkoutProvider',
            'validation' => [
                'required-entry' => 1,
                'max_text_length' => 255,
                'min_text_length' => 1
            ],
            'visible' => false,
            'sortOrder' => 230
        ];
        /*$cityIdShipping = [
            'component' => 'Tigren_CustomTheme/js/form/element/drop-down/directory/city',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
                'additionalClasses' => 'shipping-city'
            ],
            'label' => __('City'),
            'dataScope' => 'shippingAddress.city_id',
            'validation' => [
                'required-entry' => true
            ],
            'sortOrder' => 240
        ];

        $cityIdBiling = [
            'component' => 'Tigren_CustomTheme/js/form/element/drop-down/directory/city',
            'config' => [
                'customScope' => 'billingAddress.custom_attributes',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
                'additionalClasses' => 'shipping-city'
            ],
            'label' => __('City'),
            'dataScope' => 'billingAddress.city_id',
            'validation' => [
                'required-entry' => true
            ],
            'filterBy' => [
                'target' => '${ $.provider }:shippingAddress.country_id',
                'field' => 'country_id'
            ],
            'sortOrder' => 240
        ];*/

        if (isset($jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children'])) {
            $jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['city'] = $cityShipping;
            /*$jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['city_id'] = $cityIdShipping;*/
            /*unset($jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['region_id']);
            unset($jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['region']);*/
        }

        if (isset($jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children']
        )) {
            foreach ($jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'] as $key => $payment) {
                if ($key === 'before-place-order') {
                    continue;
                }
                //unset($jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']['region_id']);
                //unset($jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']['region']);
                $jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']['city'] = $cityBilling;
                //$jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']['city_id'] = $cityIdBiling;
            }
        }
        return $jsLayoutResult;
    }

    /**
     * Get quote
     *
     * @return \Magento\Quote\Model\Quote|null
     */
    public function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }

        return $this->quote;
    }

}