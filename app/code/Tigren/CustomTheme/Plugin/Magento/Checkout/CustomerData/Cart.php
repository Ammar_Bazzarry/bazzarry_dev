<?php
namespace Tigren\Customtheme\Plugin\Magento\Checkout\CustomerData;

/**
 * Class Cart
 * @package Tigren\Customtheme\Plugin\Magento\Checkout\CustomerData
 */
class Cart {

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Cart constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(\Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->_storeManager = $storeManager;
    }

    /**
     * @param \Magento\Checkout\CustomerData\Cart $subject
     * @param array $result
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterGetSectionData(\Magento\Checkout\CustomerData\Cart $subject, array $result)
    {
        $result['currency_code'] = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        return $result;
    }
}