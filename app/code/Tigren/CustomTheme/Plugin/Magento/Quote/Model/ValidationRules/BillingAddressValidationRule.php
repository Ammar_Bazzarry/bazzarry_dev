<?php

namespace Tigren\CustomTheme\Plugin\Magento\Quote\Model\ValidationRules;

use Magento\Framework\Validation\ValidationResultFactory;
use Magento\Quote\Model\Quote;

/**
 * @inheritdoc
 */
class BillingAddressValidationRule
{
    /**
     * @var string
     */
    private $generalMessage;

    /**
     * @var ValidationResultFactory
     */
    private $validationResultFactory;

    /**
     * @param ValidationResultFactory $validationResultFactory
     * @param string $generalMessage
     */
    public function __construct(
        ValidationResultFactory $validationResultFactory,
        $generalMessage = ''
    ) {
        $this->validationResultFactory = $validationResultFactory;
        $this->generalMessage = $generalMessage;
    }

    /**
     * @param \Magento\Quote\Model\ValidationRules\ShippingAddressValidationRule $sub
     * @param Quote $quote
     * @return array
     */
    public function aroundValidate(\Magento\Quote\Model\ValidationRules\BillingAddressValidationRule $sub, \Closure $process,Quote $quote)
    {
        $validationErrors = [];

        if (!$quote->isVirtual()) {
            $validationResult = $quote->getShippingAddress()->validate();
            if (is_array($validationResult)&&($cityArrKey = array_search(__('%fieldName is a required field.', ['fieldName' => 'city']),
                    $validationResult)) !== false){
                $validationResult = true;
            }
            if ($validationResult !== true) {
                $validationErrors = [__($this->generalMessage)];
            }
            if (is_array($validationResult)) {
                $validationErrors = array_merge($validationErrors, $validationResult);
            }
        }

        return [$this->validationResultFactory->create(['errors' => $validationErrors])];
    }
}
