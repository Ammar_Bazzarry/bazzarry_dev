<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsShipping\Plugin\Vnecoms\VendorsShipping\Block\Checkout;

use Closure;

/**
 *
 * @package Tigren\VendorsShipping\Plugin\Vnecoms\VendorsShipping\Block\Checkout
 */
class LayoutProcessor
{
    /**
     * @param \Vnecoms\VendorsShipping\Block\Checkout\LayoutProcessor $subject
     * @param Closure $proceed
     * @param array $jsLayout
     * @return mixed
     */
    public function aroundProcess(
        \Vnecoms\VendorsShipping\Block\Checkout\LayoutProcessor $subject,
        Closure $proceed,
        array $jsLayout
    ) {
        return $jsLayout;
    }
}
