<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsShipping\Override\Vnecoms\VendorsShipping\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Api\CartItemRepositoryInterface as QuoteItemRepository;
use Magento\Quote\Api\CartRepositoryInterface;
use Vnecoms\Vendors\Model\VendorFactory;
use Vnecoms\VendorsConfig\Helper\Data;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class ShippingConfigProvider implements ConfigProviderInterface
{
    /**
     * @var VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var Data
     */
    protected $_vendorConfig;
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;
    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;
    /**
     * @var QuoteItemRepository
     */
    private $quoteItemRepository;

    /**
     * ShippingConfigProvider constructor.
     * @param VendorFactory $vendorFactory
     * @param Data $vendorConfig
     * @param CheckoutSession $checkoutSession
     * @param CartRepositoryInterface $quoteRepository
     * @param ScopeConfigInterface $scopeConfig
     * @param QuoteItemRepository $quoteItemRepository
     */
    public function __construct(
        VendorFactory $vendorFactory,
        Data $vendorConfig,
        CheckoutSession $checkoutSession,
        CartRepositoryInterface $quoteRepository,
        ScopeConfigInterface $scopeConfig,
        QuoteItemRepository $quoteItemRepository
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->_vendorFactory = $vendorFactory;
        $this->_vendorConfig = $vendorConfig;
        $this->checkoutSession = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
        $this->quoteItemRepository = $quoteItemRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $output = [];
        $quoteId = $this->checkoutSession->getQuote()->getId();
        $quoteItems = $this->quoteItemRepository->getList($quoteId);
        $vendorsList = [];
        $hasMainProduct = false;
        $hasVendorProduct = false;
        $mainVendor = $this->scopeConfig->getValue('customvendor/general/main_vendor');
        foreach ($quoteItems as $index => $quoteItem) {
            if ($quoteItem->getProduct()->getData('vendor_id') == $mainVendor) {
                $hasMainProduct = true;
            } else {
                $hasVendorProduct = true;
            }
            $vendorId = $quoteItem->getVendorId();
            if (!$vendorId) {
                continue;
            }
            $vendor = $this->_vendorFactory->create()->load($vendorId);
            if (!$vendor->getId()) {
                continue;
            }

            $vendorsList[$vendorId] = [];
            $vendorsList[$vendorId]['vendor_id'] = $vendor->getVendorId();
            $vendorsList[$vendorId]['entity_id'] = $vendor->getId();
            $vendorsList[$vendorId]['is_main'] = $vendor->getId() == $mainVendor;
            $vendorsList[$vendorId]['items'][] = $quoteItem->getItemId();
            $title = $this->_vendorConfig->getVendorConfig('general/store_information/name', $vendorId);
            $vendorsList[$vendorId]['shipping_title'] = $title ? $title : $vendor->getVendorId();
        }
        $output['vendors_list'] = $vendorsList;
        $output['has_main_product'] = $hasMainProduct;
        $output['main_vendor'] = $mainVendor;
        $output['has_vendor_product'] = $hasVendorProduct;
        return $output;
    }
}
