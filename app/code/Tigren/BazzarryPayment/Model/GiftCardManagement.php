<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\BazzarryPayment\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface as CartRepository;
use MageWorx\GiftCards\Helper\Data as Helper;
use MageWorx\GiftCards\Helper\GiftCardManagement as GiftCardManagementHelper;
use MageWorx\GiftCards\Model\Session as GiftCardSession;
use Tigren\BazzarryPayment\Helper\Data as BazzarryPayHelper;

class GiftCardManagement implements \Tigren\BazzarryPayment\Api\GiftCardManagementInterface
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * @var \MageWorx\GiftCards\Model\GiftCardsRepository
     */
    private $giftCardsRepository;

    /**
     * @var CartRepository
     */
    private $cartRepository;

    /**
     * @var \MageWorx\GiftCards\Model\Session
     */
    private $giftCardSession;

    /**
     * @var GiftCardManagementHelper
     */
    private $giftCardManagementHelper;

    protected $_bazzarryPayhelper;

    /**
     * GiftCardManagement constructor.
     *
     * @param Helper $helper
     * @param \MageWorx\GiftCards\Model\GiftCardsRepository $giftCardsRepository
     * @param CartRepository $cartRepository
     * @param \MageWorx\GiftCards\Model\Session $giftCardSession
     * @param GiftCardManagementHelper $giftCardManagementHelper
     */
    public function __construct(
        Helper $helper,
        \MageWorx\GiftCards\Model\GiftCardsRepository $giftCardsRepository,
        CartRepository $cartRepository,
        GiftCardSession $giftCardSession,
        BazzarryPayHelper $bazzarryPayHelper,
        GiftCardManagementHelper $giftCardManagementHelper
    )
    {
        $this->helper = $helper;
        $this->giftCardsRepository = $giftCardsRepository;
        $this->cartRepository = $cartRepository;
        $this->giftCardSession = $giftCardSession;
        $this->giftCardManagementHelper = $giftCardManagementHelper;
        $this->_bazzarryPayhelper = $bazzarryPayHelper;
    }

    /**
     * @param int $cartId
     * @param string $giftCardCode
     * @return bool
     * @throws CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function applyToCart($cartId, $giftCardCode)
    {
        /** @var \MageWorx\GiftCards\Model\GiftCards $giftCard */
        $giftCard = $this->giftCardsRepository->getByCode($giftCardCode);

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->cartRepository->getActive($cartId);

        if (!$quote->getItemsCount()) {
            throw new NoSuchEntityException(__('Cart %0 doesn\'t contain products', $cartId));
        }

        if ($this->_bazzarryPayhelper->validateGiftCard(
            $giftCard,
            true,
            true,
            $quote->getStoreId(),
            $quote->getCustomerGroupId(),
            true
        )) {
            try {
                $this->giftCardSession->setActive(1);
                $this->giftCardManagementHelper->setSessionVars($giftCard);
                $this->_bazzarryPayhelper->discount();
            } catch (\Exception $e) {
                throw new CouldNotSaveException(__('Could not add gift card code'));
            }

            return true;
        }

        return false;
    }
}
