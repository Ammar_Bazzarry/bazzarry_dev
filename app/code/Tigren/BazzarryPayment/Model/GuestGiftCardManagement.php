<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\BazzarryPayment\Model;

use Magento\Quote\Model\QuoteIdMaskFactory;
use Tigren\Bazzarrypayment\Api\GiftCardManagementInterface;

class GuestGiftCardManagement implements \Tigren\BazzarryPayment\Api\GuestGiftCardManagementInterface
{
    /**
     * @var QuoteIdMaskFactory
     */
    private $quoteIdMaskFactory;

    /**
     * @var GiftCardManagementInterface
     */
    private $giftCardManagement;

    /**
     * GuestGiftCardManagement constructor.
     *
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param GiftCardManagementInterface $giftCardManagement
     */
    public function __construct(
        QuoteIdMaskFactory $quoteIdMaskFactory,
        GiftCardManagementInterface $giftCardManagement
    )
    {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->giftCardManagement = $giftCardManagement;
    }

    /**
     * @param string $cartId
     * @param string $giftCardCode
     * @return bool
     */
    public function applyToCart($cartId, $giftCardCode)
    {
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        return $this->giftCardManagement->applyToCart($quoteIdMask->getQuoteId(), $giftCardCode);
    }

}
