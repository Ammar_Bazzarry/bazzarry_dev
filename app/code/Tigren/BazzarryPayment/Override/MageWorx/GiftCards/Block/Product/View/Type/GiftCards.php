<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\BazzarryPayment\Override\MageWorx\GiftCards\Block\Product\View\Type;


class GiftCards extends \MageWorx\GiftCards\Block\Product\View\Type\GiftCards
{
    protected $_priceHelper;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        \Magento\Directory\Model\Currency $currency,
        \MageWorx\GiftCards\Helper\Data $helper,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        array $data = []
    )
    {
        $this->_priceHelper = $priceHelper;
        parent::__construct($context, $arrayUtils, $currency, $helper, $checkoutHelper, $data);
    }

    /**
     * @return string
     */
    public function getGiftCardPrice()
    {
        $from = '';
        $to = '';

        $min = $this->getMinPrice();
        $max = $this->getMaxPrice();

        if ($min == $max) {
            return $this->checkoutHelper->formatPrice($min);
        }

        if ($min !== null) {
            $from = __('From ');
        }
        if ($max) {
            $to = $from ? __(' to ') : __('To ');
        }

        return $from . $this->_priceHelper->currency($min, true, false) .
            $to . $this->_priceHelper->currency($max, true, false);
    }

    public function getPriceWithFormat($price)
    {
        return $this->_priceHelper->currency($price, true, false);
    }

    /**
     * @return string
     */
    public function getPlaceholder()
    {
        $product = $this->getProduct();
        $from = '';
        $to = '';
        $splitter = '';

        if ($product->getMageworxGcAllowOpenAmount()) {
            $from = $this->getPriceWithFormat($product->getMageworxGcOpenAmountMin());
            $to = $this->getPriceWithFormat($product->getMageworxGcOpenAmountMax());
        }

        if ($from && $to) {
            $splitter = ' - ';
        }

        return $from . $splitter . $to;
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPriceWithoutFormat($price){
        return $this->_priceHelper->currency($price, false, false);
    }

}
