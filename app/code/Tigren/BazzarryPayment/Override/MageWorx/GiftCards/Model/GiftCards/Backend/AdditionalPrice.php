<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\BazzarryPayment\Override\MageWorx\GiftCards\Model\GiftCards\Backend;

/**
 * Class AdditionalPrice
 * @package Tigren\BazzarryPayment\Override\MageWorx\GiftCards\Model\GiftCards\Backend
 */
class AdditionalPrice extends \MageWorx\GiftCards\Model\GiftCards\Backend\AdditionalPrice
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;

    /**
     * AdditionalPrice constructor.
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        \Magento\Framework\App\State $state
    )
    {
        $this->_state = $state;
    }

    /**
     * @param $object
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validate($object)
    {
        $attrCode = $this->getAttribute()->getAttributeCode();
        $value = $object->getData($attrCode);
        $areaCode = $this->_state->getAreaCode();

        if ($value) {
            if ($areaCode == 'vendors') {
                $prices = explode(';', $value);
                foreach ($prices as $price) {
                    if (!preg_match('/(^\d+(\.{0,1}\d{0,})+$)|^$/', $price)) {
                        throw new \Magento\Framework\Exception\LocalizedException(
                            __('Not correct value for Gift Card Amount. Examples: 100, 200.33, 300.56')
                        );
                    }
                }
            } else {
                foreach ($value as $price) {
                    if (!preg_match('/(^\d+(\.{0,1}\d{0,})+$)|^$/', $price['mageworx_gc_additional_price'])) {
                        throw new \Magento\Framework\Exception\LocalizedException(
                            __('Not correct value for Gift Card Amount. Examples: 100, 200.33, 300.56')
                        );
                    }
                }
            }
        }

        return true;
    }
}
