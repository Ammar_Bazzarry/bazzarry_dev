<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\BazzarryPayment\Override\MageWorx\GiftCards\Model;

use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\FactoryInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Model\StoreManagerInterface as StoreManager;
use MageWorx\GiftCards\Helper\Price as HelperPrice;

class GiftCards extends \MageWorx\GiftCards\Model\GiftCards
{

    public function sendNoOrder()
    {
        $templateVars = [
            'amount' => $this->getPriceWithCurrency($this->getCardAmount()),
            'code' => $this->getCardCode(),
            'email-to' => $this->getMailTo(),
            'email-from' => $this->getMailFrom(),
            'recipient' => $this->getMailToEmail(),
            'email-message' => nl2br($this->getMailMessage()),
            'store-phone' => $this->helper->getStorePhone(),
            'picture' => $this->getGiftCardPicture(),
            'alert-days' => $this->helper->getAlertDays()
        ];

        $this->transportBuilder->setTemplateIdentifier($this->helper->getEmailTemplateIdentifier());
        $this->transportBuilder->setTemplateVars($templateVars);
        $this->transportBuilder->setFrom('general');
        $this->transportBuilder->addTo($this->getMailToEmail(), $this->getMailTo());
        $this->transportBuilder->setTemplateOptions(
            [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $this->storeManager->getDefaultStoreView()->getId(),
            ]
        );

        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
    }


    /**
     * @param \Magento\Sales\Model\Order $order
     * @return $this
     */
    protected function prepareEmailCard($order)
    {
        $templateVars = [
            'amount' => $this->getPriceWithCurrency($this->getCardAmount()),
            'code' => $this->getCardCode(),
            'email-to' => $this->getMailTo(),
            'email-from' => $this->getMailFrom(),
            'recipient' => $this->getMailToEmail(),
            'email-message' => nl2br($this->getMailMessage()),
            'store-phone' => $this->helper->getStorePhone(),
            'picture' => $this->getGiftCardPicture($order->getStoreId()),
        ];

        $this->transportBuilder->setTemplateIdentifier($this->helper->getEmailTemplateIdentifier());
        $this->transportBuilder->setTemplateVars($templateVars);
        $this->transportBuilder->setFrom('general');
        $this->transportBuilder->addTo($this->getMailToEmail(), $this->getMailTo());
        $this->transportBuilder->setTemplateOptions(
            [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $order->getStoreId()
            ]
        );

        return $this;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return $this
     */
    protected function preparePrintCard($order)
    {
        $templateVars = [
            'amount' => $this->getPriceWithCurrency($this->getCardAmount()),
            'code' => $this->getCardCode(),
            'email-to' => $this->getMailTo(),
            'email-from' => $this->getMailFrom(),
            'recipient' => $order->getCustomerEmail(),
            'email-message' => nl2br($this->getMailMessage()),
            'store-phone' => $this->helper->getStorePhone(),
            'picture' => $this->getGiftCardPicture($order->getStoreId()),
            'link' => $this->urlBuilder->getBaseUrl() . 'mageworx_giftcards/giftcards/printCard/code/' . $this->getCardCode(),
            'customer-name' => $order->getCustomerName()
        ];

        $this->transportBuilder->setTemplateIdentifier($this->helper->getPrintTemplateIdentifier());
        $this->transportBuilder->setTemplateVars($templateVars);
        $this->transportBuilder->setFrom('general');
        $this->transportBuilder->addTo($order->getCustomerEmail(), $this->getMailTo());
        $this->transportBuilder->setTemplateOptions(
            [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $order->getStoreId()
            ]
        );

        return $this;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return $this
     */
    protected function prepareOfflineCard($order)
    {
        $templateVars = [
            'amount' => $this->getPriceWithCurrency($this->getCardAmount()),
            'code' => $this->getCardCode(),
            'email-to' => $this->getMailTo(),
            'email-from' => $this->getMailFrom(),
            'recipient' => $order->getCustomerEmail(),
            'email-message' => nl2br($this->getMailMessage()),
            'store-phone' => $this->helper->getStorePhone(),
            'picture' => $this->getGiftCardPicture($order->getStoreId()),
        ];

        $this->transportBuilder->setTemplateIdentifier($this->helper->getOfflineTemplateIdentifier());
        $this->transportBuilder->setTemplateVars($templateVars);
        $this->transportBuilder->setFrom('general');
        $this->transportBuilder->addTo($order->getCustomerEmail(), $this->getMailTo());
        $this->transportBuilder->setTemplateOptions(
            [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $order->getStoreId()
            ]
        );

        return $this;
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendExpiredCard()
    {
        if ($this->getMailToEmail()) {
            $templateVars = [
                'amount' => $this->getPriceWithCurrency($this->getCardAmount()),
                'balance' => $this->getPriceWithCurrency($this->getCardBalance()),
                'code' => $this->getCardCode(),
                'email-to' => $this->getMailTo(),
                'email-from' => $this->getMailFrom(),
                'email-message' => nl2br($this->getMailMessage()),
                'store-phone' => $this->helper->getStorePhone()
            ];

            $this->transportBuilder->setTemplateIdentifier($this->helper->getExpiredTemplateIdentifier());
            $this->transportBuilder->setTemplateVars($templateVars);
            $this->transportBuilder->setFrom('general');
            $this->transportBuilder->addTo($this->getMailToEmail(), $this->getMailTo());
            $this->transportBuilder->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $this->storeManager->getDefaultStoreView()->getId(),
                ]
            );

            $transport = $this->transportBuilder->getTransport();
            $transport->sendMessage();
        }

        return $this;
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendCardExpirationAlert()
    {
        if ($this->getMailToEmail()) {
            $templateVars = [
                'amount' => $this->getPriceWithCurrency($this->getCardAmount()),
                'balance' => $this->getPriceWithCurrency($this->getCardBalance()),
                'code' => $this->getCardCode(),
                'email-to' => $this->getMailTo(),
                'email-from' => $this->getMailFrom(),
                'email-message' => nl2br($this->getMailMessage()),
                'store-phone' => $this->helper->getStorePhone(),
                'alert-days' => $this->helper->getAlertDays(),
                'expire-date' => $this->getExpireDate()
            ];

            $this->transportBuilder->setTemplateIdentifier($this->helper->getExpirationAlertTemplateIdentifier());
            $this->transportBuilder->setTemplateVars($templateVars);
            $this->transportBuilder->setFrom('general');
            $this->transportBuilder->addTo($this->getMailToEmail(), $this->getMailTo());
            $this->transportBuilder->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $this->storeManager->getDefaultStoreView()->getId(),
                ]
            );

            $transport = $this->transportBuilder->getTransport();
            $transport->sendMessage();
        }

        return $this;
    }

    public function getPriceWithCurrency($price)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of Object Manager
        /** @var \Magento\Directory\Model\Currency $currencyModel */
        $currencyModel = $objectManager->create('Magento\Directory\Model\Currency');
        $currencyCode = $this->getCardCurrency();
        $currency = $currencyModel->load($currencyCode);
        $currencySymbol = $currency->getCurrencySymbol();
        $formattedPrice = $currencyModel->format($price, ['symbol' => $currencySymbol], true, false);
        return $formattedPrice;
    }
}
