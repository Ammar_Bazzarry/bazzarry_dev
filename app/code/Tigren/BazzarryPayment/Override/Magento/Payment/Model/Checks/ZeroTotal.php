<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Tigren\BazzarryPayment\Override\Magento\Payment\Model\Checks;

use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Model\Quote;

/**
 * Checks that order total is meaningful
 *
 * @api
 * @since 100.0.2
 */
class ZeroTotal extends \Magento\Payment\Model\Checks\ZeroTotal
{
    /**
     * Check whether payment method is applicable to quote
     * Purposed to allow use in controllers some logic that was implemented in blocks only before
     *
     * @param MethodInterface $paymentMethod
     * @param \Magento\Quote\Model\Quote $quote
     * @return bool
     */
    public function isApplicable(MethodInterface $paymentMethod, Quote $quote)
    {
        $isFree = !($quote->getBaseGrandTotal() < 0.0001 && ($paymentMethod->getCode() != 'free'));
        $isGiftCard = !($quote->getBaseGrandTotal() < 0.0001 && ($paymentMethod->getCode() != 'bazzarrypayment'));
        return $isGiftCard || $isFree;
    }
}
