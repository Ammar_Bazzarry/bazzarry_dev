<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\BazzarryPayment\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use MageWorx\GiftCards\Helper\Price as HelperPrice;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class Data
 *
 * @package Tigren\BazzarryPayment\Helper
 */
class Data extends AbstractHelper
{
    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \MageWorx\GiftCards\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var HelperPrice
     */
    protected $helperPrice;

    /**
     * @var \MageWorx\GiftCards\Model\Session
     */
    protected $giftcardsSession;

    /**
     * @var \MageWorx\GiftCards\Model\ResourceModel\GiftCards\CollectionFactory
     */
    protected $giftcardsCollection;

    /**
     * @var array
     */
    protected $cards;

    /**
     * @var \Magento\Quote\Model\Quote\Address\Total
     */
    protected $totals;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $_priceHelper;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    protected $_orderFactory;


    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        \MageWorx\GiftCards\Helper\Data $helper,
        \Magento\Checkout\Model\Cart $cart,
        HelperPrice $helperPrice,
        \MageWorx\GiftCards\Model\Session $session,
        \MageWorx\GiftCards\Model\ResourceModel\GiftCards\CollectionFactory $giftcardsCollection,
        \Magento\Quote\Model\Quote\Address\Total $total,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        StoreManagerInterface $storeManager
    )
    {
        parent::__construct($context);
        $this->priceCurrency = $priceCurrency;
        $this->objectManager = $objectManager;
        $this->storeManager = $storeManager;
        $this->_helper = $helper;
        $this->_cart = $cart;
        $this->helperPrice = $helperPrice;
        $this->giftcardsSession = $session;
        $this->giftcardsCollection = $giftcardsCollection;
        $this->totals = $total;
        $this->_priceHelper = $priceHelper;
        $this->customerSession = $customerSession;
        $this->_orderFactory = $orderFactory;
    }

    /**
     * @param \MageWorx\GiftCards\Model\GiftCards $giftCard
     * @param bool $statusCheck
     * @param bool $expireDateCheck
     * @param bool $storeViewCheck
     * @param bool $customerGroupCheck
     * @param bool $balanceCheck
     * @return bool
     * @throws \Magento\Framework\Exception\State\InvalidTransitionException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws LocalizedException
     */
    public function validateGiftCard(
        $giftCard,
        $statusCheck = true,
        $expireDateCheck = true,
        $storeViewCheck = false,
        $customerGroupCheck = false,
        $balanceCheck = true
    )
    {
        if (!$giftCard->getId()) {
            throw new LocalizedException(
                __('Gift Card %0 is not valid.', $giftCard->getCardCode())
            );
        }

        if ($statusCheck && $giftCard->getCardStatus() != self::STATUS_ACTIVE) {
            throw new LocalizedException(
                __(
                    'Gift Card %0 is not enabled. Сurrent status: %1',
                    $giftCard->getCardCode(),
                    $giftCard->getCardStatusLabel()
                )
            );
        }

        if ($expireDateCheck && $this->_helper->isExpired($giftCard)) {
            throw new LocalizedException(
                __('Gift Card %0 is expired.', $giftCard->getCardCode())
            );
        }

        if (is_numeric($storeViewCheck)) {
            $storeId = $this->storeManager->getStore($storeViewCheck)->getId();

            if (!in_array(0, $giftCard->getStoreviewIds())
                && !in_array($storeId, $giftCard->getStoreviewIds())
            ) {
                throw new LocalizedException(
                    __(
                        'Gift Card %0 can not be used in this Store View.',
                        $giftCard->getCardCode()
                    )
                );
            }
        }

        if (is_numeric($customerGroupCheck)) {
            if (!in_array($customerGroupCheck, $giftCard->getCustomerGroupIds())) {
                throw new LocalizedException(
                    __(
                        'Gift Card %0 can not be applied by this Customer Group.',
                        $giftCard->getCardCode()
                    )
                );
            }
        }

        $quote = $this->getQuote();
        $total = $quote->getGrandTotal();

        if ($balanceCheck) {
            if ($this->getCardCurrencyBalance($giftCard) <= $total) {
                throw new LocalizedException(
                    __('Gift Card %0 not enough balance.', $giftCard->getCardCode())
                );
            }
        }

        return true;
    }

    /**
     * @param \MageWorx\GiftCards\Model\GiftCards $giftCard
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCardCurrencyBalance($giftCard)
    {
        return $this->helperPrice->convertCardCurrencyToStoreCurrency(
            $giftCard->getCardBalance(),
            $this->storeManager->getStore(),
            $giftCard->getCardCurrency()
        );
    }

    public function convertBaseToCartCurrency($price){
        $rate                = $this->storeManager->getStore()->getBaseCurrency()->getRate('USD');
        $cartBalance = $price * $rate;

        return round($cartBalance,2);
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCurrentCurrency()
    {
        return $this->storeManager->getStore()->getCurrentCurrencyCode();
    }

    /**
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        return $this->_cart->getQuote();
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this
     * @throws \Exception
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function discount()
    {
        $quote = $this->getQuote();
        $store = $this->storeManager->getStore($quote->getStoreId());
        $giftcardsIds = $this->giftcardsSession->getGiftCardsIds();
        $quote->setUseGiftCard(false);
        $quote->setMageworxGiftcardsDescription(null);
        $quote->setMageworxGiftcardsAmount(0);
        $quote->setBaseMageworxGiftcardsAmount(0);

        if ($this->giftcardsSession->getActive() && $giftcardsIds) {
            $cards = $this->giftcardsCollection->create()->addFieldToFilter(
                'card_id',
                ['in' => array_keys($giftcardsIds)]
            )->load();
            if (!count($cards)) {
                $this->giftcardsSession->setActive(0);
                $this->giftcardsSession->setFrontOptions(null);
                $this->giftcardsSession->setGiftCardsIds(null);

                return $this;
            }

            $quoteSubtotal = $quote->getGrandTotal();
            $quoteBaseSubtotal = $quote->getBaseGrandTotal();
            $giftcardsTotal = 0;
            $giftcardsBaseTotal = 0;

            $quote->setUseGiftCard(true);
            foreach ($cards as $card) {
                $this->cards[$card->getId()] = [
                    'balance' => $card->getCardBalance(),
                    'code' => $card->getCardCode(),
                    'card_currency' => $card->getCardCurrency()
                ];
            }

            $frontData = [];
            foreach ($this->cards as $id => $card) {
                if ($quoteSubtotal > 0) {
                    $cardStoreBalance = $this->helperPrice->convertCardCurrencyToStoreCurrency(
                        $card['balance'],
                        $store,
                        $card['card_currency']
                    );
                    $cardBaseBalance = $this->helperPrice->convertCurrencyToBaseCurrency(
                        $card['balance'],
                        $store,
                        $card['card_currency']
                    );
                    $cardApplied = min($quoteSubtotal, $cardStoreBalance);
                    $cardBaseApplied = min($quoteBaseSubtotal, $cardBaseBalance);
                    $quoteSubtotal -= $cardApplied;
                    $quoteBaseSubtotal -= $cardBaseApplied;
                    $giftcardsTotal += $cardApplied;
                    $giftcardsBaseTotal += $cardBaseApplied;
                    $baseRemaining = $cardBaseBalance - $cardBaseApplied;
                    $currentCurrency = $store->getCurrentCurrencyCode();
                    $baseCurrency = $store->getBaseCurrencyCode();
                    if ($card['card_currency'] == $baseCurrency){
                        $cardRemaining = $baseRemaining;
                    }else{
                        if ($card['card_currency'] == $currentCurrency){
                            $cardRemaining = $cardStoreBalance - $cardApplied;
                        }
                        else{
                            $cardRemaining = $this->convertBaseToCartCurrency(
                                $baseRemaining,
                                $store,
                                $card['card_currency']
                            );
                        }
                    }

                    $frontData[$id]['applied'] = $cardApplied;
                    $frontData[$id]['remaining'] = $cardRemaining;
                    $frontData[$id]['card_remaining'] = $cardRemaining;
                    $frontData[$id]['code'] = $card['code'];
                    $frontData[$id]['card_currency'] = $card['card_currency'];
                }
            }

            $descriptionArray = [];

            foreach ($this->cards as $key => $card) {
                $descriptionArray[$key] = $card['code'];
            }

            $this->totals->setTotalAmount('mageworx_giftcards', -$giftcardsTotal);
            $this->totals->setBaseTotalAmount('mageworx_giftcards', -$giftcardsBaseTotal);

            $quote->setMageworxGiftcardsAmount(-$giftcardsTotal);
            $quote->setBaseMageworxGiftcardsAmount(-$giftcardsBaseTotal);
            $quote->setMageworxGiftcardsDescription(' Gift Card (' . implode(',', $descriptionArray) . ')');

            $quote->setSubtotalWithDiscount(0);
            $quote->setBaseSubtotalWithDiscount(0);

            $quote->setGrandTotal(0);
            $quote->setBaseGrandTotal(0);

            $quote->setGiftCardsIds($this->cards);
            $quote->save();
            $this->giftcardsSession->setFrontOptions($frontData);
        }

        return $this;
    }

    /**
     * @param $price
     * @return float|string
     */
    public function getPriceWithFormat($price)
    {
        return $this->_priceHelper->currency($price, true, false);
    }

    /**
     * @param $_giftcard
     * @return bool
     */
    public function allowViewOrder($_giftcard)
    {
        $orderid = $_giftcard->getOrderId();
        $customerId = $_giftcard->getCustomerId();
        if ($orderid && $customerId){
            if ($customerId == $this->customerSession->getId()){
                return true;
            }
        }
        return false;
    }

    /**
     * @param \MageWorx\GiftCards\Model\Order $order
     */
    public function getOrderCurrencyCode($order){
        $orderId = $order->getOrderId();
        $mainOrder = $this->_orderFactory->create()->load($orderId);
        if ($mainOrder->getId()){
            return $mainOrder->getOrderCurrencyCode();
        }
        return '';
    }

}
