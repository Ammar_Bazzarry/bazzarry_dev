<?php

namespace Tigren\VendorsReview\Override\Vnecoms\VendorsReview\Block\Customer\Account;

/**
 * Class Notification
 * @package Tigren\Vendors\Override\Vnecoms\Vendors\Block\Vendors\Customer\Account
 */
class Notification extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Vnecoms\VendorsReview\Model\Review\LinkFactory
     */
    protected $_reviewLinkFactory;

    /**
     * @var \Vnecoms\VendorsReview\Model\ResourceModel\Review\Link\Collection
     */
    protected $_reviewLinkCollection;

    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Vnecoms\VendorsPage\Helper\Data
     */
    protected $_pageHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Vnecoms\VendorsReview\Model\Review\LinkFactory $reviewLinkFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Vnecoms\VendorsReview\Model\Review\LinkFactory $reviewLinkFactory,
        \Vnecoms\VendorsPage\Helper\Data $pageHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_reviewLinkFactory = $reviewLinkFactory;
        $this->_customerSession = $customerSession;
        $this->_pageHelper = $pageHelper;
    }

    /**
     * Get Review Link Collection.
     *
     * @return \Vnecoms\VendorsReview\Model\ResourceModel\Review\Link\Collection
     */
    public function getLastReviewLink()
    {
        if (!$this->_reviewLinkCollection) {
            $this->_reviewLinkCollection = $this->_reviewLinkFactory->create()->getCollection()
                ->addFieldToFilter('customer_id', $this->_customerSession->getCustomerId())
                ->addFieldToFilter('show_review_link', 1)
                ->setPageSize(1)
                ->setCurPage(1);
            $this->_reviewLinkCollection->getSelect()->order(array('order_id DESC'));
        }
        return $this->_reviewLinkCollection->getFirstItem();
    }

    /**
     * @param $vendorId
     * @param $itemId
     * @return string
     */
    public function getReviewLinkUrl($vendorId, $itemId)
    {
        return $this->_pageHelper->getUrl($vendorId, 'reviews/form', ['item' => $itemId]);
    }
}
