<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsReview\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProcessShipment implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * @var \Vnecoms\VendorsReview\Model\Review\LinkFactory
     */
    protected $_reviewLinkFactory;


    public function __construct(
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Vnecoms\VendorsReview\Model\Review\LinkFactory $reviewLinkFactory
    ) {
        $this->_eventManager = $eventManager;
        $this->_reviewLinkFactory = $reviewLinkFactory;
    }

    /**
     * Add multiple vendor order row for each vendor.
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return self
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\model\Order\Shipment $shipment */
        $shipment = $observer->getData('shipment');
        $order = $shipment->getOrder();
        $invoices = $order->getInvoiceCollection();
        $customerId = $order->getCustomerId();
        if (!$customerId || $order->getStatus() != 'complete') {
            return;
        }
        foreach ($invoices as $invoice){
            /*Group invoice item by  vendor*/
            /** @var \Magento\Sales\Model\Order\Invoice\Item $item */
            foreach ($invoice->getAllItems() as $item) {
                /** @var \Magento\Sales\Model\Order\Item $orderItem */
                $orderItem = $item->getOrderItem();
                if ($orderItem->getParentItemId()) {
                    continue;
                }

                if ($vendorId = $orderItem->getVendorId()) {
                    $link = $this->_reviewLinkFactory->create();
                    $collection = $link->getCollection()->addFieldToFilter('order_item_id', $orderItem->getId());
                    if ($collection->count()) {
                        continue;
                    }

                    $link->setData([
                        'vendor_id' => $vendorId,
                        'customer_id' => $customerId,
                        'order_id' => $order->getId(),
                        'order_item_id' => $orderItem->getId(),
                        'show_review_link' => 1,
                        'can_review' => 1,
                    ])->save();
                }
            }
        }

        return $this;
    }
}
