<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsPriceComparison\Model;



class Update
{
    protected $_helper;

    public function __construct(
        \Tigren\VendorsPriceComparison\Helper\Data $helper
    ) {
        $this->_helper = $helper;
    }

    /**
     *
     */
    public function execute()
    {
        $this->_helper->updateMainproduct();
    }
}
