<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsPriceComparison\Block;

use IntlDateFormatter;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\CatalogInventory\Helper\Stock;
use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Checkout\Helper\Cart;
use Magento\Cms\Model\Template\Filter;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem\Directory\ReadInterface;
use Magento\Framework\Locale\FormatInterface;
use Magento\Framework\Url\EncoderInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\Config;
use Magento\MediaStorage\Helper\File\Storage\Database;
use Vnecoms\Vendors\Helper\Image;
use Vnecoms\Vendors\Model\VendorFactory;
use Vnecoms\VendorsConfig\Helper\Data as ConfigHelper;
use Magento\Framework\App\Filesystem\DirectoryList;
use Vnecoms\VendorsProduct\Helper\Data as ProductHelper;
use Vnecoms\Vendors\Helper\Data as VendorHelper;
use Vnecoms\VendorsPriceComparison\Helper\Data as PriceComparisonHelper;
use Vnecoms\VendorsSales\Model\ResourceModel\OrderFactory;
use Magento\Catalog\Model\ProductFactory;
use Zend_Json;

/**
 * Class Vendor
 * @package Tigren\VendorsPriceComparison\Block
 */
class Vendor extends Template
{
    /**
     * @var array
     */
    protected $jsLayout;

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var array|LayoutProcessorInterface[]
     */
    protected $layoutProcessors;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var Config
     */
    protected $catalogConfig;

    /**
     * @var Collection
     */
    protected $_productCollection;

    /**
     * Catalog product visibility
     *
     * @var Visibility
     */
    protected $productVisibility;

    /**
     * @var VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var ConfigHelper
     */
    protected $_configHelper;

    /**
     * @var Database
     */
    protected $_fileStorageDatabase;

    /**
     * @var ReadInterface
     */
    protected $mediaDirectory;

    /**
     * @var Image
     */
    protected $_imageHelper;

    /**
     * @var ProductHelper
     */
    protected $_productHelper;

    /**
     * @var VendorHelper
     */
    protected $_vendorHelper;

    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @var OrderFactory
     */
    protected $_orderResourceFactory;

    /**
     * @var Cart
     */
    protected $_cartHelper;

    /**
     * @var PriceComparisonHelper
     */
    protected $_priceComparisonHelper;

    /**
     * @var EncoderInterface
     */
    protected $urlEncoder;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var FormatInterface
     */
    protected $_localeFormat;

    /**
     * @var Stock
     */
    protected $stockHelper;

    /**
     * The list of loaded vendors
     * @var array
     */
    protected $_vendors = [];

    /**
     * The main product ID
     * @var int
     */
    protected $_mainProductId;

    /**
     * @var \Vnecoms\VendorsReview\Model\ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * Vendor constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param ProductFactory $productFactory
     * @param CollectionFactory $productCollectionFactory
     * @param Visibility $productVisibility
     * @param Config $catalogConfig
     * @param VendorFactory $vendorFactory
     * @param ConfigHelper $configHelper
     * @param Database $fileStorageDatabase
     * @param Image $imageHelper
     * @param ProductHelper $productHelper
     * @param VendorHelper $vendorHelper
     * @param PriceComparisonHelper $priceComparisonHelper
     * @param Filter $filter
     * @param OrderFactory $orderResourceFactory
     * @param Cart $cartHelper
     * @param EncoderInterface $urlEncoder
     * @param FormKey $formKey
     * @param FormatInterface $localeFormat
     * @param Stock $stockHelper
     * @param array $layoutProcessors
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        ProductFactory $productFactory,
        CollectionFactory $productCollectionFactory,
        Visibility $productVisibility,
        Config $catalogConfig,
        VendorFactory $vendorFactory,
        ConfigHelper $configHelper,
        Database $fileStorageDatabase,
        Image $imageHelper,
        ProductHelper $productHelper,
        VendorHelper $vendorHelper,
        PriceComparisonHelper $priceComparisonHelper,
        Filter $filter,
        OrderFactory $orderResourceFactory,
        Cart $cartHelper,
        EncoderInterface $urlEncoder,
        FormKey $formKey,
        FormatInterface $localeFormat,
        Stock $stockHelper,
        \Vnecoms\VendorsReview\Model\ReviewFactory $reviewFactory,
        array $layoutProcessors = [],
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->productFactory = $productFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->productVisibility = $productVisibility;
        $this->_coreRegistry = $coreRegistry;
        $this->catalogConfig = $catalogConfig;
        $this->_vendorFactory = $vendorFactory;
        $this->_configHelper = $configHelper;
        $this->_fileStorageDatabase = $fileStorageDatabase;
        $this->_mediaDirectory = $context->getFilesystem()->getDirectoryRead(DirectoryList::MEDIA);
        $this->_imageHelper = $imageHelper;
        $this->_productHelper = $productHelper;
        $this->_vendorHelper = $vendorHelper;
        $this->_priceComparisonHelper = $priceComparisonHelper;
        $this->_filter = $filter;
        $this->_orderResourceFactory = $orderResourceFactory;
        $this->_cartHelper = $cartHelper;
        $this->urlEncoder = $urlEncoder;
        $this->formKey = $formKey;
        $this->_localeFormat = $localeFormat;
        $this->_reviewFactory = $reviewFactory;
        $this->jsLayout = isset($data['jsLayout']) && is_array($data['jsLayout']) ? $data['jsLayout'] : [];
        $this->layoutProcessors = $layoutProcessors;
        $this->stockHelper = $stockHelper;
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    public function getJsLayout()
    {
        $this->jsLayout['components']['vendor']['products'] = $this->getProducts();
        $this->jsLayout['components']['vendor']['page_size'] = $this->_priceComparisonHelper->getShowingNumber();
        $this->jsLayout['components']['vendor']['show_country_filter'] = $this->_priceComparisonHelper->showCountryFilter();

        $this->jsLayout['components']['vendor']['children']['vendor_info']['config']['show_address'] = $this->_priceComparisonHelper->canShowAddress();
        $this->jsLayout['components']['vendor']['children']['vendor_info']['config']['show_sales_count'] = $this->_priceComparisonHelper->canShowSalesCount();
        $this->jsLayout['components']['vendor']['children']['vendor_info']['config']['show_joined_date'] = $this->_priceComparisonHelper->canShowJoinedDate();
        $this->jsLayout['components']['vendor']['children']['vendor_description']['config']['show_address'] = $this->_priceComparisonHelper->canShowAddress();
        $this->jsLayout['components']['vendor']['children']['vendor_description']['config']['show_sales_count'] = $this->_priceComparisonHelper->canShowSalesCount();
        $this->jsLayout['components']['vendor']['children']['vendor_description']['config']['show_joined_date'] = $this->_priceComparisonHelper->canShowJoinedDate();

        $this->jsLayout['components']['vendor']['children']['product_price']['priceFormat'] = $this->getPriceFormat();
        $this->jsLayout['components']['vendor']['children']['product_price']['basePriceFormat'] = $this->getBasePriceFormat();
        $this->jsLayout['components']['vendor']['children']['product_price']['exchangeRate'] = $this->getCurrentCurrencyRate();

        foreach ($this->layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }
        return Zend_Json::encode($this->jsLayout);
    }

    /**
     * Get product collection
     *
     * @return Collection
     */
    public function getProductCollection()
    {
        $currentProductId = $this->getProduct()->getId();

        if ($this->_productCollection === null) {
            if ($this->_mainProductId) {
                $this->_productCollection = $this->_productCollectionFactory->create()
                    ->addAttributeToSelect('vendor_id')
                    ->addAttributeToFilter('entity_id', ['neq' => $currentProductId])
                    ->addAttributeToFilter([
                        ['attribute' => 'select_from_product_id', 'eq' => $this->_mainProductId],
                        ['attribute' => 'entity_id', 'eq' => $this->_mainProductId]
                    ])
                    ->addAttributeToFilter('approval', ['in' => $this->_productHelper->getAllowedApprovalStatus()]);
            } else {
                $this->_productCollection = $this->_productCollectionFactory->create()
                    ->addAttributeToSelect('vendor_id')
                    ->addAttributeToFilter('select_from_product_id', $currentProductId)
                    ->addAttributeToFilter('approval', ['in' => $this->_productHelper->getAllowedApprovalStatus()]);
            }

            $this->_productCollection->addAttributeToSelect($this->catalogConfig->getProductAttributes())
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->setVisibility($this->productVisibility->getVisibleInCatalogIds());

            $this->stockHelper->addInStockFilterToCollection($this->_productCollection);
        }

        return $this->_productCollection;
    }

    /**
     * Get products
     *
     * @throws \Exception
     */
    public function getProducts()
    {
        $products = [];
        foreach ($this->getProductCollection() as $product) {
            $vendorId = $product->getVendorId();

            if (!$vendorId) {
                continue;
            }

            $productData = $this->_prepareProductData($product);
            if ($productData === false) {
                continue;
            }
            if ($productData['pc_vendor']['status'] != \Vnecoms\Vendors\Model\Vendor::STATUS_APPROVED) {
                continue;
            }
            $products[] = $productData;
        }

        return $products;
    }

    /**
     * Prepare product data
     *
     * @param Product $product
     * @return bool|mixed
     * @throws \Exception
     */
    protected function _prepareProductData(Product $product)
    {
        $vendorId = $product->getVendorId();

        if (!$vendorId) {
            return false;
        }

        if (!isset($this->_vendors[$vendorId])) {
            $vendor = $this->_vendorFactory->create();
            $vendor->load($vendorId);
            if (!$vendor->getId()) {
                return false;
            }

            /* Add vendor home page URL if the homepage is installed*/
            if (class_exists('Vnecoms\VendorsPage\Helper\Data')) {
                $om = ObjectManager::getInstance();
                $helper = $om->create('Vnecoms\VendorsPage\Helper\Data');
                $vendor->setData('pc_home_page', $helper->getUrl($vendor));
            }
            $vendor->setData('pc_title',
                $this->_configHelper->getVendorConfig('general/store_information/name', $vendorId));
            $vendor->setData('pc_description',
                $this->_configHelper->getVendorConfig('general/store_information/short_description', $vendorId));
            $vendor->setData('pc_logo_url', $this->getLogoUrl($vendor));
            $vendor->setData('pc_logo_width', $this->getLogoWidth());
            $vendor->setData('pc_logo_height', $this->getLogoHeight());
            $vendor->setData('pc_address', $this->getAddress($vendor));
            $vendor->setData('pc_country_name', $vendor->getCountryName($this->_design->getLocale()));
            $vendor->setData('pc_sales_count', $this->getSalesCount($vendor));
            $vendor->setData('pc_vendor_id', $vendorId);
            $vendor->setData(
                'pc_joined_date',
                $this->formatDate($vendor->getCreatedAt(), IntlDateFormatter::MEDIUM)
            );
            $vendor->setData('pc_rating', $this->getRating($vendorId));
            $vendor->setData('pc_review_count', (int)$this->getReviewCount($vendorId));

            $this->_vendors[$vendorId] = $vendor;
        }

        $productData = $product->getData();
        $productData['pc_product_url'] = $product->getProductUrl();
        $productData['pc_addtocart_url'] = $this->getAddToCartUrl($product);
        $productData['pc_vendor'] = $this->_vendors[$vendorId]->getData();

        return $productData;
    }

    /**
     * @param $vendorId
     * @return false|float
     */
    public function getRating($vendorId)
    {
        $reviewResource = $this->_reviewFactory->create()->getResource();
        $rating = $reviewResource->getAverageRating($vendorId);
        return round($rating * 100 / 5);
    }

    public function getReviewCount($vendorId){
        $reviewResource = $this->_reviewFactory->create()->getResource();
        return $reviewResource->getReviewCount($vendorId);
    }

    /**
     * Keep Transparency logo
     *
     * @return boolean
     */
    public function keepTransparencyLogo()
    {
        return $this->getData('keep_transparency');
    }

    /**
     * Get logo width
     *
     * @return int
     */
    public function getLogoWidth()
    {
        $logoWidth = $this->getData('logo_width');
        return $logoWidth ? $logoWidth : 75;
    }

    /**
     * Get logo height
     *
     * @return int
     */
    public function getLogoHeight()
    {
        $logoHeight = $this->getData('logo_height');
        return $logoHeight ? $logoHeight : 75;
    }

    /**
     * Get Logo URL by vendor
     *
     * @param \Vnecoms\Vendors\Model\Vendor $vendor
     * @return string
     */
    public function getLogoUrl(\Vnecoms\Vendors\Model\Vendor $vendor)
    {
        $scopeConfig = $this->_configHelper->getVendorConfig(
            'general/store_information/logo',
            $vendor->getId()
        );
        $basePath = 'ves_vendors/logo/';
        $path = $basePath . $scopeConfig;


        if ($scopeConfig && $this->checkIsFile($path)) {
            $this->_imageHelper->init($scopeConfig)
                ->setBaseMediaPath($basePath)
                ->keepTransparency($this->keepTransparencyLogo())
                ->backgroundColor([250, 250, 250])
                ->resize($this->getLogoWidth(), $this->getLogoHeight());
            return $this->_imageHelper->getUrl();
        }

        return $this->getViewFileUrl('Vnecoms_Vendors::images/no-logo.jpg');
    }

    /**
     * If DB file storage is on - find there, otherwise - just file_exists
     *
     * @param string $filename relative file path
     * @return bool
     */
    protected function checkIsFile($filename)
    {
        if ($this->_fileStorageDatabase->checkDbUsage() && !$this->_mediaDirectory->isFile($filename)) {
            $this->_fileStorageDatabase->saveFileToFilesystem($filename);
        }
        return $this->_mediaDirectory->isFile($filename);
    }

    /**
     * Get vendor address
     *
     * @param \Vnecoms\Vendors\Model\Vendor $vendor
     * @return string
     * @throws \Exception
     */
    public function getAddress(\Vnecoms\Vendors\Model\Vendor $vendor)
    {
        $template = $this->_vendorHelper->getAddressTemplate();
        $country = $vendor->getCountryName(
            $this->_design->getLocale()
        );
        $variables = [
            'street' => $vendor->getStreet(),
            'city' => $vendor->getCity(),
            'country' => $country,
            'region' => $vendor->getRegion(),
            'postcode' => $vendor->getPostcode(),
        ];
        return $this->_filter->setVariables($variables)->filter($template);
    }

    /**
     * Get sales count
     *
     * @param \Vnecoms\Vendors\Model\Vendor $vendor
     * @return number
     */
    public function getSalesCount(\Vnecoms\Vendors\Model\Vendor $vendor)
    {
        $resource = $this->_orderResourceFactory->create();
        return $resource->getSalesCount($vendor->getId());
    }

    /**
     * Retrieve url for direct adding product to cart
     *
     * @param Product $product
     * @param array $additional
     * @return string
     */
    public function getAddToCartUrl($product, $additional = [])
    {
        if ($this->getRequest()->getParam('wishlist_next')) {
            $additional['wishlist_next'] = 1;
        }

        $addUrlKey = ActionInterface::PARAM_NAME_URL_ENCODED;
        $addUrlValue = $this->_urlBuilder->getUrl('*/*/*', ['_use_rewrite' => true, '_current' => true]);
        $additional[$addUrlKey] = $this->urlEncoder->encode($addUrlValue);
        return $this->_cartHelper->getAddUrl($product, $additional);
    }

    /**
     * Get current product
     *
     * @return Product
     */
    public function getProduct()
    {
        $currentProduct = $this->_coreRegistry->registry('product');
        $mainProductId = $currentProduct->getData('select_from_product_id');
        if ($mainProductId) {
            $this->_mainProductId = $mainProductId;
        }
        return $currentProduct;
    }

    /**
     * (non-PHPdoc)
     * @see \Magento\Framework\View\Element\AbstractBlock::toHtml()
     */
    public function toHtml()
    {
        if (!$this->getProduct() || !$this->getProducts()) {
            return '';
        }

        return parent::toHtml();
    }

    /**
     * Get price format json.
     *
     * @return string
     */
    public function getPriceFormat()
    {
        return $this->_localeFormat->getPriceFormat();
    }

    /**
     * Get price format json.
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function getBasePriceFormat()
    {
        return $this->_localeFormat->getPriceFormat(null, $this->_storeManager->getStore()->getBaseCurrencyCode());
    }

    /**
     * Get current currency rate
     *
     * @return float
     * @throws NoSuchEntityException
     */
    public function getCurrentCurrencyRate()
    {
        return $this->_storeManager->getStore()->getCurrentCurrencyRate();
    }
}
