<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsPriceComparison\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;


class ProductSaveAfter implements ObserverInterface
{
    protected $_helper;

    protected $_stockItemRepository;

    public function __construct(
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Tigren\VendorsPriceComparison\Helper\Data $helper
    ) {
        $this->_stockItemRepository = $stockItemRepository;
        $this->_helper = $helper;
    }

    /**
     *
     */
    public function execute(EventObserver $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product*/
        $product = $observer->getProduct();
        if($product->getData('select_from_product_id') == 0){
            if(!$product->isInStock()){
                $this->_helper->processMainProduct($product);
            }
        }
    }
}
