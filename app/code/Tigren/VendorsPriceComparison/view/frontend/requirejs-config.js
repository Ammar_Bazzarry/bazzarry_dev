/*
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

var config = {
    map: {
        '*': {
            'Vnecoms_VendorsPriceComparison/js/vendor/addtocart': 'Tigren_VendorsPriceComparison/js/vendor/addtocart',
            'Vnecoms_VendorsPriceComparison/template/vendor/addtocart.html': 'Tigren_VendorsPriceComparison/template/vendor/addtocart.html'
        }
    }
};