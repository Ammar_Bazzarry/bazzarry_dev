<?php

namespace Tigren\VendorsPriceComparison\Override\Vnecoms\VendorsPriceComparison\Observer;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\CatalogInventory\Helper\Stock;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\UrlInterface;
use Vnecoms\VendorsProduct\Helper\Data;

/**
 * Class ProductViewPredispatch
 * @package Tigren\VendorsPriceComparison\Override\Vnecoms\VendorsPriceComparison\Observer
 */
class ProductViewPredispatch implements ObserverInterface
{
    /**
     * @var ActionFactory
     */
    protected $_actionFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var RedirectInterface
     */
    protected $redirect;

    /**
     * @var Data
     */
    protected $_productHelper;

    /**
     * @var CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var Stock
     */
    protected $stockHelper;

    /**
     * @param ActionFactory $actionFactory
     * @param ProductFactory $productFactory
     * @param RedirectInterface $redirect
     * @param Data $vendorsProductHelper
     * @param CollectionFactory $productCollectionFactory
     * @param Stock $stockHelper
     */
    public function __construct(
        ActionFactory $actionFactory,
        ProductFactory $productFactory,
        RedirectInterface $redirect,
        Data $vendorsProductHelper,
        CollectionFactory $productCollectionFactory,
        Stock $stockHelper
    ) {
        $this->_actionFactory = $actionFactory;
        $this->productFactory = $productFactory;
        $this->redirect = $redirect;
        $this->_productHelper = $vendorsProductHelper;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->stockHelper = $stockHelper;
    }

    /**
     * @param Observer $observer
     * @return ResponseInterface|ResultInterface|void
     * @throws NotFoundException
     */
    public function execute(Observer $observer)
    {
        $request = $observer->getRequest();
        $productId = $request->getParam('mpid') ?: $request->getParam('id');
        $vendorId = $request->getParam('vid');

        if ($request->getParam('vprocessed')) {
            return;
        }

        if ($vendorId) {
            /** @var Collection $relationProductCollection */
            $relationProductCollection = $this->_productCollectionFactory->create()
                ->addAttributeToFilter('vendor_id', $vendorId)
                ->addAttributeToFilter('select_from_product_id', $productId)
                ->addAttributeToFilter('approval', ['in' => $this->_productHelper->getAllowedApprovalStatus()])
                ->setPageSize(1)
                ->setCurPage(1);
            $currentProduct = $relationProductCollection->getFirstItem();
            if ($currentProduct && $currentProduct->getId()) {
                $request->setParam('vprocessed', true)
                    ->setPathInfo('/catalog/product/view/id/' . $currentProduct->getId());
                return $this->_actionFactory->create('Magento\Framework\App\Action\Forward')->execute();
            }
        }

        if ($request->getParam('mvprocessed')) {
            return;
        }
        $request->setParam('mvprocessed', true);

        $product = $this->productFactory->create()->load($productId);
        $mainProductId = $product->getData('select_from_product_id');
        if (!$product->getId() || (!$mainProductId && $product->getIsSalable())) {
            return;
        }

        $mainProductId = $mainProductId ?: $productId;

        /** @var Collection $relationProductCollection */
        $relationProductCollection = $this->_productCollectionFactory->create()
            ->addAttributeToFilter('select_from_product_id', $mainProductId)
            ->addAttributeToFilter('approval', ['in' => $this->_productHelper->getAllowedApprovalStatus()]);

        $relationProductCollection
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents();

        $this->stockHelper->addInStockFilterToCollection($relationProductCollection);

        $relationProductCollection->addAttributeToSort('price', 'ASC')
            ->setPageSize(1)
            ->setCurPage(1);

        /** @var Product $mainProduct */
        $mainProduct = $relationProductCollection->getFirstItem();
        if ($mainProduct && $mainProduct->getId()) {
            $request->setParam('mpid', $mainProductId)
                ->setPathInfo('/catalog/product/view/id/' . $mainProduct->getId());
            return $this->_actionFactory->create('Magento\Framework\App\Action\Forward')->execute();
        }
    }
}
