<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsPriceComparison\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

/**
 * Class Data
 * @package Tigren\VendorsPriceComparison\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var \Vnecoms\VendorsProduct\Helper\Data
     */
    protected $_productHelper;

    /**
     * Data constructor.
     * @param Context $context
     * @param ResourceConnection $resourceConnection
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        ResourceConnection $resourceConnection,
        \Vnecoms\VendorsProduct\Helper\Data $helper,
        CollectionFactory $collectionFactory

    ) {
        parent::__construct($context);
        $this->resource = $resourceConnection;
        $this->_collectionFactory = $collectionFactory;
        $this->_productHelper = $helper;
    }

    /**
     *
     */
    public function updateMainproduct()
    {
        $collection = $this->_collectionFactory->create();
        $collection->setFlag('has_stock_status_filter', true);

        if ($collection->isEnabledFlat()) {
            $collection->getSelect()->where('select_from_product_id = (?)', 0);
        }else{
            $collection->addAttributeToFilter('select_from_product_id', 0);
        }
        $collection->getSelect()->join(
            ['stock' => 'cataloginventory_stock_item'],
            'stock.product_id = e.entity_id',
            ''
        )->where('stock.is_in_stock = 0');


        foreach ($collection as $oldMainProduct) {
            $this->processMainProduct($oldMainProduct);
        }
    }

    public function processMainProduct($oldMainProduct)
    {
        $children = $this->_collectionFactory->create();
        $children->setFlag('has_stock_status_filter', true);
        $children->addFieldToSelect('*');
        if ($children->isEnabledFlat()) {
            $children->getSelect()->where('select_from_product_id = (?)', $oldMainProduct->getId());
        }else{
            $children->addAttributeToFilter('select_from_product_id', $oldMainProduct->getId());
        }
        $children->getSelect()->join(
            ['stock' => 'cataloginventory_stock_item'],
            'stock.product_id = e.entity_id',
            ['is_in_stock']
        );
        if (!$children->count()) return;
        $main = $oldMainProduct;
        foreach ($children as $p) {
            if (in_array($p->getData('approval'), $this->_productHelper->getAllowedApprovalStatus()) && $p->getData('is_in_stock') == 1) {
                $main = $p;
                $this->setSelectFromProductId($main, 0);
                break;
            }
        }
        foreach ($children as $p) {
            if ($p->getId() != $main->getId()) {
                $this->setSelectFromProductId($p, $main->getId());
            }
        }
        if ($main->getId() != $oldMainProduct->getId()) {
            $this->setSelectFromProductId($oldMainProduct, $main->getId());
        }

    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param $value
     */
    protected function setSelectFromProductId(\Magento\Catalog\Model\Product $product, $value)
    {
        $product->setData('select_from_product_id', $value)
            ->getResource()
            ->saveAttribute($product, 'select_from_product_id');
    }

}
