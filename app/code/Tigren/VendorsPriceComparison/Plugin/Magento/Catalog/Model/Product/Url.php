<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2020 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsPriceComparison\Plugin\Magento\Catalog\Model\Product;

use Closure;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Registry;
use Magento\Framework\Session\SidResolverInterface;

/**
 * Class Url
 * @package Tigren\VendorsPriceComparison\Plugin\Magento\Catalog\Model\Product
 */
class Url
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var SidResolverInterface
     */
    protected $sidResolver;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Url constructor.
     * @param RequestInterface $request
     * @param SidResolverInterface $sidResolver
     * @param Registry $registry
     */
    public function __construct(
        RequestInterface $request,
        SidResolverInterface $sidResolver,
        Registry $registry
    ) {
        $this->_request = $request;
        $this->sidResolver =  $sidResolver;
        $this->_coreRegistry = $registry;
    }

    /**
     * @param \Magento\Catalog\Model\Product\Url $subject
     * @param Closure $proceed
     * @param $product
     * @param null $useSid
     * @return mixed|string
     */
    public function aroundGetProductUrl(
        \Magento\Catalog\Model\Product\Url $subject,
        Closure $proceed,
        $product,
        $useSid = null
    ) {
        $currentVendor = $this->_coreRegistry->registry('current_vendor');
        if ($currentVendor) {
            $params = [
                '_query' => [
                    'vid' => $currentVendor->getId()
                ]
            ];

            if ($useSid === null) {
                $useSid = $this->sidResolver->getUseSessionInUrl();
            }
            if (!$useSid) {
                $params['_nosid'] = true;
            }

            return $subject->getUrl($product, $params);
        }

        return $proceed($product, $useSid);
    }
}
