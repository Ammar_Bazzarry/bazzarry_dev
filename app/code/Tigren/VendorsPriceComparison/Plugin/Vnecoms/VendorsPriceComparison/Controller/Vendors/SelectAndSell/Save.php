<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2020 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsPriceComparison\Plugin\Vnecoms\VendorsPriceComparison\Controller\Vendors\SelectAndSell;

use Closure;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Registry;
use Magento\Framework\Session\SidResolverInterface;

/**
 * Class Save
 * @package Tigren\VendorsPriceComparison\Plugin\Vnecoms\VendorsPriceComparison\Controller\Vendors\SelectAndSell
 */
class Save
{
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;


    /**
     * Save constructor.
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->productFactory = $productFactory;
    }

    /**
     * @param \Vnecoms\VendorsPriceComparison\Controller\Vendors\SelectAndSell\Save $subject
     */
    public function beforeExecute(
        \Vnecoms\VendorsPriceComparison\Controller\Vendors\SelectAndSell\Save $subject
    ) {
        $productId = $subject->getRequest()->getParam('id');
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->productFactory->create()->load($productId);
        if ($product->getId()) {
            $product->setData('select_from_product_id', 0)
                ->getResource()
                ->saveAttribute($product, 'select_from_product_id');
        }
    }
}
