<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Tigren\ReportGiftCard\Controller\Adminhtml\GiftCards;

/**
 * @api
 * @since 100.0.2
 */
abstract class Report extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_fileFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    ) {
        $this->_fileFactory = $fileFactory;
        parent::__construct($context);
    }

    /**
     * Add reports and shopping cart breadcrumbs
     *
     * @return $this
     */
    public function _initAction()
    {
        $this->_view->loadLayout();
        $this->_addBreadcrumb(__('Reports'), __('Reports'));
        $this->_addBreadcrumb(__('Gift Cards'), __('Gift Cards'));
        return $this;
    }

    /**
     * Determine if action is allowed for reports module
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName()) {
            case 'newCards':
                return $this->_authorization->isAllowed('Tigren_ReportGiftCard::new_cards');
                break;
            case 'activeCards':
                return $this->_authorization->isAllowed('Tigren_ReportGiftCard::active_cards');
                break;
            case 'expiredCards':
                return $this->_authorization->isAllowed('Tigren_ReportGiftCard::expired_cards');
                break;
            default:
                return $this->_authorization->isAllowed('Tigren_ReportGiftCard::report_giftcards');
                break;
        }
    }
}
