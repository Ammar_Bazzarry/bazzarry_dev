<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */
namespace Tigren\ReportGiftCard\Controller\Adminhtml\GiftCards;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class ExportActiveCardExcel extends \Tigren\ReportGiftCard\Controller\Adminhtml\GiftCards\Report
{
    public function execute()
    {
        $fileName = 'giftcards_active.xml';
        $content = $this->_view->getLayout()->createBlock(
            \Tigren\ReportGiftCard\Block\Adminhtml\GiftCards\ActiveCards\Grid::class
        )->getExcelFile(
            $fileName
        );

        return $this->_fileFactory->create($fileName, $content, DirectoryList::VAR_DIR);
    }
}
