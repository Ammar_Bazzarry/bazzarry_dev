<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */


namespace Tigren\ReportGiftCard\Controller\Adminhtml\GiftCards;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class CardUsage extends \Tigren\ReportGiftCard\Controller\Adminhtml\GiftCards\Report
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Lowstock constructor.
     * @param Context $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        Registry $coreRegistry,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context, $fileFactory);
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $this->_initAction()->_setActiveMenu(
            'Tigren_ReportGiftCard::card_usage'
        )->_addBreadcrumb(
            __('Gift Cards Usage'),
            __('Gift Cards Usage')
        )->_addContent(
            $this->_view->getLayout()->createBlock(\Tigren\ReportGiftCard\Block\Adminhtml\GiftCards\CardUsage::class)
        );
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Gift Cards Usage'));
        $this->_view->renderLayout();
    }
}
