<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */


namespace Tigren\ReportGiftCard\Block\Adminhtml\GiftCards\NewCards;

use MageWorx\GiftCards\Model\ResourceModel\Order as GiftCardsResourceModelOrder;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{

    /**
     * @var string
     */
    //protected $_template = 'Tigren_ReportGiftCard::grid.phtml';

    protected $_giftCardsCollection;

    protected $_giftCardOrder;


    /**
     * Grid constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Lowstock\CollectionFactory $lowstocksFactory
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \MageWorx\GiftCards\Model\ResourceModel\GiftCards\CollectionFactory $collectionFactory,
        GiftCardsResourceModelOrder $giftCardsResourceModelOrder,
        array $data = []
    ) {
        $this->_giftCardOrder = $giftCardsResourceModelOrder;
        $this->_giftCardsCollection = $collectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('gridNewCards');
        $this->setDefaultSort('card_id');
        $this->setDefaultDir('ASC');
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid
     */
    protected function _prepareCollection()
    {
        $now = new \DateTime();
        $collection = $this->_giftCardsCollection->create()
            ->addFieldToFilter('card_status','1');
        $collection->getSelect()->where(
            'main_table.card_amount = main_table.card_balance'
        )->where(
            "expire_date >= '".$now->format('Y-m-d') ."' OR expire_date is null"
        );
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @param array $column
     *
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        $field = $column->getFilterIndex() ? $column->getFilterIndex() : $column->getIndex();
        parent::_addColumnFilterToCollection($column);
        return $this;
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'card_id',
            [
                'header' => __('Card Id'),
                'index' => 'card_id',
                'sortable' => true,
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'card_code',
            [
                'header' => __('Gift card code'),
                'index' => 'card_code',
                'sortable' => false,
                'header_css_class' => 'col-code',
                'column_css_class' => 'col-code'
            ]
        );

        $this->addColumn(
            'card_amount',
            [
                'header' => __('Card Amount'),
                'index' => 'card_amount',
                'sortable' => true,
                'type' => 'number',
                'header_css_class' => 'col-amount',
                'column_css_class' => 'col-amount'
            ]
        );

        $this->addColumn(
            'card_currency',
            [
                'header' => __('Card Currency'),
                'index' => 'card_currency',
                'sortable' => true,
                'header_css_class' => 'col-amount',
                'column_css_class' => 'col-amount'
            ]
        );

        $this->addColumn(
            'expire_date',
            [
                'header' => __('Expire Date'),
                'index' => 'expire_date',
                'sortable' => true,
                'type' => 'date',
                'header_css_class' => 'col-date',
                'column_css_class' => 'col-date'
            ]
        );

        $this->setFilterVisibility(true);

        $this->addExportType('*/*/exportNewCardCsv', __('CSV'));
        $this->addExportType('*/*/exportNewCardExcel', __('Excel XML'));

        return parent::_prepareColumns();
    }

    /**
     * @param \Magento\Framework\DataObject $row
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('mageworx_giftcards/giftcards/edit', ['card_id' => $row->getCardId()]);
    }
}
