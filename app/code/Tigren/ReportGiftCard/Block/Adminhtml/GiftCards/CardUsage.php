<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */


namespace Tigren\ReportGiftCard\Block\Adminhtml\GiftCards;

class CardUsage extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Tigren_ReportGiftCard';
        $this->_controller = 'adminhtml_giftCards_cardUsage';
        $this->_headerText = __('Gift Cards Usage');
        parent::_construct();
        $this->buttonList->remove('add');
    }
}
