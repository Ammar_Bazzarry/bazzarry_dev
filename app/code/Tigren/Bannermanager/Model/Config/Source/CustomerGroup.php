<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Tigren\Bannermanager\Model\Config\Source;


/**
 * Class CustomerGroup
 * @package Tigren\Bannermanager\Model\Config\Source
 */
class CustomerGroup implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')->toOptionArray();
        return $groupOptions;
    }
}
