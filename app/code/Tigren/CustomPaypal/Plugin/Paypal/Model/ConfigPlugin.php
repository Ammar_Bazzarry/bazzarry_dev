<?php

namespace Tigren\CustomPaypal\Plugin\Paypal\Model;

use Magento\Paypal\Model\Config;
use Tigren\CustomPaypal\Helper\Data;

/**
 * Class ConfigPlugin
 *
 * @package Tigren\CustomPaypal\Model\Plugin\Paypal
 */
class ConfigPlugin
{
    /** @var \Tigren\CustomPaypal\Helper\Data $helper */
    protected $helper;

    /**
     * ConfigPlugin constructor.
     *
     * @param \Tigren\CustomPaypal\Helper\Data $helper
     */
    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Check whether specified currency code is supported
     *
     * @param \Magento\Paypal\Model\Config $config
     * @param                              $result
     * @return bool
     */
    public function afterIsCurrencyCodeSupported(Config $config, $result)
    {
        if (!$result && $this->helper->isModuleEnabled()) {
            $result = true;
        }

        return $result;
    }
}
