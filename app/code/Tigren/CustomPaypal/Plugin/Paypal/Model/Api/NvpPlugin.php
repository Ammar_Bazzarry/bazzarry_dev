<?php

namespace Tigren\CustomPaypal\Plugin\Paypal\Model\Api;

use Magento\Paypal\Model\Api\Nvp;
use Tigren\CustomPaypal\Helper\Data;
use Tigren\CustomPaypal\Model\CurrencyServiceFactory;

/**
 * Class NvpPlugin
 *
 * @package Tigren\CustomPaypal\Model\Plugin\Paypal\Api
 */
class NvpPlugin
{
    /** @var \Tigren\CustomPaypal\Helper\Data $helper */
    protected $helper;

    /** @var \Tigren\CustomPaypal\Model\CurrencyServiceFactory $currencyServiceFactory */
    protected $currencyServiceFactory;

    /** @var \Tigren\CustomPaypal\Model\CurrencyService\CurrencyServiceInterface|null $currencyService */
    protected $currencyService = null;

    protected $checkoutSession;
    protected $_request;
    protected $orderFactory;
    protected $quoteFactory;

    /**
     * NvpPlugin constructor.
     *
     * @param \Tigren\CustomPaypal\Helper\Data                  $helper
     * @param \Tigren\CustomPaypal\Model\CurrencyServiceFactory $currencyServiceFactory
     */
    public function __construct(
        Data $helper,
        CurrencyServiceFactory $currencyServiceFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory
    ) {
        $this->helper = $helper;
        $this->currencyServiceFactory = $currencyServiceFactory;
        $this->checkoutSession = $checkoutSession;
        $this->_request = $request;
        $this->orderFactory = $orderFactory;
        $this->quoteFactory = $quoteFactory;
    }

    /**
     *
     * used in Magento\Paypal\Model\Express\Checkout::start()
     *
     * @param \Magento\Paypal\Model\Api\Nvp $nvp
     * @param                               $key
     * @param null                          $value
     * @return array
     */
    public function beforeSetData(Nvp $nvp, $key, $value = null)
    {
        $orderId = $this->_request->getParam('order_id');

        if ($this->helper->isModuleEnabled()) {
            switch ($key) {
                case 'amount':
                    $grandTotal = (float)0;
                    $paypalCurrency = $this->helper->getPayPalCurrency();
                    $quoteCurrencyCode = $this->checkoutSession->getQuote()->getQuoteCurrencyCode();
                    if ($quoteCurrencyCode == 'USD' && $paypalCurrency == 'USD'){
                        $grandTotal = $this->checkoutSession->getQuote()->getGrandTotal();
                        $value = $grandTotal;
                    }
                    else {
                        if ($quoteCurrencyCode == 'YER') {
                            $value = $this->getCurrencyService()->exchange($value);
                        }
                        else {
                            if ($quoteCurrencyCode) {
                                $grandTotal = $this->checkoutSession->getQuote()->getGrandTotal();
                                $value = $this->getCurrencyService()->exchangeFromService($grandTotal);
                            } else {
                                $order = $this->orderFactory->create()->load($orderId);
                                $quoteId = $order->getQuoteId();
                                $quote = $this->quoteFactory->create()->load($quoteId);
                                $grandTotal = $quote->getPaypalGrandTotal();
                                $value = $this->getCurrencyService()->exchangeFromService($grandTotal);
                            }
                        }
                    }
                    break;
                case 'currency_code':
                    $value = $this->helper->getPayPalCurrency();
                    break;
                default:
                    break;
            }
        }

        return [$key, $value];
    }

    /**
     * @return false|null|\Tigren\CustomPaypal\Model\CurrencyService\CurrencyServiceInterface
     */
    public function getCurrencyService()
    {
        if (!$this->currencyService) {
            $this->currencyService = $this->currencyServiceFactory->load($this->helper->getCurrencyServiceId());
        }

        return $this->currencyService;
    }
}
