<?php

namespace Tigren\CustomPaypal\Plugin\Config\Model;

use Magento\Config\Model\Config;
use Tigren\CustomPaypal\Helper\Data;
use Tigren\CustomPaypal\Model\CurrencyServiceFactory;
use Tigren\CustomPaypal\Model\RatesFactory;

/**
 * Class ConfigPlugin
 */
class ConfigPlugin
{
    /** @var \Tigren\CustomPaypal\Helper\Data $_helper */
    protected $_helper;

    /** @var \Tigren\CustomPaypal\Model\CurrencyServiceFactory $currencyServiceFactory */
    protected $currencyServiceFactory;

    /** @var \Tigren\CustomPaypal\Model\RatesFactory $ratesFactory */
    protected $ratesFactory;

    /**
     * ConfigPlugin constructor.
     *
     * @param \Tigren\CustomPaypal\Helper\Data                  $helper
     * @param \Tigren\CustomPaypal\Model\CurrencyServiceFactory $currencyServiceFactory
     * @param \Tigren\CustomPaypal\Model\RatesFactory           $ratesFactory
     */
    public function __construct(
        Data $helper,
        CurrencyServiceFactory $currencyServiceFactory,
        RatesFactory $ratesFactory
    ) {
        $this->_helper = $helper;
        $this->currencyServiceFactory = $currencyServiceFactory;
        $this->ratesFactory = $ratesFactory;
    }

    /**
     * @param Config $config
     * @param Config $result
     * @return Config
     */
    public function afterSave(Config $config, $result)
    {
        if ($config->getSection() === 'tigren_custompaypal') {
            /** @var \Tigren\CustomPaypal\Model\CurrencyService\CurrencyServiceInterface $currencyService */
            $currencyService = $this->currencyServiceFactory->load($this->_helper->getCurrencyServiceId());

            /** @var \Tigren\CustomPaypal\Model\Rates $ratesModel */
            $ratesModel = $this->ratesFactory->create();
            $ratesModel->updateRateFromService($currencyService);
            $ratesModel->save();
        }

        return $result;
    }
}
