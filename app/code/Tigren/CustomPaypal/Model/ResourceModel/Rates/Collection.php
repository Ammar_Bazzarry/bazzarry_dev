<?php

namespace Tigren\CustomPaypal\Model\ResourceModel\Rates;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 * @package Tigren\CustomPaypal\Model\ResourceModel\Rates
 */
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Tigren\CustomPaypal\Model\Rates',
            'Tigren\CustomPaypal\Model\ResourceModel\Rates'
        );
    }
}

