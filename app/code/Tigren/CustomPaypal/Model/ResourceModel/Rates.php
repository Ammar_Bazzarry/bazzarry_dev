<?php

namespace Tigren\CustomPaypal\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Rates
 *
 * @package Tigren\CustomPaypal\Model\ResourceModel
 */
class Rates extends AbstractDb

{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('tigren_custompaypal_rates', 'entity_id');
    }
}
