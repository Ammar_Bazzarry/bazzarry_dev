<?php

namespace Tigren\CustomPaypal\Model\Config;

use Magento\Framework\Option\ArrayInterface;
use Tigren\CustomPaypal\Model\CurrencyServiceFactory;

/**
 * Class ConverterServices
 *
 * @package Tigren\CustomPaypal\Model\Config
 */
class CurrencyServices implements ArrayInterface
{
    protected $_services;

    /**
     * CurrencyServices constructor.
     *
     * @param \Tigren\CustomPaypal\Model\CurrencyServiceFactory $serviceFactory
     */
    public function __construct(CurrencyServiceFactory $serviceFactory)
    {
        $this->_services = $serviceFactory->getServices();
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->_services as $key => $service) {
            $options[] = ['value' => $key, 'label' => $service['label']];
        }

        return $options;
    }
}
