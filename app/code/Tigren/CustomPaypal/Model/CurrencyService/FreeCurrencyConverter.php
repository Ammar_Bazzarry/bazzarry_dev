<?php

namespace Tigren\CustomPaypal\Model\CurrencyService;

/**
 * Class FreeCurrencyConverter
 * docs http://www.currencyconverterapi.com/docs
 * simple GET call  http://free.currencyconverterapi.com/api/v3/convert?q=UAH_USD&compact=ultra
 *
 * @package Tigren\CustomPaypal\Model\CurrencyService
 */
class FreeCurrencyConverter extends CurrencyServiceAbstract implements CurrencyServiceInterface
{

    /**
     * @return string
     */
    public function getApiUrl()
    {
        return 'https://free.currconv.com/api/v7/convert';
    }

    /**
     * Exchange rates
     *
     * @param float $amt
     * @return float
     */

    public function exchangeFromService($amt)
    {

        $exchangeQuery = $this->getStoreCurrencyCode() . '_' . $this->getPayPalCurrencyCode();
        $url = $this->getApiUrl() . '?' . http_build_query(
                [
                    'q'       => $exchangeQuery,
                    'compact' => 'ultra',
                    'apiKey' => $this->helper->getGeneralConfig('free_currency_api')
                ]
            );
        $this->getCurl()->get($url);
        $response = json_decode($this->getCurl()->getBody());
        $result = round(floatval($response->$exchangeQuery),4) * $amt;

        return round($result, 4);
    }
}
