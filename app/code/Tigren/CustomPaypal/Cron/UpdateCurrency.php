<?php

namespace Tigren\CustomPaypal\Cron;

use Tigren\CustomPaypal\Helper\Data;
use Tigren\CustomPaypal\Model\CurrencyServiceFactory;
use Tigren\CustomPaypal\Model\RatesFactory;
use Psr\Log\LoggerInterface;

class UpdateCurrency
{
    /** @var \Tigren\CustomPaypal\Helper\Data $helper */
    protected $helper;

    /** @var \Tigren\CustomPaypal\Model\CurrencyServiceFactory $currencyServiceFactory */
    protected $currencyServiceFactory;

    /** @var \Tigren\CustomPaypal\Model\RatesFactory $ratesFactory */
    protected $ratesFactory;

    /** @var \Psr\Log\LoggerInterface $logger */
    protected $logger;

    /**
     * ConfigPlugin constructor.
     *
     * @param \Tigren\CustomPaypal\Helper\Data                  $helper
     * @param \Tigren\CustomPaypal\Model\CurrencyServiceFactory $currencyServiceFactory
     * @param \Tigren\CustomPaypal\Model\RatesFactory           $ratesFactory
     * @param \Psr\Log\LoggerInterface                                   $logger
     */
    public function __construct(
        Data $helper,
        CurrencyServiceFactory $currencyServiceFactory,
        RatesFactory $ratesFactory,
        LoggerInterface $logger
    ) {
        $this->helper = $helper;
        $this->currencyServiceFactory = $currencyServiceFactory;
        $this->ratesFactory = $ratesFactory;
        $this->logger = $logger;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        /** @var \Tigren\CustomPaypal\Model\CurrencyService\CurrencyServiceInterface $currencyService */
        $currencyService = $this->currencyServiceFactory->load($this->helper->getCurrencyServiceId());

        /** @var \Tigren\CustomPaypal\Model\Rates $ratesModel */
        $ratesModel = $this->ratesFactory->create();
        $ratesModel->updateRateFromService($currencyService);
        $ratesModel->save();
        $this->logger->info('Rate is updated');

        return $this;
    }
}
