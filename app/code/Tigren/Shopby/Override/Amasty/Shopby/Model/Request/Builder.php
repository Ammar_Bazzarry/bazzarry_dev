<?php

namespace Tigren\Shopby\Override\Amasty\Shopby\Model\Request;

/**
 * Class Builder
 * @package Tigren\Shopby\Override\Amasty\Shopby\Model\Request
 */
class Builder extends \Amasty\Shopby\Model\Request\Builder
{
    /**
     * @param string $placeholder
     * @param mixed $value
     * @return $this
     */
    public function bind($placeholder, $value)
    {
        if ($placeholder == 'search_term') {
            $value = str_ireplace(['+', '-'], ' ', $value);
        }
        
        $this->removablePlaceholders[$placeholder] = $placeholder == \Amasty\Shopby\Helper\Category::ATTRIBUTE_CODE
            ? $this->makeCategoryPlaceholder($placeholder, $value)
            : $value;

        return $this;
    }
}
