<?php

namespace Tigren\Shopby\Override\Amasty\Shopby\Plugin\Ajax;

/**
 * Class ProductListWrapper
 * @package Tigren\Shopby\Override\Amasty\Shopby\Plugin\Ajax
 */
class ProductListWrapper extends \Amasty\Shopby\Plugin\Ajax\ProductListWrapper
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    private $request;

    /**
     * @var bool
     */
    private $hasTopFilters = false;

    /**
     * ProductListWrapper constructor.
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Amasty\Shopby\Model\Layer\FilterList $filterListTop
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Amasty\Shopby\Model\Layer\FilterList $filterListTop,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver
    ) {
        parent::__construct($request, $filterListTop, $layerResolver);
        $this->request = $request;
        $layer = $layerResolver->get();
        $this->hasTopFilters = (bool)$filterListTop->getFilters($layer);
    }

    /**
     * @param \Magento\Framework\View\Element\Template $subject
     * @param $result
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterToHtml(\Magento\Framework\View\Element\Template $subject, $result)
    {
        if ($subject->getNameInLayout() !== 'category.products.list'
            && $subject->getNameInLayout() !== 'search_result_list'
            && strpos($subject->getNameInLayout(), 'product\productslist') === false // cms block widjet
        ) {
            return $result;
        }

        if ($this->request->getParam('is_scroll')) {
            return $result;
        }

        $stateHtml = '';
        $clearHtml = '';
        $layout = $subject->getLayout();
        $navigation = $layout->getBlock('catalog.leftnav') ?: $layout->getBlock('catalogsearch.leftnav');
        $state = $layout->getBlock('catalog.navigation.state') ?: $layout->getBlock('catalogsearch.navigation.state');
        if ($navigation && $state) {
            if ($state) {
                $stateHtml = $state->toHtml();
                if ($layout->getBlock('catalogsearch.navigation.state')){
                    $clearHtml = $layout->getBlock('amshopby.catalog.topnav')->toHtml();
                }
            }
        }

        return
            '<div id="amasty-shopby-product-list">'
            . '<div id="selected-choices">' . $stateHtml . '</div>'
            .'<div class="catalog-topnav amasty-catalog-topnav">'.$clearHtml.'</div>'
            . $result
            . '</div>';
    }
}