<?php

namespace Tigren\Shopby\Override\Amasty\Shopby\Plugin\Ajax;

use Amasty\Shopby\Helper\State;
use Magento\Catalog\Model\Category;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Layout\Element;

class Ajax extends \Amasty\Shopby\Plugin\Ajax\Ajax
{
    /**
     * @param \Magento\Framework\View\Result\Page $page
     *
     * @return array
     */
    protected function getAjaxResponseData($page = null)
    {
        $layout = $page->getLayout();
        $tags = [];

        $products = $layout->getBlock('category.products');
        if (!$products) {
            $products = $layout->getBlock('search.result');
        }

        $productsCount = 0;
        $productList = null;
        if ($products) {
            $tags = $this->addXTagCache($products, $tags);
            $productList = $products->getChildBlock('product_list') ?: $products->getChildBlock('search_result_list');
            $productsCount = $productList
                ? $productList->getLoadedProductCollection()->getSize()
                : $products->getResultCount();
        }
        $state = null;
        $navigation = $layout->getBlock('catalog.leftnav') ?: $layout->getBlock('catalogsearch.leftnav');
        if ($navigation) {
            $navigation->toHtml();
            $tags = $this->addXTagCache($navigation, $tags);
            $state = $layout->getBlock('catalog.navigation.state') ?: $layout->getBlock('catalogsearch.navigation.state');
            $state->toHtml();
            $tags = $this->addXTagCache($state, $tags);
        }

        $applyButton = $layout->getBlock('amasty.shopby.applybutton.sidebar');
        $tags = $this->addXTagCache($applyButton, $tags);

        $jsInit = $layout->getBlock('amasty.shopby.jsinit');
        $tags = $this->addXTagCache($jsInit, $tags);

        $categoryProducts = $products ? $products->toHtml() : '';

        $navigationTop = null;
        if (strpos($categoryProducts, 'amasty-catalog-topnav') === false) {
            $navigationTop = $layout->getBlock('amshopby.catalog.topnav');
            $tags = $this->addXTagCache($navigationTop, $tags);
        }

        $applyButtonTop = $layout->getBlock('amasty.shopby.applybutton.topnav');
        $tags = $this->addXTagCache($applyButtonTop, $tags);

        $h1 = $layout->getBlock('page.main.title');
        $tags = $this->addXTagCache($h1, $tags);

        $title = $page->getConfig()->getTitle();
        $breadcrumbs = $layout->getBlock('breadcrumbs');
        $tags = $this->addXTagCache($breadcrumbs, $tags);

        $htmlCategoryData = '';
        $children = $layout->getChildNames('category.view.container');
        foreach ($children as $child) {
            $htmlCategoryData .= $layout->renderElement($child);
            $tags = $this->addXTagCache($child, $tags);
        }

        $htmlCategoryData = '<div class="category-view">' . $htmlCategoryData . '</div>';

        $shopbyCollapse = $layout->getBlock('catalog.navigation.collapsing');
        $shopbyCollapseHtml = '';
        if ($shopbyCollapse) {
            $shopbyCollapseHtml = $shopbyCollapse->toHtml();
            $tags = $this->addXTagCache($shopbyCollapse, $tags);
        }

        $swatchesChoose = $layout->getBlock('catalog.navigation.swatches.choose');
        $swatchesChooseHtml = '';
        if ($swatchesChoose) {
            $swatchesChooseHtml = $swatchesChoose->toHtml();
        }

        $currentCategory = $productList && $productList->getLayer()
            ? $productList->getLayer()->getCurrentCategory()
            : false;

        $isDisplayModePage = $currentCategory && $currentCategory->getDisplayMode() == Category::DM_PAGE;

        $bottomBlock = $layout->getBlock('amshopby.bottom') ? $layout->getBlock('amshopby.bottom')->toHtml() : '';

        $responseData = [
            'categoryProducts'=> $categoryProducts . $swatchesChooseHtml,
            'navigation' =>
                ($navigation ? $navigation->toHtml() : '')
                . $shopbyCollapseHtml
                . ($applyButton ? $applyButton->toHtml() : ''),
            'navigationTop' =>
                ($navigationTop ? $navigationTop->toHtml() : '')
                . ($applyButtonTop ? $applyButtonTop->toHtml() : ''),
            'state' =>
                ($state ? $state->toHtml() : ''),
            'breadcrumbs' => $breadcrumbs ? $breadcrumbs->toHtml() : '',
            'h1' => $h1 ? $h1->toHtml() : '',
            'title' => $title->get(),
            'categoryData' => $htmlCategoryData,
            'bottomCmsBlock' => $bottomBlock,
            'url' => $this->stateHelper->getCurrentUrl(),
            'tags' => implode(',', array_unique($tags + [\Magento\PageCache\Model\Cache\Type::CACHE_TAG])),
            'productsCount' => $productsCount,
            'js_init' => $jsInit ? $jsInit->toHtml() : '',
            'isDisplayModePage' => $isDisplayModePage,
            'currentCategoryId' => $currentCategory ? $currentCategory->getId() ?: 0 : 0
        ];
        if ($layout->getBlock('category.amshopby.ajax')) {
            $responseData['newClearUrl'] = $layout->getBlock('category.amshopby.ajax')->getClearUrl();
        }

        try {
            $sidebarTag = $layout->getElementProperty('div.sidebar.additional', Element::CONTAINER_OPT_HTML_TAG);
            $sidebarClass = $layout->getElementProperty('div.sidebar.additional', Element::CONTAINER_OPT_HTML_CLASS);
            $sidebarAdditional = $layout->renderNonCachedElement('div.sidebar.additional');
            $responseData['sidebar_additional'] = $sidebarAdditional;
            $responseData['sidebar_additional_alias'] = $sidebarTag . '.' . str_replace(' ', '.', $sidebarClass);
        } catch (\Exception $e) {
            //container doesn't exist
        }

        $responseData = $this->removeAjaxParam($responseData);
        $responseData = $this->removeEncodedAjaxParams($responseData);

        return $responseData;
    }
    /**
     * @param mixed $element
     * @param array $tags
     * @return array
     */
    private function addXTagCache($element, array $tags)
    {
        if ($element instanceof IdentityInterface) {
            $tags = array_merge($tags, $element->getIdentities());
        }

        return $tags;
    }

    /**
     * @param array $responseData
     * @return array
     */
    private function removeEncodedAjaxParams(array $responseData)
    {
        $pattern = '@aHR0c(Dov|HM6)[A-Za-z0-9_-]+@u';
        array_walk($responseData, function (&$html) use ($pattern) {
            // 'aHR0cDov' and 'aHR0cHM6' are the beginning of the Base64 code for 'http:/' and 'https:'
            $res = preg_replace_callback($pattern, [$this, 'removeAjaxParamFromEncodedMatch'], $html);
            if ($res !== null) {
                $html = $res;
            }
        });

        return $responseData;
    }
}