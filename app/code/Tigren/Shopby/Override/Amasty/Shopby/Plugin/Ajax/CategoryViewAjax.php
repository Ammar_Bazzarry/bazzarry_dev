<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Shopby\Override\Amasty\Shopby\Plugin\Ajax;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\View\Result\Page;

/**
 * Class CategoryViewAjax
 * @package Tigren\Shopby\Override\Amasty\Shopby\Plugin\Ajax
 */
class CategoryViewAjax extends \Amasty\Shopby\Plugin\Ajax\CategoryViewAjax
{
    /**
     * @param Action $controller
     * @param Page $page
     *
     * @return Raw|Page
     */
    public function afterExecute(Action $controller, $page)
    {
        if ($controller->getRequest()->getParam('ajaxscroll') || !$this->isAjax($controller->getRequest()) || !$page instanceof Page) {
            return $page;
        }

        $responseData = $this->getAjaxResponseData($page);
        
        return $this->prepareResponse($responseData);
    }
}
