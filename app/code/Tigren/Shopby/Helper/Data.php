<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Shopby\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Amasty\Shopby;
use Magento\Framework\App\Helper\Context;

/**
 * Class Data
 * @package Amasty\Shopby\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var Shopby\Model\Request
     */
    protected $shopbyRequest;

    public function __construct(
        Context $context,
        \Amasty\Shopby\Model\Request $shopbyRequest
    ) {
        parent::__construct($context);
        $this->shopbyRequest = $shopbyRequest;
    }

    /**
     * @param \Magento\Catalog\Model\Layer\Filter\Item $filterItem
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isFilterItemSelected($filterItem)
    {
        $filter = $filterItem->getFilter();
        $data = $this->shopbyRequest->getFilterParam($filter);

        if (!empty($data)) {
            $ids = explode(',', $data);
            if (in_array($filterItem->getValue(), $ids)) {
                return 1;
            }
        }

        return 0;
    }
}
