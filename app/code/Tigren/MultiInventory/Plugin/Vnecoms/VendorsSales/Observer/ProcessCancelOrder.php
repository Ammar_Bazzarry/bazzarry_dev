<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Tigren\MultiInventory\Plugin\Vnecoms\VendorsSales\Observer;

use Magento\Framework\Event\ObserverInterface;
use Vnecoms\VendorsConfig\Helper\Data;
use Vnecoms\VendorsSales\Model\Order\Email\Sender\OrderSender;

class ProcessCancelOrder
{
    protected $registry;

    public function __construct(
        \Magento\Framework\Registry $registry
    ) {
        $this->registry = $registry;
    }

    public function beforeExecute(\Vnecoms\VendorsSales\Observer\ProcessCancelOrder $subject)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/asdsad.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 50), true));
        $this->registry->register('vendor_cancel',true);
    }

    public function afterExecute(\Vnecoms\VendorsSales\Observer\ProcessCancelOrder $subject)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/asdsadafter.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 50), true));
        $this->registry->unregister('vendor_cancel');
    }
}
