<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Tigren\MultiInventory\Plugin\Amasty\MultiInventory\Observer;

use Magento\CatalogInventory\Api\StockManagementInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Vnecoms\VendorsConfig\Helper\Data;
use Vnecoms\VendorsSales\Model\Order\Email\Sender\OrderSender;

class CancelOrderItemObserver extends \Amasty\MultiInventory\Observer\CancelOrderItemObserver
{
    protected $registry;

    /**
     * @var StockManagementInterface
     */
    private $helper;

    /**
     * @var \Amasty\MultiInventory\Model\Warehouse\OrderItemRepository
     */
    private $orderItemRepository;

    public function __construct(
        \Amasty\MultiInventory\Helper\Data $helper,
        \Amasty\MultiInventory\Api\WarehouseOrderItemRepositoryInterface $orderItemRepository,
        \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface $stockRepository,
        \Amasty\MultiInventory\Helper\System $system,
        \Amasty\MultiInventory\Logger\Logger $logger,
        \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor,
        \Magento\Framework\Registry $registry
    ) {
        $this->registry = $registry;
        $this->helper = $helper;
        $this->orderItemRepository = $orderItemRepository;
        parent::__construct($helper, $orderItemRepository, $stockRepository, $system, $logger,$processor);
    }

    public function aroundExecute(\Amasty\MultiInventory\Observer\CancelOrderItemObserver $subject,$proceed, EventObserver $observer)
    {
        if($this->registry->registry('vendor_cancel')){
            return;
        }
        if (!$this->system->isMultiEnabled()) {
            return;
        }
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/after.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($this->registry->registry('vendor_cancel'), true));
        /** @var \Magento\Sales\Model\Order\Item $item */
        $item = $observer->getEvent()->getItem();
        $children = $item->getChildrenItems();
        $qty = $item->getQtyOrdered() - max($item->getQtyShipped(), $item->getQtyInvoiced()) - $item->getQtyCanceled();
        if ($item->getId() && $item->getProductId() && empty($children) && $qty > 0) {
            $warehouseId = $this->orderItemRepository->getByOrderItemId($item->getId())->getWarehouseId();
            $productId   = $item->getProductId();
            $stockItem   = $this->stockRepository->getByProductWarehouse($productId, $warehouseId);
            if (!$stockItem->getId()) {
                return;
            }

            $oldQty = $stockItem->getQty();
            if ($stockItem->getShipQty() > 0) {
                $stockItem->setShipQty($stockItem->getShipQty() - $qty);
            } else {
                $stockItem->setQty($oldQty + $qty);
            }
            $stockItem->recalcAvailable();
            if ($stockItem->getAvailableQty() > 0) {
                $stockItem->setStockStatus(\Magento\CatalogInventory\Model\Stock::STOCK_IN_STOCK);
            }
            $this->stockRepository->save($stockItem);

            if ($this->system->isEnableLog()) {
                $this->logger->infoWh(
                    $stockItem->getProduct()->getSku(),
                    $stockItem->getProductId(),
                    $stockItem->getWarehouse()->getTitle(),
                    $stockItem->getWarehouse()->getCode(),
                    $oldQty,
                    $stockItem->getQty(),
                    'cancel'
                );
            }
            $this->processor->reindexRow($productId);
        }
    }
}
