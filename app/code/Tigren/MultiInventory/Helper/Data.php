<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\MultiInventory\Helper;

use Amasty\MultiInventory\Model\ResourceModel\Warehouse\Quote\Item\Collection;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Amasty\MultiInventory\Model\ResourceModel\Warehouse\Quote\Item\CollectionFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Quote\Model\Quote\Item;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order;

/**
 * Class Data
 * @package Tigren\MultiInventory\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var
     */
    protected $_connection;

    /**
     * @var CollectionFactory
     */
    private $quoteItemWhCollection;

    /**
     * Data constructor.
     * @param Context $context
     * @param ResourceConnection $resourceConnection
     * @param CollectionFactory $quoteItemWhCollection
     */
    public function __construct(
        Context $context,
        ResourceConnection $resourceConnection,
        CollectionFactory $quoteItemWhCollection
    ) {
        parent::__construct($context);
        $this->resource = $resourceConnection;
        $this->quoteItemWhCollection = $quoteItemWhCollection;
    }

    /**
     *
     */
    public function updateWarehouseItemStatus()
    {
        $connection = $this->getConnection();
        $itemTable = $connection->getTableName('amasty_multiinventory_warehouse_item');
        $whereCond = ['available_qty = ?' => 0];
        $connection->update($itemTable,
            ['stock_status' => '0'],
            $whereCond);
    }

    /**
     * @return AdapterInterface
     */
    public function getConnection()
    {
        if (!$this->_connection) {
            $this->_connection = $this->resource->getConnection(ResourceConnection::DEFAULT_CONNECTION);
        }
        return $this->_connection;
    }

    /**
     * @param Order $order
     * @return array
     */
    public function dispatchWarehouseForQuote($order)
    {
        $result = [];
        /** @var Collection $quoteWhItems */
        $quoteWhItems = $this->quoteItemWhCollection->create()
            ->getWarehousesFromQuote($order->getQuoteId());

        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItemId() || $this->isSimple($item)) {
                $warehouseQuote = $quoteWhItems->getItemByColumnValue('quote_item_id', $item->getQuoteItemId());
                if ($warehouseQuote !== null) {
                    $result[] = $this->getArrayItem($item, $warehouseQuote->getWarehouseId());
                }
            }else{
                $result[] = $this->getArrayItem($item, 0);
            }
        }

        return $result;
    }

    /**
     * @param \Magento\Sales\Model\Order\Item | Item $item
     * @return bool
     */
    public function isSimple($item)
    {
        return $item->getProduct()->getTypeId() == Type::TYPE_SIMPLE || ($item->getProduct()->getTypeId() == 'mageworx_giftcards' && $item->getProduct()->getData('mageworx_gc_type') && $item->getProduct()->getData('mageworx_gc_type') == '3');
    }

    /**
     * @param OrderItemInterface $item
     * @param int $warehouse
     * @return array
     */
    public function getArrayItem($item, $warehouse)
    {
        return [
            'order_id' => $item->getOrderId(),
            'order_item_id' => $item->getId(),
            'warehouse_id' => $warehouse,
            'product_id' => $item->getProductId(),
            'qty' => $item->getQtyOrdered(),
            'vendor_id' => $item->getVendorId(),
        ];
    }
}
