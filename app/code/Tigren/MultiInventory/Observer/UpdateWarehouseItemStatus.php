<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\MultiInventory\Observer;

use Tigren\MultiInventory\Helper\Data;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class UpdateWarehouseItemStatus
 * @package Tigren\MultiInventory\Observer
 */
class UpdateWarehouseItemStatus implements ObserverInterface
{
    /** @var \Tigren\MultiInventory\Helper\Data $helper */
    protected $helper;
    /**
     * @var \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Item
     */
    protected $item;

    /**
     * UpdateWarehouseItemStatus constructor.
     * @param Data $helper
     */
    public function __construct(
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Item $item,
        Data $helper
    ) {
        $this->item = $item;
        $this->helper = $helper;
    }

    /**
     *
     */
    public function execute(EventObserver $observer)
    {
        $this->item->updateItemStatus();
        $this->helper->updateWarehouseItemStatus();
    }
}
