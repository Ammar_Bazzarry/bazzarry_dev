<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\MultiInventory\Override\Vnecoms\VendorsSales\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class ProcessOrder
 * @package Tigren\MultiInventory\Override\Vnecoms\VendorsSales\Observer
 */
class ProcessOrder implements ObserverInterface
{

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool|void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        return;
    }
}
