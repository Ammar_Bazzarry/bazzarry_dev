<?php

namespace Tigren\MultiInventory\Override\Amasty\Model\ResourceModel\Warehouse;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Item
 * @package Tigren\MultiInventory\Override\Amasty\Model\ResourceModel\Warehouse
 */
class Item extends \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Item
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Amasty\MultiInventory\Model\WarehouseFactory
     */
    private $factory;

    /**
     * @var \Magento\Framework\Indexer\CacheContext
     */
    private $cacheContext;

    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    private $productCollection;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    private $configurableProduct;

    /**
     * Item constructor.
     * @param Context $context
     * @param EntityManager $entityManager
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $factory
     * @param \Magento\Framework\Indexer\CacheContext $cacheContext
     * @param ManagerInterface $eventManager
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProduct
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        EntityManager $entityManager,
        \Amasty\MultiInventory\Model\WarehouseFactory $factory,
        \Magento\Framework\Indexer\CacheContext $cacheContext,
        ManagerInterface $eventManager,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection,
        ProductRepositoryInterface $productRepository,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProduct,
        $connectionName = null
    ) {
        parent::__construct($context, $entityManager, $factory, $cacheContext, $eventManager, $productCollection,
            $productRepository, $configurableProduct, $connectionName);
        $this->entityManager = $entityManager;
        $this->factory = $factory;
        $this->cacheContext = $cacheContext;
        $this->eventManager = $eventManager;
        $this->productCollection = $productCollection;
        $this->productRepository = $productRepository;
        $this->configurableProduct = $configurableProduct;
    }

    /**
     * @param $productId
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getItems($productId)
    {
        $select = $this->getConnection()->select()->from(
            ['w' => $this->getMainTable()],
            ['w.warehouse_id', 'wh.title', 'w.qty', 'w.stock_status']
        )->where(
            'w.product_id = :product_id'
        )->joinLeft(
            ['wh' => $this->getTable('amasty_multiinventory_warehouse')],
            'wh.warehouse_id = w.warehouse_id',
            ['wh.title']
        )->where(sprintf('wh.manage=%s and w.warehouse_id<>%s', 1, $this->factory->create()->getDefaultId()));
        $bind = ['product_id' => (int)$productId];

        return $this->getConnection()->fetchAssoc($select, $bind);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateItemStatus()
    {
        $sql = 'SELECT item_id FROM ' . $this->getTable('cataloginventory_stock_item') . ' as a WHERE NOT EXISTS 
                ( SELECT * FROM ' . $this->getMainTable() . ' as b
                   WHERE a.product_id = b.product_id
                )';
        $results = $this->getConnection()->fetchCol($sql);
        if ($results) {
            $results = implode(',', $results);
            $update = 'UPDATE cataloginventory_stock_item set qty = 0, is_in_stock = 0 where item_id in(' . $results . ')';
            $this->getConnection()->query($update);
        }
    }
}