<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */


namespace Tigren\MultiInventory\Override\Amasty\MultiInventory\Model\Warehouse;

use Amasty\MultiInventory\Api\Data\WarehouseItemInterface;
use Amasty\MultiInventory\Model\AbstractWarehouse;
use Amasty\MultiInventory\Model\Config\Source\Backorders;
use Amasty\MultiInventory\Model\Config\Source\BackordersDefault;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Model\Context;

class Item extends \Amasty\MultiInventory\Model\Warehouse\Item
{
    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;



    /**
     * Item constructor.
     * @param Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Amasty\MultiInventory\Helper\System $system
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Amasty\MultiInventory\Helper\System $system,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $warehouseFactory,
            $productRepository,
            $system,
            $stockRegistry,
            $resource,
            $resourceCollection,
            $data
        );
        $this->system = $system;
    }

    /**
     * Recalculate for Available Qty
     */
    public function recalcAvailable()
    {
        if ($this->system->getAvailableDecreese()) {
            $this->setAvailableQty($this->getQty() - $this->getShipQty());
        } else {
            $this->setAvailableQty($this->getQty());
        }

        $productType = $this->getProduct()->getTypeId();
        $isSimple = $this->getProduct()->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE ||
            ($this->getProduct()->getTypeId() == 'mageworx_giftcards' && $this->getProduct()->getData('mageworx_gc_type') && $this->getProduct()->getData('mageworx_gc_type') == '3');

        if ($this->_registry->registry('am_dont_recalc_availability')
            || !$isSimple
        ) {
            return $this;
        }

        if ($this->getRealQty() <= 0 && !$this->isCanBackorder()) {
            $this->setStockStatus(\Magento\CatalogInventory\Model\Stock::STOCK_OUT_OF_STOCK);
        } else {
            $this->setStockStatus(\Magento\CatalogInventory\Model\Stock::STOCK_IN_STOCK);
        }

        return $this;
    }

}
