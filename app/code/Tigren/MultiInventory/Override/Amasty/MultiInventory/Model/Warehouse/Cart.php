<?php

namespace Tigren\MultiInventory\Override\Amasty\MultiInventory\Model\Warehouse;

class Cart extends \Amasty\MultiInventory\Model\Warehouse\Cart
{
    private $quoteItemWhCollection;
    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /**
     * @var \Amasty\MultiInventory\Model\Warehouse\Item\QuantityValidator
     */
    private $quantityValidator;

    public function __construct(
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Item\CollectionFactory $itemCollection,
        \Amasty\MultiInventory\Model\Warehouse\ItemRepository $stockRepository,
        \Amasty\MultiInventory\Model\Warehouse\Quote\ItemFactory $quoteItemWhFactory,
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Quote\Item\CollectionFactory $quoteItemWhCollection,
        \Amasty\MultiInventory\Api\WarehouseQuoteItemRepositoryInterface $quoteItemWhRepository,
        \Amasty\MultiInventory\Helper\System $system,
        \Magento\Checkout\Model\Cart $checkoutCart,
        \Amasty\MultiInventory\Model\Dispatch $dispatch,
        \Magento\Quote\Model\Quote\Item\Processor $itemProcessor,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Amasty\MultiInventory\Model\Warehouse\Item\QuantityValidator $quantityValidator,
        \Magento\Framework\App\State $appState,
        \Magento\Backend\Model\Session\Quote\Proxy $backendQuote,
        array $data = []
    ) {
        parent::__construct($itemCollection, $stockRepository, $quoteItemWhFactory, $quoteItemWhCollection, $quoteItemWhRepository,
            $system, $checkoutCart, $dispatch, $itemProcessor, $eventManager, $quantityValidator, $appState, $backendQuote,$data);
        $this->quoteItemWhCollection = $quoteItemWhCollection;
        $this->system = $system;
        $this->quantityValidator = $quantityValidator;
    }

    public function getGroupItems($warehouseId)
    {
        $collection = $this->quoteItemWhCollection->create();
        $collection->getSelect()
            ->joinLeft(
                ['soi' => 'quote_item'],
                'soi.item_id = main_table.quote_item_id',
                ['vendor_id' => 'soi.vendor_id']
            );
        $collection->addFieldToFilter('warehouse_id', $warehouseId)
            ->addFieldToFilter('main_table.quote_id', $this->getQuote()->getId());
        return $collection->getData();

    }

    /**
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @return \Amasty\MultiInventory\Model\Warehouse\Item\ValidatorResultData[]
     */
    public function dispatchWarehouse($quoteItem)
    {
        $result = [];
        $this->getDispatch()->setCallables($this->system->getDispatchOrder());
        $this->getDispatch()->resetExclude();
        if ($quoteItem->getParentItemId() || $this->quantityValidator->isProductSimple($quoteItem->getProduct()) ||
            ($quoteItem->getProduct()->getTypeId() == 'mageworx_giftcards'
                && $quoteItem->getProduct()->getData('mageworx_gc_type')
                && $quoteItem->getProduct()->getData('mageworx_gc_type') == '3')) {
            $this->prepareDispatch($quoteItem);
            $qty = $quoteItem->getQty();
            if ($quoteItem->getParentItem()) {
                $qty = $quoteItem->getParentItem()->getQty();
            }
            $checkResult = $this->quantityValidator->checkQuoteItemQty($quoteItem, $qty);
            $result = array_merge($result, $checkResult);
        }

        return $result;
    }

    /**
     * Set Dispatch Config
     *
     * @param \Magento\Quote\Model\Quote\Item $item
     */
    private function prepareDispatch($item)
    {
        $dispatch = $this->getDispatch();
        $dispatch->setQuoteItem($item);
        $dispatch->setDirection(\Amasty\MultiInventory\Model\Dispatch::DIRECTION_QUOTE);
    }
}
