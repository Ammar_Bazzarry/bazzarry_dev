<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\MultiInventory\Override\Amasty\MultiInventory\Model;

use Amasty\MultiInventory\Helper\Distance as HelperData;
use Amasty\MultiInventory\Model\CustomerCoordinatesFactory;
use Amasty\MultiInventory\Model\Warehouse\StoreFactory;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\Api\SimpleDataObjectConverter;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\DecoderInterface;
use Magento\Framework\App\Cache;
use Magento\Framework\Json\EncoderInterface;
use Amasty\MultiInventory\Model\CustomerCoordinates;

class Dispatch extends \Amasty\MultiInventory\Model\Dispatch
{
    static private $CALCULATED_NEAREST = [];

    /**
     * @var CustomerCoordinatesFactory
     */
    private $customerCoordinatesFactory;

    /**
     * @var HelperData
     */
    private $helperData;

    public function __construct(
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Store\CollectionFactory $collectionStoreFactory,
        \Amasty\MultiInventory\Model\Warehouse\ItemFactory $stockFactory,
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Item\CollectionFactory $stockCollectionFactory,
        \Amasty\MultiInventory\Model\Warehouse\StoreFactory $storeFactory,
        \Amasty\MultiInventory\Model\WarehouseFactory $whFactory,
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\CollectionFactory $warehouseCollectionFactory,
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\CustomerGroup\CollectionFactory $groupCollectionFactory,
        \Amasty\MultiInventory\Helper\System $system,
        \Magento\Framework\HTTP\Adapter\Curl $clientUrl,
        RegionFactory $regionFactory,
        \Magento\Framework\Locale\ListsInterface $localeLists,
        DecoderInterface $jsonDecoder,
        \Magento\Customer\Model\Session\Proxy $customerSession,
        \Magento\Store\Model\StoreManagerInterface\Proxy $storeManager,
        \Magento\Framework\Registry $registry,
        CustomerCoordinatesFactory $customerCoordinates,
        \Amasty\MultiInventory\Model\WarehouseRepository $warehouseRepository,
        HelperData $helperData,
        Cache $cache,
        EncoderInterface $jsonEncoder,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        array $data = []
    ) {
        parent::__construct($collectionStoreFactory,$stockFactory,$stockCollectionFactory,$storeFactory,$whFactory,$warehouseCollectionFactory,$groupCollectionFactory,
            $system,$clientUrl,$regionFactory,$localeLists,$jsonDecoder,$customerSession,$storeManager,
            $registry,$customerCoordinates,$warehouseRepository,$helperData,$cache,$jsonEncoder,$messageManager,$data);
        $this->customerCoordinatesFactory = $customerCoordinates;
        $this->helperData = $helperData;
    }

    /**
     * Check distance according coordinates
     *
     * @return $this
     */
    public function calculateByCoordinates()
    {
        $address = $this->getShippingAddress();
        $prepearedAddress = $this->helperData->prepareAddressForGoogle($address->getData());
        $cacheKey = $prepearedAddress . implode(',', $this->warehouses);
        if (isset(self::$CALCULATED_NEAREST[$cacheKey])) {
            $this->warehouses = self::$CALCULATED_NEAREST[$cacheKey];
            return $this;
        }

        $customerCoordinates = $this->customerCoordinatesFactory->create();
        $customerCoordinates->load($address->getCustomerAddressId(), CustomerCoordinates::ADDRESS_ID);
        if ($customerCoordinates->getLng() && $customerCoordinates->getLat()) {
            $from = [
                'lat' => $customerCoordinates->getLat(),
                'lng' => $customerCoordinates->getLng()
            ];
        } else {
            $from = $this->helperData->getCoordinatesByAddress($prepearedAddress);
        }

        if (!$from) {
            return;
        }

        $whCoordinates = $this->getWarehouseCoordinates();
        $distance = 0;
        foreach ($whCoordinates as $id => $to) {
            $result = $this->calculateDistance($from, $to);
            if ($result == 0){
                $this->warehouses = [$id];
                self::$CALCULATED_NEAREST[$cacheKey] = $this->warehouses;
                break;
            }
            if ($result < $distance || !$distance) {
                $distance = $result;
                $this->warehouses = [$id];
                self::$CALCULATED_NEAREST[$cacheKey] = $this->warehouses;
            }
        }
    }

}
