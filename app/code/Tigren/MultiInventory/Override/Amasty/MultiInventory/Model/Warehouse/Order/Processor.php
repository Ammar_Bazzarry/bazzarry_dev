<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\MultiInventory\Override\Amasty\MultiInventory\Model\Warehouse\Order;

use Amasty\MultiInventory\Helper\Data;
use Amasty\MultiInventory\Helper\System;
use Amasty\MultiInventory\Model\Dispatch;
use Amasty\MultiInventory\Model\Warehouse\Item\QuantityValidator;
use Amasty\MultiInventory\Model\Warehouse\Item\ValidatorResultData;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Registry;
use Magento\Paypal\Model\ConfigFactory;
use Magento\Quote\Model\Quote\Address\ToOrderAddress;
use Magento\Quote\Model\Quote\Payment\ToOrderPayment;
use Magento\Quote\Model\QuoteFactory;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface;
use Magento\Sales\Api\TransactionRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\Order\ItemFactory;
use Magento\Sales\Model\Order\Payment\State\AuthorizeCommand;
use Magento\Sales\Model\Order\Status\HistoryFactory;
use Magento\Sales\Model\OrderFactory;

/**
 * Class Processor
 * @package Tigren\MultiInventory\Override\Amasty\MultiInventory\Model\Warehouse\Order
 */
class Processor extends \Amasty\MultiInventory\Model\Warehouse\Order\Processor
{
    /**
     *
     */
    const HTML_TRANSACTION_ID =
        '<a target="_blank" href="https://www%1$s.paypal.com/cgi-bin/webscr?cmd=_view-a-trans&id=%2$s">%2$s</a>';

    /**
     * @var ToOrderPayment
     */
    protected $quotePaymentToOrderPayment;

    /**
     * @var ToOrderAddress
     */
    protected $quoteAddressToOrderAddress;

    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var TransactionRepositoryInterface
     */
    protected $transactionRepository;

    /**
     * @var HistoryFactory
     */
    protected $historyFactory;

    /**
     * @var OrderStatusHistoryRepositoryInterface
     */
    protected $historyRepository;

    /**
     * @var
     */
    protected $_au;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var System
     */
    private $system;

    /**
     * @var QuantityValidator
     */
    private $quantityValidator;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var ItemFactory
     */
    private $orderItemFactory;

    /**
     * @var OrderItemRepositoryInterface
     */
    private $orderItemRepository;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var
     */
    private $shippingMethod;

    /**
     * @var
     */
    private $shippingDescription;

    /**
     * @var
     */
    private $originOrder;

    /**
     * @var AuthorizeCommand
     */
    private $authCommand;

    /**
     * @var ConfigFactory
     */
    private $configFactory;

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Vnecoms\VendorsSales\Model\OrderFactory
     */
    protected $_vendorOrderFactory;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * @var OrderSender
     */
    protected $_orderSender;

    /**
     * Processor constructor.
     * @param Data $helper
     * @param System $system
     * @param QuantityValidator $quantityValidator
     * @param OrderRepositoryInterface $orderRepository
     * @param ItemFactory $orderItemFactory
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param PriceCurrencyInterface $priceCurrency
     * @param Registry $registry
     * @param OrderFactory $orderFactory
     * @param ToOrderPayment $quotePaymentToOrderPayment
     * @param ToOrderAddress $quoteAddressToOrderAddress
     * @param QuoteFactory $quoteFactory
     * @param AuthorizeCommand $authCommand
     * @param HistoryFactory $historyFactory
     * @param OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepository
     * @param ConfigFactory $configFactory
     * @param TransactionRepositoryInterface $transactionRepository
     */
    public function __construct(
        Data $helper,
        System $system,
        QuantityValidator $quantityValidator,
        OrderRepositoryInterface $orderRepository,
        ItemFactory $orderItemFactory,
        OrderItemRepositoryInterface $orderItemRepository,
        PriceCurrencyInterface $priceCurrency,
        Registry $registry,
        OrderFactory $orderFactory,
        ToOrderPayment $quotePaymentToOrderPayment,
        ToOrderAddress $quoteAddressToOrderAddress,
        QuoteFactory $quoteFactory,
        AuthorizeCommand $authCommand,
        HistoryFactory $historyFactory,
        OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepository,
        ConfigFactory $configFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Vnecoms\VendorsSales\Model\OrderFactory $vendorOrderFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Vnecoms\VendorsSales\Model\Order\Email\Sender\OrderSender $orderSender,
        TransactionRepositoryInterface $transactionRepository
    )
    {
        parent::__construct(
            $helper,
            $system,
            $quantityValidator,
            $orderRepository,
            $orderItemFactory,
            $orderItemRepository,
            $priceCurrency,
            $registry,
            $orderFactory
        );
        $this->helper = $helper;
        $this->system = $system;
        $this->quantityValidator = $quantityValidator;
        $this->orderRepository = $orderRepository;
        $this->orderItemFactory = $orderItemFactory;
        $this->orderItemRepository = $orderItemRepository;
        $this->priceCurrency = $priceCurrency;
        $this->registry = $registry;
        $this->orderFactory = $orderFactory;
        $this->quoteFactory = $quoteFactory;
        $this->quotePaymentToOrderPayment = $quotePaymentToOrderPayment;
        $this->quoteAddressToOrderAddress = $quoteAddressToOrderAddress;
        $this->authCommand = $authCommand;
        $this->transactionRepository = $transactionRepository;
        $this->historyFactory = $historyFactory;
        $this->historyRepository = $orderStatusHistoryRepository;
        $this->configFactory = $configFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_vendorOrderFactory = $vendorOrderFactory;
        $this->_eventManager = $eventManager;
        $this->_orderSender = $orderSender;
    }

    /**
     * Get warehouse for order
     *
     * @param Order $order
     * @return array
     */
    public function dispatchWarehouse($order)
    {
        $this->helper->getDispatch()->setCallables($this->system->getDispatchOrder());
        $this->helper->getDispatch()->resetExclude();
        $this->helper->getDispatch()->setDirection(Dispatch::DIRECTION_ORDER);

        $results = [];
        foreach ($order->getAllItems() as $orderItem) {
            if ($orderItem->getParentItemId() || $this->quantityValidator->isProductSimple($orderItem->getProduct()) ||
                ($orderItem->getProduct()->getTypeId() == 'mageworx_giftcards'
                    && $orderItem->getProduct()->getData('mageworx_gc_type')
                    && $orderItem->getProduct()->getData('mageworx_gc_type') == '3'))
            {
                $this->helper->getDispatch()->setOrderItem($orderItem);
                $validationResult = $this->quantityValidator
                    ->checkQuoteItemQty($orderItem, $orderItem->getQtyOrdered());

                $itemResults = $this->checkOrderItem($validationResult, $orderItem);
                $results = array_merge($results, $itemResults);
            }
        }

        return $results;
    }

    /**
     * If you do not have enough products in warehouse, we take the other
     *
     * @param ValidatorResultData[] $result
     * @param Item $orderItem
     * @return array
     */
    private function checkOrderItem($result, $orderItem)
    {
        $arrayResults = [];
        foreach ($result as $validatorResult) {

            if ($validatorResult->getIsSplitted()) {
                $newItem = $this->createOrderItem($orderItem, $validatorResult->getQty(), $orderItem->getOrder());
                $validatorResult->setOrderItemId($newItem->getItemId());
                $arrayResults[] = $validatorResult->getData();
                continue;
            }
            if ($validatorResult->getIsChanged()) {
                $qty = $validatorResult->getQty();
                if ($orderItem->getParentItemId()) {
                    $parentOrderItem = $this->orderItemRepository->get($orderItem->getParentItemId());
                    $parentOrderItem->setQtyOrdered($qty);
                    $parentOrderItem = $this->changeTotal($parentOrderItem, $qty);
                    $this->orderItemRepository->save($parentOrderItem);
                    $orderItem->setParentItem($parentOrderItem);
                }
                $orderItem->setQtyOrdered($qty);
                if ($orderItem->getPrice() > 0) {
                    $orderItem = $this->changeTotal($orderItem, $qty);
                }
                $this->orderItemRepository->save($orderItem);
            }
            $validationData = $validatorResult->getData();
            $validationData['vendor_id'] = $orderItem->getVendorId();
            $arrayResults[] = $validationData;
        }

        return $arrayResults;
    }

    /**
     * add Items in Order
     *
     * @param Item $orderItem
     * @param int $qty
     * @param Order $order
     * @return Item
     */
    private function createOrderItem($orderItem, $qty, $order)
    {
        $parent = 0;
        if ($orderItem->getParentItemId()) {
            $newParentOrderItem = $this->orderItemFactory->create();
            $parentOrderItem = $this->orderItemRepository->get($orderItem->getParentItemId());
            $newParentOrderItem->setData($this->beforeData($parentOrderItem->getData()));
            $newParentOrderItem->setQtyOrdered($qty);
            $newParentOrderItem = $this->changeTotal($newParentOrderItem, $qty);
            $this->orderItemRepository->save($newParentOrderItem);
            $parent = $newParentOrderItem->getId();
            $order->addItem($newParentOrderItem);
        }
        $newOrderItem = $this->orderItemFactory->create();
        $newOrderItem->setData($this->beforeData($orderItem->getData()));
        $newOrderItem->setQtyOrdered($qty);
        if ($parent) {
            $newOrderItem->setParentItemId($parent);
            $newOrderItem->setParentItem($newParentOrderItem);
        }
        if ($newOrderItem->getPrice()) {
            $newOrderItem = $this->changeTotal($newOrderItem, $qty);
        }
        $this->orderItemRepository->save($newOrderItem);
        $order->addItem($newOrderItem);

        return $newOrderItem;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function beforeData($data)
    {
        unset($data['item_id']);
        $data['quote_item_id'] = null;
        $data['parent_item_id'] = null;

        return $data;
    }

    /**
     * Calc Total for New Order
     *
     * @param Item $item
     * @param int $qty
     * @return Item
     */
    private function changeTotal($item, $qty)
    {
        $total = $this->priceCurrency->round($item->getPrice() * $qty);
        $baseTotal = $this->priceCurrency->round($item->getBasePrice() * $qty);
        $totalInclTax = $this->priceCurrency->round($item->getPriceInclTax() * $qty);
        $baseTotalInclTax = $this->priceCurrency->round($item->getBasePriceInclTax() * $qty);
        $item->setRowTotal($total);
        $item->setBaseRowTotal($baseTotal);
        $item->setRowTotalInclTax($totalInclTax);
        $item->setBaseRowTotalInclTax($baseTotalInclTax);
        if ($item->getDiscountPercent()) {
            $discount = $this->priceCurrency->round($total * ($item->getDiscountPercent() / 100));
            $baseDiscount = $this->priceCurrency->round($baseTotal * ($item->getDiscountPercent() / 100));
            $item->setDiscountAmount($discount);
            $item->setBaseDiscountAmount($baseDiscount);
        }

        return $item;
    }

    /**
     * Separate orders on warehouses
     *
     * @param $result
     * @param Order $order
     * @return array
     * @throws CouldNotSaveException
     */
    public function separateOrders($result, $order)
    {
        if (!$this->system->getSeparateOrders()) {
            return [$result, [$order]];
        }

        $list = [];
        $mainVendorId = $this->scopeConfig->getValue('customvendor/general/main_vendor');
        $countOrder = 0;
        foreach ($result as $key => $itemEntity) {
            if (!isset($list[$itemEntity['vendor_id']][$itemEntity['warehouse_id']])) {
                $countOrder++;
            }
            $result[$key]['key'] = $key;
            $list[$itemEntity['vendor_id']][$itemEntity['warehouse_id']][] = $result[$key];
        }

        $remainDiscount = 0;
        $baseRemainDiscount = 0;

        if ($order->getData('mageworx_giftcards_amount')){
            $remainDiscount = abs($order->getData('mageworx_giftcards_amount'));
            $baseRemainDiscount = abs($order->getData('base_mageworx_giftcards_amount'));
        }
        $orders = [];
        $numberOrder = 1;
        $baseShippingAmount = $order->getBaseShippingAmount();
        if ($baseShippingAmount) {
            $baseShippingAmount = round($order->getBaseShippingAmount() / $countOrder, 4);
        }
        foreach ($list as $vendorId => $whItems) {
            foreach ($whItems as $wh => $items) {
                if ($numberOrder > 1) {
                    $newOrder = $this->orderFactory->create();
                    $newOrder->setData($this->beforeDataOrder($order->getData()));

                    $quote = $this->quoteFactory->create()->load($this->originOrder->getQuoteId());
                    $billing = $this->quoteAddressToOrderAddress->convert($quote->getBillingAddress());
                    $newOrder->setBillingAddress($billing);
                    $shipping = $this->quoteAddressToOrderAddress->convert($quote->getShippingAddress());
                    $newOrder->setShippingAddress($shipping);
                    $payment = $this->quotePaymentToOrderPayment->convert($quote->getPayment());
                    $payment->setAdditionalInformation($this->originOrder->getPayment()->getAdditionalInformation());
                    $payment->setLastTransId($this->originOrder->getPayment()->getLastTransId());
                    $newOrder->setPayment($payment);
                    $this->orderRepository->save($newOrder);
                    $newOrderId = $newOrder->getId();
                    $vendorItems = [];
                    foreach ($items as $item) {
                        $orderItem = $this->orderItemRepository->get($item['order_item_id']);
                        if ($orderItem->getParentItemId()) {
                            $parentOrderItem = $this->orderItemRepository->get($orderItem->getParentItemId());
                            $parentOrderItem->setOrderId($newOrder->getId());
                            $this->orderItemRepository->save($parentOrderItem);
                        }
                        $orderItem->setOrderId($newOrderId);
                        $vendorItem = new \Magento\Framework\DataObject($item);
                        $vendorItem->setData('id', $item['order_item_id']);
                        $vendorItems[] = $vendorItem;
                        $this->orderItemRepository->save($orderItem);
                        $item['order_id'] = $newOrderId;
                        $result[$item['key']]['order_id'] = $newOrderId;
                    }
                    $newOrder->setState($this->originOrder->getState());
                    $newOrder->setStatus($this->originOrder->getStatus());
                    list($order,$remainDiscount,$baseRemainDiscount) = $this->changeDataOrder(
                        $result,
                        $items,
                        $newOrder,
                        $this->setShippingData($newOrder, $vendorId, $wh, $baseShippingAmount),
                        $wh == 0,
                        $remainDiscount,
                        $baseRemainDiscount
                    );
                    $orders[] = $order;
                    $oderVendors = $this->_vendorOrderFactory->create();
                    $vendorArrayDatas = $this->getVendorOrderData($vendorId, $order);
                    foreach ($vendorArrayDatas as $key => $value) {
                        if ($order->getData($key)) {
                            $vendorArrayDatas[$key] = $order->getData($key);
                        }
                    }
                    $oderVendors->setData($vendorArrayDatas)->setItems($vendorItems)->save();
                    if ($oderVendors->getId()) {
                        $this->_orderSender->send($oderVendors);
                    }
                    $this->_eventManager->dispatch(
                        'vnecoms_vendors_push_notification',
                        [
                            'vendor_id' => $vendorId,
                            'type' => 'sales',
                            'message' => __('New order #%1 is placed', '<strong>' . $order->getIncrementId() . '</strong>'),
                            'additional_info' => ['id' => $oderVendors->getId()],
                        ]
                    );
                } else {
                    $vendorItems = [];
                    foreach ($items as $item) {
                        $orderItem = $this->orderItemRepository->get($item['order_item_id']);
                        $vendorItem = new \Magento\Framework\DataObject($item);
                        $vendorItem->setData('id', $item['order_item_id']);
                        $vendorItems[] = $vendorItem;
                        $this->orderItemRepository->save($orderItem);
                    }
                    list($order,$remainDiscount,$baseRemainDiscount) = $this->changeDataOrder(
                        $result,
                        $items,
                        $order,
                        $this->setShippingData($order, $vendorId, $wh, $baseShippingAmount),
                        $wh == 0,
                        $remainDiscount,
                        $baseRemainDiscount
                    );
                    $orders[] = $order;
                    $this->originOrder = $order;

                    $oderVendors = $this->_vendorOrderFactory->create();
                    $vendorArrayDatas = $this->getVendorOrderData($vendorId, $order);
                    foreach ($vendorArrayDatas as $key => $value) {
                        if ($order->getData($key)) {
                            $vendorArrayDatas[$key] = $order->getData($key);
                        }
                    }
                    $oderVendors->setData($vendorArrayDatas)->setItems($vendorItems)->save();
                    if ($oderVendors->getId()) {
                        $this->_orderSender->send($oderVendors);
                    }
                    $this->_eventManager->dispatch(
                        'vnecoms_vendors_push_notification',
                        [
                            'vendor_id' => $vendorId,
                            'type' => 'sales',
                            'message' => __('New order #%1 is placed', '<strong>' . $order->getIncrementId() . '</strong>'),
                            'additional_info' => ['id' => $oderVendors->getId()],
                        ]
                    );
                }
                $numberOrder++;
            }
        }

        return [$result, $orders];
    }

    /**
     * @param array $data
     * @return array
     */
    private function beforeDataOrder($data)
    {
        $unsetKeys = ['entity_id', 'parent_id', 'item_id', 'order_id'];
        foreach ($unsetKeys as $key) {
            if (isset($data[$key])) {
                unset($data[$key]);
            }
        }

        $unsetKeys = ['increment_id', 'items', 'addresses', 'payment'];
        foreach ($unsetKeys as $key) {
            if (isset($data[$key])) {
                $data[$key] = null;
            }
        }

        return $data;
    }

    /**
     * each create order after separate
     *
     * @param array $result
     * @param $items
     * @param Order $order
     * @param float $baseShippingAmount
     * @return array
     * @throws CouldNotSaveException
     */
    private function changeDataOrder($result, $items, $order, $baseShippingAmount, $isVirtual , $remainDiscount = 0, $baseRemainDiscount = 0)
    {
        $totalQty = 0;
        $subTotal = 0;
        $baseSubTotal = 0;
        $subTotalInclTax = 0;
        $baseSubTotalInclTax = 0;
        $discount = 0;
        $baseDiscount = 0;
        $tax = 0;
        $baseTax = 0;
        foreach ($items as $item) {
            $orderItem = $this->orderItemRepository->get($item['order_item_id']);
            if ($orderItem->getParentItemId()) {
                $parentOrderItem = $this->orderItemRepository->get($orderItem->getParentItemId());
                $totalQty += $parentOrderItem->getQtyOrdered();
                $subTotal += $this->priceCurrency->round(
                    $parentOrderItem->getQtyOrdered() * $parentOrderItem->getPrice()
                );
                $baseSubTotal += $this->priceCurrency->round(
                    $parentOrderItem->getQtyOrdered() * $parentOrderItem->getBasePrice()
                );
                $subTotalInclTax += $this->priceCurrency->round(
                    $parentOrderItem->getQtyOrdered() * $parentOrderItem->getPriceInclTax()
                );
                $baseSubTotalInclTax += $this->priceCurrency->round(
                    $parentOrderItem->getQtyOrdered() * $parentOrderItem->getBasePriceInclTax()
                );
                if ($parentOrderItem->getDiscountPercent()) {
                    $discount += $this->priceCurrency->round(
                        $subTotal * ($parentOrderItem->getDiscountPercent() / 100)
                    );
                    $baseDiscount += $this->priceCurrency->round(
                        $baseSubTotal * ($parentOrderItem->getDiscountPercent() / 100)
                    );
                }
                if ($parentOrderItem->getTaxPercent()) {
                    $tax += $this->priceCurrency->round(
                        $subTotal * ($parentOrderItem->getTaxPercent() / 100)
                    );
                    $baseTax += $this->priceCurrency->round(
                        $baseSubTotal * ($parentOrderItem->getTaxPercent() / 100)
                    );
                }
            } else {
                if ($orderItem->getPrice() > 0) {
                    $totalQty += $orderItem->getQtyOrdered();
                    $subTotal += $this->priceCurrency->round(
                        $orderItem->getQtyOrdered() * $orderItem->getPrice()
                    );
                    $baseSubTotal += $this->priceCurrency->round(
                        $orderItem->getQtyOrdered() * $orderItem->getBasePrice()
                    );
                    $subTotalInclTax += $this->priceCurrency->round(
                        $orderItem->getQtyOrdered() * $orderItem->getPriceInclTax()
                    );
                    $baseSubTotalInclTax += $this->priceCurrency->round(
                        $orderItem->getQtyOrdered() * $orderItem->getBasePriceInclTax()
                    );
                    if ($orderItem->getDiscountPercent()) {
                        $discount += $this->priceCurrency->round(
                            $subTotal * ($orderItem->getDiscountPercent() / 100)
                        );
                        $baseDiscount += $this->priceCurrency->round(
                            $baseSubTotal * ($orderItem->getDiscountPercent() / 100)
                        );
                    }
                    if ($orderItem->getTaxPercent()) {
                        $tax += $this->priceCurrency->round(
                            $subTotal * ($orderItem->getTaxPercent() / 100)
                        );
                        $baseTax += $this->priceCurrency->round(
                            $baseSubTotal * ($orderItem->getTaxPercent() / 100)
                        );
                    }
                }
            }
        }
        $amountDiscount = $discount;
        $baseAmountDiscount = $baseDiscount;
        if ($discount > 0) {
            $amountDiscount = -$discount;
            $baseAmountDiscount = -$baseDiscount;
        }
        $shippingAmount = $this->priceCurrency->convert($this->priceCurrency->round($baseShippingAmount));
        $payment = $order->getPayment();
        $grandTotal = $subTotal - $discount + $tax + $shippingAmount;
        $baseGrandTotal = $baseSubTotal - $baseDiscount + $baseTax + $baseShippingAmount;
        if($order->getData('mageworx_giftcards_amount')){
            if ($payment->getMethod() == 'free') {
                $payment->setMethod('bazzarrypayment');
                $grandTotal = 0;
                $baseGrandTotal = 0;
                $order->setData('mageworx_giftcards_amount',-$grandTotal);
                $order->setData('base_mageworx_giftcards_amount',-$baseGrandTotal);
            }else{
                if ($baseRemainDiscount > (float)$baseGrandTotal){
                    $baseRemainDiscount = $baseRemainDiscount - (float)$baseGrandTotal;
                    $remainDiscount = $remainDiscount - (float)$grandTotal;
                    $order->setData('mageworx_giftcards_amount',-$grandTotal);
                    $order->setData('base_mageworx_giftcards_amount',-$baseGrandTotal);
                    $grandTotal = 0;
                    $baseGrandTotal = 0;
                }else{
                    $grandTotal = $grandTotal - $remainDiscount;
                    $baseGrandTotal = $baseGrandTotal - $baseRemainDiscount;
                    $order->setData('mageworx_giftcards_amount',-$remainDiscount);
                    $order->setData('base_mageworx_giftcards_amount',-$baseRemainDiscount);
                    $remainDiscount = 0;
                    $baseRemainDiscount = 0;
                }
            }
        }
        if ($isVirtual){
            $order->setIsVirtual(1);
        }else{
            $order->setIsVirtual(0);
        }
        $order->setPayment($payment);
        $order->setBaseDiscountAmount($baseAmountDiscount);
        $order->setDiscountAmount($amountDiscount);
        $order->setBaseTaxAmount($baseTax);
        $order->setTaxAmount($tax);
        $order->setBaseGrandTotal($baseGrandTotal);
        $order->setGrandTotal($grandTotal);
        $order->setBaseSubtotal($baseSubTotal);
        $order->setSubtotal($subTotal);
        $order->setTotalQtyOrdered($totalQty);
        $order->setBaseSubtotalInclTax($baseSubTotalInclTax);
        $order->setSubtotalInclTax($subTotalInclTax);
        $order->setBaseTotalDue($baseSubTotal - $baseDiscount);
        $order->setTotalDue($subTotal - $discount);
        $order->setBaseShippingAmount($baseShippingAmount);
        $order->setBaseShippingInclTax($baseShippingAmount);
        $order->setShippingAmount($shippingAmount);
        $order->setShippingInclTax($shippingAmount);
        $this->orderRepository->save($order);

        return [$order,$remainDiscount,$baseRemainDiscount];
    }

    /**
     * @param Order $order
     * @param int $warehouse
     * @param float $amount
     * @return float
     */
    private function setShippingData($order, $vendorId, $warehouse, $amount)
    {
        if ($warehouse == 0){
            return 0;
        }
        if (!empty($this->registry->registry('amasty_quote_methods'))) {
            $shippings = $this->registry->registry('amasty_quote_methods');
            $shippingMethod = $order->getShippingMethod(true);
            $method = str_replace('multirate_', '', $shippingMethod->getMethod());

            $quote = $this->quoteFactory->create()->load($order->getQuoteId());
            if ($quote->getId()) {
                $shippingDescription = $quote->getShippingAddress()->getShippingDescription();
            } else {
                $shippingDescription = $order->getShippingDescription();
            }
            $shippingDescription = str_replace('Multiple Rate - ', '', $shippingDescription);
            if (!$this->shippingMethod) {
                $this->shippingMethod = $method;
            }
            if (!$this->shippingDescription) {
                $this->shippingDescription = $shippingDescription;
            }
            $listMethod = explode('|_|', $this->shippingMethod);
            $listDes = explode('|_|', $this->shippingDescription);
            if ($listMethod) {
                $appliedMethod = null;
                $appliedDes = null;
                foreach ($listMethod as $method) {
                    $methodData = explode('||', $method);
                    if (count($methodData) != 3) {
                        continue;
                    }
                    if ($warehouse == $methodData[1] && $vendorId == $methodData[2]) {
                        $appliedMethod = $method;
                        break;
                    }
                }
                foreach ($listDes as $des) {
                    $desData = explode('||', $des);
                    if (isset($desData[1]) && $warehouse == $desData[1] && $vendorId == $desData[2]) {
                        $appliedDes = $desData[0];
                        break;
                    }
                }
                if ($appliedMethod) {
                    $list = $shippings[$warehouse];
                    foreach ($list as $resultMethod) {
                        if ('amstrates_' . $resultMethod['method'] == $appliedMethod && $resultMethod['carrier_code'] == 'amstrates') {
                            $amount = $resultMethod['price'];
                            $order->setShippingMethod($appliedMethod);
                            $order->setShippingDescription($appliedDes ?: $this->shippingDescription);
                            break;
                        }
                    }
                }
            }
        }

        return $amount;
    }

    /**
     * @param $vendorId
     * @param $order
     * @return array
     */
    public function getVendorOrderData($vendorId, $order)
    {
        $currentTimestamp = (new \DateTime())->getTimestamp();
        $orderData = [
            'vendor_id' => $vendorId,
            'order_id' => $order->getId(),
            'status' => $order->getStatus(),
            'subtotal' => 0,
            'weight' => 0,
            'shipping_description' => '',
            'grand_total' => 0,
            'created_at' => $currentTimestamp,
            'updated_at' => $currentTimestamp,
            'tax_amount' => 0,
            'base_subtotal' => 0,
            'base_tax_amount' => 0,
            'discount_amount' => 0,
            'shipping_amount' => 0,
            'shipping_incl_tax' => 0,
            'subtotal_incl_tax' => 0,
            'base_subtotal_incl_tax' => 0,
            'shipping_method' => '',
            'base_discount_amount' => 0,
            'base_grand_total' => 0,
            'base_shipping_amount' => 0,
            'shipping_tax_amount' => 0,
            'base_shipping_tax_amount' => 0,
            'base_shipping_incl_tax' => 0,
            'total_due' => 0,
            'base_total_due' => 0,
        ];
        return $orderData;
    }
}
