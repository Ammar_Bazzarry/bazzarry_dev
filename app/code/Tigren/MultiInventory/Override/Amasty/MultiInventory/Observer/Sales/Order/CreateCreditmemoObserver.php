<?php
namespace Tigren\MultiInventory\Override\Amasty\MultiInventory\Observer\Sales\Order;

use Amasty\MultiInventory\Helper\System as HelperSystem;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\App\RequestInterface;

/**
 * Class CreateCreditmemoObserver
 * @package Tigren\MultiInventory\Override\Amasty\MultiInventory\Observer\Sales\Order
 */
class CreateCreditmemoObserver extends \Amasty\MultiInventory\Observer\Sales\Order\CreateCreditmemoObserver
{

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface
     */
    private $itemRepository;

    /**
     * @var \Amasty\MultiInventory\Logger\Logger
     */
    private $logger;

    /**
     * @var \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor
     */
    private $processor;

    /**
     * CreateCreditmemoObserver constructor.
     * @param \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Order\Item\CollectionFactory $collectionFactory
     * @param \Amasty\MultiInventory\Helper\Data $helper
     * @param HelperSystem $system
     * @param \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface $itemRepository
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Amasty\MultiInventory\Logger\Logger $logger
     * @param \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor
     * @param RequestInterface $request
     */
    public function __construct(
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Order\Item\CollectionFactory $collectionFactory,
        \Amasty\MultiInventory\Helper\Data $helper,
        \Amasty\MultiInventory\Helper\System $system,
        \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface $itemRepository,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Amasty\MultiInventory\Logger\Logger $logger,
        \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor,
        RequestInterface $request
    ) {
        parent::__construct($collectionFactory, $helper, $system,$messageManager,$request);
        $this->logger = $logger;
        $this->messageManager = $messageManager;
        $this->itemRepository = $itemRepository;
        $this->request = $request;
        $this->processor = $processor;
    }

    /**
     * @param \Amasty\MultiInventory\Model\Warehouse\Order\Item $item
     * @param \Magento\Sales\Model\Order\Creditmemo $entity
     */
    protected function processItem($item, $entity)
    {
        $orderItemId = $item->getOrderItemId();
        $params = $this->request->getParams();
        if (isset($params['creditmemo']['items'][$orderItemId]['back_to_stock'])
            && $params['creditmemo']['items'][$orderItemId]['back_to_stock']
        ) {
            $item->setData('qty_return',$params['creditmemo']['items'][$orderItemId]['qty']);
            $this->setReturn($item, $entity);
            $this->messageManager->addNoticeMessage(
                __(
                    'The returned item(s) affected the product quantity in the appropriate Warehouse %1.',
                    $item->getWarehouse()->getTitle()
                )
            );
        }
    }


    /**
     * @param $item
     * @param $entity
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function setReturn($item, $entity)
    {
        $fields = $item->toArray();
        $fields['qty_ordered'] = $this->getQtyToReturn($entity, $fields);
        $warehouse = $fields['warehouse_id'];
        $this->setReturnQty($fields, $warehouse);
    }

    /**
     * @param $fields
     * @param $warehouseId
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function setReturnQty($fields, $warehouseId)
    {
        $stockItem = $this->itemRepository->getByProductWarehouse($fields['product_id'], $warehouseId);
        $oldQty = $stockItem->getQty();
        $stockItem->setQty($stockItem->getQty() + $fields['qty_return']);

        $stockItem->recalcAvailable();
        $this->itemRepository->save($stockItem);
        if ($this->system->isEnableLog()) {
            $this->logger->infoWh(
                $stockItem->getProduct()->getSku(),
                $stockItem->getProductId(),
                $stockItem->getWarehouse()->getTitle(),
                $stockItem->getWarehouse()->getCode(),
                $oldQty,
                $stockItem->getQty(),
                'creditmemo'
            );
        }
        $this->processor->reindexRow($fields['product_id']);
    }

    /**
     * @param $entity
     * @param $field
     * @return int
     */
    private function getQtyToReturn($entity, $field)
    {
        $itemId = $field['parent_item_id'] ?: $field['order_item_id'];

        foreach ($entity['items'] as $record) {
            if ($record->getOrderItemId() == $itemId) {
                return $record->getQty();
            }
        }

        return 0;
    }

}
