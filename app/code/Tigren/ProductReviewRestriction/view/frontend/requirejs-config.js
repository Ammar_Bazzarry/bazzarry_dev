/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            'Magento_Review/js/view/review' : 'Tigren_ProductReviewRestriction/js/view/review'
        }
    }
};
