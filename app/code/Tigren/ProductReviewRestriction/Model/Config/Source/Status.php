<?php

namespace Tigren\ProductReviewRestriction\Model\Config\Source;


class Status implements \Magento\Framework\Option\ArrayInterface
{
    protected $statusCollection;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Status\Collection $statusCollection
    )
    {
        $this->statusCollection = $statusCollection;
    }

    public function toOptionArray()
    {
        return $this->statusCollection->toOptionArray();
    }
}
