<?php

namespace Tigren\ProductReviewRestriction\Controller\Review;

use Magento\Framework\App\Action\Context;

/**
 * Class Validate
 * @package Tigren\ProductReviewRestrict\Controller\Review
 */
class Validate extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_order;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;
    /**
     * @var
     */
    protected $_customerSession;
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;
    /**
     * @var \Magento\Review\Model\ResourceModel\Review\CollectionFactory
     */
    protected $_reviewCollection;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Validate constructor.
     * @param Context $context
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $order
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollection
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $order,
        \Magento\Framework\Registry $registry,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollection,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_order = $order;
        $this->_registry = $registry;
        $this->_objectManager = $objectManager;
        $this->_customerSession = $this->_objectManager->create('Magento\Customer\Model\SessionFactory')->create();
        $this->_reviewCollection = $reviewCollection;
        $this->scopeConfig = $scopeConfig;
        parent::__construct(
            $context
        );
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $productid = $this->getRequest()->getParam('product_id');
        $result = [];
        $result['allow'] = 0;
        $customer = $this->getCustomerSession();
        if ($productid && $customer && $customer->getId()) {
            $orderCollection = $this->getOrderCollection($productid);
            if ($orderCollection->count() > 0) {
                $reviewCollection = $this->_reviewCollection->create()
                    ->addEntityFilter(
                        'product',
                        $productid
                    )->addCustomerFilter($customer->getId())
                    ->setDateOrder();
                if ($reviewCollection->count() < (int)$this->getMaxReview()) {
                    $result['allow'] = 1;
                }else{
                    $result['max_review'] = 1;
                }
            }
        }
        return $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
        );
    }

    /**
     * @return int
     */
    protected function getMaxReview()
    {
        return $this->scopeConfig->getValue('catalog/review/max_review') ?: 1;
    }

    /**
     * @param $productid
     * @return mixed
     */
    public function getOrderCollection($productid)
    {
        $orderStatus = $this->scopeConfig->getValue('catalog/review/order_status');

        $orderCollection = $this->_order->create()
            ->addFieldToFilter('customer_email', $this->getCustomerSession()->getEmail());
        if ($orderStatus) {
            $orderStatus = explode(',', $orderStatus);
            $orderCollection->addFieldToFilter('status', ['in' => $orderStatus]);
        }
        $orderCollection->getSelect()
            ->join(
                'sales_order_item',
                'main_table.entity_id = sales_order_item.order_id'
            )->where('product_id = ' . $productid);
        $orderCollection->getSelect()->limit(1);
        return $orderCollection;
    }

    /**
     * @return mixed
     */
    public function getCustomerSession()
    {
        if ($this->_customerSession->isLoggedIn()) {
            $email = $this->_customerSession->getCustomer();
            return $email;
        }
    }
}
