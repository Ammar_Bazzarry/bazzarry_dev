<?php

namespace Tigren\ProductReviewRestriction\Block;

class Form extends \Magento\Review\Block\Form
{
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Review\Helper\Data $reviewData,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Model\Url $customerUrl,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = [],
        \Magento\Framework\Serialize\Serializer\Json $serializer = null
    ) {
        parent::__construct($context, $urlEncoder, $reviewData, $productRepository, $ratingFactory, $messageManager,
            $httpContext, $customerUrl, $data
        );
        $this->scopeConfig = $scopeConfig;
        $this->jsLayout = isset($data['jsLayout']) ? $data['jsLayout'] : [];
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
    }


    public function getJsLayout()
    {
        if (isset($this->jsLayout['components'])) {
            $this->jsLayout['components']['review-form']['provider'] = 'reviewProvider';
            $provider = ['component' => 'uiComponent', 'productId' => $this->getProductId()];
            $this->jsLayout['components']['reviewProvider'] = $provider;
        }

        return $this->serializer->serialize($this->jsLayout);
    }

    public function getMaxReView()
    {
        return $this->scopeConfig->getValue('catalog/review/max_review') ?: 1;
    }
}
