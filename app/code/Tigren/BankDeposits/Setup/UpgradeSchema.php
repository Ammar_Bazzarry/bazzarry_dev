<?php

namespace Tigren\BankDeposits\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $setup->getConnection();
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'bank_receipt_url',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'bank_receipt_url'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order_grid'),
                'bank_receipt_url',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'bank_receipt_url'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('ves_vendor_sales_order'),
                'bank_receipt_url',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'bank_receipt_url'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('ves_vendor_sales_order'),
                'bank_receipt_url',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'bank_receipt_url'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('ves_vendor_sales_order'),
                'bank_name',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'bank_receipt_url'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('ves_vendor_sales_order'),
                'customer_bank_account',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => '',
                    'nullable' => true,
                    'comment' => 'Customer Bank Account for Bankdeposits Payment'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'bank_name',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => '',
                    'nullable' => true,
                    'comment' => 'Selected Bank for Bankdeposits Payment'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'customer_bank_account',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => '',
                    'nullable' => true,
                    'comment' => 'Customer Bank Account for Bankdeposits Payment'
                ]
            );
        }

        $setup->endSetup();
    }
}
