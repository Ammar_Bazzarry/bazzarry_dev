<?php

namespace Tigren\BankDeposits\Block\Adminhtml\Order\View;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Http\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Payment\Helper\Data;
use Magento\Sales\Block\Order\View;

class ViewReceipt extends View
{
    /**
     * @var Filesystem
     */
    protected $_fileSystem;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Registry $registry,
        Context $httpContext,
        Data $paymentHelper,
        Filesystem $fileSystem,
        array $data = []
    )
    {
        $this->_fileSystem = $fileSystem;
        parent::__construct($context, $registry, $httpContext, $paymentHelper, $data);
    }

    public function getDepositsUrl()
    {
        $order = $this->getOrder();
        $depositsPath = $order->getData('bank_receipt_url');


        $path = $this->_fileSystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath(
            '/bankreceipt/'
        );

        if ($depositsPath && file_exists($path . $depositsPath)) {
            try {
                $path = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
                return $path . 'bankreceipt/' . $depositsPath;
            } catch (NoSuchEntityException $e) {
            }
        } else {
            return '';
        }
    }
    public function getBankInfo(){
        $order = $this->getOrder();
        return $data = [
          'bank_name' => $order->getData('bank_name'),
          'bank_account' => $order->getData('customer_bank_account')
        ];
    }
}