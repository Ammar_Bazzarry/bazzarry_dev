<?php

namespace Tigren\BankDeposits\Model\Config\Source;


class OrderStatus implements \Magento\Framework\Option\ArrayInterface
{
    protected $statusCollection;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Status\Collection $statusCollection
    )
    {
        $this->statusCollection = $statusCollection;
    }

    public function toOptionArray()
    {
        return $this->statusCollection->toOptionArray();
    }
}
