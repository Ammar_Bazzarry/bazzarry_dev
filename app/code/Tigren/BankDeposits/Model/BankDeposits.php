<?php
/**
 * Copyright (c) 2017 www.tigren.com
 */

namespace Tigren\BankDeposits\Model;

/**
 * Pay In Store payment method model
 */
class BankDeposits extends \Magento\Payment\Model\Method\AbstractMethod
{
    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'bankdeposits';

    /**
     * @return string
     */
    public function getInstructions()
    {
        return trim($this->getConfigData('instructions'));
    }
}