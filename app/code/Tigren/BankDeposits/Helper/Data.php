<?php

namespace Tigren\BankDeposits\Helper;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Data
 * @package Tigren\BankDeposits\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_fileSystem;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Filesystem $fileSystem
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $fileSystem
    )
    {
        parent::__construct($context);
        $this->_fileSystem = $fileSystem;
        $this->_storeManager = $storeManager;
    }

    /**
     * @param $depositsPath
     * @return string
     */
    public function getDepositsUrl($depositsPath)
    {

        $path = $this->_fileSystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath(
            '/bankreceipt/'
        );

        if ($depositsPath && file_exists($path . $depositsPath)) {
            try {
                $path = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                return $path . 'bankreceipt/' . $depositsPath;
            } catch (NoSuchEntityException $e) {
            }
        } else {
            return '';
        }

    }

    public function canUpload($orderStatus){
        $result = false;
        $allowOrderStatus = $this->scopeConfig->getValue('payment/bankdeposits/specific_order_status');
        if ($allowOrderStatus){
            $allowOrderStatus = explode(',',$allowOrderStatus);
            if (in_array($orderStatus,$allowOrderStatus)){
                $result = true;
            }
        }else{
            $result = true;
        }
        return $result;
    }
}
