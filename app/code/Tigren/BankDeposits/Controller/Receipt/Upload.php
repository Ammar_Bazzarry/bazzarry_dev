<?php

namespace Tigren\BankDeposits\Controller\Receipt;

use Magento\Framework\App\Action\Context;

/**
 * Class ProductReviewSaveAfter
 *
 * @package RLTSquare\ProductReviewImages\Observer
 * @author Umar Chaudhry <umarch@rltsquare.com>
 */
class Upload extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
     */
    protected $_mediaDirectory;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    protected $_vendorOrder;

    /**
     * Upload constructor.
     * @param Context $context
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Vnecoms\VendorsSales\Model\Order $vendorOrder
    )
    {
        parent::__construct($context);
        $this->_vendorOrder = $vendorOrder;
        $this->_request = $request;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_orderFactory = $orderFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!isset($data['order_id'])) {
            return;
        }
        $order = $this->_orderFactory->create()->load($data['order_id']);
        $media = $this->_request->getFiles('bank_receipt');
        $target = $this->_mediaDirectory->getAbsolutePath('/bankreceipt/');
        if ($media) {
            try {
                $uploader = $this->_fileUploaderFactory->create(['fileId' => 'bank_receipt']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png']);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $uploader->setAllowCreateFolders(true);
                $result = $uploader->save($target);
                $order->setData('bank_receipt_url',$result['file']);
                $order->setStatus('review');
                $order->save();
                $collection = $this->_vendorOrder->getCollection()
                    ->addFieldToFilter('order_id',$data['order_id']);
                foreach ($collection as $vendorOrder){
                    $vendorOrder->setData('bank_receipt_url',$result['file']);
                    $vendorOrder->setStatus('review');
                    $vendorOrder->save();
                }
                $this->messageManager->addSuccess(__("Upload bank receipt complete."));
            } catch (\Exception $e) {
                if ($e->getCode() == 0) {
                    $this->messageManager->addError(__("Something went wrong while upload bank receipt."));
                }
            }
        }
        return $resultRedirect->setPath('sales/order/view/',['order_id'=>$data['order_id']]);
    }
}