<?php
/**
 * Copyright (c) 2017 www.tigren.com
 */

namespace Tigren\BankDeposits\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class BeforeOrderPaymentSaveObserver
 * @package Tigren\MultiCOD\Observer
 */
class BeforeOrderSave implements ObserverInterface
{
    /**
     * @param Observer $observer
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getData('order');
        $payment = $order->getPayment();
        $data = $payment->getData('additional_information');
        if (isset($data['bank_name']) && isset($data['bank_account'])){
            $order->setData('bank_name', $data['bank_name']);
            $order->setData('customer_bank_account', $data['bank_account']);
        }
    }
}
