<?php

namespace Tigren\BankDeposits\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;

class PaymentAdditionalDataAssignObserver extends AbstractDataAssignObserver
{
    const BANK_NAME = 'bank_name';
    const BANK_ACCOUNT = 'bank_account';

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $data = $this->readDataArgument($observer);

        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additionalData) || !isset($additionalData[self::BANK_NAME])) {
            return; // or throw exception depending on your logic
        }
        if (!is_array($additionalData) || !isset($additionalData[self::BANK_ACCOUNT])) {
            return; // or throw exception depending on your logic
        }

        $paymentInfo = $this->readPaymentModelArgument($observer);
        $paymentInfo->setAdditionalInformation(
            self::BANK_NAME,
            $additionalData[self::BANK_NAME]
        );
        $paymentInfo->setAdditionalInformation(
            self::BANK_ACCOUNT,
            $additionalData[self::BANK_ACCOUNT]
        );
    }
}
