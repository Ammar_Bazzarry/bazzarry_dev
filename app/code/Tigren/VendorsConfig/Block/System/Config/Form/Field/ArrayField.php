<?php
/**
 * *
 *  * @author Tigren Solutions <info@tigren.com>
 *  * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 *  * @license Open Software License ("OSL") v. 3.0
 *
 */

namespace Tigren\VendorsConfig\Block\System\Config\Form\Field;

use Tigren\VendorsConfig\Block\System\Config\Form\Field\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\BlockInterface;

/**
 * Class Ranges
 */
class ArrayField extends AbstractFieldArray
{
    /**
     * @var Type
     */
    private $type;
    /**
     * @var Day
     */
    private $day;
    /**
     * @var Hour
     */
    private $hour;
    /**
     * @var string
     */
    protected $_template = 'Tigren_VendorsConfig::system/config/array.phtml';

    /**
     * Prepare rendering the new field by adding all the needed columns
     */
    protected function _prepareToRender()
    {
        $this->addColumn('hour_from', [
            'label' => __('From'),
            'renderer' => $this->getHour()
        ]);
        $this->addColumn('type_from', [
            'label' => __(''),
            'renderer' => $this->getType()
        ]);
        $this->addColumn('to_hour', [
            'label' => __('To'),
            'renderer' => $this->getHour()
        ]);
        $this->addColumn('type_to', [
            'label' => __(''),
            'renderer' => $this->getType()
        ]);
        $this->addColumn('day_from', [
            'label' => __('Day From'),
            'renderer' => $this->getDay()
        ]);
        $this->addColumn('day_to', [
            'label' => __('Day To'),
            'renderer' => $this->getDay()
        ]);
        $this->_addAfter = false;
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @throws LocalizedException
     */
    protected function _prepareArrayRow(DataObject $row): void
    {
        $options = [];
        $options['from_hour'] = $row->getData('hour_from');
        $options['type_from'] = $row->getData('type_from');
        $options['to_hour'] = $row->getData('to_hour');
        $options['type_to'] = $row->getData('type_to');
        $options['day_from'] = $row->getData('day_from');
        $options['day_to'] = $row->getData('day_to');

        $row->setData('option_extra_attrs', $options);
    }

    /**
     * @return Type
     * @throws LocalizedException
     */
    private function getType()
    {
        if (!$this->type) {
            $this->type = $this->getLayout()->createBlock(
                Type::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->type;
    }

    /**
     * @return BlockInterface|Day
     * @throws LocalizedException
     */
    private function getDay()
    {
        if (!$this->day) {
            $this->day = $this->getLayout()->createBlock(
                Day::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->day;
    }

    /**
     * @return BlockInterface|Hour
     * @throws LocalizedException
     */
    private function getHour()
    {
        if (!$this->hour) {
            $this->hour = $this->getLayout()->createBlock(
                Hour::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->hour;
    }
}
