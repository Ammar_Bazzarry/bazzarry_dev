<?php
/**
 * *
 *  * @author Tigren Solutions <info@tigren.com>
 *  * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 *  * @license Open Software License ("OSL") v. 3.0
 *
 */

declare(strict_types=1);

namespace Tigren\VendorsConfig\Block\System\Config\Form\Field;

use Magento\Framework\View\Element\Html\Select;

/**
 * Class Type
 */
class Type extends Select
{
    /**
     * Set "name" for <select> element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Set "id" for <select> element
     *
     * @param $value
     * @return $this
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml(): string
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->getSourceOptions());
        }
        return parent::_toHtml();
    }

    /**
     * @return array
     */
    private function getSourceOptions(): array
    {
        return [
            ['label' => 'a.m', 'value' => '1'],
            ['label' => 'p.m', 'value' => '0'],
        ];
    }
}
