<?php
/**
 * *
 *  * @author Tigren Solutions <info@tigren.com>
 *  * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 *  * @license Open Software License ("OSL") v. 3.0
 *
 */

declare(strict_types=1);

namespace Tigren\VendorsConfig\Block\System\Config\Form\Field;

use Magento\Framework\View\Element\Html\Select;

class Hour extends Select
{
    /**
     * Set "name" for <select> element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Set "id" for <select> element
     *
     * @param $value
     * @return $this
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml(): string
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->getSourceOptions());
        }
        return parent::_toHtml();
    }

    private function getSourceOptions(): array
    {
        return [
            ['label' => '1', 'value' => '1'],
            ['label' => '2', 'value' => '2'],
            ['label' => '3', 'value' => '3'],
            ['label' => '4', 'value' => '4'],
            ['label' => '5', 'value' => '5'],
            ['label' => '6', 'value' => '6'],
            ['label' => '7', 'value' => '7'],
            ['label' => '8', 'value' => '8'],
            ['label' => '9', 'value' => '9'],
            ['label' => '10', 'value' => '10'],
            ['label' => '11', 'value' => '11'],
            ['label' => '12', 'value' => '12']
        ];
    }
}
