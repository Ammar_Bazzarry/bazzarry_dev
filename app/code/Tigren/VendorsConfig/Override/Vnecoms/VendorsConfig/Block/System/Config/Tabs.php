<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

/**
 * System configuration tabs block
 *
 * @method setTitle(string $title)
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace Tigren\VendorsConfig\Override\Vnecoms\VendorsConfig\Block\System\Config;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Tabs extends \Magento\Config\Block\System\Config\Tabs
{
    /**
     * Block template filename
     *
     * @var string
     */
    protected $_template = 'Tigren_VendorsConfig::system/config/tabs.phtml';
}
