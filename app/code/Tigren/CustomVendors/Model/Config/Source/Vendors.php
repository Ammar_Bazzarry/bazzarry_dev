<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomVendors\Model\Config\Source;

class Vendors implements \Magento\Framework\Option\ArrayInterface
{
    protected $vendorsCollection;

    public function __construct(
        \Vnecoms\Vendors\Model\ResourceModel\Vendor\CollectionFactory $vendorsCollection
    ) {
        $this->vendorsCollection = $vendorsCollection;
    }

    public function toOptionArray()
    {
        $collection = $this->vendorsCollection->create();
        $result = [];
        foreach ($collection as $vendor) {
            $result[] = [
                'label' => $vendor->getVendorId() ,
                'value' => $vendor->getId()
            ];
        }
        return $result;
    }
}
