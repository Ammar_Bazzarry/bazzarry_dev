<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

// @codingStandardsIgnoreFile

namespace Tigren\CustomVendors\Override\Magento\Backend\Block;

class Menu extends \Magento\Backend\Block\Menu
{

    /**
     * Replace Callback Secret Key
     *
     * @param string[] $match
     * @return string
     */
    protected function _callbackSecretKey($match)
    {
        $ruoteConfig = \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\App\Route\ConfigInterface::class);
        $routeId = $ruoteConfig->getRouteByFrontName($match[1]);
        return \Magento\Backend\Model\UrlInterface::SECRET_KEY_PARAM_NAME . '/' . $this->_url->getSecretKey(
            $routeId?:$match[1],
            $match[2],
            $match[3]
        );
    }

}
