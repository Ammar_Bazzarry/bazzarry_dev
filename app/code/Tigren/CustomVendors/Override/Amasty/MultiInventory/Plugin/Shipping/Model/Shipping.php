<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomVendors\Override\Amasty\MultiInventory\Plugin\Shipping\Model;

use Amasty\MultiInventory\Api\WarehouseRepositoryInterface;
use Amasty\MultiInventory\Model\Warehouse;
use Amasty\MultiInventory\Model\WarehouseFactory;
use Magento\Quote\Model\Quote\Item;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Shipping
 * @package Tigren\CustomVendors\Override\Amasty\MultiInventory\Plugin\Shipping\Model
 */
class Shipping
{
    /*Characters between method and vendor_id*/
    /**
     *
     */
    const SEPARATOR = '||';

    /*Characters between methods*/
    /**
     *
     */
    const METHOD_SEPARATOR = '|_|';

    /**
     * @var \Amasty\MultiInventory\Model\Warehouse\Cart
     */
    private $cart;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /**
     * @var WarehouseFactory
     */
    private $factory;

    /**
     * @var WarehouseRepositoryInterface
     */
    private $repostiory;

    /**
     * @var \Amasty\MultiInventory\Helper\Cart
     */
    private $helperCart;

    /**
     * @var \Amasty\MultiInventory\Model\ShippingFactory
     */
    private $whShipping;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    private $manager;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * Shipping constructor.
     * @param Warehouse\Cart $cart
     * @param WarehouseFactory $factory
     * @param \Amasty\MultiInventory\Helper\System $system
     * @param \Amasty\MultiInventory\Helper\Cart $helperCart
     * @param \Magento\Framework\Registry $registry
     * @param \Amasty\MultiInventory\Model\ShippingFactory $whShipping
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Amasty\MultiInventory\Model\Warehouse\Cart $cart,
        \Amasty\MultiInventory\Model\WarehouseFactory $factory,
        \Amasty\MultiInventory\Api\WarehouseRepositoryInterface $repository,
        \Amasty\MultiInventory\Helper\System $system,
        \Amasty\MultiInventory\Helper\Cart $helperCart,
        \Magento\Framework\Registry $registry,
        \Amasty\MultiInventory\Model\ShippingFactory $whShipping,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Module\Manager $manager,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
    )
    {
        $this->cart = $cart;
        $this->factory = $factory;
        $this->repostiory = $repository;
        $this->system = $system;
        $this->helperCart = $helperCart;
        $this->whShipping = $whShipping;
        $this->scopeConfig = $scopeConfig;
        $this->messageManager = $messageManager;
        $this->registry = $registry;
        $this->manager = $manager;
        $this->_rateMethodFactory = $rateMethodFactory;
    }

    /**
     * @param \Magento\Shipping\Model\Shipping $shipping
     * @param \Closure $work
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @return \Magento\Shipping\Model\Shipping|mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function aroundCollectRates(
        \Magento\Shipping\Model\Shipping $shipping,
        \Closure $work,
        \Magento\Quote\Model\Quote\Address\RateRequest $request
    )
    {
        if (!$this->system->isMultiEnabled() || !$this->system->getDefinationWarehouse()) {
            return $work($request);
        }
        $oldQuoteItems = [];
        $mainVendor = $this->scopeConfig->getValue(
            'customvendor/general/main_vendor'
        );
        $hasMainProduct = false;
        $quoteItem = current($request->getAllItems());
        if ($quoteItem instanceof \Magento\Quote\Model\Quote\Item) {
            $this->cart->setQuote($quoteItem->getQuote());
        }
        if ($this->registry->registry('finish_quote_save') !== true) {
            $this->cart->addWhToItems();
            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($this->cart->getQuote()->getItemsCollection()->getItems() as $item) {
                if ($item->getId()) {
                    $oldQuoteItems[$item->getId()] = $item->getQty();
                }
                if ($item->getProduct()->getData('vendor_id') == $mainVendor) {
                    $hasMainProduct = true;
                }
                if ($item->getProduct()->getData('vendor_id') != $mainVendor) {
                    $hasVendorProduct = true;
                }
            }
        }
        $warehouses = $this->cart->getWarehouses();
        $result = $forShipResult = [];
        foreach ($warehouses as $warehouseId) {
            $warehouse = $this->repostiory->getById($warehouseId);
            $request = $this->helperCart->changeRequestAddress(
                $request,
                $warehouse->getData()
            );
            // get all cart items of current warehouse
            $items = $this->cart->getGroupItems($warehouseId);
            $groupItems = $addedParents = [];
            foreach ($items as $item) {
                $quoteItem = $this->cart->getQuote()->getItemById($item['quote_item_id']);
                if ($this->registry->registry('finish_quote_save') !== true) {
                    $quoteItem->setData(Item::KEY_QTY, $item['qty']);
                }
                $parentId = $quoteItem->getParentItemId();
                if ($parentId) {
                    $parentItem = $this->cart->getQuote()->getItemById($quoteItem->getParentItemId());
                    if (!in_array($parentId, $addedParents)) {
                        $addedParents[] = $parentId;
                        $groupItems[] = $parentItem;
                    }
                    if ($parentItem->getProductType() == 'bundle') {
                        continue;
                    }
                }
                $groupItems[] = $quoteItem;
            }
            $request = $this->helperCart->changeRequestItems($request, $groupItems, $this->cart->getQuote());
            $shipment = $this->shipmentCalculate($request, $work);
            if ($warehouse->getIsShipping()) {
                $shipment = $this->helperCart->changePrice($shipment, $warehouse);
            }
            $result[$warehouseId] = $shipment;
            $methods = [];
            foreach ($shipment->getAllRates() as $resultMethod) {
                $methods[] = [
                    'method' => $resultMethod->getMethod(),
                    'carrier_code' => $resultMethod->getCarrier(),
                    'price' => $resultMethod->getPrice()
                ];
            }

            $forShipResult[$warehouseId] = $methods;
        }
        $this->registry->unregister('amasty_quote_methods');
        $this->registry->register('amasty_quote_methods', $forShipResult);
        $shipping->getResult()->append($this->helperCart->sumShipping($result));
        foreach ($oldQuoteItems as $key => $qty) {
            $quoteItem = $this->cart->getQuote()->getItemById($key);
            if ($quoteItem) {
                $quoteItem->setData(Item::KEY_QTY, $qty);
            }
        }
        $this->registry->unregister('finish_quote_save');
        $this->registry->register('finish_quote_save', false);
        $shippingRates = $shipping->getResult()->getAllRates();
        $newVendorRates = [];
        foreach ($this->groupShippingRatesByVendor($shippingRates, $hasMainProduct) as $vendorId => $rates) {
            if (!count($newVendorRates)) {
                foreach ($rates as $rate) {
                    $newVendorRates[$rate->getCarrier() . '_' . $rate->getMethod()] = [
                        'title' => $rate->getCarrierTitle() . ' - ' . $rate->getMethodTitle() . self::SEPARATOR . $vendorId,
                        'price' => $rate->getPrice()
                    ];
                }
            } else {
                $tmpRates = [];
                foreach ($rates as $rate) {
                    foreach ($newVendorRates as $cod => $shippingRate) {
                        $tmpRates[$cod . self::METHOD_SEPARATOR . $rate->getCarrier() . '_' . $rate->getMethod()] = [
                            'title' => $shippingRate['title'] . self::METHOD_SEPARATOR . $rate->getCarrierTitle() . ' - ' . $rate->getMethodTitle() . self::SEPARATOR . $vendorId,
                            'price' => $shippingRate['price'] + $rate->getPrice(),
                        ];
                    }
                }
                $newVendorRates = $tmpRates;
            }
        }
        foreach ($newVendorRates as $code => $shippingRate) {
            $method = $this->_rateMethodFactory->create();
            $method->setCarrier('vendor_multirate');
            $method->setCarrierTitle('Multiple_Rate');

            $method->setMethod($code);
            $method->setMethodTitle($shippingRate['title']);

            $method->setPrice($shippingRate['price']);
            $method->setCost($shippingRate['price']);
            $shipping->getResult()->append($method);
        }
        return $shipping;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @param \Closure $work
     *
     * @return \Magento\Shipping\Model\Rate\Result
     */
    private function shipmentCalculate(\Magento\Quote\Model\Quote\Address\RateRequest $request, $work)
    {
        $storeId = $request->getStoreId();
        /** @var \Amasty\MultiInventory\Model\Shipping $whShipping */
        $whShipping = $this->whShipping->create();
        $limitCarrier = $request->getLimitCarrier();
        if (!$limitCarrier || $limitCarrier == 'vendor_multirate') {
            if (!$this->manager->isEnabled('Amasty_Shiprules')) {
                foreach ($this->getCarriers($storeId) as $carrierCode => $carrierConfig) {
                    $whShipping->collectCarrierRates($carrierCode, $request);
                }
            } elseif (!$this->registry->registry('is_shipping_rules')) {
                $work($request);
                $this->registry->register('is_shipping_rules', true);
            }
        } else {
            if (!is_array($limitCarrier)) {
                $limitCarrier = [$limitCarrier];
            }
            foreach ($limitCarrier as $carrierCode) {
                $carrierConfig = $this->getCarriers($storeId, $carrierCode);
                if (!$carrierConfig) {
                    continue;
                }
                $whShipping->collectCarrierRates($carrierCode, $request);
            }
        }
        return $whShipping->getResult();
    }

    /**
     * @param int $storeId
     * @param string|null $carrierCode
     *
     * @return array
     */
    private function getCarriers($storeId, $carrierCode = null)
    {
        $configPath = 'carriers';
        if ($carrierCode !== null) {
            $configPath .= '/' . $carrierCode;
        }
        return $this->scopeConfig->getValue(
            $configPath,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Group shipping rates by each vendor.
     * @param $shippingRates
     */
    public function groupShippingRatesByVendor($shippingRates, $hasMainProduct)
    {
        $rates = [];
        $mainVendor = $this->scopeConfig->getValue(
            'customvendor/general/main_vendor'
        );
        foreach ($shippingRates as $rate) {
            if (!$rate->getVendorId()) {
                if ($hasMainProduct) {
                    if (!isset($rates[$mainVendor])) {
                        $rates[$mainVendor] = [];
                    }
                    $rates[$mainVendor][] = $rate;
                } else {
                    continue;
                }
            } else {
                if (!isset($rates[$rate->getVendorId()])) {
                    $rates[$rate->getVendorId()] = [];
                }
                $rates[$rate->getVendorId()][] = $rate;
            }
        }
        ksort($rates);
        return $rates;
    }
}
