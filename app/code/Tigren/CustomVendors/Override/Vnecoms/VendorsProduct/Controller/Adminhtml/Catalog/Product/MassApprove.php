<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomVendors\Override\Vnecoms\VendorsProduct\Controller\Adminhtml\Catalog\Product;

use Magento\Backend\App\Action\Context;
use Magento\Catalog\Controller\Adminhtml\Product\Builder;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;
use Vnecoms\VendorsProduct\Model\Source\Approval;

/**
 * Class MassApprove
 * @package Tigren\CustomVendors\Override\Vnecoms\VendorsProduct\Controller\Adminhtml\Catalog\Product
 */
class MassApprove extends \Vnecoms\VendorsProduct\Controller\Adminhtml\Catalog\Product\MassApprove
{
    /**
     * @var \Tigren\CustomVendors\Helper\Data
     */
    protected $_tigrenHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * MassApprove constructor.
     * @param Context $context
     * @param Builder $productBuilder
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param \Vnecoms\VendorsProduct\Helper\Data $productHelper
     * @param \Vnecoms\Vendors\Model\VendorFactory $vendorFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Tigren\CustomVendors\Helper\Data $data
     */
    public function __construct(
        Context $context,
        Builder $productBuilder,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Vnecoms\VendorsProduct\Helper\Data $productHelper,
        \Vnecoms\Vendors\Model\VendorFactory $vendorFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Tigren\CustomVendors\Helper\Data $data
    )
    {
        $this->storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_tigrenHelper = $data;
        parent::__construct($context, $productBuilder, $filter, $collectionFactory, $productHelper, $vendorFactory);
    }


    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $productApproved = 0;
        $vendors = [];
        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($collection->getItems() as $product) {
            $product->load($product->getId());
            $vendorId = $product->getVendorId();
            if (!$vendorId) {
                continue;
            }

            if (!isset($vendors[$vendorId])) {
                $vendor = $this->vendorFactory->create();
                $vendor->load($vendorId);
                $vendors[$vendorId] = $vendor;
            }

            $vendor = $vendors[$vendorId];

            /**
             * Approve Pending updates.
             */
            if ($product->getApproval() == Approval::STATUS_PENDING_UPDATE) {
                $this->approveExistProduct($product, $vendor);
                $message = __('Updates of %1 are approved', '<strong>' . $product->getName() . '</strong>');
            } else {
                $this->productHelper->sendProductApprovedEmailToVendor($product, $vendor);
                $message = __('Product %1 is approved', '<strong>' . $product->getName() . '</strong>');
            }
            $mainVendor = $this->_scopeConfig->getValue('customvendor/general/main_vendor');
            $product->setApproval(Approval::STATUS_APPROVED)
                ->getResource()
                ->saveAttribute($product, 'approval');
            $this->_tigrenHelper->saveUrlRewrite($product);
            if ($product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                $childProductCollection = $product->getTypeInstance()->getUsedProductCollection($product);
                /** @var \Magento\Catalog\Model\Product $childProduct */
                foreach ($childProductCollection as $childProduct) {
                    $childProduct->setApproval(Approval::STATUS_APPROVED)
                        ->getResource()
                        ->saveAttribute($childProduct, 'approval');
                    if (!$childProduct->getWebsiteIds()) {
                        $childProduct->setWebsiteIds([$this->storeManager->getWebsite()->getId() => $this->storeManager->getWebsite()->getId()]);
                        $childProduct->save();
                    }
                    if ($childProduct->getData('vendor_id') != $mainVendor) {
                        $productId = $childProduct->getId();
                        $qty = $this->_tigrenHelper->getStockQty($productId);
                        $stockStatus = $childProduct->getStatus();
                        $this->_tigrenHelper->insertWarehouseItem($productId, $qty, $stockStatus);
                    }
                }
            } else {
                if ($product->getData('vendor_id') != $mainVendor && $product->getData('approval') == '2') {
                    if (!$product->getWebsiteIds()) {
                        $product->setWebsiteIds([$this->storeManager->getWebsite()->getId() => $this->storeManager->getWebsite()->getId()]);
                        $product->save();
                    }
                    $productId = $product->getId();
                    $qty = $this->_tigrenHelper->getStockQty($productId);
                    $stockStatus = $product->getStatus();
                    $this->_tigrenHelper->insertWarehouseItem($productId, $qty, $stockStatus);
                }

            }

            $this->_eventManager->dispatch(
                'vnecoms_vendors_push_notification',
                [
                    'vendor_id' => $vendor->getId(),
                    'type' => 'product_approval',
                    'message' => $message,
                    'additional_info' => ['id' => $product->getId()],
                ]
            );

            $productApproved++;
        }

        $this->messageManager->addSuccess(
            __('A total of %1 product(s) have been approved.', $productApproved)
        );

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('vendors/catalog_product/index');
    }
}
