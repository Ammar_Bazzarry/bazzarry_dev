<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomVendors\Cron;

use Vnecoms\VendorsProduct\Model\Source\Approval;
use Magento\Eav\Setup\EavSetup;

/**
 * Class AssignProductToMainVendors
 * @package Tigren\CustomVendors\Cron
 */
class AssignProductToMainVendors
{
    const DEFAULT_STORE_ID = 0;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    protected $eavSetupFactory;

    protected $helper;

    /**
     * @var \Magento\Catalog\Model\Product\Action
     */
    private $action;

    protected $productCollectionFactory;

    /**
     * AssignProductToMainVendors constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Catalog\Model\Product\Action $action
     */
    public function __construct(
        EavSetup $eavSetupFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\Product\Action $action,
        \Tigren\CustomVendors\Helper\Data $data,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        $this->helper = $data;
        $this->action = $action;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_productFactory = $productFactory;
        $this->productCollectionFactory = $productCollectionFactory;
    }

    /**
     * @throws \Exception
     */
    public function execute(){
        $mainVendor = $this->_scopeConfig->getValue('customvendor/general/main_vendor');
        $productIds = [];
        $collection = $this->productCollectionFactory->create()
            ->addAttributeToFilter('vendor_id', '0')
            ->setPageSize(200);
        /** @var \Magento\Catalog\Model\Product $product */
        if (count($collection->getData())){
            foreach ($collection->getData() as $product){
                $productIds[] = $product['entity_id'];
            }
            if (count($productIds) > 0){
                $this->action->updateAttributes($productIds, ['approval'=>Approval::STATUS_APPROVED], self::DEFAULT_STORE_ID);
                $this->helper->updateMainVendor($productIds,$mainVendor);
            }
        }

        return true;
    }
}
