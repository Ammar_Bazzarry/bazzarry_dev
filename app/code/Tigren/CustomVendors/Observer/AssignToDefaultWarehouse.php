<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomVendors\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class AssignToDefaultWarehouse
 * @package Tigren\CustomVendors\Observer
 */
class AssignToDefaultWarehouse implements ObserverInterface
{

    /**
     * @var \Tigren\CustomVendors\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * AssignToDefaultWarehouse constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Tigren\CustomVendors\Helper\Data $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Tigren\CustomVendors\Helper\Data $data
    )
    {
        $this->storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_helper = $data;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $_product */
        $_product = $observer->getProduct();
        $mainVendor = $this->_scopeConfig->getValue('customvendor/general/main_vendor');
        if ($_product->getData('vendor_id') != $mainVendor && $_product->getData('approval') == '2' && !$_product->getHasChildren()) {
            $productId = $_product->getId();
            if (!$_product->getWebsiteIds()) {
                $_product->setWebsiteIds([$this->storeManager->getWebsite()->getId() => $this->storeManager->getWebsite()->getId()]);
                $_product->save();
            }
            $qty = $this->_helper->getStockQty($productId);
            $stockStatus = $qty?1:0;
            $this->_helper->insertWarehouseItem($productId, $qty, $stockStatus);
            $this->_helper->saveUrlRewrite($_product);
        }
    }
}
