<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsSales\Override\Vnecoms\VendorsCredit\Model\ResourceModel\Escrow\Grid;

use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Psr\Log\LoggerInterface as Logger;

/**
 * App page collection
 */
class Collection extends \Vnecoms\VendorsCredit\Model\ResourceModel\Escrow\Grid\Collection
{
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->addFilterToMap('increment_id', 'invoice.increment_id');
        $this->addFilterToMap('vendor', 'vendor.vendor_id');
        return $this;
    }
}
