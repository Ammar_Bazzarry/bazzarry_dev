<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2020 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsSales\Override\Vnecoms\VendorsSales\Controller\Vendors\Order;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Vnecoms\Vendors\App\Action\Context;
use Vnecoms\VendorsSales\Model\ResourceModel\Order\CollectionFactory;
use Vnecoms\VendorsGroup\Helper\Data;
use Vnecoms\Vendors\Model\Vendor;

class MassCancel extends \Vnecoms\VendorsSales\Controller\Vendors\Order\MassCancel
{
    protected $vendorGroupHelper;
    protected $vendorModel;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        DateTime $dateTime,
        Data $vendorGroupHelper,
        Vendor $vendorModel
    ) {
        $this->vendorModel = $vendorModel;
        $this->vendorGroupHelper = $vendorGroupHelper;
        parent::__construct($context, $filter, $collectionFactory, $dateTime);
    }

    /**
     * Cancel selected orders
     *
     * @param AbstractCollection $collection
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function massAction(AbstractCollection $collection)
    {
        $orderCollection = $this->collectionFactory->create()
            ->addFieldToFilter('entity_id', ['in' => $collection->getAllIds()]);
        $countCancelOrder = 0;
        $resultRedirect = $this->resultRedirectFactory->create();
        foreach ($orderCollection as $order) {
            $vendorId = $order->getData("vendor_id");
            $currentVendor = $this->vendorModel->load($vendorId);
            $vendorGroupId = $currentVendor->getData("group_id");
            $isAllowVendorCancelOrder = $this->vendorGroupHelper->canCancelOrder($vendorGroupId);
            if ($isAllowVendorCancelOrder == 0) {
                $this->messageManager->addErrorMessage(__('You cannot cancel the order(s).'));
            } else {
                if (!$order->canCancel()) {
                    continue;
                }
                $order->cancel();
                $order->save();
                $countCancelOrder++;
            }
        }
        $countNonCancelOrder = $collection->count() - $countCancelOrder;

        if ($countNonCancelOrder && $countCancelOrder) {
            $this->messageManager->addErrorMessage(__('%1 order(s) cannot be canceled.', $countNonCancelOrder));
        } elseif ($countNonCancelOrder) {
            $this->messageManager->addErrorMessage(__('You cannot cancel the order(s).'));
        }
        if ($countCancelOrder) {
            $this->messageManager->addSuccessMessage(__('We canceled %1 order(s).', $countCancelOrder));
        }
        $resultRedirect->setPath($this->getComponentRefererUrl());
        return $resultRedirect;
    }
}
