<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

// @codingStandardsIgnoreFile

namespace Tigren\VendorsSales\Override\Vnecoms\VendorsSales\Model\ResourceModel;

/**
 * Cms page mysql resource
 */
/**
 * Class Order
 * @package Tigren\VendorsSales\Override\Vnecoms\VendorsSales\Model\ResourceModel
 */
class Order extends \Vnecoms\VendorsSales\Model\ResourceModel\Order
{
    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this|void
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        parent::_afterLoad($object);
        $orderId = $object->getOrderId();
        if ($orderId) {
            $adapter = $this->getConnection();
            $select = $adapter->select();
            $select->from(
                $this->getTable('sales_order')
            )->where(
                'entity_id = :order_id'
            );
            $bind = ['order_id' => $orderId];

            $orderData = $adapter->fetchRow($select, $bind);
            $keys = ['shipping_description','base_shipping_amount','base_grand_total','base_subtotal','base_discount_amount','base_total_invoiced','base_total_paid','base_total_refunded','base_tax_amount','discount_amount','grand_total','shipping_amount','subtotal','tax_amount','total_invoiced','total_paid','total_refunded','tax_refunded','base_tax_refunded','tax_invoiced','base_tax_invoiced','tax_canceled','base_tax_canceled','subtotal_incl_tax','base_subtotal_incl_tax','shipping_method','shipping_incl_tax','base_shipping_incl_tax','total_due','base_total_due'];
            foreach ($keys as $key){
                if (isset($orderData[$key]) && $orderData[$key]){
                    $object->setData($key,$orderData[$key]);
                }
            }
        }
    }
}
