<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsSales\Override\Vnecoms\VendorsSales\Block\Adminhtml\Order;

class View extends \Magento\Backend\Block\Widget\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Prepare button and grid
     */
    protected function _prepareLayout()
    {
        $order = $this->_coreRegistry->registry('sales_order');
        $mainVendor = $this->_scopeConfig->getValue('customvendor/general/main_vendor');
        if ($order->getId()){
            $items = $order->getItems();
            foreach ($items as $item){
                $vendorId = $item->getVendorId();
                break;
            }
            if ($vendorId != $mainVendor){
                $this->removeButton('order_creditmemo');
                $this->removeButton('order_ship');
            }
        }
        return parent::_prepareLayout();
    }
}
