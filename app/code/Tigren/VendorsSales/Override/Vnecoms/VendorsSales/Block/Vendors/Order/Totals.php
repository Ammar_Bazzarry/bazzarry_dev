<?php
/**
 * @category    Magento
 * @package     Magento_Sales
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Tigren\VendorsSales\Override\Vnecoms\VendorsSales\Block\Vendors\Order;

/**
 * Adminhtml sales order view
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Totals extends \Vnecoms\VendorsSales\Block\Vendors\Order\Totals
{
    protected function _initTotals()
    {
        parent::_initTotals();

        $order       = $this->getSource();
        $mainOrder = $order->getOrder();
        if ($mainOrder && (float)$mainOrder->getMageworxGiftcardsAmount()) {
            $data = [
                'code'       => 'mageworx_giftcards_amount',
                'label'      => $mainOrder->getMageworxGiftcardsDescription(),
                'value'      => $mainOrder->getMageworxGiftcardsAmount(),
                'base_value' => $mainOrder->getBaseMageworxGiftcardsAmount()
            ];
            $this->_totals['mageworx_giftcards_amount'] = new \Magento\Framework\DataObject(
                [
                    'code'       => 'mageworx_giftcards_amount',
                    'label'      => $mainOrder->getMageworxGiftcardsDescription(),
                    'value'      => $mainOrder->getMageworxGiftcardsAmount(),
                    'base_value' => $mainOrder->getBaseMageworxGiftcardsAmount()
                ]
            );
        }
        return $this;
    }

}
