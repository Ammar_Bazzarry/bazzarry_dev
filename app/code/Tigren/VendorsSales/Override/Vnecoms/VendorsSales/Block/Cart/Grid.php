<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */
namespace Tigren\VendorsSales\Override\Vnecoms\VendorsSales\Block\Cart;

/**
 * Shopping cart block
 */
class Grid extends \Vnecoms\VendorsSales\Block\Cart\Grid
{

    protected $_vendorConfig;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ResourceModel\Url $catalogUrlBuilder,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Quote\Model\ResourceModel\Quote\Item\CollectionFactory $itemCollectionFactory,
        \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $joinProcessor,
        \Vnecoms\VendorsConfig\Helper\Data $helperData,
        array $data = []
    ) {
        $this->_vendorConfig = $helperData;
        parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $catalogUrlBuilder,
            $cartHelper,
            $httpContext,
            $itemCollectionFactory,
            $joinProcessor,
            $data
        );
    }

    public function getVendorName($vendorId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelFactory = $objectManager->create('\Vnecoms\Vendors\Model\Vendor')->load($vendorId);
        if(!$vendorId || !$modelFactory->getId()) return __("No Vendor");
        $title = $this->_vendorConfig->getVendorConfig('general/store_information/name', $vendorId);
        return $title?:$modelFactory->getVendorId();
    }
}
