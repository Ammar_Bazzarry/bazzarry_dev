<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

// @codingStandardsIgnoreFile

namespace Tigren\VendorsSales\Override\Vnecoms\VendorsSales\Block\Vendors\Order\Invoice;

class View extends \Vnecoms\VendorsSales\Block\Vendors\Order\Invoice\View
{
    /**
     * Constructor
     *
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _construct()
    {
        parent::_construct();
        $this->buttonList->remove('send_notification');

        if ($this->_isAllowedAction('Vnecoms_VendorsSales::sales_send_emails')) {
            $this->addButton(
                'custom_send_notification',
                [
                    'label' => __('Send Email'),
                    'class' => 'send-email',
                    'onclick' => 'confirmSetLocation(\'' . __(
                            'Are you sure you want to send an invoice email to customer?'
                        ) . '\', \'' . $this->getInvoiceEmailUrl() . '\')'
                ]
            );
        }

    }

    public function getInvoiceEmailUrl(){
        return $this->getUrl(
            'custom_sales/invoice/email',
            ['vendor_invoice_id' => $this->getVendorInvoice()->getId(), 'invoice_id' => $this->getInvoice()->getId()]
        );
    }

}
