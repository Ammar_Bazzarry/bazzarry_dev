<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https:/www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */
namespace Tigren\VendorsSales\Override\Vnecoms\VendorsSales\Observer;

use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice;

/**
 * Class ProcessInvoice
 * @package Tigren\VendorsSales\Override\Vnecoms\VendorsSales\Observer
 */
class ProcessInvoice extends \Vnecoms\VendorsSales\Observer\ProcessInvoice
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|\Vnecoms\VendorsSales\Observer\ProcessInvoice|void
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $log = \Magento\Framework\App\ObjectManager::getInstance()
            ->create('Psr\Log\LoggerInterface');
        $log->debug('ProcessInvoice');
        /* Do nothing if the extension is not enabled.*/
        if (!$this->_vendorHelper->moduleEnabled()) {
            return;
        }

        $invoice = $observer->getInvoice();
        if (!$invoice->getId()) {
            return;
        }
        $order = $invoice->getOrder();

        $paymentAction = $order->getPayment()->getMethodInstance()->getConfigPaymentAction();
        if($paymentAction == \Magento\Payment\Model\Method\AbstractMethod::ACTION_AUTHORIZE_CAPTURE){
            $order->save();
        }
        /*Check if the vendor invoices is created already*/

        if ($order->getId()) {
            $this->createVendorOrder($order);
        }
        $resourceInvoice = $this->_vendorInvoiceFactory->create()->getResource();

        if ($resourceInvoice->isCreatedVendorInvoice($invoice->getId())) {
            /*Update status of vendor invoices.*/
            $collection = $this->_vendorInvoiceFactory->create()->getCollection();
            $collection->addFieldToFilter('invoice_id', $invoice->getId());

            if ($collection->count()) {
                foreach ($collection as $vendorInvoice) {
                    if ($invoice->getState() == Invoice::STATE_PAID && $vendorInvoice->getState() != Invoice::STATE_PAID) {
                        $vendorOrder = $vendorInvoice->getOrder();

                        $vendorOrder->setTotalDue($vendorOrder->getTotalDue() - $vendorInvoice->getGrandTotal());
                        $vendorOrder->setBaseTotalDue($vendorOrder->getBaseTotalDue() - $vendorInvoice->getBaseGrandTotal());
                        $vendorOrder->setTotalPaid($vendorOrder->getTotalPaid()+$vendorInvoice->getGrandTotal());
                        $vendorOrder->setBaseTotalPaid($vendorOrder->getBaseTotalPaid()+$vendorInvoice->getBaseGrandTotal());

                        $vendorOrder->setShippingInvoiced($vendorOrder->getShippingInvoiced()+ $vendorInvoice->getShippingAmount());
                        $vendorOrder->setBaseShippingInvoiced($vendorOrder->getBaseShippingInvoiced()+$vendorInvoice->getBaseShippingAmount());

                        $vendorOrder->setTotalInvoiced($vendorOrder->getTotalInvoiced()+ $vendorInvoice->getGrandTotal());
                        $vendorOrder->setBaseTotalInvoiced($vendorOrder->getBaseTotalInvoiced()+$vendorInvoice->getBaseGrandTotal());

                        $vendorOrder->setTaxInvoiced($vendorOrder->getTaxInvoiced()+ $vendorInvoice->getTaxAmount());
                        $vendorOrder->setBaseTaxInvoiced($vendorOrder->getBaseTaxInvoiced()+$vendorInvoice->getBaseTaxAmount());

                        $vendorOrder->save();
                    }
                    $vendorInvoice->setState($invoice->getState())->save();
                }
            }
            return;
        }

        /*Create new vendor invoices for this invoice.*/
        $vendorInvoiceItems = [];
        /*Group invoice item by  vendor*/
        foreach ($invoice->getAllItems() as $item) {
            $orderItem = $item->getOrderItem();
            if ($vendorId = $orderItem->getVendorId()) {
                if (!isset($vendorInvoiceItems[$vendorId])) {
                    $vendorInvoiceItems[$vendorId]=[];
                }
                $vendorInvoiceItems[$vendorId][] = $item;
            }
        }


        $currentTimestamp = (new \DateTime())->getTimestamp();

        foreach ($vendorInvoiceItems as $vendorId => $items) {
            $vendorOrder = $this->_vendorOrderFactory->create();
            $vendorOrderId = $vendorOrder->getResource()->getVendorOrderId($vendorId, $order->getId());
            $vendorOrder->load($vendorOrderId);

            if (!$vendorOrderId || !$vendorOrder->getId()) {
                continue;
            }


            $shippingAmount = 0;
            $baseShippingAmount = 0;
            $shippingInclTaxs = 0;
            $baseShippingInclTaxs = 0;
            $shippingTaxAmount = 0;
            $baseShippingTaxAmount = 0;
            $includeShippingTax = true;
            /**
             * Check shipping amount in previous invoices
             */
            foreach ($vendorOrder->getInvoiceCollection() as $previousInvoice) {
                if ($previousInvoice->getShippingAmount() && !$previousInvoice->isCanceled()) {
                    $includeShippingTax= false;
                }
            }
            if ($includeShippingTax) {
                $shippingAmount += $vendorOrder->getShippingAmount();
                $baseShippingAmount += $vendorOrder->getBaseShippingAmount();
                $shippingInclTaxs += $vendorOrder->getShippingInclTax();
                $baseShippingInclTaxs += $vendorOrder->getBaseShippingInclTax();
                $shippingTaxAmount += $vendorOrder->getShippingTaxAmount();
                $baseShippingTaxAmount += $vendorOrder->getBaseShippingTaxAmount();
            }

            $invoiceData = [
                'vendor_id' => $vendorId,
                'vendor_order_id' => $vendorOrderId,
                'invoice_id' => $invoice->getId(),
                'state' => $invoice->getState(),
                'status' => $invoice->getStatus(),
                'subtotal' => 0,
                'base_subtotal' => 0,
                'tax_amount' => $shippingTaxAmount,
                'base_tax_amount' => $baseShippingTaxAmount,
                'shipping_tax_amount' => $shippingTaxAmount,
                'base_shipping_tax_amount' => $baseShippingTaxAmount,
                'discount_amount'  => 0,
                'base_discount_amount' => 0,
                'shipping_amount' => $shippingAmount,
                'base_shipping_amount' => $baseShippingAmount,
                'subtotal_incl_tax' => 0,
                'base_subtotal_incl_tax' => 0,
                'total_qty' => sizeof($items),
                'updated_at' => $currentTimestamp,
                'shipping_incl_tax' => $shippingInclTaxs,
                'base_shipping_incl_tax' => $baseShippingInclTaxs,
                'grand_total' => 0,
                'base_grand_total' => 0,
                'base_total_refunded' => 0,
            ];

            foreach ($items as $item) {
                $invoiceData['subtotal'] += $item->getData('row_total');
                $invoiceData['base_subtotal'] += $item->getData('base_row_total');
                $invoiceData['tax_amount'] += $item->getData('tax_amount');
                $invoiceData['base_tax_amount'] += $item->getData('base_tax_amount');
                $invoiceData['discount_amount'] += $item->getData('discount_amount');
                $invoiceData['base_discount_amount'] += $item->getData('base_discount_amount');
                $invoiceData['subtotal_incl_tax'] += $item->getData('row_total_incl_tax');
                $invoiceData['base_subtotal_incl_tax'] += $item->getData('base_row_total_incl_tax');
            }

            $invoiceData['grand_total'] = $invoiceData['subtotal'] +
                $invoiceData['shipping_amount'] +
                $invoiceData['tax_amount'] -
                $invoiceData['discount_amount'];
            $invoiceData['base_grand_total'] = $invoiceData['base_subtotal'] +
                $invoiceData['base_shipping_amount'] +
                $invoiceData['base_tax_amount'] -
                $invoiceData['base_discount_amount'];


            $invoiceDataObj = new \Magento\Framework\DataObject($invoiceData);
            $this->_eventManager->dispatch(
                'ves_vendorssales_process_invoice_before',
                [
                    'invoice_data' => $invoiceDataObj,
                    'vendor_id' => $vendorId,
                    'items' => $items,
                    'invoice' => $invoice
                ]
            );

            $invoiceData = $invoiceDataObj->getData();
            $vendorInvoice = $this->_vendorInvoiceFactory->create();
            
            $vendorInvoice->setData($invoiceData)->setItems($items)->setAllItems($items)->save();
            
            if (!$vendorOrder->canInvoice() && !$vendorOrder->canShip() || !$order->canShip()) {
                $vendorOrder->setStatus(Order::STATE_COMPLETE);
            } else {
                $vendorOrder->setStatus(Order::STATE_PROCESSING);
            }

            if ($invoice->getState() == Invoice::STATE_PAID) {
                $vendorOrder->setTotalDue($vendorOrder->getTotalDue() - $vendorInvoice->getGrandTotal());
                $vendorOrder->setBaseTotalDue($vendorOrder->getBaseTotalDue() - $vendorInvoice->getBaseGrandTotal());
                $vendorOrder->setTotalPaid($vendorOrder->getTotalPaid()+$vendorInvoice->getGrandTotal());
                $vendorOrder->setBaseTotalPaid($vendorOrder->getBaseTotalPaid()+$vendorInvoice->getBaseGrandTotal());

                $vendorOrder->setShippingInvoiced($vendorOrder->getShippingInvoiced()+ $vendorInvoice->getShippingAmount());
                $vendorOrder->setBaseShippingInvoiced($vendorOrder->getBaseShippingInvoiced()+$vendorInvoice->getBaseShippingAmount());

                $vendorOrder->setTotalInvoiced($vendorOrder->getTotalInvoiced()+ $vendorInvoice->getGrandTotal());
                $vendorOrder->setBaseTotalInvoiced($vendorOrder->getBaseTotalInvoiced()+$vendorInvoice->getBaseGrandTotal());

                $vendorOrder->setTaxInvoiced($vendorOrder->getTaxInvoiced()+ $vendorInvoice->getTaxAmount());
                $vendorOrder->setBaseTaxInvoiced($vendorOrder->getBaseTaxInvoiced()+$vendorInvoice->getBaseTaxAmount());
            }

            $vendorOrder->save();

            if ($vendorInvoice->getId()) {
                $this->_invoiceSender->send($vendorInvoice, true);
            }
        }

        return $this;
    }
}
