<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CatalogUrlRewrite\Plugin\Magento\Catalog\Model;

use Closure;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Filter\FilterManager;
use Magento\Store\Model\ScopeInterface;
use Tigren\CatalogUrlRewrite\Model\Config\Source\AdditionalSuffix;

/**
 * Class Product
 * @package Tigren\CatalogUrlRewrite\Plugin\Magento\Catalog\Model
 */
class Product
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var FilterManager
     */
    protected $filter;

    /**
     * @param FilterManager $filter
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        FilterManager $filter,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->filter = $filter;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param \Magento\Catalog\Model\Product $subject
     * @param Closure $proceed
     * @param $str
     * @return string
     */
    public function aroundFormatUrlKey(
        \Magento\Catalog\Model\Product $subject,
        Closure $proceed,
        $str
    ) {
        $formattedUrl = $proceed($str);

        $additionalSuffix = '';

        if (!$subject->getUrlKey()) {
            $additionalSuffixConf = $this->scopeConfig->getValue(
                'catalog_url_rewrite/general/additional_suffix',
                ScopeInterface::SCOPE_STORE
            );
            switch ($additionalSuffixConf) {
                case AdditionalSuffix::ADDITIONAL_SUFFIX_CONFIG_PRODUCT_ID:
                    $additionalSuffix = '-' . $subject->getId();
                    break;
                case AdditionalSuffix::ADDITIONAL_SUFFIX_CONFIG_PRODUCT_SKU:
                    $additionalSuffix = '-' . $subject->getSku();
                    break;
            }
        }

        return $this->filter->translitUrl(
            $formattedUrl . $additionalSuffix
        );
    }
}
