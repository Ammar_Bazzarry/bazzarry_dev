<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CatalogUrlRewrite\Logger;

/**
 * Class Logger
 *
 * @package Tigren\CatalogUrlRewrite\Logger
 */
class Logger extends \Monolog\Logger
{
}
