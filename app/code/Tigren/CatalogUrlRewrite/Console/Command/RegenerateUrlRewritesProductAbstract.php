<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CatalogUrlRewrite\Console\Command;

use Exception;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\UrlRewrite\Model\Exception\UrlAlreadyExistsException;

/**
 * Class RegenerateUrlRewritesProductAbstract
 *
 * @package Tigren\CatalogUrlRewrite\Console\Command
 */
abstract class RegenerateUrlRewritesProductAbstract extends RegenerateUrlRewritesAbstract
{
    /**
     * @var integer
     */
    protected $_productsCollectionPageSize = 1000;

    /**
     * @see parent::regenerateAllProductsUrlRewrites()
     */
    public function regenerateAllProductsUrlRewrites($storeId = 0)
    {
        $this->regenerateProductsRangeUrlRewrites([], $storeId);
    }

    /**
     * @see parent::regenerateProductsRangeUrlRewrites()
     */
    public function regenerateProductsRangeUrlRewrites($productsFilter = [], $storeId = 0)
    {
        /**
         * Get products collection
         *
         * @var Collection $products
         */
        $products = $this->_getProductsCollection($storeId, $productsFilter);

        $pageCount = $products->getLastPageNumber();
        $this->_progress = 0;
        $this->_total = (int)$products->getSize();
        $this->_displayProgressBar();
        $currentPage = 1;

        while ($currentPage <= $pageCount) {
            $products->clear();
            $products->setCurPage($currentPage);
            $products->load();

            foreach ($products as $product) {
                $this->_productProcess($product, $storeId);
            }

            $currentPage++;
        }
    }

    /**
     * Get products collection
     *
     * @param integer $storeId
     * @param array $productsFilter
     * @return Collection
     */
    protected function _getProductsCollection($storeId = 0, $productsFilter = [])
    {
        /**
         * @var Collection $productsCollection
         */
        $productsCollection = $this->_productCollectionFactory->create()
            ->setStore($storeId)
            ->addStoreFilter($storeId)
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('visibility')
            ->addAttributeToSelect('url_key')
            ->addAttributeToSelect('url_path')
            ->addAttributeToSelect('status')
            ->addAttributeToFilter('status', Status::STATUS_ENABLED)
            // use limit to avoid a "eating" of a memory
            ->setPageSize($this->_productsCollectionPageSize);

        if (count($productsFilter) > 0) {
            $productsCollection->addIdFilter($productsFilter);
        }

        return $productsCollection;
    }

    /**
     * Regenerate Url Rewrite for specific product
     *
     * @param  Product $product
     * @param  integer $storeId
     * @return void
     */
    protected function _productProcess($product, $storeId)
    {
        try {
            if ($this->_commandOptions['saveOldUrls']) {
                $product->setData('save_rewrites_history', true);
            }

            $product->setData('url_path', null)
                ->setStoreId($storeId);

            if (!$this->_commandOptions['noRegenUrlKey']) {
                $generatedKey = $this->_productUrlPathGenerator->getUrlKey($product);

                $product->setData('url_key', $generatedKey);

                $this->_getProductAction()->updateAttributes(
                    [$product->getId()],
                    ['url_path' => null, 'url_key' => $generatedKey],
                    $storeId
                );
            }

            $productUrlRewriteResult = $this->_getProductUrlRewriteGenerator()->generate($product);

            $productUrlRewriteResult = $this->_sanitizeProductUrlRewrites($productUrlRewriteResult);

            try {
                $this->_urlPersist->replace($productUrlRewriteResult);
            } catch (UrlAlreadyExistsException $y) {
                $connection = $product->getResource()->getConnection();

                $conflictedUrls = array_map(
                    function ($urlRewrite) use ($connection) {
                        return $connection->quote($urlRewrite['request_path']);
                    },
                    $y->getUrls()
                );

                $requestPath = implode(', ', $conflictedUrls);

                $this->_addConsoleMsg(
                    'Some URL paths already exists in url_rewrite table and not related to Product ID: ' . $product->getId() .
                    '. Please remove them and execute this command again. You can find them by following SQL:'
                );

                $this->_addConsoleMsg("SELECT * FROM url_rewrite WHERE store_id={$connection->quote($storeId, 'int')} AND request_path IN ({$requestPath});");
            } catch (Exception $y) {
                //to debugg error
                $this->_addConsoleMsg($y->getMessage() . ' Product ID: ' . $product->getId());
            }

            $this->_progress++;
            $this->_displayProgressBar();
        } catch (Exception $e) {
            $this->_addConsoleMsg($e->getMessage() . ' Product ID: ' . $product->getId());
        }
    }

    /**
     * Sanitize product URL rewrites
     *
     * @param  array $productUrlRewrites
     * @return array
     */
    protected function _sanitizeProductUrlRewrites($productUrlRewrites)
    {
        $paths = [];
        foreach ($productUrlRewrites as $key => $urlRewrite) {
            $path = $this->_clearRequestPath($urlRewrite->getRequestPath());
            if (!in_array($path, $paths)) {
                $productUrlRewrites[$key]->setRequestPath($path);
                $paths[] = $path;
            } else {
                unset($productUrlRewrites[$key]);
            }
        }

        return $productUrlRewrites;
    }

    /**
     * @see parent::regenerateSpecificProductUrlRewrites()
     */
    public function regenerateSpecificProductUrlRewrites($productId, $storeId = 0)
    {
        $this->regenerateProductsRangeUrlRewrites([$productId], $storeId);
    }
}
