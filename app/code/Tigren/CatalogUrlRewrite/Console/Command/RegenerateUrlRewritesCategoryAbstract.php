<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CatalogUrlRewrite\Console\Command;

use Exception;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;

/**
 * Class RegenerateUrlRewritesCategoryAbstract
 *
 * @package Tigren\CatalogUrlRewrite\Console\Command
 */
abstract class RegenerateUrlRewritesCategoryAbstract extends RegenerateUrlRewritesProductAbstract
{
    /**
     * @var integer
     */
    protected $_categoriesCollectionPageSize = 1000;

    /**
     * @throws LocalizedException
     * @see parent::regenerateAllCategoriesUrlRewrites()
     */
    public function regenerateAllCategoriesUrlRewrites($storeId = 0)
    {
        $this->regenerateCategoriesRangeUrlRewrites([], $storeId);
    }

    /**
     * @param  array $categoriesFilter
     * @param  int   $storeId
     * @throws LocalizedException
     * @see    parent::regenerateCategoriesRangeUrlRewrites()
     */
    public function regenerateCategoriesRangeUrlRewrites($categoriesFilter = [], $storeId = 0)
    {
        /**
         * Get categories collection
         *
         * @var Collection $categoriesCollection
         */
        $categories = $this->_getCategoriesCollection($storeId, $categoriesFilter);

        $pageCount = $categories->getLastPageNumber();
        $this->_progress = 0;
        $this->_total = (int)$categories->getSize();
        $this->_displayProgressBar();
        $currentPage = 1;

        while ($currentPage <= $pageCount) {
            $categories->clear();
            $categories->setCurPage($currentPage);
            $categories->load();

            foreach ($categories as $category) {
                $this->_categoryProcess($category, $storeId);
            }

            $categories->clear();
            $currentPage++;
        }
    }

    /**
     * Get categories collection
     *
     * @param  integer $storeId
     * @param  array   $categoriesFilter
     * @return Collection
     * @throws LocalizedException
     */
    protected function _getCategoriesCollection($storeId = 0, $categoriesFilter = [])
    {
        /** @var Collection $categoriesCollection */
        $categoriesCollection = $this->_categoryCollectionFactory->create()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('url_key')
            ->addAttributeToSelect('url_path')
            ->addFieldToFilter('level', ['gt' => '1'])
            ->setOrder(['level', 'entity_id'], 'ASC')
            // use limit to avoid a "eating" of a memory
            ->setPageSize($this->_categoriesCollectionPageSize);

        $rootCategoryId = $this->_getStoreRootCategoryId($storeId);
        if ($rootCategoryId > 0) {
            // we use this filter instead of "->setStore()" - because "setStore()" is not working now (another Magento issue)
            $categoriesCollection->addAttributeToFilter('path', ['like' => "1/{$rootCategoryId}/%"]);
        }

        if (count($categoriesFilter) > 0) {
            $categoriesCollection->addIdFilter($categoriesFilter);
        }

        return $categoriesCollection;
    }

    /**
     * Get root category Id of specific store
     *
     * @param  string $storeId
     * @return integer
     */
    protected function _getStoreRootCategoryId($storeId)
    {
        // use SQL to speed up and to not instantiate additional store object just for root category ID
        $tableName1 = $this->_resource->getTableName('store_group');
        $tableName2 = $this->_resource->getTableName('store');
        $sql = "SELECT t1.root_category_id FROM {$tableName1} t1 INNER JOIN {$tableName2} t2 ON t2.website_id = t1.website_id WHERE t2.store_id = {$storeId};";

        $result = (int)$this->_resource->getConnection()->fetchOne($sql);

        return $result;
    }

    /**
     * Process category URL rewrites re-generation
     *
     * @param  CategoryInterface|AbstractModel $category
     * @param  integer                         $storeId
     * @return void
     */
    protected function _categoryProcess($category, $storeId = 0)
    {
        try {
            if ($this->_commandOptions['saveOldUrls']) {
                $category->setData('save_rewrites_history', true);
            }
            $category->setStoreId($storeId);

            if (!$this->_commandOptions['noRegenUrlKey']) {
                $category->setUrlKey($this->_categoryUrlPathGenerator->getUrlKey($category));
                $category->getResource()->saveAttribute($category, 'url_key');
            }

            $category->setUrlPath($this->_categoryUrlPathGenerator->getUrlPath($category));
            $category->getResource()->saveAttribute($category, 'url_path');

            $this->_regenerateCategoryUrlRewrites($category, $storeId);

            //frees memory for maps that are self-initialized in multiple classes that were called by the generators
            $this->_resetUrlRewritesDataMaps($category);

            $this->_progress++;
            $this->_displayProgressBar();
        } catch (Exception $e) {
            // debugging
            $this->_addConsoleMsg('Exception: ' . $e->getMessage() . ' Category ID: ' . $category->getId());
        }
    }

    /**
     * Regenerate category and category products Url Rewrites
     *
     * @param  CategoryInterface|AbstractModel $category
     * @param  integer                         $storeId
     * @return void
     */
    protected function _regenerateCategoryUrlRewrites($category, $storeId)
    {
        try {
            $category->setStore($storeId);
            $category->setChangedProductIds([]);
            $categoryUrlRewriteResult = $this->_getCategoryUrlRewriteGenerator()->generate($category, true);
            $this->_doBunchReplaceUrlRewrites($categoryUrlRewriteResult);
        } catch (Exception $e) {
            // debugging
            $this->_addConsoleMsg('Exception: ' . $e->getMessage() . ' Category ID: ' . $category->getId());
        }
    }

    /**
     * Resets used data maps to free up memory and temporary tables
     *
     * @param $category
     * @return void
     */
    protected function _resetUrlRewritesDataMaps($category)
    {
        foreach ($this->_dataUrlRewriteClassNames as $className) {
            $this->_databaseMapPool->resetMap($className, $category->getEntityId());
        }
    }

    /**
     * @throws LocalizedException
     * @see parent::regenerateSpecificCategoryUrlRewrites()
     */
    public function regenerateSpecificCategoryUrlRewrites($categoryId, $storeId = 0)
    {
        $this->regenerateCategoriesRangeUrlRewrites([$categoryId], $storeId);
    }
}
