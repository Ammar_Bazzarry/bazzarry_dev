<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CatalogUrlRewrite\Console\Command;

use Exception;
use Magento\Framework\App\State as AppState;
use Magento\Framework\Console\Cli;
use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tigren\CatalogUrlRewrite\Model\Product\Url;
use Tigren\CatalogUrlRewrite\Model\Product\UrlFactory;

/**
 * Class CleanupUrlRewrites
 *
 * @package Tigren\CatalogUrlRewrite\Console\Command
 */
class CleanupUrlRewrites extends Command
{
    /**
     * @var AppState
     */
    protected $_appState;

    /**
     * Product url factory
     *
     * @var UrlFactory
     */
    protected $_productUrlFactory;

    /**
     * GenerateUrlRewriteCommand constructor.
     *
     * @param AppState   $appState
     * @param UrlFactory $productUrlFactory
     */
    public function __construct(
        AppState $appState,
        UrlFactory $productUrlFactory
    ) {
        $this->_appState = $appState;
        $this->_productUrlFactory = $productUrlFactory;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('tigren:url_rewrites:clean');
        $this->setDescription('Cleanup Catalog URL Rewrites');
    }

    /**
     * {@inheritdoc}
     *
     * @return mixed
     * @throws LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_appState->setAreaCode('adminhtml');

        try {
            /**
             * @var Url $productUrlModel
             */
            $productUrlModel = $this->_productUrlFactory->create();
            $productUrlModel->cleanupCatalogUrlRewrites();
        } catch (Exception $e) {
            $output->writeln("<error>{$e->getMessage()}</error>");
            return Cli::RETURN_FAILURE;
        }

        $output->writeln('=====> Catalog URL Rewrites were cleaned up successfully.');
    }
}
