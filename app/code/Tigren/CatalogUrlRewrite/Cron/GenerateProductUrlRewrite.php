<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CatalogUrlRewrite\Cron;

use Tigren\CatalogUrlRewrite\Model\Product\Url;
use Tigren\CatalogUrlRewrite\Model\Product\UrlFactory;

/**
 * Class GenerateProductUrlRewrite
 * @package Tigren\CatalogUrlRewrite\Cron
 */
class GenerateProductUrlRewrite
{
    /**
     * Product url factory
     *
     * @var UrlFactory
     */
    protected $_productUrlFactory;

    /**
     * GenerateProductUrlRewrite constructor.
     * @param UrlFactory $productUrlFactory
     */
    public function __construct(
        UrlFactory $productUrlFactory
    ) {
        $this->_productUrlFactory = $productUrlFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        /**
         * @var Url $productUrlModel
         */
        $productUrlModel = $this->_productUrlFactory->create();
        $productUrlModel->cleanupCatalogUrlRewrites();
        $productUrlModel->regenerateUrlRewrites();
    }
}
