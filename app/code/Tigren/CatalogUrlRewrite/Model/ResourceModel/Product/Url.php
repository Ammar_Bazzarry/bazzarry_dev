<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CatalogUrlRewrite\Model\ResourceModel\Product;

use Exception;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Helper\Category as CategoryHelper;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Indexer\Category\Product\TableMaintainer;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Action;
use Magento\Catalog\Model\ResourceModel\Product\ActionFactory as ProductActionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator;
use Magento\CatalogUrlRewrite\Model\CategoryUrlRewriteGenerator;
use Magento\CatalogUrlRewrite\Model\CategoryUrlRewriteGeneratorFactory;
use Magento\CatalogUrlRewrite\Model\Map\DatabaseMapPool;
use Magento\CatalogUrlRewrite\Model\Map\DataCategoryUrlRewriteDatabaseMap;
use Magento\CatalogUrlRewrite\Model\Map\DataProductUrlRewriteDatabaseMap;
use Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGeneratorFactory;
use Magento\CatalogUrlRewrite\Model\UrlRewriteBunchReplacer;
use Magento\CatalogUrlRewrite\Observer\UrlRewriteHandler;
use Magento\CatalogUrlRewrite\Observer\UrlRewriteHandlerFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State as AppState;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Store\Model\StoreManagerInterface;
use Magento\UrlRewrite\Model\Exception\UrlAlreadyExistsException;
use Magento\UrlRewrite\Model\UrlPersistInterface as UrlPersist;

/**
 * Class Url
 *
 * @package Tigren\CatalogUrlRewrite\Model\ResourceModel
 */
class Url extends \Magento\Catalog\Model\ResourceModel\Url
{
    /**
     * @var AppState $appState
     */
    protected $_appState;

    /**
     * @var CategoryHelper
     */
    protected $_categoryHelper;

    /**
     * @var CategoryCollectionFactory
     */
    protected $_categoryCollectionFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var ProductActionFactory
     */
    protected $_productActionFactory;

    /**
     * @var CategoryUrlPathGenerator
     */
    protected $_categoryUrlPathGenerator;

    /**
     * @var \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator
     */
    protected $_productUrlPathGenerator;

    /**
     * @var CategoryUrlRewriteGeneratorFactory
     */
    protected $_categoryUrlRewriteGeneratorFactory;

    /**
     * @var ProductUrlRewriteGeneratorFactory
     */
    protected $_productUrlRewriteGeneratorFactory;

    /**
     * @var UrlRewriteBunchReplacer
     */
    protected $_urlRewriteBunchReplacer;

    /**
     * @var DatabaseMapPool
     */
    protected $_databaseMapPool;

    /**
     * @var UrlRewriteHandlerFactory
     */
    protected $_urlRewriteHandlerFactory;

    /**
     * @var UrlPersist
     */
    protected $_urlPersist;

    /**
     * @var CategoryUrlRewriteGenerator
     */
    protected $_categoryUrlRewriteGenerator;

    /**
     * @var ProductUrlRewriteGenerator
     */
    protected $_productUrlRewriteGenerator;

    /**
     * @var UrlRewriteHandler
     */
    protected $_urlRewriteHandler;

    /**
     * @var Action
     */
    protected $_productAction;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var array
     */
    protected $_dataUrlRewriteClassNames;

    /**
     * @var integer
     */
    protected $_categoriesCollectionPageSize = 100;

    /**
     * @var integer
     */
    protected $_productsCollectionPageSize = 1000;

    /**
     * Url constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Catalog\Model\ResourceModel\Product $productResource
     * @param Category $catalogCategory
     * @param \Psr\Log\LoggerInterface $logger
     * @param AppState\Proxy $appState
     * @param CategoryHelper\Proxy $categoryHelper
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductActionFactory\Proxy $productActionFactory
     * @param CategoryUrlPathGenerator\Proxy $categoryUrlPathGenerator
     * @param CategoryUrlRewriteGeneratorFactory\Proxy $categoryUrlRewriteGeneratorFactory
     * @param ProductUrlPathGenerator\Proxy $productUrlPathGenerator
     * @param ProductUrlRewriteGeneratorFactory\Proxy $productUrlRewriteGeneratorFactory
     * @param UrlRewriteBunchReplacer\Proxy $urlRewriteBunchReplacer
     * @param UrlRewriteHandlerFactory\Proxy $urlRewriteHandlerFactory
     * @param DatabaseMapPool\Proxy $databaseMapPool
     * @param UrlPersist\Proxy $urlPersist
     * @param ScopeConfigInterface $scopeConfig
     * @param null $connectionName
     * @param TableMaintainer|null $tableMaintainer
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Catalog\Model\ResourceModel\Product $productResource,
        \Magento\Catalog\Model\Category $catalogCategory,
        \Psr\Log\LoggerInterface $logger,
        AppState\Proxy $appState,
        CategoryHelper\Proxy $categoryHelper,
        CategoryCollectionFactory $categoryCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        ProductActionFactory\Proxy $productActionFactory,
        CategoryUrlPathGenerator\Proxy $categoryUrlPathGenerator,
        CategoryUrlRewriteGeneratorFactory\Proxy $categoryUrlRewriteGeneratorFactory,
        ProductUrlPathGenerator\Proxy $productUrlPathGenerator,
        ProductUrlRewriteGeneratorFactory\Proxy $productUrlRewriteGeneratorFactory,
        UrlRewriteBunchReplacer\Proxy $urlRewriteBunchReplacer,
        UrlRewriteHandlerFactory\Proxy $urlRewriteHandlerFactory,
        DatabaseMapPool\Proxy $databaseMapPool,
        UrlPersist\Proxy $urlPersist,
        ScopeConfigInterface $scopeConfig,
        $connectionName = null,
        TableMaintainer $tableMaintainer = null
    ) {
        parent::__construct(
            $context,
            $storeManager,
            $eavConfig,
            $productResource,
            $catalogCategory,
            $logger,
            $connectionName,
            $tableMaintainer
        );

        $this->_appState = $appState;
        $this->_categoryHelper = $categoryHelper;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productActionFactory = $productActionFactory;
        $this->_categoryUrlPathGenerator = $categoryUrlPathGenerator;
        $this->_categoryUrlRewriteGeneratorFactory = $categoryUrlRewriteGeneratorFactory;
        $this->_productUrlPathGenerator = $productUrlPathGenerator;
        $this->_productUrlRewriteGeneratorFactory = $productUrlRewriteGeneratorFactory;
        $this->_urlRewriteBunchReplacer = $urlRewriteBunchReplacer;
        $this->_urlRewriteHandlerFactory = $urlRewriteHandlerFactory;
        $this->_databaseMapPool = $databaseMapPool;
        $this->_urlPersist = $urlPersist;
        $this->_scopeConfig = $scopeConfig;

        $this->_dataUrlRewriteClassNames = [
            DataCategoryUrlRewriteDatabaseMap::class,
            DataProductUrlRewriteDatabaseMap::class
        ];
    }

    /**
     * Cleanup all rewrite urls for all stores
     *
     * @return $this
     * @throws LocalizedException
     */
    public function cleanupCatalogUrlRewrites()
    {
        $connection = $this->getConnection();

        $cleanupUrlRewritesSelect = $connection->select()
            ->from($this->getTable('url_rewrite'))
            ->where('entity_type = ?', 'category')
            ->orWhere('entity_type = ?', 'product');
        $cleanupUrlRewritesQuery = $cleanupUrlRewritesSelect->deleteFromSelect($this->getTable('url_rewrite'));
        $connection->query($cleanupUrlRewritesQuery);

        $categoryUrlKeyAttribute = $this->_eavConfig->getAttribute(
            Category::ENTITY,
            'url_key'
        );
        $categoryUrlPathAttribute = $this->_eavConfig->getAttribute(
            Category::ENTITY,
            'url_path'
        );
        if (!empty($categoryUrlKeyAttribute) && !empty($categoryUrlPathAttribute)) {
            $cleanupCategoryAttributesSelect = $connection->select()
                ->from($this->getTable('catalog_category_entity_varchar'))
                ->where('attribute_id = ?', $categoryUrlKeyAttribute->getId())
                ->orWhere('attribute_id = ?', $categoryUrlPathAttribute->getId());
            $cleanupCategoryAttributesQuery = $cleanupCategoryAttributesSelect->deleteFromSelect($this->getTable('catalog_category_entity_varchar'));
            $connection->query($cleanupCategoryAttributesQuery);
        }

        $productUrlKeyAttribute = $this->_eavConfig->getAttribute(
            Product::ENTITY,
            'url_key'
        );
        $productUrlPathAttribute = $this->_eavConfig->getAttribute(
            Product::ENTITY,
            'url_path'
        );
        if (!empty($productUrlKeyAttribute) && !empty($productUrlPathAttribute)) {
            $cleanupCategoryAttributesSelect = $connection->select()
                ->from($this->getTable('catalog_product_entity_varchar'))
                ->where('attribute_id = ?', $productUrlKeyAttribute->getId())
                ->orWhere('attribute_id = ?', $productUrlPathAttribute->getId());
            $cleanupCategoryAttributesQuery = $cleanupCategoryAttributesSelect->deleteFromSelect($this->getTable('catalog_product_entity_varchar'));
            $connection->query($cleanupCategoryAttributesQuery);
        }

        return $this;
    }

    /**
     * Regenerate all catalog url rewrites for all stores
     *
     * @return $this
     * @throws LocalizedException
     */
    public function regenerateUrlRewrites()
    {
        $allStoreIds = $this->_getAllStoreIds();

        // Regenerate category url rewrites (the first time)
        foreach ($allStoreIds as $storeId => $storeCode) {
            $this->_storeManager->setCurrentStore($storeId);
            $this->regenerateAllCategoriesUrlRewrites($storeId);
        }

        // Regenerate product url rewrites
        foreach ($allStoreIds as $storeId => $storeCode) {
            $this->_storeManager->setCurrentStore($storeId);
            $this->regenerateAllProductsUrlRewrites($storeId);
        }

        return $this;
    }

    /**
     * Get list of all stores id/code
     *
     * @return array
     */
    protected function _getAllStoreIds()
    {
        $result = [];

        $sql = $this->getConnection()->select()
            ->from($this->_resources->getTableName('store'), ['store_id', 'code'])
            ->order('store_id', 'ASC');

        $queryResult = $this->getConnection()->fetchAll($sql);

        foreach ($queryResult as $row) {
            $result[(int)$row['store_id']] = $row['code'];
        }

        return $result;
    }

    /**
     * @return \Magento\CatalogUrlRewrite\Model\CategoryUrlRewriteGenerator
     */
    protected function _getCategoryUrlRewriteGenerator()
    {
        if (is_null($this->_categoryUrlRewriteGenerator)) {
            $this->_categoryUrlRewriteGenerator = $this->_categoryUrlRewriteGeneratorFactory->create();
        }

        return $this->_categoryUrlRewriteGenerator;
    }

    /**
     * @return \Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator
     */
    protected function _getProductUrlRewriteGenerator()
    {
        if (is_null($this->_productUrlRewriteGenerator)) {
            $this->_productUrlRewriteGenerator = $this->_productUrlRewriteGeneratorFactory->create();
        }

        return $this->_productUrlRewriteGenerator;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Action
     */
    protected function _getProductAction()
    {
        if (is_null($this->_productAction)) {
            $this->_productAction = $this->_productActionFactory->create();
        }

        return $this->_productAction;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Action
     */
    protected function _getUrlRewriteHandler()
    {
        if (is_null($this->_urlRewriteHandler)) {
            $this->_urlRewriteHandler = $this->_urlRewriteHandlerFactory->create();
        }

        return $this->_urlRewriteHandler;
    }

    /**
     * Do a bunch replace of url rewrites
     *
     * @param array $urlRewrites
     * @param string $type
     * @return void
     */
    protected function _doBunchReplaceUrlRewrites($urlRewrites = [], $type = 'Category')
    {
        try {
            $this->_urlRewriteBunchReplacer->doBunchReplace($urlRewrites);
        } catch (Exception $e) {
            if ($e instanceof UrlAlreadyExistsException
                || strpos($e->getMessage(), 'Duplicate entry') !== false
            ) {
                foreach ($urlRewrites as $singleUrlRewrite) {
                    try {
                        $this->_urlRewriteBunchReplacer->doBunchReplace([$singleUrlRewrite]);
                    } catch (Exception $y) {
                        // do something here
                    }
                }
            }
        }
    }

    /**
     * Clear request path
     *
     * @param string $requestPath
     * @return string
     */
    protected function _clearRequestPath($requestPath)
    {
        return str_replace(['//', './'], ['/', '/'], ltrim(ltrim($requestPath, '/'), '.'));
    }

    /**
     * @param int $storeId
     * @throws LocalizedException
     * @see parent::regenerateAllCategoriesUrlRewrites()
     */
    public function regenerateAllCategoriesUrlRewrites($storeId = 0)
    {
        $this->regenerateCategoriesRangeUrlRewrites([], $storeId);
    }

    /**
     * @param  array $categoriesFilter
     * @param  int   $storeId
     * @throws LocalizedException
     * @see    parent::regenerateCategoriesRangeUrlRewrites()
     */
    public function regenerateCategoriesRangeUrlRewrites($categoriesFilter = [], $storeId = 0)
    {
        // get categories collection
        $categories = $this->_getCategoriesCollection($storeId, $categoriesFilter);

        $pageCount = $categories->getLastPageNumber();
        $currentPage = 1;

        while ($currentPage <= $pageCount) {
            $categories->clear();
            $categories->setCurPage($currentPage);
            $categories->load();

            foreach ($categories as $category) {
                $this->_categoryProcess($category, $storeId);
            }

            $categories->clear();
            $currentPage++;
        }
    }

    /**
     * Get categories collection
     *
     * @param  integer $storeId
     * @param  array   $categoriesFilter
     * @return Collection
     * @throws LocalizedException
     */
    protected function _getCategoriesCollection($storeId = 0, $categoriesFilter = [])
    {
        /**
         * @var \Magento\Catalog\Model\ResourceModel\Category\Collection $categoriesCollection
         */
        $categoriesCollection = $this->_categoryCollectionFactory->create()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('url_key')
            ->addAttributeToSelect('url_path')
            ->addFieldToFilter('level', ['gt' => '1'])
            ->setOrder(['level', 'entity_id'], 'ASC')
            // use limit to avoid a "eating" of a memory
            ->setPageSize($this->_categoriesCollectionPageSize);

        $rootCategoryId = $this->_getStoreRootCategoryId($storeId);
        if ($rootCategoryId > 0) {
            // we use this filter instead of "->setStore()" - because "setStore()" is not working now (another Magento issue)
            $categoriesCollection->addAttributeToFilter('path', ['like' => "1/{$rootCategoryId}/%"]);
        }

        if (count($categoriesFilter) > 0) {
            $categoriesCollection->addIdFilter($categoriesFilter);
        }

        return $categoriesCollection;
    }

    /**
     * Get root category Id of specific store
     *
     * @param  string $storeId
     * @return integer
     */
    protected function _getStoreRootCategoryId($storeId)
    {
        // use SQL to speed up and to not instantiate additional store object just for root category ID
        $tableName1 = $this->_resources->getTableName('store_group');
        $tableName2 = $this->_resources->getTableName('store');
        $sql = "SELECT t1.root_category_id FROM {$tableName1} t1 INNER JOIN {$tableName2} t2 ON t2.website_id = t1.website_id WHERE t2.store_id = {$storeId};";

        $result = (int)$this->getConnection()->fetchOne($sql);

        return $result;
    }

    /**
     * Process category URL rewrites re-generation
     *
     * @param  CategoryInterface|AbstractModel $category
     * @param  integer                         $storeId
     * @return void
     */
    protected function _categoryProcess($category, $storeId = 0)
    {
        try {
            $category->setStoreId($storeId);
            $category->setUrlKey($this->_categoryUrlPathGenerator->getUrlKey($category));
            $category->getResource()->saveAttribute($category, 'url_key');
            $category->setUrlPath($this->_categoryUrlPathGenerator->getUrlPath($category));
            $category->getResource()->saveAttribute($category, 'url_path');

            $this->_regenerateCategoryUrlRewrites($category, $storeId);

            //frees memory for maps that are self-initialized in multiple classes that were called by the generators
            $this->_resetUrlRewritesDataMaps($category);
        } catch (Exception $e) {
            // do something here
        }
    }

    /**
     * Regenerate category and category products Url Rewrites
     *
     * @param  CategoryInterface|AbstractModel $category
     * @param  integer                         $storeId
     * @return void
     */
    protected function _regenerateCategoryUrlRewrites($category, $storeId)
    {
        try {
            $category->setStore($storeId);
            $category->setChangedProductIds([]);
            $categoryUrlRewriteResult = $this->_getCategoryUrlRewriteGenerator()->generate($category, true);
            $this->_doBunchReplaceUrlRewrites($categoryUrlRewriteResult);
        } catch (Exception $e) {
            // do something hể
        }
    }

    /**
     * Resets used data maps to free up memory and temporary tables
     *
     * @param  Category $category
     * @return void
     */
    protected function _resetUrlRewritesDataMaps($category)
    {
        foreach ($this->_dataUrlRewriteClassNames as $className) {
            $this->_databaseMapPool->resetMap($className, $category->getEntityId());
        }
    }

    /**
     * @see parent::regenerateAllProductsUrlRewrites()
     */
    public function regenerateAllProductsUrlRewrites($storeId = 0)
    {
        $this->regenerateProductsRangeUrlRewrites([], $storeId);
    }

    /**
     * @see parent::regenerateProductsRangeUrlRewrites()
     */
    public function regenerateProductsRangeUrlRewrites($productsFilter = [], $storeId = 0)
    {
        // Get products collection
        $products = $this->_getProductsCollection($storeId, $productsFilter);

        $pageCount = $products->getLastPageNumber();
        $currentPage = 1;

        while ($currentPage <= $pageCount) {
            $products->clear();
            $products->setCurPage($currentPage);
            $products->load();

            foreach ($products as $product) {
                $this->_productProcess($product, $storeId);
            }

            $currentPage++;
        }
    }

    /**
     * Get products collection
     *
     * @param integer $storeId
     * @param array $productsFilter
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getProductsCollection($storeId = 0, $productsFilter = [])
    {
        /**
         * @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productsCollection
         */
        $productsCollection = $this->_productCollectionFactory->create()
            ->setStore($storeId)
            ->addStoreFilter($storeId)
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('visibility')
            ->addAttributeToSelect('url_key')
            ->addAttributeToSelect('url_path')
            ->addAttributeToSelect('status')
            ->addAttributeToFilter('status', Status::STATUS_ENABLED)
            // use limit to avoid a "eating" of a memory
            ->setPageSize($this->_productsCollectionPageSize);

        if (count($productsFilter) > 0) {
            $productsCollection->addIdFilter($productsFilter);
        }

        return $productsCollection;
    }

    /**
     * Regenerate Url Rewrite for specific product
     *
     * @param  Product $product
     * @param  integer $storeId
     * @return void
     */
    protected function _productProcess($product, $storeId)
    {
        try {
            $product->setData('url_path', null)
                ->setStoreId($storeId);
            $generatedKey = $this->_productUrlPathGenerator->getUrlKey($product);
            $product->setData('url_key', $generatedKey);
            $this->_getProductAction()->updateAttributes(
                [$product->getId()],
                ['url_path' => null, 'url_key' => $generatedKey],
                $storeId
            );

            $productUrlRewriteResult = $this->_getProductUrlRewriteGenerator()->generate($product);

            $productUrlRewriteResult = $this->_sanitizeProductUrlRewrites($productUrlRewriteResult);

            try {
                $this->_urlPersist->replace($productUrlRewriteResult);
            } catch (UrlAlreadyExistsException $y) {
                // do something here
            } catch (Exception $y) {
                // do something here
            }
        } catch (Exception $e) {
            // do something here
        }
    }

    /**
     * Sanitize product URL rewrites
     *
     * @param  array $productUrlRewrites
     * @return array
     */
    protected function _sanitizeProductUrlRewrites($productUrlRewrites)
    {
        $paths = [];
        foreach ($productUrlRewrites as $key => $urlRewrite) {
            $path = $this->_clearRequestPath($urlRewrite->getRequestPath());
            if (!in_array($path, $paths)) {
                $productUrlRewrites[$key]->setRequestPath($path);
                $paths[] = $path;
            } else {
                unset($productUrlRewrites[$key]);
            }
        }

        return $productUrlRewrites;
    }
}
