<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CatalogUrlRewrite\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class AdditionalSuffix
 *
 * @package SITC\Sinchimport\Model\Config\Source
 */
class AdditionalSuffix implements ArrayInterface
{
    /**
     *
     */
    const ADDITIONAL_SUFFIX_CONFIG_NONE = 0;

    /**
     *
     */
    const ADDITIONAL_SUFFIX_CONFIG_PRODUCT_ID = 1;

    /**
     *
     */
    const ADDITIONAL_SUFFIX_CONFIG_PRODUCT_SKU = 2;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::ADDITIONAL_SUFFIX_CONFIG_NONE,
                'label' => 'None'
            ],
            [
                'value' => self::ADDITIONAL_SUFFIX_CONFIG_PRODUCT_ID,
                'label' => 'Product ID'
            ],
            [
                'value' => self::ADDITIONAL_SUFFIX_CONFIG_PRODUCT_SKU,
                'label' => 'Product SKU'
            ]
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::ADDITIONAL_SUFFIX_CONFIG_NONE => 'None',
            self::ADDITIONAL_SUFFIX_CONFIG_PRODUCT_ID => 'Product ID',
            self::ADDITIONAL_SUFFIX_CONFIG_PRODUCT_SKU => 'Product SKU'
        ];
    }
}
