<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CatalogUrlRewrite\Model\Product;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\ObjectManager;

/**
 * Class Url
 *
 * @package Tigren\CatalogUrlRewrite\Model\Product
 */
class Url extends Product\Url
{
    /**
     * Cleanup all rewrite urls for all stores
     *
     * @return mixed|void
     */
    public function cleanupCatalogUrlRewrites()
    {
        $this->_getResource()->cleanupCatalogUrlRewrites();
        echo "\n" . '=====> Removed all of Catalog Url Rewrites.' . "\n";
    }
    /**
     * Cleanup all rewrite urls for all stores
     *
     * @return mixed|void
     */
    public function regenerateUrlRewrites()
    {
        $this->_getResource()->regenerateUrlRewrites();
        echo "\n" . '=====> Regenerated all of Catalog Url Rewrites.' . "\n";
    }

    /**
     * Get url resource instance
     *
     * @return mixed
     */
    protected function _getResource()
    {
        return ObjectManager::getInstance()->get(
            'Tigren\CatalogUrlRewrite\Model\ResourceModel\Product\Url'
        );
    }
}
