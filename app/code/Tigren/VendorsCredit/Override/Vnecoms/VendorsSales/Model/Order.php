<?php
/**
 * @author Tigen Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\VendorsCredit\Override\Vnecoms\VendorsSales\Model;


use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Model\Order as BaseOrder;

/**
 * Class Order
 * @package Tigren\VendorsCredit\Override\Vnecoms\VendorsSales\Model
 */
class Order extends \Vnecoms\VendorsSales\Model\Order
{
    /**
     * @var \Vnecoms\VendorsCredit\Model\ResourceModel\Escrow\CollectionFactory
     */
    protected $_collectionFactory;


    /**
     * Order constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Vnecoms\VendorsCredit\Model\ResourceModel\Escrow\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PriceCurrencyInterface $priceCurrency,
        \Vnecoms\VendorsCredit\Model\ResourceModel\Escrow\CollectionFactory $collectionFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context, $registry, $priceCurrency, $resource, $resourceCollection, $data);
    }


    /**
     * @return bool
     */
    public function canCreditmemo()
    {
        if ($this->hasForcedCanCreditmemo()) {
            return $this->getForcedCanCreditmemo();
        }

        if ($this->canUnhold() || $this->getOrder()->isPaymentReview()) {
            return false;
        }

        $invoices = $this->getInvoiceCollection();
        $escrowComplete = false;
        foreach ($invoices as $invoice) {
            $escrows = $this->_collectionFactory->create()->addFieldToFilter('status', 2)
                ->addFieldToFilter('additional_info', array('like' => '%invoice|' . $invoice->getId() . '%'));
            if ($escrows->count() > 0) {
                $escrowComplete = true;
                break;
            }
        }
        if ($escrowComplete) {
            return false;
        }
        if ($this->isCanceled() || $this->getState() === BaseOrder::STATE_CLOSED) {
            return false;
        }

        if (abs($this->priceCurrency->round($this->getTotalPaid()) - $this->getTotalRefunded()) < .0001) {
            return false;
        }

        return true;
    }
}