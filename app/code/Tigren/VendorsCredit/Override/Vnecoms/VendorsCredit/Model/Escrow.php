<?php
namespace Tigren\VendorsCredit\Override\Vnecoms\VendorsCredit\Model;

use Vnecoms\VendorsCredit\Model\CreditProcessor\OrderPayment;
use Vnecoms\VendorsCredit\Model\CreditProcessor\ItemCommission;

/**
 * Class Escrow
 * @package Tigren\VendorsCredit\Override\Vnecoms\VendorsCredit\Model
 */
class Escrow extends \Vnecoms\VendorsCredit\Model\Escrow
{
    /**
     * Release the pending credit
     *
     * @throws \Exception
     * @return \Vnecoms\VendorsCredit\Model\Escrow
     */
    public function release()
    {
        if (!$this->canRelease()) {
            throw new \Exception(__("Can not release this transaction."));
        }

        /*Process pending payment.*/
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $vendor = $this->getVendor();
        $creditAccount = $om->create('Vnecoms\Credit\Model\Credit');
        $creditAccount->loadByCustomerId($vendor->getCustomer()->getId());
        $creditProcessor = $om->create('Vnecoms\Credit\Model\Processor');
        /** @var \Vnecoms\VendorsSales\Model\Order\Invoice */
        $vendorInvoice = $this->getInvoice();

        /** @var \Vnecoms\VendorsSales\Model\Order $vendorOrder */
        $vendorOrder = $vendorInvoice->getVendorOrder();
        $cancelStatus = ['canceled', 'closed'];
        if (in_array($vendorOrder->getStatus(), $cancelStatus)) {
            $this->setStatus(self::STATUS_CANCELED)->save();
            return $this;
        }

        /** @var \Magento\Sales\Model\Order */
        $order = $vendorOrder->getOrder();

        $amount = $this->getAmount();

        /*Create transaction to add invoice grandtotal to vendor credit account.*/
        $data = [
            'vendor' => $vendor,
            'type' => OrderPayment::TYPE,
            'amount' => $amount,
            'vendor_order' => $vendorOrder,
            'vendor_invoice' => $vendorInvoice,
            'order' => $order
        ];

        $creditProcessor->process($creditAccount, $data);

        /*Calculate commission and create transaction for each item.*/
        foreach ($vendorInvoice->getAllItems() as $item) {
            $orderItem = $item->getOrderItem();
            if (!$orderItem || $orderItem->getParentItemId()) {
                continue;
            }

            $trans = $om->create('Vnecoms\Credit\Model\Credit\Transaction')->getCollection()
                ->addFieldToFilter('type', ItemCommission::TYPE)
                ->addFieldToFilter('additional_info', ['like' => 'invoice_item|' . $item->getId() . '%']);
            if ($trans->count()) {
                continue;
            }

            $fee = $item->getCommission();

            /*Do nothing if the fee is zero*/
            if ($fee <= 0) {
                continue;
            }

            $additionalDescription = $item->getCommissionDescription();

            $data = [
                'vendor' => $vendor,
                'type' => ItemCommission::TYPE,
                'amount' => $fee,
                'invoice_item' => $item,
                'order' => $order,
                'vendor_invoice' => $vendorInvoice,
                'additional_description' => $additionalDescription,
            ];

            $creditProcessor->process($creditAccount, $data);
        }

        $this->setStatus(self::STATUS_COMPLETED)->save();

        return $this;
    }
}
