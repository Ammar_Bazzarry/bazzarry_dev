<?php

namespace Tigren\VendorsCredit\Override\Vnecoms\VendorsCredit\Observer;

use Vnecoms\VendorsCredit\Model\Escrow;
use Vnecoms\VendorsSales\Model\Order\Invoice;

/**
 * Class ProcessCommission
 * @package Tigren\VendorsCredit\Override\Vnecoms\VendorsCredit\Observer
 */
class ProcessCommission extends \Vnecoms\VendorsCredit\Observer\ProcessCommission
{
    /**
     * Create escrow transaction
     * @param Invoice $vendorInvoice
     */
    public function createEscrowTransaction(Invoice $vendorInvoice)
    {
        /*Add credit to vendor's credit account and calculate commission*/
        $vendor = $this->_vendorFactory->create();
        $vendor->load($vendorInvoice->getVendorId());

        /*Do nothing if the vendor is not exist*/
        if (!$vendor->getId()) {
            return;
        }

        /*Return if the transaction is exist.*/
        $escrows = $this->_escrowFactory->create()->getCollection()
            ->addFieldToFilter('relation_id', $vendorInvoice->getId());
        if ($escrows->count()) {
            return;
        }

        $amount = $vendorInvoice->getBaseGrandTotal() - $vendorInvoice->getBaseShippingAmount();

        $data = [
            'vendor_id' => $vendor->getId(),
            'relation_id' => $vendorInvoice->getId(),
            'amount' => $amount,
            'status' => Escrow::STATUS_PENDING,
            'additional_info' => 'order|' . $vendorInvoice->getOrder()->getId() . '|invoice|' . $vendorInvoice->getId(),
        ];

        $escrow = $this->_escrowFactory->create();
        $escrow->setData($data)->save();

        /* Send notification email*/
    }
}
