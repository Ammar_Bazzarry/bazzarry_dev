<?php

namespace Tigren\VendorsCredit\Override\Vnecoms\VendorsCredit\Cron;

use DateTime;
use Exception;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Vnecoms\VendorsCredit\Helper\Data;
use Vnecoms\VendorsCredit\Model\Escrow;
use Vnecoms\VendorsCredit\Model\ResourceModel\Escrow\CollectionFactory;

/**
 * Class ReleaseEscrowTrans
 * @package Tigren\VendorsCredit\Override\Vnecoms\VendorsCredit\Cron
 */
class ReleaseEscrowTrans
{
    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var Data
     */
    protected $_creditHelper;

    /**
     * ReleaseEscrowTrans constructor.
     * @param CollectionFactory $collectionFactory
     * @param LoggerInterface $logger
     * @param Data $creditHelper
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        LoggerInterface $logger,
        Data $creditHelper
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_logger = $logger;
        $this->_creditHelper = $creditHelper;
    }

    /**
     * Run process send product alerts
     *
     * @return void
     * @throws Exception
     */
    public function execute()
    {
        $this->_logger->log(LogLevel::INFO, __("Escrow cron is ran"));

        $collection = $this->_collectionFactory->create();
        $today = (new DateTime())->getTimestamp();
        $holdTimeDay = $this->_creditHelper->getHoldTimeDays();
        $holdTimeDay = $holdTimeDay ? $holdTimeDay : 0;

        $date = date('Y-m-d H:i:s', strtotime('-' . $holdTimeDay . ' days', $today));

        $collection->addFieldToFilter('status', Escrow::STATUS_PENDING)
            ->addFieldToFilter('created_at', ['lt' => $date]);

        $this->_logger->log(LogLevel::INFO,
            'Number of transactions will be processed: ' . $collection->count());

        foreach ($collection as $escrow) {
            try {
                $escrow->release();
                $this->_logger->log(LogLevel::INFO,
                    __("Escrow transaction #%1 is released", $escrow->getId()));
            } catch (Exception $e) {
                $this->_logger->log(LogLevel::ERROR, $e->getMessage());
            }
        }
    }
}
