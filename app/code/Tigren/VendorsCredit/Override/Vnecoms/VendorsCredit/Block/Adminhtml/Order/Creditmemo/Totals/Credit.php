<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2020 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */
namespace Tigren\VendorsCredit\Override\Vnecoms\VendorsCredit\Block\Adminhtml\Order\Creditmemo\Totals;

/**
 * Class Credit
 * @package Tigren\VendorsCredit\Override\Vnecoms\VendorsCredit\Block\Adminhtml\Order\Creditmemo\Totals
 */
class Credit extends \Vnecoms\Credit\Block\Adminhtml\Order\Creditmemo\Totals\Credit
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    protected $priceCurrency;
    protected $_storeManager;

    /**
     * Credit constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->priceCurrency = $priceCurrency;
        parent::__construct($context, $registry, $data);
    }

    /**
     * @return $this|\Vnecoms\Credit\Block\Adminhtml\Order\Creditmemo\Totals\Credit
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->setTemplate('Tigren_VendorsCredit::order/creditmemo/create/totals/credit.phtml');
        return $this;
    }
    /**
     * Retrieve credit memo model instance
     *
     * @return \Magento\Sales\Model\Order\Creditmemo
     */
    public function getCreditmemo()
    {
        return $this->_coreRegistry->registry('current_creditmemo');
    }

    /**
     * get Credit value that allow admin to refund.
     * @return float
     */
    public function getCreditValue()
    {
        $creditValue = $this->getCreditmemo()->getGrandTotal() + abs($this->getCreditmemo()->getCreditAmount());
        $rate = ($this->priceCurrency->convert($creditValue))/$creditValue;
        $creditValue = $creditValue / $rate;
        return $this->priceCurrency->round($creditValue);
    }

    /**
     * Display the block only for registered customer.
     * @see \Magento\Framework\View\Element\Template::_toHtml()
     */
    protected function _toHtml()
    {
        if (!$this->getCreditmemo()->getOrder()->getCustomerId()) {
            return '';
        }

        return parent::_toHtml();
    }
}
