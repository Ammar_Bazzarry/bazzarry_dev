<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */
namespace Tigren\VendorsCredit\Override\Vnecoms\VendorsCredit\Block\Vendors\Withdraw\History\View;

class Form extends \Vnecoms\VendorsCredit\Block\Vendors\Withdraw\History\View\Form
{
    protected $_template = 'Tigren_VendorsCredit::withdraw/history/view.phtml';
}
