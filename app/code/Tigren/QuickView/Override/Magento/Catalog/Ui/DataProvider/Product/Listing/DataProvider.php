<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Tigren\QuickView\Override\Magento\Catalog\Ui\DataProvider\Product\Listing;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Reporting;
use Magento\Store\Model\StoreManager;

/**
 * Provide information about current store and currency for product listing ui component
 */
class DataProvider extends \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider
{
    /**
     * @var StoreManager
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param Reporting $reporting
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param RequestInterface $request
     * @param FilterBuilder $filterBuilder
     * @param StoreManager $storeManager
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        Reporting $reporting,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        StoreManager $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            '',
            '',
            $reporting,
            $searchCriteriaBuilder,
            $request,
            $filterBuilder,
            $meta,
            $data
        );

        $this->name = $name;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $data = [];
        $store = $this->storeManager->getStore();
        $data['store'] = $store->getId();
        $data['currency'] = $store->getCurrentCurrency()->getCode();
        $data['enable_quickview'] = $this->scopeConfig->getValue('tigren_quick_view/general/enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $data['quickview_text'] = $this->scopeConfig->getValue('tigren_quick_view/general/button_text',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $data;
    }
}
