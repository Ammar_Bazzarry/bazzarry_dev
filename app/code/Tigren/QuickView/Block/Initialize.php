<?php
/**
 * @copyright Copyright (c) 2016 www.tigren.com
 */

namespace Tigren\QuickView\Block;

/**
 * Class Initialize
 * @package Tigren\QuickView\Block
 */
class Initialize extends \Magento\Framework\View\Element\Template
{
    /**
     * Returns config
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getConfig()
    {
        return [
            'baseUrl' => $this->getBaseUrl()
        ];
    }

    /**
     * Return base url.
     *
     * @codeCoverageIgnore
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }
}
