<?php

namespace Tigren\QuickView\Block\Catalog\Product\ProductList\Item\AddTo;

/**
 * Class QuickView
 * @package Tigren\QuickView\Block\Catalog\Product\ProductList\Item\AddTo
 */
class QuickView extends \Magento\Catalog\Block\Product\ProductList\Item\Block
{
    const XML_PATH_QUICKVIEW_ENABLED = 'tigren_quick_view/general/enabled';

    const XML_PATH_QUICKVIEW_BUTTONTEXT = 'tigren_quick_view/general/button_text';

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlInterface;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * QuickView constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->urlInterface = $urlInterface;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isQuickViewEnabled()
    {
        return (boolean) $this->scopeConfig->getValue(self::XML_PATH_QUICKVIEW_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getQuickViewButtonText()
    {
        return (string) $this->scopeConfig->getValue(self::XML_PATH_QUICKVIEW_BUTTONTEXT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getQuickViewUrl()
    {
        return $this->getUrl('quickview/catalog_product/view', ['id' => $this->getProduct()->getEntityId()]);
    }
}
