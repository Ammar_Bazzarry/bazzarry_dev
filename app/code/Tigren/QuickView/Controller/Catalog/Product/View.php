<?php
/**
 * @copyright Copyright (c) 2016 www.tigren.com
 */

namespace Tigren\QuickView\Controller\Catalog\Product;

class View extends \Magento\Catalog\Controller\Product\View
{
    public function execute()
    {
        if($this->getRequest()->getParam('is_redirect') && ($productId = (int) $this->getRequest()->getParam('id'))){
            $product = $this->_initProduct();
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($product->getProductUrl());
            return $resultRedirect;
        }else{
            return parent::execute();
        }
    }
}
