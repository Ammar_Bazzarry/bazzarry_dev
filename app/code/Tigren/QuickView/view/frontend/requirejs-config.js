var config = {
    map: {
        '*': {
            magnificPopup: 'Tigren_QuickView/js/jquery.magnific-popup.min',
            tigrenQuickView: 'Tigren_QuickView/js/quick_view'
        }
    },
    shim: {
        magnificPopup: {
            deps: ['jquery']
        }
    }
};