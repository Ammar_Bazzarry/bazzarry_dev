<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */
namespace Tigren\VendorsDashboard\Plugin\Vnecoms\VendorsDashboard\Controller\Vendors\Index;

class Index extends \Vnecoms\VendorsDashboard\Controller\Vendors\Index\Index
{
    public function aroundExecute(\Vnecoms\VendorsDashboard\Controller\Vendors\Index\Index $obj)
    {
        $this->_initAction();
        $this->setActiveMenu('Vnecoms_Vendors::dashboard');
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__("Seller Dashboard"));
        $this->_addBreadcrumb(__("Seller Dashboard"), __("Seller Dashboard"));
        $this->_view->renderLayout();
    }
}
