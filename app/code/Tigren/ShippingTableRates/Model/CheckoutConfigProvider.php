<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\ShippingTableRates\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

/**
 * Class CheckoutConfigProvider
 */
class CheckoutConfigProvider implements ConfigProviderInterface
{
    /**
     * @var \Amasty\MultiInventory\Model\WarehouseFactory
     */
    protected $warehouseFactory;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var \Amasty\ShippingTableRates\Model\ResourceModel\Rate
     */
    private $resourceModelRate;

    /**
     * CheckoutConfigProvider constructor.
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory
     * @param \Amasty\ShippingTableRates\Model\ResourceModel\Rate $rate
     * @param CheckoutSession $session
     */
    public function __construct(
        \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory,
        \Amasty\ShippingTableRates\Model\ResourceModel\Rate $rate,
        CheckoutSession $session
    ) {
        $this->warehouseFactory = $warehouseFactory;
        $this->checkoutSession = $session;
        $this->resourceModelRate = $rate;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $collection = $this->warehouseFactory->create()->getCollection();
        $result = [];
        foreach ($collection as $warehouse) {
            $result['warehouse_' . $warehouse->getId()]['shipping_title'] = __($warehouse->getTitle());
        }
        return ['warehouse' => $result];
    }
}