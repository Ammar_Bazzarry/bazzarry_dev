/*
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

var config = {
    map: {
        '*': {
            'Magento_Checkout/template/shipping.html':
                'Tigren_ShippingTableRates/template/shipping.html',
            'Magento_Tax/template/checkout/summary/shipping.html':
                'Tigren_ShippingTableRates/template/summary/shipping.html',
            'Magento_Checkout/js/model/shipping-rate-processor/customer-address':
                'Tigren_ShippingTableRates/js/model/shipping-rate-processor/customer-address',
            'Amasty_Conditions/js/action/recollect-totals':
                'Tigren_ShippingTableRates/js/action/recollect-totals',
            'Amasty_Conditions/js/model/conditions-subscribe':
                'Tigren_ShippingTableRates/js/model/conditions-subscribe'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Tigren_ShippingTableRates/js/view/shipping': true
            },
            'Magento_Checkout/js/view/shipping-information': {
                'Tigren_ShippingTableRates/js/view/shipping-information': true
            },
            'Magento_Tax/js/view/checkout/summary/shipping': {
                'Tigren_ShippingTableRates/js/view/summary/shipping': true
            },
            'Magento_Checkout/js/view/summary/abstract-total': {
                'Tigren_ShippingTableRates/js/view/summary/abstract-total-mixin': true
            }
        }
    }
};