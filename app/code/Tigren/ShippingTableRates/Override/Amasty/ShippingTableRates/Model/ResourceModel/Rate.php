<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\ShippingTableRates\Override\Amasty\ShippingTableRates\Model\ResourceModel;

/**
 * Class Rate
 * @package Tigren\ShippingTableRates\Override\Amasty\ShippingTableRates\Model\ResourceModel
 */
class Rate extends \Amasty\ShippingTableRates\Model\ResourceModel\Rate
{
    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return \Amasty\ShippingTableRates\Model\ResourceModel\Rate
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $connection = $this->getConnection();
        $methodId = $object->getMethodId();
        if ($methodId) {
            $select = $connection->select()->from('amasty_table_method', 'warehouse')
                ->where('id = ?', $methodId);
            $warehouseId = $connection->fetchOne($select);
            $object->setData('warehouse_id', $warehouseId);
        }
        return parent::_beforeSave($object);
    }
}
