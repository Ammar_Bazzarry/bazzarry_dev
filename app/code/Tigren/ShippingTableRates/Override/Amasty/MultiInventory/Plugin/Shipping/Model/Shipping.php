<?php
/**
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

namespace Tigren\ShippingTableRates\Override\Amasty\MultiInventory\Plugin\Shipping\Model;

use Amasty\MultiInventory\Api\WarehouseRepositoryInterface;
use Amasty\MultiInventory\Helper\Cart;
use Amasty\MultiInventory\Helper\System;
use Amasty\MultiInventory\Model\ShippingFactory;
use Amasty\MultiInventory\Model\Warehouse;
use Amasty\MultiInventory\Model\WarehouseFactory;
use Closure;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Module\Manager;
use Magento\Framework\Registry;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Quote\Model\Quote\Item;
use Magento\Shipping\Model\Rate\Result;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Shipping
 * @package Tigren\ShippingTableRates\Override\Amasty\MultiInventory\Plugin\Shipping\Model
 */
class Shipping
{
    /**
     *
     */
    const  METHOD_SEPARATOR = '|_|';

    /**
     *
     */
    const SEPARATOR = '||';

    /**
     * @var MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * @var \Amasty\MultiInventory\Model\Warehouse\Cart
     */
    private $cart;

    /**
     * @var System
     */
    private $system;

    /**
     * @var WarehouseFactory
     */
    private $factory;

    /**
     * @var WarehouseRepositoryInterface
     */
    private $repostiory;

    /**
     * @var Cart
     */
    private $helperCart;

    /**
     * @var ShippingFactory
     */
    private $whShipping;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var Manager
     */
    private $manager;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * Shipping constructor.
     * @param \Amasty\MultiInventory\Model\Warehouse\Cart $cart
     * @param WarehouseFactory $factory
     * @param WarehouseRepositoryInterface $repository
     * @param System $system
     * @param Cart $helperCart
     * @param Registry $registry
     * @param ShippingFactory $whShipping
     * @param ScopeConfigInterface $scopeConfig
     * @param ManagerInterface $messageManager
     * @param Manager $manager
     * @param MethodFactory $rateMethodFactory
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        \Amasty\MultiInventory\Model\Warehouse\Cart $cart,
        WarehouseFactory $factory,
        WarehouseRepositoryInterface $repository,
        System $system,
        Cart $helperCart,
        Registry $registry,
        ShippingFactory $whShipping,
        ScopeConfigInterface $scopeConfig,
        ManagerInterface $messageManager,
        Manager $manager,
        MethodFactory $rateMethodFactory,
        CheckoutSession $checkoutSession
    ) {
        $this->cart = $cart;
        $this->factory = $factory;
        $this->repostiory = $repository;
        $this->system = $system;
        $this->helperCart = $helperCart;
        $this->whShipping = $whShipping;
        $this->scopeConfig = $scopeConfig;
        $this->messageManager = $messageManager;
        $this->registry = $registry;
        $this->manager = $manager;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Separate rates, if some warehouses
     *
     * @param \Magento\Shipping\Model\Shipping $shipping
     * @param Closure $work
     * @param RateRequest $request
     * @return \Magento\Shipping\Model\Shipping
     * @throws LocalizedException
     */
    public function aroundCollectRates(
        \Magento\Shipping\Model\Shipping $shipping,
        Closure $work,
        RateRequest $request
    ) {
        if (!$this->system->isMultiEnabled() || !$this->system->getDefinationWarehouse()) {
            return $work($request);
        }
        $mainVendor = $this->scopeConfig->getValue('customvendor/general/main_vendor');
        $oldQuoteItems = [];
        $quoteItem = current($request->getAllItems());
        if ($quoteItem instanceof Item) {
            $this->cart->setQuote($quoteItem->getQuote());
        }

        if ($this->registry->registry('finish_quote_save') !== true) {
            $this->cart->addWhToItems();
            /** @var Item $item */
            foreach ($this->cart->getQuote()->getItemsCollection()->getItems() as $item) {
                if ($item->getId()) {
                    $oldQuoteItems[$item->getId()] = $item->getQty();
                }
            }
        }

        $warehouses = $this->cart->getWarehouses();
        $result = $forShipResult = [];
        $warehouseItemsString = [];
        foreach ($warehouses as $warehouseId) {
            $warehouse = $this->repostiory->getById($warehouseId);
            $request = $this->helperCart->changeRequestAddress(
                $request,
                $warehouse->getData()
            );
            // get all cart items of current warehouse
            $items = $this->cart->getGroupItems($warehouseId);
            $groupItems = $addedParents = [];
            $itemIds = [];
            foreach ($items as $item) {
                $quoteItem = $this->cart->getQuote()->getItemById($item['quote_item_id']);
                if ($this->registry->registry('finish_quote_save') !== true) {
                    $quoteItem->setData(Item::KEY_QTY, $item['qty']);
                }

                $parentId = $quoteItem->getParentItemId();
                if ($parentId) {
                    $itemIds[] = $parentId . '-' . (int) $item['qty'] . '-' . (int) $item['vendor_id'];
                    $parentItem = $this->cart->getQuote()->getItemById($quoteItem->getParentItemId());
                    if (!in_array($parentId, $addedParents)) {
                        $addedParents[] = $parentId;
                        $groupItems[] = $parentItem;
                    }
                    if ($parentItem->getProductType() == 'bundle') {
                        continue;
                    }
                } else {
                    $itemIds[] = $quoteItem->getId() . '-' . (int) $item['qty'] . '-' . (int) $item['vendor_id'];
                }

                $groupItems[] = $quoteItem;
            }
            $warehouseItemsString[] = $warehouseId . ':' . implode(',', $itemIds);
            $request = $this->helperCart->changeRequestItems($request, $groupItems, $this->cart->getQuote());
            $request->setWarehouseId($warehouseId);
            $shipment = $this->shipmentCalculate($request, $work);
            if ($warehouse->getIsShipping()) {
                $shipment = $this->helperCart->changePrice($shipment, $warehouse);
            }

            $result[$warehouseId] = $shipment;
            $methods = [];
            foreach ($shipment->getAllRates() as $resultMethod) {
                $methods[] = [
                    'method' => $resultMethod->getMethod(),
                    'carrier_code' => $resultMethod->getCarrier(),
                    'price' => $resultMethod->getPrice()
                ];
            }
            $forShipResult[$warehouseId] = $methods;
        }
        $warehousesMethod = $this->_rateMethodFactory->create();
        $warehousesMethod->setCarrier('warehouse_multirate');
        $warehousesMethod->setCarrierTitle('Multiple Rate');

        $warehousesMethod->setMethod('warehouse_list_' . implode('||', $warehouseItemsString));
        $warehousesMethod->setMethodTitle('warehouse_list_' . implode('||', $warehouseItemsString));
        $shipping->getResult()->append($warehousesMethod);
        $this->registry->unregister('amasty_quote_methods');
        $this->registry->register('amasty_quote_methods', $forShipResult);

        foreach ($result as $warehouseMethod) {
            $shipping->getResult()->append($warehouseMethod);
        }
        foreach ($oldQuoteItems as $key => $qty) {
            $quoteItem = $this->cart->getQuote()->getItemById($key);
            if ($quoteItem) {
                $quoteItem->setData(Item::KEY_QTY, $qty);
            }
        }
        $this->registry->unregister('finish_quote_save');
        $this->registry->register('finish_quote_save', false);

        $shippingRates = $shipping->getResult()->getAllRates();
        $newWarehouseRates = [];

        foreach ($this->groupShippingRatesByWarehouse($shippingRates) as $warehouseId => $rates) {
            if (!sizeof($newWarehouseRates)) {
                if ($warehouseId == 1) {
                    $ratesByVendor = [];
                    foreach ($rates as $rate) {
                        $methodCode = $rate->getMethod();
                        $tmp = explode(self::SEPARATOR,$methodCode);
                        $ratesByVendor[$tmp[2]][] = $rate;
                    }
                    foreach ($ratesByVendor as $vendorId => $vendorRates) {
                        if (!sizeof($newWarehouseRates)) {
                            foreach ($vendorRates as $rate) {
                                $newWarehouseRates[$rate->getCarrier() . '_' . $rate->getMethod()] = [
                                    'title' => $rate->getMethodTitle() . self::SEPARATOR . $warehouseId . self::SEPARATOR . $vendorId,
                                    'price' => $rate->getPrice()
                                ];
                            }
                        } else {
                            $tmpRates = [];
                            foreach ($vendorRates as $rate) {
                                foreach ($newWarehouseRates as $cod => $shippingRate) {
                                    $tmpRates[$cod . self::METHOD_SEPARATOR . $rate->getCarrier() . '_' . $rate->getMethod()] = [
                                        'title' => $shippingRate['title'] . self::METHOD_SEPARATOR . $rate->getMethodTitle() . self::SEPARATOR . $warehouseId . self::SEPARATOR . $vendorId,
                                        'price' => $shippingRate['price'] + $rate->getPrice(),
                                    ];
                                }
                            }
                            $newWarehouseRates = $tmpRates;
                        }
                    }
                } else {
                    foreach ($rates as $rate) {
                        $newWarehouseRates[$rate->getCarrier() . '_' . $rate->getMethod()] = [
                            'title' =>  $rate->getMethodTitle() . self::SEPARATOR . $warehouseId . self::SEPARATOR . $mainVendor,
                            'price' => $rate->getPrice()
                        ];
                    }
                }
            } else {
                $tmpRates = [];
                foreach ($rates as $rate) {
                    foreach ($newWarehouseRates as $cod => $shippingRate) {
                        $tmpRates[$cod . self::METHOD_SEPARATOR . $rate->getCarrier() . '_' . $rate->getMethod()] = [
                            'title' => $shippingRate['title'] . self::METHOD_SEPARATOR . $rate->getMethodTitle() . self::SEPARATOR . $warehouseId. self::SEPARATOR . $mainVendor,
                            'price' => $shippingRate['price'] + $rate->getPrice(),
                        ];
                    }
                }
                $newWarehouseRates = $tmpRates;
            }
        }
        foreach ($newWarehouseRates as $code => $shippingRate) {
            $method = $this->_rateMethodFactory->create();
            $method->setCarrier('warehouse_multirate');
            $method->setCarrierTitle('Multiple Rate');

            $method->setMethod($code);
            $method->setMethodTitle($shippingRate['title']);

            $method->setPrice($shippingRate['price']);
            $method->setCost($shippingRate['price']);
            $shipping->getResult()->append($method);
        }
        return $shipping;
    }

    /**
     * @param RateRequest $request
     * @param Closure $work
     *
     * @return Result
     */
    private function shipmentCalculate(RateRequest $request, $work)
    {
        $storeId = $request->getStoreId();
        /** @var \Amasty\MultiInventory\Model\Shipping $whShipping */
        $whShipping = $this->whShipping->create();
        $limitCarrier = $request->getLimitCarrier();
        if (!$limitCarrier || $limitCarrier == 'warehouse_multirate') {
            if (!$this->manager->isEnabled('Amasty_Shiprules')) {
                foreach ($this->getCarriers($storeId) as $carrierCode => $carrierConfig) {
                    $whShipping->collectCarrierRates($carrierCode, $request);
                }
            } elseif (!$this->registry->registry('is_shipping_rules')) {
                $work($request);
                $this->registry->register('is_shipping_rules', true);
            }
        } else {
            if (!is_array($limitCarrier)) {
                $limitCarrier = [$limitCarrier];
            }
            foreach ($limitCarrier as $carrierCode) {
                $carrierConfig = $this->getCarriers($storeId, $carrierCode);
                if (!$carrierConfig) {
                    continue;
                }

                $whShipping->collectCarrierRates($carrierCode, $request);
            }
        }

        return $whShipping->getResult();
    }

    /**
     * @param int $storeId
     * @param string|null $carrierCode
     *
     * @return array
     */
    private function getCarriers($storeId, $carrierCode = null)
    {
        $configPath = 'carriers';
        if ($carrierCode !== null) {
            $configPath .= '/' . $carrierCode;
        }
        return $this->scopeConfig->getValue(
            $configPath,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Group shipping rates by each warehouse.
     * @param $shippingRates
     * @return array
     */
    public function groupShippingRatesByWarehouse($shippingRates)
    {
        $rates = [];
        foreach ($shippingRates as $rate) {
            if (!$rate->getWarehouseId()) {
                continue;
            }
            if (!isset($rates[$rate->getWarehouseId()])) {
                $rates[$rate->getWarehouseId()] = [];
            }
            $rates[$rate->getWarehouseId()][] = $rate;
        }
        ksort($rates);
        return $rates;
    }
}
