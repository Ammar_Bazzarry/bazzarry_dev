<?php

namespace Tigren\CustomShipment\Plugin\Controller\Adminhtml\Order\Shipment;

use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\ShipmentItemCreationInterface;
use Magento\Sales\Api\Data\ShipmentItemCreationInterfaceFactory;
use Magento\Sales\Api\Data\ShipmentTrackCreationInterface;
use Magento\Sales\Api\Data\ShipmentTrackCreationInterfaceFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Model\Order\Shipment\Validation\QuantityValidator;
use Magento\Sales\Model\Order\ShipmentDocumentFactory;

/**
 * Class Save
 * @package Tigren\CustomShipment\Plugin\Controller\Adminhtml\Order\Shipment
 */
class Save extends \Magento\Shipping\Controller\Adminhtml\Order\Shipment\Save
{
    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $order;
    /**
     * @var
     */
    private $shipmentValidator;
    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var ShipmentDocumentFactory
     */
    private $documentFactory;

    /**
     * @var ShipmentTrackCreationInterfaceFactory
     */
    private $trackFactory;

    /**
     * @var ShipmentItemCreationInterfaceFactory
     */
    private $itemFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    protected $warehouseFactory;

    /**
     * Save constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader
     * @param \Magento\Shipping\Model\Shipping\LabelGenerator $labelGenerator
     * @param \Magento\Sales\Model\Order\Email\Sender\ShipmentSender $shipmentSender
     * @param ShipmentRepositoryInterface $shipmentRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param ShipmentDocumentFactory $documentFactory
     * @param \Magento\Framework\Registry $registry
     * @param ShipmentTrackCreationInterfaceFactory $trackFactory
     * @param ShipmentItemCreationInterfaceFactory $itemFactory
     * @param \Magento\Sales\Model\Order $order
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader,
        \Magento\Shipping\Model\Shipping\LabelGenerator $labelGenerator,
        \Magento\Sales\Model\Order\Email\Sender\ShipmentSender $shipmentSender,
        ShipmentRepositoryInterface $shipmentRepository,
        OrderRepositoryInterface $orderRepository,
        ShipmentDocumentFactory $documentFactory,
        \Magento\Framework\Registry $registry,
        ShipmentTrackCreationInterfaceFactory $trackFactory,
        ShipmentItemCreationInterfaceFactory $itemFactory,
        \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory,
        \Magento\Sales\Model\Order $order
    ) {
        $this->warehouseFactory = $warehouseFactory;
        $this->registry = $registry;
        $this->shipmentRepository = $shipmentRepository;
        $this->orderRepository = $orderRepository;
        $this->documentFactory = $documentFactory;
        $this->trackFactory = $trackFactory;
        $this->order = $order;
        $this->itemFactory = $itemFactory;
        parent::__construct($context, $shipmentLoader, $labelGenerator, $shipmentSender);
    }

    /**
     * @param \Magento\Shipping\Controller\Adminhtml\Order\Shipment\Save $subject
     * @param $proceed
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function aroundExecute(\Magento\Shipping\Controller\Adminhtml\Order\Shipment\Save $subject, $proceed)
    {
        $request = $subject->getRequest();
        $shippingData = [];
        $data = $request->getParam('shipment');
        if (isset($data['warehouse']) && count($data['warehouse'])){
            $warehouses = $data['warehouse'];
        }
        if (isset($data['items']) && count($data['items'])){
            $items = $data['items'];
            foreach ($items as $key => $item){
                if ($item == 0){
                    unset($items[$key]);
                    if (isset($warehouses[$key])){
                        unset($warehouses[$key]);
                    }
                }else{
                    if(!isset($warehouses[$key])){
                        $warehouses[$key] = $this->warehouseFactory->create()->getDefaultId();
                    }
                }
            }
        }

        if (isset($items) && count($items) && isset($warehouses) && count($warehouses)) {
            $isNeedCreateLabel = !empty($data['create_shipping_label']);
            if (count($warehouses) > 0 && count($items) > 0) {
                foreach ($warehouses as $itemId => $warehouse) {
                    $groupBranches[$warehouse][] = ['SKU' => $itemId, 'Number' => $items[$itemId]];
                }
                foreach ($groupBranches as $branchID => $items) {
                    $order = $this->order->load($request->getParam('order_id'));
                    $oitems = $order->getAllItems();
                    foreach ($oitems as $oitem) {
                        foreach ($items as $item) {
                            if ($oitem->getId() == $item['SKU']) {
                                $shippingData['items'][$oitem->getItemId()] = $item['Number'];
                            }
                        }
                    }

                    $responseAjax = new \Magento\Framework\DataObject();
                    try {
                        $shipmentId = $request->getParam('shipment_id');
                        $orderId = $request->getParam('order_id');
                        if ($shipmentId) {
                            /** @var \Magento\Sales\Model\Order\Shipment $shipment */
                            $shipment = $this->shipmentRepository->get($shipmentId);
                        } elseif ($orderId) {
                            $order = $this->orderRepository->get($orderId);

                            /**
                             * Check order existing
                             */
                            if (!$order->getId()) {
                                $this->messageManager->addError(__('The order no longer exists.'));
                                return false;
                            }
                            /**
                             * Check shipment is available to create separate from invoice
                             */
                            if ($order->getForcedShipmentWithInvoice()) {
                                $this->messageManager->addError(__('Cannot do shipment for the order separately from invoice.'));
                                return false;
                            }
                            /**
                             * Check shipment create availability
                             */
                            if (!$order->canShip()) {
                                $this->messageManager->addError(__('Cannot do shipment for the order.'));
                                return false;
                            }

                            $shipmentItems = $this->getShipmentItems($shippingData);

                            $shipment = $this->documentFactory->create(
                                $order,
                                $shipmentItems,
                                $this->getTrackingArray($request->getParam('tracking'))
                            );
                        }
                        if (!$shipment) {
                            $this->_forward('noroute');
                            return;
                        }

                        $validationResult = $this->getShipmentValidator()
                            ->validate($shipment, [QuantityValidator::class]);
                        if ($validationResult->hasMessages()) {
                            $this->messageManager->addError(
                                __("Shipment Document Validation Error(s):\n" . implode("\n",
                                        $validationResult->getMessages()))
                            );
                            $this->_redirect('*/*/new', ['order_id' => $this->getRequest()->getParam('order_id')]);
                            return;
                        }

                        $shipment->register();

                        $shipment->getOrder()->setCustomerNoteNotify(!empty($shippingData['send_email']));
                        $shipment->setData('warehouse_id', $branchID);

                        if ($isNeedCreateLabel) {
                            $this->labelGenerator->create($shipment, $this->_request);
                            $responseAjax->setOk(true);
                        }
                        $this->_saveShipment($shipment);


                        if (!empty($shippingData['send_email'])) {
                            $this->shipmentSender->send($shipment);
                        }

                        $shipmentCreatedMessage = __('The shipment has been created.');
                        $labelCreatedMessage = __('You created the shipping label.');

                        $this->messageManager->addSuccess(
                            $isNeedCreateLabel ? $shipmentCreatedMessage . ' ' . $labelCreatedMessage : $shipmentCreatedMessage
                        );
                        $this->_objectManager->get(\Magento\Backend\Model\Session::class)->getCommentText(true);
                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                        if ($isNeedCreateLabel) {
                            $responseAjax->setError(true);
                            $responseAjax->setMessage($e->getMessage());
                        } else {
                            $this->messageManager->addError($e->getMessage());
                            $this->_redirect('*/*/new', ['order_id' => $this->getRequest()->getParam('order_id')]);
                        }
                    } catch (\Exception $e) {
                        $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
                        if ($isNeedCreateLabel) {
                            $responseAjax->setError(true);
                            $responseAjax->setMessage(__('An error occurred while creating shipping label.'));
                        } else {
                            $this->messageManager->addError(__('Cannot save shipment.'));
                            $this->_redirect('*/*/new', ['order_id' => $this->getRequest()->getParam('order_id')]);
                        }
                    }
                    if ($isNeedCreateLabel) {
                        $this->getResponse()->representJson($responseAjax->toJson());
                    }
                };
                $this->registry->register('current_shipment', $shipment);
                $this->messageManager->addSuccess(
                    'The shipment has been created.'
                );
                return $this->_redirect('sales/order/view', ['order_id' => $shipment->getOrderId()]);
            }
        }

        return $proceed();
    }

    /**
     * @param array $shipmentData
     * @return array
     */
    private function getShipmentItems(array $shipmentData)
    {
        $shipmentItems = [];
        $itemQty = isset($shipmentData['items']) ? $shipmentData['items'] : [];
        foreach ($itemQty as $itemId => $quantity) {
            /** @var ShipmentItemCreationInterface $item */
            $item = $this->itemFactory->create();
            $item->setOrderItemId($itemId);
            $item->setQty($quantity);
            $shipmentItems[] = $item;
        }
        return $shipmentItems;
    }

    /**
     * @param $tracks
     * @return array
     * @throws LocalizedException
     */
    private function getTrackingArray($tracks)
    {
        $tracks = $tracks ?: [];
        $trackingCreation = [];
        foreach ($tracks as $track) {
            if (!isset($track['number']) || !isset($track['title']) || !isset($track['carrier_code'])) {
                throw new LocalizedException(
                    __('Tracking information must contain title, carrier code, and tracking number')
                );
            }
            /** @var ShipmentTrackCreationInterface $trackCreation */
            $trackCreation = $this->trackFactory->create();
            $trackCreation->setTrackNumber($track['number']);
            $trackCreation->setTitle($track['title']);
            $trackCreation->setCarrierCode($track['carrier_code']);
            $trackingCreation[] = $trackCreation;
        }

        return $trackingCreation;
    }

    /**
     * @return \Magento\Sales\Model\Order\Shipment\ShipmentValidatorInterface|mixed
     */
    private function getShipmentValidator()
    {
        if ($this->shipmentValidator === null) {
            $this->shipmentValidator = $this->_objectManager->get(
                \Magento\Sales\Model\Order\Shipment\ShipmentValidatorInterface::class
            );
        }

        return $this->shipmentValidator;
    }
}