<?php

namespace Lof\SmsNotification\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class msg91ApiCall
 * @package Lof\SmsNotification\Helper
 */
class msg91ApiCall extends AbstractHelper
{
    const XML_MSG91_API_SENDERID = 'lofsmsnotification/sms_settings/msg91senderid';

    const XML_MSG91_API_AUTHKEY = 'lofsmsnotification/sms_settings/msg91authkey';

    const XML_MSG91_API_URL = 'lofsmsnotification/sms_settings/msg91apiurl';

    /**
     * msg91ApiCall constructor.
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function validateSmsConfig()
    {
        return $this->getApiUrl() && $this->getAuthKey() && $this->getApiSenderId();
    }

    /**
     * @return mixed
     */
    public function getApiUrl()
    {
        return $this->scopeConfig->getValue(
            self::XML_MSG91_API_URL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getAuthKey()
    {
        return $this->scopeConfig->getValue(
            self::XML_MSG91_API_AUTHKEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getApiSenderId()
    {
        return $this->scopeConfig->getValue(
            self::XML_MSG91_API_SENDERID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $mobilenumbers
     * @param $message
     * @return mixed|string
     */
    public function callApiUrl($mobilenumbers, $message)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/yourFather.log');

        $logger = new \Zend\Log\Logger();

        $logger->addWriter($writer);

        $logger->info(print_r($mobilenumbers, true));
        $url = $this->getApiUrl();
        $authkey = $this->getAuthKey();
        $senderid = $this->getApiSenderId();

        $ch = curl_init();
        if (!$ch) {
            return "Couldn't initialize a cURL handle";
        }
        $ret = curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "authkey=$authkey&mobiles=$mobilenumbers&message=$message&sender=$senderid&route=4&country=0");
        $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $curlresponse = curl_exec($ch); // execute

        return $curlresponse;
    }
}