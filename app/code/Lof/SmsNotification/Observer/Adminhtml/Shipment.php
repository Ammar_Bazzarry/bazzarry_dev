<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\SmsNotification\Observer\Adminhtml;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class Shipment
 * @package Lof\SmsNotification\Observer\Adminhtml
 */
class Shipment implements ObserverInterface
{
    /**
     * @var \Lof\SmsNotification\Model\SendSms
     */
    protected $sendsms;

    /**
     * @var \Lof\SmsNotification\Helper\Data
     */
    protected $_smsNotificationData;

    /**
     * @var msg91ApiCall
     */
    protected $_msg91ApiCall;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * Shipment constructor.
     * @param \Lof\SmsNotification\Model\SendSms $sendsms
     * @param \Lof\SmsNotification\Helper\Data $smsNotificationData
     * @param \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Lof\SmsNotification\Model\SendSms $sendsms,
        \Lof\SmsNotification\Helper\Data $smsNotificationData,
        \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_smsNotificationData = $smsNotificationData;
        $this->_msg91ApiCall = $msg91ApiCall;
        $this->_logger = $logger;
        $this->sendsms = $sendsms;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Http $request */

        $helperData = $this->_smsNotificationData;
        $msg91ApiCall = $this->_msg91ApiCall;
        $shipment = $observer->getEvent()->getShipment();
        /** @var \magento\Sales\Model\Order $order */
        $order = $shipment->getOrder();

        $storeName = $order->getStore()->getName();
        $orderCretaedAt = date("F j, Y", strtotime($order->getCreatedAt()));
        $shipmentCretaedAt = date("F j, Y", strtotime($shipment->getCreatedAt()));
        $orderTotal = number_format($order->getGrandTotal(), 2, '.', '');
        $orderEmail = $order->getCustomerEmail();
        $orderId = $order->getIncrementId();
        $orderOldStatus = $order->getStatus();
        $orderNewStatus = "Complete";
        $mobilenumber = $order->getShippingAddress()->getTelephone();
        $shipNumber = $order->getShippingAddress()->getTelephone();
        $send_to = 'customer';
        $orderShimentCustomerFirstName = $order->getCustomerFirstname();
        $orderShimentCustomerLastName = $order->getCustomerLastname();

        if ($helperData->getConfig('sms_customer/enable_new_shipment')) {
            $smsType = $helperData->getConfig('sms_settings/type_sms');

            $message = $helperData->getShipmentMessageForUser(
                $orderCretaedAt,
                $shipmentCretaedAt,
                $orderTotal,
                $orderEmail,
                $orderId,
                $orderOldStatus,
                $orderNewStatus,
                $storeName,
                $mobilenumber,
                $orderShimentCustomerFirstName,
                $orderShimentCustomerLastName);
            if ($smsType === 'bulksms') {
                $temp = $this->sendsms->send($shipNumber, $message, $send_to);
            }
            if ($smsType === 'msg91') {
                if (!$msg91ApiCall->validateSmsConfig()) {
                    $this->_logger->error('Please Configure all SMS Gateway Fields Configuration.');
                    return;
                }
                $this->_msg91ApiCall->callApiUrl($shipNumber, $message);
            }
        }
        return true;
    }
}