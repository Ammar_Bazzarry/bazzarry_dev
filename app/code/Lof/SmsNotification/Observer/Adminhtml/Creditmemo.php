<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\SmsNotification\Observer\Adminhtml;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class Creditmemo
 * @package Lof\SmsNotification\Observer\Adminhtml
 */
class Creditmemo implements ObserverInterface
{
    /**
     * @var \Lof\SmsNotification\Model\SendSms
     */
    protected $sendsms;

    /**
     * @var \Lof\SmsNotification\Helper\Data
     */
    protected $_smsNotificationData;

    /**
     * @var msg91ApiCall
     */
    protected $_msg91ApiCall;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    /**
     * Creditmemo constructor.
     * @param \Lof\SmsNotification\Model\SendSms $sendsms
     * @param \Lof\SmsNotification\Helper\Data $smsNotificationData
     * @param \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Lof\SmsNotification\Model\SendSms $sendsms,
        \Lof\SmsNotification\Helper\Data $smsNotificationData,
        \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_smsNotificationData = $smsNotificationData;
        $this->_msg91ApiCall = $msg91ApiCall;
        $this->_logger = $logger;
        $this->_orderFactory = $orderFactory;
        $this->_customerFactory = $customerFactory;
        $this->sendsms = $sendsms;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Http $request */
        $helperData = $this->_smsNotificationData;
        $msg91ApiCall = $this->_msg91ApiCall;
        $creditmemo = $observer->getData('creditmemo');
        $order = $this->_orderFactory->create()->load($creditmemo->getOrderId());
        $order_information = $order->loadByIncrementId($creditmemo->getOrderId());
        $customerid = $order->getCustomerId();
        $customer = $this->_customerFactory->create()->load($customerid);
        $orderTotal = $order_information->getGrandTotal();
        $orderIncrementId = $order_information->getIncrementId();
        $email = $order_information->getCustomerEmail();
        $orderCrated = $order_information->getCreatedAt();
        $shipNumber = $customer->getMobilenumber();
        $customerName = $customer->getFirstname() . ' ' . $customer->getLastname();
        if($order_information->getShippingAddress()){
            $shipNumber = $order_information->getShippingAddress()->getTelephone();
        }
        $send_to = 'customer';
        if ($helperData->getConfig('sms_customer/enable_new_credit_memo')) {
            $smsType = $helperData->getConfig('sms_settings/type_sms');
            $message = $helperData->getCreditMemoMessageForUser(
                $orderTotal,
                $orderIncrementId,
                $email,
                $orderCrated,
                $customerName);
            if ($smsType === 'bulksms') {
                $temp = $this->sendsms->send($shipNumber, $message, $send_to);
            }
            if ($smsType === 'msg91') {
                if (!$msg91ApiCall->validateSmsConfig()) {
                    $this->_logger->error('Please Configure all SMS Gateway Fields Configuration.');
                    return;
                }
                $this->_msg91ApiCall->callApiUrl($shipNumber, $message);
            }
        }

        return true;
    }
}