<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\SmsNotification\Observer\Adminhtml;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class Invoicecreate
 * @package Lof\SmsNotification\Observer\Adminhtml
 */
class Invoicecreate implements ObserverInterface
{
    /**
     * @var \Lof\SmsNotification\Model\SendSms
     */
    protected $sendsms;

    /**
     * @var \Lof\SmsNotification\Helper\Data
     */
    protected $_smsNotificationData;

    /**
     * @var msg91ApiCall
     */
    protected $_msg91ApiCall;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    /**
     * Invoicecreate constructor.
     * @param \Lof\SmsNotification\Model\SendSms $sendsms
     * @param \Lof\SmsNotification\Helper\Data $smsNotificationData
     * @param \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Lof\SmsNotification\Model\SendSms $sendsms,
        \Lof\SmsNotification\Helper\Data $smsNotificationData,
        \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_smsNotificationData = $smsNotificationData;
        $this->_msg91ApiCall = $msg91ApiCall;
        $this->_logger = $logger;
        $this->_customerFactory = $customerFactory;
        $this->sendsms = $sendsms;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Http $request */
        $helperData = $this->_smsNotificationData;
        $msg91ApiCall = $this->_msg91ApiCall;
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getData('order');
        $invoice = $observer->getData('invoice');

        $customerid = $order->getCustomerId();
        $customer = $this->_customerFactory->create()->load($customerid);

        $storeName = $order->getStore()->getName();
        $orderCretaedAt = date("F j, Y", strtotime($order->getCreatedAt()));
        $orderTotal = number_format($order->getGrandTotal(), 2, '.', '');
        $orderEmail = $order->getCustomerEmail();
        $orderId = $order->getIncrementId();
        $orderOldStatus = $order->getStatus();
        $orderNewStatus = "Processing";
        if ($order->getShippingAddress()) {
            $mobilenumber = $order->getShippingAddress()->getTelephone();
        } else {
            $mobilenumber = $customer->getMobilenumber();
        }
        $firstName = $order->getCustomerFirstname();
        $lastName = $order->getCustomerLastname();
        $send_to = 'customer';

        if ($helperData->getConfig('sms_customer/enable_new_invoice')) {
            $smsType = $helperData->getConfig('sms_settings/type_sms');
            $message = $helperData->getInvoiceMessageForUser(
                $orderCretaedAt, $orderTotal, $orderId, $orderOldStatus, $orderNewStatus, $storeName, $firstName,
                $lastName);
            if ($smsType === 'bulksms') {
                $temp = $this->sendsms->send($mobilenumber, $message, $send_to);
            }
            if ($smsType === 'msg91') {
                if (!$msg91ApiCall->validateSmsConfig()) {
                    $this->_logger->error('Please Configure all SMS Gateway Fields Configuration.');
                    return;
                }
                $this->_msg91ApiCall->callApiUrl($mobilenumber, $message);
            }
        }

        return true;
    }
}