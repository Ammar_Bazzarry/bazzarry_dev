<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\SmsNotification\Observer;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class Contactpost
 * @package Lof\SmsNotification\Observer
 */
class Contactpost implements ObserverInterface
{
    /**
     * @var
     */
    protected $_modelOtpFactory;

    /**
     * @var \Lof\SmsNotification\Model\SendSms
     */
    protected $sendsms;

    /**
     * @var \Lof\SmsNotification\Helper\Data
     */
    protected $_smsNotificationData;

    /**
     * @var msg91ApiCall
     */
    protected $_msg91ApiCall;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * Contactpost constructor.
     * @param \Lof\SmsNotification\Model\SendSms $sendsms
     * @param \Lof\SmsNotification\Helper\Data $smsNotificationData
     * @param \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Lof\SmsNotification\Model\SendSms $sendsms,
        \Lof\SmsNotification\Helper\Data $smsNotificationData,
        \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_smsNotificationData = $smsNotificationData;
        $this->_msg91ApiCall = $msg91ApiCall;
        $this->_logger = $logger;
        $this->sendsms = $sendsms;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Http $request */
        $helperData = $this->_smsNotificationData;
        $msg91ApiCall = $this->_msg91ApiCall;
        if ($helperData->getConfig('sms_admin/enable_sms_contact_us')) {

            $request = $observer->getRequest();
            $telephone = $request->getParam('telephone');
            $name = $request->getParam('name');
            $email = $request->getParam('email');
            $countrycode = $request->getParam('countrycode');
            $comment = $request->getParam('comment');
            $tempMobile = $countrycode . "" . $telephone;
            $send_to = 'admin';
            $adminMobile = $helperData->getConfig('sms_settings/phone');
            // Customize Template for sending sms
            $smsType = $helperData->getConfig('sms_settings/type_sms');
            $message = $helperData->getContactFormMessageForAdmin($name, $email, $tempMobile, $comment);
            if ($smsType === 'bulksms') {
                // sent SMS for contact us message
                $temp = $this->sendsms->send($adminMobile, $message, $send_to);
            }
            if ($smsType === 'msg91') {
                if (!$msg91ApiCall->validateSmsConfig()) {
                    $this->_logger->error('Please Configure all SMS Gateway Fields Configuration.');
                    return;
                }
                $this->_msg91ApiCall->callApiUrl($adminMobile, $message);
            }
        }
        return true;
    }
}