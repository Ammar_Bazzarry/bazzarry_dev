<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */


namespace Lof\SmsNotification\Observer;

use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Event\ObserverInterface;

//use Lof\SmsNotification\Model\OtpFactory;

/**
 * Class SaveMobileNumber
 * @package Lof\SmsNotification\Observer
 */
class SaveMobileNumber implements ObserverInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Customer\Model\Data\Customer
     */
    protected $customerData;

    /**
     * @var
     */
    protected $repository;

    /**
     * @var
     */
    protected $customerAddressFactory;

    /**
     * @var
     */
    protected $dataObjectProcessor;

    /**
     * @var \Lof\SmsNotification\Model\SendSms
     */
    protected $sendsms;

    /**
     * @var
     */
    protected $customerRepository;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * SaveMobileNumber constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\Data\Customer $customerData
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Lof\SmsNotification\Model\SendSms $sendsms
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Data\Customer $customerData,
        \Magento\Framework\App\RequestInterface $request,
        \Lof\SmsNotification\Model\SendSms $sendsms
    )
    {
        $this->objectManager = $objectManager;
        $this->customerFactory = $customerFactory;
        $this->customerData = $customerData;
        $this->request = $request;
        $this->sendsms = $sendsms;

    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $email = $observer->getData('email');
        $customer = $this->getCustomerRepository()->get($email);
        $mobileNumber = $this->request->getParam('mobilenumber');
        $customer->setCustomAttribute('mobilenumber', $mobileNumber);
        return true;
    }

    /**
     * Get customer repository
     *
     * @return \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private function getCustomerRepository()
    {

        if (!($this->customerRepository instanceof \Magento\Customer\Api\CustomerRepositoryInterface)) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Customer\Api\CustomerRepositoryInterface::class
            );
        } else {
            return $this->customerRepository;
        }
    }
}