<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\SmsNotification\Observer;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class Orderplaceafter
 * @package Lof\SmsNotification\Observer
 */
class Orderplaceafter implements ObserverInterface
{
    /**
     * @var \Lof\SmsNotification\Model\SendSms
     */
    protected $sendsms;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var \Lof\SmsNotification\Helper\Data
     */
    protected $_smsNotificationData;

    /**
     * @var \Lof\SmsNotification\Helper\msg91ApiCall
     */
    protected $_msg91ApiCall;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * Orderplaceafter constructor.
     * @param \Lof\SmsNotification\Model\SendSms $sendsms
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Lof\SmsNotification\Helper\Data $smsNotificationData
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall
     */
    public function __construct(
        \Lof\SmsNotification\Model\SendSms $sendsms,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Lof\SmsNotification\Helper\Data $smsNotificationData,
        \Psr\Log\LoggerInterface $logger,
        \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall
    ) {
        $this->sendsms = $sendsms;
        $this->_orderFactory = $orderFactory;
        $this->_customerFactory = $customerFactory;
        $this->_smsNotificationData = $smsNotificationData;
        $this->_logger = $logger;
        $this->_msg91ApiCall = $msg91ApiCall;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Http $request */

        $order_id = $observer->getData('order_ids');
        $order = $this->_orderFactory->create()->load($order_id[0]);
        $customerid = $order->getCustomerId();
        $customer = $this->_customerFactory->create()->load($customerid);
        $helperData = $this->_smsNotificationData;
        $msg91ApiCall = $this->_msg91ApiCall;

        $order_information = $order->loadByIncrementId($order_id[0]);
        $orderIncrementId = $order_information->getIncrementId();
        $orderTotal = $order_information->getGrandTotal();
        $orderCustomerFirstName = $order_information->getCustomerFirstname();
        $orderCustomerLastName = $order_information->getCustomerLastname();
        $orderCustomerEmail = $order_information->getCustomerEmail();
        $storeName = $helperData->getStoreName();
        $smsType = $helperData->getConfig('sms_settings/type_sms');
        $shipNumber = $customer->getMobilenumber();
        if($order_information->getShippingAddress()){
            $shipNumber = $order_information->getShippingAddress()->getTelephone();
        }
        if ($helperData->getConfig('sms_customer/enable_sms_new_order')) {
            $message = $helperData->getOrderPlaceMessageForUser($orderIncrementId,
                $shipNumber,
                $orderTotal,
                $orderCustomerFirstName,
                $orderCustomerLastName,
                $orderCustomerEmail,
                $storeName);
            if ($smsType === 'bulksms') {
                $send_to = 'customer';
                $temp = $this->sendsms->send($shipNumber, $message, $send_to);
            }
            if ($smsType === 'msg91') {
                if (!$msg91ApiCall->validateSmsConfig()) {
                    $this->_logger->error('Please Configure all SMS Gateway Fields Configuration.');
                    return;
                }
                $msg91ApiCall->callApiUrl($shipNumber, $message);
            }
        }
        if ($helperData->getConfig('sms_admin/enable_sms_new_order')) {
            $adminMobile = $helperData->getConfig('sms_settings/phone');
            $message = $helperData->getOrderPlaceMessageForAdmin($orderIncrementId,
                $shipNumber,
                $orderTotal,
                $orderCustomerFirstName,
                $orderCustomerLastName,
                $orderCustomerEmail,
                $storeName);
            if ($smsType === 'bulksms') {
                $send_to = 'admin';
                $temp = $this->sendsms->send($adminMobile, $message, $send_to);
            }
            if ($smsType === 'msg91') {
                if (!$msg91ApiCall->validateSmsConfig()) {
                    $this->_logger->error('Please Configure all SMS Gateway Fields Configuration.');
                    return;
                }
                $msg91ApiCall->callApiUrl($adminMobile, $message);
            }
        }
        return true;
    }
}