<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\SmsNotification\Observer;

use Lof\SmsNotification\Helper\msg91ApiCall;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Data\Customer;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

//use Lof\SmsNotification\Model\OtpFactory;

/**
 * Class CustomerRegister
 * @package Lof\SmsNotification\Observer
 */
class CustomerRegister implements ObserverInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var Customer
     */
    protected $customerData;

    /**
     * @var
     */
    protected $repository;

    /**
     * @var
     */
    protected $customerAddressFactory;

    /**
     * @var
     */
    protected $dataObjectProcessor;

    /**
     * @var \Lof\SmsNotification\Model\SendSms
     */
    protected $sendsms;

    /**
     * @var \Lof\SmsNotification\Helper\Data
     */
    protected $_smsNotificationData;

    /**
     * @var msg91ApiCall
     */
    protected $_msg91ApiCall;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * CustomerRegister constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param CustomerFactory $customerFactory
     * @param Customer $customerData
     * @param \Lof\SmsNotification\Helper\Data $smsNotificationData
     * @param msg91ApiCall $msg91ApiCall
     * @param LoggerInterface $logger
     * @param \Lof\SmsNotification\Model\SendSms $sendsms
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Data\Customer $customerData,
        \Lof\SmsNotification\Helper\Data $smsNotificationData,
        \Lof\SmsNotification\Helper\msg91ApiCall $msg91ApiCall,
        \Psr\Log\LoggerInterface $logger,
        \Lof\SmsNotification\Model\SendSms $sendsms
    ) {
        $this->objectManager = $objectManager;
        $this->customerFactory = $customerFactory;
        $this->customerData = $customerData;
        $this->_smsNotificationData = $smsNotificationData;
        $this->_msg91ApiCall = $msg91ApiCall;
        $this->_logger = $logger;
        $this->sendsms = $sendsms;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $helperData = $this->_smsNotificationData;
        $msg91ApiCall = $this->_msg91ApiCall;
        /** @var Http $request */
        $objectManagerInstance = \Magento\Framework\App\ObjectManager::getInstance();
        $request = $observer->getRequest();
        $accountController = $observer->getData('account_controller');
        $customer = $observer->getData('customer');
        $mobilenumber = $accountController->getRequest()->getParam('mobilenumber');
        $firstname = $customer->getFirstName();
        $lastname = $customer->getLastName();
        $email = $customer->getEmail();
        $storeName = $helperData->getStoreName();
        $storeUrl = $helperData->getStoreUrl();
        //$otpString = $helperData->generateRandomString();

        $smsType = $helperData->getConfig('sms_settings/type_sms');

        if ($helperData->getConfig('sms_customer/enable_sms_register_customer')) {
            $message = $helperData->getSignupMessageForUser($firstname, $lastname, $email, $storeName, $mobilenumber,
                $storeUrl);
            if ($smsType === 'bulksms') {
                $send_to = 'customer';
                //call api for send SMS from Helper Data
                $temp = $this->sendsms->send($mobilenumber, $message, $send_to);
            }
            if ($smsType === 'msg91') {
                if (!$msg91ApiCall->validateSmsConfig()) {
                    $this->_logger->error('Please Configure all SMS Gateway Fields Configuration.');
                    return;
                }
                $this->_msg91ApiCall->callApiUrl($mobilenumber, $message);
            }
        }
        if ($helperData->getConfig('sms_admin/enable_sms_register_customer')) {
            $adminMobile = $helperData->getConfig('sms_settings/phone');
            $message = $helperData->getSignupMessageForAdmin($firstname, $lastname, $email, $storeName, $mobilenumber,
                $storeUrl);
            if ($smsType === 'bulksms') {
                $send_to = 'admin';

                //call api for send SMS from Helper Data
                $temp = $this->sendsms->send($adminMobile, $message, $send_to);
            }
            if ($smsType === 'msg91') {
                if (!$msg91ApiCall->validateSmsConfig()) {
                    $this->_logger->error('Please Configure all SMS Gateway Fields Configuration.');
                    return;
                }
                $msg91ApiCall->callApiUrl($adminMobile, $message);
            }
        }

        return true;
    }
}