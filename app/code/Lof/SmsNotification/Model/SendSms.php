<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 **/

namespace Lof\SmsNotification\Model;

/**
 * Class SendSms
 * @package Lof\SmsNotification\Model
 */
class SendSms
{
    /**
     * @var \Lof\SmsNotification\Helper\Data
     */
    protected $helper;

    /**
     * @var \Lof\SmsNotification\Model\Smslog
     */
    protected $smslog;

    /**
     * @var \Lof\SmsNotification\Model\Smsdebug
     */
    protected $smsdebug;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * SendSms constructor.
     * @param Smslog $smslog
     * @param Smsdebug $smsdebug
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Lof\SmsNotification\Helper\Data $helper
     */
    public function __construct(
        \Lof\SmsNotification\Model\Smslog $smslog,
        \Lof\SmsNotification\Model\Smsdebug $smsdebug,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Lof\SmsNotification\Helper\Data $helper
    ) {
        $this->helper = $helper;
        $this->smslog = $smslog;
        $this->smsdebug = $smsdebug;
        $this->date = $date;

    }

    /**
     * @param $mobile
     * @param $message
     * @param $send_to
     * @return $this
     * @throws \Exception
     */
    public function send($mobile, $message, $send_to)
    {
        $type_sms = $this->helper->getConfig('sms_settings/type_sms');
        $phone = $mobile;
        $phone_from = $this->helper->getConfig('sms_settings/phone');
        $sms = $this->smslog;
        $smsdebug = $this->smsdebug;
        $data['from'] = $phone_from;
        $data['to'] = $phone;
        $data['created_at'] = $this->date->gmtDate();
        $smsdebug->setData($data);
        $data['message'] = $message;
        $data['send_to'] = $send_to;
        $sms->setData($data);
        if ($sms->isBlacklist($phone)) {
            $sms->setStatus('Blacklist');
            $sms->save();
            $smsdebug->setMessage('Phone numbers have been blacklisted');
            $smsdebug->save();

        } else {
            if ($type_sms == 'bulksms') {

                $username = $this->helper->getConfig('sms_settings/username');
                $password = $this->helper->getConfig('sms_settings/password');

                if ($phone) {
                    $status = $this->helper->send_smslog($username, $password, $message, $phone);
                    $sms->setStatus($status);
                    $sms->save();
                    $smsdebug->setMessage($status);
                    $smsdebug->save();
                }

            }
        }
        return $this;
    }

    /**
     * @param $data
     */
    public function previewSms($data)
    {
        // do nothing
    }
}