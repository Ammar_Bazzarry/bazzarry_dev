<?php
/**
 * Copyright (c) 2019
 * Tigren Solution
 * All rights reserved
 */

namespace Swissup\Highlight\Controller\View;

/**
 * Class Bigsale
 * @package Swissup\Highlight\Controller\View
 */
class Categories extends \Magento\Framework\App\Action\Action
{
    /**
     * Show products
     *
     * @return void
     */
    public function execute()
    {
        return $this->_objectManager
            ->get('Swissup\Highlight\Helper\Page')
            ->preparePage(
                $this,
                \Swissup\Highlight\Block\ProductList\Categories::PAGE_TYPE
            );
    }
}
