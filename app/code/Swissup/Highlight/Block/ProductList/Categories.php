<?php
/**
 * Copyright (c) 2019
 * Tigren Solution
 * All rights reserved
 */

namespace Swissup\Highlight\Block\ProductList;

/**
 * Class Categories
 * @package Swissup\Highlight\Block\ProductList
 */
class Categories extends All
{
    const PAGE_TYPE = 'categories';

    /**
     * @var string
     */
    protected $widgetPageVarName = 'hsp';

    /**
     * @var string
     */
    protected $widgetPriceSuffix = 'categories';

    /**
     * @var string
     */
    protected $widgetCssClass = 'highlight-categories';
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
        , \Magento\Framework\Url\Helper\Data $urlHelper,
        \Swissup\Highlight\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\App\Http\Context $httpContext,
        \Swissup\Highlight\Helper\Page $highlightHelper,
        \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder,
        \Magento\CatalogWidget\Model\Rule $rule,
        \Magento\Widget\Helper\Conditions $conditionsHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Locale\Resolver $resolver,
        array $data = [])
    {
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $productCollectionFactory, $httpContext, $highlightHelper, $sqlBuilder, $rule, $conditionsHelper, $scopeConfig, $resolver, $data);
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     */
    public function prepareProductCollection($collection)
    {
        $category_id = $this->getCategory();
        parent::prepareProductCollection($collection);
            $collection->addFieldToSelect('category_id',$category_id);
    }

    public function getCategory()
    {
        return $category_id = $this->getWidgetOptions();
    }
    /**
     * @return mixed|string
     */
    public function getWidgetOptions()
    {
        $condition = $this->getData('conditions_encoded');
        if ($condition) {
            $conditions = $this->conditionsHelper->decode($condition);
            foreach ($conditions as  $condition) {
                if (!empty($condition['attribute']) && $condition['attribute'] == 'category_ids') {
                    return $condition['value'];
                }
            }
        }
        return '';
    }

    /**
     * @return array
     */
    public function getCacheKeyInfo()
    {
        if (false === $this->getIsWidget()) {
            return parent::getCacheKeyInfo();
        }

        $keyInfo = parent::getCacheKeyInfo();
        $keyInfo['period'] = $this->getPeriod();
        return $keyInfo;
    }

    /**
     * @param \Magento\Catalog\Block\Product\ProductList\Toolbar $toolbar
     */
    protected function initToolbar($toolbar)
    {
        parent::initToolbar($toolbar);
        if ($toolbar->getCurrentOrder() === 'popularity') {
            $toolbar->setSkipOrder(true);
            $this->getProductCollection()->getSelect()->order('popularity DESC');
        }
    }
}
