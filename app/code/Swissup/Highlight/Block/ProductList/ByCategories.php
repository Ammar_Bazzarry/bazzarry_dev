<?php

namespace Swissup\Highlight\Block\ProductList;

/**
 * Class ByCategories
 * @package Swissup\Highlight\Block\ProductList
 */
class ByCategories extends All
{
    const PAGE_TYPE = 'categories';

    /**
     * @var string
     */
    protected $widgetPageVarName = 'hsp';

    /**
     * @var string
     */
    protected $widgetPriceSuffix = 'categories';

    /**
     * @var string
     */
    protected $widgetCssClass = 'highlight-categories';


    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     */
    public function prepareProductCollection($collection)
    {
        parent::prepareProductCollection($collection);
    }

    /**
     * @return mixed|string
     */
    public function getCssClass()
    {
        return $this->hasData('css_class') ? $this->getData('css_class') : '';
    }

    /**
     * @return array
     */
    public function getCacheKeyInfo()
    {
        if (false === $this->getIsWidget()) {
            return parent::getCacheKeyInfo();
        }

        $keyInfo = parent::getCacheKeyInfo();
        $keyInfo['period'] = $this->getPeriod();
        return $keyInfo;
    }

    /**
     * @param \Magento\Catalog\Block\Product\ProductList\Toolbar $toolbar
     */
    protected function initToolbar($toolbar)
    {
        parent::initToolbar($toolbar);
        if ($toolbar->getCurrentOrder() === 'popularity') {
            $toolbar->setSkipOrder(true);
            $this->getProductCollection()->getSelect()->order('popularity DESC');
        }
    }
}
