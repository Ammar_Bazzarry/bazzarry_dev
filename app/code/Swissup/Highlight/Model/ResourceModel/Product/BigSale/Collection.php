<?php

namespace Swissup\Highlight\Model\ResourceModel\Product\BigSale;
class Collection extends \Magento\Catalog\Model\ResourceModel\Product\Collection
{
    /**
     * Init Select
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->caculateSale();
        return $this;
    }

    protected function caculateSale()
    {
        $collection = $this->addAttributeToSelect('price')
       -> addAttributeToFilter('special_price', array('gt' => 0))
       -> addAttributeToFilter('price', array('gt' => 0));
        $collection->getSelect()->columns(array('discount_different'=>'(`at_price`.`value` - `at_special_price`.`value`)'))->order(array('discount_different DESC'))->group('e.entity_id');
        $collection->setPageSize(10);
       return $collection;
    }
}
