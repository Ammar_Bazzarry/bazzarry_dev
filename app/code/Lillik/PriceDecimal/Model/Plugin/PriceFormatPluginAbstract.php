<?php
/**
 *
 * @package Lillik\PriceDecimal\Model\Plugin
 *
 * @author  Lilian Codreanu <lilian.codreanu@gmail.com>
 */


namespace Lillik\PriceDecimal\Model\Plugin;

use Lillik\PriceDecimal\Model\ConfigInterface;
use Lillik\PriceDecimal\Model\PricePrecisionConfigTrait;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class PriceFormatPluginAbstract
 * @package Lillik\PriceDecimal\Model\Plugin
 */
abstract class PriceFormatPluginAbstract
{
    use PricePrecisionConfigTrait;

    /**
     * @var ConfigInterface
     */
    protected $moduleConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\App\State
     */
    public $appState;

    /**
     * @param ConfigInterface $moduleConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ConfigInterface $moduleConfig,
        \Magento\Framework\App\State $appState,
        StoreManagerInterface $storeManager
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->appState = $appState;
        $this->_storeManager = $storeManager;
    }
}
