<?php
/**
 *
 * @package package Lillik\PriceDecimal\Model\Plugin\Local
 *
 * @author  Lilian Codreanu <lilian.codreanu@gmail.com>
 */

namespace Lillik\PriceDecimal\Model\Plugin\Local;

use Closure;
use Lillik\PriceDecimal\Model\ConfigInterface;
use Lillik\PriceDecimal\Model\Plugin\PriceFormatPluginAbstract;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\App\ScopeResolverInterface;
use Magento\Framework\App\State;
use Magento\Framework\Locale\FormatInterface;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Format
 * @package Lillik\PriceDecimal\Model\Plugin\Local
 */
class Format extends PriceFormatPluginAbstract
{
    /**
     * @var ScopeResolverInterface
     */
    protected $_scopeResolver;

    /**
     * @var ResolverInterface
     */
    protected $_localeResolver;

    /**
     * @var CurrencyFactory
     */
    protected $currencyFactory;

    /**
     * @var ConfigInterface
     */
    protected $moduleConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var State
     */
    public $appState;

    /**
     * @param ConfigInterface $moduleConfig
     * @param State $appState
     * @param StoreManagerInterface $storeManager
     * @param ScopeResolverInterface $scopeResolver
     * @param ResolverInterface $localeResolver
     * @param CurrencyFactory $currencyFactory
     */
    public function __construct(
        ConfigInterface $moduleConfig,
        State $appState,
        StoreManagerInterface $storeManager,
        ScopeResolverInterface $scopeResolver,
        ResolverInterface $localeResolver,
        CurrencyFactory $currencyFactory
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->appState = $appState;
        $this->_storeManager = $storeManager;
        $this->_scopeResolver = $scopeResolver;
        $this->_localeResolver = $localeResolver;
        $this->currencyFactory = $currencyFactory;
    }
    /**
     * @param $subject
     * @param Closure $proceed
     * @param null $localeCode
     * @param null $currencyCode
     * @return mixed
     */
    public function aroundGetPriceFormat(
        FormatInterface $subject,
        Closure $proceed,
        $localeCode = null,
        $currencyCode = null
    ) {
        $result = $proceed($localeCode, $currencyCode);

        $localeCode = $localeCode ?: $this->_localeResolver->getLocale();
        if (!$currencyCode) {
            $currency = $this->_scopeResolver->getScope()->getCurrentCurrency();
            $currencyCode = $currency->getCode();
        }

        if ($currencyCode === 'USD' && $localeCode === 'ar_SA') {
            $result['decimalSymbol'] = '.';
            $result['groupSymbol'] = ',';
        }

        $precision = $this->getPricePrecision();
        if ($this->getConfig()->isEnable()) {
            $result['precision'] = $precision;
            $result['requiredPrecision'] = $precision;
        }

        return $result;
    }
}
