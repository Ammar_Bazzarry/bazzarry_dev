<?php
/**
 *
 * @package Lillik\PriceDecimal
 *
 * @author  Lilian Codreanu <lilian.codreanu@gmail.com>
 */

namespace Lillik\PriceDecimal\Model;

use Magento\Framework\Registry;

/**
 * Trait PricePrecisionConfigTrait
 * @package Lillik\PriceDecimal\Model
 */
trait PricePrecisionConfigTrait
{
    /**
     * @return \Lillik\PriceDecimal\Model\ConfigInterface
     */
    public function getConfig()
    {
        return $this->moduleConfig;
    }

    /**
     * @return int|mixed
     */
    public function getPricePrecision()
    {
        $areaCode = $this->appState->getAreaCode();
        if ($this->getConfig()->canShowPriceDecimal()) {
            $currencyCode = $this->_storeManager->getStore()->getCurrentCurrencyCode();
            if ($areaCode != 'adminhtml' && $areaCode != 'vendors' && $currencyCode === 'YER') {
                return 0;
            }

            return $this->getConfig()->getPricePrecision();
        }

        return 0;
    }
}
