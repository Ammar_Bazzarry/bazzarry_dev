(function(require){
(function() {
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    'waitSeconds': 0,
    'map': {
        '*': {
            'ko': 'knockoutjs/knockout',
            'knockout': 'knockoutjs/knockout',
            'mageUtils': 'mage/utils/main',
            'rjsResolver': 'mage/requirejs/resolver'
        }
    },
    'shim': {
        'jquery/jquery-migrate': ['jquery'],
        'jquery/jquery.hashchange': ['jquery', 'jquery/jquery-migrate'],
        'jquery/jstree/jquery.hotkeys': ['jquery'],
        'jquery/hover-intent': ['jquery'],
        'mage/adminhtml/backup': ['prototype'],
        'mage/captcha': ['prototype'],
        'mage/common': ['jquery'],
        'mage/new-gallery': ['jquery'],
        'mage/webapi': ['jquery'],
        'jquery/ui': ['jquery'],
        'MutationObserver': ['es6-collections'],
        'tinymce': {
            'exports': 'tinymce'
        },
        'moment': {
            'exports': 'moment'
        },
        'matchMedia': {
            'exports': 'mediaCheck'
        },
        'jquery/jquery-storageapi': {
            'deps': ['jquery/jquery.cookie']
        }
    },
    'paths': {
        'jquery/validate': 'jquery/jquery.validate',
        'jquery/hover-intent': 'jquery/jquery.hoverIntent',
        'jquery/file-uploader': 'jquery/fileUploader/jquery.fileupload-fp',
        'jquery/jquery.hashchange': 'jquery/jquery.ba-hashchange.min',
        'prototype': 'legacy-build.min',
        'jquery/jquery-storageapi': 'jquery/jquery.storageapi.min',
        'text': 'mage/requirejs/text',
        'domReady': 'requirejs/domReady',
        'tinymce': 'tiny_mce/tiny_mce_src'
    },
    'deps': [
        'jquery/jquery-migrate'
    ],
    'config': {
        'mixins': {
            'jquery/jstree/jquery.jstree': {
                'mage/backend/jstree-mixin': true
            },
            'jquery': {
                'jquery/patches/jquery': true
            }
        },
        'text': {
            'headers': {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }
    }
};

require.config(config);
})();
(function() {
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    paths: {
        'ui/template': 'Magento_Ui/templates'
    },
    map: {
        '*': {
            uiElement:      'Magento_Ui/js/lib/core/element/element',
            uiCollection:   'Magento_Ui/js/lib/core/collection',
            uiComponent:    'Magento_Ui/js/lib/core/collection',
            uiClass:        'Magento_Ui/js/lib/core/class',
            uiEvents:       'Magento_Ui/js/lib/core/events',
            uiRegistry:     'Magento_Ui/js/lib/registry/registry',
            consoleLogger:  'Magento_Ui/js/lib/logger/console-logger',
            uiLayout:       'Magento_Ui/js/core/renderer/layout',
            buttonAdapter:  'Magento_Ui/js/form/button-adapter'
        }
    }
};

require.config(config);
})();
(function() {
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            transparent: 'Magento_Payment/js/transparent',
            'Magento_Payment/transparent': 'Magento_Payment/js/transparent'
        }
    }
};

require.config(config);
})();
(function() {
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Core
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

var config = {
    paths: {
        'mageplaza/core/jquery/popup': 'Mageplaza_Core/js/jquery.magnific-popup.min',
        'mageplaza/core/owl.carousel': 'Mageplaza_Core/js/owl.carousel.min',
        'mageplaza/core/bootstrap': 'Mageplaza_Core/js/bootstrap.min',
        mpIonRangeSlider: 'Mageplaza_Core/js/ion.rangeSlider.min',
        touchPunch: 'Mageplaza_Core/js/jquery.ui.touch-punch.min',
        mpDevbridgeAutocomplete: 'Mageplaza_Core/js/jquery.autocomplete.min'
    },
    shim: {
        "mageplaza/core/jquery/popup": ["jquery"],
        "mageplaza/core/owl.carousel": ["jquery"],
        "mageplaza/core/bootstrap": ["jquery"],
        mpIonRangeSlider: ["jquery"],
        mpDevbridgeAutocomplete: ["jquery"],
        touchPunch: ['jquery', 'jquery/ui']
    }
};

require.config(config);
})();
(function() {
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    "map": {
        '*': {
            "fastclick":            'Vnecoms_Vendors/js/fastclick',
            "translateInline":      "mage/translate-inline",
            "form":                 "mage/backend/form",
            "button":               "mage/backend/button",
            "accordion":            "mage/accordion",
            "actionLink":           "mage/backend/action-link",
            "validation":           "mage/backend/validation",
            "notification":         "mage/backend/notification",
            "loader":               "mage/loader_old",
            "loaderAjax":           "mage/loader_old",
            "floatingHeader":       "mage/backend/floating-header",
            "suggest":              "mage/backend/suggest",
            "mediabrowser":         "jquery/jstree/jquery.jstree",
            "tabs":                 "mage/backend/tabs",
            "treeSuggest":          "mage/backend/tree-suggest",
            "calendar":             "mage/calendar",
            "dropdown":             "mage/dropdown_old",
            "collapsible":          "mage/collapsible",
            /*"menu":                 "mage/backend/menu",*/
            "jstree":               "jquery/jstree/jquery.jstree",
            "details":              "jquery/jquery.details"
        }
    },
    "shim": {
        "jquery/bootstrap": ["jquery","jquery/ui"],
        "adminlte": ["jquery","jquery/bootstrap"],
        "jquery/adminlte": ["jquery","jquery/bootstrap"],
        "jquery/slimscroll": ["jquery"],
        "jquery/fix_prototype_bootstrap": ["jquery","jquery/bootstrap","prototype"],
    },
    "deps": [
         "mage/backend/bootstrap",
         "mage/adminhtml/globals",
         "fastclick",
         "jquery/bootstrap",
         "jquery/slimscroll",
         "jquery/adminlte",
         "jquery/fix_prototype_bootstrap"
     ],
    "paths": {
        "jquery/ui": "jquery/jquery-ui-1.9.2",
        "jquery/bootstrap": "Vnecoms_Vendors/js/bootstrap",
        "jquery/slimscroll": 'Vnecoms_Vendors/js/jquery.slimscroll',
        "jquery/adminlte": "Vnecoms_Vendors/js/adminlte",
        "jquery/fix_prototype_bootstrap": "Vnecoms_Vendors/js/fix_prototype_bootstrap"
    }
};

require.config(config);
})();
(function() {
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    "shim": {
        "jquery/raphael": ["jquery"],
        "jquery/chartjs": ["jquery"],
        "jquery/morris": ["jquery","jquery/raphael"]
    },
    "paths": {
        "jquery/morris": "Vnecoms_VendorsDashboard/plugins/morris/morris.min",
        "jquery/raphael": "Vnecoms_VendorsDashboard/plugins/morris/raphael-min",
        "jquery/chartjs": "Vnecoms_VendorsDashboard/plugins/chartjs/chart_min"
    }
};

require.config(config);
})();
(function() {
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            categoryForm:       'Vnecoms_VendorsProduct/catalog/category/form',
            newCategoryDialog:  'Vnecoms_VendorsProduct/js/new-category-dialog',
            categoryTree:       'Vnecoms_VendorsProduct/js/category-tree',
            productGallery:     'Vnecoms_VendorsProduct/js/product-gallery',
            baseImage:          'Vnecoms_VendorsProduct/catalog/base-image-uploader',
            newVideoDialog:     'Vnecoms_VendorsProduct/js/video/new-video-dialog',
            openVideoModal:     'Vnecoms_VendorsProduct/js/video/video-modal',
            productAttributes:  'Vnecoms_VendorsProduct/catalog/product-attributes',
            menu:               'mage/backend/menu'
        }
    },
    "shim": {
        "productGallery": ["jquery/fix_prototype_bootstrap"],
        "Vnecoms_VendorsProduct/catalog/apply-to-type-switcher": ["Vnecoms_VendorsProduct/catalog/type-events"]
    },
    "paths": {
        "Magento_Catalog/catalog/type-events": "Vnecoms_VendorsProduct/catalog/type-events",
        "Magento_Catalog/catalog/apply-to-type-switcher": "Vnecoms_VendorsProduct/catalog/apply-to-type-switcher",
        "Magento_Catalog/js/product/weight-handler":"Vnecoms_VendorsProduct/js/product/weight-handler",
        "Magento_Catalog/js/product-gallery":"Vnecoms_VendorsProduct/js/product-gallery",
        "Magento_ProductVideo/js/get-video-information":"Vnecoms_VendorsProduct/js/video/get-video-information",
        "Magento_Catalog/js/tier-price/percentage-processor":"Vnecoms_VendorsProduct/js/tier-price/percentage-processor",
        "Magento_Catalog/js/tier-price/value-type-select":"Vnecoms_VendorsProduct/js/tier-price/value-type-select",
        "Magento_Catalog/js/utils/percentage-price-calculator":"Vnecoms_VendorsProduct/js/utils/percentage-price-calculator",
        "Magento_Catalog/js/components/custom-options-component":"Vnecoms_VendorsProduct/js/components/custom-options-component",
        "Magento_Catalog/js/components/custom-options-price-type":"Vnecoms_VendorsProduct/js/components/custom-options-price-type",
        "Magento_Catalog/js/components/dynamic-rows-import-custom-options":"Vnecoms_VendorsProduct/js/components/dynamic-rows-import-custom-options"
    }
};

require.config(config);
})();
(function() {
/*
 * Copyright © 2017 Vnecoms. All rights reserved.
 */
var config = {
    map: {
        "*": {
            "vendorsCmsTinyMCESetup" : "Vnecoms_VendorsCms/js/lib/wysiwyg/tiny_mce/setup",
            "vendorsCmsWidget" : "Vnecoms_VendorsCms/js/lib/wysiwyg/widget",
            "vendorsCmsbrowser" : "Vnecoms_VendorsCms/js/browser",
            "appInstance" : "Vnecoms_VendorsCms/js/app/app",
            "insertBlock": "Vnecoms_VendorsCms/js/insert_block",
            "cmsVariables": "Vnecoms_VendorsCms/js/variables"
            /*"Magento_Variable/js/variables": "Vnecoms_VendorsProduct/js/components/variables"*/
        }
    },
    "shim": {
        "extjs/ext-tree-checkbox":["extjs/ext-tree"],
        "extjs/ext-tree":["prototype"],
    },
    "paths": {
        "CmsfolderTree": "Vnecoms_VendorsCms/js/folder-tree",
        // "mage/adminhtml/browser": "Vnecoms_VendorsCms/js/browser",
        // "mage/adminhtml/wysiwyg/tiny_mce/setup": "Vnecoms_VendorsCms/js/lib/wysiwyg/tiny_mce/setup"
    },
    "deps": [
    ]
};

require.config(config);
})();
(function() {
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
          //  "categoryForm": "Vnecoms_VendorsCategory/category/form",
            "vesLoadingPopup": "Vnecoms_VendorsCategory/js/loadingPopup",
            "Magento_Variable/js/variables": "Vnecoms_VendorsProduct/js/components/variables"
        }
    },
    "shim": {
        "extjs/ext-tree-checkbox":["extjs/ext-tree"],
        "extjs/ext-tree":["prototype"],
    }
};


require.config(config);
})();
(function() {
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            messageLoader: 'Vnecoms_VendorsMessage/js/message-loader'
        }
    }
};

require.config(config);
})();
(function() {
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*eslint no-unused-vars: 0*/
var config = {
    "shim": {
        "bannerGallery": {
            "deps": ["baseImage"]
        }
    },
    "map": {
          '*': {
              "bannerGallery": "Vnecoms_BannerManager/js/banner-gallery",
             /* "productGallery":  "Vnecoms_VendorsProduct/js/product-gallery.js.bk",
              "baseImage":       "Vnecoms_VendorsProduct/catalog/base-image-uploader.js.bk"*/
          }
    }
};

require.config(config);
})();
(function() {
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    "map": {
        '*': {
            "Magento_ConfigurableProduct/template/components/cell-sku.html":'Vnecoms_VendorsProductConfigurable/template/components/cell-sku.html',
            "Magento_ConfigurableProduct/template/components/cell-status.html":'Vnecoms_VendorsProductConfigurable/template/components/cell-status.html',
            "Magento_ConfigurableProduct/template/components/cell-html.html":"Vnecoms_VendorsProductConfigurable/template/components/cell-html.html",
            "Magento_ConfigurableProduct/template/components/actions-list.html":"Vnecoms_VendorsProductConfigurable/template/components/actions-list.html",
            "Magento_ConfigurableProduct/template/variations/steps/summary-grid.html":"Vnecoms_VendorsProductConfigurable/template/variations/steps/summary-grid.html",
            "Magento_ConfigurableProduct/template/components/file-uploader.html":"Vnecoms_VendorsProductConfigurable/template/components/file-uploader.html"
        }
    },
    "paths": {
    	/* Magento 2.2*/
		"Magento_ConfigurableProduct/js/components/price-configurable":"Vnecoms_VendorsProductConfigurable/js/components/price-configurable",
		"Magento_ConfigurableProduct/js/variations/paging/sizes":"Vnecoms_VendorsProductConfigurable/js/variations/paging/sizes",
		/* End Magento 2.2*/
        "Magento_ConfigurableProduct/js/components/modal-configurable": "Vnecoms_VendorsProductConfigurable/js/components/modal-configurable",
        "Magento_ConfigurableProduct/js/variations/variations": "Vnecoms_VendorsProductConfigurable/js/variations/variations",
        "Magento_ConfigurableProduct/js/components/custom-options-warning":"Vnecoms_VendorsProductConfigurable/js/components/custom-options-warning",
        "Magento_ConfigurableProduct/js/components/associated-product-insert-listing":"Vnecoms_VendorsProductConfigurable/js/components/associated-product-insert-listing",
        "Magento_ConfigurableProduct/js/components/container-configurable-handler":"Vnecoms_VendorsProductConfigurable/js/components/container-configurable-handler",
        "Magento_ConfigurableProduct/js/components/dynamic-rows-configurable":"Vnecoms_VendorsProductConfigurable/js/components/dynamic-rows-configurable",
        "Magento_ConfigurableProduct/js/variations/steps/select_attributes":"Vnecoms_VendorsProductConfigurable/js/variations/steps/select_attributes",
        "Magento_ConfigurableProduct/js/variations/steps/bulk":"Vnecoms_VendorsProductConfigurable/js/variations/steps/bulk",
        "Magento_ConfigurableProduct/js/variations/steps/attributes_values":"Vnecoms_VendorsProductConfigurable/js/variations/steps/attributes_values",
        "Magento_ConfigurableProduct/js/variations/steps/summary":"Vnecoms_VendorsProductConfigurable/js/variations/steps/summary",
        "Magento_ConfigurableProduct/js/components/custom-options-price-type":"Vnecoms_VendorsProductConfigurable/js/components/custom-options-price-type",
        "Magento_ConfigurableProduct/js/components/file-uploader":"Vnecoms_VendorsProductConfigurable/js/components/file-uploader",
        "Magento_ConfigurableProduct/js/configurable":"Vnecoms_VendorsProductConfigurable/js/configurable"
    }
};

require.config(config);
})();
(function() {
var config = {
    "paths": {
        "Magento_Catalog/js/bundle-proxy-button": "Vnecoms_VendorsProductBundle/js/bundle-proxy-button",
        "Magento_Bundle/js/components/bundle-dynamic-rows": "Vnecoms_VendorsProductBundle/js/components/bundle-dynamic-rows",
        "Magento_Bundle/js/components/bundle-dynamic-rows-grid": "Vnecoms_VendorsProductBundle/js/components/bundle-dynamic-rows-grid",
        "Magento_Bundle/js/components/bundle-input-type": "Vnecoms_VendorsProductBundle/js/components/bundle-input-type",
        "Magento_Bundle/js/components/bundle-option-qty": "Vnecoms_VendorsProductBundle/js/components/bundle-option-qty",
        "Magento_Bundle/js/components/bundle-record": "Vnecoms_VendorsProductBundle/js/components/bundle-record",
        "Magento_Bundle/js/components/bundle-checkbox": "Vnecoms_VendorsProductBundle/js/components/bundle-checkbox",
        "Magento_Bundle/js/components/bundle-user-defined-checkbox": "Vnecoms_VendorsProductBundle/js/components/bundle-user-defined-checkbox"
    }
};

require.config(config);
})();
(function() {
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    "map": {
        '*': {
            "Magento_Downloadable/template/components/file-uploader":'Vnecoms_VendorsProductDownloadable/template/components/file-uploader',
        }
    },
    "paths": {
        "Magento_Downloadable/js/components/downloadable-type-handler": "Vnecoms_VendorsProductDownloadable/js/components/downloadable-type-handler",
        "Magento_Downloadable/js/components/is-downloadable-handler": "Vnecoms_VendorsProductDownloadable/js/components/is-downloadable-handler",
        "Magento_Downloadable/js/components/file-uploader": "Vnecoms_VendorsProductDownloadable/js/components/file-uploader",
        "Magento_Downloadable/js/components/price-handler": "Vnecoms_VendorsProductDownloadable/js/components/price-handler",
        "Magento_Downloadable/js/components/upload-type-handler": "Vnecoms_VendorsProductDownloadable/js/components/upload-type-handler",
        "Magento_Downloadable/js/components/use-price-default-handler": "Vnecoms_VendorsProductDownloadable/js/components/use-price-default-handler"
    }
};

require.config(config);
})();
(function() {
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    "paths": {
        "Magento_GroupedProduct/js/grouped-product": "Vnecoms_VendorsProductGrouped/js/grouped-product",
    }
};

require.config(config);
})();
(function() {
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    "shim": {
        "Vnecoms_VendorsProductImportExport/js/jquery.fileuploader": ["jquery"],
    }
};

require.config(config);
})();



})(require);