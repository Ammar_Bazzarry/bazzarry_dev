/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Ui/js/form/components/insert-form',
    'mageUtils',
    'jquery'
], function (Insert, utils, $) {
    'use strict';


    return Insert.extend({
        /**
         * Reset external form data.
         */
        resetForm: function (attribute) {
            if (this.externalSource()) {
                this.externalSource().trigger('data.reset');
                this.responseStatus(undefined);
                this.externalSource().set('data.attribute', attribute);
            }
        }
    });
});
