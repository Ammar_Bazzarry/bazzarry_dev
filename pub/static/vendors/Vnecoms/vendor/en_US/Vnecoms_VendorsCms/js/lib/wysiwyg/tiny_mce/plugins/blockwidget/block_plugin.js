/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

tinyMCE.addI18n({en:{
    blockwidget:
    {
        insert_block : "Insert Block"
    }
}});

(function () {
    tinymce.create('tinymce.plugins.VendorsCmsblockPlugin', {
        /**
         * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
         * @param {string} url Absolute URL to where the plugin is located.
         */
        init : function (ed, url) {
            ed.addCommand('mceCmsBlock', function () {
                var pluginSettings = ed.settings.magentoPluginsOptions.get('blockwidget');
                VendorsCmsblockPlugin.setEditor(ed);
                VendorsCmsblockPlugin.loadChooser(pluginSettings.url, null);
            });

            // Register Widget plugin button
            ed.addButton('blockwidget', {
                title : 'blockwidget.insert_block',
                cmd : 'mceCmsBlock',
                image : url + '/img/grid.jpg'
            });
        },

        getInfo : function () {
            return {
                longname : 'Vendors Insert Block Widget Manager Plugin for TinyMCE 3.x',
                author : 'Ves Team',
                authorurl : 'http://www.vnecoms.com',
                infourl : 'http://www.vnecoms.com',
                version : "1.0"
            };
        }
    });

    // Register plugin
    tinymce.PluginManager.add('blockwidget', tinymce.plugins.VendorsCmsblockPlugin);
})();
