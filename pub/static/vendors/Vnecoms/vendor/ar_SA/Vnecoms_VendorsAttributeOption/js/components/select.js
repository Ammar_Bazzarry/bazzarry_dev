/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'underscore',
    'mageUtils',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'uiLayout'
], function (_, utils, registry, Select, layout) {
    'use strict';

    return Select.extend({
        /**
         * Parse data and set it to options.
         *
         * @param {Object} data - Response data object.
         * @returns {Object}
         */
        setParsed: function (data) {
            if (this.code != data.attribute) {
return; }
            if (!data.is_approved_seller) {
return; }
            
            var option = this.parseData(data);
            var options = this.options();
            options.push(option);
            this.setOptions(options);
            this.set('newOption', option);
            
            this.value(option.value);
        },

        /**
         * Normalize option object.
         *
         * @param {Object} data - Option object.
         * @returns {Object}
         */
        parseData: function (data) {
            return {
                value: data.option.option_id,
                label: data.option.label,
                labeltitle: data.option.label
            };
        }
    });
});
