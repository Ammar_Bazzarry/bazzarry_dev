define([
    'jquery',
    'magnificPopup',
    'Magento_Customer/js/customer-data'
], function ($, magnificPopup, customerData) {
    "use strict";

    return {
        displayContent: function (prodUrl) {
            if (!prodUrl.length) {
                return false;
            }

            $.magnificPopup.open({
                items: {
                    src: prodUrl
                },
                type: 'iframe',
                closeOnBgClick: false,
                preloader: true,
                tLoading: '',
                callbacks: {
                    open: function () {
                        $('.mfp-preloader').css('display', 'block');
                    },
                    beforeClose: function () {
                        customerData.reload(['cart'], false);
                    },
                    close: function () {
                        $('.mfp-preloader').css('display', 'none');
                    }
                }
            });
        }
    };
});