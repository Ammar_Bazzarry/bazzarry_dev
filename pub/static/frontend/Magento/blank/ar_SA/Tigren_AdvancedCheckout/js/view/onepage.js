/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

/* @api */
define([
    'uiComponent',
    'Magento_Customer/js/model/customer',
    'ko'
], function (Component,customer,ko) {
    'use strict';

    var checkoutConfig = window.checkoutConfig;

    return Component.extend({
        defaults: {
            template: 'Tigren_AdvancedCheckout/onepage'
        },

        allowGuestCheckout: ko.observable(checkoutConfig.isGuestCheckoutAllowed),
        isLoggin: ko.observable(customer.isLoggedIn())

    });
});
