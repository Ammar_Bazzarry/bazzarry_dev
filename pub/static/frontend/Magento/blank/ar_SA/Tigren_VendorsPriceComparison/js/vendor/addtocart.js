/*
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */
define([
    'Magento_Ui/js/modal/modal',
    'mage/url',
    'uiComponent',
    'jquery',
    'domReady!'
], function ( modal, urlBuilder, Component, $) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Vnecoms_VendorsPriceComparison/vendor/addtocart'
        },

        initialize: function () {
            this._super();
            this.ajaxSubmitForm();
        },

        getAddToCartUrl: function (product) {
            return product.pc_addtocart_url;
        },

        ajaxSubmitForm: function () {
            var self = this;
            $('body').delegate('button#vendors-to-cart', 'click', function (e) {
                e.stopPropagation();
                e.preventDefault();
                var form = $(this.form);
                var body = $('body');
                var postData = {};
                postData.product = form.find('input[name="product"]').val();
                postData.form_key = $.mage.cookies.get('form_key');
                var url = urlBuilder.build('customtheme/cart/add');
                $.ajax({
                    url: url,
                    data: postData,
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function () {
                        body.trigger('processStart');
                    },
                    success: function (res) {
                        if (res.redirect) {
                            window.location = res.redirect
                        } else {
                            if (res.error) {
                                var popupError = $('<div class="tigren-add-to-cart-popup-error"/>').html(res.message).modal({ //get product name from product view page only
                                    modalClass: 'confirm',
                                    title: false
                                });
                                popupError.modal('openModal');
                                body.trigger('processStop');
                                return false;
                            }
                            if (res.backUrl) {
                                window.location = res.backUrl
                            }
                            var checkoutUrl = urlBuilder.build("checkout/");
                            var popup = $('<div class="tigren-add-to-cart-popup"/>').html(res.message).modal({ //get product name from product view page only
                                modalClass: 'confirm',
                                title: false,
                                buttons: [
                                    {
                                        text: $.mage.__('Continue Shopping'),
                                        class: 'btn btn-primary',
                                        click: function () {
                                            this.closeModal();
                                        }
                                    },
                                    {
                                        text: $.mage.__("Checkout"),
                                        class: 'btn btn-secondary',
                                        click: function () {
                                            window.location = checkoutUrl
                                        }
                                    }
                                ]
                            });
                            popup.modal('openModal');
                            body.trigger('processStop');
                        }
                    },
                    error: function (response) {

                    }
                })

            })
        }
    });
});
