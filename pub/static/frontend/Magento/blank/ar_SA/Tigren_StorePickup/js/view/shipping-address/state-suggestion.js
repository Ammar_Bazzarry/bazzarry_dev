define([
    'jquery',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'mage/translate'
], function (
    $,
    registry,
    Select,
    $t
) {
    'use strict';

    var newRegionOptions = [];
    _.each(window.checkoutConfig.directoryRegions, function (region, key) {
        newRegionOptions.push({
            'label': region.name,
            'value': key
        });
    });
    newRegionOptions.sort(function (a, b) {
        if (a.label.toLowerCase() > b.label.toLowerCase()) {
            return 1
        }
        if (a.label.toLowerCase() < b.label.toLowerCase()) {
            return -1
        }
        return 0;
    });
    newRegionOptions.unshift({
        'label': $t('-- Select City'),
        'value': ''
    });

    return Select.extend({
        defaults: {
            modules: {
                parent: '${ $.parentName }'
            },

            options: newRegionOptions
        }
    });
});