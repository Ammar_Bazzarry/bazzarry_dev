/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote'
], function ($, Component, quote) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/summary/shipping'
        },
        quoteIsVirtual: quote.isVirtual(),
        totals: quote.getTotals(),
        SEPARATOR: '||',
        METHOD_SEPARATOR: '|_|',
        /**
         * @return {*}
         */
        getShippingMethodTitle: function () {
            var self = this;
            var shippingMethod,
                shippingMethodTitle = '';
            var warehouseList = checkoutConfig.warehouse;
            var vendorList = checkoutConfig.vendors_list;
            var mainVendor = window.checkoutConfig.main_vendor;
            if (!this.isCalculated()) {
                return '';
            }
            shippingMethod = quote.shippingMethod();

            if (typeof shippingMethod['method_title'] !== 'undefined' && shippingMethod['method_title'] !== '') {
                shippingMethodTitle = shippingMethod['method_title'];
            }
            if(shippingMethodTitle){
                shippingMethod = shippingMethodTitle.split(self.METHOD_SEPARATOR);
                var methodTitle = "";
                for (var index in shippingMethod) {
                    var methodCode = shippingMethod[index];
                    var warehouseMethodTitle = methodCode.toString().split(self.SEPARATOR);
                    var warehouseId = warehouseMethodTitle[1];
                    var vendorId = warehouseMethodTitle[2];
                    if(vendorId == mainVendor) {
                        if (warehouseList['warehouse_' + warehouseId] != undefined) {
                            var warehouseTitle = warehouseList['warehouse_' + warehouseId];
                            warehouseTitle = warehouseTitle.shipping_title;
                            warehouseMethodTitle = "<label class='warehouse-title'>" + warehouseTitle + "</label> : <span class='warehouse-method'>" + warehouseMethodTitle[0] + "</span>";
                            methodTitle += warehouseMethodTitle + "<br />";
                        }
                    }else{
                        if (vendorList[vendorId] != undefined) {
                            var vendorTitle = vendorList[vendorId];
                            vendorTitle = vendorTitle.shipping_title;
                            warehouseMethodTitle = "<label class='vendor-title'>" + vendorTitle + "</label> : <span class='vendor-method'>" + warehouseMethodTitle[0] + "</span>";
                            methodTitle += warehouseMethodTitle + "<br />";
                        }
                    }
                }
                methodTitle = methodTitle.trim();
                methodTitle = methodTitle.slice(0, -6);
                return methodTitle;
            }
            return '';
        },

        /**
         * @return {*|Boolean}
         */
        isCalculated: function () {
            return this.totals() && this.isFullMode() && quote.shippingMethod() != null; //eslint-disable-line eqeqeq
        },

        /**
         * @return {*}
         */
        getValue: function () {
            var price;

            if (!this.isCalculated()) {
                return this.notCalculatedMessage;
            }
            price =  this.totals()['shipping_amount'];

            return this.getFormattedPrice(price);
        }
    });
});
