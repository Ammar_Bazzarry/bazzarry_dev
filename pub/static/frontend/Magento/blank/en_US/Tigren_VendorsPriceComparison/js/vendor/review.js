/*
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */
define([
    'uiComponent',
    'jquery',
    'mage/translate',
    'domReady!'
], function (Component, $,$t) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Tigren_VendorsPriceComparison/vendor/review'
        },
        initialize: function () {
            this._super();
        },
        getRating: function (item) {
            return item.pc_vendor.pc_rating + '%';
        },
        getVendorCount: function (item) {
            var count = item.pc_vendor.pc_review_count;
            if(count < 2){
                return '( '+ count + ' ' + $t('Review') + ' )';
            }else{
                return '( '+ count + ' ' + $t('Reviews') + ' )';
            }
        }
    });
});
