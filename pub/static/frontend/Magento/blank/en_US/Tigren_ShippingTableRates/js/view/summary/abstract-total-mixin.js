/*
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/step-navigator'
    ],
    function (Component, stepNavigator) {
        "use strict";
        return function (abstractTotal) {
            return abstractTotal.extend({
                isFullMode: function() {
                    if (!this.getTotals()) {
                        return false;
                    }
                    return true; //add this line to display forcefully summary in shipping step.
                }
            });
        }
    });