/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/checkout-data',
    'Magento_Customer/js/customer-data',
    'Tigren_AdvancedCheckout/js/action/delete-shipping-address',
    'mage/url',
    'Magento_Ui/js/modal/confirm',
    'mage/translate',
    'Magento_Checkout/js/model/checkout-data-resolver'
], function ($,
             ko,
             Component,
             selectShippingAddressAction,
             quote,
             formPopUpState,
             checkoutData,
             customerData,
             deleteShippingAddress,
             url,
             confirm,
             $t,
             checkoutDataResolver
) {
    'use strict';

    var countryData = customerData.get('directory-data');

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/shipping-address/address-renderer/default'
        },

        /** @inheritdoc */
        initObservable: function () {
            this._super();
            this.isSelected = ko.computed(function () {
                var isSelected = false,
                    shippingAddress = quote.shippingAddress();

                if (shippingAddress) {
                    isSelected = shippingAddress.getKey() == this.address().getKey(); //eslint-disable-line eqeqeq
                }

                return isSelected;
            }, this);

            this.isDefault = ko.computed(function () {
                var isDefault = false,
                    address = this.address();
                if (address) {
                    isDefault = address.isDefaultShipping();
                }
                return isDefault;
            }, this);

            return this;
        },

        /**
         * @param {String} countryId
         * @return {String}
         */
        getCountryName: function (countryId) {
            return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
        },

        /** Set selected customer shipping address  */
        selectAddress: function () {
            selectShippingAddressAction(this.address());
            checkoutData.setSelectedShippingAddress(this.address().getKey());
        },

        /**
         * Edit address.
         */
        editAddress: function () {
            formPopUpState.isVisible(true);
            formPopUpState.hasUpdatedAddress(this.address());
            this.showPopup();

        },

        deleteAddress: function () {
            var self = this;
            confirm({
                content: $t('Are you sure you would like to remove this address from the addresses list?'),
                actions: {
                    /** @inheritdoc */
                    confirm: function () {
                        self._removeAddress();
                    },

                    /** @inheritdoc */
                    always: function (e) {
                        e.stopImmediatePropagation();
                    }
                }
            });
        },

        _removeAddress: function () {
            var address = this.address();
            if (address.customerAddressId) {
                if (address.isDefaultShipping()) {
                    alert($t('Can\'t delete default shipping address'));
                } else {

                    $.ajax({
                        url: url.build('advancedcheckout/customer/deleteAddress'),
                        method: 'post',
                        data: {id: address.customerAddressId},
                        datatype: 'json',
                        beforeSend: function () {
                            $('body').trigger('processStart');
                        },
                        complete: function () {
                            $('body').trigger('processStop');
                        },
                        success: function (response) {
                            if (response.success) {
                                deleteShippingAddress(address);
                                checkoutDataResolver.reloadShippingAddress();
                            } else {
                                // this.errorValidationMessage($t('You cannot delete this address for now.'));
                            }
                        }
                    });
                }
            } else {
                deleteShippingAddress(address);
                checkoutData.setNewCustomerShippingAddress();
                checkoutDataResolver.reloadShippingAddress();
                $('.action-show-popup').show();
            }
        },

        /**
         * Show popup.
         */
        showPopup: function () {
            $('[data-open-modal="opc-new-shipping-address"]').trigger('click');
        }
    });
});
