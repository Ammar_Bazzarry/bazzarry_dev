/**
 * Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 */

define([
    'underscore',
    'jquery',
    'ko',
    'uiComponent',
    'https://maps.googleapis.com/maps/api/js?key=' + window.checkoutConfig.storepickup.google.apiUrl + '&v=3.exp&libraries=places'
], function (_, $, ko, Component) {
    'use strict';

    var googleMaps = null;

    return Component.extend({
        defaults: {
            modules: {
                stateSuggestionField: '${ $.parentName }.state-suggestion',
                parent: '${ $.parentName }'
            },

            imports: {
                update: '${ $.parentName }.state-suggestion:value',
                onChangeStore: '${ $.parentName }.pickup-store:value'
            }
        },

        initPickupMap: function () {
            var currentLat = window.checkoutConfig.storepickup.center.lat,
                currentLng = window.checkoutConfig.storepickup.center.lng,
                currentZoom = parseInt(window.checkoutConfig.storepickup.zoom);

            new google.maps.Map(document.getElementById('store-pickup-map'), {
                zoom: currentZoom,
                center: new google.maps.LatLng(currentLat, currentLng)
            })
        },

        /**
         * @returns void
         */
        onChangeStore: function (storeId) {
            if (storeId) {
                var storeInfo = window.checkoutConfig.pickupStores[storeId];
                this.updateMap(storeInfo);
            }
        },

        updateMap: function (storeInfo) {
            googleMaps = new google.maps.Map(document.getElementById('store-pickup-map'), {
                zoom: parseInt(window.checkoutConfig.storepickup.marker_zoom),
                center: new google.maps.LatLng(storeInfo.latitude, storeInfo.longitude)
            });

            // Create marker
            var latLng = new google.maps.LatLng(storeInfo.latitude, storeInfo.longitude);

            new google.maps.MarkerImage(
                new google.maps.Size(71, 71),
                new google.maps.Point(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Size(44, 44)
            );

            var infoWindow = new google.maps.InfoWindow({
                content: '<div class="store-info">'
                + '<p><h3 style="margin: 10px 0">'
                + storeInfo.store_name
                + '</h3></p><p>City: '
                + storeInfo.city
                + '</p><p>Address: '
                + _.values(storeInfo.street).join(", ")
                + '</p><p>Post Code: '
                + storeInfo.postcode
                + '</p><p>Telephone: '
                + storeInfo.telephone
                + '</p></div>'
            });

            var marker = new google.maps.Marker({
                position: latLng,
                map: googleMaps,
                infowindow: infoWindow
            });

            marker.addListener('click', function (event) {
                infoWindow.open(googleMaps, marker);
                googleMaps.setZoom(parseInt(window.checkoutConfig.storepickup.marker_zoom));
                googleMaps.setCenter(marker.getPosition());
            });
        }
    });
});