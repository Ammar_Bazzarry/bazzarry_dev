/**
* Copyright © 2016 Magento. All rights reserved.
* See COPYING.txt for license details.
*/
define([
    'uiComponent',
    'jquery',
    'domReady!'
], function (Component, $) {
    'use strict';

    return Component.extend({
    	defaults: {
			template: 'Vnecoms_VendorsPriceComparison/vendor/addtocart'
    	},

        getAddToCartUrl: function(product){
        	return product.pc_addtocart_url;
        }
    });
});
