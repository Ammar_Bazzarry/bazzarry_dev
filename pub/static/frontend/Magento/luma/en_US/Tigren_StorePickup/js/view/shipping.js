/**
 * Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 */
define([
    'jquery',
    'underscore',
    'Magento_Ui/js/form/form',
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/create-shipping-address',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/action/set-shipping-information',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Ui/js/modal/modal',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/checkout-data',
    'uiRegistry',
    'mage/translate',
    'Magento_Checkout/js/model/shipping-rate-service'
], function (
    $,
    _,
    Component,
    ko,
    customer,
    addressList,
    addressConverter,
    quote,
    createShippingAddress,
    selectShippingAddress,
    shippingRatesValidator,
    formPopUpState,
    shippingService,
    selectShippingMethodAction,
    rateRegistry,
    setShippingInformationAction,
    stepNavigator,
    modal,
    checkoutDataResolver,
    checkoutData,
    registry,
    $t
) {
    'use strict';

    var mixin = {
        defaults: {
            template: 'Tigren_StorePickup/shipping',
            storeSuggestionTemplate: 'Tigren_StorePickup/shipping-address/pickup-store-container',
            shippingMethodItemTemplate: 'Tigren_StorePickup/shipping-address/shipping-method-item',

            modules: {
                pickupStoreSuggestion: '${ $.name }.pickup-store',
                billing: 'checkout.steps.billing-step.payment.payments-list.free-form'
            },

            imports: {
                onChangeStore: '${ $.name }.pickup-store:value'
            },

            deliveryDisabled: false,
            storePickupDisabled: false,
            selectedShippingType: 'delivery',
            isSelectedStorePickup: false,
            hasLoggedIn: customer.isLoggedIn()
        },

        shippingMethodNextBtnSelector: '#shipping-method-buttons-container button[type=submit]',

        /** @inheritdoc */
        initObservable: function () {
            this._super()
                .observe(['deliveryDisabled', 'storePickupDisabled', 'selectedShippingType', 'isSelectedStorePickup', 'hasLoggedIn']);

            this.selectedShippingType.subscribe(function (type) {
                this.isSelectedStorePickup(type === 'store_pickup');
            }.bind(this));

            this.isSelectedStorePickup.subscribe(function (isSelectedStorePickup) {
                if (!isSelectedStorePickup) {
                    checkoutDataResolver.reapplyShippingAddress();
                    if(this.billing()){
                        this.billing().hideButton(false);
                        this.billing().isAddressDetailsVisible(true);
                    }
                } else {
                    var shippingAddress = quote.shippingAddress(),
                        selectedPickupStore = this.pickupStoreSuggestion().value();
                    if (selectedPickupStore) {
                        if (!shippingAddress
                            || shippingAddress['extension_attributes'] === undefined
                            || shippingAddress['extension_attributes']['pickupstore_id'] === undefined) {
                            shippingAddress = addressConverter.formAddressDataToQuoteAddress(window.checkoutConfig.pickupStores[selectedPickupStore]);
                            if (shippingAddress['extension_attributes'] === undefined) {
                                shippingAddress['extension_attributes'] = {};
                            }
                            shippingAddress['extension_attributes']['pickupstore_id'] = selectedPickupStore;
                            selectShippingAddress(shippingAddress);
                        }
                    }
                    if(this.billing()){
                        this.billing().hideButton(true);
                        this.billing().isAddressSameAsShipping(false);
                        this.billing().isAddressDetailsVisible(false);
                    }
                }
            }.bind(this));

            return this;
        },

        handleAfterRender: function () {
            if (quote.shippingMethod() && quote.shippingMethod()['method_code']) {
                var carrierCode = quote.shippingMethod()['carrier_code'],
                    methodCode = quote.shippingMethod()['method_code'];

                if (carrierCode === 'storepickup' && methodCode === 'storepickup') {
                    this.selectedShippingType('store_pickup');
                } else {
                    this.selectedShippingType('delivery');
                }
            }
        },

        handleAfterRenderShippingMethod: function (method) {
            this.deliveryDisabled(false);
            this.storePickupDisabled(false);
        },

        /**
         * @returns void
         */
        onChangeStore: function (selectedPickupStore) {
            if (selectedPickupStore) {
                var shippingAddress = window.checkoutConfig.pickupStores[selectedPickupStore];
                shippingAddress.custom_attributes = {
                    pickupstore_id: selectedPickupStore
                };

                shippingAddress = addressConverter.formAddressDataToQuoteAddress(shippingAddress);
                if (shippingAddress['extension_attributes'] === undefined) {
                    shippingAddress['extension_attributes'] = {};
                }
                shippingAddress['extension_attributes']['pickupstore_id'] = selectedPickupStore;

                selectShippingAddress(shippingAddress);
            }
        },

        onClickNextButton: function () {
            $(this.shippingMethodNextBtnSelector).click();
        },

        /**
         * @return {Boolean}
         */
        validateShippingInformation: function () {
            var shippingAddress = quote.shippingAddress(),
                addressData,
                loginFormSelector = 'form[data-role=email-with-possible-login]',
                emailValidationResult = customer.isLoggedIn(),
                field;

            if (!quote.shippingMethod()) {
                this.errorValidationMessage($t('Please specify a shipping method.'));
                return false;
            }

            if (!this.selectedShippingType()) {
                this.errorValidationMessage($t('Please specify a shipping type.'));
                return false;
            }

            if (this.isSelectedStorePickup() || quote.shippingMethod().carrier_code === 'storepickup') {
                var selectedPickupStore = this.pickupStoreSuggestion().value();

                if (selectedPickupStore === '') {
                    if (shippingAddress['extension_attributes'] !== undefined
                        && shippingAddress['extension_attributes']['pickupstore_id'] !== undefined) {
                        delete shippingAddress['extension_attributes']['pickupstore_id'];
                    }
                    this.errorValidationMessage($t('Please specify a store to pickup.'));
                    return false;
                }

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());

                    if (!emailValidationResult) {
                        $(loginFormSelector + ' input[name=username]').focus();
                        return false;
                    }
                }

                return true;
            } else {
                if (shippingAddress['extension_attributes'] && shippingAddress['extension_attributes']['pickupstore_id'] !== undefined) {
                    delete shippingAddress['extension_attributes']['pickupstore_id'];
                }
                if (shippingAddress['extension_attributes']){
                    shippingAddress['extension_attributes']['location_type'] = $("[name='custom_attributes[location_type]']").val();
                }
            }

            if (!customer.isLoggedIn()) {
                $(loginFormSelector).validation();
                emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
            }

            if (this.isFormInline) {
                this.source.set('params.invalid', false);
                this.triggerShippingDataValidateEvent();

                if (emailValidationResult &&
                    this.source.get('params.invalid') ||
                    !quote.shippingMethod()['method_code'] ||
                    !quote.shippingMethod()['carrier_code']
                ) {
                    this.focusInvalid();

                    return false;
                }

                addressData = addressConverter.formAddressDataToQuoteAddress(
                    this.source.get('shippingAddress')
                );

                //Copy form data to quote shipping address object
                for (field in addressData) {
                    if (addressData.hasOwnProperty(field) &&  //eslint-disable-line max-depth
                        shippingAddress.hasOwnProperty(field) &&
                        typeof addressData[field] !== 'function' &&
                        _.isEqual(shippingAddress[field], addressData[field])
                    ) {
                        shippingAddress[field] = addressData[field];
                    } else if (typeof addressData[field] !== 'function' &&
                        !_.isEqual(shippingAddress[field], addressData[field])) {
                        shippingAddress = addressData;
                        break;
                    }
                }

                if (customer.isLoggedIn()) {
                    shippingAddress['save_in_address_book'] = 1;
                }
                selectShippingAddress(shippingAddress);
            }

            if (!emailValidationResult) {
                $(loginFormSelector + ' input[name=username]').focus();

                return false;
            }

            return true;
        }
    };

    return function (target) { // target == Result that Magento_Ui/.../default returns.
        return target.extend(mixin); // new result that all other modules receive
    };
});