/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'ko',
        'underscore',
        'Magento_Ui/js/form/form',
        'Magento_Customer/js/model/customer',
        'Magento_Customer/js/model/address-list',
        'Magento_Checkout/js/model/quote'
    ],
    function (
        ko,
        _,
        Component,
        customer,
        addressList,
        quote
    ) {
        'use strict';

        var mixin = {
            showBillingAsShipping: ko.observable(true),

            initialize: function () {
                this._super();
                if (!quote.isVirtual() && quote.shippingAddress() && quote.shippingAddress().canUseForBilling()){
                    this.showBillingAsShipping(true);
                }
            },

            hideButton: function (value) {
                this.showBillingAsShipping(!value);
            },
        };

        return function (target) { // target == Result that Magento_Ui/.../default returns.
            return target.extend(mixin); // new result that all other modules receive
        };
    });
