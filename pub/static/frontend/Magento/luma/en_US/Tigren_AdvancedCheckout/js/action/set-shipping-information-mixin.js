/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function (setShippingInformationAction) {

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            if (quote.shippingMethod()) {
                var shippingAddress = quote.shippingAddress();
                if (shippingAddress.customAttributes) {
                    if (shippingAddress['extensionAttributes'] === undefined) {
                        shippingAddress['extensionAttributes'] = {};
                    }
                    shippingAddress['extensionAttributes']['pickupstore_id'] = shippingAddress.customAttributes.pickupstore_id;
                    shippingAddress['extensionAttributes']['location_type'] = shippingAddress.customAttributes.location_type;
                    shippingAddress['extensionAttributes']['nearest_landmark'] = shippingAddress.customAttributes['nearest_landmark'];
                }
            }
            var billingAddress = quote.billingAddress();
            if (billingAddress && billingAddress.customAttributes) {
                if (billingAddress['extensionAttributes'] === undefined) {
                    billingAddress['extensionAttributes'] = {};
                }
                billingAddress['extensionAttributes']['location_type'] = billingAddress.customAttributes['location_type'];
                billingAddress['extensionAttributes']['nearest_landmark'] = billingAddress.customAttributes['nearest_landmark'];
            }
            // pass execution to original action ('Magento_Checkout/js/action/set-shipping-information')
            return originalAction();
        });
    };
});