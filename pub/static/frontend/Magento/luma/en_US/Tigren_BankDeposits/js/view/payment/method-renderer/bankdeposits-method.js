/*
 * Copyright (c) 2017 www.tigren.com
 */

define(
    [
        'Magento_Checkout/js/view/payment/default',
        'jquery',
        'ko',
        'mage/translate'
    ],
    function (Component, $,ko,$t) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Tigren_BankDeposits/payment/bankdeposits'
            },
            errorMessage: ko.observable(''),
            getInstructions: function () {
                return window.checkoutConfig.payment.instructions[this.item.method];
            },
            getData: function () {
                var self = this;
                var bank = $('input:radio[name=bank_payment]:checked').val();
                return {
                    'method': self.item.method,
                    'additional_data': {
                        'bank_name': bank?bank:'',
                        'bank_account': '665173216'
                    }
                }
            },
            validate: function () {
                var self = this;
                var bank = $('input:radio[name=bank_payment]:checked').val();
                if(!bank){
                    self.errorMessage($t('Please select bank'));
                    return false;
                }
                self.errorMessage($t(''));
                return true;
            },
        })
    }
);
