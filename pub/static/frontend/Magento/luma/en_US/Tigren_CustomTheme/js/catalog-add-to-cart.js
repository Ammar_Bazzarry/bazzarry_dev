/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Ui/js/modal/modal',
    'mage/url',
    'Magento_Customer/js/customer-data',
    'jquery',
    'mage/translate',
    'underscore',
    'Magento_Catalog/js/product/view/product-ids-resolver',
    'jquery/ui'
], function (modal, urlBuilder, customerData, $, $t, _, idsResolver) {
    'use strict';

    $.widget('mage.catalogAddToCart', {
        options: {
            processStart: null,
            processStop: null,
            bindSubmit: true,
            minicartSelector: '[data-block="minicart"]',
            messagesSelector: '[data-placeholder="messages"]',
            productStatusSelector: '.stock.available',
            addToCartButtonSelector: '.action.tocart',
            addToCartButtonDisabledClass: 'disabled',
            addToCartButtonTextWhileAdding: '',
            addToCartButtonTextAdded: '',
            addToCartButtonTextDefault: ''
        },

        /** @inheritdoc */
        _create: function () {
            var self = this;
            self._bindSubmit();
        },

        /**
         * @private
         */
        _bindSubmit: function () {
            var self = this;


            $('body').delegate(self.options.addToCartButtonSelector, 'click', function (e) {
                var form = $(this).parents('form:first');
                if(form.data('catalog-addtocart-initialized')) {
                    return;
                }
                form.data('catalog-addtocart-initialized', 1);
                e.preventDefault();
                self.submitForm(form);
            })
        },

        /**
         * @private
         * @param {String} url
         */
        _redirect: function (url) {
            var urlParts = url.split('#'),
                locationParts = window.location.href.split('#'),
                forceReload = urlParts[0] === locationParts[0];

            window.location.assign(url);

            if (forceReload) {
                window.location.reload();
            }
        },

        /**
         * @return {Boolean}
         */
        isLoaderEnabled: function () {
            return this.options.processStart && this.options.processStop;
        },

        /**
         * Handler for the form 'submit' event
         *
         * @param {jQuery} form
         */
        submitForm: function (form) {
            this.ajaxSubmit(form);
        },

        /**
         * @param {jQuery} form
         */
        ajaxSubmit: function (form) {
            var self = this,
                productIds = idsResolver(form),
                formData = new FormData(form[0]),
                body = $('body');

            $(self.options.minicartSelector).trigger('contentLoading');
            self.disableAddToCartButton(form);

            $.ajax({
                url: form.attr('action'),
                data: formData,
                type: 'post',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,

                /** @inheritdoc */
                beforeSend: function () {
                    if (self.isLoaderEnabled()) {
                        body.trigger(self.options.processStart);
                    }
                },

                /** @inheritdoc */
                success: function (res) {
                    var eventData, parameters;

                    $(document).trigger('ajax:addToCart', {
                        'sku': form.data().productSku,
                        'productIds': productIds,
                        'form': form,
                        'response': res
                    });

                    if (self.isLoaderEnabled()) {
                        body.trigger(self.options.processStop);
                    }

                    if (res.backUrl && !res.error) {
                        eventData = {
                            'form': form,
                            'redirectParameters': []
                        };
                        // trigger global event, so other modules will be able add parameters to redirect url
                        body.trigger('catalogCategoryAddToCartRedirect', eventData);

                        if (eventData.redirectParameters.length > 0) {
                            parameters = res.backUrl.split('#');
                            parameters.push(eventData.redirectParameters.join('&'));
                            res.backUrl = parameters.join('#');
                        }

                        self._redirect(res.backUrl);

                        return;
                    }

                    if (res.messages) {
                        $(self.options.messagesSelector).html(res.messages);
                    }

                    if (res.minicart) {
                        $(self.options.minicartSelector).replaceWith(res.minicart);
                        $(self.options.minicartSelector).trigger('contentUpdated');
                    }

                    if (res.product && res.product.statusText) {
                        $(self.options.productStatusSelector)
                            .removeClass('available')
                            .addClass('unavailable')
                            .find('span')
                            .html(res.product.statusText);
                    }
                    self.enableAddToCartButton(form);

                    if (res.error) {
                        var popupError = $('<div class="tigren-add-to-cart-popup-error"/>').html(res.message).modal({ //get product name from product view page only
                            modalClass: 'confirm',
                            title: false
                        });
                        popupError.modal('openModal');
                        body.trigger('processStop');
                        return false;
                    }
                    if (res.success) {
                        var checkoutUrl = urlBuilder.build("checkout/");
                        var popup = $('<div class="tigren-add-to-cart-popup"/>').html(res.message).modal({ //get product name from product view page only
                            modalClass: 'confirm',
                            title: false,
                            buttons: [
                                {
                                    text: $.mage.__('Continue Shopping'),
                                    class: 'btn btn-primary continue',
                                    click: function () {
                                        this.closeModal();
                                    }
                                },
                                {
                                    text: $.mage.__("Checkout"),
                                    class: 'btn btn-secondary to-checkout',
                                    click: function () {
                                        window.location = checkoutUrl
                                    }
                                }
                            ]
                        });
                        popup.modal('openModal');
                        body.trigger('popupSuccess');
                        body.trigger('processStop');
                    }
                },

                /** @inheritdoc */
                complete: function (res) {
                    form.data('catalog-addtocart-initialized', 0);
                    if (res.state() === 'rejected') {
                        location.reload();
                    }
                }
            });
        },

        /**
         * @param {String} form
         */
        disableAddToCartButton: function (form) {
            var addToCartButtonTextWhileAdding = this.options.addToCartButtonTextWhileAdding || $t('Adding...'),
                addToCartButton = $(form).find(this.options.addToCartButtonSelector);

            addToCartButton.addClass(this.options.addToCartButtonDisabledClass);
            addToCartButton.find('span').text(addToCartButtonTextWhileAdding);
            addToCartButton.attr('title', addToCartButtonTextWhileAdding);
        },

        /**
         * @param {String} form
         */
        enableAddToCartButton: function (form) {
            var addToCartButtonTextAdded = this.options.addToCartButtonTextAdded || $t('Added'),
                self = this,
                addToCartButton = $(form).find(this.options.addToCartButtonSelector);

            addToCartButton.find('span').text(addToCartButtonTextAdded);
            addToCartButton.attr('title', addToCartButtonTextAdded);

            setTimeout(function () {
                var addToCartButtonTextDefault = self.options.addToCartButtonTextDefault || $t('Add to Cart');

                addToCartButton.removeClass(self.options.addToCartButtonDisabledClass);
                addToCartButton.find('span').text(addToCartButtonTextDefault);
                addToCartButton.attr('title', addToCartButtonTextDefault);
            }, 1000);
        }
    });

    return $.mage.catalogAddToCart;
});
