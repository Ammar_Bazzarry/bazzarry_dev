/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/* @api */
define([
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/model/url-builder',
    'mage/translate',
    'ko',
    'Magento_Checkout/js/model/quote',
    'jquery',
    'mage/storage',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/action/redirect-on-success'
], function (Component, additionalValidators, fullScreenLoader, urlBuilder, $t, ko, quote,$,storage,errorProcessor,redirectOnSuccessAction) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_OfflinePayments/payment/cashondelivery'
        },

        sendText: ko.observable($t('Send Code')),
        confirmationText: ko.observable($t('Please check the message on the phone. We have sent a confirmation code to you')),
        sendButtonId: ko.observable('code-send-sms'),
        enable : ko.observable(false),
        visibleResend : ko.observable(false),
        validated : ko.observable(false),
        isFirst : ko.observable(true),
        codVerifyMessage: ko.observable(''),
        classVerifyMesage: ko.observable('cod-verify-msg'),



        /**
         * Returns payment method instructions.
         *
         * @return {*}
         */
        getInstructions: function () {
            return window.checkoutConfig.payment.instructions[this.item.method];
        },

        redirectUrl: function () {
            return window.checkoutConfig.verificationPageUrl;
        },

        getSendText: function () {
            return this.sendText;
        },

        getSendButtonId: function () {
            return this.sendButtonId;
        },

        getClassVerifyMessage: function () {
            return this.classVerifyMesage;
        },

        enableButton: function () {
            return this.enable;
        },

        sendSms: function () {
            var self = this;
            fullScreenLoader.startLoader();
            var result = storage.post(
                urlBuilder.createUrl('/checkout/cod/sendSms',{})
            ).done(function (response) {
                fullScreenLoader.stopLoader();
                self.sendText($t('Resend'));
                self.sendButtonId('code-resend-sms');
                self.visibleResend(true);
                if(self.isFirst()) {
                    self.codVerifyMessage(self.confirmationText());
                    self.classVerifyMesage('cod-verify-msg success-msg');
                } else {
                    self.codVerifyMessage($t('We have resent a confirmation code to you'));
                    self.classVerifyMesage('cod-verify-msg success-msg');
                }
                self.isFirst(false);
            }).fail(
                function (response) {
                    fullScreenLoader.stopLoader();
                    self.visibleResend(false);
                    self.codVerifyMessage($t('Something wrong when send sms'));
                    self.classVerifyMesage('cod-verify-msg error-msg');
                }
            );
        },

        validateCode: function () {
            var self = this;
            fullScreenLoader.startLoader();
            var result = storage.post(
                urlBuilder.createUrl('/checkout/cod/verify',{}),
                JSON.stringify({
                    code: $('#cod-verify').val()
                })
            ).done(function (response) {
                fullScreenLoader.stopLoader();
                if (response && self.getCode() == self.isChecked()){
                    self.enable(true);
                    self.validated(true);
                    $('#sms-verify-form').hide();
                    self.placeOrder();
                } else {
                    self.codVerifyMessage($t('Wrong OTP code'));
                    self.classVerifyMesage('cod-verify-msg error-msg');
                    self.enable(false);
                    self.validated(false);
                }
            }).fail(
                function (response) {
                    fullScreenLoader.stopLoader();
                    self.codVerifyMessage($t('Something wrong when verify OTP'));
                    self.classVerifyMesage('cod-verify-msg error-msg');
                    self.enable(false);
                    self.validated(false);
                }
            );
        },

        /**
         * Place order.
         */
        placeOrder: function (data, event) {
            var self = this;

            if (event) {
                event.preventDefault();
            }
            if (self.validated() && self.validate() && additionalValidators.validate()) {
                this.isPlaceOrderActionAllowed(false);

                this.getPlaceOrderDeferredObject()
                    .fail(
                        function () {
                            self.isPlaceOrderActionAllowed(true);
                        }
                    ).done(
                    function () {
                        self.afterPlaceOrder();

                        if (self.redirectAfterPlaceOrder) {
                            redirectOnSuccessAction.execute();
                        }
                    }
                );

                return true;
            }

            return false;
        },
    });
});
