define([
    'Magento_Customer/js/customer-data',
    'jquery',
    'mage/translate',
    'Magento_Ui/js/modal/modal',
    'mage/url'
], function (customerData, $, $t, modal, urlBuilder) {
    'use strict';

    $.widget('tigren.ajaxToCart', {
        options: {
            ajaxCart: {
                formKey: null,
                addToCartButtonSelector: 'button.tocart',
                addToCartButtonTextWhileAdding: $t('Adding...'),
                addToCartButtonTextAdded: $t('Added'),
                addToCartButtonTextDefault: $t('Add to Cart')
            }
        },

        _create: function () {
            $(this.options.ajaxCart.addToCartButtonSelector).prop('disabled', false);
            this.initEvents();
            this.options.ajaxCart.formKey = $.cookie('form_key');
        },


        initEvents: function () {
            var self = this;

            $('body').delegate(self.options.ajaxCart.addToCartButtonSelector, 'click', function (e) {
                var form = $(this).parents('form:first');
                if (!form.length) {

                    e.stopPropagation();
                    var postData = $(this).data('post').data,
                        url = urlBuilder.build('customtheme/cart/add');
                    postData.form_key = $.mage.cookies.get('form_key');
                    $.ajax({
                        url: url,
                        data: postData,
                        type: 'post',
                        dataType: 'json',
                        beforeSend: function () {
                            $('body').trigger('processStart');
                        },
                        success: function (res) {
                            if (res.redirect) {
                                window.location = res.redirect
                            } else {
                                if (res.error) {
                                    var popupError = $('<div class="tigren-add-to-cart-popup-error"/>').html(res.message).modal({ //get product name from product view page only
                                        modalClass: 'confirm',
                                        title: false
                                    });
                                    popupError.modal('openModal');
                                    $('body').trigger('processStop');
                                    return false;
                                }
                                if (res.backUrl) {
                                    window.location = res.backUrl
                                }
                                var checkoutUrl = urlBuilder.build("checkout/");
                                var popup = $('<div class="tigren-add-to-cart-popup"/>').html(res.message).modal({ //get product name from product view page only
                                    modalClass: 'confirm',
                                    title: false,
                                    buttons: [
                                        {
                                            text: $.mage.__('Continue Shopping'),
                                            class: 'btn btn-primary',
                                            click: function () {
                                                this.closeModal();
                                            }
                                        },
                                        {
                                            text: $.mage.__("Checkout"),
                                            class: 'btn btn-secondary',
                                            click: function () {
                                                window.location = checkoutUrl;
                                            }
                                        }
                                    ]
                                });
                                popup.modal('openModal');
                                $('body').trigger('processStop');
                            }
                        },
                        error: function (response) {

                        }
                    })
                }
            })
        }
    });

    return $.tigren.ajaxToCart;
});

