/*
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

define(
    [
        'Magento_Checkout/js/view/payment/default',
        'MageWorx_GiftCards/js/model/gift-card-info',
        'MageWorx_GiftCards/js/model/payment/gift-card-messages',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/url-builder',
        'Magento_Checkout/js/action/get-payment-information',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/error-processor',
        'jquery',
        'Magento_Checkout/js/model/totals',
        'mage/storage',
        'mage/translate',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/action/redirect-on-success',
        'ko',
        'priceUtils'
    ],
    function (Component,
              giftCardInfo,
              messageContainer,
              fullScreenLoader,
              customer,
              urlBuilder,
              getPaymentInformationAction,
              quote,
              errorProcessor,
              $,
              totals,
              storage,
              $t,
              additionalValidators,
              redirectOnSuccessAction,
              ko,
              priceUtils) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Tigren_BazzarryPayment/payment/bazzarrypayment',
                giftCartCodeFieldValue: ''
            },
            initObservable: function () {
                this._super()
                    .observe('giftCartCodeFieldValue');

                return this;
            },
            giftCardInfo: giftCardInfo,
            hasError: ko.observable(false),
            enablePlaceOrder: ko.observable(false),
            errorMessage: ko.observable(''),
            giftcardErrorMessage: ko.observable(''),
            getInstructions: function () {
                return window.checkoutConfig.payment.instructions[this.item.method];
            },

            /**
             * @return {jQuery}
             */
            validate: function () {

                return this.isAvailable();
            },

            getMessage: function (messageObj) {
                var expr = /([%])\w+/g,
                    message;

                if (!messageObj.hasOwnProperty('parameters')) {
                    this.clear();
                    return messageObj.message;
                }
                message = messageObj.message.replace(expr, function (varName) {
                    varName = varName.substr(1);

                    if (messageObj.parameters.hasOwnProperty(varName)) {
                        return messageObj.parameters[varName];
                    }

                    return messageObj.parameters.shift();
                });
                return message;
            },

            isLoading: ko.observable(false),

            validateForm: function () {
                var form = '#mageworx-giftcards-form';

                return $(form).validation() && $(form).validation('isValid');
            },

            /**
             * Activate Gift Card
             */
            activateGiftCard: function () {

                giftCardInfo.isChecked(false);
                giftCardInfo.isValid(false);

                if (this.validateForm()) {
                    var self = this,
                        url,
                        data = {};

                    if (!customer.isLoggedIn()) {
                        url = urlBuilder.createUrl(
                            '/carts/guest-carts/:cartId/mw-giftcards/:giftCardCode',
                            {
                                cartId: quote.getQuoteId(),
                                giftCardCode: self.giftCartCodeFieldValue()
                            }
                        );
                    } else {
                        url = urlBuilder.createUrl(
                            '/carts/mine/mw-giftcards/:giftCardCode',
                            {
                                giftCardCode: self.giftCartCodeFieldValue()
                            }
                        );
                        data.cartId = quote.getQuoteId()
                    }

                    messageContainer.clear();
                    fullScreenLoader.startLoader();

                    storage.put(
                        url,
                        JSON.stringify(data)
                    ).done(
                        function (response) {
                            if (response) {
                                var deferred = $.Deferred();

                                totals.isLoading(true);
                                getPaymentInformationAction(deferred, messageContainer);
                                $.when(deferred).done(
                                    function () {
                                        totals.isLoading(false);
                                    }
                                );
                                messageContainer.addSuccessMessage({
                                    'message': $t('Gift Card') + ' "' + self.giftCartCodeFieldValue() + '" ' + $t('was applied.')
                                });
                                self.enablePlaceOrder(true);
                                $('#mageworx-giftcard-code').val('');
                            }
                        }
                    ).fail(
                        function (response) {
                            totals.isLoading(false);
                            errorProcessor.process(response, messageContainer);
                            var listCardCode = '';
                            if(response.responseJSON.parameters){
                                listCardCode = response.responseJSON.parameters.join(', ');
                            }
                            self.giftcardErrorMessage(response.responseJSON.message.replace('%cardCode', listCardCode));
                        }
                    ).always(
                        function () {
                            fullScreenLoader.stopLoader();
                        }
                    )
                }
            },

            /**
             * Check Gift Card Information
             */
            checkGiftCardInfo: function () {

                if (this.validateForm()) {

                    var self = this;
                    var form = '#mageworx-giftcards-form';
                    var data = $(form).serialize();

                    messageContainer.clear();
                    this.isLoading(true);

                    storage.get(
                        'mageworx_giftcards/cart/ajaxGiftCardInfo' + '?' + data
                    ).done(
                        function (response) {

                            if (response.success) {
                                giftCardInfo.isChecked(true);
                                giftCardInfo.status(response.status);
                                giftCardInfo.balance(response.balance);
                                giftCardInfo.validTill(response.validTill);
                                giftCardInfo.isValid(true);
                                self.giftcardErrorMessage('');
                            } else {
                                giftCardInfo.isValid(false);
                                messageContainer.addErrorMessage({
                                    'message': response.message
                                });
                                self.giftcardErrorMessage(response.message);
                            }
                        }
                    ).fail(
                        function (response) {
                            giftCardInfo.isValid(false);
                            errorProcessor.process(response, messageContainer);
                        }
                    ).always(
                        function () {
                            self.isLoading(false);
                        }
                    )
                }
            },

            getAppliedGiftCards: function () {
                if (totals.getSegment('mageworx_giftcards')) {
                    return JSON.parse(totals.getSegment('mageworx_giftcards')['extension_attributes']['front_options']);
                }
                this.enablePlaceOrder(false);
                return [];
            },

            /**
             * @return {Object|Boolean}
             */
            isAvailable: function () {
                var available = totals.getSegment('mageworx_giftcards') && totals.getSegment('mageworx_giftcards').value != 0;
                if (!available) {
                    this.enablePlaceOrder(false);
                } else {
                    this.enablePlaceOrder(true);
                }
                return available;
            },

            /**
             * @param {*} price
             * @return {*|String}
             */
            getFormattedPrice: function (price) {
                return priceUtils.formatPrice(price, quote.getPriceFormat());
            },

            /**
             * @param {String} giftCardCode
             * @param {Object} event
             */
            removeGiftCard: function (giftCardCode, event) {
                event.preventDefault();

                if (giftCardCode) {
                    var url;

                    if (!customer.isLoggedIn()) {
                        url = urlBuilder.createUrl(
                            '/carts/guest-carts/:cartId/mw-giftcards/:giftCardCode',
                            {
                                cartId: quote.getQuoteId(),
                                giftCardCode: giftCardCode
                            }
                        );
                    } else {
                        url = urlBuilder.createUrl(
                            '/carts/mine/mw-giftcards/:giftCardCode',
                            {
                                giftCardCode: giftCardCode
                            }
                        );
                    }

                    messageContainer.clear();
                    fullScreenLoader.startLoader();

                    storage.delete(
                        url
                    ).done(
                        function (response) {
                            if (response) {
                                var deferred = $.Deferred();

                                totals.isLoading(true);
                                getPaymentInformationAction(deferred, messageContainer);
                                $.when(deferred).done(function () {
                                    totals.isLoading(false);
                                });
                                messageContainer.addSuccessMessage({
                                    'message': $t('Gift Card') + ' "' + giftCardCode + '" ' + $t('was removed.')
                                });
                            }
                        }
                    ).fail(
                        function (response) {
                            totals.isLoading(false);
                            errorProcessor.process(response, messageContainer);
                        }
                    ).always(
                        function () {
                            fullScreenLoader.stopLoader();
                        }
                    )
                }
            }

        });
    }
);
