/*
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

define([
    'Magento_Catalog/js/price-utils',
    'mageUtils',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/url-builder',
    'mage/storage',
    'jquery',
    'underscore',
    'Magento_Ui/js/form/form',
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/create-shipping-address',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/action/set-shipping-information',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Ui/js/modal/modal',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/checkout-data',
    'uiRegistry',
    'mage/translate',
    'Magento_Checkout/js/model/shipping-rate-service',
    'Amasty_ShippingTableRates/js/view/shipping-rates-validation'
], function (
    priceUtils,
    utils,
    errorProcessor,
    urlBuilder,
    storage,
    $,
    _,
    Component,
    ko,
    customer,
    addressList,
    addressConverter,
    quote,
    createShippingAddress,
    selectShippingAddress,
    shippingRatesValidator,
    formPopUpState,
    shippingService,
    selectShippingMethodAction,
    rateRegistry,
    setShippingInformationAction,
    stepNavigator,
    modal,
    checkoutDataResolver,
    checkoutData,
    registry,
    $t
) {
    'use strict';
    var imageData = window.checkoutConfig.imageData;
    var displayPriceMode = window.checkoutConfig.reviewItemPriceDisplayMode || 'including';
    var mainVendor = window.checkoutConfig.main_vendor;
    var vendorList = window.checkoutConfig.vendors_list;

    var mixin = {
        defaults: {
            selectedWarehouseMethods: {},
            ratesByWarehouse: ko.observableArray([]),
            track: {
                selectedWarehouseMethods: true
            }
        },
        CARRIER_CODE: 'warehouse_multirate',
        SEPARATOR: '||',
        METHOD_SEPARATOR: '|_|',
        WAREHOUSE_LIST: [],
        WAREHOUSE_LIST_ITEM: [],
        VENDOR_LIST: [],
        VENDOR_LIST_ITEM: {},
        selectedRates : ko.observable({}),

        initialize: function () {
            var self = this;
            this._super();
            this.initRatesByWarehouse();
            shippingService.getShippingRates().subscribe(function () {
                this.getWarehouseList();
                this.initRatesByWarehouse();
                this.initSelectedWarehouseMethods();
                this.selectedRates({});
            }.bind(this));

            this.initSelectedWarehouseMethods();
            quote.shippingMethod.subscribe(function () {
                this.initSelectedWarehouseMethods();
            }.bind(this));
            this.ratesByWarehouse.subscribe(function (ratesByWarehouse) {
                console.log(ratesByWarehouse);
            }.bind(this));
            return this;
        },

        getImageItem: function (item) {
            if (imageData[item['item_id']]) {
                return imageData[item['item_id']];
            }

            return [];
        },

        /**
         * @param {Object} item
         * @return {null}
         */
        getSrc: function (item) {
            if (imageData[item['item_id']]) {
                return imageData[item['item_id']].src;
            }

            return null;
        },

        /**
         * @param {Object} item
         * @return {null}
         */
        getWidth: function (item) {
            if (imageData[item['item_id']]) {
                return imageData[item['item_id']].width;
            }

            return null;
        },

        /**
         * @param {Object} item
         * @return {null}
         */
        getHeight: function (item) {
            if (imageData[item['item_id']]) {
                return imageData[item['item_id']].height;
            }

            return null;
        },

        /**
         * @param {Object} item
         * @return {null}
         */
        getAlt: function (item) {
            if (imageData[item['item_id']]) {
                return imageData[item['item_id']].alt;
            }

            return null;
        },

        isPriceInclTaxDisplayed: function () {
            return displayPriceMode == 'both' || displayPriceMode == 'including'; //eslint-disable-line eqeqeq
        },

        /**
         * @return {Boolean}
         */
        isPriceExclTaxDisplayed: function () {
            return displayPriceMode == 'both' || displayPriceMode == 'excluding'; //eslint-disable-line eqeqeq
        },

        /**
         * @param {Object} quoteItem
         * @return {*|String}
         */
        getValueInclTax: function (quoteItem) {
            return this.getFormattedPrice(quoteItem['row_total_incl_tax']);
        },

        /**
         * @param {Object} quoteItem
         * @return {*|String}
         */
        getValueExclTax: function (quoteItem) {
            return this.getFormattedPrice(quoteItem['row_total']);
        },

        getFormattedPrice: function (price) {
            return priceUtils.formatPrice(price, quote.getPriceFormat());
        },

        removeItem: function (item) {
            $('#remove-cart-item-' + item.item_id).click();
        },

        getWarehouseList: function () {
            var self = this;
            var masterMethod = 'warehouse_list_';
            shippingService.getShippingRates().each(function (rate) {
                if (rate.carrier_code == 'warehouse_multirate' && rate.method_code.indexOf(masterMethod) >= 0) {
                    var lists = [],
                        warehouseList = [],
                        warehouseWithItems = [],
                        vendorList = [],
                        vendorWithItems = {},
                        warehouseItems = rate.method_code.substr(masterMethod.length, rate.method_code.length - masterMethod.length),
                        warehouseListItems = warehouseItems.split('||');
                    for (var i = 0; i < warehouseListItems.length; i++) {
                        let items = warehouseListItems[i];
                        lists = items.split(':');
                        if (lists.length !== 2) return;
                        warehouseList.push(lists[0]);
                        let listItems = lists[1].split(',');
                        warehouseWithItems[lists[0]] = listItems;
                        $.each(listItems, function (index,item) {
                            let itemInfo = item.split('-');
                            let vendorId = itemInfo[2];
                            if (!vendorList.includes(vendorId)) {
                                vendorList.push(vendorId);
                            }
                            if (vendorWithItems[vendorId] === undefined) {
                                vendorWithItems[vendorId] = [];
                            }
                            if (!vendorWithItems[vendorId].includes(itemInfo[0])) {
                                vendorWithItems[vendorId].push(itemInfo[0]);
                            }
                        });
                    }
                    self.WAREHOUSE_LIST = warehouseList;
                    self.WAREHOUSE_LIST_ITEM = warehouseWithItems;
                    self.VENDOR_LIST = vendorList;
                    self.VENDOR_LIST_ITEM = vendorWithItems;
                }
            });
        },

        /**
         * Init selected warehouse methods
         */
        initSelectedWarehouseMethods: function () {
            console.log('INIT SELECTED WAREHOUSE METHODS');
            var selectedShippingRates = '',
                validate;
            if (quote.shippingMethod()) {
                selectedShippingRates = quote.shippingMethod()['carrier_code'] + '_' + quote.shippingMethod()['method_code'];
            }
            if (!selectedShippingRates && checkoutData.getSelectedShippingRate()) {
                selectedShippingRates = checkoutData.getSelectedShippingRate();
            }
            shippingService.getShippingRates().each(function (rate) {
                if (rate.method_code == selectedShippingRates.replace('warehouse_multirate_', '')) {
                    validate = true;
                    return;
                }
            });
            if(!validate){
                selectShippingMethodAction(null);
                checkoutData.setSelectedShippingRate(null);
                return false;
            }
            selectedShippingRates = selectedShippingRates.replace('warehouse_multirate_', '').split(this.METHOD_SEPARATOR);
            var selectedWarehouseMethods = {};
            let selectedRates = {};
            if (selectedShippingRates.length) {
                $(selectedShippingRates).each(function (index, methodCode) {
                    var warehouseId = methodCode.split(this.SEPARATOR);
                    if (warehouseId.length == 3) {
                        if (!selectedRates[warehouseId[2]]) selectedRates[warehouseId[2]] = {};
                        selectedRates[warehouseId[2]][warehouseId[1]] = methodCode.replace('amstrates_', '');
                        warehouseId = 'warehouse_' + warehouseId[1] + '_' + warehouseId[2];
                        selectedWarehouseMethods[warehouseId] = methodCode;
                    }
                }.bind(this));
                this.selectedRates(selectedRates);
            }
            console.log(selectedWarehouseMethods);
            var ratesByWarehouse = this.ratesByWarehouse();
            
            $.each(ratesByWarehouse, function (indexVendor,vendorData) {
                $.each(vendorData['warehouses'], function (index, warehouseRates) {
                    var key = 'warehouse_' + warehouseRates.warehouse_id + '_' + vendorData.entity_id;
                    var selectedRateVal = typeof (selectedWarehouseMethods[key]) != 'undefined' ? selectedWarehouseMethods[key] : false;
                    ratesByWarehouse[indexVendor]['warehouses'][index].selectedRate(selectedRateVal);
                });
            });

            this.ratesByWarehouse(ratesByWarehouse);
        },

        rates: function () {
            var self = this;
            var rates = shippingService.getShippingRates();
            var warehouseRates = [];
            rates.each(function (rate) {
                if (rate.carrier_code == self.CARRIER_CODE) {
                    warehouseRates.push(rate);
                }
            });

            return warehouseRates;
        },

        initRatesByWarehouse: function () {
            console.log('INIT RATES BY WAREHOUSE');
            var self = this,
                groups = {},
                warehouseList = checkoutConfig.warehouse,
                warehouseIds = self.WAREHOUSE_LIST,
                newWarehouseList = {},
                cartItems = quote.getItems(),
                warehouseWithItems = [];
            $.each(self.WAREHOUSE_LIST_ITEM, function (key, value) {
                $.each(value, function (keyItem, itemData) {
                    $.each(cartItems, function (keyCart, cartItem) {
                        var resItemData = itemData.split('-');
                        if (cartItem.item_id == resItemData[0]) {
                            if (typeof (warehouseWithItems['warehouse_' + key]) == 'undefined') {
                                warehouseWithItems['warehouse_' + key] = [];
                            }
                            var newCartItemData;
                            if (cartItem.qty){
                                newCartItemData = {
                                    qty: resItemData[1],
                                    row_total : (cartItem.row_total * resItemData[1])/cartItem.qty,
                                    row_total_incl_tax : (cartItem.row_total_incl_tax * resItemData[1])/cartItem.qty
                                }
                            }else{
                                newCartItemData = {
                                    qty: resItemData[1]
                                }
                            }
                            warehouseWithItems['warehouse_' + key].push(_.extend(utils.copy(cartItem), newCartItemData));
                        }
                    })
                });
            });
            for (var i = 0; i < warehouseIds.length; i++) {
                $.each(warehouseList, function (key, warehouse) {
                    if (key.substr(10, key.length - 10) == warehouseIds[i]) {
                        var item = {
                            key: warehouse
                        };
                        newWarehouseList[key] = warehouse;
                    }
                })
            }
            shippingService.getShippingRates().each(function (rate) {
                if (rate.carrier_code == 'warehouse_multirate' || !rate.method_code) {
                    return;
                }
                var tmp = rate.method_code.split(self.SEPARATOR);

                if (tmp.length != 3) {
                    return;
                }
                var warehouseId = tmp[1];
                var vendorId = tmp[2];
                var index = 'warehouse_' + warehouseId;
                if (!warehouseList || warehouseList[index] == undefined) {
                    return false;
                }

                if (typeof (groups[index]) == 'undefined') {
                    groups[index] = {
                        items: warehouseWithItems[index],
                        selectedRate: ko.observable(false),
                        warehouse_id: warehouseId,
                        warehouse: warehouseList[index],
                        title: warehouseList[index].shipping_title,
                        rates: ko.observableArray([])
                    };
                }
                rate['warehouse_id'] = warehouseId;
                rate['vendor_id'] = vendorId;
                groups[index]['rates'].push(rate);
            });

            /**
             * Add null warehouse if they do not have any method
             */
            for (var key in newWarehouseList) {
                if (typeof (groups[key]) == 'undefined') {
                    groups[key] = {
                        items: warehouseWithItems[key],
                        selectedRate: ko.observable(false),
                        warehouse_id: newWarehouseList[key].entity_id,
                        warehouse: newWarehouseList[key],
                        title: newWarehouseList[key].shipping_title,
                        rates: []
                    }
                }
            }
            this.ratesByWarehouse([]);
            if (Object.entries(groups).length === 0) {
                return;
            }
            var listItemsWhDefault = groups['warehouse_1'] ? groups['warehouse_1'].items : [];
            var vendorByWh = [];
            for (var vendorId in vendorList) {
                if (!self.VENDOR_LIST.includes(vendorId)) {
                    continue;
                }
                var vendorData = vendorList[vendorId];
                vendorData['warehouses'] = [];
                let items = self.VENDOR_LIST_ITEM[vendorId];
                let newItems = [];
                $.each(listItemsWhDefault, function (index,item) {
                    if (items.includes(item.item_id)) {
                        newItems.push(item);
                    }
                });
                if (vendorId == mainVendor) {
                    for (var key in groups) {
                        if (key != 'warehouse_1') {
                            let newRates = groups[key].rates.filter(rate => rate['vendor_id'] == vendorId);
                            let newWh = _.extend(utils.copy(groups[key]), {rates: newRates});
                            vendorData['warehouses'].push(newWh);
                        }
                    }
                } else {
                    if (groups['warehouse_1']){
                        let newRates = groups['warehouse_1'].rates.filter(rate => rate['vendor_id'] == vendorId);
                        let newWh = _.extend(utils.copy(groups['warehouse_1']), {items: newItems,rates: newRates});
                        vendorData['warehouses'].push(newWh);
                    }

                }
                this.ratesByWarehouse.push(vendorData);
            };

        },

        /**
         * Sort the groups by warehouse id ASC
         */
        sortRates: function (groups) {
            var sortedGroups = [];
            while (true) {
                var minIndex = false;
                for (var index in groups) {
                    if (!minIndex) {
                        minIndex = index;
                    } else if (parseInt(groups[index].warehouse_id) < parseInt(groups[minIndex].warehouse_id)) {
                        minIndex = index;
                    }
                }
                if (!minIndex) {
                    break;
                }
                sortedGroups.push(groups[minIndex]);
                delete (groups[minIndex]);
            }

            return sortedGroups;
        },

        isMethodChecked: function(methodCode){
            if (!methodCode) {
                return false;
            }
            var shippingMethod = quote.shippingMethod();
            if(shippingMethod && shippingMethod['method_code']){
                var methods = shippingMethod['method_code'].split(this.METHOD_SEPARATOR);
                if (methods.includes(methodCode)){
                    return methodCode;
                }
            }

            return false;
        },

        changeWarehouseMethod: function (shippingMethod, event) {
            /*combine selected warehouse shipping methods and select the main method*/
            var methods = {},
                isAllWarehouseMethodSelected = true,
                method_code = shippingMethod.method_code,
                self = this;
            if (!method_code) {
                return true;
            }

            var tmp = method_code.split('||');
            if (tmp.length != 3) return true;

            var whId = tmp[1],
                vendorId = tmp[2];
            var selectedRates = this.selectedRates();
            if (!selectedRates[vendorId]) selectedRates[vendorId] = {};
            selectedRates[vendorId][whId] = method_code;
            this.selectedRates(selectedRates);

            $(this.ratesByWarehouse()).each(function (indexVendor, vendorWh) {
                $.each(vendorWh['warehouses'], function (indexWh, warehouseRates) {
                    if (!selectedRates[vendorWh['entity_id']] || !selectedRates[vendorWh['entity_id']][warehouseRates['warehouse_id']]) {
                        isAllWarehouseMethodSelected = false;
                    }
                });

            });

            if (!isAllWarehouseMethodSelected) {
                return true;
            }
            var selectedMethods = [];
            for (var vendorId in selectedRates) {
                for (var whId in selectedRates[vendorId]) {
                    selectedMethods.push(selectedRates[vendorId][whId]);
                }
            }

            var selectedMethodCode = selectedMethods.join(this.METHOD_SEPARATOR);
            var selectedMethod = false;
            /*Get the currently selected method*/
            $(this.rates()).each(function (index, rate) {
                if (!selectedMethod) {
                    let selected = true;
                    let methods = rate.method_code.split(self.METHOD_SEPARATOR);
                    $.each(methods, function (index, method) {
                        let tmp = method.split('_');
                        if (!selectedMethods.includes(tmp[1])) {
                            selected = false;
                        }
                    });
                    if (selected) {
                        selectedMethod = rate;
                    }
                }
            });

            if (selectedMethod !== false) {
                /*Call the parent class method*/
                this.selectShippingMethod(selectedMethod);
            }

            return true;
        },

        sortWarehouseMethods: function (methods) {
            var sortedMethods = [];
            while (true) {
                var minIndex = false;
                for (var index in methods) {
                    if (!minIndex) {
                        minIndex = index;
                    } else {
                        var minWarehouseId = parseInt(minIndex.replace('warehouse_', ''));
                        var warehouseId = parseInt(index.replace('warehouse_', ''));
                        if (minWarehouseId > warehouseId) {
                            minIndex = index;
                        }
                    }
                }
                if (!minIndex) {
                    break;
                }
                sortedMethods.push(methods[minIndex]);
                delete (methods[minIndex]);
            }

            return sortedMethods;
        },

        isAllWarehouseMethodSelected: function () {
            for (var index in this.selectedWarehouseMethods) {
                if (this.selectedWarehouseMethods[index]() === false) {
                    return false;
                }
            }
            return true;
        }
    };

    return function (target) { // target == Result that Magento_Ui/.../default returns.
        return target.extend(mixin); // new result that all other modules receive
    };
});