/**
 * Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 */

define([
        'jquery',
        'mageUtils',
        'Tigren_StorePickup/js/model/shipping-rates-validation-rules/storepickup',
        'mage/translate'
    ], function ($, utils, validationRules, $t) {
        'use strict';
        return {
            validationErrors: [],
            validate: function (address) {
                var self = this;
                this.validationErrors = [];
                $.each(validationRules.getRules(), function (field, rule) {
                    if (rule.required && utils.isEmpty(address[field])) {
                        var message = $t('Field ') + field + $t(' is required.');
                        var regionFields = ['region', 'region_id', 'region_id_input', 'city'];
                        if (
                            $.inArray(field, regionFields) === -1
                            || utils.isEmpty(address['region']) && utils.isEmpty(address['region_id'] && utils.isEmpty(address['city']))
                        ) {
                            self.validationErrors.push(message);
                        }
                    }
                });
                return !Boolean(this.validationErrors.length);
            }
        };
    }
);