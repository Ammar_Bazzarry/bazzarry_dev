define([
    'underscore',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/customer-data'
], function (_, Component, quote, customerData) {
    'use strict';

    var countryData = customerData.get('directory-data');

    return function (Component) {
        return Component.extend({
            defaults: {
                template: 'Tigren_StorePickup/shipping-information'
            },

            initialize: function () {
                this._super();
            },

            isStoreSelected: function () {
                var shippingMethod = quote.shippingMethod(),
                    shippingAddress = quote.shippingAddress();

                if (shippingMethod && shippingAddress
                    && shippingAddress['extension_attributes']
                    && shippingAddress['extension_attributes']['pickupstore_id']) {
                    return 1;
                }

                return 0;
            },

            getStoreDetails: function () {
                var shippingMethod = quote.shippingMethod(),
                    shippingAddress = quote.shippingAddress();
                if (shippingMethod && shippingAddress
                    && shippingAddress['extension_attributes']
                    && shippingAddress['extension_attributes']['pickupstore_id']) {
                    return window.checkoutConfig.pickupStores[shippingAddress['extension_attributes']['pickupstore_id']];
                }

                return false;
            },

            getCountryName: function (countryId) {
                return (countryData()[countryId] !== undefined) ? countryData()[countryId].name : "";
            }
        });
    }
});