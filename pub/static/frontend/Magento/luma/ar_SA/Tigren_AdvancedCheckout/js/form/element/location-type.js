/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

define([
    'ko',
    'Magento_Ui/js/form/element/abstract'
], function (ko, Abstract) {
    'use strict';

    return Abstract.extend({
        defaults: {
            isChecked: ko.observable('home'),
            visible: true,

            links: {
                isChecked: 'value'
            },

            listens: {
                'isChecked': 'onCheckedChanged',
                'value': 'onExtendedValueChanged'
            }
        },

        /**
         * @returns {*|void|Element}
         */
        initObservable: function () {
            return this._super()
                .observe('isChecked');
        },

        /**
         * Handle checked state changes for checkbox / radio button.
         *
         * @param {*} newChecked
         */
        onCheckedChanged: function (newChecked) {
            // do something here
        },

        /**
         * Handle dataScope changes for checkbox / radio button.
         *
         * @param {*} newExportedValue
         */
        onExtendedValueChanged: function (newExportedValue) {
            // do something here
        }
    });
});
