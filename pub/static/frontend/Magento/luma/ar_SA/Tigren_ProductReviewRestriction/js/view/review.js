/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'ko',
    'mage/url',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'Magento_Customer/js/view/customer'
], function ($, ko, urlBuilder, Component, customerData) {
    'use strict';

    return Component.extend({

        showReview: ko.observable(false),
        showMessage: ko.observable(false),
        showMaxReview: ko.observable(false),

        /** @inheritdoc */
        initialize: function () {
            this._super();

            this.review = customerData.get('review').extend({
                disposableCustomerData: 'review'
            });
            this.allowReview();
        },

        /**
         * @return {*}
         */
        nickname: function () {
            return this.review().nickname || customerData.get('customer')().firstname;
        },

        allowReview: function () {
            var self = this;
            var url = urlBuilder.build('/reviewrestrict/review/validate');
            $.ajax({
                url: url,
                data: {product_id: this.source.get('productId')},
                method: 'post',
                success: function (response) {
                    if (response.allow) {
                        self.showReview(true);
                        self.showMessage(false);
                    } else {
                        self.showReview(false);
                        if(response.max_review){
                            self.showMaxReview(true);
                        }else{
                            self.showMessage(true);
                        }
                    }
                }
            });
        }
    });
});
