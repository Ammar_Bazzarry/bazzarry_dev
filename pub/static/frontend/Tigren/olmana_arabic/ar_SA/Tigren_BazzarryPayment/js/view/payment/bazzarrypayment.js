/*
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'bazzarrypayment',
                component: 'Tigren_BazzarryPayment/js/view/payment/method-renderer/bazzarrypayment-method'
            }
        );
        return Component.extend({});
    }
);