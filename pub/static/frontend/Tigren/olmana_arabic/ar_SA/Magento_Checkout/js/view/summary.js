/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'underscore',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/totals',
    'Magento_Checkout/js/model/step-navigator',
    'jquery/jquery.hashchange'
], function ($, _, ko, Component, totals, stepNavigator) {
    'use strict';

    return Component.extend({
        isLoading: totals.isLoading,
        nextBtnSelector: '#shipping-method-buttons-container button[type=submit]',
        isShowNextBtn : ko.observable(true),

        /** @inheritdoc */
        initialize: function () {
            this._super();
            $(window).hashchange(_.bind(this.checkStep, this));
        },

        onClickNextButton: function () {
            var currentStep = stepNavigator.steps()[stepNavigator.getActiveItemIndex()];
            switch (currentStep.code) {
                case 'shipping':
                    this.nextBtnSelector = '#shipping-method-buttons-container button[type=submit]';
                    break;
                default:
                    this.nextBtnSelector = '#shipping-method-buttons-container button[type=submit]';
            }

            $(this.nextBtnSelector).click();
        },

        checkStep: function () {
            var hashString = window.location.hash.replace('#', '');
            switch (hashString) {
                case 'payment':
                    this.isShowNextBtn(false);
                    break;
                default:
                    this.isShowNextBtn(true);
            }
        },

        handleAfterRender: function () {
            this.checkStep();
        }
    });
});
