/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

define([
    'uiComponent',
    'ko',
    'mage/url',
    'jquery',
    'mage/translate'
], function (Component, ko, urlBuilder, $, $t) {
    'use strict';

    return Component.extend({
        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.getProfileData();
        },

        salesCount: ko.observable(''),

        reviewCount: ko.observable(''),

        ratingTitle: ko.observable('0%'),

        moreOfferTitle: ko.observable(''),

        minPrice: ko.observable(''),

        showMoreOffer: ko.observable(false),

        getProfileData: function () {
            var self = this;
            var url = urlBuilder.build('/customvendors/product/profile');
            var saleCount = '';
            var reviewCount = '';
            var rating = '';
            $.ajax({
                url: url,
                data: {
                    vendor_id: this.source.get('vendor_id'),
                    product_id: this.source.get('product_id'),
                    product_price: this.source.get('product_price')
                },
                method: 'post',
                success: function (response) {
                    if (parseInt(response.review_count) > 1) {
                        reviewCount = response.review_count + ' ' + $t('reviews')
                    } else {
                        reviewCount = response.review_count + ' ' + $t('review')
                    }
                    self.reviewCount(reviewCount);
                    if (parseInt(response.sales_count) > 1) {
                        saleCount = response.sales_count + ' ' + $t('sales')
                    } else {
                        saleCount = response.sales_count + ' ' + $t('sale')
                    }
                    self.salesCount(saleCount);
                    rating = response.rating + '%';
                    self.ratingTitle(rating);
                    $('.rating-width').css({width: rating});
                    var moreOfferTitle$t = $t('(%1) more offers starting from %2').replace('%1',response.more_offer).replace('%2',response.min_price);
                    self.moreOfferTitle(moreOfferTitle$t);
                    self.minPrice(response.min_price);
                    self.showMoreOffer(response.show_more_offer);
                }
            });
        }

    });
});
