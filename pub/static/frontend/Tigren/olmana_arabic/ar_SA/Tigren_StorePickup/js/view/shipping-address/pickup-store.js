/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Ui/js/form/element/select',
    'jquery',
    'ko',
    'mage/url',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/full-screen-loader',
    'mage/translate',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/action/select-shipping-address'
], function (
    Select,
    $,
    ko,
    getUrl,
    quote,
    fullScreenLoader,
    $t,
    addressConverter,
    selectShippingAddress
) {
    'use strict';

    var storePickupOptions = [],
        stores = window.checkoutConfig.pickupStores ? window.checkoutConfig.pickupStores : [];

    _.each(stores, function (store) {
        var option = {
            label: store.store_name,
            value: store.store_id
        };
        storePickupOptions.push(option);
    }, this);

    storePickupOptions.sort(function (a, b) {
        if (a.label.toLowerCase() > b.label.toLowerCase()) {
            return 1
        }
        if (a.label.toLowerCase() < b.label.toLowerCase()) {
            return -1
        }
        return 0;
    });

    return Select.extend({
        defaults: {
            modules: {
                stateSuggestionField: '${ $.parentName }.state-suggestion',
                parent: '${ $.parentName }'
            },

            imports: {
                update: '${ $.parentName }.state-suggestion:value'
            },

            options: []
        },

        initialize: function () {
            this._super();

            storePickupOptions.unshift({
                'label': $t('-- Select Pickup Location --'),
                'value': ''
            });
            this.options(storePickupOptions);
            this.on('value', this.onChangeStore.bind(this));

            return this;
        },

        update: function (value) {
            if (!value) {
                this.options(storePickupOptions);
                return;
            }

            var options = [];

            _.each(stores, function (store) {
                if (store.region_id === value) {
                    var option = {
                        label: store.store_name,
                        value: store.store_id
                    };
                    options.push(option);
                }
            }, this);

            options.sort(function (a, b) {
                if (a.label.toLowerCase() > b.label.toLowerCase()) {
                    return 1
                }
                if (a.label.toLowerCase() < b.label.toLowerCase()) {
                    return -1
                }
                return 0;
            });
            options.unshift({
                'label': $t('-- Select Pickup Location --'),
                'value': ''
            });

            this.options(options);
        },

        onChangeStore: function (value) {
            if ($.type(value) === 'object' && $.type(value.value()) === 'string') {
                this.clear();
                this.stateSuggestionField().clear();
            }
        }
    });
});