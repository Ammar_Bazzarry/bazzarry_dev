/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'domReady!'
], function ($, keyboardHandler) {
    'use strict';

    if ($('body').hasClass('checkout-cart-index')) {
        if ($('#co-shipping-method-form .fieldset.rates').length > 0 &&
            $('#co-shipping-method-form .fieldset.rates :checked').length === 0
        ) {
            $('#block-shipping').on('collapsiblecreate', function () {
                $('#block-shipping').collapsible('forceActivate');
            });
        }
    }

    $('.cart-summary').mage('sticky', {
        container: '#maincontent'
    });


    $('.panel.header > .header.links').clone().appendTo('#store\\.links');

    if($('.navigation-sidebar').length){
        $('.navigation-sidebar li.parent').append( "<span class='arrow'></span>" );
        $('.navigation-sidebar li.parent .arrow').click(function (){
            $(this).prev().toggle();
            $(this).toggleClass("active");
        })

    }

    var heightHeader = $('.page-header').height();
    var topcat = $('.header.content').offset();

    if (topcat) {
        var orgElementTop = topcat.top;
        $(window).scroll(function () {
            if ($(window).scrollTop() >= (orgElementTop)) {
                $('.page-header').addClass('fixed');
                $('.page-wrapper').css('padding-top', heightHeader);
            } else if($(window).scrollTop() === (orgElementTop)) {
                $('.page-header').removeClass('fixed');
                $('.page-wrapper').css('padding-top', 0);
            }else {
                $('.page-header').removeClass('fixed');
                $('.page-wrapper').css('padding-top', 0);
            }
        });
    }

    $('.custom-header-dropdown-link').click(function (){
        $('.mb-login-dropdown').hide();
        $('.customer-welcome, .customer-name').removeClass('active');
    })

    $('.customer-welcome .customer-name').click(function (){
        $('.custom-header-dropdown-parent .mage-dropdown-dialog').hide();
        $('.custom-header-dropdown-parent, .custom-header-dropdown-link ').removeClass('active');
    });

    keyboardHandler.apply();
});
