/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'owlcarousel'
], function ($, $t) {
    'use strict';

    $.widget('tigren.owlWidget', {
        owlObj: undefined,
        options: {
            nav: true,
            dots: true,
            stagePadding: 0,
            loop: false,
            maxScreenDestroy: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 3
                },
                1600: {
                    items: 4
                }
            }
        },

        /** @inheritdoc */
        _create: function () {
            var self = this;

            if(!this.options.maxScreenDestroy) {
                self.initSlider();
            } else {
                // First load
                self.initResponsive(this.options.maxScreenDestroy);
                // On resize window
                self.updateResize(this.options.maxScreenDestroy);
            }
        },

        updateResize: function (maxScreenWidth) {
            var self = this;
            $(window).resize(function() {
                self.initResponsive(maxScreenWidth);
            });
        },

        initResponsive: function (maxScreenWidth) {
            var self = this;

            var windowWidth = $(window).width();
            if(windowWidth < maxScreenWidth) {
                if (this.owlObj) {
                    this.owlObj.trigger('destroy.owl.carousel');
                    this.owlObj.removeClass('owl-carousel');
                }
            } else {
                if (this.owlObj && !this.element.hasClass('owl-loaded')) {
                    if (!this.element.hasClass('owl-carousel')) {
                        this.owlObj.addClass('owl-carousel');
                    }
                    self.initSlider();
                } else if (!this.owlObj) {
                    self.initSlider();
                }
            }
        },

        initSlider: function () {
            this.owlObj = this.element.owlCarousel(this.options);
        }
    });

    return $.tigren.owlWidget;
});
