/*
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

define([
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/model/sidebar'
], function ($, Component, quote, stepNavigator, sidebarModel) {
    'use strict';

    var mixin = {
        SEPARATOR: '||',
        METHOD_SEPARATOR: '|_|',

        getShippingMethodTitle: function () {
            var self = this;
            var warehouseList = checkoutConfig.warehouse;
            var vendorList = checkoutConfig.vendors_list;
            var shippingMethod = quote.shippingMethod();
            if (!shippingMethod) {
                return "";
            }
            var methods = shippingMethod.method_title;
            methods = methods.split(self.METHOD_SEPARATOR);
            var mainVendor = window.checkoutConfig.main_vendor;
            var methodTitle = "";
            for (var index in methods) {
                var methodCode = methods[index];
                var warehouseMethodTitle = methodCode.toString().split(self.SEPARATOR);
                var warehouseId = warehouseMethodTitle[1];
                var vendorId = warehouseMethodTitle[2];
                if(vendorId == mainVendor){
                    if (warehouseList['warehouse_' + warehouseId] != undefined) {
                        var warehouseTitle = warehouseList['warehouse_' + warehouseId];
                        warehouseTitle = warehouseTitle.shipping_title;
                        warehouseMethodTitle = warehouseTitle + " : " + warehouseMethodTitle[0];
                        methodTitle += warehouseMethodTitle + "<br>";
                    }
                }else{
                    if (vendorList[vendorId] != undefined) {
                        var vendorTitle = vendorList[vendorId];
                        vendorTitle = vendorTitle.shipping_title;
                        warehouseMethodTitle = vendorTitle + " : " + warehouseMethodTitle[0];
                        methodTitle += warehouseMethodTitle + "<br>";
                    }
                }

            }
            methodTitle = methodTitle.trim();
            methodTitle = methodTitle.slice(0, -1);
            return methodTitle;
        }
    };

    return function (target) { // target == Result that Magento_Ui/.../default returns.
        return target.extend(mixin); // new result that all other modules receive
    };
});