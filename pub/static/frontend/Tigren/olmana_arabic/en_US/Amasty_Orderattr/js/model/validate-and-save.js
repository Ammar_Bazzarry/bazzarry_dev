define([
    'jquery',
    'underscore',
    'mage/storage',
    'Amasty_Orderattr/js/action/amasty-validate-form',
    'Amasty_Orderattr/js/action/amasty-prepare-attributes-for-api',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/url-builder'
], function ($, _, storage, validateForm, prepareAttributesForApi, customer, fullScreenLoader, errorProcessor, urlBuilder) {
    'use strict';

    var sentDataSnapshot = false;

    return function (attributesForm) {
        var waitForOrderAttributesResult = $.Deferred(),
            result = validateForm(attributesForm.attributeTypes);

        if (!result) {

            return waitForOrderAttributesResult.reject();
        } else {
            var postData = prepareAttributesForApi(result, attributesForm.formCode);

            if ((sentDataSnapshot && _.isEqual(postData.entityData, sentDataSnapshot.entityData))
                || _.isEmpty(postData.entityData.custom_attributes)
            ) {
                return waitForOrderAttributesResult.resolve();
            }

            sentDataSnapshot = postData;
            fullScreenLoader.startLoader();

            storage.post(
                urlBuilder.createUrl(
                    customer.isLoggedIn()
                    ? '/amasty_orderattr/checkoutData'
                    : '/amasty_orderattr/guestCheckoutData',
                    {}
                ),
                JSON.stringify(postData)
            ).done(
                function (response) {
                    if (!_.isUndefined(response.errors)) {
                        console.error(response.errors)
                    }
                    waitForOrderAttributesResult.resolve();
                    fullScreenLoader.stopLoader();
                }
            ).fail(
                function (response) {
                    errorProcessor.process(response);
                    waitForOrderAttributesResult.resolve();
                    fullScreenLoader.stopLoader();
                }
            );

            return waitForOrderAttributesResult.promise();
        }
    }
});
