/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'Magento_Ui/js/grid/columns/column',
    'Magento_Catalog/js/product/uenc-processor',
    'mage/url'
], function (Column, uencProcessor, urlBuilder) {
    'use strict';

    return Column.extend({
        defaults: {
            label: ''
        },


        getQuickviewUrl: function (row) {
            return urlBuilder.build('quickview/catalog_product/view') + '/id/' + row['id'];
        },

        isAllowed: function () {
            return this.source();
        },

        getLabel:function () {
            return this.label;
        }
    });
});
