/**
 * Copyright (c) 2019 Tigren Solutions
 * https://www.tigren.com
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-rates-validation-rules',
    'Tigren_StorePickup/js/model/shipping-rates-validator/storepickup',
    'Tigren_StorePickup/js/model/shipping-rates-validation-rules/storepickup'
], function (
    Component,
    defaultShippingRatesValidator,
    defaultShippingRatesValidationRules,
    storepickupShippingRatesValidator,
    storepickupShippingRatesValidationRules
) {
    'use strict';

    defaultShippingRatesValidator.registerValidator('storepickup', storepickupShippingRatesValidator);
    defaultShippingRatesValidationRules.registerRules('storepickup', storepickupShippingRatesValidationRules);

    return Component;
});