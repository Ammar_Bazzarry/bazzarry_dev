/*
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */
define([
    'Magento_Ui/js/modal/modal',
    'mage/url',
    'uiComponent',
    'jquery',
    'domReady!'
], function ( modal, urlBuilder, Component, $) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Tigren_VendorsPriceComparison/vendor/viewproduct'
        },

        initialize: function () {
            this._super();
        },

        getProductUrl: function (product) {
            var href = window.location.href;
            var search = window.location.search;
            var searchParam = new window.URLSearchParams(window.location.search);
            var vendorId = product.pc_vendor.pc_vendor_id;
            if(search && searchParam.get('vid')){
                if(searchParam.get('vid')){
                    var vid = searchParam.get('vid');
                    href = href.replace('vid='+vid,'vid='+vendorId);
                }else{
                    href = href + '&vid=' + vendorId;
                }
            }else{
                href = href + '?vid=' + vendorId;
            }
            return href;
        },
    });
});
