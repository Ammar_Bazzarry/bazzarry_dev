/*
 * Copyright (c) 2017 www.tigren.com
 */

define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'bankdeposits',
                component: 'Tigren_BankDeposits/js/view/payment/method-renderer/bankdeposits-method'
            }
        );
        return Component.extend({});
    }
);