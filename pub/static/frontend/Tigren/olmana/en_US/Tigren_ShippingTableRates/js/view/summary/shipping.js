/*
 * @author Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license Open Software License ("OSL") v. 3.0
 */

define([
    'jquery',
    'Magento_Checkout/js/view/summary/shipping',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-service'
], function ($, Component, quote, shippingService) {
    'use strict';

    var mixin = {
        CARRIER_CODE: 'warehouse_multirate',
        SEPARATOR: '||',
        METHOD_SEPARATOR: '|_|',

        defaults: {
            template: 'Tigren_ShippingTableRates/summary/shipping'
        },
        quote: quote,

        initialize: function () {
            this._super();
            window.TEST = this;
        },

        getRatesByWarehouse: function () {
            var self = this;
            var groups = {};
            var warehouseList = checkoutConfig.warehouse; /*The list of warehouses that have products in current shopping cart*/
            var vendorList = window.checkoutConfig.vendors_list;

            shippingService.getShippingRates().each(function (rate) {
                if (rate.carrier_code == 'warehouse_multirate' || !rate.method_code) {
                    return;
                }
                var tmp = rate.method_code.split(self.SEPARATOR);

                if (tmp.length != 3) {
                    return;
                }
                var warehouseId = tmp[1];
                var vendorId = tmp[2];
                var index = 'warehouse_' + warehouseId + '_' + vendorId;
                if (!warehouseList ) {
                    return false;
                }
                var mainVendor = window.checkoutConfig.main_vendor;
                if (typeof (groups[index]) == 'undefined') {
                    if (vendorId == mainVendor){
                        groups[index] = {
                            warehouse_id: warehouseId,
                            warehouse: warehouseList['warehouse_' + warehouseId],
                            title: warehouseList['warehouse_' + warehouseId].shipping_title,
                            rates: []
                        };
                    }else{
                        groups[index] = {
                            warehouse_id: warehouseId,
                            warehouse:vendorList[vendorId],
                            title: vendorList[vendorId].shipping_title,
                            rates: []
                        };
                    }

                }
                rate['warehouse_id'] = warehouseId;
                rate['vendor_id'] = vendorId;
                groups[index]['rates'].push(rate);
            });

            return groups;
        },

        getValue: function () {
            var price;

            if (!this.isCalculated()) {
                return this.notCalculatedMessage;
            }
            //var price =  this.totals().shipping_amount; //comment this line

            var shippingMethod = quote.shippingMethod(); //add these both line
            var price =  shippingMethod.amount; // update data on change of the shipping method

            return this.getFormattedPrice(price);
        },

        getDetails: function () {
            var self = this;
            var details = [];
            var warehouseRates = this.getRatesByWarehouse();

            var shippingMethod = quote.shippingMethod();
            if (!shippingMethod) {
                return details;
            }
            var methods = typeof (shippingMethod.method_code) != 'undefined' ? shippingMethod.method_code : false;

            if (!methods) {
                return details;
            }

            methods = methods.split(this.METHOD_SEPARATOR);

            for (var index in methods) {
                var methodCode = methods[index];
                var warehouse = methodCode.toString().split(self.SEPARATOR);
                var warehouseId = warehouse[1];
                var vendorId = warehouse[2];
                if (warehouseRates['warehouse_' + warehouseId + '_' + vendorId] != undefined) {
                    var rates = warehouseRates['warehouse_' + warehouseId + '_' + vendorId]['rates'];
                    for (var i in rates) {
                        if (methodCode == (rates[i].carrier_code + '_' + rates[i].method_code)) {
                            rates[i].warehouse = warehouseRates['warehouse_' + warehouseId + '_' + vendorId].warehouse;
                            details.push(rates[i]);
                            break;
                        }
                    }
                }
            }

            return details;
        },

        formatPrice: function (amount) {
            return this.getFormattedPrice(amount);
        }
    };

    return function (target) { // target == Result that Magento_Ui/.../default returns.
        return target.extend(mixin); // new result that all other modules receive
    };
});